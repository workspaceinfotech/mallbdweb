<?php
/**
 * Project  : mallbdweb
 * File     : NotificationModel.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 4/5/16 - 2:07 PM
 */
namespace App\Model;

class NotificationModel extends BaseMallBDModel {

    protected $primaryKey = 'id';
    protected $table      = 'notifications';

    /**
     * @param mixed $notification_type
     */
    public function setNotificationType($notification_type) {

        $this->setObj($notification_type);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "notification_type";
            $errorObj->msg = "Notification Type is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }

        $this->notification_type = $this->getObj();
        return true;
    }

    /**
     * @param mixed $notification_id
     */
    public function setNotificationId($notification_id) {

        $this->setObj($notification_id);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "notification_id";
            $errorObj->msg = "Notification id is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "notification_id";
            $errorObj->msg = "Notification id int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }
        $this->notification_id = $this->getObj();
        return true;
    }

    /**
     * @param mixed $notification_text
     */
    public function setNotificationText($notification_text) {

        $this->setObj($notification_text);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "notification_text";
            $errorObj->msg = "Notification Text is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }

        $this->notification_text = $this->getObj();
        return true;

    }


}
