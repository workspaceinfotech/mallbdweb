<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: BaseMallBDModel.php
 * User: rajib
 * Date: 1/12/16
 * Time: 1:02 PM
 */

namespace App\Model;

use App\Model\DataModel\AppCredential;
use App\Model\DataModel\MallBDCurrency;
use Exception;
use Illuminate\Database\Eloquent\Model as Eloquent;


class BaseMallBDModel extends Eloquent{
    public $shopId = 1;
    public $timestamps = false;
    public $errorManager;
    public $modelValidation;
    public $email_validation_pattern="/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/"; 

    private $obj;
    public $errorStatus;
    public $errorMsg;
    public $customLimit;
    public $customOffset;
    protected $customOrder = "asc";
    protected $currentUserId;
    public $fnObj = null;
    public $fnErrorMsg = "";
    public $fnError = false;
    public static $currency;
    public $NewProductOldDate;

    public function setCurrentUserId($currentUserId){
        $this->currentUserId = $currentUserId;
    }
    /**
     * @param string $customOrder
     */
    public function setCustomOrderAsc()
    {
        $this->customOrder = "asc";
    }
    public function setCustomOrderDesc()
    {
        $this->customOrder = "desc";
    }
    /**
     * BaseCabguardAdvt constructor.
     * @param bool $timestamps
     */

    public function setCustomLimit($customLimit){
        $customLimitInt = 1;
        try{
            $customLimitInt= intval($customLimit);
            $customLimitInt = ($customLimitInt==0)?1:$customLimitInt;
        }catch (Exception $ex){
            $errorObj = new ErrorObj();

            $errorObj->params = "limit";
            $errorObj->msg = "limit int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
//        $this->customLimit= ($customLimitInt>20)?20:$customLimitInt;
        $this->customLimit= $customLimitInt;
        return true;
    }
    public function setCustomOffset($offset){
        try{
            if($offset<0){
                $this->customOffset = 0;
                return true;
            }
            $this->customOffset= (int)$offset;
        }catch (Exception $ex){
            $errorObj = new ErrorObj();

            $errorObj->params = "offset";
            $errorObj->msg = "offset int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }

    }
    public function __construct()
    {
        $this->obj = new \stdClass();
        $this->errorStatus= false;
        $this->errorMsg = "";

        $this->errorManager = new ErrorManager();

        $this->customLimit = -1;
        $this->customOffset = -1;
        $this->NewProductOldDate=date('Y-m-d', strtotime(' -20 day'));
    }

    public function getObj()
    {
        return $this->obj;
    }

    /**
     * @param mixed $obj
     */
    public function setObj($obj)
    {
        $this->obj = $obj;
    }

    public function basicFilter(){
        $this->obj = trim($this->obj);
    }
    public function basicValidation(){
        $this->basicFilter();
        if($this->obj == null || $this->obj=="")
            return false;
        return true;
    }
    public function validationWithoutFilter(){
        if($this->obj == null || $this->obj=="")
            return false;
        return true;
    }


    public static function setCurrency($currency){

        self::$currency = $currency;
        return true;
    }

}
class ErrorManager{
    public $errorObj;

    /**
     * ErrorManager constructor.
     * @param $msg
     * @param $status
     */
    public function __construct(){
        $this->errorObj = [];

    }
}
class ErrorObj{
    public $params;
    public $msg;
    function __construct() {
        $this->params = "";
        $this->msg = "";
    }
}