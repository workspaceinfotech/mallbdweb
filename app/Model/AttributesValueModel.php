<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: AttributesValueModel.php
 * User: rajib
 * Date: 1/14/16
 * Time: 3:32 PM
 */

namespace App\Model;


class AttributesValueModel extends BaseMallBDModel{

    protected $primaryKey = 'id';
    protected $table = 'attribute_values';

} 