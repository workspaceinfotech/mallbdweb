<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: ShopModel.php
 * User: rajib
 * Date: 1/13/16
 * Time: 6:53 PM
 */

namespace App\Model;


use App\Model\DataModel\Shop;

class ShopModel extends BaseMallBDModel{
    protected $primaryKey = 'id';
    protected $table = 'shops';

    public function createdBy(){
        return $this->hasOne("App\Model\UserModel","id","created_by");
    }
    public function setId($id){
        $this->id = $id;
    }
    public function getById(){
        $dateRow = $this->with('createdBy')->where("id","=",$this->id)->get()->first();
        $shop = new Shop();
        $shop->castMe($dateRow);
        return $shop;
    }

} 