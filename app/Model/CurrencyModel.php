<?php

namespace App\Model;


use App\Model\DataModel\Currency;

class CurrencyModel extends BaseMallBDModel{

    protected $primaryKey = 'id';
    protected $table = 'currency';
    public function setId($id) {

        $this->setObj($id);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "currency_id";
            $errorObj->msg = "Currency ID is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "currency_id";
            $errorObj->msg = "Currency ID int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        $this->id = $this->getObj();
        return TRUE;
    }
    public function isCurrencyById(){
        $count = $this->where('id','=',$this->id)->limit(1)->count();


        return ($count>0)?true:false;
    }
    public function getCurrencyById(){
        $currencyData = $this->where('id','=',@$this->id)->limit(1)->first();
        $currency = new Currency();
        $currency->castMe($currencyData);

        if($currency->id<0){
            $this->fnErrorMsg = 'Currency id not exist';
        }
        return $currency;
    }
     public function BDTtoUSD($ammount){
        $usdammount=$ammount*0.013;
        return $usdammount;
    }

} 