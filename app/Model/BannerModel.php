<?php
/**
 * Project  : mallbdweb
 * File     : BannerModel.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 3/24/16 - 4:05 PM
 */

namespace App\Model;

class BannerModel extends BaseMallBDModel {

    protected $primaryKey = 'id';
    protected $table      = 'banners';

    private $image;
    private $description;
    private $created_on;

    /**
     * @param mixed $image
     */
    public function setImage($image) {

        $this->setObj($image);
        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "image";
            $errorObj->msg = "Image is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }

        $this->image = $this->getObj();
        return TRUE;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description) {

        $this->setObj($description);
        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "description";
            $errorObj->msg = "Description is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        $this->description = $this->getObj();
        return TRUE;
    }

    /**
     * @param mixed $created_on
     */
    public function setCreatedOn($created_on) {

        $this->setObj($created_on);
        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "created_on";
            $errorObj->msg = "Created on is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        $this->created_on = $this->getObj();
        return TRUE;
    }

    function getAllBanners($shopId = 1)
    {
        $result = BannerModel::where('shop_id', $shopId)->get();
        return $result;
    }


}