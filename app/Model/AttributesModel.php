<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: AttributesModel.php
 * User: rajib
 * Date: 1/14/16
 * Time: 3:31 PM
 */

namespace App\Model;

use App\Model\DataModel\Attributes;
use DB;
class AttributesModel extends BaseMallBDModel{

    protected $primaryKey = 'id';
    protected $table = 'attributes';
    private $categoryIdList;

    public function attributesValues()
    {
        return $this->hasMany("App\Model\AttributesValueModel","attribute_id","id");
    }
    public function setCategoryIdList($categoryIdList){
        $this->categoryIdList = $categoryIdList;
    }
    public function getByProductList(){
        $attributesValues = $this->with("attributesValues")
            ->select('attributes.*')
            ->distinct()
            ->join('product_attributes', 'product_attributes.attribute_id', '=', 'attributes.id')
            ->join('product_categories', 'product_categories.product_id', '=', 'product_attributes.product_id')
            ->whereIn('product_categories.category_id', $this->categoryIdList )->get();

        $attributesList = [];
        foreach($attributesValues as $attributeVal){
            $attribute = new Attributes();
            $attribute->castMe($attributeVal);
            array_push($attributesList,$attribute);
        }



        return $attributesList;//$this->with("attributesValues")->whereIn("id",[1,2])->get()->all();
    }
    public function getSelectedAttributes($combinationKey){
        $attributesValues = $this->select('attributes.*')

            ->join('attribute_values', 'attributes.id', '=', 'attribute_values.attribute_id')
            ->join('product_attributes_combinations','attributes.id','=','product_attributes_combinations.attributes_id')
            ->join('product_attributes_combinations as pac','attribute_values.id','=','pac.attribute_value_id')
            ->where('product_attributes_combinations.combination_key','=',$combinationKey)
            ->get();

//        $attributesList = [];
//        foreach($attributesValues as $attributeVal){
//            $attribute = new Attributes();
//            $attribute->castMe($attributeVal);
//            array_push($attributesList,$attribute);
//        }



        return $attributesValues;//$this->with("attributesValues")->whereIn("id",[1,2])->get()->all();
    }

} 