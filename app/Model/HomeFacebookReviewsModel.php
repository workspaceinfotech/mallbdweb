<?php
/**
 * Project  : mallbdweb
 * File     : ZonalOrderModel.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 3/2/16 - 5:38 PM
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class HomeFacebookReviewsModel extends BaseMallBDModel{

    protected $primaryKey = 'id';
    protected $table = 'home_facebook_reviews';

    public function getAllHomeFacebookReviews()
    {
        $all_data = HomeFacebookReviewsModel::all();

        if($all_data==null)
        {
            return null;
        }
        return $all_data;
    }

} 