<?php
/**
 * Project  : mallbdweb
 * File     : OrderProductsModel.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 2/19/16 - 11:05 AM
 */

namespace App\Model;

use App\Model\DataModel\OrderProducts;

class OrderProductsModel extends BaseMallBDModel {

    protected $primaryKey = 'id';
    protected $table      = 'order_products';


    /**
     * @param mixed $order_id
     */
    public function setOrderId($order_id) {

        $this->setObj($order_id);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "order_id";
            $errorObj->msg = "Order id is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "order_id";
            $errorObj->msg = "Order id int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }
        $this->order_id =(int) $this->getObj();
        return true;
    }

    /**
     * @param mixed $product_id
     */
    public function setProductId($product_id) {

        $this->setObj($product_id);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "product_id";
            $errorObj->msg = "Product id is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "product_id";
            $errorObj->msg = "Product id int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }
        $this->product_id =(int) $product_id;
        return true;
    }
    /**
     * @param mixed $product_id
     */
    public function setPackageId($package_id) {

        $this->setObj($package_id);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "package_id";
            $errorObj->msg = "Package id is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "package_id";
            $errorObj->msg = "Package id int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }
        $this->package_id =(int) $this->getObj();
        return true;
    }


    /**
     * @param mixed $discount_id
     */
    public function setDiscount($discount) {

        $this->setObj($discount);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "discount";
            $errorObj->msg = "Discount is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "discount";
            $errorObj->msg = "Discount  int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }
        $this->discount = (int)$this->getObj();
        return true;
    }

    /**
     * @param mixed $product_quantity
     */
    public function setProductQuantity($product_quantity) {

        $this->setObj($product_quantity);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "product_quantity";
            $errorObj->msg = "Product Quantity is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "product_quantity";
            $errorObj->msg = "Product Quantity int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }
        $this->product_quantity = $product_quantity;
        return true;
    }
    public function setPackageQuantity($package_quantity) {

        $this->setObj($package_quantity);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "package_quantity";
            $errorObj->msg = "package Quantity is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "package_quantity";
            $errorObj->msg = "Package Quantity int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }
        $this->package_quantity = $package_quantity;
        return true;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price) {

        $this->setObj($price);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "price";
            $errorObj->msg = "Price is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "price";
            $errorObj->msg = "Price int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }
        $this->price = $this->getObj();
        return true;
    }


    /**
     * @param mixed $total
     */
    public function setTotal($total) {

        $this->setObj($total);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "total";
            $errorObj->msg = "Total is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "total";
            $errorObj->msg = "Total int/float required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }
        $this->total = $this->getObj();
        return true;
    }

    /**
     * @param mixed $tax
     */
    public function setTax($tax) {

        $this->setObj($tax);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "tax";
            $errorObj->msg = "Tax is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "tax";
            $errorObj->msg = "Tax int/float required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }
        $this->tax = $this->getObj();
        return true;
    }

    public function setCreatedOn($created_on) {
        $this->created_on = $created_on;
        return true;
    }


    public function orderProductsProduct()
    {
        return $this->hasOne("App\Model\ProductModel","id","product_id");
    }


    public function getAll()
    {
        $results = OrderProductsModel::with("orderProductsOrder","orderProductsProduct")
        ->get();
        //return $result;


        if($results==null)
        {
            return null;
        }
        $resultList = [];
        foreach($results as $r)
        {
            $orderProduct = new OrderProducts();
            $orderProduct->castMeFromObj($r);
            array_push($resultList,$orderProduct);
        }
        return $resultList;
    }
    public function getByOrderId(){

       return  $this->select('id','order_id','product_id','package_id','product_quantity','package_quantity','price','total','tax','discount')
           ->groupBy('package_id')
           ->where('order_id','=',$this->order_id)
           ->where('package_id','>',0)->get();

    }
    public function getByOrderIdMobile(){

       return  $this->select('id','order_id','product_id','package_id','product_quantity','package_quantity','price','total','tax','discount')
           ->groupBy('package_id')
           ->where('order_id','=',$this->order_id)
           ->where('package_id','>',0)->get();

    }
    
    public function getProductByOrderId(){

       return  $this->select('id','order_id','product_id','product_quantity','price','total','tax','discount')
           ->where('order_id','=',$this->order_id)
           ->where('package_id','=',0)->get();

    }
    public function getPackageByOrderId(){

       return  $this->select('id','order_id','package_id','package_quantity','price','total','tax','discount')
           ->groupBy('package_id')
           ->where('order_id','=',$this->order_id)
           ->where('package_id','>',0)->get();

    }


}