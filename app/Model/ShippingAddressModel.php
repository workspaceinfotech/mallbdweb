<?php
/**
 * Project  : mallbdweb
 * File     : ShippingAddressModel.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 3/2/16 - 3:54 PM
 */

namespace App\Model;

use App\Model\DataModel\ShippingAddress;

class ShippingAddressModel extends BaseMallBDModel {

    protected $primaryKey = 'id';
    protected $table      = 'shipping_addresses';


    /**
     * @param mixed $id
     */
    public function setId($id) {

        $this->setObj($id);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "id";
            $errorObj->msg = "Id is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "$id";
            $errorObj->msg = "Id int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }
        $this->id = $this->getObj();
        return TRUE;
    }

    /**
     * @param mixed $shipping_address
     */
    public function setShippingAddress($shipping_address) {

        $this->setObj($shipping_address);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "shipping_address";
            $errorObj->msg = "Shipping Address is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        $this->shipping_address = $shipping_address;
        return TRUE;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id) {

        $this->setObj($user_id);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "user_id";
            $errorObj->msg = "User id is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "user_id";
            $errorObj->msg = "User id int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }
        $this->user_id = $user_id;
        return TRUE;
    }

    /**
     * @param mixed $shipping_country
     */
    public function setShippingCountry($shipping_country) {

        $this->setObj($shipping_country);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "shipping_country";
            $errorObj->msg = "Shipping Country is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        $this->shipping_country = $shipping_country;
        return TRUE;
    }

    /**
     * @param mixed $shipping_city
     */
    public function setShippingCity($shipping_city) {

        $this->setObj($shipping_city);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "shipping_city";
            $errorObj->msg = "Shipping City is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        $this->shipping_city = $shipping_city;
        return TRUE;
    }

    /**
     * @param mixed $shipping_zipcode
     */
    public function setShippingZipcode($shipping_zipcode) {

        $this->setObj($shipping_zipcode);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "shipping_zipcode";
            $errorObj->msg = "Shipping Zipcode is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        $this->shipping_zipcode = $shipping_zipcode;
        return TRUE;
    }

    public function getAllByUserId(){

        $data = $this->where('user_id','=',$this->user_id)->get();
        $shippingAddressList = [];
        foreach($data as $rowData){
            $shippingAddress = new ShippingAddress();
            $shippingAddress->castMe($rowData);
            array_push($shippingAddressList,$shippingAddress);
        }


        return $shippingAddressList;
    }
    public function getById(){

        $data = $this->where('id','=',$this->id)->get();
        $shippingAddress = new ShippingAddress();
        foreach($data as $rowData){
            $shippingAddress->castMe($rowData);
        }


        return $shippingAddress;
    }
    function updateInformation(){
        return $this->where("id","=",$this->id)
            ->where("user_id","=",$this->user_id)
            ->update(array('shipping_address' => $this->shipping_address,
                'shipping_country'=>$this->shipping_country,
                'shipping_city'=>$this->shipping_city,
                'shipping_zipcode'=>$this->shipping_zipcode));


    }

}