<?php
/**
 * Created by PhpStorm.
 * Project: MallBDFront
 * File: ProductAttributesCombinationModel.php
 * User: rajib
 * Date: 1/29/16
 * Time: 5:31 PM
 */

namespace App\Model;


use App\Model\DataModel\Attributes;
use App\Model\DataModel\AttributesValue;
use App\Model\DataModel\Category;

class ProductAttributesCombinationModel extends BaseMallBDModel{
    protected $primaryKey = 'id';
    protected $table = 'product_attributes_combinations';

    function getSelectedAttributeByCombinationKey($combinationKey){

        $data =  $this
        ->join('attributes','attributes.id','=','product_attributes_combinations.attributes_id')
        ->join('attribute_values','attribute_values.id','=','product_attributes_combinations.attribute_value_id')
        ->where('product_attributes_combinations.combination_key','=',$combinationKey)
        ->get();

        $attributeList = [];
        foreach($data as $row){
            $attribute = new Attributes();
            $attribute->id = $row->attributes_id;
            $attribute->name = $row->attribute_name;

            $attributeValue =  new AttributesValue();
            $attributeValue->id = $row->attribute_value_id;
            $attributeValue->attributeId =  $attribute->id;
            $attributeValue->value =  $row->value;


            array_push($attribute->attributesValue,$attributeValue);

            array_push($attributeList,$attribute);
        }
        return $attributeList;

    }
} 