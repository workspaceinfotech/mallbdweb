<?php

/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: ProductModel.php
 * User: rafsan
 * Date: 1/14/16
 * Time: 3:29 PM
 */

namespace App\Model;


class OrderStatusListModel extends BaseMallBDModel {

    protected $primaryKey = 'id';
    protected $table      = 'order_status_list';

}