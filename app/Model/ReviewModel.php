<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: ProductModel.php
 * User: rajib
 * Date: 1/14/16
 * Time: 3:29 PM
 */

namespace App\Model;



use App\Model\DataModel\Review;

class ReviewModel extends BaseMallBDModel {

    protected $primaryKey = 'id';
    protected $table      ='reviews';

    /**
     * @param mixed $product_id
     */
    public function setProductId($product_id) {

        $this->setObj($product_id);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "product_id";
            $errorObj->msg = "Product id is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "product_id";
            $errorObj->msg = "Product id int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }
        $this->product_id = $this->getObj();
        return TRUE;
    }

    /**
     * @param mixed $customer_id
     */
    public function setCustomerId($customer_id) {

        $this->setObj($customer_id);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "customer_id";
            $errorObj->msg = "Customer id is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "customer_id";
            $errorObj->msg = "Customer id int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }

        $this->customer_id = $this->getObj();
        return TRUE;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title) {

        $this->setObj($title);
        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "title";
            $errorObj->msg = "Title is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }

        $this->title = $this->getObj();
        return TRUE;
    }

    /**
     * @param mixed $note
     */
    public function setNote($note) {

        $this->setObj($note);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "note";
            $errorObj->msg = "Note is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }

        $this->note = $this->getObj();
        return TRUE;
    }

    /**
     * @param mixed $rating
     */
    public function setRating($rating) {

        $this->setObj($rating);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "rating";
            $errorObj->msg = "Rating is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "rating";
            $errorObj->msg = "Rating int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }
        $this->rating = $this->getObj();
        return TRUE;
    }


    function isDuplicate($customerId,$productId)
    {
        $countResult = ReviewModel::where("product_id","=",$productId)
                                  ->where("customer_id","=",$customerId)
        ->count();

        if($countResult>=1)
        {
            return true;
        }
        else{
            return false;
        }
    }

    /*public function reviewProduct()
    {
        return $this->hasOne("App\Model\ProductModel","id","product_id");//(modelName,foreignKey,PrimaryKey)
    }*/

    public function rewardCustomer()
    {
        return $this->hasOne("App\Model\UserModel","id","customer_id");//(modelName,foreignKey,PrimaryKey)
    }

    public function getAllReviewByProduct($productId){

        //$queryObj->limit($this->customLimit)->offset($this->customLimit*$this->customOffset)->get() ;

        //::limit(30)->offset(30)->get()

        $reviews= $this->with("rewardCustomer")->where("product_id","=",$productId)->limit($this->customLimit)->offset($this->customLimit*$this->customOffset)->get();

        $reviewList = [];

        foreach($reviews as $r)
        {
            $review = new Review();
            $review->castMeFromObj($r);
            array_push($reviewList,$review);
        }

        return $reviewList;

    }
    
    public function getReviewById($Id){

        //$queryObj->limit($this->customLimit)->offset($this->customLimit*$this->customOffset)->get() ;

        //::limit(30)->offset(30)->get()

        $reviews= $this->with("rewardCustomer")->where("id","=",$Id)->get();

        $reviewList = [];

        foreach($reviews as $r)
        {
            $review = new Review();
            $review->castMeFromObj($r);
            array_push($reviewList,$review);
        }

        return $reviewList;

    }

}