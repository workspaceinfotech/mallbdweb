<?php
/**
 * Project  : mallbdweb
 * File     : ZonalOrderModel.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 3/2/16 - 5:38 PM
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class HomeYoutubeVideosModel extends BaseMallBDModel{

    protected $primaryKey = 'id';
    protected $table = 'home_youtube_videos';

    public function getAllHomeYoutubeVideos()
    {
        $all_data = HomeYoutubeVideosModel::all();

        if($all_data==null)
        {
            return null;
        }
        return $all_data;
    }

} 