<?php
/**
 * Project  : mallbdweb
 * File     : ProductCartFrontModel.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 4/13/16 - 4:30 PM
 */

namespace App\Model;

use App\Model\DataModel\ProductCartFront;

class ProductCartFrontModel extends BaseMallBDModel {

    protected $primaryKey = 'id';
    protected $table      = 'product_cart_front';


    /**
     * @param mixed $userId
     */
    public function setUserId($user_id) {

        $this->setObj($user_id);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "user_id";
            $errorObj->msg = "User Id is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }

        $this->user_id = $this->getObj();
        return TRUE;
    }

    /**
     * @param mixed $cart
     */
    public function setCart($cart) {

        $this->setObj($cart);

        if(!json_decode($cart,TRUE)){

            $errorObj = new ErrorObj();

            $errorObj->params = "json";
            $errorObj->msg = "invalid json";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }


        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "cart";
            $errorObj->msg = "Cart is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        $this->cart = $this->getObj();
        return TRUE;
    }

    public function getAllProductCart(){
        $productCarts = $this->all();

        $productCartList = [];

        foreach($productCarts as $productCart){
            $carts = new ProductCartFront();
            $carts->castMeFromObj($productCart);
            array_push($productCartList,$carts);
        }
        return $productCartList;

    }


}