<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: DiscountModel.php
 * User: rajib
 * Date: 1/14/16
 * Time: 3:33 PM
 */

namespace App\Model;


class DiscountModel extends BaseMallBDModel{

    protected $primaryKey = 'id';
    protected $table = 'discounts';

    /**
     * @param mixed $discount_title
     */
    public function setDiscountTitle($discount_title) {
        $this->discount_title = $discount_title;
    }

    /**
     * @param mixed $discount_amount
     */
    public function setDiscountAmount($discount_amount) {
        $this->discount_amount = $discount_amount;
    }

    /**
     * @param mixed $discount_type
     */
    public function setDiscountType($discount_type) {
        $this->discount_type = $discount_type;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status) {
        $this->status = $status;
    }

    /**
     * @param mixed $createdOn
     */
    public function setCreatedOn($createdOn) {
        $this->createdOn = $createdOn;
    }






} 