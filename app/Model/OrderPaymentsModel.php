<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: ProductModel.php
 * User: rajib
 * Date: 1/14/16
 * Time: 3:29 PM
 */

namespace App\Model;


use App\Model\DataModel\ProductDiscount;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OrderPaymentsModel extends BaseMallBDModel {

    protected $primaryKey = 'id';
    protected $table      ='order_payments';

    /**
     * @param int $order_id
     */
    public function setOrderId($order_id) {

        $this->setObj($order_id);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "order_id";
            $errorObj->msg = "Order id is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "order_id";
            $errorObj->msg = "Order id int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }

        $this->order_id = $order_id;
        return true;
    }

    /**
     * @param int $payment_method_id
     */
    public function setPaymentMethodId($payment_method_id) {

        $this->setObj($payment_method_id);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "payment_method_id";
            $errorObj->msg = "Select Payment Method";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "payment_method_id";
            $errorObj->msg = "Payment Method id int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }
        if(intval($this->getObj())<=0){
            $errorObj = new ErrorObj();

            $errorObj->params = "payment_method_id";
            $errorObj->msg = "Select Payment Method";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        $this->payment_method_id = $payment_method_id;
        return true;
    }

    /**
     * @param float $payment_total
     */
    public function setPaymentTotal($payment_total) {

        $this->setObj($payment_total);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "payment_total";
            $errorObj->msg = "Payment Total is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "payment_total";
            $errorObj->msg = "Payment Total int/float required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }
        $this->payment_total = $payment_total;
        return true;
    }

    /**
     * @param string $payment_details
     */
    public function setPaymentDetails($payment_details) {

        $this->setObj($payment_details);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "payment_details";
            $errorObj->msg = "Payment Details is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }

        $this->payment_details = $payment_details;
        return true;
    }

    /**
     * @param string $payment_status
     */
    public function setPaymentStatus($payment_status) {

        $this->setObj($payment_status);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "payment_status";
            $errorObj->msg = "Payment Status is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }

        $this->payment_status = $payment_status;
        return true;
    }

    /**
     * @param string $created_on
     */
    public function setCreatedOn($created_on) {
        $this->created_on = $created_on;
        return true;
    }

    /**
     * @param int $created_by
     */
    public function setCreatedBy($created_by) {

        $this->setObj($created_by);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "created_by";
            $errorObj->msg = "Created By is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "created_by";
            $errorObj->msg = "Created By int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }
        $this->created_by = $this->getObj();
        return true;
    }

    /**
     * @param string $updated_on
     */
    public function setUpdatedOn($updated_on) {
        $this->updated_on = $updated_on;
        return true;
    }

    /**
     * @param int $updated_by
     */
    public function setUpdatedBy($updated_by) {
        $this->updated_by = $updated_by;
        return true;
    }



    public function orderPaymentsOrder()
    {
        return $this->hasOne("App\Model\Order","id","order_id");//(modelName,foreignKey,PrimaryKey)
    }

    public function orderPaymentsPayMethod()
    {
        return $this->hasOne("App\Model\PaymentMethodsModel","id","payment_method_id");//(modelName,foreignKey,PrimaryKey)
    }

    public function setId($id){
        $this->id = $id;
    }

}