<?php

/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: ManufacturerModel.php
 * User: rajib
 * Date: 1/14/16
 * Time: 3:34 PM
 */

namespace App\Model;

use App\Model\DataModel\Manufacturer;
use DB;

class ManufacturerModel extends BaseMallBDModel {

    protected $primaryKey = 'id';
    protected $table = 'manufacturers';
    private $category_id;
    private $categoryIdList;

    /**
     * ManufacturerModel constructor.
     * @param string $primaryKey
     * @param string $table
     * @param $category_id
     */
    public function __construct() {
        parent::__construct();
        $this->category_id = 0;
        $this->categoryIdList = [];
    }

    public function setCategoryId($category_id) {
        $this->category_id = (int) ($category_id != null || $category_id != "") ? $category_id : 0;
    }

    public function setCategoryIdList($categoryIdList) {
        $this->categoryIdList = $categoryIdList;
    }

    public function getManufactureByCategoryId() {
        $manufacturers = [];
        $this->category_id = [30, 28];
        $data = DB::table('manufacturers')
                ->select('manufacturers.*')
                ->distinct()
                ->join('products', 'products.manufacture_id', '=', 'manufacturers.id')
                ->join('product_categories', 'product_categories.product_id', '=', 'products.id')
                ->where('product_categories.category_id', $this->category_id)
                ->where("manufacturers.status", "=", "Active")
                ->where("manufacturers.shop_id", "=", $this->shopId)
                ->get();

        foreach ($data as $rowData) {
            $manufacturer = new Manufacturer();
            $manufacturer->castMe($rowData);

            array_push($manufacturers, $manufacturer);
        }
        return $manufacturers;
    }

    public function getManufactureByCategoryIdList() {
        $manufacturers = [];
        $data = DB::table('manufacturers')
                ->select('manufacturers.*')
                ->distinct()
                ->join('products', 'products.manufacture_id', '=', 'manufacturers.id')
                ->join('product_categories', 'product_categories.product_id', '=', 'products.id')
                ->whereIn('product_categories.category_id', $this->categoryIdList)
                ->where("manufacturers.status", "=", "Active")
                ->where("manufacturers.shop_id", "=", $this->shopId)
                ->get();

        foreach ($data as $rowData) {
            $manufacturer = new Manufacturer();
            $manufacturer->castMe($rowData);

            array_push($manufacturers, $manufacturer);
        }
        return $manufacturers;
    }

    public function getAllManufacturer() {
        $manufacturers = ManufacturerModel::where("status", "=", "Active")->where("shop_id", "=", $this->shopId)->get();
        return $manufacturers;
    }
    
    public function getAllManufacturer_for_home($limit,$offset)
    {
        $manufacturers = ManufacturerModel::where('status', 'Active')->where("shop_id", "=", $this->shopId)->orderBy(DB::raw('RAND()'))->skip($offset)->take($limit)->get();
        //$manufacturers = ManufacturerModel::paginate($limit);
        //$manufacturers = ManufacturerModel::all();
        return $manufacturers;
    }
    
    public function getManufactureIdList() {
        $manufacturers = [];
        $data = DB::table('manufacturers')
                ->select('manufacturers.*')
                ->distinct()
                ->join('products', 'products.manufacture_id', '=', 'manufacturers.id')
                ->where("manufacturers.status", "=", "Active")
                ->where("manufacturers.shop_id", "=", $this->shopId)
                ->orderBy('manufacturers.name', 'asc')
                ->get();

        foreach ($data as $rowData) {
            $manufacturer = new Manufacturer();
            $manufacturer->castMe($rowData);

            array_push($manufacturers, $manufacturer);
        }
        return $manufacturers;
    }
    
    public function getManufacturerByKeyword($keyword) {
        $result = $this->where("status", "=", "Active")
                ->where("shop_id", "=", $this->shopId)
                ->where("name", "LIKE", '%' . $keyword . '%')
//                ->limit($this->customLimit)
//                ->offset($this->customOffset * $this->customLimit)
                ->get();

        $manufacturerList = [];

        foreach ($result as $r) {
            $manufacturer = new \stdClass();
            $manufacturer->id = $r->id;
            $manufacturer->title = $r->name;
            $manufacturer->product_title = 'Brand';
            $manufacturer->manufacturer = TRUE;
            array_push($manufacturerList, $manufacturer);
        }
        return $manufacturerList;
    }

} 

