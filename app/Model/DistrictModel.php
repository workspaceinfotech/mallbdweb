<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: DiscountModel.php
 * User: rajib
 * Date: 1/14/16
 * Time: 3:33 PM
 */

namespace App\Model;
use App\Model\DataModel\District;

class DistrictModel extends BaseMallBDModel{

    protected $primaryKey = 'id';
    protected $table = 'districts';

    public function getAllDistricts()
    {
        $zones = DistrictModel::all();

        if($zones==null)
        {
            return null;
        }
        $zoneList = [];

        foreach($zones as $z)
        {
            $zone = new District();
            $zone->castMeFromObj($z);
            array_push($zoneList,$zone);
        }
        return $zoneList;
    }





} 