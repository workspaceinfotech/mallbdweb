<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: ProductModel.php
 * User: rajib
 * Date: 1/14/16
 * Time: 3:29 PM
 */

namespace App\Model;


use App\Model\DataModel\ProductDiscount;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ProductDiscountModel extends BaseMallBDModel {

    protected $primaryKey = 'id';
    protected $table      ='product_discounts';

    public function productDiscountDiscount()
    {
        return $this->hasOne("App\Model\DiscountModel","id","discount_id");//(modelName,foreignKey,PrimaryKey)
    }


     public function getProductDiscountByStartDate($startDate)
     {
        $result = ProductDiscountModel::whereDate('start_date','=',$startDate)->get();
         return $result;
     }

     public function getProductDiscountByEndDate($endDate)
     {
         $result = ProductDiscountModel::whereDate('end_date','=',$endDate)->get();
         return $result;
     }
    public function getProductDiscountByBetweenStartAndEndDate()
    {
        $result = $this->with('productDiscountDiscount')->whereDate('start_date','=',date('Y-m-d'))->whereDate('end_date','>=',date('Y-m-d'))->get()->first();
        return $result;
    }

}