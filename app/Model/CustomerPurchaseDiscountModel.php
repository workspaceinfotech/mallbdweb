<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CustomerPurchaseDiscountModel extends BaseMallBDModel {

    protected $table = 'customer_purchase_discount';
    protected $primaryKey = 'id';
    public $timestamps = FALSE;
    protected $fillable = array('discount_base', 'discount_base_limit', 'discount_type', 'discount_amount', 'created_by', 'updated_by');

    public function getPresentDiscount($totalCompletedOrders) {
        $discountData = CustomerPurchaseDiscountModel::where('discount_base', 'Number_Of_Order')
                ->where('discount_base_limit', '<=', $totalCompletedOrders)
                ->orderBy('discount_base_limit', 'desc')
                ->get();
        return $discountData;
    }

    public function getNextOrderDiscountOffer($totalCompletedOrders) {
        $totalCompletedOrders++;    //increased for getting the next order 
        $discountData = CustomerPurchaseDiscountModel::where('discount_base', 'Number_Of_Order')
                ->where('discount_base_limit', $totalCompletedOrders)
                ->get();
        return $discountData;
    }

    public function getPresentDiscountForCheckout($totalCompletedOrders) {
        $totalCompletedOrders++;
        $discountData = CustomerPurchaseDiscountModel::where('discount_base', 'Number_Of_Order')
                ->where('discount_base_limit', '<=', $totalCompletedOrders)
                ->orderBy('discount_base_limit', 'desc')
                ->get();
        return $discountData;
    }

}
