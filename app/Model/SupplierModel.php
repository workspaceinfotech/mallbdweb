<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: SupplierModel.php
 * User: rajib
 * Date: 1/14/16
 * Time: 3:42 PM
 */

namespace App\Model;


class SupplierModel extends BaseMallBDModel{
    protected $primaryKey = 'id';
    protected $table = 'suppliers';

} 