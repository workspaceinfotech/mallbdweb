<?php
/**
 * Created by PhpStorm.
 * Project: MallBDFront
 * File: CustomerWishListModel.php
 * User: rajib
 * Date: 2/4/16
 * Time: 3:17 PM
 */

namespace App\Model;


use App\Model\DataModel\CustomerWishList;
use App\Http\Controllers\BaseMallBDController;
use Illuminate\Support\Facades\Request;

class CustomerWishListModel extends BaseMallBDModel{

    protected $primaryKey = 'id';
    protected $table = 'customer_wishlist';
    /**
     * @param mixed $customer_id
     */
    public function setCustomerId($customer_id) {

        $this->setObj($customer_id);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "customer_id";
            $errorObj->msg = "Customer Id is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "customer_id";
            $errorObj->msg = "Customer Id int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        $this->customer_id = $customer_id;
        return TRUE;
    }

    /**
     * @param mixed $product_id
     */
    public function setProductId($product_id) {

        $this->setObj($product_id);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "product_id";
            $errorObj->msg = "Product Id is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "product_id";
            $errorObj->msg = "Product Id int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        $this->product_id = $product_id;
        return TRUE;
    }



    public function products(){
        return $this->hasMany("App\Model\ProductModel","id","product_id");
    }

    public function addProductInWishList($productId)
    {

        $customerWishListModel = new CustomerWishListModel();
        $customerWishListModel->customer_id = $this->appCredential->user->id;
        $customerWishListModel->product_id = $productId;

        if($customerWishListModel->save())
        {
            $customerWishListObj = new CustomerWishList();
            $customerWishListObj->castMeFromObj($customerWishListModel);
            return $customerWishListObj;
        }
        else
            return null;
    }

    public function isDistinct($productId,$customerId)
    {
        $productCount = $this->where("product_id","=",$productId)
                             ->where("customer_id","=",$customerId)
                                                     ->count();
        if($productCount>=1)
        {
            return false;
        }
        else{
            return TRUE;
        }

    }

    public function getWishListedProductsById($customer_id)
    {

        if ($this->customLimit < 0) {
            $customerWithProducts = CustomerWishListModel::with("products")->where("customer_id", "=", $customer_id)
                    ->get();
        } else {
            $customerWithProducts = CustomerWishListModel::with("products")->where("customer_id", "=", $customer_id)
                    ->limit($this->customLimit)
                    ->offset($this->customLimit * $this->customOffset)
                    ->get();
        }
        $customerWithProductsList = [];
        foreach($customerWithProducts as $products) {
            $customerWishListObj = new CustomerWishList();
          //  $customerWishListObj->setCurrency(self::$currency);
            $customerWishListObj->castMeFromObj($products);
            array_push($customerWithProductsList,$customerWishListObj);
        }
        return $customerWithProductsList;
    }
    
    public function getCountWishListedProductsById($customer_id)
    {

        $customerWithProducts = CustomerWishListModel::with("products")->where("customer_id","=",$customer_id)
                                                     ->get();
        $customerWithProductsList = [];
        foreach($customerWithProducts as $products) {
            $customerWishListObj = new CustomerWishList();
          //  $customerWishListObj->setCurrency(self::$currency);
            $customerWishListObj->castMeFromObj($products);
            array_push($customerWithProductsList,$customerWishListObj);
        }
        return count($customerWithProductsList);
    }
    public function wishListProductCount(){
        return $this->where("customer_id","=",$this->customer_id)->get()->count();
    }

    public function wishListProduct(){
        return $this->hasOne("App\Model\ProductModel","id","product_id");
    }

    public function getWishListProductArray($customerId)
    {
        $productList =[];

        if($customerId<=0)
        {
            return $productList;
        }
        $wishListProductArray = CustomerWishListModel::select("product_id")->where("customer_id","=",$customerId)->get();
        foreach($wishListProductArray as $wishListedProduct)
        {
            array_push($productList,$wishListedProduct->product_id);
        }
        return $productList;
    }

} 