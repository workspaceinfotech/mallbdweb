<?php

/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: CategoryModel.php
 * User: rajib
 * Date: 1/13/16
 * Time: 6:48 PM
 */

namespace App\Model;

use App\Model\DataModel\Category;
use Illuminate\Support\Facades\DB;

class CategoryModel extends BaseMallBDModel {

    protected $primaryKey = 'id';
    protected $table = 'categories';

    public function shop() {
        return $this->hasOne("App\Model\ShopModel", "id", "shop_id");
    }

    public function children() {
        return $this->hasMany("App\Model\CategoryModel", "parent", "id")->where('status', 'Active');
    }

    public function parents() {
        return $this->hasOne("App\Model\CategoryModel", "id", "parent");
    }

    /**
     * @param $shop_id
     * @return array|null
     * Task: All parent categories. just parent categories with its child
     */
    public function getAllParentsCategory($shop_id) {
        $categories = CategoryModel::whereNull("parent")->where("shop_id", "=", $shop_id)
                        ->where("status", "=", "Active")->get();

        if ($categories == null) {
            return null;
        }
        $categoryList = [];
        foreach ($categories as $c) {
            $category = new Category();
            $category->castMe($c);
            array_push($categoryList, $category);
        }
        return $categoryList;
    }

    /**
     * @param $shop_id
     * @param $parent_id
     * @return array|null
     * get children's of specific parent. only children's with its children's...
     */
    public function getAllChildsCategory($shop_id, $parent_id) {
        $categories = CategoryModel::where("parent", "=", $parent_id)->where("shop_id", "=", $shop_id)
                        ->where("status", "=", "Active")->get();

        if ($categories == null) {
            return null;
        }
        $categoryList = [];
        foreach ($categories as $c) {
            $category = new Category();
            $category->castMe($c);
            array_push($categoryList, $category);
        }
        return $categoryList;
    }

    /**
     * @param $shop_id
     * @param $parent_id
     * @return array|null
     * return ID's of single parent category with its all children's recursively
     */
    public function allCategoryWithParentsAndItsChildrens($shop_id, $parent_id) {
        $categories = CategoryModel::where("shop_id", "=", $shop_id)
                ->where("status", "=", "Active")
                ->where("id", "=", $parent_id)
                ->get();

        if ($categories == null) {
            return null;
        }
        $categoryList = [];
        foreach ($categories as $c) {
            $category = new Category();
            $category->castMe($c);
            array_push($categoryList, $category);
        }
        return $categoryList;
    }

    public function getAllCategory($shop_id) {
        $categories = CategoryModel::where("shop_id", "=", $shop_id)->where("status", "=", "Active")->get()->all();
        // return $categories;
        if ($categories == null) {
            return null;
        }
        $categoryList = [];
        foreach ($categories as $c) {
            $category = new Category();
            $category->castMe($c);
            array_push($categoryList, $category);
        }
        return $categoryList;
    }

    public function getAllParentList($childId) {
        $parentList = [];
        $result = DB::table('categories as a')
                        ->select('b.*')
                        ->join('categories AS b', 'a.parent', '=', 'b.id')
                        ->where('a.id', $childId)->get();



        if ($result != null) {
            foreach ($result as $res) {
                $category = new Category();
                $category->castMe($res);
                array_push($parentList, $category);
                if ($res != null) {
                    $result = DB::table('categories as a')
                                    ->select('b.*')
                                    ->join('categories AS b', 'a.parent', '=', 'b.id')
                                    ->where('a.id', $res->id)->first();

                    $category = new Category();
                    $category->castMe($result);
                    array_push($parentList, $category);
                    return array_reverse($parentList);
                }
            }
        }
    }

    function getAllParentWithChildList($childId) {
        $parentList = [];
        $parents = $this->getAllParentList($childId);
        if ($parents != null) {
            foreach ($parents as $parent) {
                array_push($parentList, $parent);
            }
        }


        $result = DB::table('categories')
                        ->select('categories.*')
                        ->where('id', $childId)->where("status", "=", "Active")->get();
        if ($result != NULL) {
            foreach ($result as $res) {
                $category = new Category();
                $category->castMe($res);
                array_push($parentList, $category);
            }
            return $parentList;
        }
        return $parentList;
    }

    function getAllCategoryByProductTitle($keyword) {
        $result = CategoryModel::
                join('product_categories', 'product_categories.category_id', '=', 'categories.id')
                ->join('products', 'products.id', '=', 'product_categories.product_id')
                ->join('product_images', 'products.id', '=', 'product_images.product_id')
                ->where("products.product_title", "LIKE", '%' . $keyword . '%')
                ->where("categories.shop_id", "=", $this->shopId)
                ->get();
        return $result;
    }
    function getAllCategoryByProductTitleMobile($keyword) {
        $result = CategoryModel::
                join('product_categories', 'product_categories.category_id', '=', 'categories.id')
                ->join('products', 'products.id', '=', 'product_categories.product_id')
                ->join('product_images', 'products.id', '=', 'product_images.product_id')
                ->where("products.product_title", "LIKE", '%' . $keyword . '%')
                ->where("categories.shop_id", "=", $this->shopId)
                ->select('products.id', 'products.product_title','product_images.image_name')
                ->get();
        return $result;
    }
    function getAllCategoryByProductId($id) {
        $result = CategoryModel::
                join('product_categories', 'product_categories.category_id', '=', 'categories.id')
                ->join('products', 'products.id', '=', 'product_categories.product_id')
                ->where("products.id", "=",  $id )
                ->where("categories.shop_id", "=", $this->shopId)
                ->get();
        
        return $result;
    }
    
    public function getCategoryByKeyword($keyword) {
        $result = $this->where("status", "=", "Active")
                ->where("shop_id", "=", $this->shopId)
                ->where("title", "LIKE", '%' . $keyword . '%')
//                ->limit($this->customLimit)
//                ->offset($this->customOffset * $this->customLimit)
                ->get();

        $categoryList = [];

        foreach ($result as $r) {
            $category = new \stdClass();
            $category->id = $r->id;
            $category->encoded_title = urlencode($r->title);
            $category->title = $r->title;
            $category->product_title = strlen($r->meta_description) > 20 ? substr($r->meta_description, 0, 19) . '...' : $r->meta_description;
            $category->category = TRUE;
            array_push($categoryList, $category);
        }
        return $categoryList;
    }

}
