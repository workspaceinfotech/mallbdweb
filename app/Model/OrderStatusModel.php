<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: ProductModel.php
 * User: rajib
 * Date: 1/14/16
 * Time: 3:29 PM
 */

namespace App\Model;


use App\Model\DataModel\OrderStatus;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OrderStatusModel extends BaseMallBDModel {

    protected $primaryKey = 'id';
    protected $table      ='order_status';

    /**
     * @param mixed $order_id
     */
    public function setOrderId($order_id) {

        $this->setObj($order_id);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "order_id";
            $errorObj->msg = "Order id is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "order_id";
            $errorObj->msg = "Order id int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }
        $this->order_id = $this->getObj();
        return true;
    }

    /**
     * @param mixed $status
     */
    public function setStatusId($status) {

        $this->setObj($status);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "status_id";
            $errorObj->msg = "Status id is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "status_id";
            $errorObj->msg = "Status id int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }
        $this->status_id = $this->getObj();
        return true;
    }

    /**
     * @param mixed $comments
     */
    public function setComments($comments) {

        $this->setObj($comments);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "comments";
            $errorObj->msg = "Comments is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        $this->comments = $this->getObj();
        return true;
    }

    /**
     * @param mixed $created_by
     */
    public function setCreatedBy($created_by) {

        $this->setObj($created_by);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "created_by";
            $errorObj->msg = "Created By is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "created_by";
            $errorObj->msg = "Created By int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }
        $this->created_by = $this->getObj();
        return true;
    }

    /**
     * @param mixed $created_on
     */
    public function setCreatedOn($created_on) {
        $this->created_on = $created_on;
        return true;
    }

    /**
     * @param mixed $updated_on
     */
    public function setUpdatedOn($updated_on) {
        $this->updated_on = $updated_on;
        return true;
    }

    /**
     * @param mixed $updated_by
     */
    public function setUpdatedBy($updated_by) {
        $this->updated_by = $updated_by;
        return true;
    }





    public function orderStatusList()
    {
        return $this->hasOne("App\Model\OrderStatusListModel","id","status_id");//(modelName,foreignKey,PrimaryKey)
    }


    public function getAll(){
        $result = OrderStatusModel::with("orderStatusList")->get();

        $orderStatusList=[];
        foreach($result as $r)
        {
            $order = new OrderStatus();
            $order->castMeFromObj($r);
            array_push($orderStatusList,$order);
        }

        return $orderStatusList;
    }
}