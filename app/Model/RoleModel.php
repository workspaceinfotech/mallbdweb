<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: RoleModel.php
 * User: rajib
 * Date: 1/12/16
 * Time: 1:33 PM
 */

namespace App\Model;


class RoleModel extends BaseMallBDModel{

    protected $primaryKey = 'id';
    protected $table = 'roles';

} 