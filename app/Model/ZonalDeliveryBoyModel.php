<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: ProductModel.php
 * User: rafsan
 * Date: 1/14/16
 * Time: 3:29 PM
 */

namespace App\Model;
use App\Model\DataModel\ZonalDeliveryBoy;

class ZonalDeliveryBoyModel extends BaseMallBDModel {

    protected $primaryKey = 'id';
    protected $table      = 'zonal_delivery_boys';

    /**
     * @param mixed $zone_id
     */
    public function setZoneId($zone_id) {

        $this->setObj($zone_id);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "zone_id";
            $errorObj->msg = "Select Zone";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "zone_id";
            $errorObj->msg = "Zone Id int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(intval($this->getObj()) <=0){
            $errorObj = new ErrorObj();

            $errorObj->params = "zone_id";
            $errorObj->msg = "Select Zone ";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        $this->zone_id = $this->getObj();
        return TRUE;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id) {

        $this->setObj($user_id);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "user_id";
            $errorObj->msg = "User Id is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "user_id";
            $errorObj->msg = "User Id int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        $this->user_id = $user_id;
        return true;
    }

    /**
     * @param mixed $created_on
     */
    public function setCreatedOn($created_on) {
        $this->created_on = $created_on;
    }


    public function getUserByZoneId($zoneId)
    {
        $result = $this->where('zone_id','=',$zoneId)->get();
        return $result;
    }
    public function getFirstDeliveryIdByZoneId()
    {
        $id = 0;
        $zoneModel = new ZoneModel();
        if($zoneModel->setId($this->zone_id) && $zoneModel->isZoneExist()){
            $result = $this->where('zone_id','=',$this->zone_id)->limit(1)->get()->first();
            if($result!=null){
                $id = @$result->id;
            }
            if($id == null || $id <= 0){
                $this->fnError = true;
                $this->fnErrorMsg = "No Delivery boy found on this zone";
            }

        }else{
            $this->fnError = true;
            $this->fnErrorMsg = "Zone does not exist ".$zoneModel->id;
        }



        return $id;
    }

}

