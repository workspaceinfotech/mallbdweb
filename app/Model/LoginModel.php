<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: LoginModel.php
 * User: rajib
 * Date: 1/12/16
 * Time: 12:40 PM
 */

namespace App\Model;

use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Auth\Authenticatable;
use Illuminate\Hashing\BcryptHasher;
use Illuminate\Support\Facades\Hash;

class LoginModel extends BaseMallBDModel implements AuthenticatableContract, CanResetPasswordContract{

    use  Authenticatable,CanResetPassword;

    protected $primaryKey = 'id';
    protected $table = 'user_login';

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id) {

        $this->setObj($user_id);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "user_id";
            $errorObj->msg = "User ID is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "user_id";
            $errorObj->msg = "User ID int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        $this->user_id = $this->getObj();
        return TRUE;
    }



    public function setEmail($email) {

        $this->setObj($email);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();
            $errorObj->params = "email";
            $errorObj->msg = "Email is empty";
            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        $this->email = $this->getObj();
        return TRUE;
    }

    public function setPassword($password) {

        $this->setObj($password);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();
            $errorObj->params = "password";
            $errorObj->msg = "Password is empty";
            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        $this->password = bcrypt($password);
        return TRUE;
    }

    public function setAccesstoken($access_token) {

        $this->setObj($access_token);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();
            $errorObj->params = "access_token";
            $errorObj->msg = "Access Token is empty";
            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        $this->access_token = md5($access_token);
        return TRUE;
    }

    public function setCreatedOn($created_on) {
        $this->created_on = $created_on;
    }



    public function user(){
        return $this->hasOne("App\Model\UserModel","id","user_id");
    }
    public function isEmailExist(){
        $tempEmail = $this->email;
        $count =  $this->with('user')->whereHas('user',function($q)use ($tempEmail){
                $q->where('email','=',$tempEmail)->where('role_id','=',7)->where('status','=','Active');
        })->where('email','=',$tempEmail)->limit(1)->get()->count();
        return ($count>0)?true:false;
    }


    public function matchPassword($pass, $customerID) {

        $user = $this->where('user_id', '=', $customerID)->first();


        if (Hash::check($pass, $user->password))
        {
            return true;
        }else{
            return false;
        }

    }

} 