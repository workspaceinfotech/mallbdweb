<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: ProductPictureModel.php
 * User: rajib
 * Date: 1/14/16
 * Time: 3:35 PM
 */

namespace App\Model;


class ProductPictureModel extends BaseMallBDModel{

    protected $primaryKey = 'id';
    protected $table = 'product_images';

} 