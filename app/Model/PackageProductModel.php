<?php
/**
 * Project  : mallbdweb
 * File     : PackageProductModel.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 4/6/16 - 12:03 PM
 */

namespace App\Model;


use App\Model\DataModel\PackageProduct;

class PackageProductModel extends BaseMallBDModel {

    protected $primaryKey = 'id';
    protected $table      = 'package_products';

    /**
     * @param mixed $package_id
     */
    public function setPackageId($package_id) {

        $this->setObj($package_id);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "package_id";
            $errorObj->msg = "Package Id is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "package_id";
            $errorObj->msg = "Package Id int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }
        $this->package_id = $this->getObj();
        return true;

    }

    /**
     * @param mixed $product_id
     */
    public function setProductId($product_id) {

        $this->setObj($product_id);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "product_id";
            $errorObj->msg = "Product Id is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "product_id";
            $errorObj->msg = "Product Id int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }
        $this->product_id = $this->getObj();
        return true;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity) {

        $this->setObj($quantity);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "quantity";
            $errorObj->msg = "Quantity is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "quantity";
            $errorObj->msg = "Quantity int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }
        $this->quantity = $this->getObj();
        return true;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price) {

        $this->setObj($price);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "price";
            $errorObj->msg = "Price is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "price";
            $errorObj->msg = "Price int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }
        $this->price = $this->getObj();
        return true;
    }

    /**
     * @param mixed $created_on
     */
    public function setCreatedOn($created_on) {
        $this->created_on = $created_on;
    }


    public function product()
    {
        return $this->hasOne("App\Model\ProductModel","id","product_id");
    }


    public function getAll()
    {
        $result = $this->all();

        $packageList = [];

        foreach($result as $r)
        {
            $packages = new PackageProduct();
            $packages->castMe($r);
            array_push($packageList,$packages);
        }
        return $packageList;
    }

}
