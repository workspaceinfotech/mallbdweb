<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: TaxModel.php
 * User: rajib
 * Date: 1/14/16
 * Time: 3:43 PM
 */

namespace App\Model;


class TaxModel extends BaseMallBDModel{

    protected $primaryKey = 'id';
    protected $table = 'taxes';

} 