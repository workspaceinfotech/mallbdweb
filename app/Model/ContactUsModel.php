<?php

namespace App\Model;

class ContactUsModel extends BaseMallBDModel {
    protected $primaryKey = 'id';
    protected $table = 'contact_us';

    /**
     * @param mixed $shop_id
     */
    public function setShopId($shop_id) {
        $this->shop_id = $shop_id;
    }

    /**
     * @param mixed $name
     */
    public function setName($name) {

        $this->setObj($name);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "name";
            $errorObj->msg = "Name is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }

        $this->name = $this->getObj();
        return TRUE;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email) {

        $this->setObj($email);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "email";
            $errorObj->msg = "Email is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }

        $this->email = $this->getObj();
        return TRUE;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject($subject) {
        $this->setObj($subject);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "subject";
            $errorObj->msg = "Subject is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }

        $this->subject = $this->getObj();
        return TRUE;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message) {

        $this->setObj($message);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "message";
            $errorObj->msg = "Message is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }

        $this->message = $this->getObj();
        return TRUE;
    }

    /**
     * @param mixed $is_read
     */
    public function setIsRead($is_read) {
        $this->is_read = $is_read;
    }


}
