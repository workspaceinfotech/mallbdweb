<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: UserDetailsModel.php
 * User: rajib
 * Date: 1/13/16
 * Time: 1:46 PM
 */

namespace App\Model;


class UserDetailsModel extends BaseMallBDModel{

    protected $primaryKey = 'id';
    protected $table = 'user_details';

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id) {

        $this->setObj($user_id);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "user_id";
            $errorObj->msg = "User ID is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "user_id";
            $errorObj->msg = "User Id int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        $this->user_id = $user_id;
        return TRUE;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address) {

        $this->setObj($address);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "address";
            $errorObj->msg = "*Address is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }

        $this->address = $address;
        return TRUE;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country) {

        $this->setObj($country);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "country";
            $errorObj->msg = "Country is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }

        $this->country = $country;
        return TRUE;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city) {

        $this->setObj($city);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "city";
            $errorObj->msg = "City is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        $this->city = $city;
        return TRUE;
    }

    /**
     * @param mixed $zipcode
     */
    public function setZipcode($zipcode) {

        $this->setObj($zipcode);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "zipcode";
            $errorObj->msg = "Zipcode is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        $this->zipcode = $zipcode;
        return TRUE;
    }

    /**
     * @param mixed $shipping_address
     */
    public function setShippingAddress($shipping_address) {

        $this->setObj($shipping_address);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();
            $errorObj->params = "shipping_address";
            $errorObj->msg = "Shipping Address is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        $this->shipping_address = $shipping_address;
        return TRUE;
    }

    /**
     * @param mixed $shipping_country
     */
    public function setShippingCountry($shipping_country) {

        $this->setObj($shipping_country);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "shipping_country";
            $errorObj->msg = "Shipping Country is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        $this->shipping_country = $shipping_country;
        return TRUE;
    }

    /**
     * @param mixed $shipping_city
     */
    public function setShippingCity($shipping_city) {

        $this->setObj($shipping_city);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "shipping_city";
            $errorObj->msg = "Shipping City is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        $this->shipping_city = $shipping_city;
        return TRUE;
    }

    /**
     * @param mixed $shipping_zipcode
     */
    public function setShippingZipcode($shipping_zipcode) {

        $this->setObj($shipping_zipcode);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "shipping_zipcode";
            $errorObj->msg = "Shipping Zipcpde is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        $this->shipping_zipcode = $shipping_zipcode;
        return TRUE;
    }

    /**
     * @param mixed $created_on
     */
    public function setCreatedOn($created_on) {
        $this->created_on = $created_on;

    }


} 