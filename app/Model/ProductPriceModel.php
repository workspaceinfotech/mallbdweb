<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: ProductPriceModel.php
 * User: rajib
 * Date: 1/14/16
 * Time: 3:36 PM
 */

namespace App\Model;


class ProductPriceModel extends BaseMallBDModel{

    protected $primaryKey = 'id';
    protected $table = 'product_prices';

    /*public function prices(){
        return $this->belongsTo("App\Model\ProductModel");
    }*/

    public function priceTax(){
        return $this->hasOne("App\Model\TaxModel","id","tax_id");
    }

} 