<?php
/**
 * Project  : mallbdweb
 * File     : OrderAttribute.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 2/24/16 - 12:35 PM
 */

namespace App\Model;
use App\Model\DataModel\OrderAttribute;

class OrderAttributeModel extends BaseMallBDModel{
    protected $primaryKey = 'id';
    protected $table = 'order_attributes';

    /**
     * @param mixed $order_id
     */
    public function setOrderId($order_id) {

        $this->setObj($order_id);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "oder_id";
            $errorObj->msg = "Order id is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "oder_id";
            $errorObj->msg = "Order id int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }
        $this->order_id = $order_id;
        return true;
    }

    /**
     * @param mixed $product_id
     */
    public function setProductId($product_id) {

        $this->setObj($product_id);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "product_id";
            $errorObj->msg = "Product id is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "product_id";
            $errorObj->msg = "Product id int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }
        $this->product_id = $product_id;
        return true;
    }

    /**
     * @param mixed $combination_key
     */
    public function setCombinationKey($combination_key) {

        $this->setObj($combination_key);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "combination_key";
            $errorObj->msg = "Combination Key is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "combination_key";
            $errorObj->msg = "Combination Key int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }
        $this->combination_key = $combination_key;
        return true;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity) {

        $this->setObj($quantity);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "quantity";
            $errorObj->msg = "Quantity is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "quantity";
            $errorObj->msg = "Quantity int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }
        $this->quantity = $quantity;
        return true;
    }

    /**
     * @param mixed $created_on
     */
    public function setCreatedOn($created_on) {

        $this->setObj($created_on);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "created_on";
            $errorObj->msg = "Created On is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "created_on";
            $errorObj->msg = "Created On int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }
        $this->created_on = $created_on;

        return true;
    }

    public function getByOrderIdAndProductId($orderId,$productId){

        return $this->where('order_id','=',$orderId)->where('product_id','=',$productId)->get();
    }


}