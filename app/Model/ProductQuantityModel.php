<?php
/**
 * Created by PhpStorm.
 * Project: MallBDFront
 * File: ProductQuantityModel.php
 * User: rajib
 * Date: 1/29/16
 * Time: 5:51 PM
 */

namespace App\Model;


class ProductQuantityModel extends BaseMallBDModel{
    protected $primaryKey = 'id';
    protected $table = 'product_quantity';

    public function attributesCombinations(){
        return $this->hasMany("App\Model\ProductAttributesCombinationModel","combination_key","combination_key");
    }
} 