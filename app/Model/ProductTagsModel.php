<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: ProductModel.php
 * User: rajib
 * Date: 1/14/16
 * Time: 3:29 PM
 */

namespace App\Model;


use App\Model\DataModel\Review;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ProductTagsModel extends BaseMallBDModel {

    protected $primaryKey = 'id';
    protected $table      ='product_tags';

    public function tagProduct()
    {
        return $this->hasOne("App\Model\ProductModel","id","product_id");//(modelName,foreignKey,PrimaryKey)
    }

    public function productTag()
    {
        return $this->hasOne("App\Model\TagModel","id","tag_id");//(modelName,foreignKey,PrimaryKey)
    }

    public function setId($id){
        $this->id = $id;
    }

}