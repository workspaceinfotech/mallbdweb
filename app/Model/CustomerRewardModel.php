<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: ProductModel.php
 * User: rajib
 * Date: 1/14/16
 * Time: 3:29 PM
 */

namespace App\Model;


use App\Model\DataModel\CustomerReward;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CustomerRewardModel extends BaseMallBDModel {

    protected $primaryKey = 'id';
    protected $table      ='customer_reward';

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id) {

        $this->setObj($user_id);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "user_id";
            $errorObj->msg = "User id is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "user_id";
            $errorObj->msg = "User id int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }
        $this->user_id = $this->getObj();
        return TRUE;
    }

    /**
     * @param mixed $order_id
     */
    public function setOrderId($order_id) {

        $this->setObj($order_id);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "order_id";
            $errorObj->msg = "Order id is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "order_id";
            $errorObj->msg = "Order id int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;

        }
        $this->order_id = $this->getObj();
        return TRUE;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description) {

        $this->setObj($description);
        $this->basicFilter();
//        if(!$this->basicValidation())
//        {
//            $errorObj = new ErrorObj();
//
//            $errorObj->params = "description";
//            $errorObj->msg = "Description is empty";
//
//            array_push($this->errorManager->errorObj,$errorObj);
//            return false;
//        }
        $this->description = $this->getObj();
        return TRUE;
    }

    /**
     * @param mixed $points
     */
    public function setPoints($points) {

        $this->setObj($points);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "points";
            $errorObj->msg = "Points id is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        $this->points = $this->getObj();
        return true;
    }

    /**
     * @param mixed $created_on
     */
    public function setCreatedOn($created_on) {
        $this->created_on = $created_on;
    }




    public function rewardCustomer()
    {
        return $this->hasOne("App\Model\UserModel","id","customer_id");//(modelName,foreignKey,PrimaryKey)
    }

    public function rewardOrder()
    {
        return $this->hasOne("App\Model\OrderModel","id","order_id");//(modelName,foreignKey,PrimaryKey)
    }

    public function setId($id){
        $this->id = $id;
    }

}