<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: ProductModel.php
 * User: rajib
 * Date: 1/14/16
 * Time: 3:29 PM
 */

namespace App\Model;


use App\Model\DataModel\PaymentMethods;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PaymentMethodsModel extends BaseMallBDModel {

    protected $primaryKey = 'id';
    protected $table      ='payment_methods';

    public function getAllPaymentMethods()
    {
        $methods = PaymentMethodsModel::limit(4)->get();

        if($methods==null)
        {
            return null;
        }
        $methodList = [];

        foreach($methods as $m)
        {
            $method = new PaymentMethods();
            $method->castMeFromObj($m);
            array_push($methodList,$method);
        }
        return $methodList;
    }

}