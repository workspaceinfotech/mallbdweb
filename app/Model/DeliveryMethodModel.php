<?php
/**
 * Project  : mallbdweb
 * File     : ZonalOrderModel.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 3/2/16 - 5:38 PM
 */

namespace App\Model;
use App\Model\DataModel\DeliveryMethod;
use App\Model\DataModel\Zone;

class DeliveryMethodModel extends BaseMallBDModel{

    protected $primaryKey = 'id';
    protected $table = 'delivery_methods';

    public function getAllDeliveryMethods()
    {
        $deliveryMethods = DeliveryMethodModel::all();

        if($deliveryMethods==null)
        {
            return null;
        }
        $deliveryMethodList = [];

        foreach($deliveryMethods as $m)
        {
            $deliveryMethod = new DeliveryMethod();
            $deliveryMethod->castMeFromObj($m);
            array_push($deliveryMethodList,$deliveryMethod);
        }
        return $deliveryMethodList;
    }

} 