<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: UserModel.php
 * User: rajib
 * Date: 1/12/16
 * Time: 12:40 PM
 */

namespace App\Model;


use App\Model\DataModel\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class UserModel extends BaseMallBDModel{

    protected $primaryKey = 'id';
    protected $table = 'users';
    protected $fillable = array('firstname', 'lastname', 'email', 'role_id', 'phone', 'status', 'created_by', 'updated_by');
    

    public function userDetails(){
        return $this->hasOne("App\Model\UserDetailsModel","user_id","id");
    }


    public function setCode($code) {

        $this->setObj($code);

        $this->code = $this->getObj();
        return true;
    }


    /**
     * @param mixed $firstname
     */
    public function setFirstname($firstname) {

        $this->setObj($firstname);

        if(!$this->basicValidation())
        {
             $errorObj = new ErrorObj();
            $errorObj->params = "firstname";
            $errorObj->msg = "*First Name is is empty";
            array_push($this->errorManager->errorObj,$errorObj);
             return false;
        }

        $this->firstname = $this->getObj();
        return true;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname) {

        $this->setObj($lastname);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();
            $errorObj->params = "lastname";
            $errorObj->msg = "*Last Name is empty";
            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        $this->lastname = $this->getObj();
        return TRUE;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email,$constrains=true) {

        $this->setObj($email);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();
            $errorObj->params = "email";
            $errorObj->msg = "*Email is empty";
            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
         
         if (!preg_match($this->email_validation_pattern, $email)) {
             $errorObj = new ErrorObj();
            $errorObj->params = "email";
            $errorObj->msg = "*Email is invalid";
            array_push($this->errorManager->errorObj,$errorObj);
            return false;  
         }
        if($constrains){
            if($this->isDuplicateEmail($email)){
                $errorObj = new ErrorObj();
                $errorObj->params = "email";
                $errorObj->msg = "*Duplicate Email found !";
                array_push($this->errorManager->errorObj,$errorObj);
                return false;
            }
        }


        $this->email = $this->getObj();
        return TRUE;
    }
    public function setEmailRegistration($email,$constrains=true) {

        $this->setObj($email);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();
            $errorObj->params = "email";
            $errorObj->msg = "*Email is empty";
            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
         
         if (!preg_match($this->email_validation_pattern, $email)) {
             $errorObj = new ErrorObj();
            $errorObj->params = "email";
            $errorObj->msg = "*Email is invalid";
            array_push($this->errorManager->errorObj,$errorObj);
            return false;  
         }
        if($constrains){
            if($this->isDuplicateEmailRegistration($email)){
                $errorObj = new ErrorObj();
                $errorObj->params = "email";
                $errorObj->msg = "*Duplicate Email found !";
                array_push($this->errorManager->errorObj,$errorObj);
                return false;
            }
        }


        $this->email = $this->getObj();
        return TRUE;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone) {

        $this->setObj($phone);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();
            $errorObj->params = "phone";
            $errorObj->msg = "*Phone is empty";
            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }

        $this->phone = $this->getObj();
        return true;
    }

    /**
     * @param mixed $role_id
     */
    public function setRoleId($role_id) {

        $this->setObj($role_id);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "role_id";
            $errorObj->msg = "Role ID is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "role_id";
            $errorObj->msg = "Role ID int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        $this->role_id = $this->getObj();
        return TRUE;
    }


    /**
     * @param mixed $status
     */
    public function setStatus($status) {

        $this->setObj($status);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();
            $errorObj->params = "status";
            $errorObj->msg = "Status is empty";
            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }

        $this->status = $this->getObj();
        return TRUE;
    }

    public function isDuplicateEmail($email)
    {
        $count = UserModel::where('email', '=', $email)->where('role_id','=',7)->count();
        if($count>=1)
        {
            return TRUE;
        }
        else{
            return FALSE;
        }
    }
    public function isDuplicateEmailRegistration($email)
    {
        $count = UserModel::where('email', '=', $email)->where('role_id','=',7)->where('status', '!=', 'Deleted')->count();
        if($count>=1)
        {
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    /**
     * @param mixed $created_on
     */
    public function setCreatedOn($created_on) {
        $this->created_on = $this->getObj();
    }


    public function role(){
        return $this->belongsTo("App\Model\RoleModel","role_id","id");
    }




    public function getImageType($str) {
        // $str should start with 'data:' (= 5 characters long!)
        return substr($str, 5, strpos($str, ';')-5);
    }

    public function savePicture($image)
    {
        $type = $this->getImageType($image);
        $pic = $image;

        if($type=="image/png"){
            $pic = str_replace('data:image/png;base64,', '', $pic);
        }
        else
            $pic = str_replace('data:image/jpeg;base64,', '', $pic);

        $pic = str_replace(' ', '+', $pic);
        $pic = base64_decode($pic);

        define('UPLOAD_DIR', '');
     //   $basePath ="/var/www/vip12/sites/vip2859267/httpd/htdocs/public/images/"; // Server

        $basePath = "/var/www/html/themallbd/Frontend/images/"; // Local
        //$file = 'assets/image/'.uniqid().'.png';
        if($type=="image/png")
            $fileName =UPLOAD_DIR.uniqid().'.png';
        else
            $fileName =UPLOAD_DIR.uniqid().'.jpg';

        $file = $basePath.$fileName;

        $success = file_put_contents($file,$pic);

        if($success)
            return "images/".$fileName;
        else
            return "null";
       //
    }
    function getByEmail(){
        $data = $this->with('userDetails')->where('email','=',$this->email)->where('role_id','=',7)->limit(1)->first();
        $user = new User();
        $user->castMe($data);
        return $user;
    }
    function isEmailExist(){
        $data = $this->with('userDetails')->where('email','=',$this->email)->limit(1)->get()->first();
        if($data==null){
            return false;
        }else{
            $this->fnObj = $data;
            return true;
        }

    }
    function isAdminEmail(){
        $data = $this->with('userDetails')->where('email','=',$this->email)->where('role_id','=',1)->limit(1)->get()->first();
        if($data==null){
            return false;
        }else{
            $this->fnObj = $data;
            return true;
        }

    }
    function isEmailExistByOtherRole($email){
        $data = $this->with('userDetails')->where('email','=',$email)->where('role_id','!=',7)->where('role_id','!=',8)->limit(1)->first();
        if($data==null){
            return false;
        }else{ 
            $this->fnObj = $data;
            return true;
        }

    }
    function isEmailExistByCustomerRole($email){
        $data = $this->with('userDetails')->where('email','=',$email)->where('role_id','=',7)->where('status', '!=', 'Deleted')->limit(1)->first();
        if($data==null){
            return false;
        }else{ 
            $this->fnObj = $data;
            return true;
        }

    }
    function isPhoneExistByCustomerRole($phone){
        $data = $this->with('userDetails')->where('phone','=',$phone)->where('role_id','=',7)->where('status', '!=', 'Deleted')->limit(1)->first();
        if($data==null){
            return false;
        }else{ 
            $this->fnObj = $data;
            return true;
        }

    }
    function isEmailExistByForGuest($email){
        $data = $this->with('userDetails')->where('email','=',$email)->where('role_id','=',8)->limit(1)->first();
        if($data==null){
            return false;
        }else{   
            $this->fnObj = $data;
            return true;
        }

    }
    
    function isEmailExistByForDeletedCustomer($email){
        $data = $this->with('userDetails')->where('email','=',$email)->where('role_id','=',7)->where('status', '=','Deleted')->limit(1)->first();
        if($data==null){
            return false;
        }else{   
            $this->fnObj = $data;
            return true;
        }

    }

} 