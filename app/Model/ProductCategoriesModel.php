<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: ProductModel.php
 * User: rajib
 * Date: 1/14/16
 * Time: 3:29 PM
 */

namespace App\Model;


use App\Model\DataModel\ProductCategories;
use App\Model\DataModel\ProductDiscount;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ProductCategoriesModel extends BaseMallBDModel {

    protected $primaryKey = 'id';
    protected $table      ='product_categories';

    public function productCatCategory()
    {
        return $this->hasOne("App\Model\CategoryModel","id","category_id");//(modelName,foreignKey,PrimaryKey)
    }

    public function productCatProduct()
    {
        return $this->hasOne("App\Model\ProductModel","id","product_id");//(modelName,foreignKey,PrimaryKey)
    }

    public function setId($id){
        $this->id = $id;
    }

    public function getRelatedProducts($categoryId){

        $relatedProducts = $this->where("category_id","=",$categoryId)->distinct()->get();
        $relatedProductList = [];
        foreach($relatedProducts as $relatedProduct)
        {
            $productCategory = new ProductCategories();
            $productCategory->castMe($relatedProduct);
            array_push($relatedProductList,$productCategory);
        }
        return $relatedProductList;
    }

}