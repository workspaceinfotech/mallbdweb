<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class VoucherModel extends BaseMallBDModel {

    protected $fillable = array('user_id', 'voucher_code', 'percent', 'created_by', 'updated_by');
    protected $table = 'vouchers';
    protected $primaryKey = 'id';
    public $timestamps = FALSE;

    public function user() {
        return $this->hasOne("App\Models\UserModel", "id", "user_id");
    }

    public function getFilterVoucherData($searchVal = '', $limit = 10, $offset = 0, $orderBy = 0, $orderStyle = 'asc') {
        $orderByFields = array(
            0 => 'vouchers.id',
            1 => 'vouchers.voucher_code',
            2 => 'vouchers.percent',
            3 => 'users.firstname',
            4 => 'vouchers.id',
        );
        $data = VoucherModel::select('vouchers.*', 'users.firstname', 'users.lastname')
                ->leftJoin('users', 'vouchers.user_id', '=', 'users.id')
                ->where(function($query) use ($searchVal) {
                    $query->where('vouchers.id', 'LIKE', '%' . $searchVal . '%')
                    ->orWhere('vouchers.voucher_code', 'LIKE', '%' . $searchVal . '%')
                    ->orWhere('vouchers.percent', 'LIKE', '%' . $searchVal . '%')
                    ->orWhere('users.firstname', 'LIKE', '%' . $searchVal . '%')
                    ->orWhere('users.lastname', 'LIKE', '%' . $searchVal . '%');
                })
                ->limit($limit)
                ->offset($offset)
                ->orderBy($orderByFields[$orderBy], $orderStyle)
                ->get();
        return $data;
    }

    public function getFilterVoucherCount($searchVal = '', $limit = 10, $offset = 0, $orderBy = 0, $orderStyle = 'asc') {
        $orderByFields = array(
            0 => 'vouchers.id',
            1 => 'vouchers.voucher_code',
            2 => 'vouchers.percent',
            3 => 'users.firstname',
            4 => 'vouchers.id',
        );
        $data = VoucherModel::select('vouchers.*', 'users.firstname', 'users.lastname')
                ->leftJoin('users', 'vouchers.user_id', '=', 'users.id')
                ->where(function($query) use ($searchVal) {
                    $query->where('vouchers.id', 'LIKE', '%' . $searchVal . '%')
                    ->orWhere('vouchers.voucher_code', 'LIKE', '%' . $searchVal . '%')
                    ->orWhere('vouchers.percent', 'LIKE', '%' . $searchVal . '%')
                    ->orWhere('users.firstname', 'LIKE', '%' . $searchVal . '%')
                    ->orWhere('users.lastname', 'LIKE', '%' . $searchVal . '%');
                })
                ->orderBy($orderByFields[$orderBy], $orderStyle)
                ->count();
        return $data;
    }

    public function getAllVoucherCount() {
        $cont = VoucherModel::all();
        return count($cont);
    }

}
