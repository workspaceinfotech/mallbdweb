<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: TagModel.php
 * User: rajib
 * Date: 1/14/16
 * Time: 3:43 PM
 */

namespace App\Model;



use App\Http\Requests\Request;
use App\Model\DataModel\Product;

class TagModel extends BaseMallBDModel{

    protected $primaryKey = 'id';
    protected $table = 'tags';

    public function tagShop()
    {
        return $this->hasOne("App\Model\ShopModel","id","shop_id");
    }

    public function products()
    {
        return $this->belongsToMany("App\Model\ProductModel","product_tags","product_id","tag_id");
    }

    public function getAllProductWithTag()
    {
        $products = TagModel::where("tag_name","LIKE","%men%")->get()->all();
     //   $products = TagModel::find(2);
        //return $products;
        $productList = [];
        foreach($products->productTags as $p)
        {
            $prod = new Product();
            $prod->castMeFromObj($p);
            array_push($productList,$prod);
        }
        return $productList;
    }

    public function getTagIdByName($keywords)
    {
        $pages = TagModel::query();

        foreach($keywords as $word){
            $pages->orWhere('tag_name', 'LIKE', '%'.$word.'%');
        }
        $pages = $pages->distinct()->get();

        $tagIds = [];
        foreach($pages as $c)
        {
            array_push($tagIds,$c->tag_id);
        }
        return $tagIds;
    }

} 