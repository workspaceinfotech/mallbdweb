<?php
/**
 * Created by PhpStorm.
 * User: mi
 * Date: 2/23/16
 * Time: 2:09 PM
 */

namespace App\Model\DataModel;


class ShoppingCart extends BaseDataModel
{
    public $shoppingCartCell;
    public $orderTotal;
    public $totalPrice;
    public $totalDiscount;
    public $totalTax;
    public $shippingCost;

    /**
     * ShoppingCart constructor.
     */
    public function __construct(){
        $this->shoppingCartCell = [];
        $this->orderTotal = 0;
        $this->totalPrice = 0;
        $this->totalDiscount = 0;
        $this->totalTax = 0;
        $this->shippingCost = 0;
    }
    public function castMe($obj){
        $this->orderTotal = $obj->orderTotal;
        $this->totalPrice = $obj->totalPrice;
        $this->totalDiscount = $obj->totalDiscount;
        $this->totalTax = $obj->totalTax;
        $this->shoppingCartCell = $obj->shoppingCartCell;
        $this->shippingCost = $obj->shippingCost;
    }
}