<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: Role.php
 * User: rajib
 * Date: 1/12/16
 * Time: 11:36 AM
 */
namespace App\Model\DataModel;

class Role extends BaseDataModel{

    public $id;
    public $roleName;

    function __construct()
    {
        $this->id = 0;
        $this->roleName = "";
    }

    public function castMe($obj)
    {
        if($obj!=null) {
            $this->id = $obj->id;
            $this->roleName = $obj->role_name;
        }
    }
    public function castMeFromObject($obj)
    {
        if($obj!=null){
            $this->id  =(int) $obj['id'];
            $this->roleName = $obj['role_name'];
        }
    }


}