<?php
/**
 * Project  : mallbdweb
 * File     : CustomerReward.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 2/15/16 - 1:41 PM
 */
namespace App\Model\DataModel;

class CustomerReward extends BaseDataModel{
    public $id;
    public $description;
    public $points;
    public $createdOn;
    public $createdBy;
    public $updatedOn;
    public $updatedBy;
    public $customer;
    public $order;

    function __construct()
    {
        $this->id = 0;
        $this->description="";
        $this->points = "";
        $this->createdOn = "";
        $this->createdBy =  "";
        $this->updatedOn = "";
        $this->updatedBy = "";
        $this->customer = new User();
        $this->order = new Order();

    }

    public function castMe($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj->id;
            $this->description = $obj->description;
            $this->points = $obj->points;
            $this->createdOn = $obj->created_on;
            $this->createdBy = $obj->created_by;
            $this->updatedOn = $obj->updated_on;
            $this->updatedBy= $obj->updated_by;

            $this->customer->castMe($obj->rewardCustomer);
            $this->order->castMe($obj->rewardOrder);
        }
    }

    public function castMeFromObj($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj['id'];
            $this->description = $obj['description'];
            $this->points = $obj['points'];
            $this->createdOn = $obj['created_on'];
            $this->createdBy = $obj['created_by'];
            $this->updatedOn = $obj['updated_on'];
            $this->updatedBy = $obj['updated_by'];

            $this->customer->castMeFromObject($obj['rewardCustomer']);
            $this->order->castMeFromObj($obj['rewardOrder']);
        }
    }


}