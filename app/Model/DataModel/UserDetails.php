<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: UserDetails.php
 * User: rajib
 * Date: 1/13/16
 * Time: 1:34 PM
 */

namespace App\Model\DataModel;


class UserDetails extends BaseDataModel{

    public $id;
    public $address;
    public $shippingAddress;

    function __construct()
    {
        $this->address = new Address();
        $this->id = 0;
        $this->shippingAddress = new ShippingAddress();

    }

    public function castMe($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj->id;
            $this->address->castMe($obj);
            $this->shippingAddress->castMe($obj);

        }
    }

    public function castMeFromObj($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj['id'];
            $this->address->castMeFromObj($obj);
            $this->shippingAddress->castMeFromObj($obj);

        }
    }


} 