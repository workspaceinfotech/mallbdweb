<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: Zone.php
 * User: rajib
 * Date: 1/14/16
 * Time: 1:14 PM
 */

namespace App\Model\DataModel;


use Mockery\CountValidator\Exception;

class DeliveryMethod extends BaseDataModel{

    public $id;
    public $title;
    public $icon;
    public $deliveryPrice;
    public $createdOn;

    function __construct()
    {
        $this->id = 0;
        $this->title = "";
        $this->icon = "";
        $this->deliveryPrice =0;
        $this->createdOn = "";
    }

    public function castMe($obj)
    {
        if($obj!=null) {
            $this->id =(int)  $obj->id;
            $this->title = $obj->title;
            $this->icon = $obj->icon;
            try{
                $this->deliveryPrice = floatval($obj->delivery_price);
            }catch(Exception $ex){
                $this->deliveryPrice = 0;
            }
            try{
                $this->delivery_price_limit =  floatval($obj['delivery_price_limit']);
            }catch(Exception $ex){
                $this->delivery_price_limit = 0;
            }

            $this->createdOn = $obj->created_on;
        }
    }

    public function castMeFromObj($obj)
    {
        if($obj!=null) {
            $this->id = (int) $obj['id'];
            $this->title = $obj['title'];
            $this->icon = $obj['icon'];
            try{
                $this->deliveryPrice =  floatval($obj['delivery_price']);
            }catch(Exception $ex){
                $this->deliveryPrice = 0;
            }
            try{
                $this->delivery_price_limit =  floatval($obj['delivery_price_limit']);
            }catch(Exception $ex){
                $this->delivery_price_limit = 0;
            }

            $this->createdOn = $obj['created_on'];
        }
    }



} 