<?php
/**
 * Project  : mallbdweb
 * File     : Package.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 4/6/16 - 11:20 AM
 */

namespace App\Model\DataModel;


class PackageMobile extends BaseDataModel{

    public $id;
    public $packageTitle;
    public $description;
    //public $startDate;
    //public $endDate;
    public $image;
    public $originalPriceTotal;
    public $packagePriceTotal;
    //public $status;
   // public $createdOn;
    public $packageProduct;
    //public static $currency;

    function __construct()
    {
        $this->id=0;
        $this->packageTitle = "";
        $this->description= "";
        //$this->startDate="";
        //$this->endDate = "";
        $this->image="";
        $this->originalPriceTotal= 0;
        $this->packagePriceTotal=0;
        //$this->status = "";
        //$this->createdOn="";
        $this->packageProduct = [];
    }

   /* public static function setCurrency($currency){

        self::$currency = $currency;
        return true;
    }*/

    public function castMe($obj)
    {
        if($obj!=null) {
            $this->id = $obj->id;
            $this->packageTitle= $obj->package_title;
            $this->description = $obj->description_mobile;
            //$this->startDate = $obj->start_date;
            //$this->endDate = $obj->end_date;
            $this->image = $obj->image;
            $this->originalPriceTotal =(float) $obj->original_price_total;
            $this->packagePriceTotal =(float) $obj->package_price_total;
            //$this->status = $obj->status;
           // $this->createdOn = $obj->created_on;

            foreach($obj->packageProduct as $p)
            {
                $pack = new PackageProductMobile();
                //$pack->setCurrency(self::getCurrency());
                $pack->castMeFromObj($p);
                array_push($this->packageProduct,$pack);
            }
        }
    }
    public function castMeFromObj($obj)
    {
        if($obj!=null) {
            $this->id = $obj['id'];
            $this->packageTitle = $obj['package_title'];
            $this->description = $obj['description_mobile'];
            //$this->startDate = $obj['start_date'];
            //$this->endDate = $obj['end_date'];
            $this->image = $obj['image'];
            $this->originalPriceTotal =(float) $obj['original_price_total'];
            $this->packagePriceTotal = (float)$obj['package_price_total'];
            //$this->status = $obj['status'];
            //$this->createdOn = $obj['created_on'];

            foreach($obj['packageProduct'] as $p)
            {
                $pack = new PackageProduct();
                $pack->castMeFromObj($p);
                array_push($this->packageProduct,$pack);
            }
        }
    }

}