<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: Product.php
 * User: rafsan
 * Date: 1/14/16
 * Time: 1:17 PM
 */

namespace App\Model\DataModel;


class ZonalDeliveryBoy extends BaseDataModel{
    public $id;
    public $zoneId;
    public $userId;
    public $createdOn;

    //public zone;
    //public user;

    function __construct()
    {
        $this->id = 0;
        $this->zoneId=0;
        $this->userId=0;
        $this->createdOn = "";

        $this->zone= new Zone();
        $this->user= new User();

    }

    public function castMe($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj->id;
            $this->zoneId = $obj->zone_id;
            $this->userId = $obj->user_id;
            $this->createdOn = $obj->created_on;

            $this->zone->castMe($obj->deliveryBoyZone);
            $this->user->castMe($obj->deliveryBoyUser);
        }
    }

    public function castMeFromObj($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj['id'];
            $this->zoneId = $obj['zone_id'];
            $this->userId = $obj['user_id'];
            $this->createdOn = $obj['created_on'];

            $this->zone->castMeFromObj($obj['deliveryBoyZone']);
            $this->user->castMeFromObject($obj['deliveryBoyUser']);
        }
    }


}