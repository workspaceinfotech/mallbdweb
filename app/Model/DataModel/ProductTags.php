<?php
/**
 * Project  : mallbdweb
 * File     : CustomerReward.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 2/15/16 - 1:41 PM
 */
namespace App\Model\DataModel;

class ProductTags extends BaseDataModel{
    public $id;
    public $createdOn;
    public $createdBy;
    public $updatedOn;
    public $updatedBy;
    public $product;
    public $tag;

    function __construct()
    {
        $this->id = 0;
        $this->createdOn = "";
        $this->createdBy =  "";
        $this->updatedOn = "";
        $this->updatedBy = "";
        $this->product = new Product();
        $this->tag = new Tags();

    }

    public function castMe($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj->id;
            $this->createdOn = $obj->created_on;
            $this->createdBy = $obj->created_by;
            $this->updatedOn = $obj->updated_on;
            $this->updatedBy= $obj->updated_by;

            //$this->product->castMe($obj->tagProduct);
            $this->tag->castMe($obj->productTag);

        }
    }

    public function castMeFromObj($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj['id'];
            $this->createdOn = $obj['created_on'];
            $this->createdBy = $obj['created_by'];
            $this->updatedOn = $obj['updated_on'];
            $this->updatedBy = $obj['updated_by'];

            //$this->product->castMeFromObj($obj['tagProduct']);
            $this->tag->castMeFromObj($obj['productTag']);
        }
    }


}