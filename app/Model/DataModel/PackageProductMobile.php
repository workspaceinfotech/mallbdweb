<?php
/**
 * Project  : mallbdweb
 * File     : PackageProduct.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 4/6/16 - 11:47 AM
 */

namespace App\Model\DataModel;

class PackageProductMobile extends BaseDataModel{

    public $id;
    public $packageId;
    public $product;
    public $quantity;
    public $price;
    public static $currency;
    //public $createdOn;

    function __construct()
    {
        $this->id=0;
        $this->packageId = 0;
        $this->product = new ProductMobile();
        $this->quantity=0;
        $this->price =0.0;
        //$this->createdOn="";

    }

    /*public static function setCurrency($currency){

        self::$currency = $currency;
        return true;
    }*/

    public function castMe($obj)
    {
        if($obj!=null) {
            //$this->product->setCurrency(self::$currency);
            $this->id = $obj->id;
            $this->packageId = $obj->package_id;
            $this->quantity= $obj->quantity;
            $this->price = $obj->description;
            //$this->createdOn = $obj->created_on;

            $this->product->castMe($obj->product);
        }
    }
    public function castMeFromObj($obj)
    {
        if($obj!=null) {
            //$this->product->setCurrency(self::$currency);
            $this->id = $obj['id'];
            $this->packageId = $obj['package_id'];
            $this->quantity = $obj['quantity'];
            $this->price = $obj['price'];
           // $this->createdOn = $obj['created_on'];

            $this->packageId = $obj['package_id'];

            $this->product->castMeFromObj($obj['product']);

        }
    }

}