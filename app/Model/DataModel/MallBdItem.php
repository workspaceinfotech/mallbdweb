<?php
/**
 * Created by PhpStorm.
 * User: mi
 * Date: 5/2/16
 * Time: 3:53 PM
 */

namespace App\Model\DataModel;


class MallBdItem extends BaseDataModel{
    const _PRODUCT = 'product';
    const _PACKAGE = 'package';
    public $id;
    public $type;
    public $item;
    public $createdOn;

    /**
     * MallBdItem constructor.
     */
    public function __construct()
    {
        $this->id = 0;
        $this->type ="";
        $this->item = new \stdClass();
        $this->createdOn = "";
    }
    public function setProduct($product){
        $this->id = $product->id;
        $this->type = self::_PRODUCT;
        $this->item = $product;
        $this->createdOn = $product->createdOn;
    }
    public function setPackage($package){
        $this->id = $package->id;
        $this->type = self::_PACKAGE;
        $this->item = $package;
        $this->createdOn = $package->createdOn;
    }

}