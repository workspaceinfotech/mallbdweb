<?php
/**
 * Project  : mallbdweb
 * File     : CustomerReward.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 2/15/16 - 1:41 PM
 */
namespace App\Model\DataModel;

class ProductCategories extends BaseDataModel{
    public $id;
    public $productId;
    public $createdOn;
    public $createdBy;
    public $updatedOn;
    public $updatedBy;
    public $category;
    //public $product;

    function __construct()
    {
        $this->id = 0;
        $this->productId = 0;
        $this->createdOn = "";
        $this->createdBy =  "";
        $this->updatedOn = "";
        $this->updatedBy = "";
        $this->category = new Category();
        $this->product = new Product();

    }

    public function castMe($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj->id;
            $this->productId = $obj->product_id;
            $this->createdOn = $obj->created_on;
            $this->createdBy = $obj->created_by;
            $this->updatedOn = $obj->updated_on;
            $this->updatedBy= $obj->updated_by;

            $this->category->castMe($obj->productCatCategory);
            $this->product->castMe($obj->productCatProduct);

        }
    }

    public function castMeFromObj($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj['id'];
            $this->productId = $obj['product_id'];
            $this->createdOn = $obj['created_on'];
            $this->createdBy = $obj['created_by'];
            $this->updatedOn = $obj['updated_on'];
            $this->updatedBy = $obj['updated_by'];

            $this->category->castMeFromObj($obj['productCatCategory']);
            $this->product->castMeFromObj($obj['productCatProduct']);
        }
    }



}