<?php
/**
 * Project  : mallbdweb
 * File     : CustomerReward.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 2/15/16 - 1:41 PM
 */
namespace App\Model\DataModel;

use App\Model\OrderAttributeModel;
use App\Model\OrderProductsModel;
use App\Model\PackageModel;
use App\Model\PaymentMethodsModel;
use App\Model\ProductAttributesCombinationModel;
use Mockery\CountValidator\Exception;

class OrderProducts extends BaseDataModel{
    public $id;
    public $productQuantity;
    public $packageQuantity;
    public $price;
    public $total;
    public $tax;
    public $discount;
    public $productId;
    public $packageId;
    public $selectedProductAttribute;
    public $itemType;
    static $_PRODUCT = 'product';
    static $_PACKAGE = 'package';
    function __construct()
    {
        $this->id = 0;
        $this->productQuantity = 0;
        $this->packageQuantity = 0;
        $this->price = 0.0;
        $this->total = 0.0;
        $this->tax = 0.0;
        $this->discount = 0.0;
        $this->productId = 0;
        $this->packageId = 0;
        $this->item = new \stdClass();
        $this->productitem = new \stdClass();
        //$this->packagaeitem = new \stdClass();
        //$this->package = new Package();
        $this->selectedProductAttribute = [];
        $this->itemType = '';
    }

    public function castMe($obj)
    {
//        if($obj!=null)
//        {
//            $this->id = $obj->id;
//            $this->productQuantity = $obj->product_quantity;
//            $this->price = $obj->price;
//            $this->total = $obj->total;
//            $this->tax = $obj->tax;
//            $this->discount = $obj->discount;
//            $this->createdOn = $obj->created_on;
//            $this->item->castMeExtended($obj->order_id,$obj->orderProductsProduct);
//
//
//
//
//        }
    }

    public function castMeFromObj($obj)
    {
        if($obj!=null)
        {


            $this->id = $obj['id'];
            $this->productId = $obj['product_id'];
            $this->packageId = $obj['package_id'];
            $this->productQuantity = $obj['product_quantity'];
            $this->packageQuantity = $obj['package_quantity'];
            $this->price =round($obj['price'],2);
            $this->total =round($obj['total'],2);
            $this->tax = (float)$obj['tax'];
            $this->discount = $obj['discount'];

            try{
                if($obj['package_id']==null || intval($obj['package_id'])==0){
                    $this->itemType = self::$_PRODUCT;
                    $this->item = new OrderProduct();
                    $this->item->castMeFromObjExtended($obj['orderProductsProduct']);
                }else{

                }
            }catch (Exception $ex){

            }


            $orderAttributeModel = new OrderAttributeModel();
            $productAttributesCombinationModel = new ProductAttributesCombinationModel();
            if($obj!=null){
                //   parent::castMeFromObj($obj);

                $combinationKey = $orderAttributeModel->getByOrderIdAndProductId($obj['order_id'],$obj['orderProductsProduct']['id']);

                foreach($combinationKey as $ck){
                    $selectedProductAttribute = new SelectedProductAttribute();
                    $selectedProductAttribute->selectedAttribute = $productAttributesCombinationModel->getSelectedAttributeByCombinationKey($ck->combination_key);
                    $selectedProductAttribute->quantity = $ck->quantity ;
                    $selectedProductAttribute->price =  $obj['price'];

                    array_push($this->selectedProductAttribute,$selectedProductAttribute);
                }

            }

        }
    }
    public function castMeFromObjMobile($obj)
    {
        if($obj!=null)
        {
            

            $this->id = $obj['id'];
            $this->productId = (int)$obj['product_id'];
            $this->packageId = (int)$obj['package_id'];
            $this->productQuantity = (int)$obj['product_quantity'];
            $this->packageQuantity = (int)$obj['package_quantity'];
            $this->price =round($obj['price'],2);
            $this->total =round($obj['total'],2);
            $this->tax = (float)$obj['tax'];
            $this->discount = $obj['discount'];

            try{
                if($obj['package_id']==null || intval($obj['package_id'])==0){
                    $this->itemType = self::$_PRODUCT;
                    $this->productitem = new OrderProduct();
                    $this->productitem->castMeFromObjExtended($obj['orderProductsProduct']);
                }else{

                }
            }catch (Exception $ex){

            }

        }
    }

}
