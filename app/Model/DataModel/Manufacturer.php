<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: Manufacturer.php
 * User: rajib
 * Date: 1/14/16
 * Time: 1:16 PM
 */

namespace App\Model\DataModel;


class Manufacturer extends BaseDataModel{
    public $id;
    public $name;
    public $icon;
    public $createdOn;

    function __construct()
    {
        $this->id = 0;
        $this->name = "";
        $this->icon = "";
        $this->createdOn = "";
    }

    public function castMe($obj)
    {
        if($obj!=null){
            $this->id = $obj->id;
            $this->name = $obj->name;
            $this->icon = $obj->icon;
            $this->createdOn = $obj->created_on;
        }

    }

    public function castMeFromObj($obj)
    {
        if($obj!=null) {
            $this->id = $obj['id'];
            $this->name = $obj['name'];
            $this->icon = $obj['icon'];
            $this->createdOn = $obj['created_on'];
        }
    }


} 