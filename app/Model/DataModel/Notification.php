<?php
/**
 * Project  : mallbdweb
 * File     : Notification.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 4/5/16 - 1:07 PM
 */
namespace App\Model\DataModel;

class Notification extends BaseDataModel{
    public $id;
    public $notificationType;
    public $notificationId;
    public $notificationText;

    function __construct()
    {
        $this->id = 0;
        $this->notificationType = "";
        $this->notificationId = 0;
        $this->notificationText = "";
    }

    public function castMe($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj->id;
            $this->notificationType = $obj->notification_type;
            $this->notificationId = $obj->notification_id;
            $this->notificationText = $obj->notification_text;
        }
    }

    public function castMeFromObj($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj['id'];
            $this->notificationType = $obj['notification_type'];
            $this->notificationId = $obj['notification_id'];
            $this->notificationText = $obj['notification_text'];
        }
    }

}