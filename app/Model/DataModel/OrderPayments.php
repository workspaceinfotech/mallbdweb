<?php
/**
 * Project  : mallbdweb
 * File     : CustomerReward.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 2/15/16 - 1:41 PM
 */
namespace App\Model\DataModel;

use App\Model\PaymentMethodsModel;

class OrderPayments extends BaseDataModel{
    public $id;
    public $order_id;
    public $paymentTotal;
    public $paymentDetails;
    public $paymentStatus;
    public $createdOn;
    public $createdBy;
    public $updatedOn;
    public $updatedBy;
    public $paymentMethod;

    function __construct()
    {
        $this->id = 0;
        $this->order_id = 0;
        $this->paymentTotal = "";
        $this->paymentDetails = "";
        $this->paymentStatus = "";
        $this->createdOn = "";
        $this->createdBy =  "";
        $this->updatedOn = "";
        $this->updatedBy = "";

       // $this->order = new Order();
        $this->paymentMethod = new PaymentMethods();

    }

    public function castMe($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj->id;
            $this->order_id = $obj->order_id;
            $this->paymentTotal = $obj->payment_total;
            $this->paymentDetails = $obj->payment_details;
            $this->paymentStatus = $obj->payment_status;
            $this->createdOn = $obj->created_on;
            $this->createdBy = $obj->created_by;
            $this->updatedOn = $obj->updated_on;
            $this->updatedBy= $obj->updated_by;

            //$this->order->castMe($obj->orderPaymentsOrder);
            $this->paymentMethod->castMe($obj->orderPaymentsPayMethod);

        }
    }

    public function castMeFromObj($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj['id'];
            $this->order_id = $obj['order_id'];
            $this->paymentTotal = $obj['payment_total'];
            $this->paymentDetails = $obj['payment_details'];
            $this->paymentStatus = $obj['payment_status'];
            $this->createdOn = $obj['created_on'];
            $this->createdBy = $obj['created_by'];
            $this->updatedOn = $obj['updated_on'];
            $this->updatedBy = $obj['updated_by'];

            //$this->order->castMeFromObj($obj['orderPaymentsOrder']);
            $this->paymentMethod->castMeFromObj($obj['orderPaymentsPayMethod']);
        }
    }



}