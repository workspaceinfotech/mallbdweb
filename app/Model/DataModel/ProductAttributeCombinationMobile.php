<?php
/**
 * Created by PhpStorm.
 * Project: MallBDFront
 * File: ProductAttributeCombination.php
 * User: rajib
 * Date: 1/29/16
 * Time: 6:33 PM
 */

namespace App\Model\DataModel;


class ProductAttributeCombinationMobile extends BaseDataModel{

    //public $id;
    //public $productId;
    public $productAttributesId;
    public $attributeValueId;
    public $combinationKey;
    public $createdOn;

    function __construct()
    {
        //$this->id = 0;
        //$this->productId = 0;
        $this->productAttributesId = 0;
        $this->attributeValueId = 0;
        $this->combinationKey = 0;

    }
    public function castMe($obj)
    {
        if($obj!=null) {
            //$this->id = $obj->id;
            //$this->productId = $obj->product_id;
            $this->productAttributesId = $obj->product_attributes_id;;
            $this->attributeValueId = $obj->attribute_value_id;
            $this->combinationKey = $obj->combination_key;
        }
    }

    public function castMeFromObj($obj)
    {
        if($obj!=null) {
            //$this->id = $obj['id'];
            //$this->productId = $obj['product_id'];
            $this->productAttributesId = $obj['product_attributes_id'];
            $this->attributeValueId = $obj['attribute_value_id'];
            $this->combinationKey = $obj['combination_key'];
        }
    }


} 