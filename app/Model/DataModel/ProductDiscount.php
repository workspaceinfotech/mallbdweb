<?php
/**
 * Project  : mallbdweb
 * File     : CustomerReward.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 2/15/16 - 1:41 PM
 */
namespace App\Model\DataModel;

class ProductDiscount extends BaseDataModel{
    public $id;
    public $discountId;
    public $categoryId;
    public $manufacturerId;
    public $productId;
    public $image;
    public $startDate;
    public $endDate;
    public $createdOn;

    function __construct()
    {
        $this->id = 0;
        $this->discountId = 0;
        $this->categoryId =  0;
        $this->manufacturerId = 0;
        $this->productId = 0;
        $this->image = "";
        $this->startDate = "";
        $this->endDate = "";
        $this->createdOn = "";

    }

    public function castMe($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj->id;
            $this->discountId = $obj->discount_id;
            $this->categoryId = $obj->category_id;
            $this->manufacturerId = $obj->manufacturer_id;
            $this->productId= $obj->product_id;
            $this->image = $obj->image;
            $this->startDate = $obj->start_date;
            $this->endDate = $obj->end_date;
            $this->createdOn= $obj->created_on;
        }
    }

    public function castMeFromObj($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj['id'];
            $this->discountId = $obj['created_on'];
            $this->categoryId = $obj['created_by'];
            $this->manufacturerId = $obj['updated_on'];
            $this->productId = $obj['updated_by'];
            $this->image = $obj['image'];
            $this->startDate = $obj['start_date'];
            $this->endDate = $obj['end_date'];
            $this->createdOn = $obj['created_on'];

        }
    }


}