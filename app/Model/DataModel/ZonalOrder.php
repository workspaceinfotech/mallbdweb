<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: Product.php
 * User: rafsan
 * Date: 1/14/16
 * Time: 1:17 PM
 */

namespace App\Model\DataModel;


class ZonalOrder extends BaseDataModel{
    public $id;
    public $orderId;
    public $zoneId;
    public $userId;
    public $createdOn;

    //public zone;
    //public user;
    //public order;

    function __construct()
    {
        $this->id = 0;
        $this->orderId=0;
        $this->zoneId=0;
        $this->userId=0;
        $this->createdOn = "";

        //$this->order = new Order();
        //$this->zone= new Zone();
        //$this->user= new User();

    }

    public function castMe($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj->id;
            $this->orderId = $obj->order_id;
            $this->zoneId = $obj->zone_id;
            $this->userId = $obj->user_id;
            $this->createdOn = $obj->created_on;

            //$this->order->castMe($obj->zonalOrderOrder);
            //$this->zone->castMe($obj->zonalOrderZone);
            // $this->user->castMe($obj->zonalOrderUser);
        }
    }

    public function castMeFromObj($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj['id'];
            $this->orderId = $obj['order_id'];
            $this->zoneId = $obj['zone_id'];
            $this->userId = $obj['user_id'];
            $this->createdOn = $obj['created_on'];

            //$this->order->castMeFromObject($obj['zonalOrderOrder']);
            //$this->zone->castMeFromObject($obj['zonalOrderZone']);
            //$this->user->castMeFromObject($obj['zonalOrderUser']);
        }
    }


}