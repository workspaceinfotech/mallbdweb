<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: User.php
 * User: rajib
 * Date: 1/12/16
 * Time: 11:17 AM
 */

namespace App\Model\DataModel;

class User extends BaseDataModel{
    public $id;
    public $code;
    public $firstName;
    public $lastName;
    public $createdOn;
    public $email;
    public $role;
    public $status;
    public $phone;
    public $image;
    public $userDetails;

    function __construct()
    {
        $this->id = 0;
        $this->code ="";
        $this->email = "";
        $this->createdOn = "";
        $this->firstName = "";
        $this->lastName = "";
        $this->status = "";
        $this->phone = "";
        $this->image  = "";
        $this->role = new Role();
        $this->userDetails = new UserDetails();
    }
    public function castMe($obj){
        if($obj!=null) {
            $this->id = (int) $obj->id;
            $this->code = $obj->code;
            $this->firstName = $obj->firstname;
            $this->lastName = $obj->lastname;
            $this->email = $obj->email;
            $this->status = $obj->status;
            $this->createdOn = $obj->created_on;
            $this->phone = $obj->phone;
            $this->image = $obj->image;
            $this->role->castMe($obj->role);
            $this->userDetails->castMe($obj->userDetails);

        }
    }
    public function castMeFromObject($obj){

        if($obj!=null){
            $this->id =(int) $obj['id'];
            $this->code = $obj['code'];
            $this->firstName = $obj['firstname'];
            $this->lastName = $obj['lastname'];
            $this->email = $obj['email'];
            $this->createdOn = $obj['created_on'];
            $this->status = $obj['status'];
            $this->phone = $obj['phone'];
            $this->image = $obj['image'];
            $this->role->castMeFromObject($obj['role']);
            $this->userDetails->castMeFromObj($obj['userDetails']);
        }
    }


}