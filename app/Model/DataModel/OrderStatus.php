<?php
/**
 * Project  : mallbdweb
 * File     : CustomerReward.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 2/15/16 - 1:41 PM
 */
namespace App\Model\DataModel;

class OrderStatus extends BaseDataModel{
    public $id;
    public $status;
    public $comments;
    public $createdOn;
    public $createdBy;
    public $updatedOn;
    public $updatedBy;
    public $orderStatusList;

    function __construct()
    {
        $this->id = 0;
        $this->status="";
        $this->comments = "";
        $this->createdOn = "";
        $this->createdBy =  "";
        $this->updatedOn = "";
        $this->updatedBy = "";
        $this->orderStatusList = new OrderStatusList();

    }

    public function castMe($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj->id;
            $this->status = $obj->status;
            $this->comments = $obj->comments;
            $this->createdOn = $obj->created_on;
            $this->createdBy = $obj->created_by;
            $this->updatedOn = $obj->updated_on;
            $this->updatedBy= $obj->updated_by;

            $this->orderStatusList->castMe($obj->orderStatusList);
        }
    }

    public function castMeFromObj($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj['id'];
            $this->status = $obj['status'];
            $this->comments = $obj['comments'];
            $this->createdOn = $obj['created_on'];
            $this->createdBy = $obj['created_by'];
            $this->updatedOn = $obj['updated_on'];
            $this->updatedBy = $obj['updated_by'];

            $this->orderStatusList->castMeFromObj($obj['orderStatusList']);
        }
    }


}