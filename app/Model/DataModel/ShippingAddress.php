<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: ShippingAddress.php
 * User: rajib
 * Date: 1/13/16
 * Time: 1:35 PM
 */

namespace App\Model\DataModel;


class ShippingAddress extends BaseDataModel{

    public $userId;
    public $shippingAddress;
    public $shippingCountry;
    public $shippingCity;
    public $shippingZipCode;

    function __construct()
    {
        $this->id=0;
        $this->userId=0;
        $this->shippingAddress = "";
        $this->shippingCity = "";
        $this->shippingCountry = "";
        $this->shippingZipCode = "";
    }

    public function castMe($obj)
    {
        $this->id=(int)$obj->id;
        $this->userId =(int) $obj->user_id;
        $this->shippingAddress = $obj->shipping_address;
        $this->shippingCountry = $obj->shipping_country;
        $this->shippingCity = $obj->shipping_city;
        $this->shippingZipCode = $obj->shipping_zipcode;
    }

    public function castMeFromObj($obj)
    {
        $this->id = (int)$obj['id'];
        $this->userId =(int) $obj['user_id'];
        $this->shippingAddress = $obj['shipping_address'];
        $this->shippingCountry = $obj['shipping_country'];
        $this->shippingCity = $obj['shipping_city'];
        $this->shippingZipCode = $obj['shipping_zipcode'];

    }


} 