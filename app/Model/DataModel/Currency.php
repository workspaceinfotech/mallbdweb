<?php


namespace App\Model\DataModel;


class Currency extends BaseDataModel{
    public $id;
    public $title;
    public $code;
    public $symbol;
    public $value;
    public $status;
    public $createdOn;
    public $createdBy;
    public $updatedOn;
    public $updatedBy;

    function __construct()
    {
        $this->id = 0;
        $this->title="";
        $this->code = "";
        $this->symbol = "";
        $this->value = "";
        $this->status = "";
        $this->createdOn = "";
        $this->createdBy =  "";
        $this->updatedOn = "";
        $this->updatedBy = "";

    }

    public function castMe($obj)
    {
        if($obj!=null)
        {
            $this->id = (int)$obj->id;
            $this->title = $obj->title;
            $this->code = $obj->code;
            $this->symbol = $obj->symbol;
            $this->value = (float)$obj->value;
            $this->status = $obj->status;
            $this->createdOn = $obj->created_on;
            $this->createdBy = $obj->created_by;
            $this->updatedOn = $obj->updated_on;
            $this->updatedBy= $obj->updated_by;
        }
    }

    public function castMeFromObj($obj)
    {
        if($obj!=null)
        {
            $this->id =(int) $obj['id'];
            $this->title = $obj['title'];
            $this->code = $obj['code'];
            $this->symbol = $obj['symbol'];
            $this->value = (float)$obj['value'];
            $this->status = $obj['status'];
            $this->createdOn = $obj['created_on'];
            $this->createdBy = $obj['created_by'];
            $this->updatedOn = $obj['updated_on'];
            $this->updatedBy = $obj['updated_by'];
        }
    }


} 