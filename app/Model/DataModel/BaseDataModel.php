<?php
/**
 * Project  : mallbdweb
 * File     : BaseDataModel.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 6/16/16 - 5:42 PM
 */
namespace App\Model\DataModel;

class BaseDataModel{

    private static $currency;

    /**
     * @param mixed $currency
     */
    public static function setCurrency($currency) {
        self::$currency = $currency;
    }

    /**
     * @return mixed
     */
    public static function getCurrency() {
        return self::$currency;
    }


}