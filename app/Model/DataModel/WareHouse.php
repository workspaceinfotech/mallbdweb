<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: WareHouse.php
 * User: rajib
 * Date: 1/14/16
 * Time: 1:15 PM
 */

namespace App\Model\DataModel;


class WareHouse extends BaseDataModel{

    public $id;
    public $code;
    public $name;
    public $zone;
    public $address;
    public $country;
    public $city;
    public $zipCode;
    public $manager;
    public $createdOn;
    public $createdBy;
    public $updatedOn;
    public $updatedBy;

    function __construct()
    {
        $this->id = 0;
        $this->code = "";
        $this->name = "";
        $this->zone = "";
        $this->address = "";
        $this->country = "";
        $this->city = "";
        $this->zipCode = "";
        $this->manager = new User();
        $this->createdOn = "";
        $this->createdBy = new User();
        $this->updatedOn = "";
        $this->updatedBy = new User();
    }
    public function castMe($obj){
        if($obj!=null){

            $this->id = $obj->id;
            $this->code = $obj->warehouse_code;
            $this->name = $obj->name;
            $this->zone = $obj->zone;
            $this->address = $obj->address;
            $this->country = $obj->country;
            $this->city = $obj->city;
            $this->zipCode = $obj->zipcode;
            $this->manager->castMe($obj->wareHouseManager);
            $this->createdOn = "";
        //    $this->createdBy->castMeFromObj($obj->createdBy);
            $this->updatedOn = "";
         //   $this->updatedBy->castMeFromObj($obj->updatedBy);

        }
    }
    public function castMeFromObj($obj){
        if($obj!=null){

            $this->id = $obj->id;
            $this->code = $obj->warehouse_code;
            $this->name = $obj->name;
            $this->zone = $obj->zone;
            $this->address = $obj->address;
            $this->manager->castMeFromObject($obj->wareHouseManager);
            $this->createdOn = "";
            $this->createdBy = new AppCredential();
            $this->updatedOn = "";
            $this->updatedBy = new AppCredential();

        }
    }

} 