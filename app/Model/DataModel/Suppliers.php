<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: Suppliers.php
 * User: rajib
 * Date: 1/14/16
 * Time: 1:15 PM
 */

namespace App\Model\DataModel;


class Suppliers extends BaseDataModel{
    public $id;
    public $code;
    public $name;
    public $phone;
    public $email;
    public $contactPerson;
    public $address;
    public $country;
    public $createdOn;

    function __construct()
    {
       $this->id = 0;
        $this->code = "";
        $this->name = "";
        $this->phone = "";
        $this->email = "";
        $this->contactPerson = "";
        $this->address =  "";
        $this->country = "";
        $this->createdOn = "";

    }

    public function castMe($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj->id;
            $this->code = $obj->supplier_code;
            $this->name = $obj->name;
            $this->phone = $obj->phone;
            $this->email = $obj->email;
            $this->contactPerson = $obj->contact_person;
            $this->address = $obj->address;
            $this->country = $obj->country;
            $this->createdOn = $obj->created_on;
        }
    }

    public function castMeFromObj($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj['id'];
            $this->code = $obj['supplier_code'];
            $this->name = $obj['name'];
            $this->phone = $obj['phone'];
            $this->email = $obj['email'];
            $this->contactPerson = $obj['contact_person'];
            $this->address = $obj['address'];
            $this->country = $obj['country'];
            $this->createdOn = $obj['created_on'];
        }
    }


} 