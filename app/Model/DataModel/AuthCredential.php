<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: AuthCredential.php
 * User: rajib
 * Date: 1/12/16
 * Time: 12:05 PM
 */
namespace App\Model\DataModel;

class AuthCredential extends AppCredential{

    public $accesstoken;

    function __construct()
    {
        parent::__construct();
        $this->accesstoken = "";
    }

    public function castMe($obj)
    {
        if($obj!=null) {
            parent::castMe($obj);
            $this->accesstoken = $obj->access_token;
        }
    }


}