<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: AttributesValue.php
 * User: rajib
 * Date: 1/14/16
 * Time: 1:16 PM
 */

namespace App\Model\DataModel;


class AttributesValueMobile extends BaseDataModel{

    public $id;
    //public $attributeId;
    public $value;
    //public $createdOn;

    function __construct()
    {
        $this->id = 0;
        //$this->attributeId = 0;
        $this->value = "";
        //$this->createdOn = "";
    }

    public function castMe($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj->id;
            //$this->attributeId = $obj->attribute_id;
            $this->value = $obj->value;
            //$this->createdOn = $obj->created_on;
        }
    }
    public function castMeFromObj($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj['id'];
            //$this->attributeId = $obj['attribute_id'];
            $this->value = $obj['value'];
            //$this->createdOn = $obj['created_on'];
        }
    }


} 