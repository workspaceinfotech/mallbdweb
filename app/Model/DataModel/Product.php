<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: Product.php
 * User: rajib
 * Date: 1/14/16
 * Time: 1:17 PM
 */

namespace App\Model\DataModel;


use App\Model\CustomerWishListModel;
use App\Model\ProductModel;

class Product extends BaseDataModel{
    public $id;
    public $code;
    public $title;
    public $description;
    public $productDescriptionMobile;
    public $url;
    public $barCode;
    public $point;
    public $avgRating;
    public $width;
    public $height;
    public $depth;
    public $weight;
    public $quantity;
    public $discountAmount;
    public $discountActiveFlag;
    public $discountStartDate;
    public $discountEndDate;
    public $status;
    public $metaTitle;
    public $metaDescription;
    public $expireDate;
    public $manufacturedDate;
    public $createdOn;
    public $shop;
    public $supplier;
    public $warehouse;
    public $manufacturer;
    public $prices;
    public $discounts;
    public $categories;
    public $tags;
    public $pictures;
    public $attributes;
    public $isFeatured;
    public $isBestSeller;
    public $productQuantity;
    public $previousPrice;
    public $videoLink;
    public $videoLinkWeb;
    public $isWished;
    public $longDescription;
    public $longDescriptionMobile;
    public $minimumOrderQuantity;
    //public static $currency;

    function __construct()
    {
       $this->id = 0;
        $this->code = "";
        $this->title = "";
        $this->description = "";
        $this->productDescriptionMobile = "";
        $this->url = "";
        $this->barCode = "";
        $this->point = 0;
        $this->avgRating = 0.0;
        $this->width = "";
        $this->height = "";
        $this->depth = "";
        $this->weight = "";
        $this->quantity = 0;
        $this->discountActiveFlag=FALSE;
        $this->discountAmount=0.0;
        $this->discountStartDate ="";
        $this->discountEndDate = "";
        $this->status = "";
        $this->metaTitle = "";
        $this->metaDescription = "";
        $this->expireDate = "";
        $this->manufacturedDate = "";
        $this->createdOn = "";
        $this->shop = new Shop();
        $this->supplier = new Suppliers();
        $this->warehouse = new WareHouse();
        $this->manufacturer = new Manufacturer();
        $this->prices = [];
        $this->pictures = [];
        $this->discounts = [];
        $this->categories = [];
        $this->tags = [];
        $this->productQuantity = [];
        $this->attributes = [];
        $this->isFeatured = false;
        $this->isBestSeller = false;
        $this->previousPrice = 0;
        $this->videoLink= "";
        $this->videoLinkWeb = "";
        $this->isWished = FALSE;
        $this->longDescription = "";
        $this->longDescriptionMobile = "";
        $this->minimumOrderQuantity=1;
    }

   /* public static function setCurrency($currency){

        self::$currency = $currency;

        return true;
    }

   */
    public function castMe($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj->id;
            $this->code = $obj->product_code;
            $this->title = $obj->product_title;
            $this->description = $obj->product_description;
            $this->productDescriptionMobile = $obj->product_description_mobile;
            $this->url = $obj->product_url;
            $this->barCode = $obj->barcode;
            $this->point = (int)$obj->point;
            $this->avgRating = (float)$obj->avg_rating;
            $this->width = $obj->width;
            $this->height = $obj->height;
            $this->depth = $obj->depth;
            $this->weight = $obj->wight;
            $this->quantity = $obj->product_quantity;

            $this->discountAmount = (float)$obj->discount *self::getCurrency();
            $this->discountStartDate = $obj->discount_start_date;
            $this->discountEndDate = $obj->discount_end_date;

            if($this->discountAmount>0) {
                if ((date("Y-m-d H:i:s") >= ($this->discountStartDate)) && (date("Y-m-d H:i:s") <= $this->discountEndDate)) {
                    $this->discountActiveFlag = TRUE;
                } else {
                    $this->discountActiveFlag = FALSE;
                }
            }else{
                $this->discountActiveFlag = FALSE;
            }

            $this->status = $obj->status;
            $this->metaTitle = $obj->meta_title;
            $this->metaDescription = $obj->meta_description;
            $this->expireDate = $obj->expire_date;
            $this->manufacturedDate = $obj->manufactured_date;
            $this->createdOn = $obj->created_on;
            $this->isFeatured = (strtolower($obj->is_featured)=='yes')?true:false;
            $this->longDescription = $obj->long_description;
            $this->longDescriptionMobile = $obj->long_description_mobile;
            $this->minimumOrderQuantity = $obj->minimum_order_quantity;

            $this->videoLink= $obj->video_link;

            if($this->videoLink != ""){
                $videoToken = explode("v=",$this->videoLink);
                if(count($videoToken)<2){
                    $this->videoLinkWeb ="";
                }else{
                    $this->videoLinkWeb = "http://www.youtube.com/embed/".$videoToken[1];
                }

            }
            $productModel = new ProductModel();
            $bestSellercheck = $productModel->getAllBestSellerCheckByProductId($obj['id']);
            $this->isBestSeller = $bestSellercheck;

            $this->shop->castMe($obj->productShop);

            $this->supplier->castMe($obj->productSupplier);

            $this->manufacturer->castMe($obj->productManufacturer);

            $this->warehouse->castMe($obj->productWareHouse);

            foreach($obj->productPrices as $price)
            {
                $pPrice = new Price();
                //$pPrice->setCurrency(self::getCurrency());
                $pPrice->castMeFromObj($price);
                array_push($this->prices,$pPrice);
            }

            foreach ($obj->productImages as $image) {
                $pImage = new Picture();
                $pImage->castMeFromObj($image);
                array_push($this->pictures, $pImage);
            }

            foreach ($obj->productDiscount as $discount) {
                $pDiscount = new Discount();
                $pDiscount->castMeFromObj($discount);
                array_push($this->discounts, $pDiscount);
            }


            foreach ($obj->productTags as $tags) {
                $pTags = new Tags();
                $pTags->castMeFromObj($tags);
                array_push($this->tags, $pTags);
            }


            foreach ($obj->productAttributes as $attributes) {
                $pAttributes = new Attributes();
                $pAttributes->castMeFromObj($attributes);
                array_push($this->attributes, $pAttributes);
            }

//            foreach ($obj->productCategories as $cat) {
//                $category = new Category();
//                $category->castMeFromObj($cat);
//                array_push($this->categories, $category);
//            }
            foreach($obj->productQuantity as $quant)
            {
                $quantity = new Quantity();
                $quantity->castMe($quant);
                array_push($this->productQuantity,$quantity);
            }

        }
    }

    public function castMeFromObj($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj['id'];
            $this->code = $obj['product_code'];
            $this->title = $obj['product_title'];
            $this->description = $obj['product_description'];
            $this->productDescriptionMobile = $obj['product_description_mobile'];
            $this->url = $obj['product_url'];
            $this->barCode = $obj['barcode'];
            $this->point = (int)$obj['point'];
            $this->avgRating = (float)$obj['avg_rating'];
            $this->width = $obj['width'];
            $this->height = $obj['height'];
            $this->depth = $obj['depth'];
            $this->weight = $obj['weight'];
            $this->quantity = $obj['product_quantity'];

            $this->discountAmount = $obj['discount'] * self::getCurrency();
            $this->discountStartDate = $obj['discount_start_date'];
            $this->discountEndDate = $obj['discount_end_date'];

            if(($this->discountStartDate<=date("Y-m-d H:i:s")) && ($this->discountEndDate>=date("Y-m-d H:i:s")))
            {
                $this->discountActiveFlag = true;
            }else{
                $this->discountActiveFlag = false;
            }

            $this->status = $obj['status'];
            $this->metaTitle = $obj['meta_title'];
            $this->metaDescription = $obj['meta_description'];
            $this->expireDate = $obj['expire_date'];
            $this->manufacturedDate = $obj['manufactured_date'];
            $this->createdOn = $obj['created_on'];
            $this->isFeatured = (strtolower($obj['is_featured'])=='yes')?true:false;
           
            $this->videoLink= $obj['video_link'];

            if($this->videoLink != ""){
                $videoToken = explode("v=",$this->videoLink);
                if(count($videoToken)<2){
                    $this->videoLinkWeb ="";
                }else{
                    $this->videoLinkWeb = "http://www.youtube.com/embed/".$videoToken[1];
                }

            }
            $productModel = new ProductModel();
            $bestSellercheck = $productModel->getAllBestSellerCheckByProductId($obj['id']);
            $this->isBestSeller = $bestSellercheck;
//            var_dump($bestSellercheck);die;

            $this->longDescription = $obj['long_description'];
            $this->longDescriptionMobile = $obj['long_description_mobile'];
            $this->minimumOrderQuantity = $obj['minimum_order_quantity'];

            $this->shop->castMeFromObj($obj['productShop']);

            $this->supplier->castMeFromObj($obj['productSupplier']);

            $this->manufacturer->castMeFromObj($obj['productManufacturer']);

            $this->warehouse->castMeFromObj($obj['productWareHouse']);

            foreach($obj['productPrices'] as $price)
            {
                $pPrice = new Price();
                //$pPrice->setCurrency(self::getCurrency());
                $pPrice->castMeFromObj($price);
                array_push($this->prices,($pPrice));
            }

            foreach($obj['productImages'] as $image)
            {
             //   var_dump($image);
                $pImage = new Picture();
                $pImage->castMeFromObj($image);
                array_push($this->pictures,$pImage);
            }

            foreach($obj['productDiscount'] as $discount)
            {
                $pDiscount = new Discount();
                $pDiscount->castMeFromObj($discount);
                array_push($this->discounts,$pDiscount);
            }

            foreach($obj['productTags'] as $tags)
            {
                $pTags = new Tags();
                $pTags->castMeFromObj($tags);
                array_push($this->tags,$pTags);
            }

            foreach($obj['productAttributes'] as $attributes)
            {
                $pAttributes = new Attributes();
                $pAttributes->castMeFromObj($attributes);
                array_push($this->attributes,$pAttributes);
            }

            foreach($obj['productCategories'] as $cat)
            {
                $category = new Category();
                $category->castMeFromObj($cat);
                array_push($this->categories,$category);
            }
            foreach($obj['productQuantity'] as $quant)
            {
                $quantity = new Quantity();
                $quantity->castMeFromObj($quant);
                array_push($this->productQuantity,$quantity);
            }

        }
    }


} 