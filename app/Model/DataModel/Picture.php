<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: Picture.php
 * User: rajib
 * Date: 1/14/16
 * Time: 1:16 PM
 */

namespace App\Model\DataModel;


class Picture extends BaseDataModel{
    public $id;
    public $product_id;
    public $name;
    public $caption;
    public $position;
    public $cover;
    public $createdOn;

    function __construct()
    {
        $this->id = 0;
        $this->product_id = 0;
        $this->name = "";
        $this->caption = "";
        $this->position = "";
        $this->cover = "";
        $this->createdOn = "";
    }

    public function castMe($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj->id;
            $this->product_id = $obj->product_id;
            $this->name = $obj->image_name;
            $this->caption = $obj->caption;
            $this->position = $obj->position;
            $this->cover = $obj->cover;
            $this->createdOn = $obj->created_on;
        }
    }
    public function castMeFromObj($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj['id'];
            $this->product_id = $obj['product_id'];
            $this->name = $obj['image_name'];
            $this->caption = $obj['caption'];
            $this->position = $obj['position'];
            $this->cover = $obj['cover'];
            $this->createdOn = $obj['created_on'];
        }

    }

} 