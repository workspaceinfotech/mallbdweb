<?php
/**
 * Project  : mallbdweb
 * File     : CustomerReward.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 2/15/16 - 1:41 PM
 */
namespace App\Model\DataModel;

class Review extends BaseDataModel{
    public $id;
    public $note;
    public $rating;
    public $createdOn;
    public $createdBy;
    public $updatedOn;
    public $updatedBy;
    //public $product;
    public $customer;

    function __construct()
    {
        $this->id = 0;
        $this->note="";
        $this->rating = "";
        $this->createdOn = "";
        $this->createdBy =  "";
        $this->updatedOn = "";
        $this->updatedBy = "";
        //$this->product = new Product();
        $this->customer = new User();

    }

    public function castMe($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj->id;
            $this->note = $obj->note;
            $this->rating = $obj->rating;
            $this->createdOn = $obj->created_on;
            $this->createdBy = $obj->created_by;
            $this->updatedOn = $obj->updated_on;
            $this->updatedBy= $obj->updated_by;

            //$this->product->castMe($obj->reviewProduct);
            $this->customer->castMe($obj->reviewCustomer);

        }
    }

    public function castMeFromObj($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj['id'];
            $this->note = $obj['note'];
            $this->rating = $obj['rating'];
            $this->createdOn = $obj['created_on'];
            $this->createdBy = $obj['created_by'];
            $this->updatedOn = $obj['updated_on'];
            $this->updatedBy = $obj['updated_by'];

            //$this->product->castMeFromObj($obj['reviewProduct']);
            $this->customer->castMeFromObject($obj['rewardCustomer']);
        }
    }


}