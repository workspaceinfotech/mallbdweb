<?php
/**
 * Project  : mallbdweb
 * File     : MallBDCurrency.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 5/18/16 - 12:31 PM
 */
namespace App\Model\DataModel;

class MallBDCurrency extends BaseDataModel{

    public $HTMLCode;
    public $currencyCode;
    public $currencyRate;

    function __construct() {

        $this->HTMLCode = "";
        $this->currencyCode = "";
        $this->currencyRate = 0.0;
    }

    public function setHTMLCode($HTMLCode){
        $this->HTMLCode = $HTMLCode;
    }
    public function setCurrencyCode($currencyCode){
        $this->currencyCode = $currencyCode;
    }
    public function setCurrencyRate($currencyRate){
        $this->currencyRate = $currencyRate;
    }



}

