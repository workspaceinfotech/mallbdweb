<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: Discount.php
 * User: rajib
 * Date: 1/14/16
 * Time: 1:15 PM
 */

namespace App\Model\DataModel;


class Discount extends BaseDataModel{
    public $id;
    public $discountTitle;
    public $discountAmount;
    public $discountType;
    public $status;
    public $createdOn;

    function __construct()
    {
        $this->id = 0;
        $this->discountTitle = "";
        $this->discountAmount = 0;
        $this->discountType = "";
        $this->status = "";
        $this->createdOn = "";
    }

    public function castMe($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj->id;
            $this->discountTitle = $obj->discount_title;
            $this->discountAmount = $obj->discount_amount;
            $this->discountType = $obj->discount_type;
            $this->status = $obj->status;
            $this->createdOn = $obj->created_on;
        }
    }

    public function castMeFromObj($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj['id'];
            $this->discountTitle = $obj['discount_title'];
            $this->discountAmount = $obj['discount_amount'];
            $this->discountType = $obj['discount_type'];
            $this->status = $obj['end_date'];
            $this->createdOn = $obj['created_on'];
        }
    }

} 