<?php
/**
 * Created by PhpStorm.
 * Project: MallBDFront
 * File: Quantity.php
 * User: rajib
 * Date: 1/29/16
 * Time: 5:58 PM
 */

namespace App\Model\DataModel;


class Quantity extends BaseDataModel{
    public $id;
    public $productId;
    public $combinations;
    public $quantity;

    function __construct()
    {
        $this->id = 0;
        $this->productId = 0;
        $this->combinations = [];
        $this->quantity = 0;
    }

    public function castMe($obj)
    {
        if($obj!=null) {
            $this->id = $obj->id;
            $this->productId = $obj->product_id;

            foreach($obj->attributesCombinations as $attr)
            {
                $attributeCombination = new ProductAttributeCombination();
                $attributeCombination->castMe($attr);
                array_push($this->combinations,$attr);
            }
            $this->quantity = $obj->quantity;
        }
    }

    public function castMeFromObj($obj)
    {
        if($obj!=null) {
            $this->id = $obj['id'];
            $this->productId = $obj['product_id'];
            foreach($obj['attributesCombinations'] as $attr)
            {
                $attributeCombination = new ProductAttributeCombination();
                $attributeCombination->castMeFromObj($attr);
                array_push($this->combinations,$attributeCombination);
            }
            $this->quantity = $obj['quantity'];
        }
    }


} 