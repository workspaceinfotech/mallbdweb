<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: Product.php
 * User: rajib
 * Date: 1/14/16
 * Time: 1:17 PM
 */

namespace App\Model\DataModel;


use App\Model\OrderAttributeModel;
use App\Model\ProductAttributesCombinationModel;

class OrderProduct extends Product{
    //public $selectAttribute;
    //public $quantity;

    function __construct()
    {
       parent::__construct();


    }

    public function castMeExtended($obj)
    {
        $orderAttributeModel = new OrderAttributeModel();
        $productAttributesCombinationModel = new ProductAttributesCombinationModel();
        if($obj!=null){
            $this->id = $obj->id;
            $this->code = $obj->product_code;
            $this->title = $obj->product_title;
            $this->description = $obj->product_description;
            $this->productDescriptionMobile = $obj->product_description_mobile;
            $this->url = $obj->product_url;
            $this->barCode = $obj->ber_code;
            $this->point = (int)$obj->point;
            $this->avgRating = (float)$obj->avg_rating;
            $this->width = $obj->width;
            $this->height = $obj->height;
            $this->depth = $obj->depth;
            $this->weight = $obj->wight;
            foreach($obj->productPrices as $price)
            {
                $pPrice = new Price();
                $pPrice->castMeFromObj($price);
                array_push($this->prices,$pPrice);
            }
            foreach($obj->productPrices as $price)
            {
                $pPrice = new Price();
                $pPrice->castMeFromObj($price);
                array_push($this->prices,$pPrice);
            }
        }
    }

    public function castMeFromObjExtended($obj)
    {
        $this->id = $obj['id'];
        $this->code = $obj['product_code'];
        $this->title = $obj['product_title'];
        $this->description = $obj['product_description'];
        $this->productDescriptionMobile = $obj['product_description_mobile'];
        $this->url = $obj['product_url'];
        $this->barCode = $obj['bar_code'];
        $this->point = (int)$obj['point'];
        $this->avgRating = (int)$obj['avg_rating'];
        $this->width = $obj['width'];
        $this->height = $obj['height'];
        $this->depth = $obj['depth'];
        $this->weight = $obj['weight'];
        foreach($obj->productPrices as $price)
            {
                $pPrice = new Price();
                $pPrice->castMeFromObj($price);
                array_push($this->prices,$pPrice);
            }
        foreach($obj['productImages'] as $image)
        {
            //   var_dump($image);
            $pImage = new Picture();
            $pImage->castMeFromObj($image);
            array_push($this->pictures,$pImage);
        }
    }


} 