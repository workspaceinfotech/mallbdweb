<?php
/**
 * Project  : mallbdweb
 * File     : ProductCartFront.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 4/13/16 - 4:18 PM
 */
namespace App\Model\DataModel;

class ProductCartFront extends BaseDataModel{
    public $id;
    public $userId;
    public $cart;

    function __construct()
    {
        $this->id = 0;
        $this->userId=0;
        $this->cart = "";

    }

    public function castMe($obj)
    {
        if($obj!=null)
        {
            $this->id = (int)$obj->id;
            $this->userId = $obj->user_id;
            $this->cart = $obj->cart;

        }
    }

    public function castMeFromObj($obj)
    {
        if($obj!=null)
        {
            $this->id =(int) $obj['id'];
            $this->userId = $obj['user_id'];
            $this->cart = $obj['cart'];

        }
    }


}