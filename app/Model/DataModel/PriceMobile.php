<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: Price.php
 * User: rajib
 * Date: 1/14/16
 * Time: 3:09 PM
 */

namespace App\Model\DataModel;


class PriceMobile extends BaseDataModel{

    //public $id;
    //public $tax;
    public $purchasePrice;
    public $wholesalePrice;
    public $retailPrice;
    //public static $currency;
    //public $createdOn;

    function __construct()
    {
        //$this->id=0;
        //$this->tax = new Taxes();
        $this->purchasePrice=0.0;
        $this->wholesalePrice=0.0;
        $this->retailPrice = 0.0;
        //$this->createdOn="";
    }

    /*public static function setCurrency($currency){

        self::$currency = $currency;
        return TRUE;
    }*/

    public function castMe($obj)
    {
        if($obj!=null) {
            //$this->id = $obj->id;
            //$this->tax->castMe($obj->priceTax);
            $this->purchasePrice = (float)$obj->purchase_price * (float)self::getCurrency();
            $this->wholesalePrice = (float)$obj->wholesale_price * (float)self::getCurrency();
            $this->retailPrice = (float)$obj->retail_price * (float)self::getCurrency();
            //$this->createdOn = $obj->created_on;
        }
    }
    public function castMeFromObj($obj)
    {
       
        if($obj!=null) {
            //$this->id = $obj['id'];
            //$this->tax->castMeFromObj($obj['priceTax']);
            $this->purchasePrice = $obj['purchase_price'] * self::getCurrency();
            $this->wholesalePrice = $obj['wholesale_price'] * self::getCurrency();
            $this->retailPrice = $obj['retail_price'] * self::getCurrency();
            //$this->createdOn = $obj['created_on'];
        }
    }

} 