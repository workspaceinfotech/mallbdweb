<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: AppCredential.php
 * User: rajib
 * Date: 1/12/16
 * Time: 12:02 PM
 */

namespace App\Model\DataModel;

class AppCredential extends BaseDataModel{

    public $id;
    public $email;
    public $user;

    function __construct()
    {
        $this->email = "";
        $this->user = new User();
        $this->id = 0;
    }
    public function castMe($obj)
    {
        if($obj!=null){
            $this->id = (int) $obj->id;
            $this->email = $obj->email;
            $this->user->castMe($obj->user);
        }
    }
    public function castMeFromObj($obj)
    {

        if($obj!=null){
            $this->id = (int) $obj['id'];
            $this->email = $obj['email'];
            $this->user->castMe($obj->user);
        }
    }


}