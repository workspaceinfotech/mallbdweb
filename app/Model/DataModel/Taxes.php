<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: Taxes.php
 * User: rajib
 * Date: 1/14/16
 * Time: 1:15 PM
 */

namespace App\Model\DataModel;


class Taxes extends BaseDataModel{

    public $id;
    public $title;
    public $type;
    public $amount;
    public $createdOn;

    function __construct()
    {
        $this->id = 0;
        $this->title = "";
        $this->type = "";
        $this->amount = 0.0;
        $this->createdOn = "";
    }

    public function castMe($obj)
    {
        if($obj!=null) {
            $this->id = $obj->id;
            $this->title = $obj->title;
            $this->type = $obj->type;
            $this->amount = $obj->amount;
            $this->createdOn = $obj->created_on;
        }
    }

    public function castMeFromObj($obj)
    {
        if($obj!=null) {
            $this->id = $obj['id'];
            $this->title = $obj['title'];
            $this->type = $obj['type'];
            $this->amount = $obj['amount'];
            $this->createdOn = $obj['created_on'];
        }
    }


} 