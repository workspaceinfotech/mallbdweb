<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: Product.php
 * User: rajib
 * Date: 1/14/16
 * Time: 1:17 PM
 */

namespace App\Model\DataModel;
use App\Model\OrderModel;

class Order extends BaseDataModel{
    public $id;
    public $shopId;
    public $zoneId;
    public $invoiceNo;
    public $currencyCode;
    public $currencyValue;
    public $orderTotal;
    public $orderFrom;
    public $voucher_discount;
    public $discount_total;
    public $shipping_cost;
    public $employee_discount;
    public $onpurchase_discount;
    public $special_discount;
    public $packageSize;
    public $packageWeight;
    public $shippingAddress;
    public $shippingCountry;
    public $shippingZipCode;
    public $orderDate;
    public $isWrapped;
    public $wrappedNote;
    public $createdOn;
    public $createdBy;
    public $updatedOn;
    public $updatedBy;
    public $shop;
    public $customer;
    public $currency;
    public $zone;
    public $orderpayments;
    public $totalQuantity;
    public $orderStatus;

    function __construct()
    {
        $this->id = 0;
        $this->shopId=0;
        $this->zoneId=0;
        $this->invoiceNo = "";
        $this->currencyCode = "";
        $this->currencyValue = "";
        $this->orderTotal = "";
        $this->orderFrom = "";
        $this->voucher_discount = "";
        $this->discount_total = "";
        $this->shipping_cost = "";
        $this->onpurchase_discount="";
        $this->employee_discount = "";
        $this->special_discount = "";
        $this->packageSize = "";
        $this->packageWeight = "";
        $this->shippingAddress = "";
        $this->shippingCountry = "";
        $this->shippingZipCode = 0;
        $this->orderDate="";
        $this->isWrapped = "";
        $this->wrappedNote = "";
        $this->createdOn = "";
        $this->createdBy = "";
        $this->updatedOn = "";
        $this->updatedBy = "";
        $totalQuantity = 0;


        $this->orderProducts = [];

        $this->shop = new Shop();
        $this->customer = new User();
        $this->currency = new Currency();
        $this->zone = new Zone();
        $this->orderpayments = new OrderPayments();
        $this->orderStatus = new OrderStatus();

    }

    public function castMe($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj->id;
            $this->shopId = $obj->shop_id;
            $this->zoneId = $obj->zone_id;
            $this->invoiceNo = $obj->invoice_no;
            $this->currencyCode = $obj->currency_code;
            $this->currencyValue = $obj->currency_value;
            $this->orderTotal = $obj->order_total;
            $this->orderFrom = $obj->order_from;
            $this->voucher_discount = $obj->voucher_discount;
            $this->discount_total = $obj->discount_total;
            $this->shipping_cost = $obj->shipping_cost;
            $this->onpurchase_discount = $obj->onpurchase_discount;
            $this->employee_discount = $obj->employee_discount;
            $this->special_discount = $obj->special_discount;
            $this->packageSize = $obj->package_size;
            $this->packageWeight = $obj->package_weight;
            $this->shippingAddress = $obj->shipping_address;
            $this->shippingCountry = $obj->shipping_country;
            $this->shippingZipCode = $obj->shipping_zipcode;
            $this->orderDate=$obj->order_date;
            $this->isWrapped = $obj->is_wrapped;
            $this->wrappedNote = $obj->wrapped_note;
            $this->createdOn = $obj->created_on;
            $this->createdBy = $obj->created_by;
            $this->updatedOn = $obj->updated_on;
            $this->updatedBy = $obj->updated_by;

            $this->shop->castMe($obj->orderShop);

            $this->customer->castMe($obj->orderCustomer);

            $this->currency->castMe($obj->orderCurrency);

            $this->zone->castMe($obj->orderZone);
            
            $this->orderpayments->castMe($obj->orderPayments);

            foreach ($obj->orderOrderProducts as $orderProduct) {
                $oProduct = new OrderProducts();
                $oProduct->castMeFromObj($orderProduct);
                $this->totalQuantity +=$oProduct->productQuantity;
                array_push($this->orderProducts, $oProduct);
            }

            //$this->orderStatus->castMe($obj->orderStatus);

        }
    }

    public function castMeFromObj($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj['id'];
            $this->shopId = $obj['shop_id'];
            $this->zoneId = $obj['zone_id'];
            $this->invoiceNo = $obj['invoice_no'];
            $this->currencyCode = $obj['currency_code'];
            $this->currencyValue = $obj['currency_value'];
            $this->orderTotal = $obj['order_total'];
            $this->orderFrom = $obj['order_from'];
            $this->voucher_discount = $obj['voucher_discount'];
            $this->discount_total = $obj['discount_total'];
            $this->shipping_cost = $obj['shipping_cost'];            
            $this->onpurchase_discount = $obj['onpurchase_discount'];
            $this->employee_discount = $obj['employee_discount'];
            $this->special_discount = $obj['special_discount'];
            $this->packageSize = $obj['package_size'];
            $this->packageWeight = $obj['package_weight'];
            $this->shippingAddress = $obj['shipping_address'];
            $this->shippingCountry = $obj['shipping_country'];
            $this->shippingZipCode = $obj['shipping_zipcode'];
            $this->orderDate=$obj['order_date'];
            $this->isWrapped = $obj['is_wrapped'];
            $this->wrappedNote = $obj['wrapped_note'];
            $this->createdOn = $obj['created_on'];
            $this->createdBy = $obj['created_by'];
            $this->updatedOn = $obj['updated_on'];
            $this->updatedBy = $obj['updated_by'];

            $this->shop->castMeFromObj($obj['orderShop']);
            $this->customer->castMeFromObject($obj['orderCustomer']);
            $this->currency->castMeFromObj($obj['orderCurrency']);
            $this->zone->castMeFromObj($obj['orderZone']);
            $this->orderpayments->castMe($obj['orderPayments']);
            $this->orderStatus->castMe($obj['orderStatus']);
            
            foreach ($obj['orderOrderProducts'] as $orderProduct) {
                $oProduct = new OrderProducts();
                $oProduct->castMeFromObj($orderProduct);
                $this->totalQuantity +=$oProduct->productQuantity;
                array_push($this->orderProducts, $oProduct);
            }

           // $this->orderStatus->castMeFromObj($obj['orderStatus']);

        }
    }
    public function castMeFromObjFormobileOrders($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj['id'];
            //$this->shopId = $obj['shop_id'];
            //$this->zoneId = $obj['zone_id'];
            $this->invoiceNo = $obj['invoice_no'];
            $this->currencyCode = $obj['currency_code'];
            $this->currencyValue = $obj['currency_value'];
            $this->orderTotal = (float)$obj['order_total'];
            $this->orderFrom = $obj['order_from'];
            $this->voucher_discount = (float)$obj['voucher_discount'];
            $this->discount_total = (float)$obj['discount_total'];
            $this->shipping_cost = (float)$obj['shipping_cost'];                       
            $this->employee_discount = (float)$obj['employee_discount'];
            $this->onpurchase_discount = (float)$obj['onpurchase_discount'];
            $this->special_discount = (float)$obj['special_discount'];
            //$this->packageSize = $obj['package_size'];
//            $this->packageWeight = $obj['package_weight'];
            $this->shippingAddress = $obj['shipping_address'];
            $this->shippingCountry = $obj['shipping_country'];
            $this->shippingZipCode = $obj['shipping_zipcode'];
            $this->orderDate=date('jS F Y',strtotime($obj['order_date']));
//            $this->isWrapped = $obj['is_wrapped'];
//            $this->wrappedNote = $obj['wrapped_note'];
//            $this->createdOn = $obj['created_on'];
//            $this->createdBy = $obj['created_by'];
//            $this->updatedOn = $obj['updated_on'];
//            $this->updatedBy = $obj['updated_by'];

//            $this->shop->castMeFromObj($obj['orderShop']);
//            $this->customer->castMeFromObject($obj['orderCustomer']);
//            $this->currency->castMeFromObj($obj['orderCurrency']);
//            $this->zone->castMeFromObj($obj['orderZone']);
            $this->orderpayments->castMe($obj['orderPayments']);
            
            foreach ($obj['orderOrderProducts'] as $orderProduct) {
                $oProduct = new OrderProducts();
                $oProduct->castMeFromObjMobile($orderProduct);
                $this->totalQuantity +=$oProduct->productQuantity;
                if($oProduct->packageId == 0){
                    array_push($this->orderProducts, $oProduct);
                }
            }

            //$this->orderStatus->castMeFromObj($obj['orderStatus']);

        }
    }


}