<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: Shop.php
 * User: rajib
 * Date: 1/13/16
 * Time: 6:10 PM
 */

namespace App\Model\DataModel;



class Shop extends BaseDataModel{

    public $id;
    public $name;
    public $description;
    public $createdBy;
    public $createdOn;

    function __construct()
    {
        $this->name = "";
        $this->id = 0;
        $this->description = "";
        $this->createdBy = new User();
        $this->createdOn = "";
    }
    public function castMe($obj)
    {
       if($obj!=null)
       {
           $this->id = $obj->id;
           $this->name = $obj->shop_name;
           $this->description = $obj->shop_description;
           $this->createdBy->castMe($obj->createdBy);
           $this->createdOn = $obj->create_on;
       }
    }
    public function castMeFromObj($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj['id'];
            $this->name = $obj['shop_name'];
            $this->createdBy->castMe($obj->createdBy);
            $this->description = $obj['shop_description'];
            $this->createdOn = $obj['create_on'];
        }

    }


} 