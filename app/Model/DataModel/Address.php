<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: Address.php
 * User: rajib
 * Date: 1/13/16
 * Time: 1:35 PM
 */

namespace App\Model\DataModel;


class Address extends BaseDataModel{

    public $country;
    public  $city;
    public $zipCode;
    public $address;

    function __construct()
    {
        $this->address = "";
        $this->city = "";
        $this->country = "";
        $this->zipCode = "";
    }

    public function castMe($obj)
    {
        if($obj!=null) {
            $this->country = $obj->country;
            $this->city = $obj->city;
            $this->zipCode = $obj->zipcode;
            $this->address = $obj->address;
        }
    }

    public function castMeFromObj($obj)
    {
        if($obj!=null) {
            $this->country = $obj['country'];
            $this->city = $obj['city'];
            $this->zipCode = $obj['zipcode'];
            $this->address = $obj['address'];
        }
    }


} 