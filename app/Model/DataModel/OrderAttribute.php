<?php
/**
 * Project  : mallbdweb
 * File     : OrderAttribute.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 2/24/16 - 12:40 PM
 */

namespace App\Model\DataModel;
use App\Model\OrderAttributeModel;

class OrderAttribute extends BaseDataModel{

    public $id;
    public $orderId;
    public $productId;
    public $combinationKey;
    public $quantity;
    public $createdOn;

    function __construct()
    {
        $this->id=0;
        $this->orderId = 0;
        $this->productId= 0;
        $this->combinationKey =0;
        $this->quantity = 0;
        $this->createdOn = 0;
    }

    public function castMe($obj)
    {
        if($obj!=null) {
            $this->id = $obj->id;
            $this->orderId = $obj->order_id;
            $this->productId = $obj->product_id;
            $this->combinationKey = $obj->combination_key;
            $this->quantity = $obj->quantity;
            $this->createdOn = $obj->created_on;
        }
    }
    public function castMeFromObj($obj)
    {
        if($obj!=null) {
            $this->id = $obj['id'];
            $this->orderId = $obj['order_id'];
            $this->productId = $obj['product_id'];
            $this->combinationKey = $obj['combination_key'];
            $this->quantity = $obj['quantity'];
            $this->createdOn = $obj['created_on'];
        }
    }

}