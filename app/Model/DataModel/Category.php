<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: Category.php
 * User: rajib
 * Date: 1/13/16
 * Time: 6:06 PM
 */

namespace App\Model\DataModel;

use App\Model\CategoryModel;

class Category extends BaseDataModel{

    public $id;
    public $title;
    public $shop;
    public $icon;
    public $banner;
    public $metaTitle;
    public $metaDescription;
    public $createdOn;
    public $childrens;

    function __construct()
    {
        $this->createdOn = "";
        $this->icon = "";
        $this->id = 0;
        $this->banner="";
        $this->metaDescription = "";
        $this->metaTitle = "";
        $this->shop = new Shop();
        $this->title = "";
        $this->childrens = [];
    }

    public function castMe($obj){

        if($obj!=null)
        {
            $this->id = $obj->id;
            $this->title = $obj->title;
            $this->icon = $obj->icon;
            $this->banner = $obj->banner;
            if(@$obj->meta_title!=null)
            {
                $this->metaTitle = $obj->meta_title;
            }
            if(@$obj->meta_description!=null) {
                $this->metaDescription = $obj->meta_description;
            }
            if(@$obj->created_on!=null)
            {
                $this->createdOn = $obj->created_on;
            }
            if(@$obj->shop!=null)
            {
                $this->shop->castMe($obj->shop);
            }
            if(@$obj->children!=null) {

                foreach ($obj->children as $child) {
                    $childObj = new Category();
                    $childObj->castMe($child);
                    array_push($this->childrens, $childObj);
                }
            }
        }
    }

    public function castMeFromObj($obj){

        if($obj!=null)
        {
            $this->id = $obj['id'];
            $this->title = $obj['title'];
            $this->icon = $obj['icon'];
            $this->banner = $obj['banner'];
            $this->metaTitle = $obj['meta_title'];
            $this->metaDescription = $obj['meta_description'];
            $this->createdOn = $obj['created_on'];
            $this->shop->castMe($obj['shop']);
            foreach($obj['children'] as $child)
            {
                $childObj = new Category();
                $childObj->castMe($child);
                array_push($this->childrens,$childObj);
            }
        }
    }


}