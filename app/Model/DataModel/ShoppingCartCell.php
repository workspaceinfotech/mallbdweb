<?php
/**
 * Created by PhpStorm.
 * User: mi
 * Date: 2/23/16
 * Time: 2:12 PM
 */

namespace App\Model\DataModel;


class ShoppingCartCell extends BaseDataModel
{
    public $id;
    public $quantity;
    public $product; // Array of Product class
    public $selectedAttributes;

    /**
     * ProductCell constructor.
     * @param $id
     * @param $quantity
     * @param $product
     * @param $selectedAttributes
     */
    public function __construct()
    {
        $this->id = 0;
        $this->quantity = 0;
        $this->product = new Product();
        $this->selectedAttributes = [];
    }
    public function castMe($obj){
        $this->id = $obj->id;
        $this->quantity = $obj->quantity;
        $this->product = $obj->product;
        $this->selectedAttributes = $obj->selectedAttributes;
    }


}