<?php
/**
 * Project  : mallbdweb
 * File     : OrderStatusList.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 3/29/16 - 3:17 PM
 */
namespace App\Model\DataModel;

class OrderStatusList extends BaseDataModel{

    public $id;
    public $statusName;

    function __construct()
    {
        $this->id = 0;
        $this->statusName="";
    }

    public function castMe($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj->id;
            $this->statusName = $obj->status_name;
        }
    }

    public function castMeFromObj($obj) {

        if ($obj != NULL)
        {
            $this->id         = $obj['id'];
            $this->statusName = $obj['status_name'];
        }
    }
}