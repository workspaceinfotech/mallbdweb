<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: Zone.php
 * User: rajib
 * Date: 1/14/16
 * Time: 1:14 PM
 */

namespace App\Model\DataModel;


class Zone extends BaseDataModel{

    public $id;
    public $code;
    public $city;
    public $area;
    public $createdOn;

    function __construct()
    {
        $this->id        = 0;
        $this->code      = "";
        $this->city      = "";
        $this->area      = "";
        $this->createdOn = "";
    }

    public function castMe($obj)
    {
        if($obj!=null) {
            $this->id = $obj->id;
            $this->code = $obj->code;
            $this->city = $obj->city;
            $this->area = $obj->area;
            $this->createdOn = $obj->createdOn;
        }
    }

    public function castMeFromObj($obj)
    {
        if($obj!=null) {
            $this->id = $obj['id'];
            $this->code = $obj['zone_code'];
            $this->city = $obj['zone_city'];
            $this->area = $obj['zone_area'];
            $this->createdOn = $obj['created_on'];
        }
    }



} 