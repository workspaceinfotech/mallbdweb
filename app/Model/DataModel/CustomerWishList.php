<?php
/**
 * Created by PhpStorm.
 * Project: MallBDFront
 * File: CustomerWishList.php
 * User: rajib
 * Date: 2/4/16
 * Time: 3:07 PM
 */

namespace App\Model\DataModel;


class CustomerWishList extends BaseDataModel{

    public $id;
    public $customerId;
    public $products;
    public $createdOn;
    public static $currency;
    
    function __construct()
    {
        $this->id = 0;
        $this->customerId = 0;
        $this->products = [];
        $this->createdOn = "";
    }
    public static function setCurrency($currency){

            self::$currency = $currency;
            /*var_dump(self::$currency);
            die();*/
            return true;
        }
    public function castMe($obj)
    {
        $this->id = $obj->id;
        $this->customerId = $obj->customer_id;
        foreach($obj->products as $product){
            $productObj = new Product();
            $productObj->castMe($product);
            array_push($this->products,$productObj);
        }
        $this->createdOn = $obj->created_on;

    }
    public function castMeFromObj($obj)
    {
        $this->id = $obj['id'];
        $this->customerId = $obj['customer_id'];
        foreach($obj['products'] as $product){
            $productObj = new Product();
            //$productObj->setCurrency(self::$currency);
            $productObj->castMe($product);
            array_push($this->products,$productObj);
        }
        $this->createdOn = $obj['created_on'];

    }


} 