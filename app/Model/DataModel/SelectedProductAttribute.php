<?php
/**
 * Created by PhpStorm.
 * User: mi
 * Date: 4/12/16
 * Time: 1:19 PM
 */

namespace App\Model\DataModel;


class SelectedProductAttribute extends BaseDataModel
{
    public $selectedAttribute;
    public $price;
    public $quantity;
    /**
     * SelectedProductAttribute constructor.
     * @param $selectedAttribute
     */
    public function __construct()
    {
        $this->selectedAttribute = [];
        $this->price = 0;
        $this->quantity = 0;
    }

}