<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: Zone.php
 * User: rajib
 * Date: 1/14/16
 * Time: 1:14 PM
 */

namespace App\Model\DataModel;


class District extends BaseDataModel{

    public $name;

    function __construct()
    {
        $this->name      = "";
    }

    public function castMe($obj)
    {
        if($obj!=null) {
            $this->name = $obj->name;
        }
    }

    public function castMeFromObj($obj)
    {
        if($obj!=null) {
            $this->name = $obj['name'];
        }
    }



} 