<?php
/**
 * Project  : mallbdweb
 * File     : CustomerReward.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 2/15/16 - 1:41 PM
 */
namespace App\Model\DataModel;

class PaymentMethods extends BaseDataModel{
    public $id;
    public $methodTitle;
    public $description;
    public $icon;
    public $createdOn;
    public $createdBy;
    public $updatedOn;
    public $updatedBy;

    function __construct()
    {
        $this->id = 0;
        $this->methodTitle="";
        $this->description = "";
        $this->icon = "";
        $this->createdOn = "";
        $this->createdBy =  "";
        $this->updatedOn = "";
        $this->updatedBy = "";

    }

    public function castMe($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj->id;
            $this->methodTitle = $obj->method_title;
            $this->description = $obj->description;
            $this->icon = $obj->icon;
            $this->createdOn = $obj->created_on;
            $this->createdBy = $obj->created_by;
            $this->updatedOn = $obj->updated_on;
            $this->updatedBy= $obj->updated_by;

        }
    }

    public function castMeFromObj($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj['id'];
            $this->methodTitle = $obj['method_title'];
            $this->description = $obj['description'];
            $this->icon = $obj['icon'];
            $this->createdOn = $obj['created_on'];
            $this->createdBy = $obj['created_by'];
            $this->updatedOn = $obj['updated_on'];
            $this->updatedBy = $obj['updated_by'];

        }
    }


}