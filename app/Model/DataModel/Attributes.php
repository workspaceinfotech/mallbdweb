<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: Attributes.php
 * User: rajib
 * Date: 1/14/16
 * Time: 1:16 PM
 */

namespace App\Model\DataModel;


class Attributes extends BaseDataModel{

    public $id;
    public $name;
    public $attributesValue;
    public $createdOn;

    function __construct()
    {
        $this->id = 0;
        $this->name = "";
        $this->attributesValue = [];
        $this->createdOn = "";
    }

    public function castMe($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj->id;
            $this->name = $obj->attribute_name;
            $this->createdOn = $obj->created_on;

            foreach($obj->attributesValues as $value)
            {
                $val = new AttributesValue();
                $val->castMe($value);
                array_push($this->attributesValue,$val);
            }

        }
    }

    public function castMeFromObj($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj['id'];
            $this->name = $obj['attribute_name'];
            $this->createdOn = $obj['created_on'];

            foreach($obj['attributesValues'] as $value)
            {
                $val = new AttributesValue();
                $val->castMeFromObj($value);
                array_push($this->attributesValue,$val);
            }

        }
    }


} 