<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: Tags.php
 * User: rajib
 * Date: 1/14/16
 * Time: 1:13 PM
 */

namespace App\Model\DataModel;


class TagsMobile extends BaseDataModel{

    //public $id;
    public $title;
    //public $createdOn;
    //public $shop;

    function __construct()
    {
        //$this->id = 0;
        $this->title = "";
        //$this->createdOn = "";
        //$this->shop = new Shop();
    }

    public function castMe($obj)
    {
        if($obj!=null)
        {
            //$this->id = $obj->id;
            $this->title = $obj->tag_name;
            //$this->createdOn = $obj->created_on;
           // $this->shop->castMe($obj->tagShop);
        }
    }
    public function castMeFromObj($obj)
    {
        if($obj!=null)
        {
            //$this->id = $obj['id'];
            $this->title = $obj['tag_name'];
           // $this->createdOn = $obj['created_on'];
            //$this->shop->castMe($obj['tagShop']);
        }
    }


} 