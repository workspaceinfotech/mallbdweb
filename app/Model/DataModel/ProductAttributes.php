<?php
/**
 * Project  : mallbdweb
 * File     : CustomerReward.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 2/15/16 - 1:41 PM
 */
namespace App\Model\DataModel;

class ProductAttributes extends BaseDataModel{
    public $id;
    public $createdOn;
    public $createdBy;
    public $updatedOn;
    public $updatedBy;
    public $product;
    public $attributes;


    function __construct()
    {
        $this->id = 0;
        $this->createdOn = "";
        $this->createdBy =  "";
        $this->updatedOn = "";
        $this->updatedBy = "";
        $this->product = new Product();
        $this->attributes = new Attributes();

    }

    public function castMe($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj->id;
            $this->createdOn = $obj->created_on;
            $this->createdBy = $obj->created_by;
            $this->updatedOn = $obj->updated_on;
            $this->updatedBy= $obj->updated_by;

            $this->product->castMe($obj->productAttrProduct);
            $this->attributes->castMe($obj->productAttrAttributes);

        }
    }

    public function castMeFromObj($obj)
    {
        if($obj!=null)
        {
            $this->id = $obj['id'];
            $this->createdOn = $obj['created_on'];
            $this->createdBy = $obj['created_by'];
            $this->updatedOn = $obj['updated_on'];
            $this->updatedBy = $obj['updated_by'];

            $this->product->castMeFromObj($obj['productAttrProduct']);
            $this->attributes->castMeFromObj($obj['productAttrAttributes']);
        }
    }


}