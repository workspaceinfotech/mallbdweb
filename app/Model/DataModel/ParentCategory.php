<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: ParentCategory.php
 * User: rajib
 * Date: 1/14/16
 * Time: 12:39 PM
 */

namespace App\Model\DataModel;


class ParentCategory extends BaseDataModel{

    public $id;
    public $title;
    public $shop;
    public $parent;
    public $icon;
    public $metaTitle;
    public $metaDescription;
    public $createdOn;

    function __construct()
    {
        $this->createdOn = "";
        $this->icon = "";
        $this->id = 0;
        $this->metaDescription = "";
        $this->metaTitle = "";
        $this->parent = "";
        $this->shop = new Shop();
        $this->title = "";
    }

    public function castMe($obj){

        if($obj!=null) {
            $this->id = $obj->id;
            $this->title = $obj->title;
            $this->icon = $obj->icon;
            $this->metaTitle = $obj->meta_title;
            $this->metaDescription = $obj->meta_description;
            $this->createdOn = $obj->created_on;
            $this->shop->castMe($obj->shop);

            $this->parent = $obj->parent;
        }
    }
    public function castMeFromObj($obj)
    {
        if($obj!=null) {
            $this->id = $obj['id'];
            $this->title = $obj['title'];
            $this->icon = $obj['icon'];
            $this->metaTitle = $obj['meta_title'];
            $this->metaDescription = $obj['meta_description'];
            $this->createdOn = $obj['created_on'];
            $this->shop->castMe($obj['shop']);
            $this->parent = $obj['parent'];
        }
    }
} 