<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class StaticPagesModel extends BaseMallBDModel {

    protected $table = 'static_pages';
    protected $primaryKey = 'id';
    public $timestamps = FALSE;
    protected $fillable = array();

}
