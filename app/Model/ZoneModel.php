<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: ZoneModel.php
 * User: rajib
 * Date: 1/14/16
 * Time: 3:45 PM
 */

namespace App\Model;
use App\Model\DataModel\Zone;

class ZoneModel extends BaseMallBDModel{

    protected $primaryKey = 'id';
    protected $table = 'zones';

    public function  setId($id){
        $this->setObj($id);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "zone_id";
            $errorObj->msg = "Select Zone";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "zone_id";
            $errorObj->msg = "Zone Id int required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }

        if(intval($this->getObj()) <=0){
            $errorObj = new ErrorObj();

            $errorObj->params = "zone_id";
            $errorObj->msg = "Select Zone";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }

        $this->id = $this->getObj();
        return TRUE;
    }
    /**
     * @param mixed $zone_code
     */
    public function setZoneCode($zone_code) {

        $this->setObj($zone_code);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "zone_code";
            $errorObj->msg = "Zone Code is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }

        $this->zone_code = $zone_code;
        return true;
    }

    /**
     * @param mixed $zone_city
     */
    public function setZoneCity($zone_city) {

        $this->setObj($zone_city);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "zone_city";
            $errorObj->msg = "Zone City is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }

        $this->zone_city = $zone_city;
        return TRUE;
    }


    /**
     * @param mixed $zone_area
     */
    public function setZoneArea($zone_area) {

        $this->setObj($zone_area);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "zone_area";
            $errorObj->msg = "Zone Area is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }

        $this->zone_area = $zone_area;
        return TRUE;
    }

    /**
     * @param mixed $created_on
     */
    public function setCreatedOn($created_on) {
        $this->created_on = $created_on;
    }




    public function getAllZones()
    {
        $zones = ZoneModel::where("status","=","Active")->get();

        if($zones==null)
        {
            return null;
        }
        $zoneList = [];

        foreach($zones as $z)
        {
            $zone = new Zone();
            $zone->castMeFromObj($z);
            array_push($zoneList,$zone);
        }
        return $zoneList;
    }
    public function isZoneExist()
    {
        $count = $this->where('id','=',$this->id)->limit(1)->get()->count();

        if($count>0)
            return true;
        return false;
    }

} 