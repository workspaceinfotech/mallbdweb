<?php

/**
 * Project  : mallbdweb
 * File     : NewsletterModel.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 3/24/16 - 1:09 PM
 */
namespace App\Model;

class NewsletterModel extends BaseMallBDModel {

    protected $primaryKey = 'id';
    protected $table = 'newsletters';

    public function setEmail($email) {

        $this->setObj($email);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "email";
            $errorObj->msg = "Email is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }

        $this->email = $this->getObj();
        return TRUE;
    }



}