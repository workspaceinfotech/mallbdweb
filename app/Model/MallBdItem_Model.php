<?php
/**
 * Created by PhpStorm.
 * User: mi
 * Date: 5/2/16
 * Time: 4:00 PM
 */

namespace App\Model;


use App\Model\DataModel\MallBdItem;

class MallBdItem_Model extends BaseMallBDModel
{
    public function getItem($products=null,$packages=null,$limit=5,$offset=0){
        $productModel = new ProductModel();
        $packageModel = new PackageModel();

        $productModel->setCustomLimit($limit);
        $productModel->setCustomOffset($offset);

        $productList = [];
        $packageList = [];
        if($products==null)
            $productList = $productModel->getAllProducts(1);
        else
            $productList = $products;

        $mallBdItemList = [];
        foreach($productList as $product){

            $mallBdItem = new MallBdItem();

            $mallBdItem->setProduct($product);
            array_push($mallBdItemList,$mallBdItem);
        }

        $packageModel->setCustomLimit($limit);
        $packageModel->setCustomOffset($offset);

        if($packages==null)
            $packageList = $packageModel->getAll();
        else
            $packageList = $packages;

        foreach($packageList as $package){

            $mallBdItem = new MallBdItem();

            $mallBdItem->setPackage($package);
            array_push($mallBdItemList,$mallBdItem);
        }

        usort($mallBdItemList, function($item1, $item2)
        {
            return strcmp( $item2->createdOn,$item1->createdOn);
        });

        return $mallBdItemList;
    }
}