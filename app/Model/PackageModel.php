<?php

/**
 * Project  : mallbdweb
 * File     : PackageModel.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 4/6/16 - 11:29 AM
 */

namespace App\Model;

use App\Model\DataModel\Package;
use App\Model\DataModel\PackageMobile;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\DB;

class PackageModel extends BaseMallBDModel {

    protected $primaryKey = 'id';
    protected $table = 'packages';

    /**
     * @param mixed $package_title
     */
    function setId($id) {

        $this->setObj($id);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "id";
            $errorObj->msg = "id is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        if (!is_numeric($this->getObj())) {
            $errorObj = new ErrorObj();

            $errorObj->params = "id";
            $errorObj->msg = "id int/float required";


            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        $this->id = (int) $this->getObj();
        return true;
    }

    public function setPackageTitle($package_title) {

        $this->setObj($package_title);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "package_title";
            $errorObj->msg = "Package Title is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }

        $this->package_title = $this->getObj();
        return true;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description) {


        $this->setObj($description);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "description";
            $errorObj->msg = "Description is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        $this->description = $this->getObj();
        return true;
    }

    /**
     * @param mixed $start_date
     */
    public function setStartDate($start_date) {

        $this->setObj($start_date);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "start_date";
            $errorObj->msg = "Start Date is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        $this->start_date = $this->getObj();
        return true;
    }

    /**
     * @param mixed $end_date
     */
    public function setEndDate($end_date) {

        $this->setObj($end_date);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "end_date";
            $errorObj->msg = "End Date is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        $this->end_date = $this->getObj();
        return true;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image) {

        $this->setObj($image);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "image";
            $errorObj->msg = "Image is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        $this->image = $this->getObj();
        return true;
    }

    /**
     * @param mixed $original_price_total
     */
    public function setOriginalPriceTotal($original_price_total) {

        $this->setObj($original_price_total);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "original_price_total";
            $errorObj->msg = "Original Price Total is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        if (!is_numeric($this->getObj())) {
            $errorObj = new ErrorObj();

            $errorObj->params = "original_price_total";
            $errorObj->msg = "Original Price Total int required";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        $this->original_price_total = $this->getObj();
        return true;
    }

    /**
     * @param mixed $package_price_total
     */
    public function setPackagePriceTotal($package_price_total) {

        $this->setObj($package_price_total);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "package_price_total";
            $errorObj->msg = "Package Price Total is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        if (!is_numeric($this->getObj())) {
            $errorObj = new ErrorObj();

            $errorObj->params = "original_price_total";
            $errorObj->msg = "Original Price Total int required";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        $this->package_price_total = $this->getObj();
        return true;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status) {

        $this->setObj($status);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "status";
            $errorObj->msg = "Status is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        $this->status = $this->getObj();
        return true;
    }

    /**
     * @param mixed $created_on
     */
    public function setCreatedOn($created_on) {
        $this->created_on = $created_on;
    }

    /* -------------END of Setters------------------- */

    public function packageProduct() {
        return $this->hasMany("App\Model\PackageProductModel", "package_id", "id");
    }

    public function getAll() {
        $toDay = date('Y-m-d');
        $result = $this->with("packageProduct")
                ->where('start_date', '<=', $toDay)
                ->where('end_date', '>=', $toDay)
                ->where("status", "=", "Active")
                ->where("shop_id", "=", $this->shopId)
                ->orderBy('id', 'DESC')
                ->limit($this->customLimit)->offset($this->customOffset * $this->customLimit)
                ->get();
        /* $query = DB::getQueryLog();
          var_dump($query);
          die(); */
        $packageList = [];

        foreach ($result as $r) {
            $packages = new Package();
            //$packages->setCurrency(self::$currency);
            $packages->castMe($r);
            array_push($packageList, $packages);
        }
        return $packageList;
    }
    public function getAllMobile() {
        $toDay = date('Y-m-d');
        $result = $this->with("packageProduct")
                ->where('start_date', '<=', $toDay)
                ->where('end_date', '>=', $toDay)
                ->where("status", "=", "Active")
                ->where("shop_id", "=", $this->shopId)
                ->orderBy('id', 'DESC')
                ->limit($this->customLimit)->offset($this->customOffset * $this->customLimit)
                ->get();
        $packageList = [];

        foreach ($result as $r) {
            $packages = new PackageMobile();
            //$packages->setCurrency(self::$currency);
            $packages->castMe($r);
            array_push($packageList, $packages);
        }
        return $packageList;
    }
    public function getAlltoCompare() {
        $toDay = date('Y-m-d');
        $result = $this->with("packageProduct")
                ->where('start_date', '<=', $toDay)
                ->where('end_date', '>=', $toDay)
                ->where("status", "=", "Active")
                ->where("shop_id", "=", $this->shopId)
                ->orderBy('id', 'DESC')
                ->get();
        $packageList = [];

        foreach ($result as $r) {
            $packages = new Package();
            //$packages->setCurrency(self::$currency);
            $packages->castMe($r);
            array_push($packageList, $packages);
        }
        return $packageList;
    }

    public function getPackageById() {

        $result = $this->where("id", "=", $this->id)->with("packageProduct")->get()->first();

        //return $result;
        $packages = new Package();
        //$packages->setCurrency(self::$currency);
        $packages->castMe($result);
        return $packages;
    }
    public function getPackageByIdMobile() {

        $result = $this->where("id", "=", $this->id)->with("packageProduct")->get()->first();

        $packages = new PackageMobile();
        $packages->castMe($result);
        return $packages;
    }
    
    public function getPackageByIdAndTitle($title) {
        $title = urldecode($title);
        $result = $this->where("id", "=", $this->id)->where("package_title", "=", $title)->with("packageProduct")->get()->first();

        //return $result;
        $packages = new Package();
        //$packages->setCurrency(self::$currency);
        $packages->castMe($result);
        return $packages;
    }

    public function getPackageByKeyword($keyword) {
        $result = $this->where("status", "=", "Active")
                ->where("shop_id", "=", $this->shopId)
                ->where("package_title", "LIKE", '%' . $keyword . '%')
//                ->limit($this->customLimit)
//                ->offset($this->customOffset * $this->customLimit)
                ->get();

        $packageList = [];

        foreach ($result as $r) {
            $package = new \stdClass();
            $package->id = $r->id;
            $package->encoded_title = urlencode($r->package_title);
            $package->title = $r->package_title;
            $package->product_title = strlen($r->description) > 20 ? substr($r->description, 0, 19) . '...' : $r->description;
            $package->package = TRUE;
            array_push($packageList, $package);
        }
        return $packageList;
    }

    public function allPackageCount() {

        $packageCount = $this->where("status", "=", "Active")->where("shop_id", "=", $this->shopId)->count();
        return $packageCount;
    }

    public function getFilteredPackage($minPrice = -1, $maxPrice = -1, $orderBy = 0) {
//        if ($minPrice > -1 || $maxPrice > 0) {
////            $queryObj->whereHas('productPrices', function ($q) use ($minPrice, $maxPrice) {
//
//                $queryObj->where(DB::raw('product_prices.retail_price - products.discount'), '>=', doubleval($minPrice))->where(DB::raw('product_prices.retail_price - products.discount'), '<=', doubleval($maxPrice));
////            });
//        }
        //DB::connection()->enableQueryLog();
         $toDay = date('Y-m-d');
        $queryObj = $this->with("packageProduct")->where("status", "=", "Active")->where("shop_id", "=", $this->shopId)->where('start_date', '<=', $toDay)->where('end_date', '>=', $toDay);
        if ($orderBy == 1) {
            $queryObj = $queryObj->orderBy('package_price_total', 'asc');
        }
        if ($orderBy == 2) {
            $queryObj = $queryObj->orderBy('package_price_total', 'desc');
        }
        if ($orderBy == 3) {
            $queryObj = $queryObj->orderBy('package_title', 'asc');
        }
        if ($orderBy == 4) {
            $queryObj = $queryObj->orderBy('package_title', 'desc');
        }
        $queryObj = $queryObj->orderBy('id', 'DESC')->limit($this->customLimit)->offset($this->customOffset * $this->customLimit);

        $result = $queryObj->get();
        /* $query = DB::getQueryLog();
          var_dump($query);
          die(); */
        $packageList = [];

        foreach ($result as $r) {
            $packages = new Package();
            //$packages->setCurrency(self::$currency);
            $packages->castMe($r);
            array_push($packageList, $packages);
        }
        return $packageList;
    }

    public function getFilteredPackageCount($minPrice = -1, $maxPrice = -1, $orderBy = 0) {
//        if ($minPrice > -1 || $maxPrice > 0) {
////            $queryObj->whereHas('productPrices', function ($q) use ($minPrice, $maxPrice) {
//
//                $queryObj->where(DB::raw('product_prices.retail_price - products.discount'), '>=', doubleval($minPrice))->where(DB::raw('product_prices.retail_price - products.discount'), '<=', doubleval($maxPrice));
////            });
//        }
        //DB::connection()->enableQueryLog();
        $toDay = date('Y-m-d');
        $queryObj = $this->with("packageProduct")->where("status", "=", "Active")->where("shop_id", "=", $this->shopId)->where('start_date', '<=', $toDay)->where('end_date', '>=', $toDay);
        if ($orderBy == 1) {
            $queryObj = $queryObj->orderBy('package_price_total', 'asc');
        }
        if ($orderBy == 2) {
            $queryObj = $queryObj->orderBy('package_price_total', 'desc');
        }
        if ($orderBy == 3) {
            $queryObj = $queryObj->orderBy('package_title', 'asc');
        }
        if ($orderBy == 4) {
            $queryObj = $queryObj->orderBy('package_title', 'desc');
        }
        $queryObj = $queryObj->orderBy('id', 'DESC')->limit($this->customLimit)->offset($this->customOffset * $this->customLimit);

        $result = $queryObj->get();
        /* $query = DB::getQueryLog();
          var_dump($query);
          die(); */
        $packageList = [];

        foreach ($result as $r) {
            $packages = new Package();
            //$packages->setCurrency(self::$currency);
            $packages->castMe($r);
            array_push($packageList, $packages);
        }
        return count($packageList);
    }

}
