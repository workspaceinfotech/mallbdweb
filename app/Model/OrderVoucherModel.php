<?php

/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: ProductModel.php
 * User: rafsan
 * Date: 1/14/16
 * Time: 3:29 PM
 */

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;



class OrderVoucherModel extends BaseMallBDModel {

    protected $primaryKey = 'id';
    protected $table      = 'order_vouchers';
    protected $fillable = array(
        'voucher_id',
        'order_id',
    );

}