<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: WareHouseModel.php
 * User: rajib
 * Date: 1/14/16
 * Time: 3:44 PM
 */

namespace App\Model;


use App\Model\DataModel\WareHouse;

class WareHouseModel extends BaseMallBDModel{

    protected $primaryKey = 'id';
    protected $table = 'warehouses';

    public function wareHouseManager(){
        return $this->hasOne("App\Model\UserModel","id","manager");
    }
    public function setId($id){
        $this->id = $id;
    }
    public function getById(){

        $dataRow = $this->with("wareHouseManager")->where("id","=",$this->id)->get();
        $wareHouse = new WareHouse();
        foreach($dataRow as $row){
            $wareHouse->castMe($row);
        }
        return $wareHouse;
    }

} 