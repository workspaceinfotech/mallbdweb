<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: ProductModel.php
 * User: rajib
 * Date: 1/14/16
 * Time: 3:29 PM
 */

namespace App\Model;


use App\Model\DataModel\ProductAttributes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ProductAttributesModel extends BaseMallBDModel {

    protected $primaryKey = 'id';
    protected $table      ='product_attributes';

    public function productAttrProduct()
    {
        return $this->hasOne("App\Model\ProductModel","id","product_id");//(modelName,foreignKey,PrimaryKey)
    }

    public function productAttrAttributes()
    {
        return $this->hasOne("App\Model\AttributesModel","id","attribute_id");//(modelName,foreignKey,PrimaryKey)
    }


    public function setId($id){
        $this->id = $id;
    }

}