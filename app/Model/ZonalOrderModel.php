<?php
/**
 * Project  : mallbdweb
 * File     : ZonalOrderModel.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 3/2/16 - 5:38 PM
 */

namespace App\Model;


class ZonalOrderModel extends BaseMallBDModel {

    protected $primaryKey = 'id';
    protected $table = 'zonal_orders';

    /**
     * @param mixed $order_id
     */
    public function setOrderId($order_id) {

        $this->setObj($order_id);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "order_id";
            $errorObj->msg = "Order Id is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "order_id";
            $errorObj->msg = "Order Id int/float required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        $this->order_id = $this->getObj();
        return TRUE;
    }

    /**
     * @param mixed $zone_id
     */
    public function setZoneId($zone_id) {

        $this->setObj($zone_id);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "zone_id";
            $errorObj->msg = "Select Zone Id";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "zone_id";
            $errorObj->msg = "Zone Id int/float required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(intval($this->getObj()) <=0){
            $errorObj = new ErrorObj();

            $errorObj->params = "zone_id";
            $errorObj->msg = "Select Zone ";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        $this->zone_id = $this->getObj();
        return TRUE;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id) {

        $this->setObj($user_id);

        if(!$this->basicValidation())
        {
            $errorObj = new ErrorObj();

            $errorObj->params = "user_id";
            $errorObj->msg = "User Id is empty";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        if(!is_numeric($this->getObj())){
            $errorObj = new ErrorObj();

            $errorObj->params = "user_id";
            $errorObj->msg = "User Id int/float required";

            array_push($this->errorManager->errorObj,$errorObj);
            return false;
        }
        $this->user_id = $user_id;
        return TRUE;
    }

    /**
     * @param mixed $created_on
     */
    public function setCreatedOn($created_on) {
        $this->created_on = $created_on;
    }


    public function zonalOrderOrder()
    {
        return $this->hasOne("App\Model\OrderModel","id","order_id");
    }

    public function zonalOrderZone()
    {
        return $this->hasOne("App\Model\ZoneModel","id","zone_id");
    }

    public function zonalOrderUser()
    {
        return $this->hasOne("App\Model\UserModel","id","user_id");
    }

}

