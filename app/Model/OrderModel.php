<?php

/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: ProductModel.php
 * User: rajib
 * Date: 1/14/16
 * Time: 3:29 PM
 */

namespace App\Model;

use App\Model\DataModel\Order;
use App\Model\DataModel\OrderProduct;
use App\Model\DataModel\OrderProducts;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OrderModel extends BaseMallBDModel {

    protected $primaryKey = 'id';
    protected $table = 'orders';
    
    /**
     * @param int $shop_id
     */
    public function setOnpurchaseDiscount($onpurchaseDiscount) {

        $this->setObj($onpurchaseDiscount);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "onpurchase_discount";
            $errorObj->msg = "Discount is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        if (!is_numeric($this->getObj())) {
            $errorObj = new ErrorObj();

            $errorObj->params = "onpurchase_discount";
            $errorObj->msg = "Discount int required";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        $this->onpurchase_discount = $this->getObj();
        return true;
    }

    /**
     * @param int $shop_id
     */
    public function setId($id) {

        $this->setObj($id);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "id";
            $errorObj->msg = "id is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        if (!is_numeric($this->getObj())) {
            $errorObj = new ErrorObj();

            $errorObj->params = "id";
            $errorObj->msg = "Shop id int required";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        $this->id = $this->getObj();
        return true;
    }

    /**
     * @param int $shop_id
     */
    public function setShopId($shop_id) {

        $this->setObj($shop_id);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "shop_id";
            $errorObj->msg = "Shop id is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        if (!is_numeric($this->getObj())) {
            $errorObj = new ErrorObj();

            $errorObj->params = "shop_id";
            $errorObj->msg = "Shop id int required";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        $this->shop_id = $this->getObj();
        return true;
    }

    public function setZoneId($zone_id) {

        $this->setObj($zone_id);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "zone_id";
            $errorObj->msg = "Zone id is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        if (!is_numeric($this->getObj())) {
            $errorObj = new ErrorObj();

            $errorObj->params = "zone_id";
            $errorObj->msg = "Zone id int required";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        $this->zone_id = $this->getObj();
        return true;
    }

    /**
     * @param int $invoice_no
     */
    public function setInvoiceNo($invoice_no) {

        $this->setObj($invoice_no);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "invoice_no";
            $errorObj->msg = "Invoice ID is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }

        $this->invoice_no = $this->getObj();
        return true;
    }

    public function setUniqueCode($unique_code) {

        $this->setObj($unique_code);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "unique_code";
            $errorObj->msg = "unique code ID is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }

        $this->unique_code = $this->getObj();
        return true;
    }

    /**
     * @param int $customer_id
     */
    public function setCustomerId($customer_id) {

        $this->setObj($customer_id);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "customer_id";
            $errorObj->msg = "Customer ID is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        if (!is_numeric($this->getObj())) {
            $errorObj = new ErrorObj();

            $errorObj->params = "customer_id";
            $errorObj->msg = "Customer Id int required";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }

        $this->customer_id = $this->getObj();
        return true;
    }

    /**
     * @param int $currency_id
     */
    public function setCurrencyId($currency_id) {

        $this->setObj($currency_id);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "currency_id";
            $errorObj->msg = "Currency ID is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        if (!is_numeric($this->getObj())) {
            $errorObj = new ErrorObj();

            $errorObj->params = "currency_id";
            $errorObj->msg = "Currency Id int required";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }

        $this->currency_id = $this->getObj();
        return true;
    }

    /**
     * @param string $currency_code
     */
    public function setCurrencyCode($currency_code) {

        $this->setObj($currency_code);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "currency_code";
            $errorObj->msg = "Currency Code is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }

        $this->currency_code = $this->getObj();
        return true;
    }

    /**
     * @param float $currency_value
     */
    public function setCurrencyValue($currency_value) {

        $this->setObj($currency_value);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "currency_value";
            $errorObj->msg = "Currency Value is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        if (!is_numeric($this->getObj())) {
            $errorObj = new ErrorObj();

            $errorObj->params = "currency_value";
            $errorObj->msg = "Currency Value int/float required";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }

        $this->currency_value = $this->getObj();
        return true;
    }

    /**
     * @param float $order_total
     */
    public function setOrderTotal($order_total) {

        $this->setObj($order_total);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "oder_total";
            $errorObj->msg = "Oder Total is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        if (!is_numeric($this->getObj())) {
            $errorObj = new ErrorObj();

            $errorObj->params = "oder_total";
            $errorObj->msg = "Oder total int/float required";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        $this->order_total = $this->getObj();
        return true;
    }

    /**
     * @param string $order_from
     */
    public function setOrderFrom($order_from) {

        $this->setObj($order_from);
        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "order_from";
            $errorObj->msg = "Oder From is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        $this->order_from = $this->getObj();
        return true;
    }

    /**
     * @param string $package_size
     */
    public function setPackageSize($package_size) {

        $this->setObj($package_size);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "package_size";
            $errorObj->msg = "Package Size is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        $this->package_size = $this->getObj();
        return true;
    }

    /**
     * @param string $package_weight
     */
    public function setPackageWeight($package_weight) {

        $this->setObj($package_weight);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "package_weight";
            $errorObj->msg = "Package Weight is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        $this->package_weight = $package_weight;
        return true;
    }

    public function setShippingCost($shipping_cost) {

        $this->setObj($shipping_cost);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "shipping_address";
            $errorObj->msg = "Shipping Address is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        if (!is_numeric($this->getObj())) {
            $errorObj = new ErrorObj();

            $errorObj->params = "shipping_cost";
            $errorObj->msg = "shipping cost  int/float required";
            $errorObj->msg = "shipping cost  int/float required";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        $this->shipping_cost = $this->getObj();
        return true;
    }

    /**
     * @param string $shipping_address
     */
    public function setShippingAddress($shipping_address) {

        $this->setObj($shipping_address);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "shipping_address";
            $errorObj->msg = "Shipping Address is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        $this->shipping_address = $this->getObj();
        return true;
    }
    
    public function setCustomerDiscountDetails($customer_discount_details) {

        $this->setObj($customer_discount_details);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "onpurchase_discount_details";
            $errorObj->msg = "Details is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        $this->onpurchase_discount_details = $this->getObj();
        return true;
    }
    
    public function setVoucherDiscountDetails($voucher_discount_details) {

        $this->setObj($voucher_discount_details);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "voucher_discount_details";
            $errorObj->msg = "Details is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        $this->voucher_discount_details = $this->getObj();
        return true;
    }

    /**
     * @param string $shipping_country
     */
    public function setShippingCountry($shipping_country) {

        $this->setObj($shipping_country);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "shipping_country";
            $errorObj->msg = "Shipping Country is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        $this->shipping_country = $this->getObj();
        return true;
    }

    /**
     * @param string $shipping_city
     */
    public function setShippingCity($shipping_city) {

        $this->setObj($shipping_city);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "shipping_city";
            $errorObj->msg = "Shipping city is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        $this->shipping_city = $this->getObj();
        return true;
    }
    public function setShippingFirstName($shipping_firstname) {

        $this->setObj($shipping_firstname);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "shipping_firstname";
            $errorObj->msg = "Shipping First Name is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        $this->shipping_firstname = $this->getObj();
        return true;
    }
    public function setShippingLastName($shipping_lastname) {

        $this->setObj($shipping_lastname);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "shipping_lastname";
            $errorObj->msg = "Shipping Last Name is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        $this->shipping_lastname = $this->getObj();
        return true;
    }
    public function setShippingPhone($shipping_phone) {

        $this->setObj($shipping_phone);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "shipping_phone";
            $errorObj->msg = "Shipping Phone is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        $this->shipping_phone = $this->getObj();
        return true;
    }

    /**
     * @param int $shipping_zipcode
     */
    public function setShippingZipcode($shipping_zipcode) {

        $this->setObj($shipping_zipcode);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "shipping_zipcode";
            $errorObj->msg = "Shipping zip code is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }

        $this->shipping_zipcode = $this->getObj();
        return true;
    }

    /**
     * @param string $is_wrapped
     */
    public function setIsWrapped($is_wrapped) {

        $this->setObj($is_wrapped);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "is_wrapped";
            $errorObj->msg = "Is Wrapped is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        $this->is_wrapped = $is_wrapped;
        return true;
    }

    /**
     * @param string $wrapped_note
     */
    public function setWrappedNote($wrapped_note) {

        $this->setObj($wrapped_note);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "wrapped_note";
            $errorObj->msg = "Wrapped Note is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        $this->wrapped_note = $wrapped_note;
        return true;
    }

    /**
     * @param string $discount
     */
    public function setDiscountTotal($discount) {
        $this->setObj($discount);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "discount";
            $errorObj->msg = "Discount is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        if (!is_numeric($this->getObj())) {
            $errorObj = new ErrorObj();

            $errorObj->params = "discount";
            $errorObj->msg = "Discount int/float required";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        $this->discount_total = $this->getObj();
        return true;
    }

    /**
     * @param string $discount
     */
    public function setVoucherDiscountTotal($voucherdiscount) {
        $this->setObj($voucherdiscount);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "voucherdiscount";
            $errorObj->msg = "Voucher Discount is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        if (!is_numeric($this->getObj())) {
            $errorObj = new ErrorObj();

            $errorObj->params = "voucherdiscount";
            $errorObj->msg = "Voucher Discount int/float required";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        $this->voucher_discount = $this->getObj();
        return true;
    }

    /**
     * @param string $created_on
     */
    public function setCreatedOn($created_on) {

        $this->created_on = $created_on;
        return true;
    }

    /**
     * @param int $created_by
     */
    public function setCreatedBy($created_by) {

        $this->setObj($created_by);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "created_by";
            $errorObj->msg = "Created By is empty";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        if (!is_numeric($this->getObj())) {
            $errorObj = new ErrorObj();

            $errorObj->params = "created_by";
            $errorObj->msg = "Created By int required";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }

        $this->created_by = $created_by;
        return true;
    }

    /**
     * @param string $updated_on
     */
    public function setUpdatedOn($updated_on) {
        $this->updated_on = $updated_on;
        return true;
    }

    /**
     * @param int $updated_by
     */
    public function setUpdatedBy($updated_by) {

        $this->setObj($updated_by);

        if (!is_numeric($this->getObj())) {
            $errorObj = new ErrorObj();

            $errorObj->params = "updated_by";
            $errorObj->msg = "Updated By int required";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }

        $this->updated_by = $updated_by;
        return true;
    }

    public function setDeliveryMethodsId($delivery_method_id) {

        $this->setObj($delivery_method_id);

        if (!$this->basicValidation()) {
            $errorObj = new ErrorObj();

            $errorObj->params = "delivery_method_id";
            $errorObj->msg = "Select delivery method";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        if (!is_numeric($this->getObj())) {
            $errorObj = new ErrorObj();

            $errorObj->params = "delivery_method_id";
            $errorObj->msg = "delivery method id int required";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        if (intval($this->getObj()) <= 0) {
            $errorObj = new ErrorObj();

            $errorObj->params = "delivery_method_id";
            $errorObj->msg = "Select delivery method";

            array_push($this->errorManager->errorObj, $errorObj);
            return false;
        }
        $this->delivery_methods_id = (int) $this->getObj();
        return true;
    }

    public function orderShop() {
        return $this->hasOne("App\Model\ShopModel", "id", "shop_id");
    }

    public function orderZone() {
        return $this->hasOne("App\Model\ZoneModel", "id", "zone_id");
    }

    public function orderCustomer() {
        return $this->hasOne("App\Model\UserModel", "id", "customer_id");
    }

    public function orderCurrency() {
        return $this->hasOne("App\Model\CurrencyModel", "id", "currency_id");
    }

    public function orderOrderProducts() {
        return $this->hasMany("App\Model\OrderProductsModel", "order_id", "id");
    }

    public function orderStatus() {
        return $this->hasOne("App\Model\OrderStatusModel", "order_id", "id");
    }

    public function orderPayments() {
        return $this->hasOne("App\Model\OrderPaymentsModel", "order_id", "id");
    }

    public function getById() {

        $dataRow = OrderModel::with(["orderOrderProducts" => function($q) {
                        $q->where('order_products.package_id', '<=', 0);
                    }])->where("id", "=", $this->id)->where("customer_id", "=", $this->customer_id)->get()->first();
        $order = new Order();
        $order->castMe($dataRow);

        $orderProductsModel = new OrderProductsModel();
        $orderProductsModel->setOrderId($this->id);
        $orderProduct = $orderProductsModel->getByOrderId();
        $packageModel = new PackageModel();

        foreach ($orderProduct as $row) {


            $orderProducts = new OrderProducts();
            $orderProducts->id = (int) $row->id;
            $orderProducts->discount = round($row->discount, 2);
            $orderProducts->price = round($row->price, 2);
            $orderProducts->packageId = (int) $row->package_id;
            $orderProducts->productQuantity = $row->product_quantity;
            $orderProducts->tax = $row->tax;
            $orderProducts->total = round($row->total, 2);

            $packageModel->setId($row->package_id);
            $orderProducts->itemType = OrderProducts::$_PACKAGE;
            $orderProducts->item = $packageModel->getPackageById();

            array_push($order->orderProducts, $orderProducts);
        }

        return $order;
    }

    public function getAllOrders($shop_id) {
        $orders = OrderModel::with("orderShop", "orderCustomer", "orderCurrency")
                        ->where("shop_id", "=", $shop_id)
                        ->limit($this->customLimit)
                        ->offset($this->customLimit * $this->customOffset)->get();

        if ($orders == null) {
            return null;
        }
        $orderList = [];

        foreach ($orders as $o) {
            $order = new order();
            $order->castMeFromObj($o);
            array_push($orderList, $order);
        }
        return $orderList;
    }

    public function getOrderByUserId($userId) {
        $orders = $this->with("orderShop", "orderZone", "orderCustomer", "orderCurrency", "orderStatus", "orderPayments")
                ->where("customer_id", "=", $userId)
                ->limit($this->customLimit)
                ->offset($this->customLimit * $this->customOffset)
                ->orderBy('id', 'desc')
                ->get();
        if ($orders == null) {
            return null;
        }
        $orderList = [];

        foreach ($orders as $o) {
            if ($o['orderPayments']->payment_status == "completed") {
                $order = new Order();
                $order->castMeFromObj($o);


                $orderProductsModel = new OrderProductsModel();
                $orderProductsModel->setOrderId($o->id);
                $orderProduct = $orderProductsModel->getByOrderId();
                $packageModel = new PackageModel();

                foreach ($orderProduct as $row) {


                    $orderProducts = new OrderProducts();
                    $orderProducts->id = (int) $row->id;
                    $orderProducts->discount = round($row->discount, 2);
                    $orderProducts->price = round($row->price, 2);
                    $orderProducts->packageId = (int) $row->package_id;
                    $orderProducts->productQuantity = $row->product_quantity;
                    $orderProducts->tax = $row->tax;
                    $orderProducts->total = round($row->total, 2);

                    $packageModel->setId($row->package_id);
                    $orderProducts->itemType = OrderProducts::$_PACKAGE;
                    $orderProducts->item = $packageModel->getPackageById();

                    array_push($order->orderProducts, $orderProducts);
                }
                array_push($orderList, $order);
            }
        }
        return $orderList;
    }

    public function getOrderByUserIdMobile($userId) {
        $orders = $this->with("orderShop", "orderZone", "orderCustomer", "orderCurrency", "orderStatus", "orderPayments")
                ->where("customer_id", "=", $userId)
//                            ->limit($this->customLimit)
//                            ->offset($this->customLimit*$this->customOffset)
                ->orderBy('id', 'desc')
                ->get();
        if ($orders == null) {
            return null;
        }
        $orderList = [];

        foreach ($orders as $o) {
            if ($o['orderPayments']->payment_status == "completed") {
                $order = new Order();
                $order->castMeFromObjFormobileOrders($o);


                $orderProductsModel = new OrderProductsModel();
                $orderProductsModel->setOrderId($o->id);
                $orderProduct = $orderProductsModel->getByOrderId();
                $packageModel = new PackageModel();

                foreach ($orderProduct as $row) {
                    $orderProducts = new OrderProducts();
                    $orderProducts->id = (int) $row->id;
                    $orderProducts->discount = round($row->discount, 2);
                    $orderProducts->price = round($row->price, 2);
                    $orderProducts->packageId = (int) $row->package_id;
                    $orderProducts->productQuantity = $row->product_quantity;
                    $orderProducts->packageQuantity = $row->package_quantity;
                    $orderProducts->tax = $row->tax;
                    $orderProducts->total = round($row->total, 2);

                    $packageModel->setId($row->package_id);
                    $orderProducts->itemType = OrderProducts::$_PACKAGE;
                    $orderProducts->packageitem = $packageModel->getPackageById();

                    array_push($order->orderProducts, $orderProducts);
                }
                array_push($orderList, $order);
            }
        }
        return $orderList;
    }

    public function getAllOrderPayments($customerId) {
        $orders = OrderModel::with("orderOrderProducts.orderProductsProduct")->where("customer_id", "=", $customerId)
                ->get();

        //return $orders;
        $orderList = [];

        foreach ($orders as $o) {
            $order = new Order();
            $order->castMeFromObj($o);
            array_push($orderList, $order);
        }


        return $orderList;
    }

    public function customerCompletedOrderById($customer_id) {
        $countData = OrderModel::select('orders.*')
                ->leftJoin('order_status', 'order_status.order_id', '=', 'orders.id')
                ->where('order_status.status_id', 6)
                ->where('orders.customer_id', $customer_id)
                ->count();

        return $countData;
    }

}
