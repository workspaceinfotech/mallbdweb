<?php
/*
	Author: Walletmix Ltd.
	Version: 2.3.5.16
	Title: Walletmix Payment Gateway Integration (data submission)
	Email: support@walletmix.com

*/
include ('walletmix.php');

$walletmix = NEW walletmix('wmx_10041_4590', 'wmx_10041_22159', 'WMX54f19eaf3e68c','704c0b14daaecb642b05b8f93c23cf3f1d2ea742');

$customer_info = array(
	"customer_name" 	=> "Michel Schofield",
	"customer_email" 	=> 'schofield@gmail.com',
	"customer_add" 		=> "Nikunjo",
	"customer_city" 	=> "Dhaka",
	"customer_country" 	=> "Bangladesh",
	"customer_postcode" => "1229",
	"customer_phone" 	=> "1229",
);
$shipping_info = array(
	"shipping_name" => "Linking Borrows",
	"shipping_add" => "1600 Amphitheatre Pkwy, Mountain View",
	"shipping_city" => "New York City",
	"shipping_country" => "United States",
	"shipping_postCode" => "1600",
);
$walletmix->set_shipping_charge(0);
$walletmix->set_discount(0);

$product_1 = array('name' => 'Adata 16GB Pendrive','price' => 5,'quantity' => 2);
$product_2 = array('name' => 'Adata 8GB Pendrive','price' => 5,'quantity' => 1);

$products = array($product_1,$product_2);

$walletmix->set_product_description($products);

$walletmix->set_merchant_order_id(10);

$walletmix->set_app_name('themallbd.com');
$walletmix->set_currency('BDT');
$walletmix->set_callback_url('http://themallbd.com/callback.php');

$extra_data = array('param_1' => 'data_1','param_2' => 'data_2','param_3' => 'data_3');
$walletmix->set_extra_json($extra_data);

$walletmix->set_transaction_related_params($customer_info);
$walletmix->set_transaction_related_params($shipping_info);

$walletmix->set_database_driver('session');	// options: "txt" or "session"

$walletmix->send_data_to_walletmix();
