<?php
/*
	Author: Walletmix Ltd.
	Version: 2.3.5.16
	Title: Walletmix Payment Gateway Integration (data callback)
	Email: support@walletmix.com

*/
include ('walletmix.php');

$walletmix = NEW walletmix('wmx_10041_4590', 'wmx_10041_22159', 'WMX54f19eaf3e68c','704c0b14daaecb642b05b8f93c23cf3f1d2ea742');

$walletmix->set_database_driver('session');	// options: "txt" or "session"

if(isset($_POST['merchant_txn_data'])){
	$merchant_txn_data = json_decode($_POST['merchant_txn_data']);
	
	$walletmix->get_database_driver();
	
	if($walletmix->get_database_driver() == 'txt'){
		$saved_data = json_decode($walletmix->read_file());
	}elseif($walletmix->get_database_driver() == 'session'){
		// Read data from your database
		$saved_data = json_decode($walletmix->read_data());
	}
	
	if($merchant_txn_data->token === $saved_data->token){
		
		$wmx_response = json_decode($walletmix->check_payment($saved_data));
		$walletmix->debug($wmx_response,true);
		if(	($wmx_response->wmx_id == $saved_data->wmx_id) ){
			if(	($wmx_response->txn_status == '1000') ){
				if(	($wmx_response->bank_amount >= $saved_data->amount) ){
					if(	($wmx_response->merchant_amount == $saved_data->amount) ){	
						echo 'Update merchant database with success. amount : '.$amount;
					}else{
						echo 'Merchant amount mismatch Merchant Amount : '.$saved_data->amount.' Bank Amount : '.$wmx_response->bank_amount.'. Update merchant database with success';
					}
				}else{
					echo 'Bank amount is less then merchant amount like partial payment.You can make it failed transaction.';
				}
			}else{
				echo 'Update merchant database with failed';
			}
		}else{
			echo 'Merchant ID Mismatch';
		}
	}else{
		echo 'token mismatch';
	}
}else{
	echo 'Try to direct access';
}

?>