<?php

namespace App\Http\Middleware;

use App\Http\Controllers\BaseMallBDController;
use App\Model\DataModel\AppCredential;
use App\Model\DataModel\AuthCredential;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class RedirectIfAuthenticated extends BaseMallBDController
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        if (Auth::guard($guard)->check()) {
            $user =  Auth::user();
            $appCredential = new AppCredential();
            $appCredential->castMe($user);

            if(!Session::has('AppCredential')){
                Session::put('AppCredential', $appCredential);
            }
            $this->serviceResponse->responseStat->isLogin=true;
            $this->serviceResponse->responseStat->status=true;
            $this->serviceResponse->responseData=$appCredential;
          //  return redirect('/');
        }

        return $next($request);
    }
}
