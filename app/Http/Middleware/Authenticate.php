<?php

namespace App\Http\Middleware;

use App\Http\Controllers\BaseMallBDController;
use Closure;
use Illuminate\Support\Facades\Auth;


class Authenticate extends BaseMallBDController
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                $this->serviceResponse->responseStat->status=false;
                $this->serviceResponse->responseStat->isLogin=false;
                $this->serviceResponse->responseStat->msg="Session Expired";
                return $this->response();
                //return redirect()->guest('login');
            }
        }

        return $next($request);
    }
}
