<?php
/**
 * Created by PhpStorm.
 * User: mi
 * Date: 3/11/16
 * Time: 4:12 PM
 */

namespace App\Http\Middleware;
use App\Http\Controllers\coreBaseClass\ServiceResponse;
use App\Model\DataModel\AppCredential;
use Closure;
use Illuminate\Support\Facades\Session;

class AuthServiceMallBDController
{


    public function handle($request, Closure $next)
    {
        $appCredential = (Session::get('AppCredential')==null)?new AppCredential():Session::get('AppCredential');
        $serviceResponse = new ServiceResponse();
        if($appCredential->id <=0){
            $serviceResponse->responseStat->status = false;
            $serviceResponse->responseStat->msg = "Please login first";
            return response()->json($serviceResponse);
        }
        return $next($request);

    }
}