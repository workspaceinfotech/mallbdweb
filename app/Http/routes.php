<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::any('/', 'Web\HomeController@index');
Route::any('/homefordesign', 'Web\HomeController@indexfordesign');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
/* ============== Start All Service For Mobile =================== */
/* ==============  End All Service For Mobile =================== */
Route::group(['middleware' => 'web'], function () {
    Route::auth();
    //  Route::get('/home', 'HomeController@index');
    Route::resource('registration/create', 'Web\SignUpController@create');
    Route::post('api/registration/register', 'API\Service\SignUpService@store');
});
Route::group(['middleware' => 'AuthServiceMallBDController'], function () {

    // add product to wishlist
    Route::post('api/customer/wishlist/add', 'API\Service\WishListService@addWishListedProduct');
    //get wishlist of customers
    Route::get('api/customer/wishlist/all', 'API\Service\WishListService@getWishListedProducts');

    Route::post('api/pass/reset','API\Service\PasswordResetService@index');


    Route::post('api/myaccount/update','API\Service\MyAccountService@update');


});
Route::get('_checkout', 'Web\CheckoutController@_index');
Route::group(['middleware' => 'AuthWebMallBDController'], function () {

    Route::get('customer/wishlist/get', 'Web\CustomerWishListController@getWishListedProducts');
    Route::get('pass/reset/req','Web\MyAccountController@resetPassReq');

    /* ===========MyAccount======================*/

    Route::get('myaccount/info','Web\MyAccountController@index');
    Route::post('order/partial/get','Web\MyAccountController@getMoreOrders');
    Route::get('order/details/{id}','Web\OrderController@details');


});

Route::get('checkout', 'Web\CheckoutController@index');
Route::get('checkout/greeting/{order_id}', 'Web\CheckoutController@greeting');
Route::get('checkout/paymentgreeting', 'Web\CheckoutController@paymentGreeting');
//Route::get('checkout/paymentgreeting', 'Web\CheckoutController@bkashError');
Route::get('test', 'Web\TestController@index');
Route::get('test/product', 'Web\TestController@product');


Route::post('api/login/accesstoken',"Auth\AuthController@accesstokenLogin");

//profile
Route::post('api/profile/update','API\Service\ProfileService@updateProfile');

//categories
// All categories with parents first then their children's all level ***
Route::post('api/category/parents/show','API\Service\CategoryService@parentsCategory');
//get all categories of specific parent by parent_id
Route::post('api/category/childs/show','API\Service\CategoryService@childsCategory');
//show all categories in the database. just select categories.
Route::post('api/category/all/show','API\Service\CategoryService@allCategories');


// Home Page Product
Route::post('api/mallbditem/all/show','API\Service\ProductService@getHomePageItem');
//products
Route::post('api/products/all/show','API\Service\ProductService@getAllProducts');
//products for Mobile test
Route::post('api/products/all/show/mobile','API\Service\ProductService@getAllProductsMobile');
//products find by Id
Route::post('api/products/findbyid','API\Service\ProductService@getProductById');
//products find all by keyword search
Route::post('api/products/all/search','API\Service\ProductService@getAllProductsBySearch');
//products find all by keyword search
Route::post('api/products/all/search/suggestion','API\Service\ProductService@getAllProductsBySearchForSuggestion');
//get all featured products
Route::post('api/products/featured/show','API\Service\ProductService@getAllFeaturedProducts');
Route::post('api/products/featured/show/mobile','API\Service\ProductService@getAllFeaturedProductsMobile');
//get all Special Offers products
Route::post('api/products/special/show','API\Service\ProductService@getAllSpecialProducts');
Route::post('api/products/special/show/mobile','API\Service\ProductService@getAllSpecialProductsMobile');

Route::post('api/products/new/show','API\Service\ProductService@getAllNewProducts');
Route::post('api/products/new/show/mobile','API\Service\ProductService@getAllNewProductsMobile');
//search product with
Route::post('api/products/search','API\Service\SearchService@getProductByKeyWord');

//get all products with category Id.
Route::post('api/product/category','API\Service\ProductService@getProductByCategory');

//get all zones

Route::get('api/zone/all','API\Service\ZoneService@getAllZones');

//get all payment methods

Route::get('api/paymentmethods/all','API\Service\PaymentMethodService@getAllPaymentMethods');

//order Service

Route::post('api/order','API\Service\OrderService@order');

//search by filter
Route::post('api/search/filter/product', 'API\Service\SearchService@searchByFilter');

//get all delivery methods

Route::get('api/deliverymethod/all', 'API\Service\DeliveryMethodService@getAllDeliveryMethods');

//checkout service
Route::post('api/checkout/submit', 'API\Service\CheckoutService@submitCheckout');

Route::post('api/checkout/submitWeb', 'API\Service\CheckoutService@submitCheckoutWeb');

Route::post('api/checkout/productquantitycheck', 'API\Service\CheckoutService@productQuantityCheck');

Route::post('api/checkout/packagequantitycheck', 'API\Service\CheckoutService@packageQuantityCheck');

//voucher code service
Route::post('api/checkout/voucher', 'API\Service\VoucherService@checkVoucher');

//get product by manufacturer Id

Route::post('api/product/manufacturer/all', 'API\Service\ProductService@getProductByManufacturer');


/* ============= WEB PAGE ROUTE ================*/
Route::any('/', 'Web\HomeController@index');
Route::resource('home', 'Web\HomeController@index');
Route::resource('homeproducts', 'Web\HomeController@gethomeproducts');
Route::resource('home_dev', 'Web\HomeController@index_dev');
Route::resource('home_dev1', 'Web\HomeController@index_dev1');
Route::resource('home_1', 'Web\HomeController@home1');
Route::resource('home_2', 'Web\HomeController@home2');
Route::get('page/{page_url}', 'Web\HomeController@pages');


Route::post('test2', 'Web\TestController2@insertIntoCustomerWishList');
Route::get('test2/view', 'Web\TestController2@index');
Route::post('wishlist/array', 'Web\TestController2@wishListArray');

/* ===========Categories======================*/
Route::post('web/category/parents/show','Web\CategoryController@parentsCategory');
Route::post('web/category/childs/show','Web\CategoryController@childsCategory');
Route::post('web/category/all/show','Web\CategoryController@allCategories');



/* ============ Packages ================== */
Route::get('packages/{title}/{id}','Web\PackagesController@details');
Route::get('packages','Web\PackagesController@index');

/* ===========Products======================*/

Route::get('category/{title}/{id}/{id2?}/{id3?}','Web\ProductController@getProductByCategory');
Route::get('manufacturer','Web\ProductController@getProductByManufacturer');
Route::get('newproducts','Web\ProductController@getNewProduct');
Route::get('featuredproducts','Web\ProductController@getFeaturedProduct');
Route::get('specialproducts','Web\ProductController@getSpecialProduct');
Route::get('bestsellerproducts','Web\ProductController@getBestSellerProduct');
Route::get('recentviewproducts','Web\ProductController@getrecentViewProduct');
Route::get('packagelist','Web\PackagesController@packageListView');

Route::get('compareProducts/{product_id}','Web\ProductController@compareProducts');
Route::get('comparePackages/{package_id}','Web\PackagesController@comparePackages');

/* product by id */

//Route::get('product/{product_url}/{product_code}','Web\ProductController@getProductById'); previous one
Route::get('product/{product_url}/{product_code}','Web\ProductController@getProductByProductCode');

// search product by manufacturer
Route::get('search/product', 'Web\SearchController@getProductByKeyWord');
Route::get('tags/product', 'Web\SearchController@getProductByTags');
Route::get('product/manufacturer/{id}','Web\SearchController@getProductByManufacturerId');


// get all product from wishlist



/* =====================Unused=======================
//add product to wishlist
Route::post('wishlist/product/add','Web\CustomerWishListController@addProductToCustomerWishList');
*/

/* ============= ENDS ================*/

/* ==== Guest Cart Session : START ==============*/
Route::post('api/guest/cart/set', 'API\Service\GuestSession@setCart');
Route::post('api/guest/cart/get', 'API\Service\GuestSession@getCart');

/* ============= Guest Cart Session : ENDS ================*/


/* ==== Checkout : START ===*/

Route::post('api/checkout/get/user_information', 'API\Service\CheckoutService@getInformationByEmail');
/* === Checkout : Ends ===*/




/* ===========Testing======================*/


Route::get('new/view','Web\TestController2@loadView');
Route::post('api/currency/conversion','API\Service\CurrencyConverterService@getCurrencyRate');

/* ===========Remove Product from WishList======= */

Route::post('api/wishlist/remove','API\Service\WishListService@removeFromWishList');
Route::post('api/wishlist/remove/array','API\Service\WishListService@removeFromWishListFromArray');
Route::post('api/wishlist/product/array','API\Service\WishListService@getWishListedProductArray');

/* ============== Contact Us ================================ */

Route::get('contactus', 'Web\ContactUs@index');
Route::post('send-contactus-mail', 'Web\ContactUs@sendContactusMail');

/* ==============Shipping Address Service =================== */

Route::post('api/shippingaddress/user','API\Service\ShippingAddressService@getAllShippingAddressesByUserId');
Route::post('api/shippingaddress/add','API\Service\ShippingAddressService@addShippingAddress');
Route::post('api/shippingaddress/update','API\Service\ShippingAddressService@editShippingAddress');
Route::post('api/shippingaddress/delete/by/email','API\Service\ShippingAddressService@deleteShippingAddressByEmail');


/* ============== User Service =================== */
Route::post('api/user/information/get','API\Service\UserInformationService@index');

Route::post('api/newsletter/subscribe','API\Service\NewsletterService@userSubscribe');
Route::post('api/customer/review/add','API\Service\ReviewService@addReview');
Route::any('api/order/getbyuser','API\Service\OrderService@getOrderById');
Route::post('api/order/getbyuser/mobile','API\Service\OrderService@getOrderByIdMobile');


Route::post('product/decrease', 'Web\TestController2@package');

Route::get('api/banner/all', 'API\Service\BannerService@getAllBanners');

Route::post('api/manufacturer/all', 'API\Service\ManufacturerService@allManufacturer');
Route::post('api/manufacturer/allforweb', 'API\Service\ManufacturerService@getAllManufacturerforWeb');

Route::post('api/product/review', 'API\Service\ReviewService@getAllReviewsByProduct');

Route::post('api/contactus/add', 'API\Service\ContactUsService@addContactUs');

Route::post('review/get', 'Web\ReviewController@getAllReviewsByProductId');

Route::post('api/product/category/searchbytitle', 'API\Service\SearchService@searchCategoryByProductTitleKeyword');
Route::get('product/category/searchbytitle/web', 'API\Service\SearchService@searchCategoryByProductTitleKeywordWeb');
Route::post('api/product/category/searchbytitle/mobile', 'API\Service\SearchService@searchCategoryByProductTitleKeywordMobile');
Route::post('api/review/count', 'API\Service\ReviewService@countReview');

Route::post('api/discount/bystartdate', 'API\Service\ProductDiscountService@getDiscountByStartDate');
Route::post('api/discount/byenddate', 'API\Service\ProductDiscountService@getDiscountByEndDate');

Route::post('api/package/all', 'API\Service\PackageService@getAllPackages');
Route::post('api/package/all/mobile', 'API\Service\PackageService@getAllPackagesMobile');
Route::post('api/package/byid', 'API\Service\PackageService@getPackageById');


Route::post('api/relatedproduct/get', 'API\Service\ProductService@getRelatedProducts');
//district API
Route::get('api/districts/get', 'API\Service\DistrictService@getAllDistricts');

//payment gateways API
Route::post('api/paymentstatuscheckbyid', 'API\Service\PaymentGatewayService@paymentStatusCheckById');

Route::get('api/walletmix/{order_id}', 'API\Service\PaymentGatewayService@walletmix');
Route::post('api/walletmix', 'API\Service\PaymentGatewayService@walletmixService');
Route::any('walletmix/callback', 'API\Service\PaymentGatewayService@walletmixCallback');
Route::any('walletmix/mobilecallback', 'API\Service\PaymentGatewayService@walletmixMobileCallback');

Route::get('bkash/{unique_code}', 'API\Service\PaymentGatewayService@bkash');
Route::get('bkash/cancel/{order_id}', 'API\Service\PaymentGatewayService@bkashcancel');
Route::post('api/bkash-data-process', 'API\Service\PaymentGatewayService@bkashDataProcess');

Route::get('api/paypal/{order_id}', 'API\Service\PaymentGatewayService@paypal');
Route::get('paypal/return', 'API\Service\PaymentGatewayService@paypalReturn');
Route::get('paypal/cancel', 'API\Service\PaymentGatewayService@paypalCancel');
Route::get('paypal/notify', 'API\Service\PaymentGatewayService@paypalNotify');

Route::post('api/sendsms', 'API\Service\PaymentGatewayService@sendSMS');

//payment success or cancel
Route::post('api/paymentsuccess', 'API\Service\PaymentGatewayService@paymentSuccess');
Route::get('paymentsuccess/{order_id}', 'API\Service\PaymentGatewayService@paymentSuccessGet');
Route::post('api/paymentcancel', 'API\Service\PaymentGatewayService@paymentCancel');
Route::get('paymentcancel/{order_id}', 'API\Service\PaymentGatewayService@paymentCancelGet');

Route::post('create-chat-guest', 'API\Service\SignUpService@createChatGuest');
//changeproductview
Route::post('changeproductview','API\Service\ProductService@changeProductView');


//customer purchase discount API
Route::get('api/getcustomerpurchasediscount', 'API\Service\CustomerPurchaseDiscountService@calculateOnPurchaseDiscount');

//customer purchase discount
Route::get('getCustomerPurchaseDiscount', 'Web\CustomerPurchaseDiscountController@calculateOnPurchaseDiscount');