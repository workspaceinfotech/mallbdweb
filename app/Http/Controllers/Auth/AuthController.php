<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\BaseMallBDController;
use App\Model\DataModel\AppCredential;
use App\Model\DataModel\AuthCredential;
use App\Model\LoginModel;
use App\Model\ChatStatusModel;
use App\Model\ChatTempDataModel;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Auth;

class AuthController extends BaseMallBDController
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use ThrottlesLogins;
    //use RedirectsUsers;
    use AuthenticatesUsers, RegistersUsers {
        AuthenticatesUsers::redirectPath insteadof RegistersUsers;
    }

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogin()
    {
        return $this->showLoginForm();
    }

    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        if (view()->exists('auth.authenticate')) {
            return view('auth.authenticate');
        }

        return view('auth.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
        return $this->login($request);
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            $this->loginUsername() => 'required', 'password' => 'required',
        ]);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->getCredentials($request);

        if (Auth::guard($this->getGuard())->attempt($credentials, $request->has('remember'))) {
            return $this->handleUserWasAuthenticated($request, $throttles);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles) {
            $this->incrementLoginAttempts($request);
        }

        $this->serviceResponse->responseStat->status = false;
        $this->serviceResponse->responseStat->msg="Credential do not match!";
        return $this->response();

        //return $this->sendFailedLoginResponse($request);
    }

    public function accesstokenLogin(Request $request){

        $accesstoken = $request->get('accesstoken');
        $member = LoginModel::where("access_token", "=", $accesstoken)->first();
        if($member==null)
        {
            $this->serviceResponse->responseStat->status=false;
            $this->serviceResponse->responseStat->msg="Your credential do not match";
            return $this->response();
        }
        Auth::login($member);

        if(Auth::check()) {

            $user  = Auth::user();
            $loginObj = new AuthCredential();
            $loginObj->castMe($user);

            $this->serviceResponse->responseStat->status=true;
            $this->serviceResponse->responseStat->isLogin=true;
            $this->serviceResponse->responseStat->msg="Successfully login";
            $this->serviceResponse->responseData = $loginObj;

            $appCredential = new AppCredential();
            $appCredential->castMe($user);

            Session::put('AppCredential', $appCredential);
            return $this->response();

            // return redirect($this->redirectPath());
        }
        $this->serviceResponse->responseStat->status=false;
        $this->serviceResponse->responseStat->isLogin=false;
        $this->serviceResponse->responseStat->msg="Login fail";

        return $this->response();
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  bool  $throttles
     * @return \Illuminate\Http\Response
     */
    protected function handleUserWasAuthenticated(Request $request, $throttles)
    {

        if(Auth::check()) {
            $user = Auth::user();
            $authCredential = new AuthCredential();
            $authCredential->castMe($user);

            $appCredential = new AppCredential();
            $appCredential->castMe($user);
             if($appCredential->user->role->id == 7 && $appCredential->user->status == "Active"){
                 Session::put('AppCredential',$appCredential);

                $this->serviceResponse->responseStat->isLogin = true;
                $this->serviceResponse->responseStat->msg="Successfully Login";
                $this->serviceResponse->responseData = $authCredential;
                Session::put('userId', $appCredential->user->id);
                Session::put('chatUserId', $appCredential->user->id);
                //ChatStatusModel::where('customer_id', $appCredential->user->id)->delete();
                //ChatTempDataModel::where('message_from', $appCredential->user->id)->orWhere('message_to', $appCredential->user->id)->delete();
                return $this->response();
             }
             else{
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseStat->msg="Credential do not match!";
                return $this->response(); 
             }
            
        }
        /*if ($throttles) {
            $this->clearLoginAttempts($request);
        }

        if (method_exists($this, 'authenticated')) {
            return $this->authenticated($request, Auth::user());
        }*/

        return redirect()->intended($this->redirectPath());
    }

    /**
     * Get the failed login response instance.
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        return redirect()->back()
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                $this->loginUsername() => $this->getFailedLoginMessage(),
            ]);
    }

    /**
     * Get the failed login message.
     *
     * @return string
     */
    protected function getFailedLoginMessage()
    {
        return Lang::has('auth.failed')
            ? Lang::get('auth.failed')
            : 'These credentials do not match our records.';
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function getCredentials(Request $request)
    {

        return $request->only($this->loginUsername(), 'password');
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogout()
    {
        return $this->logout();
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {

        Auth::guard($this->getGuard())->logout();

        Session::forget('AppCredential');
        $this->serviceResponse->responseStat->isLogin = false;
        $this->serviceResponse->responseStat->status = true;
        $this->serviceResponse->responseStat->msg="Successfully Logout";
        Session::put('chatUserId', 0);
        return $this->response();

       // return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function loginUsername()
    {
        return property_exists($this, 'username') ? $this->username : 'email';
    }

    /**
     * Determine if the class is using the ThrottlesLogins trait.
     *
     * @return bool
     */
    protected function isUsingThrottlesLoginsTrait()
    {
        return in_array(
            ThrottlesLogins::class, class_uses_recursive(get_class($this))
        );
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return string|null
     */
    protected function getGuard()
    {
        return property_exists($this, 'guard') ? $this->guard : null;
    }

    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function redirectPath()
    {
        if (property_exists($this, 'redirectPath')) {
            return $this->redirectPath;
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/home';
    }
}
