<?php
/**
 * Project  : mallbdweb
 * File     : MyAccountController.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 3/14/16 - 3:56 PM
 */

namespace App\Http\Controllers\Web;

use App\Http\Controllers\BaseMallBDController;
use App\Model\ReviewModel;
use Illuminate\Http\Request;

class ReviewController extends BaseMallBDController{

    function getAllReviewsByProductId(Request $request)
    {
        $productId = $request->input("product_id");

        $offset = $request->input("offset");

        if($offset==""){
            $offset=0;
        }

        $reviewModel = new ReviewModel();

        $reviewModel->setCustomOffset($offset);
        $reviewModel->setCustomLimit(3);

        $reviews = $reviewModel->getAllReviewByProduct($productId);

        $this->pageData['allReviews'] = $reviews;
        return view('web.review.partial.partial_review',$this->pageData);
    }

}