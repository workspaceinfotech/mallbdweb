<?php
/**
 * Project  : mallbdweb
 * File     : MyAccountController.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 3/14/16 - 3:56 PM
 */

namespace App\Http\Controllers\Web;


use App\Http\Controllers\API\Service\OrderService;
use App\Http\Controllers\BaseMallBDController;
use App\Model\DataModel\Order;
use App\Model\OrderModel;
use App\Model\ShippingAddressModel;
use App\Model\UserModel;
use App\Model\UserDetailsModel;
use App\Model\ProductModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Model\DistrictModel;
class MyAccountController extends BaseMallBDController
{
    function index() {
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        $userId = $this->appCredential->user->id;


        $userModel = new UserModel();


        $userInfo = $userModel->where("id","=",$userId)->first();
        $this->pageData['userInfo'] = $userInfo;

        $userDetailsModel = new UserDetailsModel();

        $userDetails = $userDetailsModel->where("user_id","=",$userId)->first();

        $orderModel = new OrderModel();
        $orderModel->setCustomLimit(3);
        $orderModel->setCustomOffset(0);
        $orderList = $orderModel->getOrderByUserId($this->appCredential->user->id);
        //$orderList = $orderModel->getOrderByUserId(4);


        $allOrders = $orderModel->getAllOrderPayments($this->appCredential->user->id);

        $shippingAddressModel = new ShippingAddressModel();
        $shippingAddressModel->setUserId($this->appCredential->user->id);
        $shippingAddressList = $shippingAddressModel->getAllByUserId();
        
        $productModel = new ProductModel();
        $results = $productModel->getBestSellerProductIdList();

        $bestSellerProductList = [];

        foreach ($results as $result) {
            $product = $productModel->getProductById($result->product_id);
            array_push($bestSellerProductList, $product);
        }

        $this->pageData['bestSellerProductList'] = $bestSellerProductList;

        $this->pageData['allOrders'] = $allOrders;
        $this->pageData['orderList'] = $orderList;
        $this->pageData['userDetails'] = $userDetails;
        $this->pageData['shippingAddressList'] = $shippingAddressList;
        
        $districtModel = new DistrictModel();

        $this->pageData['districts'] = $districtModel::all();
        return view('web.myaccount.index',$this->pageData);
    }

    function resetPassReq()
    {
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        return view('web.resetpass.index',$this->pageData);
    }

    function getMoreOrders(Request $request)
    {
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        $orderModel = new OrderModel();
        $offset = $request->input("offset");
        $orderModel->setCustomLimit(3);
        $orderModel->setCustomOffset($offset);

        $orderList = $orderModel->getOrderByUserId($this->appCredential->user->id);
        //$orderList = $orderModel->getOrderByUserId(4);


        $this->pageData['orderList'] = $orderList;
        //return $orderList[0]->orderProducts[0]->item->id;

        return view('web.order.partial.partial_orders',$this->pageData);

    }
}