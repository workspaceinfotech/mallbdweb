<?php
/**
 * Created by PhpStorm.
 * Project: MallBDFront
 * File: CategoryController.php
 * User: rajib
 * Date: 1/26/16
 * Time: 12:34 PM
 */

namespace App\Http\Controllers\Web;


use App\Http\Controllers\BaseMallBDController;
use App\Model\CategoryModel;
use App\Model\OrderAttributeModel;
use App\Model\OrderModel;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;

class OrderController extends BaseMallBDController{


    public function details($id)
    {   
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        $orderModel = new OrderModel();

        $orderModel->setId($id);
        $orderModel->setCustomerId($this->appCredential->user->id);
        $this->pageData['orderDetails'] = $orderModel->getById();

        return view('web.order.details',$this->pageData);
    }


} 