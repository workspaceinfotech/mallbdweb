<?php
/**
 * Created by PhpStorm.
 * User: mi
 * Date: 2/29/16
 * Time: 4:01 PM
 */

namespace App\Http\Controllers\Web;


use App\Http\Controllers\BaseMallBDController;
use App\Model\PaymentMethodsModel;
use App\Model\ZoneModel;
use App\Model\DistrictModel;

class CheckoutController extends BaseMallBDController
{
    public function index(){
        $this->pageData['redirectMainPage'] = true;
        $paymentMethodsModel = new PaymentMethodsModel();
        $zoneModel = new ZoneModel();
        $districtModel = new DistrictModel();

        $this->pageData['paymentMethods'] = $paymentMethodsModel->getAllPaymentMethods();
        $this->pageData['zones'] = $zoneModel->getAllZones();
        $this->pageData['districts'] = $districtModel::all();
        return view('web.checkout.index',$this->pageData);
    }
    public function _index(){
        $paymentMethodsModel = new PaymentMethodsModel();
        $zoneModel = new ZoneModel();

        $this->pageData['paymentMethods'] = $paymentMethodsModel->getAllPaymentMethods();
        $this->pageData['zones'] = $zoneModel->getAllZones();
        return view('web.checkout._index',$this->pageData);
    }
    public function greeting($order_id){
        $paymentMethodsModel = new PaymentMethodsModel();
        $zoneModel = new ZoneModel();
        $orderData = \App\Model\OrderModel::find($order_id);

        $this->pageData['orderData'] = $orderData;
        $this->pageData['paymentMethods'] = $paymentMethodsModel->getAllPaymentMethods();
        $this->pageData['zones'] = $zoneModel->getAllZones();
        return view('web.checkout.greeting',$this->pageData);
    }
    public function paymentGreeting(){
        $paymentMethodsModel = new PaymentMethodsModel();
        $zoneModel = new ZoneModel();

        $this->pageData['paymentMethods'] = $paymentMethodsModel->getAllPaymentMethods();
        $this->pageData['zones'] = $zoneModel->getAllZones();
        return view('web.checkout.paymentgreeting',$this->pageData);
    }
}