<?php
/**
 * Created by PhpStorm.
 * User: mi
 * Date: 1/20/16
 * Time: 5:11 PM
 */

namespace App\Http\Controllers\Web;

use App\Http\Controllers\BaseMallBDController;
use App\Model\AttributesModel;
use App\Model\LoginModel;
use App\Model\MallBdItem_Model;
use App\Model\OrderAttributeModel;
use App\Model\OrderModel;
use App\Model\ProductAttributesCombinationModel;
use App\Model\ProductDiscountModel;
use App\Model\ZonalDeliveryBoyModel;

class TestController extends BaseMallBDController
{
    function index()
    {
        $mallBdItem_Model  = new MallBdItem_Model();

        return response()->json(  $mallBdItem_Model->getItem());
    }
    function product(){
            $jsonStr =    '{
                    "category": [
                        {"character": "Superman", "title": "Clark Kent"},
                        {"character": "Batman", "title": "Bruce Wayne"},
                        {"character": "Wonder Woman", "title": "Diana Prince"},
                        {"character": "Flash", "title": "Barry Allen"},
                        {"character": "Green Lantern", "title": "Hal Jordan"},
                        {"character": "Catwoman", "title": "Selina Kyle"},
                        {"character": "Nightwing", "title": "Dick Grayson"},
                        {"character": "Captain Marvel", "title": "Billy Batson"},
                        {"character": "Aquaman", "title": "Arthur Curry"},
                        {"character": "Green Arrow", "title": "Oliver Queen"},
                        {"character": "Martian Manhunter", "title": "J onn J onzz"},
                        {"character": "Batgirl", "title": "Barbara Gordon"},
                        {"character": "Supergirl", "title": "Kara Zor-El"},
                        {"character": "Black Canary", "title": "Dinah Lance"},
                        {"character": "Hawkgirl", "title": "Shiera Hall"},
                        {"character": "Cyborg", "title": "Vic Stone"},
                        {"character": "Robin", "title": "Damian Wayne"}
                    ]
                }';

        return response()->json(json_decode($jsonStr));
    }

    private function orderUniuqeNumber($length) {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }
}