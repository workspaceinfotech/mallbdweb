<?php

/**
 * Created by PhpStorm.
 * User: mi
 * Date: 4/6/16
 * Time: 3:22 PM
 */

namespace App\Http\Controllers\Web;

use App\Model\DataModel\MallBdItem;
use App\Http\Controllers\BaseMallBDController;
use App\Model\AttributesModel;
use App\Model\CategoryModel;
use App\Model\ManufacturerModel;
use App\Model\ProductCategoriesModel;
use App\Model\ProductModel;
use App\Model\PackageModel;
use Illuminate\Http\Request;

class PackagesController extends BaseMallBDController {

    function index(Request $request) {

        $packageModel = new PackageModel();

        $limit = 0;
        if ($request->input("limit") != null) {
            $limit = (is_numeric($request->input("limit"))) ? intval($request->input("limit")) : 5;
        }

        $limit = ($limit == (0 || "")) ? 5 : $limit;
        $packageModel->setCustomLimit($limit);
        $packageModel->customOffset = ($request->input("offset") == (0 || "")) ? 0 : $request->input("offset");
        $currentPage = ($request->input("offset") == (0 || "")) ? 0 : ("offset");


        $totalPackages = $packageModel->allPackageCount();
        $allPackages = $packageModel->getAll();

        $this->pageData['packages'] = $allPackages;
        $this->pageData['totalPackage'] = $totalPackages;
        $this->pageData['limit'] = $limit;
        $this->pageData['currentPage'] = $currentPage;

        return view('web.packages.all', $this->pageData);
    }

    function details($title, $id) {

        $packageModel = new PackageModel();
        $packageModel->setId($id);
//        $package = $packageModel->getPackageById(); the old one
        $package = $packageModel->getPackageByIdAndTitle($title);


        $productModel = new ProductModel();
        $results = $productModel->getBestSellerProductIdList();

        $bestSellerProductList = [];

        foreach ($results as $result) {
            $product = $productModel->getProductById($result->product_id);
            array_push($bestSellerProductList, $product);
        }

        $this->pageData['bestSellerProductList'] = $bestSellerProductList;



        $this->pageData['package'] = $package;

        return view('web.packages.details', $this->pageData);
    }

    public function comparePackages(Request $request, $package_id) {
        $selectedPackageDetails = PackageModel::with('packageProduct', 'packageProduct.product')
                ->where('id', $package_id)
                ->where("shop_id", "=", $this->shopId)
                ->get();
        $toDay = date('Y-m-d');
        $allPackages = PackageModel::with('packageProduct', 'packageProduct.product')
                ->where('status', 'Active')->where('start_date', '<=', $toDay)->where('end_date', '>=', $toDay)
                ->where('id', '!=', $package_id)
                ->where("shop_id", "=", $this->shopId)
                ->get();
        $packageModel = new PackageModel();
        $productModel = new ProductModel();
        $packageList = $packageModel->getAlltoCompare();

        $this->pageData['selectedPackageDetails'] = $selectedPackageDetails;
        $this->pageData['allPackages'] = $allPackages;
        $this->pageData['packageList'] = $packageList;
        return view("web.compare.comparePackage", $this->pageData);
    }

    public function packageListView(Request $request) {

        /* Searching params for product by category and Attribute */
        /* Starts Here */

        $q = $request->input("q");
        $selectedProductAttribute = [];
        $selectedPrice = new \stdClass();
        $searchProduct = false;
        $priceValidation = true;
        $selectedManufacturersIdList = [];
        $parentCategoryId = 0;
        if ($q != null && $q != "") {
            $pa = [];
            $tmpQstar = json_decode($q);

            try {
                $selectedProductAttribute = (isset($tmpQstar->pa)) ? $tmpQstar->pa : [];
                $searchProduct = true;
            } catch (Exception $ex) {
                $selectedProductAttribute = [];
            }
            try {
                $selectedPrice = (isset($tmpQstar->price)) ? $tmpQstar->price : new \stdClass();
                if (!isset($selectedPrice->min) || !isset($selectedPrice->max)) {
                    $priceValidation = false;
                } else {
                    if ($selectedPrice->min <= 0) {
                        $priceValidation = false;
                    }
                    if ($selectedPrice->max <= 0) {
                        $priceValidation = false;
                    }
                }


                if (!$priceValidation) {
                    $selectedPrice = new \stdClass();
                } else {
                    $searchProduct = true;
                }
            } catch (Exception $ex) {
                $selectedProductAttribute = [];
            }
            try {

                $selectedManufacturersIdList = (isset($tmpQstar->manufacturers)) ? $tmpQstar->manufacturers : new \stdClass();

                if (!is_array($selectedManufacturersIdList)) {
                    $selectedManufacturersIdList = [];
                    $searchProduct = true;
                }
            } catch (Exception $ex) {
                $selectedManufacturersIdList = [];
            }
        } else {
            $priceValidation = false;
            $selectedManufacturersIdList = [];
            $searchProduct = true;
        }

        $this->pageData['selectedManufacturersIdList'] = $selectedManufacturersIdList;
        $this->pageData['selectedProductAttribute'] = $selectedProductAttribute;
        $this->pageData['selectPrice'] = $selectedPrice;
        /* End Here */



        $categoryIdList = [];


        $orderBy = 0;
        if ($request->input("orderBy") != null) {
            $orderBy = (is_numeric($request->input("orderBy"))) ? intval($request->input("orderBy")) : 0;
        }

        $limit = 0;
        if ($request->input("limit") != null) {
            $limit = (is_numeric($request->input("limit"))) ? intval($request->input("limit")) : 10;
        }



        $packageModel = new PackageModel();
        $packageModel->setCurrentUserId($this->appCredential->user->id);
        $productModel = new ProductModel();
//        $productModel->setCurrentUserId($this->appCredential->user->id);
        $limit = ($limit == (0 || "")) ? 40 : $limit;
        $packageModel->setCustomLimit($limit);
        $packageModel->customOffset = ($request->input("page") == (0 || "")) ? 0 : $request->input("page");
//        $productModel->setCustomLimit($limit);
//        $productModel->customOffset = ($request->input("page") == (0 || "")) ? 0 : $request->input("page");
        $currentPage = ($request->input("page") == (0 || "")) ? 0 : $request->input("page");

        $startProductNumber = $limit * $currentPage + 1;
        $endProductNumber = $limit * ($currentPage + 1);

        $productList = [];
        $packageList = [];

//        $bestSellerList = $productModel->getAllBestSellerProductId();
//        $bestSellerArray = array();
//        foreach ($bestSellerList as $best):
//            array_push($bestSellerArray, $best->product_id);
//        endforeach;

        if ($priceValidation) {
            $packageList = $packageModel->getFilteredPackage($selectedPrice->min, $selectedPrice->max, $orderBy);
            $totalPackage = $packageModel->getFilteredPackageCount($selectedPrice->min, $selectedPrice->max, $orderBy);

//                $productList = $productModel->getAllBestSellerProducts($categoryIdList, $selectedProductAttribute, $selectedPrice->min, $selectedPrice->max, $selectedManufacturersIdList, $bestSellerArray, $orderBy);
//                $totalProduct = $productModel->getAllBestSellerProductsCount($categoryIdList, $selectedProductAttribute, $selectedPrice->min, $selectedPrice->max, $selectedManufacturersIdList, $bestSellerArray, $orderBy);
        } else {
            $packageList = $packageModel->getFilteredPackage(-1, -1, $orderBy);
            $totalPackage = $packageModel->getFilteredPackageCount(-1, -1, $orderBy);

//                $productList = $productModel->getAllBestSellerProducts($categoryIdList, $selectedProductAttribute, -1, -1, $selectedManufacturersIdList, $bestSellerArray, $orderBy);
//                $totalProduct = $productModel->getAllBestSellerProductsCount($categoryIdList, $selectedProductAttribute, -1, -1, $selectedManufacturersIdList, $bestSellerArray, $orderBy);
        }

        $mallBdItemList = [];
        foreach ($packageList as $package) {

            $mallBdItem = new MallBdItem();

            $mallBdItem->setPackage($package);
            array_push($mallBdItemList, $mallBdItem);
        }


        if ($endProductNumber > $totalPackage) {
            $endProductNumber = $totalPackage;
        }

        if (empty($packageList)) {
            $this->pageData['status'] = false;
            $this->pageData['msg'] = "No Data received";
        }

        $this->pageData['status'] = true;
        $this->pageData['selected'] = true;
        $this->pageData['selectedAction'] = $orderBy;
        $this->pageData['msg'] = "No Data received";
        $this->pageData['productList'] = $packageList;
        $this->pageData['totalProduct'] = $totalPackage;
        $this->pageData['mallBdItemList'] = $mallBdItemList;


        $manufacturerModel = new ManufacturerModel();

        $this->pageData['manufacturers'] = $manufacturerModel->getManufactureIdList();

        $price = [];
        $price['min'] = 0;
        $price['max'] = 5000;
        $this->pageData['minMaxPrice'] = $price;

        $attributesModel = new AttributesModel();
        $attributesModel->setCategoryIdList($categoryIdList);

        /* Ends Here */
        $this->pageData['parentCategoryId'] = $parentCategoryId;
        $this->pageData['currentPage'] = $currentPage;
        $this->pageData['limit'] = $limit;
        $this->pageData['startProductNumber'] = $startProductNumber;
        $this->pageData['endProductNumber'] = $endProductNumber;
        $this->pageData['productsAttributes'] = $attributesModel->getByProductList();


        $results = $productModel->getBestSellerProductIdList();

        $bestSellerProductList = [];

        foreach ($results as $result) {
            $product = $productModel->getProductById($result->product_id);
            array_push($bestSellerProductList, $product);
        }

        $this->pageData['bestSellerProductList'] = $bestSellerProductList;

        $categoryNameList = [];


        $this->pageData['categoryNameList'] = array();
        $this->pageData['subcategoryList'] = array();
        $this->pageData['selectedCategory'] = array();
        return view("web.home.packagelist", $this->pageData);
    }

}
