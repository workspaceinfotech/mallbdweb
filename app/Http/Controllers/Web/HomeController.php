<?php
/**
 * Created by PhpStorm.
 * User: mi
 * Date: 1/20/16
 * Time: 5:11 PM
 */

namespace App\Http\Controllers\Web;


use App\Http\Controllers\BaseMallBDController;
use App\Http\Controllers\Controller;
use App\Model\CategoryModel;
use App\Model\MallBdItem_Model;
use App\Model\ProductModel;
use App\Model\ManufacturerModel;
use App\Model\HomeAfterProductImagesModel;
use App\Model\HomeDownImagesModel;
use App\Model\HomeFacebookReviewsModel;
use App\Model\HomeYoutubeVideosModel;
use App\Model\BannerModel;
use App\Model\StaticPagesModel;
use Illuminate\Support\Facades\Session;

class HomeController extends BaseMallBDController
{
    function index_dev(){


        $products = new ProductModel();
        $limit= 10;
        $products->setCustomLimit($limit);
        $products->customOffset = 0;
        $products->setCurrentUserId($this->appCredential->user->id);

        //$productList = $products->getAllProducts($this->shopId);

        $featuredProductList =$products->getFeaturedProducts($this->shopId);
        $specialProductList =$products->getSpecialProducts($this->shopId);
        $newProductList = $products->getNewProducts($this->shopId);

        $productList = array_merge($newProductList,$featuredProductList,$specialProductList);
        $productList = array_unique($productList, SORT_REGULAR);


        $mallBdItem_Model = new MallBdItem_Model();
        $mallBdItemList = $mallBdItem_Model->getItem($productList);

        if(empty($mallBdItemList))
        {
            $this->pageData['status'] = false;
            $this->pageData['msg'] = "No Data Received";
            $this->pageData['productList'] = $mallBdItemList;
        }


        $manufacturerMode = new ManufacturerModel();
        $data = $manufacturerMode->getAllManufacturer();

        $bannerModel = new BannerModel();
        $banners = $bannerModel->getAllBanners($this->shopId);


        $this->pageData['manufacturer'] = $data;
        $this->pageData['status'] = true;
        $this->pageData['msg'] = "Data Received";
        $this->pageData['mallBdItemList'] = $mallBdItemList; //$productList;
        $this->pageData['banners'] = $banners;
        $this->pageData['chatUserId'] = $this->appCredential->user->id;


        return view('web.home.index_dev',$this->pageData);//response()->json( $this->pageData['itemList'] );

    }
    function index(){

        $products = new ProductModel();
        $limit= 12;
        $products->setCustomLimit($limit);
        $products->customOffset = 0;
        $products->setCurrentUserId($this->appCredential->user->id);

        //$productList = $products->getAllProducts($this->shopId);

        $featuredProductList =$products->getFeaturedProducts($this->shopId);
        $specialProductList =$products->getSpecialProducts($this->shopId);
        $newProductList = $products->getNewProducts($this->shopId);

        $productList = array_merge($newProductList,$featuredProductList,$specialProductList);
        $productList = array_unique($productList, SORT_REGULAR);


        $mallBdItem_Model = new MallBdItem_Model();
        $mallBdItemList = $mallBdItem_Model->getItem($productList);

        if(empty($mallBdItemList))
        {
            $this->pageData['status'] = false;
            $this->pageData['msg'] = "No Data Received";
            $this->pageData['productList'] = $mallBdItemList;
        }

        //Get All category parents first then children's all level.
        $categoryModel = new CategoryModel();
        $categoryList = $categoryModel->getAllParentsCategory($this->shopId);

        $manufacturerMode = new ManufacturerModel();
        $data = $manufacturerMode->getAllManufacturer_for_home(40,0);

        $bannerModel = new BannerModel();
        $banners = $bannerModel->getAllBanners($this->shopId);
        
        $homeafterproductimagesmodel = new HomeAfterProductImagesModel();
        $homeafterproductimages = $homeafterproductimagesmodel->getAllHomeAfterProductImages();
        $homedownimagesmodel = new HomeDownImagesModel();
        $homedownimages = $homedownimagesmodel->getAllHomeDownImages();
        $homefacebookreviewsmodel = new HomeFacebookReviewsModel();
        $homefacebookreviews = $homefacebookreviewsmodel->getAllHomeFacebookReviews();
        $homeyoutubevideosmodel = new HomeYoutubeVideosModel();
        $homeyoutubevideos = $homeyoutubevideosmodel->getAllHomeYoutubeVideos();


        $this->pageData['manufacturer'] = $data;
        $this->pageData['status'] = true;
        $this->pageData['msg'] = "Data Received";
        $this->pageData['mallBdItemList'] = $mallBdItemList; //$productList;
        $this->pageData['categoryList'] = $categoryList;
        $this->pageData['banners'] = $banners;
        $this->pageData['homeafterproductimages'] = $homeafterproductimages;
        $this->pageData['homedownimages'] = $homedownimages;
        $this->pageData['homefacebookreviews'] = $homefacebookreviews;
        $this->pageData['homeyoutubevideos'] = $homeyoutubevideos;
        $this->pageData['chatUserId'] = $this->appCredential->user->id;


        return view('web.home.index',$this->pageData);//response()->json( $this->pageData['itemList'] );
    }
    function indexfordesign(){

        $products = new ProductModel();
        $limit= 10;
        $products->setCustomLimit($limit);
        $products->customOffset = 0;
        $products->setCurrentUserId($this->appCredential->user->id);

        //$productList = $products->getAllProducts($this->shopId);

        $featuredProductList =$products->getFeaturedProducts($this->shopId);
        $specialProductList =$products->getSpecialProducts($this->shopId);
        $newProductList = $products->getNewProducts($this->shopId);

        $productList = array_merge($newProductList,$featuredProductList,$specialProductList);
        $productList = array_unique($productList, SORT_REGULAR);


        $mallBdItem_Model = new MallBdItem_Model();
        $mallBdItemList = $mallBdItem_Model->getItem($productList);

        if(empty($mallBdItemList))
        {
            $this->pageData['status'] = false;
            $this->pageData['msg'] = "No Data Received";
            $this->pageData['productList'] = $mallBdItemList;
        }

        //Get All category parents first then children's all level.
        $categoryModel = new CategoryModel();
        $categoryList = $categoryModel->getAllParentsCategory($this->shopId);

        $manufacturerMode = new ManufacturerModel();
        $data = $manufacturerMode->getAllManufacturer_for_home(40,0);

        $bannerModel = new BannerModel();
        $banners = $bannerModel->getAllBanners($this->shopId);
        
        $homeafterproductimagesmodel = new HomeAfterProductImagesModel();
        $homeafterproductimages = $homeafterproductimagesmodel->getAllHomeAfterProductImages();
        $homedownimagesmodel = new HomeDownImagesModel();
        $homedownimages = $homedownimagesmodel->getAllHomeDownImages();
        $homefacebookreviewsmodel = new HomeFacebookReviewsModel();
        $homefacebookreviews = $homefacebookreviewsmodel->getAllHomeFacebookReviews();
        $homeyoutubevideosmodel = new HomeYoutubeVideosModel();
        $homeyoutubevideos = $homeyoutubevideosmodel->getAllHomeYoutubeVideos();


        $this->pageData['manufacturer'] = $data;
        $this->pageData['status'] = true;
        $this->pageData['msg'] = "Data Received";
        $this->pageData['mallBdItemList'] = $mallBdItemList; //$productList;
        $this->pageData['categoryList'] = $categoryList;
        $this->pageData['banners'] = $banners;
        $this->pageData['homeafterproductimages'] = $homeafterproductimages;
        $this->pageData['homedownimages'] = $homedownimages;
        $this->pageData['homefacebookreviews'] = $homefacebookreviews;
        $this->pageData['homeyoutubevideos'] = $homeyoutubevideos;
        $this->pageData['chatUserId'] = $this->appCredential->user->id;


        return view('web.home.indexfordesign',$this->pageData);//response()->json( $this->pageData['itemList'] );
    }

    function index_dev1(){

        $products = new ProductModel();
        $limit= 10;
        $products->setCustomLimit($limit);
        $products->customOffset = 0;
        $products->setCurrentUserId($this->appCredential->user->id);

        //$productList = $products->getAllProducts($this->shopId);

        $featuredProductList =$products->getFeaturedProducts($this->shopId);
        $specialProductList =$products->getSpecialProducts($this->shopId);
        $newProductList = $products->getNewProducts($this->shopId);

        $productList = array_merge($newProductList,$featuredProductList,$specialProductList);
        $productList = array_unique($productList, SORT_REGULAR);


        $mallBdItem_Model = new MallBdItem_Model();
        $mallBdItemList = $mallBdItem_Model->getItem($productList);

        if(empty($mallBdItemList))
        {
            $this->pageData['status'] = false;
            $this->pageData['msg'] = "No Data Received";
            $this->pageData['productList'] = $mallBdItemList;
        }

        //Get All category parents first then children's all level.
        $categoryModel = new CategoryModel();
        $categoryList = $categoryModel->getAllParentsCategory($this->shopId);

        $manufacturerMode = new ManufacturerModel();
        $data = $manufacturerMode->getAllManufacturer_dev(6,1);

        $bannerModel = new BannerModel();
        $banners = $bannerModel->getAllBanners($this->shopId);


        $this->pageData['manufacturer'] = $data;
        $this->pageData['status'] = true;
        $this->pageData['msg'] = "Data Received";
        $this->pageData['mallBdItemList'] = $mallBdItemList; //$productList;
        $this->pageData['categoryList'] = $categoryList;
        $this->pageData['banners'] = $banners;
        $this->pageData['chatUserId'] = $this->appCredential->user->id;


        return view('web.home.index_dev1',$this->pageData);//response()->json( $this->pageData['itemList'] );
    }

    function gethomeproducts(){
        $products = new ProductModel();
        $limit= 10;
        $products->setCustomLimit($limit);
        $products->customOffset = 0;

        $featuredProductList =$products->getFeaturedProducts($this->shopId);
        $specialProductList =$products->getSpecialProducts($this->shopId);
        $newProductList = $products->getNewProducts($this->shopId);

        $productList = array_merge($newProductList,$featuredProductList,$specialProductList);
        $productList = array_unique($productList, SORT_REGULAR);


        $mallBdItem_Model = new MallBdItem_Model();
        $mallBdItemList = $mallBdItem_Model->getItem($productList);
        $this->pageData['mallBdItemList'] = $mallBdItemList;

        return view('web.partial.ajaxview.products_container',$this->pageData);

    }
    
    function pages($page_url){
        
        $productModel = new ProductModel();
        $results = $productModel->getBestSellerProductIdList();

        $bestSellerProductList = [];

        foreach ($results as $result) {
            $product = $productModel->getProductById($result->product_id);
            array_push($bestSellerProductList, $product);
        }

        $this->pageData['bestSellerProductList'] = $bestSellerProductList;
        
        $page_name = urldecode($page_url);
        $staticPageInformation = StaticPagesModel::where('page_name', $page_name)->where('shop_id', $this->shopId)->get();
//        echo '<pre>';
//        print_r($staticPageInformation);
//        die;
        $this->pageData['staticPageInformation'] = $staticPageInformation;

        return view('web.pages.index',$this->pageData);

    }



    function home1(){

        /* $validator = Validator::make($request->all(), [
             'limit' => 'required|Integer',
             'offset' => 'required|Integer',
             'shop_id' => 'required|Integer',
         ]);

         if ($validator->fails())
         {

             $this->serviceResponse->responseStat->status = false;
             $this->serviceResponse->responseStat->isLogin = false;
             $this->serviceResponse->responseStat->msg=$validator->errors()->first();
             return $this->response();
         }*/

        $products = new ProductModel();
        $limit= 10;
        $products->setCustomLimit($limit);
        $products->customOffset = 0;

        $productList = $products->getAllProducts($this->shopId);

        if(empty($productList))
        {
            $this->pageData['status'] = false;
            $this->pageData['msg'] = "No Data Received";
            $this->pageData['productList'] = $productList;
        }

        $this->pageData['status'] = true;
        $this->pageData['msg'] = "Data Received";
        $this->pageData['productList'] = $productList;





        return view('web.home.home1',$this->pageData);

    }
    function home2(){

        /* $validator = Validator::make($request->all(), [
             'limit' => 'required|Integer',
             'offset' => 'required|Integer',
             'shop_id' => 'required|Integer',
         ]);

         if ($validator->fails())
         {

             $this->serviceResponse->responseStat->status = false;
             $this->serviceResponse->responseStat->isLogin = false;
             $this->serviceResponse->responseStat->msg=$validator->errors()->first();
             return $this->response();
         }*/

        $products = new ProductModel();
        $limit= 10;
        $products->setCustomLimit($limit);
        $products->customOffset = 0;

        $productList = $products->getAllProducts($this->shopId);

        if(empty($productList))
        {
            $this->pageData['status'] = false;
            $this->pageData['msg'] = "No Data Received";
            $this->pageData['productList'] = $productList;
        }

        //Get All category parents first then children's all level.
        $categoryModel = new CategoryModel();
        $categoryList = $categoryModel->getAllParentsCategory($this->shopId);

        $userId= Session::get('userId', '0');

        $this->pageData['status'] = true;
        $this->pageData['msg'] = "Data Received";
        $this->pageData['productList'] = $productList;
        $this->pageData['categoryList'] = $categoryList;
        $this->pageData['userId'] = $userId;

        return view('web.home.home2',$this->pageData);

    }

}