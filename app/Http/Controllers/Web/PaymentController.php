<?php

/**
 * Created by PhpStorm.
 * User: mi
 * Date: 4/6/16
 * Time: 3:22 PM
 */

namespace App\Http\Controllers\Web;

use App\Http\Controllers\BaseMallBDController;
use App\Helper\WALLETMIX;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use App\Model\OrderModel;

class PaymentController extends BaseMallBDController {

       function index($order_id) {
        $walletmix = new WALLETMIX();


        $walletmix->set_service_access_username('wmx_10041_4590');
        $walletmix->set_service_access_password('wmx_10041_22159');
        $walletmix->set_merchant_id('WMX54f19eaf3e68c');
        $walletmix->set_access_app_key('704c0b14daaecb642b05b8f93c23cf3f1d2ea742');


        $string = json_decode(file_get_contents($this->walletmixGatewayUrl . "check-server"));


        $orderDetails = OrderModel::with('orderCustomer')->with('orderCustomer.userDetails')->where('id', $order_id)->get();

        if ($string->selectedServer) {
            $customer_info = array(
                "customer_name" => $orderDetails[0]['orderCustomer']['firstname'] . ' ' . $orderDetails[0]['orderCustomer']['lastname'],
                "customer_email" => $orderDetails[0]['orderCustomer']['email'],
                "customer_add" => $orderDetails[0]['orderCustomer']['userDetails']['address'],
                "customer_city" => $orderDetails[0]['orderCustomer']['userDetails']['city'],
                "customer_country" => $orderDetails[0]['orderCustomer']['userDetails']['country'],
                "customer_postcode" => $orderDetails[0]['orderCustomer']['userDetails']['zipcode'],
                "customer_phone" => $orderDetails[0]['orderCustomer']['phone'],
            );
            $shipping_info = array(
                "shipping_name" => $orderDetails[0]['orderCustomer']['firstname'] . ' ' . $orderDetails[0]['orderCustomer']['lastname'],
                "shipping_add" => $orderDetails[0]['orderCustomer']['userDetails']['shipping_address'],
                "shipping_city" => $orderDetails[0]['orderCustomer']['userDetails']['shipping_city'],
                "shipping_country" => $orderDetails[0]['orderCustomer']['userDetails']['shipping_country'],
                "shipping_postCode" => $orderDetails[0]['orderCustomer']['userDetails']['shipping_zipcode'],
            );
            $walletmix->set_shipping_charge(0);
            $walletmix->set_discount(0);

            $product_1 = array('name' => 'Adata 16GB Pendrive', 'price' => 15, 'quantity' => 2);
            $product_2 = array('name' => 'Adata 8GB Pendrive', 'price' => 5, 'quantity' => 1);

            $products = array($product_1, $product_2);

            $walletmix->set_product_description($products);
            
            $walletmix->set_amount(15);

            $walletmix->set_merchant_order_id($order_id);

            $walletmix->set_app_name('themallbd.com');
            $walletmix->set_currency('BDT');
            $walletmix->set_callback_url('http://themallbd.com/callback.php');

            $extra_data = array('param_1' => 'data_1', 'param_2' => 'data_2', 'param_3' => 'data_3');
            $walletmix->set_extra_json($extra_data);

            $walletmix->set_transaction_related_params($customer_info);
            $walletmix->set_transaction_related_params($shipping_info);

            $walletmix->set_database_driver('session'); // options: "txt" or "session"

            $walletmix->send_data_to_walletmix($this->walletmixGatewayUrl . "check-server");
        }
    }

}
