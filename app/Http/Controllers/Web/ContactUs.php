<?php

/**
 * Created by PhpStorm.
 * User: mi
 * Date: 2/29/16
 * Time: 4:01 PM
 */

namespace App\Http\Controllers\Web;

use App\Http\Controllers\BaseMallBDController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactUs extends BaseMallBDController {

    public function index() {

        return view('web.contactus.index', $this->pageData);
    }

    public function sendContactusMail() {
//        return response()->json(['success' => TRUE, 'alldata' => 'asd'], 200);

        $inputs = array(
            'name' => Input::get('name'),
            'email' => Input::get('email'),
            'message' => Input::get('message'),
        );

        $rules = array(
            'name' => "required",
            'email' => "required|email",
            'message' => "required",
        );
        $validate = Validator::make($inputs, $rules);
        if ($validate->fails()) {
            $errors = $validate->errors();
            $errors = json_decode($errors);

            return response()->json(['success' => FALSE, 'message' => $errors], 422);
        } else {
            Mail::send('web.emails.contactus', ['name' => $inputs['name'], 'msg' => $inputs['message'], 'email' => $inputs['email']], function($message) {
                $message->to($this->contactusReceiver, 'Admin')->subject('Contact us mail');
            });
            if (count(Mail::failures()) > 0) {
                $errors = array('failure' => 'Failed to send email.');
                return response()->json(['success' => FALSE, 'message' => $errors], 422);
            }
            return response()->json(['success' => TRUE, 'pagol' => 'dagol'], 200);
        }
    }

}
