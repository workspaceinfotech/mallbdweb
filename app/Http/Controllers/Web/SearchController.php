<?php

namespace App\Http\Controllers\Web;

/**
 * Project  : mallbdweb
 * File     : SearchController.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 3/7/16 - 5:08 PM
 */
use App\Http\Controllers\BaseMallBDController;
use App\Model\AttributesModel;
use App\Model\CategoryModel;
use App\Model\ManufacturerModel;
use Illuminate\Http\Request;
use App\Model\ProductModel;

class SearchController extends BaseMallBDController {

    public function getProductByManufacturerId($id) {

        $productModel = new ProductModel();
        $data = $productModel->getProductByManufacturerId($id);

        echo $data;
    }

    public function getProductByKeyWord(Request $request) {

        //$category,$category2=null,$category3=null)
        /* Searching params for product by category and Attribute */
        /* Starts Here */
        $queryString = urldecode($request->input("product_query"));
        if ($queryString == null || $queryString == "") {
            
        }
        $this->pageData['previous_search'] = $request->input("product_query");


//////modified starts
        $q = $request->input("q");
        $selectedProductAttribute = [];
        $selectedPrice = new \stdClass();
        $searchProduct = false;
        $priceValidation = TRUE;
        $selectedManufacturersIdList = [];
        $parentCategoryId = 0;
        if ($q != null && $q != "") {
            $pa = [];
            $tmpQstar = json_decode($q);

            try {
                $selectedProductAttribute = (isset($tmpQstar->pa)) ? $tmpQstar->pa : [];
                $searchProduct = true;
            } catch (Exception $ex) {
                $selectedProductAttribute = [];
            }
            try {

                $selectedPrice = (isset($tmpQstar->price)) ? $tmpQstar->price : new \stdClass();

                if (!isset($selectedPrice->min) || !isset($selectedPrice->max)) {
                    $priceValidation = false;
                } else {
                    if ($selectedPrice->min <= 0) {
                        $priceValidation = false;
                    }
                    if ($selectedPrice->max <= 0) {
                        $priceValidation = false;
                    }
                }


                if (!$priceValidation) {
                    $selectedPrice = new \stdClass();
                } else {
                    $searchProduct = true;
                }
            } catch (Exception $ex) {
                $selectedProductAttribute = [];
            }
            try {

                $selectedManufacturersIdList = (isset($tmpQstar->manufacturers)) ? $tmpQstar->manufacturers : new \stdClass();

                if (!is_array($selectedManufacturersIdList)) {
                    $selectedManufacturersIdList = [];
                    $searchProduct = true;
                }
            } catch (Exception $ex) {
                $selectedManufacturersIdList = [];
            }
        } else {
            $selectedManufacturersIdList = [];
            $searchProduct = true;
            $priceValidation = false;
        }

        $this->pageData['selectedManufacturersIdList'] = $selectedManufacturersIdList;
        $this->pageData['selectedProductAttribute'] = $selectedProductAttribute;
        $this->pageData['selectPrice'] = $selectedPrice;
        /* End Here */


//        $mainCategoryForReturningProduct = $category;
//        $subcategoryList = [];
        $categoryIdList = [];
        //////modified ends

        $productModel = new ProductModel();
//
//        $currentPage =  ($request->input("page")==(0 || ""))?0:("page");
//        $productList = [];
//        if($searchProduct){
//            // Attribute selection search  Wise product
//
//            if($priceValidation){
//                $productList = $productModel->getProductBySearch($categoryIdList,$selectedProductAttribute,$selectedPrice->min,$selectedPrice->max,$selectedManufacturersIdList);
//            }else{
//                $productList = $productModel->getProductBySearch($categoryIdList,$selectedProductAttribute,-1,-1,$selectedManufacturersIdList);
//            }
//        }else{
//            // Category Wise product
//           // $productList = $productModel->getAllProductByCategory($categoryIdList);
//
//
//        }
//
//        $totalProduct = $productModel->countAllProductOfThisCategory($categoryIdList);
//
//        if(empty($productList))
//        {
//            $this->pageData['status'] = false;
//            $this->pageData['msg'] = "No Data received";
//            $this->pageData['subcategoryList'] = $subcategoryList;
//            $this->pageData['selectedCategory'] = $finalCategoryForProducts;
//        }
//
//        $this->pageData['status'] = true;
//        $this->pageData['msg'] = "No Data received";
//        $this->pageData['productList'] = $productList;
//        $this->pageData['subcategoryList'] = $subcategoryList;
//        $this->pageData['selectedCategory'] = $finalCategoryForProducts;
//        $this->pageData['totalProduct'] = $totalProduct;
//
//        $currentCategory = 0;
//        if($category3!=null){
//            $currentCategory = $category3;
//        }elseif($category2!=null){
//            $currentCategory = $category2;
//        }else{
//            $currentCategory = $category;
//        }
//
//        /* For Search selection  */
//
//        $this->pageData['categoryIdList'] = $categoryIdList;
//        $manufacturerModel = new ManufacturerModel();
//        $manufacturerModel->setCategoryIdList($categoryIdList);
//
//        $this->pageData['manufacturers'] = $manufacturerModel->getManufactureByCategoryIdList();
//
//        $this->pageData['minMaxPrice'] = $productModel->getMinMaxPrice();
//
//        $attributesModel = new AttributesModel();
//        $attributesModel->setCategoryIdList($categoryIdList);

        $userId = $this->appCredential->user->id;
        $productModel->setCurrentUserId($userId);

        $orderBy = 0;
        if ($request->input("orderBy") != null) {
            $orderBy = (is_numeric($request->input("orderBy"))) ? intval($request->input("orderBy")) : 0;
        }

        $limit = 0;
        if ($request->input("limit") != null) {
            $limit = (is_numeric($request->input("limit"))) ? intval($request->input("limit")) : 40;
        }
        $limit = ($limit == (0 || "")) ? 40 : $limit;
        $productModel->setCustomLimit($limit);
        $productModel->customOffset = ($request->input("page") == (0 || "")) ? 0 : $request->input("page");

        if ($priceValidation) {
            $productList = $productModel->getAllProductByMatchStringManufacturerAndPrice($queryString, $selectedPrice->min, $selectedPrice->max, $selectedManufacturersIdList, $orderBy);
            $totalProduct = $productModel->getAllProductByMatchStringManufacturerAndPriceCount($queryString, $selectedPrice->min, $selectedPrice->max, $selectedManufacturersIdList);
        } else {
            $productList = $productModel->getAllProductByMatchStringManufacturerAndPrice($queryString, -1, -1, $selectedManufacturersIdList, $orderBy);
            $totalProduct = $productModel->getAllProductByMatchStringManufacturerAndPriceCount($queryString, -1, -1, $selectedManufacturersIdList);
        }

//        $productList = $productModel->getAllProductByMatchString($queryString, $orderBy);
        $this->pageData['productList'] = $productList;
        $this->pageData['limit'] = $limit;

//        $totalProduct = $productModel->getSearchedProductCount($queryString);
        $this->pageData['totalProduct'] = $totalProduct;
        $currentPage = ($request->input("page") == (0 || "")) ? 0 : $request->input("page");

        $startProductNumber = $limit * $currentPage + 1;
        $endProductNumber = $limit * ($currentPage + 1);
        if ($endProductNumber > $totalProduct) {
            $endProductNumber = $totalProduct;
        }
        if ($endProductNumber < 0) {
            $endProductNumber = $totalProduct;
        }
        if ($startProductNumber < 0) {
            $startProductNumber = 1;
        }

        $results = $productModel->getBestSellerProductIdList();

        $bestSellerProductList = [];

        foreach ($results as $result) {
            $product = $productModel->getProductById($result->product_id);
            array_push($bestSellerProductList, $product);
        }

        $this->pageData['bestSellerProductList'] = $bestSellerProductList;


        $manufacturerModel = new ManufacturerModel();
//        $manufacturerModel->setCategoryIdList($categoryIdList);

        $this->pageData['manufacturers'] = $manufacturerModel->getManufactureIdList();

        $this->pageData['minMaxPrice'] = $productModel->getMinMaxPrice();



        $this->pageData['status'] = true;
        $this->pageData['selected'] = true;
        $this->pageData['selectedAction'] = $orderBy;
        $this->pageData['currentPage'] = $currentPage;
//        $this->pageData['minMaxPrice'] = 0;
        $this->pageData['productsAttributes'] = [];
        $this->pageData['queryString'] = $queryString;
        $this->pageData['startProductNumber'] = $startProductNumber;
        $this->pageData['endProductNumber'] = $endProductNumber;
//
//        $this->pageData['productsAttributes'] = $attributesModel->getByProductList();
        // For later use
        // Source found http://stackoverflow.com/questions/163809/smart-pagination-algorithm
        // https://code.google.com/archive/p/spaceshipcollaborative/wikis/PHPagination.wiki
        // $pagination = new Pagination();

        return view("web.search.index", $this->pageData);
    }

    public function getProductByTags(Request $request) {

        /* Starts Here */
        $queryString = urldecode($request->input("product_tag"));
        if ($queryString == null || $queryString == "") {
            
        }
        $this->pageData['previous_search'] = $request->input("product_tag");

        $productModel = new ProductModel();
        $userId = $this->appCredential->user->id;
        $productModel->setCurrentUserId($userId);

        $orderBy = 0;
        if ($request->input("orderBy") != null) {
            $orderBy = (is_numeric($request->input("orderBy"))) ? intval($request->input("orderBy")) : 0;
        }

        $limit = 0;
        if ($request->input("limit") != null) {
            $limit = (is_numeric($request->input("limit"))) ? intval($request->input("limit")) : 40;
        }
        $limit = ($limit == (0 || "")) ? 40 : $limit;
        $productModel->setCustomLimit($limit);
        $productModel->customOffset = ($request->input("page") == (0 || "")) ? 0 : $request->input("page");
        $productList = $productModel->getAllProductByTags($queryString, $orderBy);
        $this->pageData['productList'] = $productList;
        $this->pageData['limit'] = $limit;

        $totalProduct = $productModel->getAllProductByTagsCount($queryString, $orderBy);
        $this->pageData['totalProduct'] = $totalProduct;
        $currentPage = ($request->input("page") == (0 || "")) ? 0 : $request->input("page");

        $startProductNumber = $limit * $currentPage + 1;
        $endProductNumber = $limit * ($currentPage + 1);
        if ($endProductNumber > $totalProduct) {
            $endProductNumber = $totalProduct;
        }
        if ($endProductNumber < 0) {
            $endProductNumber = $totalProduct;
        }
        if ($startProductNumber < 0) {
            $startProductNumber = 1;
        }

        $results = $productModel->getBestSellerProductIdList();

        $bestSellerProductList = [];

        foreach ($results as $result) {
            $product = $productModel->getProductById($result->product_id);
            array_push($bestSellerProductList, $product);
        }

        $this->pageData['bestSellerProductList'] = $bestSellerProductList;

        $this->pageData['status'] = true;
        $this->pageData['selected'] = true;
        $this->pageData['selectedAction'] = $orderBy;
        $this->pageData['currentPage'] = $currentPage;
        $this->pageData['minMaxPrice'] = 0;
        $this->pageData['productsAttributes'] = [];
        $this->pageData['queryString'] = $queryString;
        $this->pageData['startProductNumber'] = $startProductNumber;
        $this->pageData['endProductNumber'] = $endProductNumber;


        return view("web.search.tags", $this->pageData);
    }

}
