<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\BaseMallBDController;
use App\Model\OrderModel;
use App\Model\CustomerPurchaseDiscountModel;
use Illuminate\Http\Request;

class CustomerPurchaseDiscountController extends BaseMallBDController {

    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store() {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function calculateOnPurchaseDiscount() {
//        return 10;

        if ($this->serviceResponse->responseStat->isLogin) {
            $orderModel = new OrderModel();
            $customerPurchaseDiscountModel = new CustomerPurchaseDiscountModel();
            
            $customerCompletedOrders = $orderModel->customerCompletedOrderById($this->appCredential->user->id);

            $presentDiscount = 0;
            $presentDiscountData = $customerPurchaseDiscountModel->getPresentDiscountForCheckout($customerCompletedOrders);

            if(sizeof($presentDiscountData) == 0){
                $this->serviceResponse->responseStat->status = false;
            }
            else{
                $this->serviceResponse->responseStat->status = true;
            }
            
             $this->serviceResponse->responseStat->msg = "Data Found";
            $this->serviceResponse->discountMessage = $this->pageData['discountMessage'];
            $this->serviceResponse->responseData = $presentDiscountData;
            return $this->response();
        } else {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "No Data Received";
            $this->serviceResponse->discountMessage = $this->pageData['discountMessage'];
            return $this->response();
        }
    }

}
