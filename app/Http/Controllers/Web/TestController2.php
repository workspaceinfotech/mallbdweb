<?php
/**
 * Created by PhpStorm.
 * User: mi
 * Date: 1/20/16
 * Time: 5:11 PM
 */

namespace App\Http\Controllers\Web;

use App\Http\Controllers\BaseMallBDController;
use App\Model\DataModel\OrderProducts;
use App\Model\DataModel\PaymentMethods;
use App\Model\DataModel\Product;
use App\Model\DataModel\ProductCartFront;
use App\Model\NotificationModel;
use App\Model\OrderStatusModel;
use App\Model\PackageModel;
use App\Model\PackageProductModel;
use App\Model\PaymentMethodsModel;
use App\Model\ProductCartFrontModel;
use App\Model\ProductDiscountModel;
use App\Model\ProductQuantityModel;
use App\Model\ReviewModel;
use App\Model\ZoneModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\OrderAttributeModel;
use App\Model\OrderPaymentsModel;
use App\Model\OrderModel;
use App\Model\OrderProductsModel;
use App\Model\ProductModel;
use App\Model\CategoryModel;
use Illuminate\Support\Facades\Validator;
use Mockery\CountValidator\Exception;
use App\Model\UserModel;
use App\Model\LoginModel;
use App\Model\DataModel\AuthCredential;
use App\Model\ShippingAddressModel;
use App\Model\UserDetailsModel;
use App\Model\ZonalOrderModel;
use App\Model\CustomerRewardModel;
use App\Model\ZonalDeliveryBoyModel;
use App\Model\CustomerWishListModel;
use App\Model\BannerModel;

class TestController2 extends BaseMallBDController
{
    public function wishListArray(Request $request)
    {


        $customerWishListModel = new CustomerWishListModel();

        $wishListedProductArray = $customerWishListModel->getWishListProductArray($this->appCredential->user->id);
        return $wishListedProductArray;

        $results = DB::table('order_products')
            ->select('product_id',DB::raw('COUNT(*) AS c'))
            ->havingRaw(DB::raw('c > 1'))
            ->groupBy('product_id')
            ->orderByRaw('c desc')
            ->limit(3)
            ->offset(0)
            ->get();

        $bestSellerProductList = [];

        $productModel = new ProductModel();

        foreach($results as $result)
        {
            $product = $productModel->getProductById($result->product_id);
            array_push($bestSellerProductList,$product);
        }


        //return $bestSellerProductList;




        $packageModel = new PackageModel();
        $packageModel->setCustomLimit(20);
        $packageModel->setCustomOffset(0);
        $testResult = $packageModel->getAll();
        return $testResult;

        $productModel = new ProductModel();
        $userId= $this->appCredential->user->id;
        $productModel->setCurrentUserId($userId);

        $productModel->setCustomLimit(5);
        $productModel->setCustomOffset(0);

        $result = $productModel->getFeaturedProducts(1);
        return $result;
        $productCartFrontModel = new ProductCartFrontModel();

        //$result = $productCartFrontModel->getAllProductCart();
        $productCartFrontModel->setUserId($request->input("userId"));
        $productCartFrontModel->setCart($request->input("cart"));

        $this->setError($productCartFrontModel->errorManager->errorObj);

        if(!$this->hasError())
        {
            $productCartFrontModel->save();
            $this->serviceResponse->responseStat->status = true;
            $this->serviceResponse->responseStat->msg="Insert Successful";
            $this->serviceResponse->responseData = $productCartFrontModel;
            return $this->response();
        }
        else{
            $this->serviceResponse->responseStat->status = FALSE;
            $this->serviceResponse->responseStat->msg="Insert Unsuccessful";
            return $this->response();
        }

    }

    public function search(Request $request){

        $categoryModel = new CategoryModel();
        $childId = $request->input("child_id");

        $result = $categoryModel->getAllParentWithChildList($childId);

        return $result;


    }

    public function index()
    {
        return view('web.home.test');
    }
    public function insertIntoCustomerWishList(Request $request)
    {
        $customerWishList = new CustomerWishListModel();
        $customerWishList->setCustomerId($request->input("customer_id"));
        $customerWishList->setProductId($request->input("product_id"));
        $this->appCredential->user->id;

        $this->setError($customerWishList->errorManager->errorObj);

        if(!$this->hasError())
        {
            if(($customerWishList->isDistinct($request->input("customer_id"),$request->input("product_id"))) && $this->serviceResponse->responseStat->isLogin )
            {
                if($customerWishList->save()){
                    $this->serviceResponse->responseStat->status = true;
                    $this->serviceResponse->responseStat->msg="Insert Successful";
                    $this->serviceResponse->responseData = $customerWishList;
                    return $this->response();
                }
                else{
                    $this->serviceResponse->responseStat->status = false;
                    $this->serviceResponse->responseStat->msg="Error in saving CustomerWishListModel";
                    return $this->response();
                }
            }
            else{
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseStat->msg="duplicate product not allowed";
                return $this->response();
            }

        }
        else{
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg="Error in input";
            return $this->response();
        }
    }

    function insertIntOZoneModel(Request $request)
    {
        $zoneModel = new ZoneModel();

        $zoneModel->setZoneCode($request->input("zone_code"));
        $zoneModel->setZoneCity($request->input("zone_city"));
        $zoneModel->setZoneArea($request->input("zone_area"));

        $this->setError($zoneModel->errorManager->errorObj);

        if(!$this->hasError())
        {
            if($zoneModel->save()){
                $this->serviceResponse->responseStat->status = true;
                $this->serviceResponse->responseStat->isLogin = false;
                $this->serviceResponse->responseStat->msg="Insert Successful";
                $this->serviceResponse->responseData = $zoneModel;
                return $this->response();
            }
            else{
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseStat->isLogin = false;
                $this->serviceResponse->responseStat->msg="Error in saving ZoneModel";
                return $this->response();
            }
        }
        else{
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->isLogin = false;
            $this->serviceResponse->responseStat->msg="Error in input";
            return $this->response();
        }

    }


    function insertIntoZonalDeliveryBoy(Request $request)
    {
        $zonalDeliveryBoyModel = new ZonalDeliveryBoyModel();

        $zonalDeliveryBoyModel->setZoneId($request->input("zone_id"));
        $zonalDeliveryBoyModel->setUserId($request->input("user_id"));

        $this->setError($zonalDeliveryBoyModel->errorManager->errorObj);

        if(!$this->hasError())
        {
            if($zonalDeliveryBoyModel->save()){
                $this->serviceResponse->responseStat->status = true;
                $this->serviceResponse->responseStat->isLogin = false;
                $this->serviceResponse->responseStat->msg="Insert Successful";
                $this->serviceResponse->responseData = $zonalDeliveryBoyModel;
                return $this->response();
            }
            else{
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseStat->isLogin = false;
                $this->serviceResponse->responseStat->msg="Error in saving ZonalDeliveryBoyModel";
                return $this->response();
            }
        }
        else{
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->isLogin = false;
            $this->serviceResponse->responseStat->msg="Error in input";
            return $this->response();
        }
    }


    function insertIntoCustomerReward(Request $request)
    {
        $customerRewardModel = new CustomerRewardModel();

        $customerRewardModel->setUserId($request->input("user_id"));
        $customerRewardModel->setOrderId($request->input("order_id"));
        $customerRewardModel->setDescription($request->input("description"));
        $customerRewardModel->setPoints($request->input("points"));

        $this->setError($customerRewardModel->errorManager->errorObj);

        if(!$this->hasError())
        {
            if($customerRewardModel->save()){
                $this->serviceResponse->responseStat->status = true;
                $this->serviceResponse->responseStat->isLogin = false;
                $this->serviceResponse->responseStat->msg="Checkout Successful";
                $this->serviceResponse->responseData = $customerRewardModel;
                return $this->response();
            }
            else{
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseStat->isLogin = false;
                $this->serviceResponse->responseStat->msg="Error in saving userDetailModel";
                return $this->response();
            }
        }
        else{
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->isLogin = false;
            $this->serviceResponse->responseStat->msg="Error in input";
            return $this->response();
        }
    }

    function insertCheckOut(Request $request)
    {
        $userModel = new UserModel();
        $userModel->setFirstname($request->input("first_name"));
        $userModel->setLastname($request->input("last_name"));
        $userModel->setEmail($request->input("email"));
        $userModel->setPhone($request->input("phone"));
        $userModel->setRoleId("1");
        $userModel->setStatus("Active");

        $this->setError($userModel->errorManager->errorObj);

        $userDetailModel = new UserDetailsModel();

        $userDetailModel->setAddress($request->input("address"));
        $userDetailModel->setCity($request->input("city"));
        $userDetailModel->setZipcode($request->input("zipcode"));
        $userDetailModel->setCountry($request->input("country"));
        $userDetailModel->setShippingAddress($request->input("shipping_address"));
        $userDetailModel->setShippingCountry($request->input("shipping_country"));
        $userDetailModel->setShippingZipcode($request->input("shipping_zipcode"));
        $userDetailModel->setShippingCity($request->input("shipping_city"));

        $this->setError($userDetailModel->errorManager->errorObj);

        if(!$this->hasError())
        {
            if($userModel->save())
            {
                $userDetailModel->setUserId($userModel->id);

                if($userDetailModel->save())
                {
                    $this->serviceResponse->responseStat->status = true;
                    $this->serviceResponse->responseStat->isLogin = false;
                    $this->serviceResponse->responseStat->msg="Checkout Successful";
                    $this->serviceResponse->responseData = $userDetailModel;
                    return $this->response();
                }
                else
                {
                    $this->serviceResponse->responseStat->status = false;
                    $this->serviceResponse->responseStat->isLogin = false;
                    $this->serviceResponse->responseStat->msg="Error in saving userDetailModel";
                    return $this->response();
                }

            }
            else
            {
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseStat->isLogin = false;
                $this->serviceResponse->responseStat->msg="Error in saving userModel";
                return $this->response();
            }
        }
        else{
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->isLogin = false;
            $this->serviceResponse->responseStat->msg="Checkout failed";
            return $this->response();
        }



    }

    public function insertIntoZonalOrder(Request $request)
    {
        $zonalOrderModel = new ZonalOrderModel();

        $zonalOrderModel->setOrderId($request->input("order_id"));
        $zonalOrderModel->setZoneId($request->input("zone_id"));
        $zonalOrderModel->setUserId($request->input("user_id"));

        $this->setError($zonalOrderModel->errorManager->errorObj);

        if(!$this->hasError()) {

            $zonalOrderModel->save();
            $this->serviceResponse->responseStat->status = true;
            $this->serviceResponse->responseStat->isLogin = false;
            $this->serviceResponse->responseStat->msg="Successful";
            $this->serviceResponse->responseData = $zonalOrderModel;
            return $this->response();

        }else{

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->isLogin = false;
            $this->serviceResponse->responseStat->msg="fail";
            return $this->response();
        }
    }

    public function insertIntoUserDetails(Request $request)
    {
        $userDetailModel = new UserDetailsModel();

        $userDetailModel->setUserId($request->input("user_id"));
        $userDetailModel->setAddress($request->input("address"));
        $userDetailModel->setCountry($request->input("country"));
        $userDetailModel->setCity($request->input("city"));
        $userDetailModel->setZipcode($request->input("zipcode"));
        $userDetailModel->setShippingAddress($request->input("shipping_address"));
        $userDetailModel->setShippingCountry($request->input("shipping_country"));
        $userDetailModel->setShippingCity($request->input("shipping_city"));
        $userDetailModel->setShippingZipcode($request->input("shipping_zipcode"));

        $this->setError($userDetailModel->errorManager->errorObj);

        if(!$this->hasError()) {

            $userDetailModel->save();
            $this->serviceResponse->responseStat->status = true;
            $this->serviceResponse->responseStat->isLogin = false;
            $this->serviceResponse->responseStat->msg="Successful";
            $this->serviceResponse->responseData = $userDetailModel->all();
            return $this->response();

        }else{

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->isLogin = false;
            $this->serviceResponse->responseStat->msg="fail";
            return $this->response();
        }
    }

    public function insertIntoShippingAddress(Request $request)
    {
        $shoppingAddressModel = new ShippingAddressModel();
        $shoppingAddressModel->setUserId($request->input("user_id"));
        $shoppingAddressModel->setShippingAddress($request->input("shipping_address"));
        $shoppingAddressModel->setShippingCountry($request->input("shipping_country"));
        $shoppingAddressModel->setShippingCity($request->input("shipping_city"));
        $shoppingAddressModel->setShippingZipcode($request->input("shipping_zipcode"));

        $this->setError($shoppingAddressModel->errorManager->errorObj);

        if(!$this->hasError()) {
            $shoppingAddressModel->save();
            $this->serviceResponse->responseStat->status = true;
            $this->serviceResponse->responseStat->isLogin = false;
            $this->serviceResponse->responseStat->msg="Successful";
            $this->serviceResponse->responseData = $shoppingAddressModel;
            return $this->response();
        }else{
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->isLogin = false;
            $this->serviceResponse->responseStat->msg="fail";
            return $this->response();
        }


    }

    public function loadView()
    {
        return view('test.view');
    }

    public function signUpProcess(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|max:55',
            'last_name' => 'required|max:55',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',  // |confirm
            'phone'     => 'required'
        ]);

        if ($validator->fails())
        {

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->isLogin = false;
            $this->serviceResponse->responseStat->msg=$validator->errors()->first();
            return $this->response();
        }

        $userModel = new UserModel();
        $userModel->setFirstname($request->input("first_name"));
        $userModel->setLastname($request->input("last_name"));
        $userModel->setEmail($request->input("email"));
        $userModel->setRoleId("1");
        $userModel->setStatus("Active");
        $userModel->setPhone($request->input("phone"));
        //$userModel->save();

        $loginModel = new LoginModel();
        $loginModel->setEmail($request->input("email"));
        $loginModel->setPassword($request->input("password"));
        $loginModel->setAccesstoken($request->input("email").$request->input("password").date("YMDHis"));
        $this->setError($userModel->errorManager->errorObj);
        $this->setError($loginModel->errorManager->errorObj);

        if(!$this->hasError()){
            if(!($userModel->isDuplicateEmail($request->input("email"))))
            {
                if($userModel->save())
                {
                    $loginModel->setUserId($userModel->id);
                    $loginModel->save();
                    $authCredential = new AuthCredential();
                    $authCredential->castMe($loginModel);

                    $this->serviceResponse->responseStat->status = true;
                    $this->serviceResponse->responseStat->isLogin = false;
                    $this->serviceResponse->responseStat->msg="Registration Successful";
                    $this->serviceResponse->responseData = $authCredential;
                    return $this->response();
                }
                else
                {
                    $this->serviceResponse->responseStat->status = false;
                    $this->serviceResponse->responseStat->isLogin = false;
                    $this->serviceResponse->responseStat->msg="Registration fail";
                    return $this->response();
                }
            }else{
                $this->serviceResponse->responseStat->status=false;
                $this->serviceResponse->responseStat->msg = "Email already exists";
            }

        }else{
            $this->serviceResponse->responseStat->status=false;
            $this->serviceResponse->responseStat->msg = "Error found on UserModel";
        }
    }

}