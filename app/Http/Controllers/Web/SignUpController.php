<?php
/**
 * Project  : mallbdweb
 * File     : SignUpController.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 3/21/16 - 12:27 PM
 */
namespace App\Http\Controllers\Web;

use App\Http\Controllers\BaseMallBDController;

class SignUpController extends BaseMallBDController{

    public function create()
    {
        if($this->serviceResponse->responseStat->isLogin){
            return redirect('/home');
        }
        $this->pageData['redirectMainPage'] = true;
        return view("web.registration.index",$this->pageData);
    }
}