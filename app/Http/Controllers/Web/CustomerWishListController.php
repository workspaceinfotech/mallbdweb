<?php

/**
 * Created by PhpStorm.
 * Project: MallBDFront
 * File: CustomerWishListController.php
 * User: rajib
 * Date: 2/4/16
 * Time: 3:16 PM
 */

namespace App\Http\Controllers\Web;

use App\Http\Controllers\BaseMallBDController;
use App\Model\CustomerWishListModel;
use App\Model\ProductModel;
use Illuminate\Http\Request;

class CustomerWishListController extends BaseMallBDController {

    public function getWishListedProducts(Request $request) {
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        $customerId = $this->appCredential->user->id;
        $customerWishListModelObj = new CustomerWishListModel();

        $limit = 0;
        if ($request->input("limit") != null) {
            $limit = (is_numeric($request->input("limit"))) ? intval($request->input("limit")) : 10;
        }

        $limit = ($limit == (0 || "")) ? 10 : $limit;
        $customerWishListModelObj->setCustomLimit($limit);
        $customerWishListModelObj->customOffset = ($request->input("page") == (0 || "")) ? 0 : $request->input("page");
        $currentPage = ($request->input("page") == (0 || "")) ? 0 : $request->input("page");



        $products = $customerWishListModelObj->getWishListedProductsById($customerId);
        $productList = [];
        foreach ($products as $product) {
            foreach ($product->products as $p) {
                array_push($productList, $p);
            }
        }

        $startProductNumber = $limit * $currentPage + 1;
        $endProductNumber = $limit * ($currentPage + 1);

        $totalProduct = $customerWishListModelObj->getCountWishListedProductsById($customerId);
        if ($endProductNumber > $totalProduct) {
            $endProductNumber = $totalProduct;
        }
         $productModel = new ProductModel();
        $results = $productModel->getBestSellerProductIdList();

        $bestSellerProductList = [];

        foreach ($results as $result) {
            $product = $productModel->getProductById($result->product_id);
            array_push($bestSellerProductList, $product);
        }

        $this->pageData['bestSellerProductList'] = $bestSellerProductList;
        
        $this->pageData['totalProduct'] = $totalProduct;
        $this->pageData['currentPage'] = $currentPage;
        $this->pageData['status'] = true;
        $this->pageData['selected'] = true;
        $this->pageData['limit'] = $limit;
        $this->pageData['startProductNumber'] = $startProductNumber;
        $this->pageData['endProductNumber'] = $endProductNumber;

        $this->pageData['products'] = $productList;
        return view('web.wishlist.index', $this->pageData);
    }

    public function addProductToCustomerWishList(Request $request) {
        $customerWishList = new CustomerWishListModel();
        $customerWishList->setCustomerId($this->appCredential->user->id);
        $customerWishList->setProductId($request->input("product_id"));
        $this->setError($customerWishList->errorManager->errorObj);

        if (!$this->hasError()) {
            if (($customerWishList->isDistinct($request->input("customer_id"), $request->input("product_id"))) && $this->serviceResponse->responseStat->isLogin) {
                if ($customerWishList->save()) {
                    $this->serviceResponse->responseStat->status = true;
                    $this->serviceResponse->responseStat->msg = "Insert Successful";
                    $this->serviceResponse->responseData = $customerWishList;
                    return $this->response();
                } else {
                    $this->serviceResponse->responseStat->status = false;
                    $this->serviceResponse->responseStat->msg = "Error in saving CustomerWishListModel";
                    return $this->response();
                }
            } else {
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseStat->msg = "duplicate product not allowed";
                return $this->response();
            }
        } else {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "Error in input";
            return $this->response();
        }
    }

}
