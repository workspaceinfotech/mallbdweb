<?php
/**
 * Created by PhpStorm.
 * Project: MallBDFront
 * File: CategoryController.php
 * User: rajib
 * Date: 1/26/16
 * Time: 12:34 PM
 */

namespace App\Http\Controllers\Web;


use App\Http\Controllers\BaseMallBDController;
use App\Model\CategoryModel;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;

class CategoryController extends BaseMallBDController{


    public function parentsCategory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'shop_id' => 'required|Integer',
        ]);

        if ($validator->fails())
        {

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg=$validator->errors()->first();
            return $this->response();
        }

        $shop = $request->input("shop_id");

        $categoryModel = new CategoryModel();
        $categoryList = $categoryModel->getAllParentsCategory($shop);

        // return $categories;

        if(empty($categoryList))
        {
            $this->serviceResponse->responseStat->status=false;
            $this->serviceResponse->responseStat->msg="No Data Received";
            return $this->response();
        }
        //   return $categoryList;

        $this->serviceResponse->responseStat->status=true;
        $this->serviceResponse->responseStat->msg="Data received";
        $this->serviceResponse->responseData = $categoryList;
        return $this->response();

    }

    public function childsCategory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'shop_id' => 'required|Integer',
            'parent_id' => 'required|Integer'
        ]);

        if ($validator->fails())
        {

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg=$validator->errors()->first();
            return $this->response();
        }

        $shop = $request->input("shop_id");
        $parent_id = $request->input("parent_id");

        $categoryModel = new CategoryModel();
        $categoryList = $categoryModel->getAllChildsCategory($shop,$parent_id);

        // return $categories;

        if(empty($categoryList))
        {
            $this->serviceResponse->responseStat->status=false;
            $this->serviceResponse->responseStat->msg="No Data Received";
            return $this->response();
        }
        //   return $categoryList;

        $this->serviceResponse->responseStat->status=true;
        $this->serviceResponse->responseStat->msg="Data received";
        $this->serviceResponse->responseData = $categoryList;
        return $this->response();
    }

    public function allCategories(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'shop_id' => 'required|Integer',
        ]);

        if ($validator->fails())
        {

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg=$validator->errors()->first();
            return $this->response();
        }

        $shop = $request->input("shop_id");

        $categoryModel = new CategoryModel();
        $categoryList = $categoryModel->getAllCategory($shop);

        // return $categories;

        if(empty($categoryList))
        {
            $this->serviceResponse->responseStat->status=false;
            $this->serviceResponse->responseStat->msg="No Data Received";
            return $this->response();
        }
        //   return $categoryList;

        $this->serviceResponse->responseStat->status=true;
        $this->serviceResponse->responseStat->msg="Data received";
        $this->serviceResponse->responseData = $categoryList;
        return $this->response();
    }


} 