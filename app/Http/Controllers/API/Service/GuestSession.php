<?php
/**
 * Created by PhpStorm.
 * User: mi
 * Date: 2/4/16
 * Time: 2:44 PM
 */

namespace App\Http\Controllers\API\Service;


use App\Http\Controllers\BaseMallBDController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class GuestSession  extends BaseMallBDController
{
    public function setCart(Request $request){
        //Session::set('cart','4554');
        $request->session()->put('cart',$request->input('cart'));
        return response()->json($data = $request->session()->get('cart'));
    }
    public function getCart(Request $request){
        if($request->session()->has('cart')){
            $this->serviceResponse->responseData =  json_decode($data = $request->session()->get('cart'));
        }else{
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "No cart record found";
        }
        return $this->response();
    }
}