<?php
/**
 * Project  : mallbdweb
 * File     : PackageService.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 4/6/16 - 2:56 PM
 */

namespace App\Http\Controllers\API\Service;

use App\Http\Controllers\BaseMallBDController;
use App\Model\PackageModel;
use Illuminate\Http\Request;

class PackageService extends BaseMallBDController{

    function getAllPackages(Request $request)
    {
        $packageModel = new PackageModel();

        $limit = $request->input("limit");
        $offset = $request->input("offset");
        if($limit=="")
        {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg="limit can't be empty!";
            return $this->response();
        }

        if($offset=="")
        {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg="offset can't be empty!";
            return $this->response();
        }
        $packageModel->setCustomLimit($limit);
        $packageModel->setCustomOffset($offset);

        $allPackages = $packageModel->getAll();

       
        if(empty($allPackages))
        {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "No Package Found";
            return $this->response();
        }

        $this->serviceResponse->responseStat->status = true;
        $this->serviceResponse->responseStat->msg = "Data Found";
        $this->serviceResponse->responseData = $allPackages;
        return $this->response();
    }
    
    function getAllPackagesMobile(Request $request)
    {
        $packageModel = new PackageModel();

        $limit = $request->input("limit");
        $offset = $request->input("offset");
        if($limit=="")
        {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg="limit can't be empty!";
            return $this->response();
        }

        if($offset=="")
        {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg="offset can't be empty!";
            return $this->response();
        }
        $packageModel->setCustomLimit($limit);
        $packageModel->setCustomOffset($offset);

        $allPackages = $packageModel->getAllMobile();

       
        if(empty($allPackages))
        {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "No Package Found";
            return $this->response();
        }

        $this->serviceResponse->responseStat->status = true;
        $this->serviceResponse->responseStat->msg = "Data Found";
        $this->serviceResponse->responseData = $allPackages;
        return $this->response();
    }

    public function getPackageById(Request $request)
    {
        $packageId = $request->input("package_id");

        if($packageId=="" || (!is_numeric($packageId)))
        {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg="offset can't be empty/must be numeric!";
            return $this->response();
        }

        $packageModel = new PackageModel();
        $packageModel->setId($packageId);
        $package = $packageModel->getPackageByIdMobile();
        if(empty($package))
        {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "No Package Found";
            return $this->response();
        }
        $this->serviceResponse->responseStat->status = true;
        $this->serviceResponse->responseStat->msg = "Data Found";
        $this->serviceResponse->responseData = $package;
        return $this->response();
    }

}