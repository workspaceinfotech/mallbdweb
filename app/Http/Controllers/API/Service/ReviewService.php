<?php
/**
 * Project  : mallbdweb
 * File     : ReviewService.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 3/24/16 - 2:57 PM
 */
namespace App\Http\Controllers\Web;
namespace App\Http\Controllers\API\Service;
use App\Http\Controllers\BaseMallBDController;
use App\Model\ProductModel;
use Illuminate\Http\Request;
use App\Model\ReviewModel;

class ReviewService extends BaseMallBDController {

    function addReview(Request $request){

        $customerId = $this->appCredential->user->id;

        if($customerId<1){

            $this->serviceResponse->responseStat->status = FALSE;
            $this->serviceResponse->responseStat->msg="You must login first";
            return $this->response();
        }

        $reviewModel = new ReviewModel();

        $reviewModel->setProductId($request->input("product_id"));
        $reviewModel->setCustomerId($customerId);
        $reviewModel->setTitle($request->input("title"));
        $reviewModel->setNote($request->input("note"));
        $reviewModel->setRating($request->input("rating"));

        if((strlen($request->input("rating")<1)) && (strlen($request->input("note"))<1))
        {
            $this->serviceResponse->responseStat->status = FALSE;
            $this->serviceResponse->responseStat->msg="Must contain rating/review";
            return $this->response();
        }
        //return $reviewModel->customer_id;

        if($reviewModel->isDuplicate($reviewModel->customer_id,$reviewModel->product_id))
        {
            $this->serviceResponse->responseStat->status = FALSE;
            $this->serviceResponse->responseStat->msg="Duplicate Review not allowed";
            return $this->response();
        }

        if(!$this->hasError())
        {
            if($reviewModel->save())
            {
                $reviewId = $reviewModel->id;
                $productModel = ProductModel::where("id","=",$reviewModel->product_id)->first();

                $productModel->avg_rating = ReviewModel::where("product_id","=",$reviewModel->product_id)->avg('rating');

                if($productModel->save())
                {
                    $reviewDetails['allReviews'] = $reviewModel->getReviewById($reviewId);
                    $this->serviceResponse->responseStat->status = true;
                    $this->serviceResponse->responseStat->msg="Review added Successful";
                    $this->serviceResponse->responseData = $reviewModel;
                    $this->serviceResponse->htmlData = ''.view('web.review.partial.partial_review',$reviewDetails);
                    return $this->response();
                }else{
                    $this->serviceResponse->responseStat->status = false;
                    $this->serviceResponse->responseStat->msg="Product Table Update Failed";
                    return $this->response();
                }


            }else{
                $this->serviceResponse->responseStat->status = FALSE;
                $this->serviceResponse->responseStat->msg="Review unsuccessful";
                return $this->response();
            }
        }else{

            $this->serviceResponse->responseStat->status = FALSE;
            $this->serviceResponse->responseStat->msg="Review unsuccessful";
            return $this->response();
        }

    }

    public function getAllReviewsByProduct(Request $request)
    {
        $productId = $request->input("product_id");
        $limit = $request->input("limit");
        $offset = $request->input("offset");

        if(($productId=="") || ($limit=="") || ($offset=="")  )
        {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg="limit/offset/productId can't be empty!";
            return $this->response();
        }


        $reviewModel = new ReviewModel();
        $reviewModel->setCustomOffset($offset);
        $reviewModel->setCustomLimit($limit);

        $reviews = $reviewModel->getAllReviewByProduct($productId);

        //return $reviews;
        if($reviews==null)
        {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg="No Review Found";
            return $this->response();

        }else{
            $this->serviceResponse->responseStat->status=TRUE;
            $this->serviceResponse->responseStat->msg="Data found";
            $this->serviceResponse->responseData=$reviews;
            return $this->response();
        }

    }

    public function countReview(Request $request)
    {
        $productId = $request->input("product_id");

        $countReview = ReviewModel::where("product_id","=",$productId)->count();

        if($productId !=0)
        {
            $this->serviceResponse->responseStat->status = TRUE;
            $this->serviceResponse->responseStat->msg="Review Count Found";
            $this->serviceResponse->responseData=$countReview;
            return $this->response();
        }
        else{
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg="No Review Count Found";
            return $this->response();
        }

    }
}