<?php
/**
 * Created by PhpStorm.
 * User: rafsan
 * Date: 18/2/15
 * Time: 5:25 PM
 */

namespace App\Http\Controllers\API\Service;


use App\Http\Controllers\BaseMallBDController;
use App\Model\OrderAttributeModel;
use App\Model\ProductQuantityModel;
use Illuminate\Http\Request;
use App\Model\OrderModel;
use App\Model\OrderPaymentsModel;
use App\Model\OrderProductsModel;
use App\Model\OrderStatusModel;

class OrderService  extends BaseMallBDController
{

    function order(Request $request) // order and orderPayment
    {
        $orderModel = new OrderModel();

        $orderModel->setShopId($request->input("shop_id"));
        $orderModel->setZoneId($request->input("zone_id"));
        $orderModel->setInvoiceNo($request->input("invoice_no"));
        $orderModel->setCustomerId($request->input("customer_id"));
        $orderModel->setCurrencyId($request->input("currency_id"));
        $orderModel->setCurrencyCode($request->input("currency_code"));
        $orderModel->setCurrencyValue($request->input("currency_value"));
        $orderModel->setOrderTotal($request->input("order_total"));
        $orderModel->setOrderFrom($request->input("order_from"));
        $orderModel->setPackageSize($request->input("package_size"));
        $orderModel->setPackageWeight($request->input("package_weight"));
        $orderModel->setShippingAddress($request->input("shipping_address"));
        $orderModel->setShippingCountry($request->input("shipping_country"));
        $orderModel->setShippingCity($request->input("shipping_city"));
        $orderModel->setShippingZipcode($request->input("shipping_zipcode"));
        $orderModel->setIsWrapped($request->input("is_wrapped"));
        $orderModel->setWrappedNote($request->input("wrapped_note"));

        $this->setError($orderModel->errorManager->errorObj);

        if(!$this->hasError()){

            $orderModel->save();


        }else{
            $this->serviceResponse->responseStat->msg = "Error found on Order Place Request";
        }

        $orderPaymentsModel = new OrderPaymentsModel();
        $orderId = $orderModel->id;
        $orderPaymentsModel->setOrderId($orderModel->id);
        $orderPaymentsModel->setPaymentMethodId($request->input("payment_method_id"));
        $orderPaymentsModel->setPaymentTotal($request->input("payment_total"));
        $orderPaymentsModel->setPaymentDetails($request->input("payment_details"));
        $orderPaymentsModel->setPaymentStatus($request->input("payment_status"));

        $this->setError($orderPaymentsModel->errorManager->errorObj);

        if(!$this->hasError()){

            $orderPaymentsModel->save();

        }else{

            $this->serviceResponse->responseStat->msg = "Error found on Order Payment Request";
        }

        //inserting into orderProducts

        $input = $request->input("shopping_cart");

        $shopping_cart = json_decode($input, true);
        $reducedShoppingCart = array();
        foreach($shopping_cart['shoppingCartCell'] as $cart)
        {
            if(array_key_exists($cart['id'],$reducedShoppingCart))
            {
                $reducedShoppingCart[$cart['id']]['quantity'] += $cart['quantity'];
            }
            else {
                $reducedShoppingCart[$cart['id']] = array();
                $reducedShoppingCart[$cart['id']]['quantity'] = $cart['quantity'];
                $reducedShoppingCart[$cart['id']]['price'] = $cart['product']['prices'][0]['retailPrice'];
            }
        }
        foreach($reducedShoppingCart as $key =>$value) {

            $orderProductsModelObj = new OrderProductsModel();
            $orderProductsModelObj->setOrderId($orderId);
            $orderProductsModelObj->setProductId($key);
            $orderProductsModelObj->setPrice($value['price']);
            $orderProductsModelObj->setProductQuantity($value['quantity']);
            $orderProductsModelObj->setTotal($value['price']*$value['quantity']);
            $orderProductsModelObj->setDiscountId(1);
            $orderProductsModelObj->setTax(0.0);

            $this->setError($orderProductsModelObj->errorManager->errorObj);

            if(!$this->hasError())
            {
                $orderProductsModelObj->save();
            }
            else{
                $this->serviceResponse->responseStat->msg = "Error inserting in order products";
            }

        }

        foreach($shopping_cart['shoppingCartCell'] as $cart)
        {
            $attributeList=[];
            $productId=0;
            $combinationKey=0;
            if($cart['selectedAttributes'] != null)
            {
                foreach ($cart['selectedAttributes'] as $attribute)
                {
                    $productId = $cart['id'];
                    array_push($attributeList, $attribute['id']);

                }
                $strOfArr = implode(",", $attributeList);
                $strOfArr = trim($strOfArr);
                $result   = ProductQuantityModel::where('product_id', '=', $productId)
                                                ->where('combination', '=', $strOfArr)
                                                ->get()->first();
                $combinationKey = $result['combination_key'];
            }

            $productId = $cart['id'];
            $quantity= $cart['quantity'];

            $orderAttrModel = new OrderAttributeModel();

            $orderAttrModel->setOrderId($orderId);
            $orderAttrModel->setProductId($productId);
            $orderAttrModel->setCombinationKey($combinationKey);
            $orderAttrModel->setQuantity($quantity);

            $this->setError($orderAttrModel->errorManager->errorObj);

            if(!$this->hasError()){

                $orderAttrModel->save();


            }else{
                $this->serviceResponse->responseStat->msg = "Error found on Order Place Request";
            }

        }

        return $this->response();
    }



    function orderProducts(Request $request)
    {
        $orderProductsModel = new OrderProductsModel();

        $orderProductsModel->setOrderId($request->input("order_id"));
        $orderProductsModel->setProductId($request->input("product_id"));
        $orderProductsModel->setDiscountId($request->input("discount_id"));
        $orderProductsModel->setProductQuantity($request->input("product_quantity"));
        $orderProductsModel->setPrice($request->input("price"));
        $orderProductsModel->setTotal($request->input("total"));
        $orderProductsModel->setTax($request->input("tax"));
        $orderProductsModel->setCreatedOn($request->input("created_on"));

        $this->setError($orderProductsModel->errorManager->errorObj);

        if(!$this->hasError()){

            $orderProductsModel->save();

        }else{

            $this->serviceResponse->responseStat->msg = "Error found on Request";
        }

        return $this->response();

    }

    function orderStatus(Request $request)
    {
        $orderStatusModel = new OrderStatusModel();

        $orderStatusModel->setOrderId($request->input("order_id"));
        $orderStatusModel->setStatus($request->input("status"));
        $orderStatusModel->setComments($request->input("comments"));
        $orderStatusModel->setCreatedBy($request->input("created_by"));

        $this->setError($orderStatusModel->errorManager->errorObj);

        if(!$this->hasError()){

            $orderStatusModel->save();

        }else{

            $this->serviceResponse->responseStat->msg = "Error found on Request";
        }

        return $this->response();
    }

    public function getOrderById()
    {
        $userId = $this->appCredential->user->id;
        $orderModel = new OrderModel();
        $orderModel->setCustomLimit(3);
        $orderModel->setCustomOffset(0);
        $result = $orderModel->getOrderByUserId($userId);

        if($result !=null )
        {
            $this->serviceResponse->responseStat->msg = "Data Found";
            $this->serviceResponse->responseStat->status = true;
            $this->serviceResponse->responseData = $result;
            return $this->response();
        }
        else{
            $this->serviceResponse->responseStat->msg = "No Data Found";
            $this->serviceResponse->responseStat->status = false;
            return $this->response();
        }


    }
     public function getOrderByIdMobile()
    {
        $userId = $this->appCredential->user->id;
        $orderModel = new OrderModel();
//        $orderModel->setCustomLimit(3);
//        $orderModel->setCustomOffset(0);
        $result = $orderModel->getOrderByUserIdMobile($userId);

        if($result !=null )
        {
            $this->serviceResponse->responseStat->msg = "Data Found";
            $this->serviceResponse->responseStat->status = true;
            $this->serviceResponse->responseData = $result;
            return $this->response();
        }
        else{
            $this->serviceResponse->responseStat->msg = "No Data Found";
            $this->serviceResponse->responseStat->status = false;
            return $this->response();
        }


    }

}