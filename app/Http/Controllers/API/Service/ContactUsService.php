<?php
/**
 * Created by PhpStorm.
 * User: rafsan
 * Date: 18/2/15
 * Time: 5:25 PM
 */
namespace App\Http\Controllers\API\Service;

use App\Http\Controllers\BaseMallBDController;
use App\Model\ContactUsModel;
use Illuminate\Http\Request;

class ContactUsService  extends BaseMallBDController{

    function addContactUs(Request $request){

        $contactUsModel = new ContactUsModel();

        $contactUsModel->setName($request->input("name"));
        $contactUsModel->setEmail($request->input("email"));
        $contactUsModel->setSubject($request->input("subject"));
        $contactUsModel->setMessage($request->input("message"));

        if((strlen($request->input("name"))<1) || (strlen($request->input("email"))<1) || (strlen($request->input("subject"))<1))
        {
            $this->serviceResponse->responseStat->status = FALSE;
            $this->serviceResponse->responseStat->msg="Please fill up the required fields";
            return $this->response();
        }

        $this->setError($contactUsModel->errorManager->errorObj);

        if($contactUsModel->save()){
            $this->serviceResponse->responseStat->status = TRUE;
            $this->serviceResponse->responseStat->msg="Successfully submitted";
            $this->serviceResponse->responseData=$contactUsModel;
            return $this->response();
        }
        else{
            $this->serviceResponse->responseStat->status = FALSE;
            $this->serviceResponse->responseStat->msg="Submission unsuccessful";
            return $this->response();
        }



    }

}

