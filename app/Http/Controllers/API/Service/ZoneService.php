<?php

namespace App\Http\Controllers\API\Service;
use App\Http\Controllers\BaseMallBDController;
use App\Model\ZoneModel;

use App\Http\Requests;

/**
 * Project  : mallbdweb
 * File     : ZoneService.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 2/23/16 - 11:54 AM
 */

class ZoneService extends BaseMallBDController {

    public function getAllZones()
    {
        $result = new ZoneModel();
        $zoneList = $result->getAllZones();

        if(empty($zoneList))
        {
            $this->serviceResponse->responseStat->status=false;
            $this->serviceResponse->responseStat->msg="No Data Received";
            return $this->response();
        }

        $this->serviceResponse->responseStat->status = true;
        $this->serviceResponse->responseStat->msg = "Data Found";
        $this->serviceResponse->responseData = $zoneList;
        return $this->response();

    }
}
