<?php
/**
 * Created by PhpStorm.
 * User: rafsan
 * Date: 18/2/15
 * Time: 5:25 PM
 */
namespace App\Http\Controllers\API\Service;

use App\Model\UserModel;
use App\Model\UserDetailsModel;
use App\Http\Controllers\BaseMallBDController;
use Illuminate\Http\Request;

class MyAccountService  extends BaseMallBDController {

    function update(Request $request)
    {
        $userId = $this->appCredential->user->id;
        $firstName  = $request->input("firstname");
        $lastName  = $request->input("lastname");
        $phone  = $request->input("phone");

        $user = UserModel::find($userId);
        $user->firstname = $firstName;
        $user->lastname = $lastName;
        $user->phone = $phone;

        //$user->save();

        $address = $request->input("address");
        $zipcode = $request->input("zipcode");
        $city = $request->input("city");
        $country = $request->input("country");

        $userDetails = UserDetailsModel::where("user_id","=",$userId)
            ->update(array('address' => $address,'zipcode'=>$zipcode,'city'=>$city,'country'=>$country));



        if(($user->save()) && ($userDetails==1))
        {
            $this->serviceResponse->responseStat->status = TRUE;
            $this->serviceResponse->responseStat->msg = "Successfully Updated";
            $this->serviceResponse->responseData = $user;
            return $this->response();
        }
        else{
            $this->serviceResponse->responseStat->status = TRUE;
            $this->serviceResponse->responseStat->msg = "update failed/no data updated";
            return $this->response();
        }


    }


}