<?php
/**
 * Project  : mallbdweb
 * File     : ProductDiscountService.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 4/5/16 - 12:28 PM
 */

namespace App\Http\Controllers\API\Service;
use App\Http\Controllers\BaseMallBDController;
use App\Model\ProductDiscountModel;
use Illuminate\Http\Request;

class ProductDiscountService extends BaseMallBDController{

    function getDiscountByStartDate(Request $request)
    {
        $productDiscountModel = new ProductDiscountModel();

        if($request->input("start_date")=="")
        {
            $this->serviceResponse->responseStat->status=false;
            $this->serviceResponse->responseStat->msg="Start date is empty";
            return $this->response();
        }

        $result = $productDiscountModel->getProductDiscountByStartDate($request->input("start_date"));

        $this->serviceResponse->responseStat->status=TRUE;
        $this->serviceResponse->responseStat->msg="Start date is valid";
        $this->serviceResponse->responseData= $result;
        return $this->response();

    }

    function getDiscountByEndDate(Request $request)
    {
        $productDiscountModel = new ProductDiscountModel();

        if($request->input("end_date")=="")
        {
            $this->serviceResponse->responseStat->status=false;
            $this->serviceResponse->responseStat->msg="Start date is empty";
            return $this->response();
        }

        $result = $productDiscountModel->getProductDiscountByEndDate($request->input("end_date"));

        $this->serviceResponse->responseStat->status=TRUE;
        $this->serviceResponse->responseStat->msg="End date is valid";
        $this->serviceResponse->responseData= $result;
        return $this->response();

    }
}
