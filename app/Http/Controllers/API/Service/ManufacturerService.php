<?php

/**
 * Created by PhpStorm.
 * User: rafsan
 * Date: 18/2/15
 * Time: 5:25 PM
 */

namespace App\Http\Controllers\API\Service;

use App\Model\ManufacturerModel;
use App\Http\Controllers\BaseMallBDController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;

class ManufacturerService extends BaseMallBDController {

    function allManufacturer(Request $request) {
        $validator = Validator::make($request->all(), [
                    'limit' => 'required|Integer',
                    'offset' => 'required|Integer',
        ]);

        if ($validator->fails()) {

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = $validator->errors()->first();
            return $this->response();
        }

        $limit = $request->input("limit");
        $limit = ($limit == (0 || "")) ? 5 : $limit;
        $offset = ($request->input("offset") == (0 || "")) ? 0 : $request->input("offset");

        $manufacturers = ManufacturerModel::where('status', 'Active')
                ->limit($limit)->offset($offset * $limit)->get();

        if (count($manufacturers) < 1) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "No Data Received";
            return $this->response();
        }

        $this->serviceResponse->responseStat->status = true;
        $this->serviceResponse->responseStat->msg = "Data Found";
        $this->serviceResponse->responseData = $manufacturers;
        return $this->response();
    }

    function getAllManufacturerforWeb(Request $request) {

        $validator = Validator::make($request->all(), [
                    'limit' => 'required|Integer',
                    'offset' => 'required|Integer',
                        //'shop_id' => 'required|Integer'
        ]);

        if ($validator->fails()) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = $validator->errors()->first();
            return $this->response();
        }

        $limit = ($request->input("limit") == (0 || "")) ? 6 : $request->input("limit");
        $offset = $request->input("offset");

        $data['manufacturers'] = ManufacturerModel::where('status', 'Active')->orderBy(DB::raw('RAND()'))->skip($offset)->take($limit)->get();
        $count = ManufacturerModel::where('status', 'Active')->count();
        $data['count'] = $count;
        $data['nextpage'] = $offset;
        $data['lastpage'] = ceil($count / $limit);

        return $data;
    }

}
