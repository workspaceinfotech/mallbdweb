<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: CategoryService.php
 * User: rajib
 * Date: 1/13/16
 * Time: 6:48 PM
 */

namespace App\Http\Controllers\API\Service;
use App\Model\LoginModel;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseMallBDController;


class PasswordResetService extends BaseMallBDController {

    function index(Request $request)
    {
        $loginModel = new LoginModel();
        $customerId = $this->appCredential->user->id;
        $oldPassword = $request->input("o_password");
        $newPassword = bcrypt($request->input("n_password"));

        $passMatchFlag = $loginModel->matchPassword($oldPassword,$customerId);

        if($passMatchFlag)
        {
            if(!$this->hasError())
            {


                $loginModel->where("user_id","=",$customerId)
                    ->update(['password' => $newPassword]);


            }
            else{
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseStat->msg="Error in setting new password";
                return $this->response();
            }

        }else{
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg="Old password is not correct";
            return $this->response();
        }

        $this->setError($loginModel->errorManager->errorObj);

        if(!$this->hasError()) {
            $this->serviceResponse->responseStat->status = true;
            $this->serviceResponse->responseStat->msg="Password has been successfully reset.";
            return $this->response();
        }
        else{
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg="Error in reset passs";
            return $this->response();
        }

    }
}