<?php

/**
 * Created by PhpStorm.
 * Project: MallBDFront
 * File: CustomerWishListService.php
 * User: rajib
 * Date: 2/4/16
 * Time: 4:57 PM
 */

namespace App\Http\Controllers\API\Service;

use App\Http\Controllers\BaseMallBDController;
use App\Model\CategoryModel;
use App\Model\VoucherModel;
use App\Model\DataModel\Product;
use App\Model\MallBdItem_Model;
use App\Model\ProductModel;
use App\Model\TagModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class VoucherService extends BaseMallBDController {

    public function checkVoucher(Request $request) {

        $validator = Validator::make($request->all(), [
                    'voucher_code' => 'required'
        ]);

        if ($validator->fails()) {

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = $validator->errors()->first();
            return $this->response();
        }
        $voucher_code = $request->input("voucher_code");
        $voucher_data = VoucherModel::where('voucher_code', $voucher_code)->get();
        if (count($voucher_data) > 0) {

            //////modification starts

            if (time() >= strtotime($voucher_data[0]['start_date']) && time() < strtotime("+1 day", strtotime($voucher_data[0]['end_date']))) {
                if ($voucher_data[0]['number_of_time_use'] > 0) {
                    if ($voucher_data[0]['user_id'] == 0) {
                        $discount_percent = $voucher_data[0]->percent;
                        // $discount_amount=($total_price*$discount_percent)/100;
                        $this->serviceResponse->responseStat->status = true;
                        $this->serviceResponse->responseData->voucher_id = $voucher_data[0]->id;
                        $this->serviceResponse->responseData->voucher_code = $voucher_code;
                        $this->serviceResponse->responseData->discount = round($discount_percent);
                        return $this->response();
                    } else {
                        if ($this->serviceResponse->responseStat->isLogin) {
                            if ($this->appCredential->user->id == $voucher_data[0]['user_id']) {
                                $discount_percent = $voucher_data[0]->percent;
                                // $discount_amount=($total_price*$discount_percent)/100;
                                $this->serviceResponse->responseStat->status = true;
                                $this->serviceResponse->responseData->voucher_id = $voucher_data[0]->id;
                                $this->serviceResponse->responseData->voucher_code = $voucher_code;
                                $this->serviceResponse->responseData->discount = round($discount_percent);
                                return $this->response();
                            } else {
                                $this->serviceResponse->responseStat->status = false;
                                $this->serviceResponse->responseStat->msg = "The voucher is not applicable for you.";
                                $this->serviceResponse->responseStat->discount_amount = $voucher_data[0];
                                return $this->response();
                            }
                        } else {
                            $this->serviceResponse->responseStat->status = false;
                            $this->serviceResponse->responseStat->msg = "The voucher is not applicable for you.";
                            $this->serviceResponse->responseStat->discount_amount = $voucher_data[0];
                            return $this->response();
                        }
                    }
                } else {
                    $this->serviceResponse->responseStat->status = false;
                    $this->serviceResponse->responseStat->msg = "The voucher is no more available.";
                    $this->serviceResponse->responseStat->discount_amount = $voucher_data[0];
                    return $this->response();
                }
            } else {
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseStat->msg = "The voucher is expired.";
                $this->serviceResponse->responseStat->discount_amount = $voucher_data[0];
                return $this->response();
            }

            //////modification ends
//            $discount_percent = $voucher_data[0]->percent;
//            // $discount_amount=($total_price*$discount_percent)/100;
//            $this->serviceResponse->responseStat->status = true;
//            $this->serviceResponse->responseData->voucher_code = $voucher_code;
//            $this->serviceResponse->responseData->discount = round($discount_percent);
//            return $this->response();
        } else {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "Wrong Voucher Code";
            $this->serviceResponse->responseStat->discount_amount = 0;
            return $this->response();
        }
    }

}
