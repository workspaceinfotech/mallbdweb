<?php
/**
 * Project  : mallbdweb
 * File     : BannerService.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 3/28/16 - 10:42 AM
 */
namespace App\Http\Controllers\API\Service;

use App\Http\Controllers\BaseMallBDController;
use App\Model\BannerModel;

class BannerService extends BaseMallBDController {

    function getAllBanners(){

        $bannerModel = new BannerModel();

        $banners = $bannerModel->getAllBanners();

        if(empty($banners))
        {
            $this->serviceResponse->responseStat->status=false;
            $this->serviceResponse->responseStat->msg="No Data Received";
            return $this->response();
        }

        $this->serviceResponse->responseStat->status = true;
        $this->serviceResponse->responseStat->msg = "Data Found";
        $this->serviceResponse->responseData = $banners;
        return $this->response();
    }
}
