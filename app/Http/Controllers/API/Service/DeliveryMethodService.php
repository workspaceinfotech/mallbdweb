<?php

namespace App\Http\Controllers\API\Service;
use App\Http\Controllers\BaseMallBDController;
use App\Model\DeliveryMethodModel;
use App\Model\ZoneModel;

use App\Http\Requests;

/**
 * Project  : mallbdweb
 * File     : ZoneService.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 2/23/16 - 11:54 AM
 */

class DeliveryMethodService extends BaseMallBDController {

    public function getAllDeliveryMethods()
    {
        $result = new DeliveryMethodModel();
        $deliveryMethodList = $result->getAllDeliveryMethods();

        if(empty($deliveryMethodList))
        {
            $this->serviceResponse->responseStat->status=false;
            $this->serviceResponse->responseStat->msg="No Data Received";
            return $this->response();
        }

        $this->serviceResponse->responseStat->status = true;
        $this->serviceResponse->responseStat->msg = "Data Found";
        $this->serviceResponse->responseData = $deliveryMethodList;
        return $this->response();

    }
}
