<?php
/**
 * Created by PhpStorm.
 * User: rafsan
 * Date: 18/2/15
 * Time: 5:25 PM
 */
namespace App\Http\Controllers\API\Service;

use App\Http\Controllers\BaseMallBDController;
use Illuminate\Http\Request;

class CurrencyConverterService  extends BaseMallBDController{


    function getCurrencyRate(Request $request){
        $currencyList = ['BDT','GBP'];
        $to_currency    = ($request->input('to')==null)?"":$request->input('to');
        $from_currency    = ($request->input('from')==null)?"":$request->input('from');

        if(!in_array($to_currency,$currencyList)){
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "currency $to_currency not in our list";
            $this->serviceResponse->responseData = 0;
            return $this->response();
        }
        if(!in_array($from_currency,$currencyList)){
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "currency $from_currency not in our list";
            $this->serviceResponse->responseData = 0;
            return $this->response();
        }


        $amount            = 1;
        $results = $this->convertCurrency($from_currency,$to_currency,$amount);
        $regularExpression     = '#\<span class=bld\>(.+?)\<\/span\>#s';
        preg_match($regularExpression, $results, $finalData);
        $output = preg_replace( '/[^0-9|.]/', '', $finalData[0] );

        if(!is_numeric($output)){
           $this->serviceResponse->responseStat->status = false;
           $this->serviceResponse->responseStat->msg = "Error on conversion";
           $this->serviceResponse->responseData = 0;
        }
        $this->serviceResponse->responseData = round($output,2);
        return $this->response();
    }
    function convertCurrency($from,$to,$amount){
        $url = "http://www.google.com/finance/converter?a=$amount&from=$from&to=$to";
        $request = curl_init();
        $timeOut = 0;
        curl_setopt ($request, CURLOPT_URL, $url);
        curl_setopt ($request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($request, CURLOPT_USERAGENT,"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
        curl_setopt ($request, CURLOPT_CONNECTTIMEOUT, $timeOut);
        $response = curl_exec($request);
        curl_close($request);
        return $response;
    }
}