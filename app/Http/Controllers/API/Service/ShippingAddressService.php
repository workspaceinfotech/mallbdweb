<?php
/**
 * Project  : mallbdweb
 * File     : ShippingAddress.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 3/18/16 - 5:36 PM
 */
namespace App\Http\Controllers\API\Service;
use App\Http\Controllers\BaseMallBDController;
use App\Model\ShippingAddressModel;
use App\Model\UserModel;
use Illuminate\Http\Request;

class ShippingAddressService extends BaseMallBDController {

    public function getAllShippingAddressesByUserId(Request $request)
    {

        $shippingAddressModel = new ShippingAddressModel();
        $shippingAddressModel->setUserId($request->input("user_id"));

        $this->setError($shippingAddressModel->errorManager->errorObj);

        if(!$this->hasError()){

            $shippingAddressList = $shippingAddressModel->getAllByUserId();
        }

        //return $shippingAddressList;


        if(empty($shippingAddressList))
        {
            $this->serviceResponse->responseStat->status=false;
            $this->serviceResponse->responseStat->msg="No Data Received";
            return $this->response();
        }

        $this->serviceResponse->responseStat->status = true;
        $this->serviceResponse->responseStat->msg = "Data Found";
        $this->serviceResponse->responseData = $shippingAddressList;
        return $this->response();

    }

    public function getAllShippingAddresses()
    {
        $shippingAddressModel = new ShippingAddressModel();
        $shippingAddressList = $shippingAddressModel->all();

        if(empty($shippingAddressList)) {
            $this->serviceResponse->responseStat->status = FALSE;
            $this->serviceResponse->responseStat->msg    = "No Data Received";

            return $this->response();
        }
        $this->serviceResponse->responseStat->status = true;
        $this->serviceResponse->responseStat->msg = "Data Found";
        $this->serviceResponse->responseData = $shippingAddressList;
        return $this->response();

    }

    public function addShippingAddress(Request $request)
    {
        $shippingAddressModel = new ShippingAddressModel();
        if(!$this->serviceResponse->responseStat->isLogin){
            $shippingAddressModel->setUserId($request->input("user_id"));
        }else{
            $shippingAddressModel->setUserId($this->appCredential->user->id);
        }
        $shippingAddressModel->setShippingAddress($request->input("shipping_address"));
        $shippingAddressModel->setShippingCountry($request->input("shipping_country"));
        $shippingAddressModel->setShippingCity($request->input("shipping_city"));
        $shippingAddressModel->setShippingZipcode($request->input("shipping_zipcode"));

        $this->setError($shippingAddressModel->errorManager->errorObj);

        if(!$this->hasError()) {
            $shippingAddressModel->save();

            $data = $shippingAddressModel->getAllByUserId();
            $this->serviceResponse->responseStat->status = true;
            $this->serviceResponse->responseStat->isLogin = false;
            $this->serviceResponse->responseStat->msg="Successful !! ";
            $this->serviceResponse->responseData = $data;
            return $this->response();
        }else{
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->isLogin = false;
            $this->serviceResponse->responseStat->msg="failed !! ";
            return $this->response();
        }
    }

    public function editShippingAddress(Request $request) {

        $shippingAddressModel = new ShippingAddressModel();


        $userId = $this->appCredential->user->id;

        $shippingAddressId = $request->input("shipping_id");
        $shippingAddress = $request->input("shipping_address");
        $shippingCountry = $request->input("shipping_country");
        $shippingCity = $request->input("shipping_city");
        $shippingZipcode = $request->input("shipping_zipcode");

        $shippingAddressModel->setId($shippingAddressId);
        $shippingAddressModel->setUserId($userId);
        $shippingAddressModel->setShippingAddress($shippingAddress);
        $shippingAddressModel->setShippingCity($shippingCity);
        $shippingAddressModel->setShippingCountry($shippingCountry);
        $shippingAddressModel->setShippingZipcode($shippingZipcode);



        if(!$this->hasError()){
            if($shippingAddressModel->updateInformation()>=1)
            {
                $data = $shippingAddressModel->getById();

                $this->serviceResponse->responseStat->status = TRUE;
                $this->serviceResponse->responseStat->msg = "Successfully Updated";
                $this->serviceResponse->responseData = $data;
                return $this->response();
            }
            else{
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseStat->msg = "update failed/no data updated";
                return $this->response();
            }
        }

    }
    public function deleteShippingAddressByEmail(Request $request) {

        $shoppingAddressModel = new ShippingAddressModel();

        $userModel = new  UserModel();

        if(!$this->serviceResponse->responseStat->isLogin){
            $userModel->setEmail($request->input('email'),false);
        }

        $shoppingAddressModel->setId($request->input("shipping_id"));



        $this->setError($userModel->errorManager->errorObj);
        $this->setError($shoppingAddressModel->errorManager->errorObj);

        if(!$this->hasError()) {
            $user = new \stdClass();
            if($this->serviceResponse->responseStat->isLogin){
                $user = $this->appCredential->user;
            }else{

                $user = $userModel->getByEmail();
            }
            ShippingAddressModel::where('id','=',$shoppingAddressModel->id)->where('user_id','=',$user->id)->delete();
            $shoppingAddressModel->setUserId($user->id);
            $data = $shoppingAddressModel->getAllByUserId();
            $this->serviceResponse->responseStat->msg = "Successfully deleted";
            $this->serviceResponse->responseData = $data;
        }else{
            $this->serviceResponse->responseStat->status = TRUE;
        }

        return $this->response();
    }

}
