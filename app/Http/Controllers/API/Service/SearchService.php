<?php

/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: CategoryService.php
 * User: rajib
 * Date: 1/13/16
 * Time: 6:48 PM
 */

namespace App\Http\Controllers\API\Service;

use App\Model\CategoryModel;
use App\Model\PackageModel;
use Illuminate\Http\Request;
use App\Model\ProductModel;
use App\Model\ManufacturerModel;
use App\Http\Controllers\BaseMallBDController;
use App\Http\Requests;

class SearchService extends BaseMallBDController {

    function searchByCategoryAndAttribute(Request $request) {
        $jsonString1 = $request->input("categories");
        $jsonString2 = $request->input("attributes");

        $decodedCategory = [];
        if ($jsonString1 != NULL && $jsonString1 != "") {
            $decodedCategory = json_decode($jsonString1);
        }

        $decodedAttributes = [];
        if ($jsonString2 != NULL && $jsonString2 != "") {
            $decodedAttributes = json_decode($jsonString2);
        }

        $p = new ProductModel();

        return $p->getProductBySearch($decodedCategory, $decodedAttributes);
    }

    function searchByFilter(Request $request) {
        $category = $request->input("category");
        $attributeList = [];
        $manufacturerList = [];
        $minPrice = -1;
        $maxPrice = -1;
        $limit = 20;
        $minPrice = $request->input("minPrice");
        $maxPrice = $request->input("maxPrice");
        $limit = $request->input("limit");
        $offset = $request->input("offset");

        try {
            $attributeList = json_decode($request->input("attribute_value_list"));

            if ($attributeList == NULL && (strlen($request->input("attribute_value_list")) >= 1)) {
                $this->serviceResponse->responseStat->msg = "Error in input Array(Attribute Value List)";
                $this->serviceResponse->responseStat = FALSE;
            }
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
        }

        try {

            $manufacturerList = json_decode($request->input("manufacturer_id"));

            if ($manufacturerList == NULL && (strlen($request->input("manufacturer_id")) >= 1)) {
                $this->serviceResponse->responseStat->msg = "Error in input Array(Manufacturer List)";
                $this->serviceResponse->responseStat->status = FALSE;
            }
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
        }


        if ($category == NULL || !is_numeric($category)) {
            $this->serviceResponse->responseStat->msg = "Must Select a Category/can not be blank";
            $this->serviceResponse->responseStat->status = FALSE;
        }


        if ($minPrice != NULL && !is_numeric($minPrice)) {
            $this->serviceResponse->responseStat->msg = "minPrice must be int/empty";
            $this->serviceResponse->responseStat->status = FALSE;
        }


        if ($maxPrice != NULL && !is_numeric($maxPrice)) {
            $this->serviceResponse->responseStat->msg = "maxPrice must be int/empty";
            $this->serviceResponse->responseStat->status = FALSE;
        }

        if (!is_numeric($limit)) {
            $this->serviceResponse->responseStat->msg = "limit must be int";
            $this->serviceResponse->responseStat->status = FALSE;
        }
        if (!is_numeric($offset)) {

            $this->serviceResponse->responseStat->msg = "Offset must be int";
            $this->serviceResponse->responseStat->status = FALSE;
        }

        $productModel = new ProductModel();

        $productModel->setCustomLimit($limit);
        $productModel->setCustomOffset($offset);
        $this->setError($productModel->errorManager->errorObj);


        $this->setError($productModel->errorManager->errorObj);

        if ($this->hasError()) {
            return $this->response();
        }

        $categoryIdList = [];
        $categoryObj = new CategoryModel();

        $finalCategoryForProducts = $categoryObj->allCategoryWithParentsAndItsChildrens($this->shopId, $category);

        foreach ($finalCategoryForProducts as $cat) {
            array_push($categoryIdList, $cat->id);
            if (!empty($cat->childrens)) {
                foreach ($cat->childrens as $child) {
                    array_push($categoryIdList, $child->id);
                    if (!empty($child->childrens)) {
                        foreach ($child->childrens as $child2) {
                            array_push($categoryIdList, $child2->id);
                        }
                    }
                }
            }
        }


        if ($this->serviceResponse->responseStat->status == TRUE) {
            $this->serviceResponse->responseData = $productModel->getProductBySearch($categoryIdList, $attributeList, $minPrice, $maxPrice, $manufacturerList);
        }


        return $this->response();
    }

    public function searchCategoryByProductTitleKeyword(Request $request) {

        $keyword = $request->input("keyword");

        if (strlen($keyword) < 1) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "No Keywords Given !";
            return $this->response();
        }

        $productModel = new ProductModel();

        $result = $productModel->findDistinctCategory($keyword);

        if (count($result) < 1) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "No Category Found !";
            return $this->response();
        } else {

            $this->serviceResponse->responseStat->status = TRUE;
            $this->serviceResponse->responseStat->msg = "Categories Found !";
            $this->serviceResponse->responseData = $result;
            return $this->response();
        }
    }

    public function searchCategoryByProductTitleKeywordWeb(Request $request) {

        $keyword = $request->input("keyword");

        $resultObj = new \stdclass();
        $resultObj->product = [];
        $resultObj->package = [];
        $resultObj->manufacturer = [];
        $resultObj->category = [];

        if (strlen($keyword) < 1) {
            return response()->json($resultObj);
        }

        $productModel = new ProductModel();
        $packageModel = new PackageModel();
        $manufacturerModel = new ManufacturerModel();
        $categoryModel = new CategoryModel();

        $selectedProducts = $productModel->getProductByKeyword($keyword);
        $selectedPackages = $packageModel->getPackageByKeyword($keyword);
        $selectedManufacturers = $manufacturerModel->getManufacturerByKeyword($keyword);
        $selectedCategories = $categoryModel->getCategoryByKeyword($keyword);

//        $result = $categoryModel->getAllCategoryByProductTitle($keyword);
//        $packageModel = new PackageModel();
//        $packageModel->setCustomOffset(0);
//        $packageModel->setCustomLimit(5);
//        $packages = $packageModel->getPackageByKeyword($keyword);


        if (count($selectedProducts) < 1 && count($selectedPackages) < 1 && count($selectedCategories) < 1 && count($selectedManufacturers) < 1) {
            return response()->json($resultObj);
        } else {
            $resultObj->product = $selectedProducts;
            $resultObj->package = $selectedPackages;
            $resultObj->manufacturer = $selectedManufacturers;
            $resultObj->category = $selectedCategories;
            return response()->json($resultObj);
        }
    }

    public function getProductByKeyWord(Request $request) {

        //$category,$category2=null,$category3=null)
        /* Searching params for product by category and Attribute */
        /* Starts Here */
        $queryString = urldecode($request->input("keywords"));
        if ($queryString == null || $queryString == "") {
            
        }
        $productModel = new ProductModel();
        if ($request->input("limit") != null) {
            $limit = (is_numeric($request->input("limit"))) ? intval($request->input("limit")) : 5;
        }
        $limit = ($limit == (0 || "")) ? 5 : $limit;
        $productModel->setCustomLimit($limit);
        $productModel->customOffset = ($request->input("offset") == (0 || "")) ? 0 : $request->input("offset");
        $productList = $productModel->getAllProductByMatchString($queryString);

        $this->serviceResponse->responseStat->status = count($productList) > 0;
        if (!$this->serviceResponse->responseStat->status) {

            $this->serviceResponse->responseStat->msg = 'No product found';
        }
        $this->serviceResponse->responseData = $productList;

        return $this->response();
    }

    public function searchCategoryByProductTitleKeywordMobile(Request $request) {

        $keyword = $request->input("keyword");

        $resultObj = new \stdclass();
        $resultObj->category = [];

        if (strlen($keyword) < 1) {
            $this->serviceResponse->responseStat->status = FALSE;
            $this->serviceResponse->responseStat->msg = "no keywords given";
            $this->serviceResponse->responseData = $resultObj;
            return $this->response();
        }

        $categoryModel = new CategoryModel();

        $result = $categoryModel->getAllCategoryByProductTitleMobile($keyword);

        if (count($result) < 1) {
            $this->serviceResponse->responseStat->status = TRUE;
            $this->serviceResponse->responseStat->msg = "no result found";
            $this->serviceResponse->responseData = $resultObj;
            return $this->response();
        } else {
            $resultObj->category = $result;
            $this->serviceResponse->responseStat->status = TRUE;
            $this->serviceResponse->responseStat->msg = count($result) . " result found";
            $this->serviceResponse->responseData = $resultObj;
            return $this->response();
        }
    }

}
