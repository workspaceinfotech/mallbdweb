<?php
/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: ProfileService.php
 * User: rajib
 * Date: 1/13/16
 * Time: 2:47 PM
 */

namespace App\Http\Controllers\API\Service;


use App\Http\Controllers\BaseMallBDController;
use App\Model\DataModel\AuthCredential;
use App\Model\DataModel\User;
use App\Model\DataModel\UserDetails;
use App\Model\LoginModel;
use App\Model\UserDetailsModel;
use App\Model\UserModel;
use Illuminate\Http\Request;
use Validator;

class ProfileService extends BaseMallBDController{


    public function updateProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|max:55',
            'last_name' => 'required|max:55',
            'phone'     => 'required'
        ]);

        if ($validator->fails())
        {

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg=$validator->errors()->first();
            return $this->response();
        }

        $userId = $this->appCredential->user->id;

        $user = UserModel::find($userId);

        if($user == null || $user->id<=0)
        {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "You Must Login First";
            return $this->response();
        }
        $userModelTempObj = new UserModel();

        $user->firstname = $request->input("first_name");
        $user->lastname = $request->input("last_name");
        $user->phone = $request->input("phone");

        $image = $request->input("image");
        if($image!=null)
            $user->image =$userModelTempObj->savePicture($image);

        $userDetails = UserDetailsModel::where("user_id",$userId)->first();

        if($userDetails==null || $userDetails->user_id<=0)
        {
            $newUserDetails = new UserDetailsModel();

            $newUserDetails->address = $request->input("address");
            $newUserDetails->country = $request->input("country");
            $newUserDetails->city = $request->input("city");
            $newUserDetails->zipcode = $request->input("zip_code");
            $newUserDetails->shipping_address = $request->input("shipping_address");
            $newUserDetails->shipping_country = $request->input("shipping_country");
            $newUserDetails->shipping_city = $request->input("shipping_city");
            $newUserDetails->shipping_zipcode = $request->input("shipping_zipcode");

            $user->save();

            $newUserDetails->user_id = $userId;

            $newUserDetails->save();

            $newUser = LoginModel::where("user_id","=",$userId)->get()->first();
            $authCredential = new AuthCredential();
            $authCredential->castMe($newUser);

            $this->serviceResponse->responseStat->status = true;
            $this->serviceResponse->responseStat->msg = "Profile Updated";
            $this->serviceResponse->responseData = $authCredential;
            return $this->response();

        }

        $userDetails->address = $request->input("address");
        $userDetails->country = $request->input("country");
        $userDetails->city = $request->input("city");
        $userDetails->zipcode = $request->input("zip_code");
        $userDetails->shipping_address = $request->input("shipping_address");
        $userDetails->shipping_country = $request->input("shipping_country");
        $userDetails->shipping_city = $request->input("shipping_city");
        $userDetails->shipping_zipcode = $request->input("shipping_zipcode");

        $user->save();

        $userDetails->user_id = $user->id;

        $userDetails->save();

        $newUser = LoginModel::where("user_id","=",$user->id)->get()->first();
        $authCredential = new AuthCredential();
        $authCredential->castMe($newUser);

        $this->serviceResponse->responseStat->status = true;
        $this->serviceResponse->responseStat->msg = "Profile Updated";
        $this->serviceResponse->responseData = $authCredential;
        return $this->response();

    }

} 