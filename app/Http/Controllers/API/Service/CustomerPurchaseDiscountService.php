<?php
/**
 * Created by PhpStorm.
 * User: rafsan
 * Date: 18/2/15
 * Time: 5:25 PM
 */
namespace App\Http\Controllers\API\Service;

use App\Http\Controllers\BaseMallBDController;
use App\Model\OrderModel;
use App\Model\CustomerPurchaseDiscountModel;
use Illuminate\Http\Request;

class CustomerPurchaseDiscountService  extends BaseMallBDController{

    public function calculateOnPurchaseDiscount() {
        if ($this->serviceResponse->responseStat->isLogin) {
            $orderModel = new OrderModel();
            $customerPurchaseDiscountModel = new CustomerPurchaseDiscountModel();
            
            $customerCompletedOrders = $orderModel->customerCompletedOrderById($this->appCredential->user->id);

            $presentDiscount = 0;
            $presentDiscountData = $customerPurchaseDiscountModel->getPresentDiscountForCheckout($customerCompletedOrders);
            if(sizeof($presentDiscountData) == 0){
                $this->serviceResponse->responseStat->status = false;
            }
            else{
                $this->serviceResponse->responseStat->status = true;
            }
            
            $this->serviceResponse->responseStat->msg = "Data Found";
            $this->serviceResponse->discountMessage = $this->pageData['discountMessage'];
            $this->serviceResponse->responseData = $presentDiscountData;
            return $this->response();
        } else {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "No Data Received";
            $this->serviceResponse->discountMessage = $this->pageData['discountMessage'];
            return $this->response();
        }
    }

}

