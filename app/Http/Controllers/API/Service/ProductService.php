<?php

/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: ProductService.php
 * User: rajib
 * Date: 1/14/16
 * Time: 5:11 PM
 */

namespace App\Http\Controllers\API\Service;

use App\Http\Controllers\BaseMallBDController;
use App\Model\CategoryModel;
use App\Model\DataModel\Product;
use App\Model\MallBdItem_Model;
use App\Model\ProductModel;
use App\Model\TagModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

class ProductService extends BaseMallBDController {

    public function getAllProducts(Request $request) {
        $validator = Validator::make($request->all(), [
                    'limit' => 'required|Integer',
                    'offset' => 'required|Integer'
        ]);

        if ($validator->fails()) {

            $this->serviceResponse->responseStat->status = false;

            $this->serviceResponse->responseStat->msg = $validator->errors()->first();
            return $this->response();
        }

        $products = new ProductModel();
        $limit = ($request->input("limit") == (0 || "")) ? 5 : $request->input("limit");
        $products->setCustomLimit($limit);
        $products->customOffset = $request->input("offset");

        $productList = $products->getAllProducts($this->shopId);

        if (empty($productList)) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "No Data Received";
            return $this->response();
        }
        $this->serviceResponse->responseStat->status = true;
        $this->serviceResponse->responseStat->msg = "Data Received";
        $this->serviceResponse->responseData = $productList;
        return $this->response();
    }

    public function getAllProductsMobile(Request $request) {
        $validator = Validator::make($request->all(), [
                    'limit' => 'required|Integer',
                    'offset' => 'required|Integer'
        ]);

        if ($validator->fails()) {

            $this->serviceResponse->responseStat->status = false;

            $this->serviceResponse->responseStat->msg = $validator->errors()->first();
            return $this->response();
        }

        $products = new ProductModel();
        $limit = ($request->input("limit") == (0 || "")) ? 5 : $request->input("limit");
        $products->setCustomLimit($limit);
        $products->customOffset = $request->input("offset");

        $productList = $products->getAllProductsMobile($this->shopId);

        if (empty($productList)) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "No Data Received";
            return $this->response();
        }
        $this->serviceResponse->responseStat->status = true;
        $this->serviceResponse->responseStat->msg = "Data Received";
        $this->serviceResponse->responseData = $productList;
        return $this->response();
    }

    public function getProductById(Request $request) {
        $validator = Validator::make($request->all(), [
                    'id' => 'required|Integer'
        ]);

        if ($validator->fails()) {

            $this->serviceResponse->responseStat->status = false;

            $this->serviceResponse->responseStat->msg = $validator->errors()->first();
            return $this->response();
        }

        $products = new ProductModel();
        $product_id = $request->input("id");

        $productList = $products->getProductById($product_id);

        if (empty($productList)) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "No Data Received";
            return $this->response();
        }
        $this->serviceResponse->responseStat->status = true;
        $this->serviceResponse->responseStat->msg = "Data Received";
        $this->serviceResponse->responseData = $productList;
        return $this->response();
    }

    public function getAllProductsBySearch(Request $request) {
        $validator = Validator::make($request->all(), [
                    'limit' => 'required|Integer',
                    'offset' => 'required|Integer',
                    'keyword' => 'required'
        ]);

        if ($validator->fails()) {

            $this->serviceResponse->responseStat->status = false;

            $this->serviceResponse->responseStat->msg = $validator->errors()->first();
            return $this->response();
        }
        $keyword = $request->input("keyword");
        $products = new ProductModel();
        $limit = ($request->input("limit") == (0 || "")) ? 5 : $request->input("limit");
        $products->setCustomLimit($limit);
        $products->customOffset = $request->input("offset");

        $productList = $products->getAllProductsBySearch($this->shopId, $keyword);

        if (empty($productList)) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "No Data Received";
            return $this->response();
        }
        $this->serviceResponse->responseStat->status = true;
        $this->serviceResponse->responseStat->msg = "Data Received";
        $this->serviceResponse->responseData = $productList;
        return $this->response();
    }

    public function getAllProductsBySearchForSuggestion(Request $request) {
        $validator = Validator::make($request->all(), [
                    'limit' => 'required|Integer',
                    'keyword' => 'required'
        ]);

        if ($validator->fails()) {

            $this->serviceResponse->responseStat->status = false;

            $this->serviceResponse->responseStat->msg = $validator->errors()->first();
            return $this->response();
        }
        $keyword = $request->input("keyword");
        $products = new ProductModel();
        $limit = ($request->input("limit") == (0 || "")) ? 5 : $request->input("limit");
        $products->setCustomLimit($limit);

        $productList = $products->getAllProductsBySearchForSuggestion($this->shopId, $keyword);

        if (empty($productList)) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "No Data Received";
            return $this->response();
        }
        $this->serviceResponse->responseStat->status = true;
        $this->serviceResponse->responseStat->msg = "Data Received";
        $this->serviceResponse->responseData = $productList;
        return $this->response();
    }

    public function getAllFeaturedProducts(Request $request) {
        $validator = Validator::make($request->all(), [
                    'limit' => 'required|Integer',
                    'offset' => 'required|Integer',
                    'shop_id' => 'required|Integer'
        ]);

        if ($validator->fails()) {

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = $validator->errors()->first();
            return $this->response();
        }

        $products = new ProductModel();
        $limit = ($request->input("limit") == (0 || "")) ? 5 : $request->input("limit");
        $products->setCustomLimit($limit);
        $products->customOffset = $request->input("offset");

        $productList = $products->getFeaturedProducts($request->input("shop_id"));

        if (empty($productList)) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "No Data Received";
            return $this->response();
        }
        $this->serviceResponse->responseStat->status = true;
        $this->serviceResponse->responseStat->msg = "Data Received";
        $this->serviceResponse->responseData = $productList;
        return $this->response();
    }

    public function getAllFeaturedProductsMobile(Request $request) {
        $validator = Validator::make($request->all(), [
                    'limit' => 'required|Integer',
                    'offset' => 'required|Integer',
                        //'shop_id' => 'required|Integer'
        ]);

        if ($validator->fails()) {

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = $validator->errors()->first();
            return $this->response();
        }

        $products = new ProductModel();
        $limit = ($request->input("limit") == (0 || "")) ? 5 : $request->input("limit");
        $products->setCustomLimit($limit);
        $products->customOffset = $request->input("offset");

        $productList = $products->getFeaturedProductsMobile($this->shopId);

        if (empty($productList)) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "No Data Received";
            return $this->response();
        }
        $this->serviceResponse->responseStat->status = true;
        $this->serviceResponse->responseStat->msg = "Data Received";
        $this->serviceResponse->responseData = $productList;
        return $this->response();
    }

    public function getAllNewProducts(Request $request) {
        $validator = Validator::make($request->all(), [
                    'limit' => 'required|Integer',
                    'offset' => 'required|Integer',
                    'shop_id' => 'required|Integer'
        ]);

        if ($validator->fails()) {

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = $validator->errors()->first();
            return $this->response();
        }

        $products = new ProductModel();
        $limit = ($request->input("limit") == (0 || "")) ? 5 : $request->input("limit");
        $products->setCustomLimit($limit);
        $products->customOffset = $request->input("offset");

        $productList = $products->getNewProducts($request->input("shop_id"));

        if (empty($productList)) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "No Data Received";
            return $this->response();
        }
        $this->serviceResponse->responseStat->status = true;
        $this->serviceResponse->responseStat->msg = "Data Received";
        $this->serviceResponse->responseData = $productList;
        return $this->response();
    }

    public function getAllNewProductsMobile(Request $request) {
        $validator = Validator::make($request->all(), [
                    'limit' => 'required|Integer',
                    'offset' => 'required|Integer',
                        //'shop_id' => 'required|Integer'
        ]);

        if ($validator->fails()) {

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = $validator->errors()->first();
            return $this->response();
        }

        $products = new ProductModel();
        $limit = ($request->input("limit") == (0 || "")) ? 5 : $request->input("limit");
        $products->setCustomLimit($limit);
        $products->customOffset = $request->input("offset");

        $productList = $products->getNewProductsMobile($this->shopId);

        if (empty($productList)) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "No Data Received";
            return $this->response();
        }
        $this->serviceResponse->responseStat->status = true;
        $this->serviceResponse->responseStat->msg = "Data Received";
        $this->serviceResponse->responseData = $productList;
        return $this->response();
    }

    public function getAllSpecialProducts(Request $request) {
        $validator = Validator::make($request->all(), [
                    'limit' => 'required|Integer',
                    'offset' => 'required|Integer',
                    'shop_id' => 'required|Integer'
        ]);

        if ($validator->fails()) {

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = $validator->errors()->first();
            return $this->response();
        }

        $products = new ProductModel();
        $limit = ($request->input("limit") == (0 || "")) ? 5 : $request->input("limit");
        $products->setCustomLimit($limit);
        $products->customOffset = $request->input("offset");

        $productList = $products->getSpecialProducts($request->input("shop_id"));

        if (empty($productList)) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "No Data Received";
            return $this->response();
        }
        $this->serviceResponse->responseStat->status = true;
        $this->serviceResponse->responseStat->msg = "Data Received";
        $this->serviceResponse->responseData = $productList;
        return $this->response();
    }

    public function getAllSpecialProductsMobile(Request $request) {
        $validator = Validator::make($request->all(), [
                    'limit' => 'required|Integer',
                    'offset' => 'required|Integer',
                        //'shop_id' => 'required|Integer'
        ]);

        if ($validator->fails()) {

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = $validator->errors()->first();
            return $this->response();
        }

        $products = new ProductModel();
        $limit = ($request->input("limit") == (0 || "")) ? 5 : $request->input("limit");
        $products->setCustomLimit($limit);
        $products->customOffset = $request->input("offset");

        $productList = $products->getSpecialProductsMobile($this->shopId);

        if (empty($productList)) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "No Data Received";
            return $this->response();
        }
        $this->serviceResponse->responseStat->status = true;
        $this->serviceResponse->responseStat->msg = "Data Received";
        $this->serviceResponse->responseData = $productList;
        return $this->response();
    }

    public function getProductByKeyword(Request $request) {
        $validator = Validator::make($request->all(), [
                    'limit' => 'required|Integer',
                    'offset' => 'required|Integer',
                    'shop_id' => 'required|Integer',
        ]);

        if ($validator->fails()) {

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = $validator->errors()->first();
            return $this->response();
        }
        $keywords = $request->input("keywords");
        if (empty($keywords)) {

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "No keyword provided";
            return $this->response();
        }
        $shop_id = $request->input("shop_id");


        $productModel = new ProductModel();
        $limit = ($request->input("limit") == (0 || "")) ? 5 : $request->input("limit");
        $productModel->setCustomLimit($limit);
        $productModel->customOffset = $request->input("offset");

        $results = $productModel->getAllProductsByTitle($keywords, $shop_id);

        $tagModel = new TagModel();
        $tagIds = $tagModel->getTagIdByName($keywords);


        $productsWithTag = TagModel::with("products")->whereIn("tag_id", $tagIds)->get();

        foreach ($productsWithTag as $p) {
            foreach ($p->products as $prod) {
                if (!in_array($prod, $results)) {
                    array_push($results, $prod);
                }
            }
        }

        $results = array_unique($results);
        if (empty($results)) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "No Data received";
            return $this->response();
        }


        $productList = [];
        foreach ($results as $result) {
            $prod = new Product();
            $prod->castMe($result);
            array_push($productList, $prod);
        }

        $this->serviceResponse->responseStat->status = true;
        $this->serviceResponse->responseStat->msg = "Data received";
        $this->serviceResponse->responseData = $productList;
        return $this->response();
    }

    public function getProductByCategory(Request $request) {
        $validator = Validator::make($request->all(), [
                    'limit' => 'required|Integer',
                    'offset' => 'required|Integer',
                    'category' => 'required|Integer',
                    'shop_id' => 'required|Integer'
        ]);

        if ($validator->fails()) {

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = $validator->errors()->first();
            return $this->response();
        }
        $category = $request->input("category");
        $shop = $request->input("shop_id");
        $limit = $request->input("limit");
        $categoryIdList = [];
        $categoryList = [];

        if (isset($category)) {
            $categoryObj = new CategoryModel();
            $categoryList = $categoryObj->allCategoryWithParentsAndItsChildrens($shop, $category);
        }

        foreach ($categoryList as $cat) {
            array_push($categoryIdList, $cat->id);
            if (!empty($cat->childrens)) {
                foreach ($cat->childrens as $child) {
                    array_push($categoryIdList, $child->id);
                    if (!empty($child->childrens)) {
                        foreach ($child->childrens as $child2) {
                            array_push($categoryIdList, $child2->id);
                        }
                    }
                }
            }
        }

        if (empty($categoryIdList)) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "No Data for this category";
            return $this->response();
        }

        $productModel = new ProductModel();
        $limit = ($limit == (0 || "")) ? 5 : $limit;
        $productModel->setCustomLimit($limit);
        $productModel->customOffset = ($request->input("offset") == (0 || "")) ? 0 : $request->input("offset");
        $productModel->setCurrentUserId($this->appCredential->user->id);
        $productList = $productModel->getAllProductByCategory($categoryIdList);

        if (empty($productList)) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "No Data received";
            return $this->response();
        }
        $this->serviceResponse->responseStat->status = true;
        $this->serviceResponse->responseStat->msg = "Data received";
        $this->serviceResponse->responseData = $productList;
        return $this->response();
    }

    public function getProductByManufacturer(Request $request) {
        $validator = Validator::make($request->all(), [
                    'limit' => 'required|Integer',
                    'offset' => 'required|Integer',
                    'manufacture_id' => 'required|Integer',
//            'shop_id'  => 'required|Integer'
        ]);

        if ($validator->fails()) {

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = $validator->errors()->first();
            return $this->response();
        }

        $manufacturerId = $request->input('manufacture_id');
        $limit = $request->input("limit");


        $productModel = new ProductModel();
        $limit = ($limit == (0 || "")) ? 5 : $limit;
        $productModel->setCustomLimit($limit);
        $productModel->customOffset = ($request->input("offset") == (0 || "")) ? 0 : $request->input("offset");

        $products = $productModel->getProductByManufacturerId($manufacturerId);


        if (count($products) < 1) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "No Data Received";
            return $this->response();
        } else {

            $this->serviceResponse->responseStat->status = true;
            $this->serviceResponse->responseStat->msg = "Data Found";
            $this->serviceResponse->responseData = $products;
            return $this->response();
        }
    }

    public function getRelatedProducts(Request $request) {

        $limit = $request->input("limit");
        $offset = $request->input("offset");

        if ($limit == "" || empty($limit)) {
            $this->serviceResponse->responseStat->status = FALSE;
            $this->serviceResponse->responseStat->msg = "limit can not be empty";
            return $this->response();
        }
        if ($offset == "") {
            $this->serviceResponse->responseStat->status = FALSE;
            $this->serviceResponse->responseStat->msg = "Offset can not be empty";
            return $this->response();
        }

        $productCategoryId = $request->input("product_category_id");

        if ($productCategoryId == "" || empty($productCategoryId)) {
            $this->serviceResponse->responseStat->status = FALSE;
            $this->serviceResponse->responseStat->msg = "Category Id can not be empty";
            return $this->response();
        }

        if (!is_numeric($productCategoryId)) {
            $this->serviceResponse->responseStat->status = FALSE;
            $this->serviceResponse->responseStat->msg = "Category Id must be numeric";
            return $this->response();
        }

        $productId = $request->input("product_id");

        if ($productId == "" || empty($productId)) {
            $this->serviceResponse->responseStat->status = FALSE;
            $this->serviceResponse->responseStat->msg = "Product Id can not be empty";
            return $this->response();
        }

        if (!is_numeric($productId)) {
            $this->serviceResponse->responseStat->status = FALSE;
            $this->serviceResponse->responseStat->msg = "Product Id must be numeric";
            return $this->response();
        }


        $productModel = new ProductModel();
        $productModel->setCurrentUserId($this->appCredential->id);
        $productModel->setCustomOffset($offset);
        $productModel->setCustomLimit($limit);
        $relatedProducts = $productModel->getRelatedProducts($productCategoryId, $productId);

        if ($relatedProducts == "" || empty($relatedProducts)) {
            $this->serviceResponse->responseStat->status = FALSE;
            $this->serviceResponse->responseStat->msg = "No Related Product Found";
            return $this->response();
        }
        $this->serviceResponse->responseStat->status = TRUE;
        $this->serviceResponse->responseStat->msg = "Product/s Found";
        $this->serviceResponse->responseData = $relatedProducts;
        return $this->response();
    }

    public function getHomePageItem(Request $request) {
        $validator = Validator::make($request->all(), [
                    'limit' => 'required|Integer',
                    'offset' => 'required|Integer',
                    'shop_id' => 'required|Integer'
        ]);

        if ($validator->fails()) {

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = $validator->errors()->first();
            return $this->response();
        }


        $limit = ($request->input("limit") == (0 || "")) ? 5 : $request->input("limit");
        $offset = $request->input("offset");

        $mallBdItem_Model = new MallBdItem_Model();
        $mallBdItemList = $mallBdItem_Model->getItem(null, null, $limit, $offset);

        if (empty($mallBdItemList)) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "No Data Received";
            return $this->response();
        }
        $this->serviceResponse->responseStat->status = true;
        $this->serviceResponse->responseStat->msg = "Data Received";
        $this->serviceResponse->responseData = $mallBdItemList;
        return $this->response();
    }

    public function getHomePageItem_dev(Request $request) {
        $validator = Validator::make($request->all(), [
                    'limit' => 'required|Integer',
                    'offset' => 'required|Integer',
                    'shop_id' => 'required|Integer'
        ]);

        if ($validator->fails()) {

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = $validator->errors()->first();
            return $this->response();
        }


        $limit = ($request->input("limit") == (0 || "")) ? 5 : $request->input("limit");
        $offset = $request->input("offset");

        $mallBdItem_Model = new MallBdItem_Model();
        $mallBdItemList = $mallBdItem_Model->getItem(null, null, $limit, $offset);

        if (empty($mallBdItemList)) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "No Data Received";
            return $this->response();
        }
        $this->serviceResponse->responseStat->status = true;
        $this->serviceResponse->responseStat->msg = "Data Received";
        $this->serviceResponse->responseData = $mallBdItemList;
        return $this->response();
    }

    public function get_manufacturer(Request $request) {

        $validator = Validator::make($request->all(), [
                    'limit' => 'required|Integer',
                    'offset' => 'required|Integer',
                    'shop_id' => 'required|Integer'
        ]);

        if ($validator->fails()) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = $validator->errors()->first();
            return $this->response();
        }

        $limit = ($request->input("limit") == (0 || "")) ? 5 : $request->input("limit");
        $offset = $request->input("offset");


        $perpage = $limit;
        $starting = $offset * $perpage;
        $ending = $starting + $perpage;
        $data['status'] = "ok";
        $data['nextpage'] = $offset + 1;
        $data['lastpage'] = 10;

        $manufacturerMode = new ManufacturerModel();

        $all = $manufacturerMode->getAllManufacturer();
        $manufacturerMode->getAllManufacturer_dev($limit, $offset);

        echo json_encode($data);
    }

    public function changeProductView(Request $request) {
        $validator = Validator::make($request->all(), [
                    'view' => 'required',
        ]);

        if ($validator->fails()) {

            $this->serviceResponse->responseStat->status = false;

            $this->serviceResponse->responseStat->msg = $validator->errors()->first();
            return $this->response();
        }
        $sessionData['productview'] = $request->input("view");
        Session::put($sessionData);
        $this->serviceResponse->responseStat->status = true;
        $this->serviceResponse->responseStat->msg = "View Changed!!!!";
        return $this->response();
    }

}
