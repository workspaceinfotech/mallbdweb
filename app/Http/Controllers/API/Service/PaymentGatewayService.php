<?php

/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: ProfileService.php
 * User: rajib
 * Date: 1/13/16
 * Time: 2:47 PM
 */

namespace App\Http\Controllers\API\Service;

use App\Http\Controllers\BaseMallBDController;
use App\Helper\WALLETMIX;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;
use App\Model\OrderModel;
use App\Model\OrderPaymentsModel;
use App\Model\OrderProductsModel;
use App\Model\OrderStatusModel;
use App\Model\ProductModel;
use \App\Model\CurrencyModel;
class PaymentGatewayService extends BaseMallBDController {

    function walletmix($order_id) {
        $walletmix = new WALLETMIX();


        $walletmix->set_service_access_username($this->walletmixUsername);
        $walletmix->set_service_access_password($this->walletmixPassword);
        $walletmix->set_merchant_id($this->walletmixMerchantId);
        $walletmix->set_access_app_key($this->walletmixAppKey);


        $string = json_decode(file_get_contents($this->walletmixGatewayUrl . "check-server"));


        $orderDetails = OrderModel::with('orderCustomer')->with('orderCustomer.userDetails')->where('id', $order_id)->get();

        if ($string->selectedServer) {
            $customer_info = array(
                "customer_name" => $orderDetails[0]['orderCustomer']['firstname'] . ' ' . $orderDetails[0]['orderCustomer']['lastname'],
                "customer_email" => $orderDetails[0]['orderCustomer']['email'],
                "customer_add" => $orderDetails[0]['orderCustomer']['userDetails']['address'],
                "customer_city" => $orderDetails[0]['orderCustomer']['userDetails']['city'],
                "customer_country" => $orderDetails[0]['orderCustomer']['userDetails']['country'],
                "customer_postcode" => $orderDetails[0]['orderCustomer']['userDetails']['zipcode'],
                "customer_phone" => $orderDetails[0]['orderCustomer']['phone'],
            );
            $shipping_info = array(
                "shipping_name" => $orderDetails[0]['orderCustomer']['firstname'] . ' ' . $orderDetails[0]['orderCustomer']['lastname'],
                "shipping_add" => $orderDetails[0]['orderCustomer']['userDetails']['shipping_address'],
                "shipping_city" => $orderDetails[0]['orderCustomer']['userDetails']['shipping_city'],
                "shipping_country" => $orderDetails[0]['orderCustomer']['userDetails']['shipping_country'],
                "shipping_postCode" => $orderDetails[0]['orderCustomer']['userDetails']['shipping_zipcode'],
            );
            $walletmix->set_shipping_charge(0);
            $walletmix->set_discount(0);

            $product_1 = array('name' => 'Adata 16GB Pendrive', 'price' => 15, 'quantity' => 2);
            $product_2 = array('name' => 'Adata 8GB Pendrive', 'price' => 5, 'quantity' => 1);

            $products = array($product_1, $product_2);

            $walletmix->set_product_description($products);
            $order_subtotal = $orderDetails[0]['order_total'];
            $shipping_cost = $orderDetails[0]['shipping_cost'];
            $discount_total = $orderDetails[0]['discount_total'];
            $voucher_discount = $orderDetails[0]['voucher_discount'];
            $employee_discount = $orderDetails[0]['employee_discount'];
            $special_discount = $orderDetails[0]['special_discount'];
            $grand_total = ($order_subtotal + $shipping_cost) - ($discount_total + $employee_discount + $special_discount+$voucher_discount);

            $walletmix->set_amount($grand_total);

            $walletmix->set_merchant_order_id($order_id);

            $walletmix->set_app_name('themallbd.com');
            $walletmix->set_currency('BDT');
            $walletmix->set_callback_url($this->walletmixGatewayCallbackUrl);

            $extra_data = array('param_1' => 'data_1', 'param_2' => 'data_2', 'param_3' => 'data_3');
            $walletmix->set_extra_json($extra_data);

            $walletmix->set_transaction_related_params($customer_info);
            $walletmix->set_transaction_related_params($shipping_info);

            $walletmix->set_database_driver('session'); // options: "txt" or "session"

            $walletmix->send_data_to_walletmix($this->walletmixGatewayUrl . "check-server");
        }
    }

    function walletmixService(Request $request) {
        $order_id = $request->input("order_id");
        $walletmix = new WALLETMIX();


        $walletmix->set_service_access_username($this->walletmixUsername);
        $walletmix->set_service_access_password($this->walletmixPassword);
        $walletmix->set_merchant_id($this->walletmixMerchantId);
        $walletmix->set_access_app_key($this->walletmixAppKey);


        $getServerDetails = json_decode(file_get_contents($this->walletmixGatewayUrl . "check-server"));


        $orderDetails = OrderModel::with('orderCustomer')->with('orderCustomer.userDetails')->where('id', $order_id)->get();

        if ($getServerDetails->selectedServer) {
            $customer_info = array(
                "customer_name" => $orderDetails[0]['orderCustomer']['firstname'] . ' ' . $orderDetails[0]['orderCustomer']['lastname'],
                "customer_email" => $orderDetails[0]['orderCustomer']['email'],
                "customer_add" => $orderDetails[0]['orderCustomer']['userDetails']['address'],
                "customer_city" => $orderDetails[0]['orderCustomer']['userDetails']['city'],
                "customer_country" => $orderDetails[0]['orderCustomer']['userDetails']['country'],
                "customer_postcode" => $orderDetails[0]['orderCustomer']['userDetails']['zipcode'],
                "customer_phone" => $orderDetails[0]['orderCustomer']['phone'],
            );
            $shipping_info = array(
                "shipping_name" => $orderDetails[0]['orderCustomer']['firstname'] . ' ' . $orderDetails[0]['orderCustomer']['lastname'],
                "shipping_add" => $orderDetails[0]['orderCustomer']['userDetails']['shipping_address'],
                "shipping_city" => $orderDetails[0]['orderCustomer']['userDetails']['shipping_city'],
                "shipping_country" => $orderDetails[0]['orderCustomer']['userDetails']['shipping_country'],
                "shipping_postCode" => $orderDetails[0]['orderCustomer']['userDetails']['shipping_zipcode'],
            );
            $walletmix->set_shipping_charge(0);
            $walletmix->set_discount(0);

            $product_1 = array('name' => 'Adata 16GB Pendrive', 'price' => 15, 'quantity' => 2);
            $product_2 = array('name' => 'Adata 8GB Pendrive', 'price' => 5, 'quantity' => 1);

            $products = array($product_1, $product_2);

            $walletmix->set_product_description($products);
            
            $order_subtotal = $orderDetails[0]['order_total'];
            $shipping_cost = $orderDetails[0]['shipping_cost'];
            $discount_total = $orderDetails[0]['discount_total'];
            $voucher_discount = $orderDetails[0]['voucher_discount'];
            $employee_discount = $orderDetails[0]['employee_discount'];
            $special_discount = $orderDetails[0]['special_discount'];
            $grand_total = ($order_subtotal + $shipping_cost) - ($discount_total + $employee_discount + $special_discount+$voucher_discount);

            $walletmix->set_amount($grand_total);

            $walletmix->set_merchant_order_id($order_id);

            $walletmix->set_app_name('themallbd.com');
            $walletmix->set_currency('BDT');
            $walletmix->set_callback_url($this->walletmixGatewayCallbackMobileUrl);

            $extra_data = array('param_1' => 'data_1', 'param_2' => 'data_2', 'param_3' => 'data_3');
            $walletmix->set_extra_json($extra_data);

            $walletmix->set_transaction_related_params($customer_info);
            $walletmix->set_transaction_related_params($shipping_info);

            $walletmix->set_database_driver('txt'); // options: "txt" or "session"

            $wmx_response = $walletmix->send_data_to_walletmix_mobile($this->walletmixGatewayUrl . "check-server");
            $wmx_response_d = json_decode($wmx_response);
            if ($wmx_response_d->statusCode === '1000') {
                $wmx_url = $getServerDetails->bank_payment_url . "/" . $wmx_response_d->token;
                $this->serviceResponse->responseStat->status = true;
                $this->serviceResponse->responseData = $wmx_response_d;
                $this->serviceResponse->wmx_url = $wmx_url;
                $this->serviceResponse->wmx_callback_url = $this->walletmixGatewayCallbackMobileUrl;
                $this->serviceResponse->responseStat->msg = "token success";
                return $this->response();
            } else {
                $this->serviceResponse->responseData = $wmx_response_d;
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseStat->msg = "token not  success";
                return $this->response();
            }
        } else {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "Server Down Now";
            return $this->response();
        }
    }

    function walletmixCallback() {
        $walletmix = new WALLETMIX();

        $walletmix->set_service_access_username($this->walletmixUsername);
        $walletmix->set_service_access_password($this->walletmixPassword);
        $walletmix->set_merchant_id($this->walletmixMerchantId);
        $walletmix->set_access_app_key($this->walletmixAppKey);

        $walletmix->set_database_driver('session'); // options: "txt" or "session"

        if (isset($_POST['merchant_txn_data'])) {
            $merchant_txn_data = json_decode($_POST['merchant_txn_data']);

            $walletmix->get_database_driver();

            if ($walletmix->get_database_driver() == 'txt') {
                $saved_data = json_decode($walletmix->read_file());
            } elseif ($walletmix->get_database_driver() == 'session') {
                // Read data from your database
                $saved_data = json_decode($walletmix->read_data());
            }

            if ($merchant_txn_data->token === $saved_data->token) {

                $wmx_response = json_decode($walletmix->check_payment($saved_data));
               // $walletmix->debug($wmx_response, true);
                if (($wmx_response->wmx_id == $saved_data->wmx_id)) {
                    if (($wmx_response->txn_status == '1000')) {
                        if (($wmx_response->bank_amount >= $saved_data->amount)) {
                            if (($wmx_response->bank_amount_bdt == $saved_data->amount)) {
                                $this->paymentSuccessGet($wmx_response->merchant_order_id,$wmx_response);
                                 return redirect('checkout/paymentgreeting');
                                //echo 'Update merchant database with success. amount : ' . $wmx_response->bank_amount_bdt;
                            } else {
                                $this->paymentCancelGet($wmx_response->merchant_order_id);
                                $this->pageData['error'] = 'Merchant amount mismatch Merchant Amount : ' . $saved_data->amount . ' Bank Amount : ' . $wmx_response->bank_amount_bdt . '. Update merchant database with success';
                                return view('web.checkout.paymenterror',$this->pageData);
                            }
                        } else {
                            $this->paymentCancelGet($wmx_response->merchant_order_id);
                            $this->pageData['error'] = 'Bank amount is less then merchant amount like partial payment.You can make it failed transaction.';
                            return view('web.checkout.paymenterror',$this->pageData);
                        }
                    } else {
                        $this->paymentCancelGet($wmx_response->merchant_order_id);
                         $this->pageData['error'] = 'Transaction canceled or falied';
                         return view('web.checkout.paymenterror',$this->pageData);
                    }
                } else {
                    $this->paymentCancelGet($wmx_response->merchant_order_id);
                     $this->pageData['error'] = 'Merchant ID Mismatch';
                     return view('web.checkout.paymenterror',$this->pageData);
                }
            } else {
                $this->paymentCancelGet($wmx_response->merchant_order_id);
                $this->pageData['error'] = 'token mismatch for this trasanction';
                return view('web.checkout.paymenterror',$this->pageData);
            }
        } else {
            return redirect('checkout');
        }
    }
    
    function walletmixMobileCallback() {
        $walletmix = new WALLETMIX();

       $walletmix->set_service_access_username($this->walletmixUsername);
        $walletmix->set_service_access_password($this->walletmixPassword);
        $walletmix->set_merchant_id($this->walletmixMerchantId);
        $walletmix->set_access_app_key($this->walletmixAppKey);

        $walletmix->set_database_driver('txt'); // options: "txt" or "session"

        if (isset($_POST['merchant_txn_data'])) {
            $merchant_txn_data = json_decode($_POST['merchant_txn_data']);

            $walletmix->get_database_driver();

            if ($walletmix->get_database_driver() == 'txt') {
                $saved_data = json_decode($walletmix->read_file());
            } elseif ($walletmix->get_database_driver() == 'session') {
                // Read data from your database
                $saved_data = json_decode($walletmix->read_data());
            }

            if ($merchant_txn_data->token === $saved_data->token) {

                $wmx_response = json_decode($walletmix->check_payment($saved_data));
               // $walletmix->debug($wmx_response, true);
                if (($wmx_response->wmx_id == $saved_data->wmx_id)) {
                    if (($wmx_response->txn_status == '1000')) {
                        if (($wmx_response->bank_amount >= $saved_data->amount)) {
                            if (($wmx_response->bank_amount_bdt == $saved_data->amount)) {
                                $this->paymentSuccessGet($wmx_response->merchant_order_id,$wmx_response);
                                 $this->pageData['success'] = 'Thank you for your order. We will contact you soon.';
                                 return view('web.checkout.paymentsuccessrmobile',$this->pageData);
                                //echo 'Update merchant database with success. amount : ' . $wmx_response->bank_amount_bdt;
                            } else {
                                $this->paymentCancelGet($wmx_response->merchant_order_id);
                                $this->pageData['error'] = 'Merchant amount mismatch Merchant Amount : ' . $saved_data->amount . ' Bank Amount : ' . $wmx_response->bank_amount_bdt . '. Update merchant database with success';
                                return view('web.checkout.paymenterrormobile',$this->pageData);
                            }
                        } else {
                            $this->paymentCancelGet($wmx_response->merchant_order_id);
                            $this->pageData['error'] = 'Bank amount is less then merchant amount like partial payment.You can make it failed transaction.';
                            return view('web.checkout.paymenterrormobile',$this->pageData);
                        }
                    } else {
                        $this->paymentCancelGet($wmx_response->merchant_order_id);
                         $this->pageData['error'] = 'Transaction canceled or falied';
                         return view('web.checkout.paymenterrormobile',$this->pageData);
                    }
                } else {
                    $this->paymentCancelGet($wmx_response->merchant_order_id);
                     $this->pageData['error'] = 'Merchant ID Mismatch';
                     return view('web.checkout.paymenterrormobile',$this->pageData);
                }
            } else {
                $this->paymentCancelGet($wmx_response->merchant_order_id);
                $this->pageData['error'] = 'token mismatch for this trasanction';
                return view('web.checkout.paymenterrormobile',$this->pageData);
            }
        } else {
            echo 'Try to direct access';
        }
    }

    function bkash($unique_code) {
        $orderDetails = OrderModel::with('orderCustomer')->with('orderCustomer.userDetails')->where('unique_code', $unique_code)->get();
        if (!isset($orderDetails[0])) {
            return redirect('checkout');
        }
        $order_subtotal = $orderDetails[0]['order_total'];
        $shipping_cost = $orderDetails[0]['shipping_cost'];
        $discount_total = $orderDetails[0]['discount_total'];
        $voucher_discount = $orderDetails[0]['voucher_discount'];
        $employee_discount = $orderDetails[0]['employee_discount'];
        $special_discount = $orderDetails[0]['special_discount'];
        $grand_total = ($order_subtotal + $shipping_cost) - ($discount_total + $employee_discount + $special_discount+$voucher_discount);
        $bkash_charge = ceil($grand_total * 0.02);
        $total_pay = $grand_total + $bkash_charge;
        $this->pageData['orderDetails'] = $orderDetails;
        $this->pageData['grand_total'] = $grand_total;
        $this->pageData['bkash_charge'] = $bkash_charge;
        $this->pageData['total_pay'] = $total_pay;
        return view("web.payment.bkash", $this->pageData);
    }

    function bkashDataProcess(Request $request) {

        $validator = Validator::make($request->all(), [
                    'unique_code' => 'required',
                    'mobileNumber' => 'required|digits:11',
                    'transactionId' => 'required|min:10'
        ]);

        if ($validator->fails()) {

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = $validator->errors()->first();
            return $this->response();
        }
        $token = $request->input("unique_code");
        $mobileNumber = $request->input("mobileNumber");
        $transactionId = $request->input("transactionId");
        $orderDetails = OrderModel::where('unique_code', $token)->get();
        if (!isset($orderDetails[0])) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "Wrong order or Wrong information provided";
            return $this->response();
        }
        $data = array(
            "user" => $this->BkashUserName,
            "psss" => $this->BkashPassword,
            "msisdn" => $this->BakshmsisdnID,
            "trxid" => $transactionId
        );
        $data_json = json_encode($data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->BkashGatewayUrl);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $res = curl_exec($ch);
        //curl_close($ch);
        $result_data = json_decode($res);
        if ($errno = curl_errno($ch)) {
            $error_message = curl_strerror($errno);
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = $error_message;
            return $this->response();
        }

        if ($result_data->transaction->trxStatus == 0000) {
            if($result_data->transaction->sender != $mobileNumber){
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseData = $result_data;
                $this->serviceResponse->responseStat->msg = "Mobile number is matched against TrxID";
                return $this->response();
            }
            $order_subtotal = $orderDetails[0]['order_total'];
            $shipping_cost = $orderDetails[0]['shipping_cost'];
            $discount_total = $orderDetails[0]['discount_total'];
            $voucher_discount = $orderDetails[0]['voucher_discount'];
            $employee_discount = $orderDetails[0]['employee_discount'];
            $special_discount = $orderDetails[0]['special_discount'];
            $grand_total = ($order_subtotal + $shipping_cost) - ($discount_total + $employee_discount + $special_discount+$voucher_discount);
            
            $bkash_charge = ceil($grand_total * 0.02);
            $baksh_total = $grand_total + $bkash_charge;
            if(!$result_data->transaction->amount < $baksh_total){
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseData = $result_data;
                $this->serviceResponse->responseStat->msg = "You can't send correct amount of money for this order ";
                return $this->response();
            }
            $this->serviceResponse->responseStat->status = true;
            $this->serviceResponse->responseData = $result_data;
            $this->serviceResponse->OrderId = $orderDetails[0]['id'];
            $this->serviceResponse->responseStat->msg = $error_message;
            return $this->response();
        } else if ($result_data->transaction->trxStatus == 0010 || $result_data->transaction->trxStatus == 0011) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseData = $result_data;
            $this->serviceResponse->responseStat->msg = "TrxID valid but transaction is in pending state";
            return $this->response();
        }
        if ($result_data->transaction->trxStatus == 0100) {
            $this->serviceResponse->responseStat->status = fasle;
            $this->serviceResponse->responseData = $result_data;
            $this->serviceResponse->responseStat->msg = "TrxID valid but transaction is in reversed";
            return $this->response();
        }
        if ($result_data->transaction->trxStatus == 0111) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseData = $result_data;
            $this->serviceResponse->responseStat->msg = "TrxID valid but transaction has falied";
            return $this->response();
        }
        if ($result_data->transaction->trxStatus == 1001) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseData = $result_data;
            $this->serviceResponse->responseStat->msg = "Invalid MSISDN input .Try correct mobile number";
            return $this->response();
        }
        if ($result_data->transaction->trxStatus == 1002) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseData = $result_data;
            $this->serviceResponse->responseStat->msg = "Invalid MSISDN input does not exist";
            return $this->response();
        }
        if ($result_data->transaction->trxStatus == 1003) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseData = $result_data;
            $this->serviceResponse->responseStat->msg = "Access Denied. UserName or Password is incorrect";
            return $this->response();
        }
        if ($result_data->transaction->trxStatus == 1004) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseData = $result_data;
            $this->serviceResponse->responseStat->msg = "Access Denied. TrxID is not related to the username";
            return $this->response();
        }
        if ($result_data->transaction->trxStatus == 9999) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseData = $result_data;
            $this->serviceResponse->responseStat->msg = "Could not process request at this momment please try again";
            return $this->response();
        } else {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseData = $result_data;
            $this->serviceResponse->responseStat->msg = "Check your Mobile Number & TrxID again";
            return $this->response();
        }
    }

    function paymentSuccess(Request $request) {
        $order_id = $request->input("order_id");
        $payment_details = $request->input("payment_details");
        $orderData = array(
            'payment_status' => "completed",
            'payment_details' =>$payment_details
        );
        if (OrderPaymentsModel::where('order_id', $order_id)->update($orderData)) {
            $this->serviceResponse->responseStat->status = true;
            $this->serviceResponse->responseStat->msg = "Order successfully completed";
            return $this->response();
        } else {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "Please Try again";
            return $this->response();
        }
    }
    
    function paymentSuccessGet($order_id,$payment_details) {
        $order_id = $order_id;
        $payment_details = $payment_details;
        $orderData = array(
            'payment_status' => "completed",
            'payment_details' =>$payment_details
        );
        if (OrderPaymentsModel::where('order_id', $order_id)->update($orderData)) {
            $this->serviceResponse->responseStat->status = true;
            $this->serviceResponse->responseStat->msg = "Order successfully completed";
            return $this->response();
        } else {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "Please Try again";
            return $this->response();
        }
    }

    function paymentCancel(Request $request) {
        $order_id = $request->input("order_id");
        $orderData = array(
            'payment_status' => "cancel",
        );
        if (OrderPaymentsModel::where('order_id', $order_id)->update($orderData)) {
            $OrderProducts = OrderProductsModel::where('order_id', $order_id)->get();
            $productModel = new ProductModel();
            if ($OrderProducts != NULL) {
                foreach ($OrderProducts as $products) {
                    $productModel->setId($products['product_id']);
                    $productModel->increaseQuantity($products['product_quantity']);
                }
            }
            OrderModel::where('id', '=', $order_id)->delete();
            OrderProductsModel::where('order_id', '=', $order_id)->delete();
            //OrderPaymentsModel::where('order_id', '=', $order_id)->delete();
            OrderStatusModel::where('order_id', '=', $order_id)->delete();
            $this->serviceResponse->responseStat->status = true;
            $this->serviceResponse->responseStat->msg = "Order successfully Cancelled";
            return $this->response();
        } else {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "Please Try again";
            return $this->response();
        }
        
    }
    
    function paymentCancelGet($order_id) {
        $orderData = array(
            'payment_status' => "cancel",
        );
        if (OrderPaymentsModel::where('order_id', $order_id)->update($orderData)) {
            $OrderProducts = OrderProductsModel::where('order_id', $order_id)->get();
            $productModel = new ProductModel();
            if ($OrderProducts != NULL) {
                foreach ($OrderProducts as $products) {
                    $productModel->setId($products['product_id']);
                    $productModel->increaseQuantity($products['product_quantity']);
                }
            }
            OrderModel::where('id', '=', $order_id)->delete();
            OrderProductsModel::where('order_id', '=', $order_id)->delete();
            //OrderPaymentsModel::where('order_id', '=', $order_id)->delete();
            OrderStatusModel::where('order_id', '=', $order_id)->delete();
            $this->serviceResponse->responseStat->status = true;
            $this->serviceResponse->responseStat->msg = "Order successfully Cancelled";
            return $this->response();
        } else {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "Please Try again";
            return $this->response();
        }
    }

    //paypal 
    function paypal($order_id) {
        $orderDetails = OrderModel::with('orderCustomer')->with('orderCustomer.userDetails')->where('id', $order_id)->get(); 
        if (!isset($orderDetails[0])) {
            return redirect('checkout');
        }
        $sessionData['paypal_order_id'] = $order_id;
        Session::put($sessionData);
        $order_subtotal = $orderDetails[0]['order_total'];
        $shipping_cost = $orderDetails[0]['shipping_cost'];
        $discount_total = $orderDetails[0]['discount_total'];
        $voucher_discount = $orderDetails[0]['voucher_discount'];
        $employee_discount = $orderDetails[0]['employee_discount'];
        $special_discount = $orderDetails[0]['special_discount'];
        $grand_total = ($order_subtotal + $shipping_cost) - ($discount_total + $employee_discount + $special_discount+$voucher_discount);
        $currebcymodel= new CurrencyModel();
        $item_name = 'Mall Bd Products';
        $item_amount = $currebcymodel->BDTtoUSD($grand_total);
        $querystring = '';

        // Firstly Append paypal account to querystring
        $querystring .= "?business=" . urlencode($this->paypalSellerEmail) . "&";

        // get all post data
        $querystring .= "item_name=" . urlencode($item_name) . "&";
        $querystring .= "amount=" . urlencode($item_amount) . "&";

        $querystring .= "cmd=" . urlencode('_xclick') . "&";
        $querystring .= "no_note=" . urlencode(1) . "&";
        $querystring .= "lc=" . urlencode('USA') . "&";
        $querystring .= "currency_code=" . urlencode('USD') . "&";
        $querystring .= "bn=" . urlencode('PP-BuyNowBF:btn_buynow_LG.gif:NonHostedGuest') . "&";
        $querystring .= "first_name=" . urlencode($orderDetails[0]['orderCustomer']['firstname']) . "&";
        $querystring .= "last_name=" . urlencode($orderDetails[0]['orderCustomer']['lastname']) . "&";
        $querystring .= "payer_email=" . urlencode($orderDetails[0]['orderCustomer']['eamil']) . "&";
        $querystring .= "item_number=" . urlencode($order_id) . "&";


        // Append paypal return addresses
        $querystring .= "return=" . urlencode(stripslashes($this->paypalReturnUrl)) . "&";
        $querystring .= "cancel_return=" . urlencode(stripslashes($this->paypalCancelUrl)) . "&";
        $querystring .= "notify_url=" . urlencode($this->paypalNotifyUrl);

        // Redirect to paypal IPN
        return redirect($this->paypalGatewayUrl . $querystring);
    }

    function paypalReturn() {
        if(isset($_REQUEST)){
            $this->paymentSuccessGet($_REQUEST['item_number'], json_encode($_REQUEST));
            return redirect('checkout/paymentgreeting');
        }
             return redirect('checkout');
        
    }
    function paypalCancel() {       
        if(Session::get('paypal_order_id') !=NULL){
            $order_id=Session::get('paypal_order_id');
            $this->paymentCancelGet($order_id);
            return redirect('checkout');            
        }
        return redirect('checkout');
        
    }

    function paypalNotify() {
        echo '<pre>';
        print_r($_REQUEST);
    }
    
    
    //payment status check by id
    function paymentStatusCheckById(Request $request) {
        $order_id = $request->input("order_id");
        $orderDetails = OrderPaymentsModel::where('order_id', $order_id)->get();
        if (!isset($orderDetails[0])) {
             $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "Wrong Order ID";
            return $this->response();
        }   
        if($orderDetails[0]['payment_status'] == 'completed'){
             $this->serviceResponse->responseStat->status = true;
            $this->serviceResponse->responseStat->msg = "Order successfully Completed";
            return $this->response();
        }
        else{
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "Order cancel or onprocess";
            return $this->response();
        }
       
        
    }
    
    //sms 
    //Send SMS
    function sendSMS(Request $request) {
        $validator = Validator::make($request->all(), [
            'mobile_number' => 'required',
            'message' => 'required'
        ]);

        if ($validator->fails())
        {
            return response()->json(['success' => FALSE,'error' =>$validator->errors()->first()], 200);
            
        }
        $mobileNumber = $request->input("mobile_number");
        $message = $request->input("message");
        $sending_url=  $this->smssendurl."?username=".$this->smsusername."&password=".$this->smspassword."&type=0&destination=".$mobileNumber."&source=".urlencode($this->smssource)."&message=".urlencode($message);
         
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $sending_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $res = curl_exec($ch);
        curl_close($ch);       
       
        $result_data = json_decode($res);
        if($result_data->statusCode == 1000){
            $this->serviceResponse->responseStat->status = true;
            $this->serviceResponse->responseStat->msg = $result_data->statusMsg;
            return $this->response();
        }
        else{
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = $result_data->statusMsg;
            return $this->response();
        }
        
    }

}

