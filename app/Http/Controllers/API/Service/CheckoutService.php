<?php

namespace App\Http\Controllers\API\Service;

use App\Http\Controllers\BaseMallBDController;
use App\Http\Controllers\RequestErrorObj;
use App\Model\CurrencyModel;
use App\Model\CustomerRewardModel;
use App\Model\DataModel\PackageProduct;
use App\Model\LoginModel;
use App\Model\NotificationModel;
use App\Model\OrderAttributeModel;
use App\Model\OrderModel;
use App\Model\OrderPaymentsModel;
use App\Model\OrderProductsModel;
use App\Model\OrderStatusModel;
use App\Model\PackageModel;
use App\Model\ProductModel;
use App\Model\ProductQuantityModel;
use App\Model\ShippingAddressModel;
use App\Model\ZonalDeliveryBoyModel;
use App\Model\ZonalOrderModel;
use App\Model\OrderVoucherModel;
use Illuminate\Http\Request;
use App\Model\UserModel;
use App\Model\UserDetailsModel;
use App\Model\VoucherModel;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;
use Illuminate\Support\Facades\Mail;

/**
 * Project  : mallbdweb
 * File     : CheckoutService.php
 */
class CheckoutService extends BaseMallBDController {

    public function submitCheckout(Request $request) {

        DB::beginTransaction();

        $shoppingCartStr = $request->input("shopping_cart");
        $shopping_cart = json_decode($shoppingCartStr, true);

        if (count($shopping_cart['productCell']) <= 0 && count($shopping_cart['mallBdPackageCell']) <= 0) {
            $this->setModelFunctionError("shopping_cart", "Your shopping cart empty");

            $this->serviceResponse->responseStat->msg = "Your shopping cart empty";

            return $this->response();
        }
        $totalPoint = 0;
        $operationInsert = true;
        $userModel = new UserModel();
        $setEmail = $userModel->setEmail($request->input("email"), false);


        $userLoginModel = new LoginModel();

        $loginModel = new LoginModel();
        $obj = null;
        if ($setEmail && !$this->serviceResponse->responseStat->isLogin) {

            $loginModel->setEmail($request->input("email"));
            if ($loginModel->isEmailExist()) {
                $extraObj = new \stdclass();

                $extraObj->hasLoginCreadential = true;
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseStat->extra = $extraObj;

                return $this->response();
            }
        }

        // If email already exist but don't have login credential (Guest) then update information

        if ($setEmail && $userModel->isEmailExist()) {
            $operationInsert = false;
            $userModel = UserModel::find($userModel->fnObj->id);
        }
        if ($userModel->isAdminEmail()) {
            $this->controllerErrorObj->params = "email";
            $this->controllerErrorObj->msg = "You are not allowed to use this email address";
            $this->setControllerError($this->controllerErrorObj);


            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "You are not allowed to user this email address";
            return $this->response();
        }

        $userModel->setFirstname($request->input("first_name"));
        $userModel->setLastname($request->input("last_name"));
        $userModel->setPhone($request->input("phone"));
        if (!$this->serviceResponse->responseStat->isLogin) {
            $userModel->setRoleId("8");
        }
        $userModel->setStatus("Active");



        $userDetailModel = new UserDetailsModel();

        // If email already exist but don't have login credential (Guest) then update information
        if (!$operationInsert) {
            $userDetailModel = UserDetailsModel::find($userModel->userDetails->id);
        }

        $userDetailModel->setAddress($request->input("address"));
        $userDetailModel->setCity($request->input("city"));
        //$userDetailModel->setZipcode($request->input("zipcode"));
        //$userDetailModel->setCountry($request->input("country"));
        $userDetailModel->setShippingAddress($request->input("shipping_address"));
        // $userDetailModel->setShippingCountry($request->input("shipping_country"));
        //$userDetailModel->setShippingZipcode($request->input("shipping_zipcode"));
        $userDetailModel->setShippingCity($request->input("shipping_city"));

        $this->setError($userDetailModel->errorManager->errorObj);

        $currencyModel = new CurrencyModel();
        $currencyModel->setId($request->input("currency_id"));

        $this->setError($currencyModel->errorManager->errorObj);


        $currency = $currencyModel->getCurrencyById();




        $orderModel = new OrderModel();
        $orderModel->setShopId($this->shopId);
        $orderModel->setOrderFrom($request->input("order_from"));
        try {
            $orderModel->setCurrencyId(@$currency->id);
            $orderModel->setCurrencyCode(@$currency->code);
            $orderModel->setCurrencyValue(@$currency->value);
        } catch (\Exception $ex) {
            $this->controllerErrorObj->params = "currency_id";
            $this->controllerErrorObj->msg = $ex->getMessage();
            $this->setControllerError($this->controllerErrorObj);
        }

        try {
            $orderModel->setOrderTotal($shopping_cart['orderTotal']);
        } catch (\Exception $ex) {
            $this->controllerErrorObj->params = "shopping_cart";
            $this->controllerErrorObj->msg = $ex->getMessage();
            $this->setControllerError($this->controllerErrorObj);
        }

//        $orderModel->setPackageSize('Large');
//        $orderModel->setPackageWeight('1 Ton');

        try {
            $orderModel->setShippingCost($shopping_cart['shippingCost']);
        } catch (\Exception $ex) {
            $this->controllerErrorObj->params = "shopping_cart";
            $this->controllerErrorObj->msg = $ex->getMessage();
            $this->setControllerError($this->controllerErrorObj);
        }

        //new changes
        if ($request->input("invoice_address") && $request->input("invoice_address") == 'true') {
            $orderModel->setShippingAddress($request->input("shipping_address"));
            // $orderModel->setShippingCountry($request->input("shipping_country"));
            //$orderModel->setShippingZipcode($request->input("shipping_zipcode"));
            $orderModel->setShippingCity($request->input("shipping_city"));
            $orderModel->setShippingFirstName($request->input("shipping_firstname"));
            $orderModel->setShippingLastName($request->input("shipping_lastname"));
            $orderModel->setShippingPhone($request->input("shipping_phone"));
        } else {
            $orderModel->setShippingAddress($request->input("address"));
            // $orderModel->setShippingCountry($request->input("shipping_country"));
            //$orderModel->setShippingZipcode($request->input("shipping_zipcode"));
            $orderModel->setShippingCity($request->input("city"));
            $orderModel->setShippingFirstName($request->input("first_name"));
            $orderModel->setShippingLastName($request->input("last_name"));
            $orderModel->setShippingPhone($request->input("phone"));
        }
        //new changes
        $orderModel->setIsWrapped('no');
        $orderModel->setDeliveryMethodsId($request->input("delivery_method_id"));

        $this->setError($orderModel->errorManager->errorObj);


        $orderPaymentsModel = new OrderPaymentsModel();


        $orderPaymentsModel->setPaymentMethodId($request->input("payment_method_id"));
        $orderPaymentsModel->setPaymentTotal($shopping_cart['totalPrice']);
        if ($request->input("payment_method_id") == 1) {
            $orderPaymentsModel->setPaymentStatus("completed");
        }
        //$orderPaymentsModel->setPaymentDetails($request->input("payment_details"));


        $this->setError($orderPaymentsModel->errorManager->errorObj);

        // $zonalDeliveryBoyModel = new ZonalDeliveryBoyModel();
        // $zonalOrderModel = new ZonalOrderModel();
        //$zoneDeliveryBoyId = 0;
//        if($orderModel->delivery_methods_id ==1 ){
//            $setZoneId = $zonalDeliveryBoyModel->setZoneId($request->input("zone_id"));
//            if($setZoneId){
//
//                $zoneDeliveryBoyId = $zonalDeliveryBoyModel->getFirstDeliveryIdByZoneId();
//                if($zonalDeliveryBoyModel->fnError){
//                    $this->setModelFunctionError("zone_id",$zonalDeliveryBoyModel->fnErrorMsg);
//                }
//                $zonalOrderModel->setZoneId($request->input("zone_id"));
//                $zonalOrderModel->setUserId($zoneDeliveryBoyId);
//
//                $this->setError($zonalOrderModel->errorManager->errorObj);
//
//                $orderModel->setZoneId($request->input("zone_id")); //$request->input("zone_id")
//            }
//            $this->setError($zonalDeliveryBoyModel->errorManager->errorObj);
//        }

        $this->setError($userModel->errorManager->errorObj);

        if ($this->hasError()) {
            return $this->response();
        }

        // User Table Insert
        if (!$userModel->save()) {
            DB::rollBack();

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "Internal error on UserModel";
            return $this->response();
        }

        // User_Details Table Insert
        $userDetailModel->setUserId($userModel->id);

        if (!$userDetailModel->save()) {
            DB::rollBack();

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "Internal error on UserDetailModel";
            return $this->response();
        }
        // Order Table Insert
        $orderModel->setCustomerId($userModel->id);
        $orderModel->setCreatedBy($userModel->id);
        if (!$orderModel->save()) {
            DB::rollBack();

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "Internal error on orderModel";
            return $this->response();
        }

        // Order Table Update Invoice by Order Id [ Invoice is depending on order id ]
        $orderModel->setInvoiceNo($this->generateInvoceNumber($orderModel->id));
        $orderModel->setUniqueCode($this->orderUniuqeNumber(10));

        if (!$orderModel->save()) {
            DB::rollBack();

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "Internal error on updating Invoice orderModel";
            return $this->response();
        }

        // Order Payment Table Insert
        $orderId = $orderModel->id;

        $orderPaymentsModel->setOrderId($orderModel->id);
        $orderPaymentsModel->setCreatedBy($userModel->id);

        if (!$orderPaymentsModel->save()) {
            DB::rollBack();

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "Internal error on orderPaymentsModel";
            return $this->response();
        }



        //inserting into orderProducts
        // Parsing Shopping cart
        /* ===Started ==== */

        $reducedShoppingCart = array();

        if (count($shopping_cart['productCell']) <= 0 && count($shopping_cart['mallBdPackageCell']) <= 0) {
            $this->setModelFunctionError("shopping_cart", "Your shopping basket empty");

            $this->serviceResponse->responseStat->msg = "Your shopping basket empty";

            return $this->response();
        }

        foreach ($shopping_cart['productCell'] as $cart) {
            if (array_key_exists($cart['id'], $reducedShoppingCart)) {
                $reducedShoppingCart[$cart['id']]['quantity'] += $cart['quantity'];
                $totalPoint = $totalPoint + ($cart['quantity'] * $cart['product']['point']);
            } else {
                $reducedShoppingCart[$cart['id']] = array();
                $reducedShoppingCart[$cart['id']]['quantity'] = $cart['quantity'];
                $totalPoint = $totalPoint + ($cart['quantity'] * $cart['product']['point']);
                $reducedShoppingCart[$cart['id']]['price'] = @$cart['product']['prices'][0]['retailPrice'];
            }
            //  echo $cart['quantity']*$cart['product']['point'];
        }



        /* === Order Product entry [Started] */

        $productModel = new ProductModel();
        $totalDiscountAmount = 0;
        $totalOrder = 0;

        foreach ($reducedShoppingCart as $key => $value) {

            $productModel->setId($key);
            $product = $productModel->getById();
            if (!$productModel->decreaseQuantity((int) $value['quantity'])) {
                DB::rollBack();
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseStat->msg = 'Sorry this product : ' . $product->title . ' Out of stock now';
                return $this->response();
            }
            $productPrice = 0;
            $discountAmount = 0;
            try {
                $productPrice = $product->prices[0]->retailPrice;
            } catch (Exception $ex) {
                DB::rollBack();
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseStat->msg = 'Price not found for product ' . $product->title . ' ';
                return $this->response();
            }

            $orderProductsModelObj = new OrderProductsModel();

            $discountAmount = $product->discountAmount * $value['quantity'];

            $orderProductsModelObj->setOrderId($orderModel->id);
            $orderProductsModelObj->setProductId($key);
            $orderProductsModelObj->setPrice($productPrice);
            $orderProductsModelObj->setProductQuantity($value['quantity']);
            $orderProductsModelObj->setTotal($productPrice * $value['quantity']);
            $orderProductsModelObj->setDiscount($discountAmount);

            try {

                $totalDiscountAmount = $totalDiscountAmount + $discountAmount;
            } catch (Exception $ex) {

                DB::rollBack();
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseStat->msg = 'Discount value unacceptable for Product : ' . $product->title . ' ';
                return $this->response();
            }
            //$orderProductsModelObj->setDiscountId(1);
            $orderProductsModelObj->setTax(0.0);



            $this->setError($orderProductsModelObj->errorManager->errorObj);

            if ($this->hasError()) {
                DB::rollBack();
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseStat->msg = "Internal error on fetching product from reducedShoppingCart";
                return $this->response();
            }

            if (!$orderProductsModelObj->save()) {
                DB::rollBack();
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseStat->msg = "Internal error on orderProductsModel";
                return $this->response();
            }
        }
        foreach ($shopping_cart['mallBdPackageCell'] as $key => $value) {

            $packageId = $value['mallBdPackage']['id'];
            $packageQuantity = $value['quantity'];
            $productPrice = 0;
            $discountAmount = 0;

            $packageModel = new PackageModel();
            $packageModel->setId($packageId);
            $mallBdPackage = $packageModel->getPackageById();
            foreach ($mallBdPackage->packageProduct as $packageProduct) {
                $productModel->setId($packageProduct->product->id);
                if (!$productModel->decreaseQuantity($packageProduct->quantity * $packageQuantity)) {
                    DB::rollBack();
                    $this->serviceResponse->responseStat->status = false;
                    $this->serviceResponse->responseStat->msg = 'Sorry this product : ' . $packageProduct->product->title . ' Out of stock now ' . $packageProduct->quantity;
                    return $this->response();
                }
                try {
                    $productPrice = $packageProduct->product->prices[0]->retailPrice;
                } catch (Exception $ex) {
                    DB::rollBack();
                    $this->serviceResponse->responseStat->status = false;
                    $this->serviceResponse->responseStat->msg = 'Price not found for product ' . $packageProduct->product->title . ' ';
                    return $this->response();
                }


                $orderProductsModelObj = new OrderProductsModel();

                $discountAmount = $mallBdPackage->originalPriceTotal - $mallBdPackage->packagePriceTotal;
                $orderProductsModelObj->setOrderId($orderModel->id);
                $orderProductsModelObj->setProductId($packageProduct->product->id);
                $orderProductsModelObj->setPackageId($packageId);
                $orderProductsModelObj->setPrice($productPrice);
                $orderProductsModelObj->setProductQuantity($packageProduct->quantity * $packageQuantity);
                $orderProductsModelObj->setPackageQuantity($packageQuantity);
                $orderProductsModelObj->setTotal($productPrice * $packageProduct->quantity * $packageQuantity);
                $orderProductsModelObj->setDiscount($discountAmount);

                $orderProductsModelObj->setTax(0.0);



                $this->setError($orderProductsModelObj->errorManager->errorObj);

                if ($this->hasError()) {
                    DB::rollBack();
                    $this->serviceResponse->responseStat->status = false;
                    $this->serviceResponse->responseStat->msg = "Internal error on fetching package from mallBdPackageCell";
                    return $this->response();
                }

                if (!$orderProductsModelObj->save()) {
                    DB::rollBack();
                    $this->serviceResponse->responseStat->status = false;
                    $this->serviceResponse->responseStat->msg = "Internal error on saving package from mallBdPackageCell";
                    return $this->response();
                }
            }
//            $totalDiscountAmount = $totalDiscountAmount + $discountAmount;
        }
        /* === Order Product entry [Ends] */

        /* Discount Entry [Started] */
        $orderModel->setDiscountTotal($totalDiscountAmount);
        $orderModel->setVoucherDiscountTotal($shopping_cart['voucherDiscount']);

        $this->setError($orderModel->errorManager->errorObj);
        if ($this->hasError()) {
            DB::rollBack();
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "Internal error on orderModel on discount set";
            return $this->response();
        }
        if (!$orderModel->save()) {
            DB::rollBack();
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "Internal error on orderModel on discount update";
            return $this->response();
        }
        /* Discount Entry [Ends] */
        foreach ($shopping_cart['productCell'] as $cart) {
            $attributeList = [];
            $productId = 0;
            $combinationKey = 0;
            if ($cart['selectedAttributes'] != null) {
                foreach ($cart['selectedAttributes'] as $attribute) {
                    $productId = $cart['id'];
                    array_push($attributeList, $attribute['id']);
                }
                $strOfArr = implode(",", $attributeList);
                $strOfArr = trim($strOfArr);
                $result = ProductQuantityModel::where('product_id', '=', $productId)
                                ->where('combination', '=', $strOfArr)
                                ->get()->first();
                $combinationKey = $result['combination_key'];
            }

            $productId = $cart['id'];
            $quantity = $cart['quantity'];

            $orderAttrModel = new OrderAttributeModel();

            $orderAttrModel->setOrderId($orderId);
            $orderAttrModel->setProductId($productId);
            $orderAttrModel->setCombinationKey($combinationKey);
            $orderAttrModel->setQuantity($quantity);

            $this->setError($orderAttrModel->errorManager->errorObj);

            if ($this->hasError()) {

                DB::rollBack();
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseStat->msg = "Internal error on fetching product from productCell. No product combination key found";
                return $this->response();
            }
            if (!$orderAttrModel->save()) {
                DB::rollBack();
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseStat->msg = "Internal error on orderAttrModel";
                return $this->response();
            }
        }
        /* ===Ends==== */


        // Inserting to customerRewardModel
        $customerRewardModel = new CustomerRewardModel();

        $customerRewardModel->setUserId($userModel->id);
        $customerRewardModel->setOrderId($orderModel->id);
        $customerRewardModel->setDescription("");
        $customerRewardModel->setPoints($totalPoint);

        $this->setError($customerRewardModel->errorManager->errorObj);

        if ($this->hasError()) {
            DB::rollBack();
            return $this->response();
        }
        if (!$customerRewardModel->save()) {
            DB::rollBack();

            $this->serviceResponse->responseStat->msg = "Internal error on  customerRewardModel";
            return $this->response();
        }

        $orderStatusModel = new OrderStatusModel();

        $orderStatusModel->setOrderId($orderModel->id);
        $orderStatusModel->setCreatedBy($userModel->id);
        $orderStatusModel->setStatusId(1);
        $this->setError($orderStatusModel->errorManager->errorObj);

        if ($this->hasError()) {
            DB::rollBack();
            return $this->response();
        }

        if (!$orderStatusModel->save()) {
            DB::rollBack();

            $this->serviceResponse->responseStat->msg = "Internal error on  orderStatusModel";
            return $this->response();
        }

//        if ($orderModel->delivery_methods_id == 1 && $zoneDeliveryBoyId > 0) {
//
//            $zonalOrderModel->setOrderId($orderModel->id);
//
//            if (!$zonalOrderModel->save()) {
//                DB::rollBack();
//
//                $this->serviceResponse->responseStat->msg = "Internal error on  zonalOrderModel";
//                return $this->response();
//            }
//        }




        DB::commit();




        $notificationModel = new NotificationModel();

        $notificationModel->setNotificationType("order");
        $notificationModel->setNotificationId($orderModel->id);

        $text = "New Order Placed for Total " . $shopping_cart['totalPrice'];

        $notificationModel->setNotificationText($text);

        $notificationModel->save();

        /* === Call Order entry cron job to backoffice== */

        $this->callBackOfficeCron($orderModel->id);
        $grand_total = ($orderModel->order_total + $orderModel->shipping_cost) - ($orderModel->discount_total + $orderModel->voucher_discount);
//        $bkash_charge = ceil($grand_total * 0.02);
        ////sms send for order store to the customer
        $customerDetails = UserModel::find($orderModel->customer_id);
        if (sizeof($customerDetails) > 0) {
            // Get cURL resource
            $curl = curl_init();
            // Set some options - we are passing in a useragent too here
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => url('/api/sendsms'),
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => array(
                    'mobile_number' => $customerDetails->phone,
                    'message' => 'Dear Mr. ' . $customerDetails->firstname . ' ' . $customerDetails->lastname . ', your order have been placed. Thank you for being with us.',
                )
            ));
            // Send the request & save response to $resp
            $resp = curl_exec($curl);
            // Close request to clear up some resources
            curl_close($curl);

            //curl process ends
            ////sms sending ends
            ////mail sending starts
            Mail::send('emails.orderplaced', ['name' => $customerDetails->firstname . ' ' . $customerDetails->lastname], function($message) use ($customerDetails) {
                $message->to($customerDetails->email, $customerDetails->firstname . ' ' . $customerDetails->lastname)->subject('Order placed successfully');
            });
            ////mail sending ends
        }
        $bkash_charge = 0.00;
        $total_bkash_pay = $grand_total;
        $currebcymodel = new CurrencyModel();
        $paypal_item_amount = $currebcymodel->BDTtoUSD($grand_total);
        $respone_data_checkout = array(
            'order_id' => $orderModel->id,
            'unique_code' => $orderModel->unique_code,
            'invoice_no' => $orderModel->invoice_no,
            'order_grand_total' => $grand_total,
            'bkash_charge' => $bkash_charge,
            'total_bkash_pay' => $total_bkash_pay,
            'paypal_pay_amount' => $paypal_item_amount,
        );

        $this->serviceResponse->responseStat->msg = "Order successfully done";

        $this->serviceResponse->responseData = $respone_data_checkout;
        return $this->response();
    }

    public function submitCheckoutWeb(Request $request) {
        $voucherDetails = json_decode($request->input("voucherDiscountDetails"), true);
//        var_dump(json_decode($request->input("voucherDiscountDetails"), true));
//        $this->serviceResponse->responseStat->status = false;
//
//        return $this->response();
//        echo "<pre>";
//        print_r($_POST);die;

        DB::beginTransaction();

        $shoppingCartStr = $request->input("shopping_cart");
        $shopping_cart = json_decode($shoppingCartStr, true);

        if (count($shopping_cart['productCell']) <= 0 && count($shopping_cart['mallBdPackageCell']) <= 0) {
            $this->setModelFunctionError("shopping_cart", "Your shopping cart empty");

            $this->serviceResponse->responseStat->msg = "Your shopping cart empty";

            return $this->response();
        }
        $totalPoint = 0;
        $operationInsert = true;
        $userModel = new UserModel();
        $setEmail = $userModel->setEmail($request->input("email"), false);


        $userLoginModel = new LoginModel();

        $loginModel = new LoginModel();
        $obj = null;
        if ($setEmail && !$this->serviceResponse->responseStat->isLogin) {

            $loginModel->setEmail($request->input("email"));
            if ($loginModel->isEmailExist()) {
                $extraObj = new \stdclass();

                $extraObj->hasLoginCreadential = true;
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseStat->extra = $extraObj;

                return $this->response();
            }
        }

        // If email already exist but don't have login credential (Guest) then update information

        if ($setEmail && $userModel->isEmailExist()) {
            $operationInsert = false;
            $userModel = UserModel::find($userModel->fnObj->id);
        }
        if ($userModel->isAdminEmail()) {
            $this->controllerErrorObj->params = "email";
            $this->controllerErrorObj->msg = "You are not allowed to use this email address";
            $this->setControllerError($this->controllerErrorObj);


            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "You are not allowed to user this email address";
            return $this->response();
        }

        $userModel->setFirstname($request->input("first_name"));
        $userModel->setLastname($request->input("last_name"));
        $userModel->setPhone($request->input("phone"));
        if (!$this->serviceResponse->responseStat->isLogin) {
            $userModel->setRoleId("8");
        }
        $userModel->setStatus("Active");



        $userDetailModel = new UserDetailsModel();

        // If email already exist but don't have login credential (Guest) then update information
        if (!$operationInsert) {
            $userDetailModel = UserDetailsModel::find($userModel->userDetails->id);
        }

        $userDetailModel->setAddress($request->input("address"));
        $userDetailModel->setCity($request->input("city"));
        //$userDetailModel->setZipcode($request->input("zipcode"));
        //$userDetailModel->setCountry($request->input("country"));
        $userDetailModel->setShippingAddress($request->input("shipping_address"));
        // $userDetailModel->setShippingCountry($request->input("shipping_country"));
        //$userDetailModel->setShippingZipcode($request->input("shipping_zipcode"));
        $userDetailModel->setShippingCity($request->input("shipping_city"));

        $this->setError($userDetailModel->errorManager->errorObj);

        $currencyModel = new CurrencyModel();
        $currencyModel->setId($request->input("currency_id"));

        $this->setError($currencyModel->errorManager->errorObj);


        $currency = $currencyModel->getCurrencyById();




        $orderModel = new OrderModel();
        $orderModel->setShopId($this->shopId);
        $orderModel->setOrderFrom($request->input("order_from"));
        try {
            $orderModel->setCurrencyId(@$currency->id);
            $orderModel->setCurrencyCode(@$currency->code);
            $orderModel->setCurrencyValue(@$currency->value);
        } catch (\Exception $ex) {
            $this->controllerErrorObj->params = "currency_id";
            $this->controllerErrorObj->msg = $ex->getMessage();
            $this->setControllerError($this->controllerErrorObj);
        }

        try {
            $orderModel->setOrderTotal($shopping_cart['orderTotal']);
        } catch (\Exception $ex) {
            $this->controllerErrorObj->params = "shopping_cart";
            $this->controllerErrorObj->msg = $ex->getMessage();
            $this->setControllerError($this->controllerErrorObj);
        }

//        $orderModel->setPackageSize('Large');
//        $orderModel->setPackageWeight('1 Ton');

        try {
            $orderModel->setShippingCost($shopping_cart['shippingCost']);
        } catch (\Exception $ex) {
            $this->controllerErrorObj->params = "shopping_cart";
            $this->controllerErrorObj->msg = $ex->getMessage();
            $this->setControllerError($this->controllerErrorObj);
        }
        //new changes
        if ($request->input("invoice_address") && $request->input("invoice_address") == 'true') {
            $orderModel->setShippingAddress($request->input("shipping_address"));
            // $orderModel->setShippingCountry($request->input("shipping_country"));
            //$orderModel->setShippingZipcode($request->input("shipping_zipcode"));
            $orderModel->setShippingCity($request->input("shipping_city"));
            $orderModel->setShippingFirstName($request->input("shipping_firstname"));
            $orderModel->setShippingLastName($request->input("shipping_lastname"));
            $orderModel->setShippingPhone($request->input("shipping_phone"));
        } else {
            $orderModel->setShippingAddress($request->input("address"));
            // $orderModel->setShippingCountry($request->input("shipping_country"));
            //$orderModel->setShippingZipcode($request->input("shipping_zipcode"));
            $orderModel->setShippingCity($request->input("city"));
            $orderModel->setShippingFirstName($request->input("first_name"));
            $orderModel->setShippingLastName($request->input("last_name"));
            $orderModel->setShippingPhone($request->input("phone"));
        }

        //new changes

        $orderModel->setIsWrapped('no');
        $orderModel->setDeliveryMethodsId($request->input("delivery_method_id"));

        $this->setError($orderModel->errorManager->errorObj);


        $orderPaymentsModel = new OrderPaymentsModel();


        $orderPaymentsModel->setPaymentMethodId($request->input("payment_method_id"));
        $orderPaymentsModel->setPaymentTotal($shopping_cart['totalPrice']);
        if ($request->input("payment_method_id") == 1) {
            $orderPaymentsModel->setPaymentStatus("completed");
        }
        //$orderPaymentsModel->setPaymentDetails($request->input("payment_details"));


        $this->setError($orderPaymentsModel->errorManager->errorObj);

        // $zonalDeliveryBoyModel = new ZonalDeliveryBoyModel();
        // $zonalOrderModel = new ZonalOrderModel();
        //$zoneDeliveryBoyId = 0;
//        if($orderModel->delivery_methods_id ==1 ){
//            $setZoneId = $zonalDeliveryBoyModel->setZoneId($request->input("zone_id"));
//            if($setZoneId){
//
//                $zoneDeliveryBoyId = $zonalDeliveryBoyModel->getFirstDeliveryIdByZoneId();
//                if($zonalDeliveryBoyModel->fnError){
//                    $this->setModelFunctionError("zone_id",$zonalDeliveryBoyModel->fnErrorMsg);
//                }
//                $zonalOrderModel->setZoneId($request->input("zone_id"));
//                $zonalOrderModel->setUserId($zoneDeliveryBoyId);
//
//                $this->setError($zonalOrderModel->errorManager->errorObj);
//
//                $orderModel->setZoneId($request->input("zone_id")); //$request->input("zone_id")
//            }
//            $this->setError($zonalDeliveryBoyModel->errorManager->errorObj);
//        }

        $this->setError($userModel->errorManager->errorObj);

        if ($this->hasError()) {
            return $this->response();
        }

        // User Table Insert
        if (!$userModel->save()) {
            DB::rollBack();

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "Internal error on UserModel";
            return $this->response();
        }

        // User_Details Table Insert
        $userDetailModel->setUserId($userModel->id);

        if (!$userDetailModel->save()) {
            DB::rollBack();

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "Internal error on UserDetailModel";
            return $this->response();
        }
        // Order Table Insert
        $orderModel->setOnpurchaseDiscount($request->input("customerDiscount"));
        $customerPurchaseDiscount = json_decode($request->input("customerDiscountDetails"), TRUE);
        if ($customerPurchaseDiscount['id'] != 0) {
            $orderModel->setCustomerDiscountDetails(serialize($customerPurchaseDiscount));
        }

        $orderModel->setCustomerId($userModel->id);
        $orderModel->setCreatedBy($userModel->id);
        if (!$orderModel->save()) {
            DB::rollBack();

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "Internal error on orderModel";
            return $this->response();
        }

        // Order Table Update Invoice by Order Id [ Invoice is depending on order id ]
        $orderModel->setInvoiceNo($this->generateInvoceNumber($orderModel->id));
        $orderModel->setUniqueCode($this->orderUniuqeNumber(10));

        if (!$orderModel->save()) {
            DB::rollBack();

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "Internal error on updating Invoice orderModel";
            return $this->response();
        }

        // Order Payment Table Insert
        $orderId = $orderModel->id;

        $orderPaymentsModel->setOrderId($orderModel->id);
        $orderPaymentsModel->setCreatedBy($userModel->id);

        if (!$orderPaymentsModel->save()) {
            DB::rollBack();

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "Internal error on orderPaymentsModel";
            return $this->response();
        }



        //inserting into orderProducts
        // Parsing Shopping cart
        /* ===Started ==== */

        $reducedShoppingCart = array();

        if (count($shopping_cart['productCell']) <= 0 && count($shopping_cart['mallBdPackageCell']) <= 0) {
            $this->setModelFunctionError("shopping_cart", "Your shopping basket empty");

            $this->serviceResponse->responseStat->msg = "Your shopping basket empty";

            return $this->response();
        }

        foreach ($shopping_cart['productCell'] as $cart) {
            if (array_key_exists($cart['id'], $reducedShoppingCart)) {
                $reducedShoppingCart[$cart['id']]['quantity'] += $cart['quantity'];
                $totalPoint = $totalPoint + ($cart['quantity'] * $cart['product']['point']);
            } else {
                $reducedShoppingCart[$cart['id']] = array();
                $reducedShoppingCart[$cart['id']]['quantity'] = $cart['quantity'];
                $totalPoint = $totalPoint + ($cart['quantity'] * $cart['product']['point']);
                $reducedShoppingCart[$cart['id']]['price'] = @$cart['product']['prices'][0]['retailPrice'];
            }
            //  echo $cart['quantity']*$cart['product']['point'];
        }



        /* === Order Product entry [Started] */

        $productModel = new ProductModel();
        $totalDiscountAmount = 0;
        $totalOrder = 0;

        foreach ($reducedShoppingCart as $key => $value) {

            $productModel->setId($key);
            $product = $productModel->getById();
            if (!$productModel->decreaseQuantity((int) $value['quantity'])) {
                DB::rollBack();
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseStat->msg = 'Sorry this product : ' . $product->title . ' Out of stock now';
                return $this->response();
            }
            $productPrice = 0;
            $discountAmount = 0;
            try {
                $productPrice = $product->prices[0]->retailPrice;
            } catch (Exception $ex) {
                DB::rollBack();
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseStat->msg = 'Price not found for product ' . $product->title . ' ';
                return $this->response();
            }

            $orderProductsModelObj = new OrderProductsModel();

            if ($product->discountActiveFlag)
                $discountAmount = $product->discountAmount * $value['quantity'];

            $orderProductsModelObj->setOrderId($orderModel->id);
            $orderProductsModelObj->setProductId($key);
            $orderProductsModelObj->setPrice($productPrice);
            $orderProductsModelObj->setProductQuantity($value['quantity']);
            $orderProductsModelObj->setTotal($productPrice * $value['quantity']);
            $orderProductsModelObj->setDiscount($discountAmount);

            try {

                $totalDiscountAmount = $totalDiscountAmount + $discountAmount;
            } catch (Exception $ex) {

                DB::rollBack();
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseStat->msg = 'Discount value unacceptable for Product : ' . $product->title . ' ';
                return $this->response();
            }
            //$orderProductsModelObj->setDiscountId(1);
            $orderProductsModelObj->setTax(0.0);



            $this->setError($orderProductsModelObj->errorManager->errorObj);

            if ($this->hasError()) {
                DB::rollBack();
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseStat->msg = "Internal error on fetching product from reducedShoppingCart";
                return $this->response();
            }

            if (!$orderProductsModelObj->save()) {
                DB::rollBack();
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseStat->msg = "Internal error on orderProductsModel";
                return $this->response();
            }
        }
        foreach ($shopping_cart['mallBdPackageCell'] as $key => $value) {

            $packageId = $value['mallBdPackage']['id'];
            $packageQuantity = $value['quantity'];
            $productPrice = 0;
            $discountAmount = 0;

            $packageModel = new PackageModel();
            $packageModel->setId($packageId);
            $mallBdPackage = $packageModel->getPackageById();
            foreach ($mallBdPackage->packageProduct as $packageProduct) {
                $productModel->setId($packageProduct->product->id);
                if (!$productModel->decreaseQuantity($packageProduct->quantity * $packageQuantity)) {
                    DB::rollBack();
                    $this->serviceResponse->responseStat->status = false;
                    $this->serviceResponse->responseStat->msg = 'Sorry this product : ' . $packageProduct->product->title . ' Out of stock now ' . $packageProduct->quantity;
                    return $this->response();
                }
                try {
                    $productPrice = $packageProduct->product->prices[0]->retailPrice;
                } catch (Exception $ex) {
                    DB::rollBack();
                    $this->serviceResponse->responseStat->status = false;
                    $this->serviceResponse->responseStat->msg = 'Price not found for product ' . $packageProduct->product->title . ' ';
                    return $this->response();
                }


                $orderProductsModelObj = new OrderProductsModel();

//                $discountAmount = $mallBdPackage->originalPriceTotal - $mallBdPackage->packagePriceTotal;
                $discountAmount = 0;
                $orderProductsModelObj->setOrderId($orderModel->id);
                $orderProductsModelObj->setProductId($packageProduct->product->id);
                $orderProductsModelObj->setPackageId($packageId);
                $orderProductsModelObj->setPrice($productPrice);
                $orderProductsModelObj->setProductQuantity($packageProduct->quantity);
                $orderProductsModelObj->setPackageQuantity($packageQuantity);
                $orderProductsModelObj->setTotal($productPrice * $packageProduct->quantity * $packageQuantity);
                $orderProductsModelObj->setDiscount($discountAmount);

                $orderProductsModelObj->setTax(0.0);



                $this->setError($orderProductsModelObj->errorManager->errorObj);

                if ($this->hasError()) {
                    DB::rollBack();
                    $this->serviceResponse->responseStat->status = false;
                    $this->serviceResponse->responseStat->msg = "Internal error on fetching package from mallBdPackageCell";
                    return $this->response();
                }

                if (!$orderProductsModelObj->save()) {
                    DB::rollBack();
                    $this->serviceResponse->responseStat->status = false;
                    $this->serviceResponse->responseStat->msg = "Internal error on saving package from mallBdPackageCell";
                    return $this->response();
                }
            }
//            $totalDiscountAmount = $totalDiscountAmount + $discountAmount;
        }
        /* === Order Product entry [Ends] */

        /* Discount Entry [Started] */
        $orderModel->setDiscountTotal($totalDiscountAmount);

        if (count($voucherDetails) > 0) {
            $responseData = $this->manupulateVoucherDiscount($voucherDetails);
            if ($responseData['status'] == FALSE) {
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseStat->msg = "Voucher " . $responseData['name'] . " is no more available.";
                return $this->response();
            }
            $orderModel->setVoucherDiscountDetails(serialize($voucherDetails));
        }

        $orderModel->setVoucherDiscountTotal($shopping_cart['voucherDiscount']);

        $this->setError($orderModel->errorManager->errorObj);
        if ($this->hasError()) {
            DB::rollBack();
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "Internal error on orderModel on discount set";
            return $this->response();
        }
        if (!$orderModel->save()) {
            DB::rollBack();
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "Internal error on orderModel on discount update";
            return $this->response();
        }
        /* Discount Entry [Ends] */
        foreach ($shopping_cart['productCell'] as $cart) {
            $attributeList = [];
            $productId = 0;
            $combinationKey = 0;
            if ($cart['selectedAttributes'] != null) {
                foreach ($cart['selectedAttributes'] as $attribute) {
                    $productId = $cart['id'];
                    array_push($attributeList, $attribute['id']);
                }
                $strOfArr = implode(",", $attributeList);
                $strOfArr = trim($strOfArr);
                $result = ProductQuantityModel::where('product_id', '=', $productId)
                                ->where('combination', '=', $strOfArr)
                                ->get()->first();
                $combinationKey = $result['combination_key'];
            }

            $productId = $cart['id'];
            $quantity = $cart['quantity'];

            $orderAttrModel = new OrderAttributeModel();

            $orderAttrModel->setOrderId($orderId);
            $orderAttrModel->setProductId($productId);
            $orderAttrModel->setCombinationKey($combinationKey);
            $orderAttrModel->setQuantity($quantity);

            $this->setError($orderAttrModel->errorManager->errorObj);

            if ($this->hasError()) {

                DB::rollBack();
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseStat->msg = "Internal error on fetching product from productCell. No product combination key found";
                return $this->response();
            }
            if (!$orderAttrModel->save()) {
                DB::rollBack();
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseStat->msg = "Internal error on orderAttrModel";
                return $this->response();
            }
        }
        /* ===Ends==== */


        // Inserting to customerRewardModel
        $customerRewardModel = new CustomerRewardModel();

        $customerRewardModel->setUserId($userModel->id);
        $customerRewardModel->setOrderId($orderModel->id);
        $customerRewardModel->setDescription("");
        $customerRewardModel->setPoints($totalPoint);

        $this->setError($customerRewardModel->errorManager->errorObj);

        if ($this->hasError()) {
            DB::rollBack();
            return $this->response();
        }
        if (!$customerRewardModel->save()) {
            DB::rollBack();

            $this->serviceResponse->responseStat->msg = "Internal error on  customerRewardModel";
            return $this->response();
        }

        $orderStatusModel = new OrderStatusModel();

        $orderStatusModel->setOrderId($orderModel->id);
        $orderStatusModel->setCreatedBy($userModel->id);
        $orderStatusModel->setStatusId(1);
        $this->setError($orderStatusModel->errorManager->errorObj);

        if ($this->hasError()) {
            DB::rollBack();
            return $this->response();
        }

        if (!$orderStatusModel->save()) {
            DB::rollBack();

            $this->serviceResponse->responseStat->msg = "Internal error on  orderStatusModel";
            return $this->response();
        }

//        if ($orderModel->delivery_methods_id == 1 && $zoneDeliveryBoyId > 0) {
//
//            $zonalOrderModel->setOrderId($orderModel->id);
//
//            if (!$zonalOrderModel->save()) {
//                DB::rollBack();
//
//                $this->serviceResponse->responseStat->msg = "Internal error on  zonalOrderModel";
//                return $this->response();
//            }
//        }




        DB::commit();




        $notificationModel = new NotificationModel();

        $notificationModel->setNotificationType("order");
        $notificationModel->setNotificationId($orderModel->id);

        $text = "New Order Placed for Total " . $shopping_cart['totalPrice'];

        $notificationModel->setNotificationText($text);

        $notificationModel->save();


        ////inserting voucher data into order_vouchers table
        foreach ($voucherDetails as $voucher):
            $orderVoucherModel = new OrderVoucherModel();
            $orderVoucherModel->voucher_id = $voucher['voucher_id'];
            $orderVoucherModel->order_id = $orderModel->id;
            $orderVoucherModel->save();
        endforeach;
        ////inserting voucher data into order_vouchers table ends


        /* === Call Order entry cron job to backoffice== */

        $this->callBackOfficeCron($orderModel->id);
        $grand_total = ($orderModel->order_total + $orderModel->shipping_cost) - ($orderModel->discount_total + $orderModel->voucher_discount);
//        $bkash_charge = ceil($grand_total * 0.02);
        ////sms send for order store to the customer
        $customerDetails = UserModel::find($orderModel->customer_id);
        if (sizeof($customerDetails) > 0) {
            // Get cURL resource
            $curl = curl_init();
            // Set some options - we are passing in a useragent too here
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => url('/api/sendsms'),
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => array(
                    'mobile_number' => $customerDetails->phone,
                    'message' => 'Dear Mr. ' . $customerDetails->firstname . ' ' . $customerDetails->lastname . ', your order have been placed. Thank you for being with us.',
                )
            ));
            // Send the request & save response to $resp
            $resp = curl_exec($curl);
            // Close request to clear up some resources
            curl_close($curl);

            //curl process ends
            ////sms sending ends
            ////mail sending starts
            Mail::send('emails.orderplaced', ['name' => $customerDetails->firstname . ' ' . $customerDetails->lastname], function($message) use ($customerDetails) {
                $message->to($customerDetails->email, $customerDetails->firstname . ' ' . $customerDetails->lastname)->subject('Order placed successfully');
            });
            ////mail sending ends
        }
        $bkash_charge = 0.00;
        $total_bkash_pay = $grand_total;
        $currebcymodel = new CurrencyModel();
        $paypal_item_amount = $currebcymodel->BDTtoUSD($grand_total);
        $respone_data_checkout = array(
            'order_id' => $orderModel->id,
            'unique_code' => $orderModel->unique_code,
            'invoice_no' => $orderModel->invoice_no,
            'order_grand_total' => $grand_total,
            'bkash_charge' => $bkash_charge,
            'total_bkash_pay' => $total_bkash_pay,
            'paypal_pay_amount' => $paypal_item_amount,
        );

        $this->serviceResponse->responseStat->msg = "Order successfully done";

        $this->serviceResponse->responseData = $respone_data_checkout;
        return $this->response();
    }

    private function callBackOfficeCron($orderId) {
        $ch = curl_init();
        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json',
        );

        curl_setopt($ch, CURLOPT_URL, $this->backOfficeSeriveUrl . 'updateProductAndPackage/' . $orderId);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Timeout in seconds
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        $response = curl_exec($ch);
    }

    function getInformationByEmail(Request $request) {

        $userModel = new UserModel();
        $userModel->setEmail($request->input("email"), false);

        $shippingAddressModel = new ShippingAddressModel();

        $this->setError($userModel->errorManager->errorObj);

        if (!$this->hasError()) {
            $user = $userModel->getByEmail();
            $shippingAddressModel->setUserId($user->id);


            $responseObj = (object) array(
                        'userInformation' => $userModel->getByEmail(),
                        'shippingInformation' => $shippingAddressModel->getAllByUserId()
            );

            $this->serviceResponse->responseData = $responseObj;
        }
        return $this->response();
    }

    function productQuantityCheck(Request $request) {
        $product_id = $request->input("product_id");
        $quantity = $request->input("quantity");
        $productModel = new ProductModel();
        $productModel->setId($product_id);
        $product = $productModel->getById();
        if (!$productModel->productQuantityCheck((int) $quantity)) {
            $qan = $quantity - 1;
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = 'Sorry this product : "' . $product->title . '"     ' . $qan . ' in stock ';
        } else {
            $this->serviceResponse->responseStat->status = true;
            $this->serviceResponse->responseStat->msg = 'this product : ' . $product->title . ' In stock now';
        }
        return $this->response();
    }

    function packageQuantityCheck(Request $request) {
        $packageId = $request->input("package_id");
        $quantity = $request->input("quantity");

        $packageModel = new PackageModel();
        $packageModel->setId($packageId);
        $mallBdPackage = $packageModel->getPackageById();
        $productModel = new ProductModel();

        foreach ($mallBdPackage->packageProduct as $packageProduct) {
            $productModel->setId($packageProduct->product->id);
            $product = $productModel->getById();
            $quant = $quantity * $packageProduct->quantity;
            if (!$productModel->productQuantityCheck((int) $quant)) {
                $qan = $quantity - 1;
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseStat->msg = 'Sorry this package : "' . $mallBdPackage->packageTitle . '" ' . $qan . ' in stock ';
                return $this->response();
            }
        }

        $this->serviceResponse->responseStat->status = true;
        $this->serviceResponse->responseStat->msg = 'this product : ' . $product->title . ' In stock now';

        return $this->response();
    }

    private function generateInvoceNumber($odreId) {
        return "invoice 000000" . $odreId;
    }

    private function orderUniuqeNumber($length) {
        $key = '';
        $keys = array_merge(range(0, 9), range('A', 'Z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }

    private function manupulateVoucherDiscount($voucherList) {
        $responseData = array(
            'status' => TRUE,
            'name' => '',
        );
        foreach ($voucherList as $voucher):
            $voucherData = VoucherModel::where('voucher_code', $voucher['voucher_code'])->get();
            if ($voucherData[0]['number_of_time_use'] < 1) {
                $responseData['status'] = FALSE;
                $responseData['name'] = $voucher['voucher_code'];
                DB::rollBack();
                return $responseData;
            } else {
                $updateArray = array(
                    'number_of_time_use' => $voucherData[0]['number_of_time_use'] - 1,
                );
                VoucherModel::where('voucher_code', $voucher['voucher_code'])->update($updateArray);
            }
        endforeach;

        return $responseData;
    }

}
