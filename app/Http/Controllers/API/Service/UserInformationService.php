<?php
/**
 * Created by PhpStorm.
 * User: mi
 * Date: 3/22/16
 * Time: 4:05 PM
 */

namespace App\Http\Controllers\API\Service;


use App\Http\Controllers\BaseMallBDController;
use App\Model\DataModel\User;
use App\Model\ShippingAddressModel;
use App\Model\UserModel;
use Illuminate\Http\Request;

class UserInformationService extends BaseMallBDController
{
    public function index(Request $request)
    {
        $user = new User();
        $shippingAddressList = [];
        $userModel = new  UserModel();
        if($this->serviceResponse->responseStat->isLogin){
            $userModel->setEmail($this->appCredential->email,false);
        }else{

            $userModel->setEmail($request->input('email'),false);
        }

        $this->setError($userModel->errorManager->errorObj);
        if(!$this->hasError()){
            $user = $userModel->getByEmail();
            if($user->id>0) {
                $shippingAddressModel = new ShippingAddressModel();
                $shippingAddressModel->setUserId($user->id);
                $shippingAddressList = $shippingAddressModel->getAllByUserId();
            }

            $this->serviceResponse->responseData->userDetails = ($user->id>0)?$user:new \stdClass();
            $this->serviceResponse->responseData->shippingAddress = $shippingAddressList;

        }

        $this->serviceResponse->responseStat->status = ($user->id>0);
        if(!$this->serviceResponse->responseStat->status){
            $this->serviceResponse->responseStat->msg = "No record found";
        }

        return $this->response();

    }
}