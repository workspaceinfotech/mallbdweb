<?php

/**
 * Created by PhpStorm.
 * Project: themallbd
 * File: BaseMallBDController.php
 * User: rajib
 * Date: 1/12/16
 * Time: 10:02 AM
 */

namespace App\Http\Controllers\API\Service;

use App\Http\Controllers\BaseMallBDController;
use App\Model\DataModel\AuthCredential;
use App\Model\DataModel\User;
use App\Model\LoginModel;
use App\Model\NotificationModel;
use App\Model\UserModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use App\Model\UserDetailsModel;
use App\Model\ChatStatusModel;
use App\Http\Requests;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Auth;

class SignUpService extends BaseMallBDController {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $user = UserModel::with("role")->get();

        $userList = [];
        foreach ($user as $u) {
            $userObj = new User();
            $userObj->castMe($u);
            array_push($userList, $userObj);
        }

        $this->serviceResponse->responseStat->status = true;
        $this->serviceResponse->responseStat->msg = "data received";
        $this->serviceResponse->responseData = $userList;
        return $this->response();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $email = $request->input("email");
        $phone = $request->input("phone");
        $guest_eamil = FALSE;
        $userModel = new UserModel();
        if ($userModel->isEmailExistByOtherRole($email)) {    //except 7 and 8
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "This Eamil address already Taken";
            return $this->response();
        }
        if ($userModel->isEmailExistByCustomerRole($email)) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "This Eamil address already Taken";
            return $this->response();
        }
        if ($userModel->isPhoneExistByCustomerRole($phone)) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "This Phone Number already Taken";
            return $this->response();
        }
        $validator = Validator::make($request->all(), [
                    'first_name' => 'required|max:55',
                    'last_name' => 'required|max:55',
                    'email' => 'required|email|max:255',
                    'password' => 'required|min:6',
                    'confirm_password' => 'required|min:6|same:password',
                    'phone' => 'required|digits:11'
        ]);

        if ($validator->fails()) {

            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = $validator->errors()->first();
            return $this->response();
        }

        if ($userModel->isEmailExistByForDeletedCustomer($email)) {
            $userModel = UserModel::find($userModel->fnObj->id);
        }
        if ($userModel->isEmailExistByForGuest($email)) {
            $guest_eamil = TRUE;
            $userModel = UserModel::find($userModel->fnObj->id);
        }

        $userModel->setFirstname($request->input("first_name"));
        $userModel->setLastname($request->input("last_name"));
        $userModel->setEmailRegistration($request->input("email"));
        $userModel->setRoleId("7");
        $userModel->setStatus("Active");
        $userModel->setPhone($request->input("phone"));

        //$userModel->image = $userModel->savePicture($request->input("image"));

        $this->setError($userModel->errorManager->errorObj);

        $userDetailsModel = new UserDetailsModel();
        $loginModel = new LoginModel();
        $loginModel->setEmail($request->input("email"));
        $loginModel->setPassword($request->input("password"));

        if (!$this->hasError()) {
            if ($userModel->save()) {
                //updating the coutomer code
                $strCode = str_pad($userModel->id, 9, '0', STR_PAD_LEFT);
                $userModel->setCode("customer-" . $strCode);

                $userModel->save();

                $loginModel->setUserId($userModel->id);
                $loginModel->setAccesstoken($loginModel->email . $loginModel->password . date('Y-m-d H:i:s'));


                $userDetailsModel->setUserId($userModel->id);


                if ($userDetailsModel->save() && $loginModel->save()) {
                    $notificationModel = new NotificationModel();

                    $notificationModel->setNotificationType("customer");
                    $notificationModel->setNotificationId($userModel->id);

                    $text = "New customer added named " . $request->input("first_name") . " " . $request->input("last_name");

                    $notificationModel->setNotificationText($text);
                    if ($notificationModel->save()) {
                        // Get cURL resource
                        $curl = curl_init();
                        // Set some options - we are passing in a useragent too here
                        curl_setopt_array($curl, array(
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_URL => url('/api/sendsms'),
                            CURLOPT_POST => 1,
                            CURLOPT_POSTFIELDS => array(
                                'mobile_number' => $request->input("phone"),
                                'message' => 'Dear Mr. ' . $request->input("first_name") . ' ' . $request->input("last_name") . ', your REsgistration is successfully completed. Thank you for being with us.',
                            )
                        ));
                        // Send the request & save response to $resp
                        $resp = curl_exec($curl);
                        // Close request to clear up some resources
                        curl_close($curl);

                        //curl process ends
                        ////sms sending ends
                        $this->serviceResponse->responseStat->status = true;
                        $this->serviceResponse->responseStat->msg = "Registration has been successfully completed.";
                        $this->serviceResponse->responseData = $userModel;

//                        $userData = array(
//                            'email' => $request->input("email"),
//                            'password' => $request->input("password")
//                        );
////                        if (Auth::attempt($userData)) {
//////                            return Redirect::to('dashboard');
////                            return $this->response();
////                        }
//                        Auth::login($userData);
////                        Auth::loginUsingId($userModel->id);

                        return $this->response();
                    } else {
                        $this->serviceResponse->responseStat->status = false;
                        $this->serviceResponse->responseStat->msg = "Error setting notification model";
                        return $this->response();
                    }
                } else {
                    $this->serviceResponse->responseStat->status = false;
                    $this->serviceResponse->responseStat->msg = "Error in saving User Details";
                    return $this->response();
                }
            } else {
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseStat->msg = "Error in saving User Data";
                return $this->response();
            }
        } else {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "Error in Input";
            return $this->response();
        }


        /* $loginModel = new LoginModel();

          $loginModel->email = $request->input("email");
          $loginModel->password = $request->input("password");
          $loginModel->access_token = $request->input("email").$request->input("password").date("YMDHis");

          if($userModel->save())
          {
          $loginModel->user_id = $userModel->id;
          $loginModel->save();
          $authCredential = new AuthCredential();
          $authCredential->castMe($loginModel);

          $this->serviceResponse->responseStat->status = true;
          $this->serviceResponse->responseStat->msg="Registration Successful";
          $this->serviceResponse->responseData = $authCredential;
          return $this->response();
          }
          $this->serviceResponse->responseStat->status = false;
          $this->serviceResponse->responseStat->msg="Registration fail";
          return $this->response(); */
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $user = UserModel::where("user_id", "=", $id)->get()->first();
        if ($user == null || $user->user_id <= 0) {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg = "no data received";
            return $this->response();
        }
        $userObj = new User();
        $userObj->castMe($user);
        $this->serviceResponse->responseStat->status = true;
        $this->serviceResponse->responseStat->msg = "data received";
        $this->serviceResponse->responseData = $userObj;
        return $this->response();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function createChatGuest(Request $request) {
//        echo '<pre>';
//        echo $request->input("firstname");
//        echo $request->input("email");
//        echo $request->input("phone");
        $inputs = array(
            'firstname' => $request->input("firstname"),
            'email' => $request->input("email"),
            'phone' => $request->input("phone"),
            'role_id' => 8,
            'status' => 'Active',
        );

        $rules = array(
            'firstname' => "required",
            'email' => "required|email",
            'phone' => "required|min:11",
        );
        $validate = Validator::make($inputs, $rules);
        if ($validate->fails()) {
            $errors = $validate->errors();
            $errors = json_decode($errors);

            return response()->json(['success' => FALSE, 'message' => $errors,], 422);
        } else {
            $ruleForPhone = array(
                'phone' => "numeric",
            );
            $validatePhone = Validator::make($inputs, $ruleForPhone);
            if ($validatePhone->fails()) {
                $errors = $validatePhone->errors();
                $errors = json_decode($errors);

                return response()->json(['success' => FALSE, 'message' => $errors,], 422);
            }
            $existingUserData = UserModel::where('email', $request->input("email"))->get();
            if (count($existingUserData) > 0) { //user exists for given email
                $last_id = $existingUserData[0]['id'];
//                
                $chatStatusDataForUser = ChatStatusModel::where('customer_id', $last_id)->get();
                if (count($chatStatusDataForUser) > 0) {
                    $errors = array(
                        'temp_name' => 'This email address is already in use with customer care executive.'
                    );

                    return response()->json(['success' => FALSE, 'message' => $errors], 422);
                }
            } else {
                $last_id = UserModel::insertGetId($inputs);
            }
            Session::put('chatUserId', $last_id);
            return response()->json(['success' => TRUE, 'user_id' => $last_id], 200);
        }
    }

}
