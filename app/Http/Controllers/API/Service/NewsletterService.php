<?php
/**
 * Project  : mallbdweb
 * File     : NewsletterService.php
 * Author   : Abu Bakar Siddique
 * Email    : absiddique.live@gmail.com
 * Date     : 3/24/16 - 12:43 PM
 */
namespace App\Http\Controllers\API\Service;

use App\Http\Controllers\BaseMallBDController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Model\NewsletterModel;

class NewsletterService extends BaseMallBDController {

    function userSubscribe(Request $request){

        $email = $request->input("email");

        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255|unique:newsletters'

        ]);
        if ($validator->fails())
        {
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg=$validator->errors()->first();
            return $this->response();
        }

        $newsletterModel = new NewsletterModel();
        $newsletterModel->setEmail($email);

        $this->setError($newsletterModel->errorManager->errorObj);

        if($newsletterModel->save()){
            $this->serviceResponse->responseStat->status = true;
            $this->serviceResponse->responseStat->msg="Subscription Successful";
            $this->serviceResponse->responseData = $newsletterModel;
            return $this->response();

        }else{
            $this->serviceResponse->responseStat->status = FALSE;
            $this->serviceResponse->responseStat->msg="Subscription unsuccessful";
            return $this->response();
        }

    }

}