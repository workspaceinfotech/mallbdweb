<?php
/**
 * Created by PhpStorm.
 * Project: MallBDFront
 * File: CustomerWishListService.php
 * User: rajib
 * Date: 2/4/16
 * Time: 4:57 PM
 */

namespace App\Http\Controllers\API\Service;

use App\Http\Controllers\BaseMallBDController;
use App\Model\CustomerWishListModel;
use Illuminate\Http\Request;

class WishListService extends BaseMallBDController{


    public function getWishListedProducts(Request $request) {
        $customerId = $this->appCredential->user->id;

        if (($customerId != 0) || ($customerId != "")) {

            $customerWishListModelObj = new CustomerWishListModel();
            $products                 = $customerWishListModelObj->getWishListedProductsById($customerId);
            $productList              = [];
            foreach ($products as $product) {
                foreach ($product->products as $p) {
                    array_push($productList, $p);
                }
            }
            $this->pageData['products'] = $productList;

            if(empty($productList))
            {
                $this->serviceResponse->responseStat->status=false;
                $this->serviceResponse->responseStat->msg="No Data Received(empty)";
                return $this->response();
            }
            else{
                $this->serviceResponse->responseStat->status = true;
                $this->serviceResponse->responseStat->msg = "Data Found";
                $this->serviceResponse->responseData = $this->pageData['products'];
                return $this->response();
            }

        }else{

            $this->serviceResponse->responseStat->status=false;
            $this->serviceResponse->responseStat->msg="Login Failed";
            return $this->response();
        }
    }

    public function addWishListedProduct(Request $request)
    {

        $this->serviceResponse->responseStat->msg ="Child";
        $customerWishList = new CustomerWishListModel();
        $customerWishList->setCustomerId($this->appCredential->user->id);
        $customerWishList->setProductId($request->input("product_id"));
        $this->setError($customerWishList->errorManager->errorObj);

        $customerID = $this->appCredential->user->id;

        if(!$this->hasError())
        {
            if(($customerWishList->isDistinct($request->input("product_id"),$customerID)) && $this->serviceResponse->responseStat->isLogin )
            {
                if($customerWishList->save()){
                    $this->serviceResponse->responseStat->status = true;
                    $this->serviceResponse->responseStat->msg="Insert Successful";
                    $this->serviceResponse->responseData = $customerWishList;
                    return $this->response();
                }
                else{
                    $this->serviceResponse->responseStat->status = false;
                    $this->serviceResponse->responseStat->msg="Error in saving CustomerWishListModel";
                    return $this->response();
                }
            }
            else{
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseStat->msg="duplicate product not allowed";
                return $this->response();
            }

        }
        else{
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg="Error in input";
            return $this->response();
        }
    }

    public function removeFromWishList(Request $request){

        $productId = $request->input("product_id");

        //$customerWishListModel = new CustomerWishListModel();

        $removeFromWishList = CustomerWishListModel::where("product_id","=",$productId)
            ->where("customer_id","=",$this->appCredential->user->id);


        if($removeFromWishList->delete())
        {
            $this->serviceResponse->responseStat->status = true;
            $this->serviceResponse->responseStat->msg="Successfully Deleted";
            return $this->response();
        }else{
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg="Delete Unsuccessful";
            return $this->response();
        }
    }

    public function removeFromWishListFromArray(Request $request){

        $productId = $request->input("product_list");

        $productList = json_decode($productId);
        //return $productList;

        foreach($productList as $product)
        {
            $removeFromWishList = CustomerWishListModel::where("product_id","=",$product)
            ->where("customer_id","=",$this->appCredential->user->id);
            if($removeFromWishList->delete())
            {
                $this->serviceResponse->responseStat->status = true;
                $this->serviceResponse->responseStat->msg="Successfully Deleted";
                //return $this->response();
            }else{
                $this->serviceResponse->responseStat->status = false;
                $this->serviceResponse->responseStat->msg="Delete Unsuccessful";
                return $this->response();
            }

        }
        return $this->response();
        //$customerWishListModel = new CustomerWishListModel();

        /*$bear = CustomerWishListModel::where("product_id","=",$productId);


        if($bear->delete())
        {
            $this->serviceResponse->responseStat->status = true;
            $this->serviceResponse->responseStat->msg="Successfully Deleted";
            return $this->response();
        }else{
            $this->serviceResponse->responseStat->status = false;
            $this->serviceResponse->responseStat->msg="Delete Unsuccessful";
            return $this->response();
        }*/
    }

    public function getWishListedProductArray()
    {
        $customerWishListModel = new CustomerWishListModel();

        $wishListedProductArray = $customerWishListModel->getWishListProductArray($this->appCredential->user->id);

        if(empty($wishListedProductArray) || $wishListedProductArray=="" || (count($wishListedProductArray)<1)){

            $this->serviceResponse->responseStat->status = FALSE;
            $this->serviceResponse->responseStat->msg="No Product Found";
            $this->serviceResponse->responseData = $wishListedProductArray;
            return $this->response();
        }

        $this->serviceResponse->responseStat->status = TRUE;
        $this->serviceResponse->responseStat->msg="Product Found";
        $this->serviceResponse->responseData = $wishListedProductArray;
        return $this->response();
    }

} 