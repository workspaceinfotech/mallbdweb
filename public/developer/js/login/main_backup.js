//var socket = io.connect('http://192.168.1.39:3000');  //riyad vai ip
//var socket = io.connect('http://192.168.1.8:3000');  //saiful ip
//var socket = io.connect('http://188.166.214.41:3000');  //server ip
var socket = io.connect('http://188.166.214.41:8085');  //server ip
function doLogin(callBackFn) {

    var email = $('#username').val();
    var password = $('#password').val();
    $('#msg').show();
    $('#msg').html("");
    if (email == "" || password == "")
    {
        $('#msg').html("Email or Password field is empty");
        return false;
    }
    $('#msg').html("Processing....");
    $('#username').prop('disabled', true);
    $('#password').prop('disabled', true);
    $.ajax({
        url: $("#baseUrl").val() + "login",
        method: "POST",
        data: {
            "email": email,
            "password": password
        },
        success: function (data) {
            

            if (callBackFn != undefined) {
                callBackFn(data);
            }
            $('#msg').html(data.responseStat.msg);
            if (data.responseStat.status && data.responseStat.isLogin) {
                $('#msg').delay(1000).fadeOut(500, function () {
                    $('#login_popup').hide();
                    $('#userName').val(data.responseData.user.firstName);
                    $('#loginText').hide();
                    $('#loginName').html(data.responseData.user.firstName).show();
                    $('#logoutText').show();
                    afterLoginFunctionCall();
                    $('#open_chat_anchor').attr('onclick', 'openChat(' + data['responseData']['user']['id'] + ')');
                    //            $('#open_chat_anchor').prop("onclick", false);
                    //            document.getElementById('open_chat_anchor').onclick = function () { openChat(data['responseData']['id']) };
                    //            console.log($('#open_chat_anchor').html());
                                socket.emit('init', data['responseData']['user']['id']);

                    //location.reload();
                });
            }
            else {
                $('#username').prop('disabled', false);
                $('#password').prop('disabled', false);
            }
        }
    });
    return false;
}
function showLoginForm(msg) {
    $("#loginFormViewer").trigger("click");
}
$(document).ready(function () {

    try {
        if (typeof url('?login') != 'undefined' && url('?login') != null && url('?login') != "") {
            var tempLoginVal = url('?login');
            if (!JSON.parse(tempLoginVal)) {
                setTimeout(function () {
                    $("#loginFormViewer").trigger("click");
                }, 3000);

            }
        }
    } catch (ex) {
        console.log(ex);
    }

});
function doLogout() {
    $.ajax({
        url: $("#baseUrl").val() + "logout",
        method: "GET",
        success: function (data) {
            $('#loginName').hide();
            window.location.href = $("#baseUrl").val() + "home";
        }

    });
}
function afterLoginFunctionCall() {

    var index = location.pathname.split('/').indexOf('_checkout');
    try {
        if (index >= 0) {
            if (angular != undefined) {
                angular.element(document.getElementsByTagName('body')).scope().getUserInformationAndShippingAddressByEmail("element_email");
            }
        } else {
            addWishListClass();
        }

    } catch (ex) {
        console.log(ex);
    }

}
function doLoginFromCheckout_OLD() {
    $('#username').val($('#checkoutLogInEmail').val());
    $('#password').val($('#checkoutLogInPassword').val());
    $('#checkoutLoginMsg').html('Processing...');
    doLogin(function (data) {
        console.log('data from checkout');
        console.log(data);
        if (data.responseStat.status && data.responseStat.isLogin) {
            $('#checkoutLoginForm').fadeOut(500);
            $('#checkoutLoginMsg').html('');
           // location.reload();
        } else {
            $('#checkoutLoginMsg').html(data.responseStat.msg);
        }
    });
}