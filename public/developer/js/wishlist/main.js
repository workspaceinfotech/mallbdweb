/**
 * Created by mi on 3/8/16.
 */
function increaseDecreaseWishLisCount(count){
    var wishListCount = parseInt($('#wishListCount').html());
    wishListCount = wishListCount + count;
    $('#wishListCount').html(wishListCount);
}

function submitWishList(productId,quickViewId){
    $('#load-img'+productId).show();
    $('#load-img2'+productId).show();
    $.ajax({
        url: $("#baseUrl").val()+"api/customer/wishlist/add",
        method: "POST",
        data: {
            "product_id" : productId

        },
        success: function (data) {
            if(!data.responseStat.isLogin)
            {
                $('#load-img'+productId).hide();
                $('#load-img2'+productId).hide();
                try{
                    if(typeof  quickViewId!="undefined"){
                        $('#'+quickViewId).fadeOut(500,function(){
                            showLoginForm("");
                        });
                    }else{
                        showLoginForm("");
                    }
                }catch(ex){
                    console.log(ex);
                }


                console.log(data);
            }
            else{
                $('.favourite_'+productId).removeClass('fa-heart-o');
                $('.favourite_'+productId).addClass('fa-heart');
                $('#load-img'+productId).hide();
                $('#load-img2'+productId).hide();
                $('#inner'+productId).addClass("active");
                $('#outer'+productId).addClass("active");

                console.log(data);
            }

            if(data.responseStat.status){
                try{
                    increaseDecreaseWishLisCount(1);
                    $('#addnotify'+productId).show();
                    $('#addnotify'+productId).fadeOut(2000);
                    $('#outeraddnotify'+productId).show();
                    $('#outeraddnotify'+productId).fadeOut(2000);
                }catch(ex){
                    console.log(ex);
                }

            }else{
                if(!data.responseStat.isLogin){
                    return;
                }

                $('#loginnotify'+productId).show();
                $('#loginnotify'+productId).fadeOut(2000);
                $('#addnotify2'+productId).show();
                $('#addnotify2'+productId).fadeOut(2000);
                $('#outeraddnotify2'+productId).show();
                $('#outeraddnotify2'+productId).fadeOut(2000);
            }
        }

    });

}

function doRemove(prouctId, divId)
{
    $.ajax({

        url: $("#baseUrl").val() + "api/wishlist/remove",
        method: "POST",
        data: {
            "product_id": prouctId
        },
        success: function (data) {
            if (data.responseStat.status) {
                increaseDecreaseWishLisCount(-1);
                $("#"+divId).fadeOut("slow");
                console.log(data);

            }
            else {
                $("#errmsg").html(data.responseStat.msg);
                console.log(data);
            }

        }

    });

    $("#"+divId).fadeOut("slow");
}