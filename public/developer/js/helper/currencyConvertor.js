
var currencyCode = {'BDT':'&#2547','GBP':'&#163;'}


function changeCurrencyToGBP(to,from,rate){
    if(rate==0){
        return;
    }
    var currencyRate = rate;
    var currentCurrencyCode = currencyCode[to];
    $('.currencySymb').html(currentCurrencyCode);
    $('.currencyVal').each(function(){
        var currency = $(this).html();
        var number = Number(currency.replace(/[^0-9\.]+/g,""));
        number =number*currencyRate;
        $(this).html(formatMoneyValue(number));
    });
}
function formatMoneyValue(number){
    return parseFloat(number).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')
}
function initiateCurrencyChange(){
    var to = "GBP";
    var from = "BDT";
    $.ajax({
        url: $("#baseUrl").val()+"api/currency/conversion",
        method: "POST",
        data: {
            "to": to,
            "from": from
        },
        success: function (data) {
            if(data.responseStat){
                changeCurrencyToGBP(to,from,data.responseData);
            }
        }
    });
}