/**
 * Created by mi on 3/25/16.
 */
function doSearch(event){
        var char = event.which || event.keyCode;
        console.log(char);
        var searchProduct = $("#searchProduct").val().trim();

        if(char==13 && searchProduct!=null && searchProduct!= ""){
            window.location = $("#baseUrl").val()+'search/product?product_query='+searchProduct;
        }
}

var options = {
        url:function(keyWord){
                return $("#baseUrl").val() +"product/category/searchbytitle/web?keyword="+keyWord;
        },
        categories: [
                {
                        listLocation: "product",
                        maxNumberOfElements: 5,
                        header: "<b>Products</b>",
                },
                {
                        listLocation: "package",
                        maxNumberOfElements: 3,
                        header: "<b>Packages</b>"
                },
                {
                        listLocation: "category",
                        maxNumberOfElements: 4,
                        header: "<b>Categories</b>"
                },
                {
                        listLocation: "manufacturer",
                        maxNumberOfElements: 2,
                        header: "<b>Brand</b>"
                },
                
        ],
        getValue: function (element) {
                return element.title;
        },
        template: {
                type: "description",
                fields: {
                        description: "product_title"
                }
        },
        list: {
                maxNumberOfElements: 14,
                match: {
                        enabled: true
                },
                sort: {
                        enabled: true
                },
                onClickEvent: function() {
                        var categoryObj = $("#searchProduct").getSelectedItemData();
                        if(categoryObj.product != undefined && categoryObj.product){
                            window.location =$("#baseUrl").val()+"product/"+categoryObj.url+"/"+categoryObj.code;
                        }else if(categoryObj.package != undefined && categoryObj.package){
                            window.location =$("#baseUrl").val()+"packages/"+categoryObj.encoded_title+"/"+categoryObj.id;
                        }else if(categoryObj.manufacturer != undefined && categoryObj.manufacturer){
                            var searchParamObj = {"pa":[]};
                            var manufacturersIdList = [];
                            manufacturersIdList.push(categoryObj.id);


                            if( manufacturersIdList.length > 0){
                                searchParamObj['manufacturers'] =manufacturersIdList;
                            }


                            var searchParams =(
                                                searchParamObj.pa.length > 0
                                                || manufacturersIdList.length > 0
                                              )?"?q="+encodeURIComponent(JSON.stringify(searchParamObj)):"";
                            var viewSize = 10;
                            searchParams+=(searchParams!="")?"&limit="+viewSize+"&page=0":"";
                            window.location =$("#baseUrl").val()+"manufacturer"+searchParams;
                        }else if(categoryObj.category != undefined && categoryObj.category){
                            window.location =$("#baseUrl").val()+"category/"+categoryObj.encoded_title+"/"+categoryObj.id;
                        }
                        
//                        if(categoryObj.package != undefined && categoryObj.package)
//                                window.location =$("#baseUrl").val()+"packages/"+categoryObj.id;
//                        else
//                                window.location =$("#baseUrl").val()+"product/"+categoryObj.product_id;
                },
                onKeyEnterEvent:function(){
                        var categoryObj = $("#searchProduct").getSelectedItemData();
                        if(categoryObj.product != undefined && categoryObj.product){
                            window.location =$("#baseUrl").val()+"product/"+categoryObj.product_id;
                        }else if(categoryObj.package != undefined && categoryObj.package){
                            window.location =$("#baseUrl").val()+"packages/"+categoryObj.id;
                        }else if(categoryObj.manufacturer != undefined && categoryObj.manufacturer){
                            var searchParamObj = {"pa":[]};
                            var manufacturersIdList = [];
                            manufacturersIdList.push(categoryObj.id);


                            if( manufacturersIdList.length > 0){
                                searchParamObj['manufacturers'] =manufacturersIdList;
                            }


                            var searchParams =(
                                                searchParamObj.pa.length > 0
                                                || manufacturersIdList.length > 0
                                              )?"?q="+encodeURIComponent(JSON.stringify(searchParamObj)):"";
                            var viewSize = 10;
                            searchParams+=(searchParams!="")?"&limit="+viewSize+"&page=0":"";
                            window.location =$("#baseUrl").val()+"manufacturer"+searchParams;
                        }else if(categoryObj.category != undefined && categoryObj.category){
                            window.location =$("#baseUrl").val()+"category/"+categoryObj.id;
                        }
//                        if(categoryObj.package != undefined && categoryObj.package)
//                                window.location =$("#baseUrl").val()+"packages/"+categoryObj.id;
//                        else
//                                window.location =$("#baseUrl").val()+"product/"+categoryObj.product_id;
                }
        },
        theme: "square"
};

$("#searchProduct").easyAutocomplete(options);
