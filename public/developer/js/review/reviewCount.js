
function numberOfReview(productId)
{

    var reviewSpan = $('#reviewCount'+productId).html();
    if(reviewSpan.length>1)
    {
        return;
    }

    $.ajax({

        url: $("#baseUrl").val() + "api/review/count",
        method: "POST",
        data: {
            "product_id": productId
        },
        success: function (data) {

            $('#reviewCount'+productId).html(data.responseData);
            console.log(data.responseData);

        }

    });
}
