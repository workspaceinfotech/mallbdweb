function UserDetails() {

    this.id = 0;
    this.address = new Address();
    this.shippingAddress = new ShippingAddress();
}
function Suppliers() {
    this.id = 0;
    this.code = "";
    this.name = "";
    this.phone = "";
    this.email = "";
    this.contactPerson = "";
    this.address = "";
    this.country = "";
    this.createdOn = "";
}
function User() {
    this.id = 0;
    this.firstName = "";
    this.lastName = "";
    this.createdOn = "";
    this.email = "";
    this.role = "";
    this.status = "";
    this.phone = "";
    this.image = "";
    this.userDetails = new UserDetails();
}
function WareHouse() {

    this.id = 0;
    this.code = "";
    this.name = "";
    this.zone = "";
    this.address = "";
    this.country = "";
    this.city = "";
    this.zipCode = "";
    this.manager = "";
    this.createdOn = "";
    this.createdBy = new User();
    this.updatedOn = "";
    this.updatedBy = "";
}
function Shop() {

    this.id = 0;
    this.name = "";
    this.description = "";
    this.createdBy = User();
    this.createdOn = "";
}
function Manufacturer()
{
    this.id = 0;
    this.name = "";
    this.icon = "";
    this.createdOn = "";
}
function Prices() {
    this.id = 0;
    this.tax = new Taxes();
    this.purchasePrice = 0.0;
    this.wholesalePrice = 0.0;
    this.retailPrice = 0.0;
    this.createdOn = "";

}
function Taxes() {
    this.id = 0;
    this.title = "";
    this.type = "";
    this.amount = 0.0;
    this.createdOn = "";
}
function Product()
{
    this.id = 0;
    this.code = "";
    this.title = "";
    this.description = "";
    this.url = "";
    this.barCode = "";
    this.point = 0;
    this.avgRating = 0;
    this.width = "";
    this.height = "";
    this.depth = "";
    this.weight = "";
    this.quantity = 0;
    this.status = "";
    this.metaTitle = "";
    this.metaDescription = "";
    this.expireDate = "";
    this.manufacturedDate = "";
    this.createdOn = "";
    this.shop = new Shop();
    this.supplier = new Suppliers();
    this.warehouse = new WareHouse();
    this.manufacturer = new Manufacturer();
    this.prices = [];
    this.pictures = [];
    this.discounts = [];
    this.categories = [];
    this.tags = [];
    this.attributes = [];
    this.previousPrice = 0;
}
function ProductCell() {
    this.id = 0;
    this.quantity = 0;
    this.minimumOrderQuantity = 0;
    this.productQuantity = 0;
    this.product = new Product(); // Array of Product class
    this.selectedAttributes = [];
}
function MallBdPackageCell() {
    this.id = 0;
    this.quantity = 0;
    this.mallBdPackage = new MallBdPackage(); // Array of Product class
}
function SelectedAttribute()
{
    this.id = 0;
    this.name = "";
    this.value = "";
}
function DisplayCell() {
    this.cartCell = [];
    this.type = '';
}
function ShoppingCart() {
    this.productCell = []; // Array of ProductCell
    this.mallBdPackageCell = [];
    this.orderTotal = 0;
    this.productDiscountTotal = 0.00;
    this.shippingCost = 0;
    this.totalDiscount = 0;
    this.totalPrice = 0;
    this.totalTax = 0;
    this.voucherDiscount = 0.00;
}
function MallBdPackageProduct()
{
    this.id = 0;
    this.packageId = 0;
    this.product = new Product();
    this.quantity = 0;
    this.price = 0.0;
    this.createdOn = "";

}
function MallBdPackage() {
    this.id = 0;
    this.packageTitle = "";
    this.description = "";
    this.startDate = "";
    this.endDate = "";
    this.image = "";
    this.package_quantity = "";
    this.originalPriceTotal = "";
    this.packagePriceTotal = "";
    this.status = "";
    this.createdOn = "";
    this.mallBdPackageProduct = [];
}
function MallBdPackageCell()
{
    this.id = 0;
    this.quantity = 0;
    this.mallBdPackage = [];
}
function PaymentMethods()
{
    this.id = 0;
    this.methodTitle = "";
    this.description = "";
    this.createdOn = "";
    this.createdBy = "";
    this.updatedOn = "";
    this.updatedBy = "";
}

function DeliveryMethod()
{
    this.id = 0;
    this.title = 0;
    this.icon = "";
    this.deliveryPrice = 0;
    this.delivery_price_limit = 0;
    this.createdOn = "";
}

function Zone()
{
    this.id = 0;
    this.code = "";
    this.city = "";
    this.area = "";
    this.createdOn = "";
}

function ShippingAddress()
{
    this.userId = 0;
    this.shippingAddress = "";
    this.shippingCity = "";
    this.shippingCountry = "";
    this.shippingZipCode = "";
}
function Address()
{
    // for shipping address
    this.shipping_firstname = "";
    this.shipping_lastname = "";
    this.shipping_phone = "";
    this.shipping_address = "";
    this.shipping_city = "";
    // for shipping address
    this.address = "";
    this.city = "";
    this.country = "";
    this.zipCode = "";
}

function orders() {

}

function CustomerPurchaseDiscount()
{
    this.id = 0;
    this.discount_base = "";
    this.discount_base_limit = 0;
    this.discount_type = "";
    this.discount_amount = 0;
    this.createdOn = "";
}