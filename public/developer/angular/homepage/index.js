/**
 * Created by Sarwar on 6/22/2016.
 */

var app = angular.module("mallBd",['lazy-scroll']);
app.controller('testController', function($scope,$http) {

    var reachedLast = false;
    $scope.testData = [];
    $scope.loadmore = "Loading more data...";

    NextpageNumber = 0;//next page is made it zeero on starting
    $scope.listData = function() {
        if(reachedLast)
            return false;


        $scope.url = 'lazyscroll-serverdata.php?page=' + NextpageNumber;

        $scope.url = $("#baseUrl").val()+'api/products/all/show';

       // $scope.keywords="dsdssssds";
        console.log(NextpageNumber);
        var dataObj = {
            limit : 10,
            offset : NextpageNumber,
        };

        $http.post($scope.url,dataObj).success(function(data, status) {

            $scope.status = status;
            $scope.data = data;
            var currentpage = NextpageNumber;//taking current pagenumber
            var lastpage = data.lastpage; //setting last page number
            NextpageNumber = data.nextpage;

            if((currentpage == NextpageNumber) ){
                return false;
            }

            if((currentpage == lastpage)){
                //reached at the bottom
                reachedLast = true;
                $scope.loadmore = "Reached at bottom";
//                 $scope.$apply();

            }

            $scope.testData = $scope.testData.concat(data.result);
//             if ($scope.testData != undefined) {
//                 $scope.testData = $scope.testData.concat(data.result);
//             }else {
//                 $scope.testData =data.result;
//             }

        }).error(function(data, status) {
            $scope.data = data || "Request failed";
            $scope.status = status;
        });


    };
    $scope.listData();// loading initial content

});

