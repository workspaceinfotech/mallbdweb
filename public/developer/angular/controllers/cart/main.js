/**
 * Created by mi on 1/25/16.
 */
var mallBdApp = angular.module('mallBd', []);
var NextpageNumber = 0;//next page is made it zeero on starting

mallBdApp.controller('CartController', ['$scope', '$sce', '$http', '$filter', '$timeout', function($scope, $sce, $http, $filter, $timeout) {
        var cookieName = "cart";
        $scope._PRODUCT = 'product';
        $scope._PACKAGE = 'package';
        $scope.lastItemAddedInCart = $scope._PRODUCT;
        $scope.agreeTerms = false;
        $scope.checkoutFormParam = ['email', 'firstname', 'lastname', 'phone', 'address', 'city', 'zipcode', 'country', 'shipping_firstname', 'shipping_lastname', 'shipping_phone', 'shipping_address', 'shipping_country', 'shipping_zipcode', 'shipping_city', 'delivery_method_id', 'zone_id', 'payment_method_id'];
        $scope.success = {status: false, msg: ""};
        $scope.responseError = {status: false, msg: ""};
        $scope.processing = {done: true, msg: ""};
        $scope.htmlManipulateObj = initiateHtmlManipulateObj($scope.checkoutFormParam);

        $scope.shoppingCart = new ShoppingCart();



        $scope.currencySymbol = $sce.trustAsHtml("&#2547");
        $scope.displayedCell = [];
        $scope.shippingAddressList = [];
        $scope.paymentMethods = [];
        $scope.deliveryMethods = [];
        $scope.customerPurchaseDiscounts = [];
        $scope.zones = [];
        $scope.voucherarray = [];
        $scope.voucherusedarray = [];

        $scope.userInfo = new User();
        $scope.shippingAddress = new ShippingAddress();
        $scope.paymentMethod = new PaymentMethods();
        $scope.deliveryMethod = new DeliveryMethod();
        $scope.customerPurchaseDiscount = new CustomerPurchaseDiscount();
        $scope.userInfo.userDetails.address.city = "Dhaka";
        $scope.userInfo.userDetails.address.shipping_city = "Dhaka";
        $scope.voucherdiscount = 0.00;
        $scope.deliveryPriceforthisorder = 0;
        $scope.customerpurchasediscountforthisorder = 0;
        $scope.zone = new Zone();

        $scope.shippingAddressAction = {edit: false, add: false};

//    For empty object check
        $scope.isEmpty = function(obj) {
            for (var i in obj)
                if (obj.hasOwnProperty(i))
                    return false;
            return true;
        };

        $scope.initiateShoppingCartFromLocalStorage = function() {
            var tmpShoppingCart = readFromLocalStorage(cookieName);
            try {
                if (tmpShoppingCart != null && tmpShoppingCart != "") {
                    $scope.shoppingCart = JSON.parse(tmpShoppingCart);
                    if (typeof $scope.shoppingCart.productCell == 'undefined' || typeof $scope.shoppingCart.mallBdPackageCell == 'undefined') {
                        eraseLocalStorage(cookieName);
                        $scope.shoppingCart = new ShoppingCart();
                    }
                } else {
                    $scope.shoppingCart = new ShoppingCart();
                }
            } catch (ex) {
                console.log(ex);
            }
        };

        $scope.initiateShoppingCartFromLocalStorage();

        $scope.checkCartIsUpdated = function() {
            var tmpShoppingCart = readFromLocalStorage(cookieName);
            if (tmpShoppingCart != null && tmpShoppingCart != "") {
                tmpShoppingCart = JSON.parse(tmpShoppingCart);
            } else {
                return;
            }
            if (JSON.stringify($scope.shoppingCart) === JSON.stringify(tmpShoppingCart)) {
                console.log('Match Found');
            } else {
                $scope.shoppingCart = tmpShoppingCart;
                console.log('Not Matched');
            }
        };
        $scope.imageUrl = document.getElementById("imageUrl").value;
        $scope.baseUrl = document.getElementById("baseUrl").value;

        $scope.isCheckoutPage = function() {
            try {
                var val = (document.getElementById("isCheckoutPage") != null) ? document.getElementById("isCheckoutPage").value : "";
                if (val == "true")
                    return true;
                return false;
            } catch (ex) {
                console.log(ex);
                return false;
            }

        };
        $scope.isMyAccountPage = function() {
            try {
                var val = (document.getElementById("isMyAccountPage") != null) ? document.getElementById("isMyAccountPage").value : "";
                if (val == "true")
                    return true;
                return false;
            } catch (ex) {
                console.log(ex);
                return false;
            }

        };
        $scope.isLogin = function() {
            try {
                var val = (document.getElementById("isLogin") !== null) ? document.getElementById("isLogin").value : "";
                if (val == "true")
                    return true;
                return false;
            } catch (ex) {
                console.log(ex);
                return false;
            }

        };
        $scope.addToCart = function(id, quantity, fromQuickView) {
            $scope.initiateShoppingCartFromLocalStorage();
            var productJsonStr = document.getElementById(id).value;
            var product = JSON.parse(productJsonStr);

            if (product.prices.length == 0) {
                alert("Product does not have any price");
                return;
            }
            if (!fromQuickView) {
                if (product.attributes.length > 0) {

                    document.getElementById("quick_view_product_" + product.id + "_trigger_btn").click();
                    return;
                }
            }
            $scope.lastItemAddedInCart = $scope._PRODUCT;
            if (quantity == -1) {
                try {
                    if (fromQuickView) {
                        quantity = parseInt($("#quantity_modifier_" + product.id).val());
                        $("#quantity_modifier_" + product.id).val(quantity);
                    } else {
                        quantity = parseInt($("#outter_quantity_modifier_" + product.id).val());
                        $("#outter_quantity_modifier_" + product.id).val(quantity);
                    }
                } catch (ex) {
                    console.log(ex);
                    quantity = 1;
                }




            } else {
                quantity = 1;
            }

            var productCellIndex = getProductGroupCell($scope.shoppingCart.productCell, product);


            if (productCellIndex >= 0) {
                if(parseInt($scope.shoppingCart.productCell[productCellIndex].quantity+quantity)>parseInt($scope.shoppingCart.productCell[productCellIndex].productQuantity)){
                    $scope.shoppingCart.productCell[productCellIndex].quantity=$scope.shoppingCart.productCell[productCellIndex].productQuantity;
                }
                else{
                   $scope.shoppingCart.productCell[productCellIndex].quantity += quantity;// = product;  
                }              

                var element = $scope.shoppingCart.productCell[productCellIndex];
                $scope.shoppingCart.productCell.splice(productCellIndex, 1);
                $scope.shoppingCart.productCell.splice(0, 0, element);

                $scope.prepareDisplayedCell(element, $scope._PRODUCT);

                $timeout(function() {
                    $scope.$apply(function() {

                    });
                }, 500);
            } else {
                if(parseInt(quantity)<parseInt(product.minimumOrderQuantity)){
                    quantity=parseInt(product.minimumOrderQuantity);
                }
                if(parseInt(quantity)>parseInt(product.quantity)){
                    quantity=parseInt(product.quantity);
                }
                var productCell = new ProductCell();
                productCell.id = product.id;
                productCell.quantity = quantity;
                productCell.minimumOrderQuantity = product.minimumOrderQuantity;
                productCell.productQuantity = product.quantity;
                productCell.product = product;
                var selectedAttributes = getSelectedAttributeFromProduct(productCell.product);
                productCell.selectedAttributes = selectedAttributes;

                $scope.shoppingCart.productCell.unshift(productCell);

                $scope.prepareDisplayedCell(productCell, $scope._PRODUCT);
            }
            try {
                document.getElementById("quick_view_product_" + product.id).style.display = "none";
            } catch (ex) {
                console.log(ex);
            }
            if($('#device_track').val() == "mobile"){
                document.getElementById("add_to_cart_mobilepopup_triggerBtn").click();
            }
            else{
                 document.getElementById("add_to_cart_popup_triggerBtn").click();
            }
            createCookie(cookieName, JSON.stringify($scope.shoppingCart), 7);

        };
        $scope.addPackageToCart = function(id, quantity, fromQuickView) {
            $scope.initiateShoppingCartFromLocalStorage();
            $scope.lastItemAddedInCart = $scope._PACKAGE;

            var packageJsonStr = document.getElementById(id).value;
            var mallBdPackage = JSON.parse(packageJsonStr);


            if (!fromQuickView) {
                //document.getElementById("quick_view_product_"+mallBdPackage.id+"_trigger_btn").click();
                //return;
            }

            if (quantity == -1) {
                try {
                    if (typeof $("#quantity_modifier_" + mallBdPackage.id).val() != 'undefined') {
                        if (fromQuickView) {

                            quantity = parseInt($("#quantity_modifier_" + mallBdPackage.id).val());
                            $("#quantity_modifier_" + mallBdPackage.id).val("1");
                        } else {
                            quantity = parseInt($("#outter_quantity_modifier_" + mallBdPackage.id).val());
                            $("#outter_quantity_modifier_" + mallBdPackage.id).val("1");
                        }
                    }

                } catch (ex) {
                    console.log(ex);
                    quantity = 1;
                }
            } else {
                quantity = 1;
            }

            var packageCellIndex = getPackageGroupCell($scope.shoppingCart.mallBdPackageCell, mallBdPackage);
            if (packageCellIndex >= 0) {
                $scope.shoppingCart.mallBdPackageCell[packageCellIndex].quantity += quantity;// = product;



                var element = $scope.shoppingCart.mallBdPackageCell[packageCellIndex];
                $scope.shoppingCart.mallBdPackageCell.splice(packageCellIndex, 1);
                $scope.shoppingCart.mallBdPackageCell.splice(0, 0, element);
                $scope.prepareDisplayedCell(element, $scope._PACKAGE);
                $timeout(function() {
                    $scope.$apply(function() {

                    });
                }, 500);
            } else {
                var mallBdPackageCell = new MallBdPackageCell();
                mallBdPackageCell.quantity = quantity;
                mallBdPackageCell.mallBdPackage = mallBdPackage;

                // Package product are not added in cart... If need the products just remove the below line
//                mallBdPackageCell.mallBdPackage.mallBdPackageProduct = [];
                mallBdPackageCell.mallBdPackage.mallBdPackageProduct = mallBdPackage.packageProduct;
                // var selectedAttributes = getSelectedAttributeFromProduct(productCell.mallBdPackage);
                // productCell.selectedAttributes = selectedAttributes;

                $scope.shoppingCart.mallBdPackageCell.unshift(mallBdPackageCell);
                $scope.prepareDisplayedCell(mallBdPackageCell, $scope._PACKAGE);
            }

            try {
                document.getElementById("quick_view_package_" + mallBdPackage.id).style.display = "none";
            } catch (ex) {
                console.log(ex);
            }
            if($('#device_track').val() == "mobile"){
                document.getElementById("add_to_cart_mobilepopup_triggerBtn").click();
            }
            else{
                 document.getElementById("add_to_cart_popup_triggerBtn").click();
            }           
            createCookie(cookieName, JSON.stringify($scope.shoppingCart), 7);

        };
        $scope.emptyCart = function() {
            $scope.shoppingCart = new ShoppingCart();
            eraseLocalStorage(cookieName);
        };
        $scope.removeProductCell = function(index) {
            $timeout(function() {

            }, 2000);
            if ($scope.shoppingCart.productCell.length > 0) {
                $scope.shoppingCart.productCell.splice(index, 1);
                ($scope.shoppingCart.productCell.length > 0) ? createCookie(cookieName, JSON.stringify($scope.shoppingCart), 7) : eraseLocalStorage(cookieName);
            }
        };
        $scope.removeVoucherarray = function(index) {
            //console.log($scope.voucherdiscount);
            //console.log($scope.voucherarray[index].discount_amount);
            $scope.voucherdiscount = parseInt($scope.voucherdiscount) - parseInt($scope.voucherarray[index].discount_amount);
            $scope.voucherusedarray.pop($scope.voucherarray[index].voucher_code);
            $scope.voucherarray.splice(index, 1);
        };
        $scope.removePackageCell = function(index) {
            $timeout(function() {

            }, 2000);
            if ($scope.shoppingCart.mallBdPackageCell.length > 0) {
                $scope.shoppingCart.mallBdPackageCell.splice(index, 1);
                ($scope.shoppingCart.mallBdPackageCell.length > 0) ? createCookie(cookieName, JSON.stringify($scope.shoppingCart), 7) : eraseLocalStorage(cookieName);
            }
        };
//        $scope.increaseProductInProductCell = function(index) {
//            
//           // $scope.shoppingCart.productCell[index].quantity++;
//           // createCookie(cookieName, JSON.stringify($scope.shoppingCart), 7);
//        };
        $scope.increaseProductInProductCell = function(index) {
            var product_id = $scope.shoppingCart.productCell[index].id;
            var quantity = $scope.shoppingCart.productCell[index].quantity + 1;
            $('#product_qutantity_msg').html('');
            var postData = {
                "product_id": product_id,
                "quantity": quantity
            };
            $("#checkout_loader").css("display", "block");
            //$("#checkout_loader_cart").css("display", "block");
            $http({
                method: 'POST',
                url: $("#baseUrl").val() + 'api/checkout/productquantitycheck',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: postData
            }).success(function(response) {
                var data = response;
                //console.log(data);
                 //$("#checkout_loader_cart").css("display", "none");
                 $("#checkout_loader").css("display", "none");
                if (data.responseStat.status) {
                    $scope.shoppingCart.productCell[index].quantity++;
                    createCookie(cookieName, JSON.stringify($scope.shoppingCart), 7); 
                    $('.product_qutantity_msg').html("");
                }
                else {
                    $('.product_qutantity_msg').fadeIn("fast");
                    $('.product_qutantity_msg').html(data.responseStat.msg);
                }
                $('.product_qutantity_msg').fadeOut(10000);
            });


        };
        $scope.increasePackageInPackageCell = function(index) {
            var package_id = JSON.stringify($scope.shoppingCart.mallBdPackageCell[index].mallBdPackage.id);
            var quantity = $scope.shoppingCart.mallBdPackageCell[index].quantity + 1;
            $('#product_qutantity_msg').html('');
            var postData = {
                "package_id": package_id,
                "quantity": quantity
            };
            $("#checkout_loader").css("display", "block");
            $http({
                method: 'POST',
                url: $("#baseUrl").val() + 'api/checkout/packagequantitycheck',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: postData
            }).success(function(response) {
                var data = response;
                //console.log(data);
                $("#checkout_loader").css("display", "none");
                if (data.responseStat.status) {
                    $scope.shoppingCart.mallBdPackageCell[index].quantity++;
                    createCookie(cookieName, JSON.stringify($scope.shoppingCart), 7); 
                    $('##product_qutantity_msg').html("");
                }
                else {
                    $('#product_qutantity_msg').fadeIn("fast");
                    $('#product_qutantity_msg').html(data.responseStat.msg);
                }
                $('#product_qutantity_msg').fadeOut(7000);
            });


        };
        
        //cart manin page
        $scope.increaseProductInProductCellCartMain = function(index) {
            var product_id = $scope.shoppingCart.productCell[index].id;
            var quantity = $scope.shoppingCart.productCell[index].quantity + 1;
            $('#product_qutantity_msg').html('');
            var postData = {
                "product_id": product_id,
                "quantity": quantity
            };
            $("#checkout_loader_cart").css("display", "block");
            $http({
                method: 'POST',
                url: $("#baseUrl").val() + 'api/checkout/productquantitycheck',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: postData
            }).success(function(response) {
                var data = response;
                //console.log(data);
                $("#checkout_loader_cart").css("display", "none");
                if (data.responseStat.status) {
                    $scope.shoppingCart.productCell[index].quantity++;
                    createCookie(cookieName, JSON.stringify($scope.shoppingCart), 7); 
                    $('.product_qutantity_msg').html("");
                }
                else {
                    $('.product_qutantity_msg').fadeIn("fast");
                    $('.product_qutantity_msg').html(data.responseStat.msg);
                }
                $('.product_qutantity_msg').fadeOut(10000);
            });


        };
        $scope.increasePackageInPackageCellCartMain = function(index) {
            var package_id = JSON.stringify($scope.shoppingCart.mallBdPackageCell[index].mallBdPackage.id);
            var quantity = $scope.shoppingCart.mallBdPackageCell[index].quantity + 1;
            $('#product_qutantity_msg').html('');
            var postData = {
                "package_id": package_id,
                "quantity": quantity
            };
            $("#checkout_loader_cart").css("display", "block");
            $http({
                method: 'POST',
                url: $("#baseUrl").val() + 'api/checkout/packagequantitycheck',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: postData
            }).success(function(response) {
                var data = response;
//                console.log(data);
                $("#checkout_loader_cart").css("display", "none");
                if (data.responseStat.status) {
                    $scope.shoppingCart.mallBdPackageCell[index].quantity++;
                    createCookie(cookieName, JSON.stringify($scope.shoppingCart), 7); 
                    $('##product_qutantity_msg').html("");
                }
                else {
                    $('#product_qutantity_msg').fadeIn("fast");
                    $('#product_qutantity_msg').html(data.responseStat.msg);
                }
                $('#product_qutantity_msg').fadeOut(7000);
            });


        };
        //for cart main page
//        $scope.increasePackageInPackageCell = function(index) {
//            console.log($scope.shoppingCart.mallBdPackageCell[index]);
////            $scope.shoppingCart.mallBdPackageCell[index].quantity++;
////            createCookie(cookieName, JSON.stringify($scope.shoppingCart), 7);
//        };
        $scope.decreaseProductInProductCell = function(index) {            
            if ($scope.shoppingCart.productCell[index].quantity <= 1) {
                if(confirm("Are You Sure to Remove this product from cart?")){
                 $scope.removeProductCell(index);
                 $scope.shoppingCart.productCell[index].quantity--;
                }                
            }else if ($scope.shoppingCart.productCell[index].quantity <= $scope.shoppingCart.productCell[index].minimumOrderQuantity) {
                if(confirm("This Product minimum order quantity is:"+$scope.shoppingCart.productCell[index].minimumOrderQuantity+" If you decrease from this quantity it will remove from your cart.Are You Sure to Remove this product from cart?")){
                 $scope.removeProductCell(index); 
                 $scope.shoppingCart.productCell[index].quantity--;
                } 
            } else {
                createCookie(cookieName, JSON.stringify($scope.shoppingCart), 7);
                $scope.shoppingCart.productCell[index].quantity--;
            }
            
        };
        $scope.decreasePackageInPackageCell = function(index) {
            $scope.shoppingCart.mallBdPackageCell[index].quantity--;
            
            if ($scope.shoppingCart.mallBdPackageCell[index].quantity <= 0) {
                $scope.removePackageCell(index);
            } else {
                createCookie(cookieName, JSON.stringify($scope.shoppingCart), 7);
            }
        };
        $scope.getPictureUrl = function(val) {
            if (typeof val == 'undefined') {
                return "";
            }
            return $scope.imageUrl + "product/thumbnail/" + val;

        };
        $scope.getPackagesPictureUrl = function(val) {
            if (typeof val == 'undefined') {
                return "";
            }
            return $scope.imageUrl + "package/thumbnail/" + val;

        };
        $scope.prepareDisplayedCell = function(cartCell, cellType) {

            if ($scope.displayedCell.length == 0) {
                var displayCell = new DisplayCell();
                displayCell.cartCell = cartCell;
                displayCell.type = cellType;

                $scope.displayedCell.push(displayCell);
                return;
            }
            for (var i = 0; i < $scope.displayedCell.length; i++) {
                if (cartCell.id == $scope.displayedCell[i].cartCell.id && $scope.displayedCell[i].cartCell.type == cellType) {
                    var element = $scope.displayedCell[i];
                    $scope.displayedCell.splice(i, 1);
                    $scope.displayedCell.splice(0, 0, element);
                    return;
                }
            }
            var displayCell = new DisplayCell();
            displayCell.cartCell = cartCell;
            displayCell.type = cellType;

            $scope.displayedCell.push(displayCell);

        };
        var getProductGroupCell = function(productCell, product) {
            var matchedAttributes = 0;
            for (var i = 0; i < productCell.length; i++) {
                if (productCell[i].id == product.id) {
                    var selectedAttributes = productCell[i].selectedAttributes;
                    for (var j = 0; j < product.attributes.length; j++) {
                        var selectedAttributesVal = document.getElementById('select_attribute_' + product.id + '_' + product.attributes[j].id).value;
                        for (var k = 0; k < selectedAttributes.length; k++) {
                            if (selectedAttributes[k].id == selectedAttributesVal) {
                                matchedAttributes++;
                            }
                        }
                    }
                    if (product.attributes.length == matchedAttributes) {
                        return i;
                    } else {
                        matchedAttributes = 0;
                    }

                }


            }
            return -1;
        };
        var getPackageGroupCell = function(mallBdPackageCell, mallBdPackage) {
            var matchedAttributes = 0;
            for (var i = 0; i < mallBdPackageCell.length; i++) {
                if (mallBdPackageCell[i].mallBdPackage.id == mallBdPackage.id) {
                    return i;

                }


            }
            return -1;
        };
        var getSelectedAttributeFromProduct = function(product) {
            var selectedAttributes = [];
            var matchedAttributes = 0;
            for (var j = 0; j < product.attributes.length; j++) {
                var selectedAttributesId = document.getElementById('select_attribute_' + product.id + '_' + product.attributes[j].id).value;

                for (var k = 0; k < product.attributes[j].attributesValue.length; k++) {
                    if (product.attributes[j].attributesValue[k].id == selectedAttributesId) {
                        var selectedAttribute = new SelectedAttribute();
                        selectedAttribute.id = selectedAttributesId;
                        selectedAttribute.name = product.attributes[j].name;
                        selectedAttribute.value = product.attributes[j].attributesValue[k].value;

                        selectedAttributes.push(selectedAttribute);
                    }
                }
            }
            return selectedAttributes;

        };
        var addQuantity = function(accumulatedTotal, currentObj) {
            return accumulatedTotal + currentObj.quantity;
        };
        var addAmount = function(accumulatedTotal, currentObj) {
            return accumulatedTotal + (currentObj.product.prices[0].retailPrice * currentObj.quantity);
        };
        var addDiscountAmount = function(accumulatedTotal, currentObj) {
            if (currentObj.product.discountActiveFlag) {
                return accumulatedTotal + (currentObj.product.discountAmount * currentObj.quantity);
            }
            else {
                return accumulatedTotal;
            }

        };
        var addVoucherDiscountAmount = function(accumulatedTotal, currentObj) {
            return accumulatedTotal + (currentObj.discount * $scope.getSubTotalAmount()) / 100;
        };

        var addPackageAmountAmount = function(accumulatedTotal, currentObj) {
            return accumulatedTotal + (currentObj.mallBdPackage.packagePriceTotal * currentObj.quantity);
        };
        $scope.getProductQuantity = function(index) {
            return this.shoppingCart.productCell[index].quantity;
        };
        $scope.getPackageQuantity = function(index) {
            return this.shoppingCart.mallBdPackageCell[index].quantity;
        };
        $scope.getPrice = function(index) {
            return '৳ ' + this.shoppingCart.productCell[index].quantity * this.shoppingCart.productCell[index].product.prices[0].retailPrice;
        };
        $scope.getPackageItemPrice = function(index) {
            return '৳ ' + this.shoppingCart.mallBdPackageCell[index].quantity * this.shoppingCart.mallBdPackageCell[index].mallBdPackage.packagePriceTotal;
        };
        $scope.getPriceIndividual = function(index) {
            return '৳ ' + this.shoppingCart.productCell[index].product.prices[0].retailPrice;
        };
        $scope.getPriceAfterDiscount = function(index) {
            //alert(this.shoppingCart.productCell[index].product.discountAmount);
            var previous_price=parseInt(this.shoppingCart.productCell[index].product.prices[0].retailPrice)- parseInt(this.shoppingCart.productCell[index].product.discountAmount);
            return '৳ ' + previous_price+'  ';
        };
        $scope.getPackageItemPriceIndividual = function(index) {
            return '৳ ' + this.shoppingCart.mallBdPackageCell[index].mallBdPackage.packagePriceTotal;
        };
        $scope.getTotalQuantity = function() {
            return parseInt($scope.shoppingCart.productCell.reduce(addQuantity, 0)) + parseInt($scope.shoppingCart.mallBdPackageCell.reduce(addQuantity, 0));
        };
        $scope.getTotalAmount = function() {
//            console.log($scope.shoppingCart);
//            console.log("Price " + parseFloat($scope.shoppingCart.mallBdPackageCell.reduce(addPackageAmountAmount, 0)));
            if ($scope.shoppingCart.productCell.length == 0 && $scope.shoppingCart.mallBdPackageCell.length == 0) {
                return 0;
            }
            
            var totalPrice = parseFloat($scope.shoppingCart.productCell.reduce(addAmount, 0)) + parseFloat($scope.shoppingCart.mallBdPackageCell.reduce(addPackageAmountAmount, 0))+ parseFloat($scope.getDeliveryCharge()) - parseFloat($scope.getvoucherDiscountAmount()) - parseFloat($scope.getTotalDiscountAmount()) - parseFloat($scope.getCustomerPurchaseDiscountCalculation());
            //alert(parseFloat($scope.shoppingCart.productCell.reduce(addAmount, 0)) + parseFloat($scope.shoppingCart.mallBdPackageCell.reduce(addPackageAmountAmount, 0))+ parseFloat($scope.getDeliveryCharge()));
            return totalPrice.toFixed(2);
        };
        $scope.getSubTotalAmount = function() {
            var totalPrice = parseFloat($scope.shoppingCart.productCell.reduce(addAmount, 0)) + parseFloat($scope.shoppingCart.mallBdPackageCell.reduce(addPackageAmountAmount, 0));
            return totalPrice.toFixed(2);
        };
        $scope.getvoucherDiscountAmount = function() {
//            alert($scope.getTotalDiscountAmount());
            if($scope.getTotalDiscountAmount()<1){
                var totalVoucherDiscount = parseFloat($scope.voucherarray.reduce(addVoucherDiscountAmount, 0));
                return totalVoucherDiscount.toFixed(2);
            }
            else{
                var totalVoucherDiscount = 0;
                return totalVoucherDiscount.toFixed(2);
            }
        };
        $scope.getTotalDiscountAmount = function() {
            var totalDiscount = parseFloat($scope.shoppingCart.productCell.reduce(addDiscountAmount, 0));
            return totalDiscount.toFixed(2);
        };
        $scope.getDeliveryCharge = function() {
            var totalPr = parseFloat($scope.shoppingCart.productCell.reduce(addAmount, 0)) + parseFloat($scope.shoppingCart.mallBdPackageCell.reduce(addPackageAmountAmount, 0));
            if (totalPr > $scope.deliveryMethod.delivery_price_limit) {
                $scope.deliveryPriceforthisorder = 0;
            }
            else {
                $scope.deliveryPriceforthisorder = $scope.deliveryMethod.deliveryPrice;
            }
            return $scope.deliveryPriceforthisorder.toFixed(2);
        };
        $scope.getCustomerPurchaseDiscountCalculation = function() {
            var totalPr = parseFloat($scope.shoppingCart.productCell.reduce(addAmount, 0)) + parseFloat($scope.shoppingCart.mallBdPackageCell.reduce(addPackageAmountAmount, 0));
            if ($scope.customerPurchaseDiscounts.length > 0) {
                $scope.selectCustomerPurchaseDiscounts(0);
                if ($scope.customerPurchaseDiscount.discount_base == 'Number_Of_Order') {
                    if ($scope.customerPurchaseDiscount.discount_type == "Fixed") {
                        $scope.customerpurchasediscountforthisorder = $scope.customerPurchaseDiscount.discount_amount;
                    } else {    //for percentage discount
                        $scope.customerpurchasediscountforthisorder = $scope.customerPurchaseDiscount.discount_amount * totalPr / 100;
                    }
                } else {    //this is for amount discount
                    $scope.customerpurchasediscountforthisorder = 0;
                }
            } else {
                $scope.customerpurchasediscountforthisorder = 0;
            }

//            if (totalPr > $scope.deliveryMethod.delivery_price_limit) {
//                $scope.deliveryPriceforthisorder = 0;
//            }
//            else {
//                $scope.deliveryPriceforthisorder = $scope.deliveryMethod.deliveryPrice;
//            }
            return parseFloat($scope.customerpurchasediscountforthisorder).toFixed(2);
        };
        $scope.getZones = function() {

            $http({
                method: 'GET',
                url: $scope.baseUrl + "api/zone/all"
            }).then(function successCallback(response) {
                $scope.zone.id = "0";
                $scope.zones = response.data.responseData;
//                console.log($scope.zones);
                $timeout(function() {
                    initiateSelectField();
                }, 1000);
                // $scope.zone.id = (typeof $scope.zones[0]!='undefined'? $scope.zones[0].id:0);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });

        };
        $scope.getDeliveryMethods = function() {

            $http({
                method: 'GET',
                url: $scope.baseUrl + "api/deliverymethod/all"
            }).then(function successCallback(response) {
                $scope.deliveryMethods = response.data.responseData;

                $timeout(function() {
                    try {
                        (function() {
                            var b = $('.deliveryMethodRadioForAngular');
                            b.each(function() {
                                var _this = $(this);
                                if (_this.find('input[type="radio"]').is(':checked'))
                                    _this.addClass('selected');
                            });
                            b.on('click', function() {
                                $(this)
                                        .addClass('selected')
                                        .find('input[type="radio"]')
                                        .prop('checked', true)
                                        .end()
                                        .parent()
                                        .siblings()
                                        .find('.selected')
                                        .removeClass('selected');
                            });

                            // Selecting first element of Delivery method
                            if ($scope.shoppingCart.productCell.length > 0 || $scope.shoppingCart.mallBdPackageCell.length > 0) {
                                $('#element_delivery_method_id').find("figure").first().click();
                            }


                        })();
                    } catch (ex) {
                        console.log(ex);
                    }
                }, 3000);

            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });

        };
        $scope.getCustomerPurchaseDiscounts = function() {

            $http({
                method: 'GET',
                url: $scope.baseUrl + "getCustomerPurchaseDiscount"
            }).then(function successCallback(response) {
                $scope.customerPurchaseDiscounts = response.data.responseData;
                if(response.data.responseStat.status){
                    $('#customer_discount_message_div').css("display","block");
                    $('#customer_discount_message_show').html(response.data.discountMessage);
                }
                else{
                    $('#customer_discount_message_div').css("display","none");
                    $('#customer_discount_message_show').html(response.data.discountMessage);
                }
//                $timeout(function () {
//                    try {
//                        (function () {
//                            var b = $('.deliveryMethodRadioForAngular');
//                            b.each(function () {
//                                var _this = $(this);
//                                if (_this.find('input[type="radio"]').is(':checked'))
//                                    _this.addClass('selected');
//                            });
//                            b.on('click', function () {
//                                $(this)
//                                        .addClass('selected')
//                                        .find('input[type="radio"]')
//                                        .prop('checked', true)
//                                        .end()
//                                        .parent()
//                                        .siblings()
//                                        .find('.selected')
//                                        .removeClass('selected');
//                            });
//
//                            // Selecting first element of Delivery method
//                            if ($scope.shoppingCart.productCell.length > 0 || $scope.shoppingCart.mallBdPackageCell.length > 0) {
//                                $('#element_delivery_method_id').find("figure").first().click();
//                            }
//
//
//                        })();
//                    } catch (ex) {
//                        console.log(ex);
//                    }
//                }, 3000);

            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });

        };
        $scope.disableElements = function(id) {

        };
        $scope.getUserInformationAndShippingAddressByEmail = function(email, id) {
            // $("#"+id).attr("disabled","disabled");
            $http({
                method: 'POST',
                url: $scope.baseUrl + "api/user/information/get",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {email: email}
            }).success(function(response) {

                if (response.responseStat.status) {
                    var userDetails = response.responseData.userDetails;
                    var shippingAddressList = response.responseData.shippingAddress;
                    $scope.userInfo = userDetails;
                    $scope.userInfo.userDetails.address.shipping_firstname = response.responseData.userDetails.firstName;
                    $scope.userInfo.userDetails.address.shipping_lastname = response.responseData.userDetails.lastName;
                    $scope.userInfo.userDetails.address.shipping_phone = response.responseData.userDetails.phone;
                    $scope.userInfo.userDetails.address.shipping_address = response.responseData.userDetails.userDetails.address.address;
                    $scope.userInfo.userDetails.address.shipping_city = response.responseData.userDetails.userDetails.address.city;
                    $("#element_city").val(response.responseData.userDetails.userDetails.address.city).change();
                    $("#element_shipping_city").val(response.responseData.userDetails.userDetails.address.city).change();
                    $scope.shippingAddressList = shippingAddressList;


                }


            });

        };
        $scope.getShippingAddress = function(user_id) {

            $http({
                method: 'POST',
                url: $scope.baseUrl + "api/shippingaddress/user",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {user_id: user_id}
            }).success(function(response) {
                var data = response;
//                console.log(response);

            });

        };
        $scope.resetRequestStatusParams = function() {
            $scope.success.status = false;
            $scope.success.msg = "";
            $scope.responseError.status = false;
            $scope.responseError.msg = "";
        };
        $scope.editShippingAddress = function() {
            $scope.shippingAddressAction.edit = true;
            $scope.shippingAddressAction.add = false;
            $scope.resetRequestStatusParams();
            $scope.processing.status = true;
            $scope.processing.msg = "Processing....";

            $http({
                method: 'POST',
                url: $scope.baseUrl + "api/shippingaddress/update",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {
                    shipping_id: $scope.shippingAddress.id,
                    email: $scope.userInfo.email,
                    shipping_address: $scope.shippingAddress.shippingAddress,
                    shipping_country: $scope.shippingAddress.shippingCountry,
//                    shipping_city: $scope.shippingAddress.shippingCountry,
                    shipping_city: $scope.shippingAddress.shippingCity,
                    shipping_zipcode: $scope.shippingAddress.shippingZipCode
                }
            }).success(function(response) {
                $scope.processing.status = false;
                $scope.processing.msg = "";

                $scope.shippingAddress = response.responseData;

                if (response.responseStat.status) {
                    $scope.success.status = response.responseStat.status;
                    $scope.success.msg = response.responseStat.msg;
                    for (var i = 0; i < $scope.shippingAddressList.length; i++) {
                        if ($scope.shippingAddressList[i].id == response.responseData.id) {
                            $scope.shippingAddressList[i] = angular.copy(response.responseData);
                            break;
                        }
                    }
                    $timeout(function() {
                        $scope.shippingAddress = new ShippingAddress();
                        $scope.shippingAddressAction.edit = false;
                        $scope.shippingAddressAction.add = false;
                        $scope.resetRequestStatusParams();
                    }, 3000);
                } else {
                    $scope.responseError.status = response.responseStat.status;
                    $scope.responseError.msg = response.responseStat.msg;
                }

            });

        };


        $scope.addShippingAddress = function(index) {
            $scope.shippingAddressAction.edit = false;
            $scope.shippingAddressAction.add = true;
            var shippingAddress = $scope.shippingAddressList[index];
            $http({
                method: 'POST',
                url: $scope.baseUrl + "api/shippingaddress/add",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {
                    shipping_id: $scope.shippingAddress.id,
                    email: $scope.userInfo.email,
                    shipping_address: $scope.shippingAddress.shippingAddress,
                    shipping_country: $scope.shippingAddress.shippingCountry,
                    shipping_city: $scope.shippingAddress.shippingCountry,
                    shipping_city: $scope.shippingAddress.shippingCity,
                            shipping_zipcode: $scope.shippingAddress.shippingZipCode
                }
            }).success(function(response) {
                if (response.responseStat.status) {
                    $scope.success.status = response.responseStat.status;
                    $scope.success.msg = response.responseStat.msg;
                    $scope.shippingAddressList = angular.copy(response.responseData);
                    $timeout(function() {
                        $scope.shippingAddress = new ShippingAddress();
                        $scope.shippingAddressAction.edit = false;
                        $scope.shippingAddressAction.add = false;
                        $scope.resetRequestStatusParams();
                    }, 3000);
                } else {
                    $scope.responseError.status = response.responseStat.status;
                    $scope.responseError.msg = response.responseStat.msg;
                }

            });

        };

        $scope.removeShippingAddress = function(index) {
            // $("#"+id).attr("disabled","disabled");
            var shippingAddress = $scope.shippingAddressList[index];
            $http({
                method: 'POST',
                url: $scope.baseUrl + "api/shippingaddress/delete/by/email",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {shipping_id: shippingAddress.id, email: $scope.userInfo.email}
            }).success(function(response) {
                // $("#"+id).removeAttr("disabled");
                if (response.responseStat.status) {
                    $scope.success.status = response.responseStat.status;
                    $scope.success.msg = response.responseStat.msg;
                    $scope.shippingAddressList = angular.copy(response.responseData);
                } else {
                    $scope.responseError.status = response.responseStat.status;
                    $scope.responseError.msg = response.responseStat.msg;
                }


            });

        };
        $scope.getPaymentMethods = function() {

            $http({
                method: 'GET',
                url: $scope.baseUrl + "api/paymentmethods/all"
            }).then(function successCallback(response) {
                //console.log();
                $scope.paymentMethods = response.data.responseData;

                $timeout(function() {
                    try {
                        (function() {
                            var b = $('.paymentMethodRadioForAngular');

                            b.each(function() {
                                var _this = $(this);
                                if (_this.find('input[type="radio"]').is(':checked'))
                                    _this.addClass('selected');
                            });
                            b.on('click', function() {
                                $(this)
                                        .addClass('selected')
                                        .find('input[type="radio"]')
                                        .prop('checked', true)
                                        .end()
                                        .parent()
                                        .siblings()
                                        .find('.selected')
                                        .removeClass('selected');
                            });
                            if ($scope.shoppingCart.productCell.length > 0 || $scope.shoppingCart.mallBdPackageCell.length > 0) {
                                $("#element_payment_method_id").find("figure").first().click();
                            }

                        })();
                    } catch (ex) {
                        console.log(ex);
                    }
                }, 3000);



            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
        };
        $scope.productByManufacturer = function(i) {
            var searchParamObj = {"pa": []};
            var manufacturersIdList = [];
            manufacturersIdList.push(i);


            if (manufacturersIdList.length > 0) {
                searchParamObj['manufacturers'] = manufacturersIdList;
            }


            var searchParams = (
                    searchParamObj.pa.length > 0
                    || manufacturersIdList.length > 0
                    ) ? "?q=" + encodeURIComponent(JSON.stringify(searchParamObj)) : "";
            var viewSize = 10;
            searchParams += (searchParams != "") ? "&limit=" + viewSize + "&page=0" : "";
//    window.location = location.protocol + '//' + location.host + location.pathname+searchParams;
//            console.log(location);
            window.location = $("#baseUrl").val() + 'manufacturer' + searchParams;
        };
        $scope.selectPaymentMethods = function(i) {
            $scope.paymentMethod = $scope.paymentMethods[i];
        };
        $scope.selectDeliveryMethods = function(i) {
            $scope.deliveryMethod = $scope.deliveryMethods[i];
        };
        $scope.selectCustomerPurchaseDiscounts = function(i) {
            $scope.customerPurchaseDiscount = $scope.customerPurchaseDiscounts[i];
        };
        $scope.updateHtmlManipulateObj = function(htmlManipulateObj) {
            $scope.htmlManipulateObj = htmlManipulateObj;
        };

        $scope.setShippingAddress = function(index) {

            $scope.shippingAddress = angular.copy($scope.shippingAddressList[index]);
        };
        $scope.setShippingAddressAndFocusToElement = function(index, id) {
            /* For Myaccount shipping address add / edit [Start]*/
            $scope.shippingAddressAction.edit = true;
            $scope.shippingAddressAction.add = false;
            /* For Myaccount shipping address add / edit [Ends]*/
            $scope.setShippingAddress(index);
            $timeout(function() {
                try {
                    $('#' + id).focus();
                } catch (ex) {
                    console.log(ex);
                }
            }, 500);

        };
        $scope.showShippingAddressAddFrom = function(id) {
            /* For Myaccount shipping address add / edit [Start]*/
            $scope.shippingAddressAction.edit = false;
            $scope.shippingAddressAction.add = true;
            /* For Myaccount shipping address add / edit [Ends]*/

            $timeout(function() {
                try {
                    $('#' + id).focus();
                } catch (ex) {
                    console.log(ex);
                }
            }, 1000);

            $scope.shippingAddress = new ShippingAddress();
        };
        $scope.submitOrder = function() {
            $('#aftersubmit_login_email_found').html('');
            $("#checkout_loader").css("display", "block");
            $scope.responseError.status = false;
            $scope.responseError.msg = "";
//        if(!$scope.agreeTerms ){
//            $scope.responseError.status = true;
//            $scope.responseError.msg = "Agree the terms";
//            return;
//        }
            $scope.processing.done = false;
            $scope.processing.msg = "Processing....";
            $scope.shoppingCart.orderTotal = $scope.getSubTotalAmount();
            $scope.shoppingCart.totalPrice = $scope.getTotalAmount();
            $scope.shoppingCart.shippingCost = $scope.getDeliveryCharge();
            $scope.shoppingCart.voucherDiscount = $scope.getvoucherDiscountAmount();
            $scope.shoppingCart.productDiscountTotal = $scope.getTotalDiscountAmount();
            $scope.zone.id = ($scope.zone.id == "") ? 0 : $scope.zone.id;

            var customerDiscount = $scope.getCustomerPurchaseDiscountCalculation();
            var customerDiscountDetails = [];

            if ($scope.customerPurchaseDiscounts.length > 0) {
                customerDiscountDetails = $scope.customerPurchaseDiscount;
            }
            var shipping_firstname = $scope.userInfo.firstName;
            var shipping_lastname = $scope.userInfo.lastName;
            var shipping_phone = $scope.userInfo.phone;            
            $scope.userInfo.userDetails.address.address = $("#element_address").val();
            var shipping_address = $scope.userInfo.userDetails.address.address;
//            var shipping_city = $scope.userInfo.userDetails.address.city;
            var shipping_city = $("#element_city").val();
            if ($('#invoice_address').prop('checked')) {
                 shipping_firstname = $scope.userInfo.userDetails.address.shipping_firstname;
                 shipping_lastname = $scope.userInfo.userDetails.address.shipping_lastname;
                 shipping_phone = $scope.userInfo.userDetails.address.shipping_phone;
                 $scope.userInfo.userDetails.address.shipping_address = $("#element_shipping_address").val();
                 shipping_address = $scope.userInfo.userDetails.address.shipping_address;
//                 shipping_city = $scope.userInfo.userDetails.address.shipping_city;
                 shipping_city = $("#element_shipping_city").val();
            }

//            console.log($scope.zone);
            var postData = {
                first_name: $scope.userInfo.firstName,
                last_name: $scope.userInfo.lastName,
                email: $scope.userInfo.email,
                phone: $scope.userInfo.phone,
                address: $scope.userInfo.userDetails.address.address,
//                city: $scope.userInfo.userDetails.address.city,
                city: $("#element_city").val(),
                order_from: "SITE",
                invoice_address: $('#invoice_address').prop('checked'),
                //zipcode: $scope.userInfo.userDetails.address.zipCode,
                // country: $scope.userInfo.userDetails.address.country,
                shipping_firstname: shipping_firstname,
                shipping_lastname: shipping_lastname,
                shipping_phone: shipping_phone,
                shipping_address: shipping_address, //$scope.shippingAddress.shippingAddress,
                shipping_country: $scope.userInfo.userDetails.address.country, //$scope.shippingAddress.shippingCountry,
                shipping_zipcode: $scope.userInfo.userDetails.address.zipCode, //$scope.shippingAddress.shippingZipCode,
                shipping_city: shipping_city, //$scope.shippingAddress.shippingCity,
                delivery_method_id: $scope.deliveryMethod.id,
                payment_method_id: $scope.paymentMethod.id,
                currency_id: 1,
                zone_id: $scope.zone.id,
                customerDiscount: customerDiscount,
                voucherDiscountDetails: JSON.stringify($scope.voucherarray),
                customerDiscountDetails: JSON.stringify($scope.customerPurchaseDiscount),
                //voucherdiscount: $scope.voucherdiscount,
                shopping_cart: JSON.stringify($scope.shoppingCart)
            };
            $http({
                method: 'POST',
                url: $("#baseUrl").val() + 'api/checkout/submitWeb',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: postData
            }).success(function(response) {
                $("#checkout_loader").css("display", "none");
                var data = response;

                $scope.processing.done = true;
                $scope.processing.msg = "";

                $scope.responseError.status = !data.responseStat.status;
                $scope.responseError.msg = data.responseStat.msg;

                $scope.success.status = data.responseStat.status;
                $scope.success.msg = data.responseStat.msg;
                if (!data.responseStat.status) {
                    if (typeof data.responseStat.extra.hasLoginCreadential != "undefined") {
                        if (data.responseStat.extra.hasLoginCreadential) {
                            $('#aftersubmit_login_email_found').html("You already register by '"+$scope.userInfo.email+"' Please login");
                            showLoginForm("");
                        }
                    }
                }

                if (data.responseStat.status) {
                    //alert($scope.paymentMethod.id);
                    // Redirect to Sucess page
                    if ($scope.paymentMethod.id == 2) {
                        window.location.assign($("#baseUrl").val() + "bkash/" + data.responseData.unique_code);
                    }
                    else if ($scope.paymentMethod.id == 3) {
                        window.location.assign($("#baseUrl").val() + "api/paypal/" + data.responseData.order_id);
                    }
                    else if ($scope.paymentMethod.id == 4) {
                        window.location.assign($("#baseUrl").val() + "api/walletmix/" + data.responseData.order_id);
                    }
                    else {
                        $scope.emptyCart();
                        window.location.assign($("#baseUrl").val() + "checkout/greeting/" + data.responseData.order_id);
                    }

                }
                $('#element_email').focus();
                $('#checkout_page_big_error').fadeIn();
                $('#checkout_page_big_error').html("Error Found!! Please Check All Data Carefully and submit again");
                window.scrollTo(0, 0);
                $scope.htmlManipulateObj = initiateErrorTracing($scope.htmlManipulateObj, $scope.checkoutFormParam, data.responseStat.requestError);
//                console.log($scope.htmlManipulateObj);
            });


//            $("#checkout_loader").css("display", "none");
        };

        $scope.doLoginFromCheckout = function() {
            $('#checkoutLoginMsg').html('Processing...');
            var email = $('#checkoutLogInEmail').val();
            var password = $('#checkoutLogInPassword').val();
            if (email == "" || password == "")
            {
                $('#checkoutLoginMsg').html("Email or Password field is empty");
                return false;
            }
            var postData = {
                "email": email,
                "password": password
            };
            $http({
                method: 'POST',
                url: $("#baseUrl").val() + 'login',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: postData
            }).success(function(response) {
                var data = response;
//                console.log(data);
                $('#checkoutLoginMsg').html(data.responseStat.msg);
                if (data.responseStat.status && data.responseStat.isLogin) {
                    $scope.userInfo.email = data.responseData.user.email;
                    $scope.userInfo.firstName = data.responseData.user.firstName;
                    $scope.userInfo.lastName = data.responseData.user.lastName;
                    $scope.userInfo.phone = data.responseData.user.phone;
                    $scope.userInfo.userDetails.address.address = data.responseData.user.userDetails.address.address;
                    $scope.userInfo.userDetails.address.city = data.responseData.user.userDetails.address.city;
                    $("#element_city").val(data.responseData.user.userDetails.address.city).change();
                    //inovice shipping address
                    $scope.userInfo.userDetails.address.shipping_firstname = data.responseData.user.firstName;
                    $scope.userInfo.userDetails.address.shipping_lastName = data.responseData.user.lastName;
                    $scope.userInfo.userDetails.address.shipping_phone = data.responseData.user.phone;
                    $scope.userInfo.userDetails.address.shipping_address = data.responseData.user.userDetails.address.address;
                    $scope.userInfo.userDetails.address.shipping_city = data.responseData.user.userDetails.address.city;
                    $("#element_shipping_city").val(data.responseData.user.userDetails.address.city).change();
                    $('#element_email').attr("disabled", "disabled");
                    $('#checkoutLoginForm').fadeOut(500);
                    $('#checkoutLoginMsg').html('');
                    $('#login_popup').hide();
                    $('#userName').val(data.responseData.user.firstName);
                    $('#loginText').hide();
                    $('#loginName').html(data.responseData.user.firstName).show();
                    $('#logoutText').show();
                    $scope.getCustomerPurchaseDiscounts();
                    $scope.getCustomerPurchaseDiscountCalculation();
                    afterLoginFunctionCall();
                    $('#open_chat_anchor').attr('onclick', 'openChat(' + data['responseData']['user']['id'] + ')');
                    //            $('#open_chat_anchor').prop("onclick", false);
                    //            document.getElementById('open_chat_anchor').onclick = function () { openChat(data['responseData']['id']) };
                    //            console.log($('#open_chat_anchor').html());
                    socket.emit('init', data['responseData']['user']['id']);

                }
                else {
                    $('#checkoutLogInEmail').prop('disabled', false);
                    $('#checkoutLogInPassword').prop('disabled', false);
                }

//                console.log(response);
            });


        };

        $scope.submitVoucher = function() {
            if($scope.getTotalDiscountAmount()>0){
                 $("#voucher_error").html("You cann't add voucher on discount products in Order");
                 return false;
            }
            var voucher_code = $("#voucher_code").val();
//            console.log($scope.voucherusedarray.indexOf(voucher_code));
            if (voucher_code != "" && $scope.voucherusedarray.indexOf(voucher_code) === -1) {
                var postData = {
                    voucher_code: $("#voucher_code").val(),
                    total_price: $scope.getSubTotalAmount()
                };
                $http({
                    method: 'POST',
                    url: $("#baseUrl").val() + 'api/checkout/voucher',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    transformRequest: function(obj) {
                        var str = [];
                        for (var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                        return str.join("&");
                    },
                    data: postData
                }).success(function(response) {
                    var data = response;
//                    console.log(data);
                    if (data.responseStat.status) {
                        $scope.voucherusedarray.push(voucher_code);
                        $scope.voucherarray.push(data.responseData);
//                        console.log($scope.voucherusedarray);
//                        console.log($scope.voucherarray);
                        // $scope.voucherdiscount = parseInt($scope.voucherdiscount) + parseInt(amount);
                        $("#voucher_code").css("border", "");
                        $("#voucher_code").val("");
                         $("#voucher_error").html("");
                    }
                    else {
                        $("#voucher_code").css("border", "1px solid red");
                        $('#voucher_error').fadeIn();
//                        $("#voucher_error").html("Wrong voucher code");
                        $("#voucher_error").html(data['responseStat']['msg']);
                        $('#voucher_error').fadeOut(7000);

                    }

//                    console.log($scope.htmlManipulateObj);
                });
            }
            else {
                $("#voucher_code").css("border", "1px solid red");
                $('#voucher_error').fadeIn();
                $("#voucher_error").html("Already Used This code in this Order");
                $('#voucher_error').fadeOut(7000);

            }


        };
        // Call Ajax for data only on checkout page
        if ($scope.isCheckoutPage()) {
            $scope.getPaymentMethods();
            $scope.getZones();
            $scope.getDeliveryMethods();
            $scope.getCustomerPurchaseDiscounts();
            if ($scope.isLogin()) {
                $scope.getUserInformationAndShippingAddressByEmail("element_email");
            }

        }
        if ($scope.isMyAccountPage()) {
            if ($scope.isLogin()) {
                $scope.getUserInformationAndShippingAddressByEmail("element_email");
            }
        }
        $scope.checkoutPageInitAfterLogin = function() {
            if ($scope.isLogin()) {
//                $scope.getCustomerPurchaseDiscounts();
//                $scope.getCustomerPurchaseDiscountCalculation();
                $scope.getUserInformationAndShippingAddressByEmail("element_email");
            }
        };

        var reachedLast = false;
        $scope.testData = [];
        $scope.loadmore = "Loading more data...";


        $scope.listData = function($pageType) {
            if (reachedLast)
                return false;


            $scope.url = $scope.baseUrl + 'api/manufacturer/allforweb';

            var hasBrands = $('.product_brands').find('.owl-wrapper-outer');

            if ($pageType == "prev") {
                if (NextpageNumber > 0) {
                    NextpageNumber = NextpageNumber - 1;
                } else {
                    NextpageNumber = 0;
                }
            } else {
                if (hasBrands.length > 0) {
                    NextpageNumber = NextpageNumber + 1;
                } else {
                    NextpageNumber = 0;
                }
            }

            var dataObj = {
                limit: 6,
                offset: NextpageNumber,
                page_type: $pageType,
            };

            $http.post($scope.url, dataObj).success(function(data, status) {

                $scope.status = status;
                $scope.data = data;
                var currentpage = NextpageNumber;
                var lastpage = data.lastpage;
                NextpageNumber = data.nextpage;

                getManufecturer(data, $scope.imageUrl, function(dataSet) {
                    $scope.$applyAsync(function() {

                        if (hasBrands.length <= 0) {

                            $scope.brandDataSet = dataSet;
                            setTimeout(function() {
                                var globalDfd = $.Deferred();
                                globalDfd.resolve();
                                $('.product_brands a.animate_fade').unbind().bind().waypointSynchronise({
                                    container: '.product_brands',
                                    delay: 0,
                                    offset: 830,
                                    classN: "animate_sj_finished animate_fade_finished"
                                });

                                if ($('.product_brands').length) {
                                    var pb = $(".product_brands").unbind().bind().owlCarousel({
                                        itemsCustom: $('.product_brands').hasClass('with_sidebar') ? [
                                            [1199, 4],
                                            [992, 4],
                                            [768, 3],
                                            [480, 3],
                                            [300, 2]
                                        ] : [
                                            [1199, 6],
                                            [992, 5],
                                            [768, 4],
                                            [480, 3],
                                            [300, 2]
                                        ],
                                        autoPlay: false,
                                        stopOnHover: true,
                                        slideSpeed: 600,
                                        addClassActive: true
                                    });

                                    $('.pb_prev').unbind().bind('click', function() {
                                        pb.trigger('owl.prev');
                                    });

                                    $('.pb_next').unbind().bind('click', function() {
                                        pb.trigger('owl.next');
                                    });
                                }
                            }, 200);
                        } else {
                            var html = "";
                            dataSet.forEach(function(index) {
//                                console.log(index);
                                html += '<div class="owl-item active" style="width: 190px;">'
                                        + '<a class="d_block t_align_c animate_fade ng-scope animate_sj_finished animate_fade_finished">'
                                        + '<img style="cursor: pointer;" onerror="this.src="http://placehold.it/160x120?text=Icon not found"" alt="' + index.imageUrl + '" src="' + index.imageUrl + '" onclick="productByManufacturer(' + index.id + ')">'
                                        + '</a>'
                                        + '</div>';
                            });

                            $('.product_brands').find('.owl-item').remove();
                            $('.product_brands').find('.owl-wrapper').html(html);
                        }

                    });
                });

                if ((currentpage == NextpageNumber)) {
                    return false;
                }

                if ((currentpage == lastpage)) {

                    reachedLast = true;
                    $scope.loadmore = "Reached at bottom";
//                 $scope.$apply();

                }

                $scope.testData = $scope.testData.concat(data.result);
//             if ($scope.testData != undefined) {
//                 $scope.testData = $scope.testData.concat(data.result);
//             }else {
//                 $scope.testData =data.result;
//             }

            }).error(function(data, status) {
                $scope.data = data || "Request failed";
                $scope.status = status;
            });


        };


    }]);

function getManufecturer($json, imageUrl, callback) {
    var dataObject = [];
    if (typeof $json.manufacturers == 'object' && $json.manufacturers.length > 0) {
        var manufacturersObj = {};
        imageUrl = imageUrl + "manufacturer/";
        $json.manufacturers.forEach(function(index) {
            index.imageUrl = imageUrl + '' + index.icon;
//            console.log(index);
            dataObject.push(index);
        });
        callback(dataObject);
    }
}

function initiateSelectField() {
    $('.custom_select').each(function() {
        var list = $(this).children('ul'),
                select = $(this).find('select'),
                title = $(this).find('.select_title');
        title.css('min-width', title.outerWidth());

        // select items to list items

        if ($(this).find('[data-filter]').length) {
            for (var i = 0, len = select.children('option').length; i < len; i++) {
                list.append('<li data-filter="' + select.children('option').eq(i).data('filter') + '" class="tr_delay_hover">' + select.children('option').eq(i).text() + '</li>')
            }
        }
        else {
            for (var i = 0, len = select.children('option').length; i < len; i++) {
                list.append('<li class="tr_delay_hover">' + select.children('option').eq(i).text() + '</li>')
            }
        }
        select.hide();

        // open list

        title.on('click', function() {
            list.slideToggle(400);
            $(this).toggleClass('active');
        });

        // selected option

        list.on('click', 'li', function() {
            var val = $(this).text();
            title.text(val);
            list.slideUp(400);
            select.val(val);
            title.toggleClass('active');
        });

    });

}
function createCookie(name, value, days) {
    //var expires = "";
    //if (days) {
    //    var date = new Date();
    //    date.setTime(date.getTime()+(days*24*60*60*1000));
    //    expires = "; expires="+date.toGMTString();
    //}
    //console.log(value);
    //document.cookie = name+"="+btoa(value)+expires+"; path=/";

    //sessionStorage.setItem(name,btoa(value));
    localStorage.setItem(name, value);

}

function readFromLocalStorage(name) {
    //var nameEQ = name + "=";
    //var ca = document.cookie.split(';');
    //for(var i=0;i < ca.length;i++) {
    //    var c = ca[i];
    //    while (c.charAt(0)==' ') c = c.substring(1,c.length);
    //    if (c.indexOf(nameEQ) == 0) return atob(c.substring(nameEQ.length,c.length));
    //}
    //return null;
    var tmpShoppingCart = localStorage.getItem(name);

    //return (tmpShoppingCart!=null)?atob(tmpShoppingCart):tmpShoppingCart;
    return tmpShoppingCart; //(tmpShoppingCart!=null)?tmpShoppingCart:tmpShoppingCart;
}

function eraseLocalStorage(name) {
    createCookie(name, "", -1);
}
function getUserInformation(id, shipping_elem_id) {
    var email = $("#" + id).val();
    var scope = angular.element(document.getElementById("MainWrap")).scope();
    scope.$apply(function() {
        scope.getUserInformationAndShippingAddressByEmail(email, id, shipping_elem_id);
    });
}
function viewCart() {
    document.getElementById("add_to_cart_popup_triggerBtn").click();
}

angular.element(document).ready(function() {
    setTimeout(function() {
        $('.pb_next_new').trigger('click');
    }, 200);

    //homeproduct();

});

// function homeproduct(){
//
//     var baseUrl = document.getElementById("baseUrl").value;
//
//     $.ajax({
//         url: baseUrl+"homeproducts",
//         type: "post",
//         data: "" ,
//         success: function (response) {
//             $('#product_show_container').html(response);
//         },
//         error: function(jqXHR, textStatus, errorThrown) {
//
//         }
//
//
//     });
// }

