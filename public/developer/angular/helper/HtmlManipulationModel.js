function HtmlManipulateObj()
{
    this.isHidden = true;
    this.htmlValus = "";
    this.tag = "";
}

function initiateHtmlManipulateObj(tagList){
    var htmlManipulateObjList={};
    for(var i=0;i<tagList.length;i++){
        var htmlManipulateObj = new HtmlManipulateObj();
        htmlManipulateObj.tag = tagList[i];
        htmlManipulateObjList[tagList[i]] = htmlManipulateObj;
    }
    return htmlManipulateObjList;
}
function resetManipulateObj(htmlManipulateObj){

    for(var key in htmlManipulateObj){
        htmlManipulateObj[key].isHidden = true;
        htmlManipulateObj[key].htmlValus = "";
        htmlManipulateObj[key].tag = "";

    }
   return htmlManipulateObj;
}
function initiateErrorTracing(htmlManipulateObj,formParam,requestError){
    var minFormParamIndex = formParam.length;
    htmlManipulateObj = resetManipulateObj(htmlManipulateObj);
    for(var i=0;i<requestError.length;i++){
        console.log(requestError[i].params+" "+requestError[i].msg);
        console.log(htmlManipulateObj[requestError[i].params]);
        if(typeof htmlManipulateObj[requestError[i].params]!='undefined'){
            htmlManipulateObj[requestError[i].params].isHidden = false;
            htmlManipulateObj[requestError[i].params].htmlValus = requestError[i].msg;

            var tmpIndex = formParam.indexOf(requestError[i].params);
            if(tmpIndex<minFormParamIndex){
                minFormParamIndex = tmpIndex;
            }
        }else{
            console.log("Key : "+requestError[i].params+" "+requestError[i].msg+" not found");
        }

    }


    if(minFormParamIndex<formParam.length){
        try{
            console.log('#element_'+formParam[minFormParamIndex]);
            var parentElement;
            var elementsParentScrollPostion = 0;
            if($('#element_'+formParam[minFormParamIndex]).parents('.angularFocus').first().length ==1 ){
                elementsParentScrollPostion = $('#element_'+formParam[minFormParamIndex]).parents('.angularFocus').first().attr('scrolltopposition');
            }else{
                elementsParentScrollPostion = (typeof $('#element_'+formParam[minFormParamIndex]).attr('scrolltopposition')!='undefined')
                                                        ?$('#element_'+formParam[minFormParamIndex]).attr('scrolltopposition'):0;
            }


            if(elementsParentScrollPostion > 0){
                $('html, body').animate({scrollTop:elementsParentScrollPostion}, 'slow',function(){
                    try{
                         $('#element_'+formParam[minFormParamIndex]).focus();
                    }catch(ex){
                        console.log(ex);
                    }
                });
            }
        }catch(ex){
            console.log(ex);
        }

    }
    return htmlManipulateObj;
}