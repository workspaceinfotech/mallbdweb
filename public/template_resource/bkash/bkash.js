$(document).ajaxStart(function()
{
    $('.ajax-loader').show();
});

$(document).ajaxComplete(function()
{
    $('.ajax-loader').hide();
});

function returntostore(order_id) {
    $.ajax({
        "type": "POST",
        "url": $("#baseUrl").val() + '/api/paymentcancel',
        "data": {
            order_id: order_id
        }
    }).done(function(result)
    {
        var delay = 5000; //Your delay in milliseconds
        setTimeout(function() {
            window.location = $("#baseUrl").val() + "checkout";
        }, delay);

    });
}

$(document).ready(function()
{
    $(document).on('submit', '#gateway_bkash_card', function()
    {
        var sArray = $(this).serializeArray()

        $.ajax({
            "type": "POST",
            "url": $("#baseUrl").val() + 'api/bkash-data-process',
            "data": sArray,
            "dataType": "json"
        }).done(function(result)
        {
            if (result.responseStat.status) {
                myShowStatusMessage(result.message, result.messageType, '#gateway_bkash_card');
                if (result.txn_status) {
                    $("#bKashBtn").attr("disabled", "disabled");
                    $.ajax({
                        "type": "POST",
                        "url": $("#baseUrl").val() + '/api/paymentsuccess',
                        "data": {
                            order_id: result.responseStat.OrderId,
                            payment_details:result.responseData
                        }
                    }).done(function(result)
                    {
                        eraseLocalStorage('cart');
                        var delay = 5000; //Your delay in milliseconds
                        setTimeout(function() {
                            window.location = $("#baseUrl").val() + "checkout/paymentgreeting";
                        }, delay);

                    });
                }
            }
            else {
                
                $('#error_show').html(result.responseStat.msg);
            }

        });

        return false;
    });
});

var myShowStatusMessage = function(message, type, selector)
{
    $('.status-message').remove();
    $('.label-danger').remove();

    var html = '<div class="row status-message">\n\
                        <div class="col-lg-12">\n\
                            <div class="alert alert-' + type + '">\n\
                                ' + message + '\n\
                            </div>\n\
                        </div>\n\
                </div>';

    $(html).prependTo(selector).hide().fadeIn(500);
};

var showStatusMessage = function(message, type)
{
    $('.status-message').remove();
    $('.label-danger').remove();

    var html = '<div class="row status-message">\n\
                        <div class="col-lg-12">\n\
                            <div class="alert alert-' + type + '">\n\
                                ' + message + '\n\
                            </div>\n\
                        </div>\n\
                </div>';

    $(html).prependTo('#main-container').hide().fadeIn(900);
};

var showRegisterFormAjaxErrors = function(errors)
{
    $('.status-message').remove();
    $('.label-danger').remove();
    for (var errorType in errors)
    {
        for (var i in errors[errorType])
        {
            $('[name="' + errorType + '"]').closest('.form-group').append('<span class="label label-danger error-' + errorType + '">' + errors[errorType][i] + '</span>');
        }
    }
};
