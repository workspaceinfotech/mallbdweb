@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" onsubmit=" return doRegistration();">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">First Name</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control mendatory" id="first_name" name="first_name" value="{{ old('first_name') }}">

                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Last Name</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control mendatory" id="last_name" name="last_name" value="{{ old('last_name') }}">

                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input type="email" class="form-control mendatory" id="email" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                         <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Phone Number</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control mendatory" id="phone" name="phone" value="{{ old('phone') }}">

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input type="password" id="password" class="form-control mendatory" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input type="password" id="confirm_password" class="form-control mendatory" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                           <p class="help-block text-muted text-center">
                               <span id="msg" style="display: none" class="alert alert-danger"></span>
                           </p>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i>Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script>
function doRegistration(){
        var valid = true;
        var errorMsg = "";
         $('#msg').show();
        $('#msg').html("");
        $('.mendatory').each(function () {
            if ((valid && $(this).val() == "") || ($(this).prop("tagName") == "SELECT" && $(this).val()==0)) {
                console.log("")
                if ($(this).prop("tagName") == "SELECT") {
                    errorMsg = "Select " + $(this).attr("name");
                } else {
                    errorMsg = $(this).attr("name") + " field required";
                }

                $(this).focus();
                valid = false;
                return false;
            }
        });



            var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

            if ($('#email').val() != "" && !re.test($('#email').val())) {
                $('#email').focus();
                valid = false;
                errorMsg = 'Email is not in valid format';
            }
            if ($('#password').val() != "" && $('#password').val().length < 6) {
                $('#password').focus();
                valid = false;
                errorMsg = 'At least 6 digit required';
            }
            if (!valid) {
                $('#msg').html(errorMsg);
                return false;
            }

            var firstName = $('#first_name').val();
            var lastName = $('#last_name').val();
            var phone = $('#phone').val();
            var email = $('#email').val();
            var password = $('#password').val();
            var confirm =  $('#confirm_password').val();


            $('#msg').html("");
            $.ajax({
                url: $("#baseUrl").val() + "index.php/api/registration",
                method: "POST",
                data: {
                    "first_name": firstName,
                    "last_name": lastName,
                    "phone": phone,
                    "email": email,
                    "password": password,
                    "password_confirmation":confirm
                },
                success: function (data) {
                    console.log(data);
                    console.log(data.responseStat.status);
                    console.log(data.responseStat.isLogin);
                    $('#msg').html(data.responseStat.msg);
                    if (data.responseStat.status) {
                        $('#msg').delay(1000).fadeOut(500, function () {
                            window.location.href = $("#baseUrl").val() + "index.php/login"
                        });
                    }
                }

            });
            return false;
}
</script>
