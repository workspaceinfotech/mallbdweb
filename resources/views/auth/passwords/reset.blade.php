<!doctype html>
<!--[if IE 9 ]><html class="ie9" lang="en" ng-app="mallBd"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" ng-app="mallBd"><!--<![endif]-->

@include('web/partial/header/mainStart')
@include('web/partial/header/cartScript')
@include('web/partial/header/mainEnds')

<body ng-controller="CartController">
<!--wide layout-->
<div class="wide_layout relative">
    <!--[if (lt IE 9) | IE 9]>
    <div style="background:#fff;padding:8px 0 10px;">
        <div class="container" style="width:1170px;">
            <div class="row wrapper">
                <div class="clearfix" style="padding:9px 0 0;float:left;width:83%;"><i
                        class="fa fa-exclamation-triangle scheme_color f_left m_right_10"
                        style="font-size:25px;color:#e74c3c;"></i><b style="color:#e74c3c;">Attention! This page may not
                    display correctly.</b> <b>You are using an outdated version of Internet Explorer. For a faster, safer
                    browsing experience.</b></div>
                <div class="t_align_r" style="float:left;width:16%;"><a
                        href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"
                        class="button_type_4 r_corners bg_scheme_color color_light d_inline_b t_align_c" target="_blank"
                        style="margin-bottom:2px;">Update Now!</a></div>
            </div>
        </div>
    </div>
    <![endif]-->
    <!--markup header-->
    <header role="banner" class="type_5 fixed-top">


        @include('web/partial/header/top')
                <!--header bottom part-->
        @include('web/partial/new_menu')

    </header>
    <div class="other-free-gap">

    </div>
    <!--breadcrumbs-->
    <section class="breadcrumbs">
        <div class="container">
            <ul class="horizontal_list clearfix bc_list f_size_medium">
                <li class="m_right_10 current"><a href="#" class="default_t_color">Home<i
                                class="fa fa-angle-right d_inline_middle m_left_10"></i></a></li>
                <li><a href="" class="default_t_color">Reset Password</a></li>
            </ul>
        </div>
    </section>
    <!--content-->
    <div class="page_content_offset">
        <div class="container">
            <div class="row clearfix">
                <!--left content column-->
                <section class="col-lg-9 col-md-9 col-sm-9 m_xs_bottom_30">
                    <h2 class="tt_uppercase color_dark m_bottom_25" align="center">Reset Password</h2>
                    <!--cart table-->

                    <!--tabs-->
                    <div class="tabs m_bottom_45">
                        <!--tabs navigation-->
                        <section class="tabs_content shadow r_corners">
                            <div id="tab-2">
                                @if (session('status'))
                                    <div class="alert alert-success">
                                        {{ session('status') }}
                                    </div>
                                @endif

                                <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                                    {!! csrf_field() !!}

                                    <input type="hidden" name="token" value="{{ $token }}">

                                    <ul>
                                        <br>
                                        <li class="m_bottom_25">
                                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                <label class="col-md-4 control-label">E-Mail Address</label>

                                                <div class="col-md-6">
                                                    <input type="email" class="form-control" name="email" value="{{ $email or old('email') }}">

                                                    @if ($errors->has('email'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </li>

                                        <li class="m_bottom_15">
                                            <br>
                                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                <label class="col-md-4 control-label">Password</label>

                                                <div class="col-md-6">
                                                    <input type="password" class="form-control" name="password">

                                                    @if ($errors->has('password'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </li>

                                        <li class="m_bottom_15">
                                            <br>
                                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                                <label class="col-md-4 control-label">Confirm Password</label>
                                                <div class="col-md-6">
                                                    <input type="password" class="form-control" name="password_confirmation">

                                                    @if ($errors->has('password_confirmation'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </li>

                                        <li class="m_bottom_15">
                                            <br>
                                            <br>
                                            <div class="form-group">
                                                <div class="col-md-6 col-md-offset-4">
                                                    <button type="submit" class="button_type_4 bg_scheme_color btn-red">
                                                        <i class="fa fa-btn fa-refresh"></i>Reset Password
                                                    </button>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </form>
                            </div>
                        </section>
                    </div>

                </section>
                <!--right column-->
                <aside class="col-lg-3 col-md-3 col-sm-3">
     
                    <!--banner-->
                    <a href="#" class="d_block r_corners m_bottom_30">
                        <img src="{{asset('/template_resource/images/banner_img_6.jpg')}}" alt="">
                    </a>
                    <a href="#" class="d_block r_corners m_bottom_30">
                        <img src="{{asset('/template_resource/images/banner_img_6.jpg')}}" alt="">
                    </a>
                </aside>
            </div>
        </div>
    </div>
    <!--markup footer-->
    @include('web/partial/footer/newbottom')

</div>
<!--social widgets-->
@include('web/partial/social_widgets/main')

<button class="t_align_c r_corners tr_all_hover type_2 animate_ftl" id="go_to_top"><i class="fa fa-angle-up"></i>
</button>

@include('web/partial/script/core')
<script src="{{asset('/template_resource/js/scripts.js')}}"></script>
<script>

    function redirectToHome()
    {
        window.location.href = $('#baseUrl').val()+"/home";
    }

    function doRegister()
    {

        var validationFlag = true;

        if($("#first_name").val()=="")
        {
            validationFlag = false;
            $("#errmessage").html("First Name is required.");
            $("#first_name").focus();
        }
        else if($("#last_name").val()=="")
        {
            validationFlag = false;
            $("#errmessage").html("Last Name is required.");
            $("#last_name").focus();
        }

        else if($("#email").val()=="")
        {
            validationFlag = false;
            $("#errmessage").html("Email is required.");
            $("#email").focus();
        }

        else if($("#phone").val()=="")
        {
            validationFlag = false;
            $("#errmessage").html("Phone is required.");
            $("#phone").focus();
        }
        else if($("p_#password").val()=="")
        {
            validationFlag = false;
            $("#errmessage").html("Password is required.");
            $("#p_password").focus();
        }
        else{
            validationFlag = true;
        }


        if(validationFlag){
            $.ajax({

                url: $("#baseUrl").val()+"api/registration/register",
                method: "POST",
                data: {
                    "first_name": $("#first_name").val(),
                    "last_name": $("#last_name").val(),
                    "email": $("#email").val(),
                    "phone": $("#phone").val(),
                    "password": $("#p_password").val(),
                    "confirm_password": $("#confirm_password").val()
                },
                success: function (data) {
                    if (data.responseStat.status) {
                        $.ajax({
                        url: $("#baseUrl").val() + "login",
                        method: "POST",
                        data: {
                            "email": $("#email").val(),
                            "password": $("#p_password").val()
                        },
                        success: function (data) {
                             $("#errmessage").html(data.responseStat.msg);
                            var MytimeOut = setTimeout(redirectToHome,2000);
                            $('#msg').html(data.responseStat.msg);
                            if (data.responseStat.status && data.responseStat.isLogin) {
                                $('#msg').delay(1000).fadeOut(500, function () {
                                    $('#login_popup').hide();
                                    $('#userName').val(data.responseData.user.firstName);
                                    $('#loginText').hide();
                                    $('#loginName').html(data.responseData.user.firstName).show();
                                    $('#logoutText').show();
                                    afterLoginFunctionCall();
                                    $('#open_chat_anchor').attr('onclick', 'openChat(' + data['responseData']['user']['id'] + ')');
                                    //            $('#open_chat_anchor').prop("onclick", false);
                                    //            document.getElementById('open_chat_anchor').onclick = function () { openChat(data['responseData']['id']) };
                                    //            console.log($('#open_chat_anchor').html());
                                                socket.emit('init', data['responseData']['user']['id']);

                                    //location.reload();
                                });
                            }
                            else {
                                $('#username').prop('disabled', false);
                                $('#password').prop('disabled', false);
                            }
                        }
                    });
                       
                    }
                    else {
                        $("#errmessage").html(data.responseStat.msg);
                        console.log(data);
                    }


                }

            });
        }else{
            console.log("Error in user input");
        }

    }
</script>

</body>
</html>