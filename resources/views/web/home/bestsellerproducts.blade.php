<!doctype html>
<!--[if IE 9 ]><html class="ie9" lang="en" ng-app="mallBd"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" ng-app="mallBd"><!--<![endif]-->

@include('web/partial/header/mainStart')
@include('web/partial/header/cartScript')
@include('web/partial/header/mainEnds')

<body ng-controller="CartController" >
    @include('web/partial/loader')
<!--wide layout-->
<div class="wide_layout relative">
<!--[if (lt IE 9) | IE 9]>
<div style="background:#fff;padding:8px 0 10px;">
    <div class="container" style="width:1170px;">
        <div class="row wrapper">
            <div class="clearfix" style="padding:9px 0 0;float:left;width:83%;"><i
                    class="fa fa-exclamation-triangle scheme_color f_left m_right_10"
                    style="font-size:25px;color:#e74c3c;"></i><b style="color:#e74c3c;">Attention! This page may not
                display correctly.</b> <b>You are using an outdated version of Internet Explorer. For a faster, safer
                browsing experience.</b></div>
            <div class="t_align_r" style="float:left;width:16%;"><a
                    href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"
                    class="button_type_4 r_corners bg_scheme_color color_light d_inline_b t_align_c" target="_blank"
                    style="margin-bottom:2px;">Update Now!</a></div>
        </div>
    </div>
</div>
<![endif]-->
<!--markup header-->
<header role="banner" class="type_5 fixed-top">

    <!--header top part-->
@include('web/partial/header/top')

    <!--main menu container-->
@include('web/partial/new_menu')
</header>

<div class="other-free-gap">
</div>

    <?php
            $linkStr ="";
            $count=1;
        ?>
@include('web/partial/sideblock/shippingtext')
<!--breadcrumbs-->
<section class="breadcrumbs">
    <div class="container">
        <ul class="horizontal_list clearfix bc_list f_size_medium">
            <li class="m_right_10"><a href="{{url('')}}" class="default_t_color">Home<i
                    class="fa fa-angle-right d_inline_middle m_left_10"></i></a></li>
            <li class="m_right_10"><a href="#" class="default_t_color">Best Sellers Products</a></li>
        </ul>
    </div>
</section>
<!--content-->
<div class="page_content_offset">
<div class="container">
<div class="row clearfix">
<aside class="col-lg-3 col-md-3 col-sm-3">
<!--widgets-->
@include('web/partial/sideblock/manufacturer')
<!--<figure class="widget shadow r_corners wrapper m_bottom_30">
    <figcaption>
        <h3 class="color_light">Categories</h3>
    </figcaption>
    <div class="widget_content">
        Categories list
         @foreach($subcategoryList as $category)
        <ul class="categories_list">
            <li class="active">
                <a href="{{url("category/$category->id")}}" class="f_size_large scheme_color d_block relative">
                   <b>{{$category->title}}</b>
                    <span class="bg_light_color_1 r_corners f_right color_dark talign_c"></span>
                </a>
                second level
                @if(!empty(@$category->childrens))
                <ul>
                @foreach(@$category->childrens as $firstChild)
                    <li class="active">
                        <a href="{{url("category/$category->id/$firstChild->id")}}" class="d_block f_size_large color_dark relative">
                             {{$firstChild->title}}<span class="bg_light_color_1 r_corners f_right color_dark talign_c"></span>
                        </a>
                        third level
                         @if(!empty($firstChild->childrens))
                        <ul>
                          @foreach(@$firstChild->childrens as $secondChild)
                            <li>
                                <a href="{{url("category/$category->id/$firstChild->id/$secondChild->id")}}" class="color_dark d_block">
                                {{$secondChild->title}}</a>
                            </li>
                          @endforeach
                        </ul>
                        @endif
                    </li>
                     @endforeach
                </ul>
                @endif
            </li>
        </ul>
        @endforeach
    </div>
</figure>-->

<!--banner-->
<a href="#" class="d_block r_corners m_bottom_30">
    <img src="{{asset('/template_resource/images/banner_img_6.jpg')}}" alt="">
</a>
<!--Bestsellers-->
 @include('web/partial/sideblock/tags')
  @include('web/partial/sideblock/bestsellers')
<!--tags-->

</aside>
<!--left content column-->
<section class="col-lg-9 col-md-9 col-sm-9">
    @include('web/product/productlistview')
</section>
<!--right column-->
</div>
</div>
</div>
<!--markup footer-->
    @include('web/partial/footer/newbottom')
</div>
<!--social widgets-->
@include('web/partial/social_widgets/main')
<!--<ul class="social_widgets d_xs_none">
    facebook
    <li class="relative">
        <button class="sw_button t_align_c facebook"><i class="fa fa-facebook"></i></button>
        <div class="sw_content">
            <h3 class="color_dark m_bottom_20">Join Us on Facebook</h3>
            <iframe src="http://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fenvato&amp;width=235&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=438889712801266"
                    style="border:none; overflow:hidden; width:235px; height:258px;"></iframe>
        </div>
    </li>
    twitter feed
    <li class="relative">
        <button class="sw_button t_align_c twitter"><i class="fa fa-twitter"></i></button>
        <div class="sw_content">
            <h3 class="color_dark m_bottom_20">Latest Tweets</h3>

            <div class="twitterfeed m_bottom_25"></div>
            <a role="button" class="button_type_4 d_inline_b r_corners tr_all_hover color_light tw_color"
               href="https://twitter.com/fanfbmltemplate">Follow on Twitter</a>
        </div>
    </li>
    contact form
    <li class="relative">
        <button class="sw_button t_align_c contact"><i class="fa fa-envelope-o"></i></button>
        <div class="sw_content">
            <h3 class="color_dark m_bottom_20">Contact Us</h3>

            <p class="f_size_medium m_bottom_15">Lorem ipsum dolor sit amet, consectetuer adipis mauris</p>

            <form id="contactform" class="mini">
                <input class="f_size_medium m_bottom_10 r_corners full_width" type="text" name="cf_name"
                       placeholder="Your name">
                <input class="f_size_medium m_bottom_10 r_corners full_width" type="email" name="cf_email"
                       placeholder="Email">
                <textarea class="f_size_medium r_corners full_width m_bottom_20" placeholder="Message"
                          name="cf_message"></textarea>
                <button type="submit" class="button_type_4 r_corners mw_0 tr_all_hover color_dark bg_light_color_2">
                    Send
                </button>
            </form>
        </div>
    </li>
    contact info
    <li class="relative">
        <button class="sw_button t_align_c googlemap"><i class="fa fa-map-marker"></i></button>
        <div class="sw_content">
            <h3 class="color_dark m_bottom_20">Store Location</h3>
            <ul class="c_info_list">
                <li class="m_bottom_10">
                    <div class="clearfix m_bottom_15">
                        <i class="fa fa-map-marker f_left color_dark"></i>

                        <p class="contact_e">8901 Marmora Road,<br> Glasgow, D04 89GR.</p>
                    </div>
                    <iframe class="r_corners full_width" id="gmap_mini"
                            src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=ru&amp;geocode=&amp;q=Manhattan,+New+York,+NY,+United+States&amp;aq=0&amp;oq=monheten&amp;sll=37.0625,-95.677068&amp;sspn=65.430355,129.814453&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=%D0%9C%D0%B0%D0%BD%D1%85%D1%8D%D1%82%D1%82%D0%B5%D0%BD,+%D0%9D%D1%8C%D1%8E-%D0%99%D0%BE%D1%80%D0%BA,+%D0%9D%D1%8C%D1%8E+%D0%99%D0%BE%D1%80%D0%BA,+%D0%9D%D1%8C%D1%8E-%D0%99%D0%BE%D1%80%D0%BA&amp;ll=40.790278,-73.959722&amp;spn=0.015612,0.031693&amp;z=13&amp;output=embed"></iframe>
                </li>
                <li class="m_bottom_10">
                    <div class="clearfix m_bottom_10">
                        <i class="fa fa-phone f_left color_dark"></i>

                        <p class="contact_e">800-559-65-80</p>
                    </div>
                </li>
                <li class="m_bottom_10">
                    <div class="clearfix m_bottom_10">
                        <i class="fa fa-envelope f_left color_dark"></i>
                        <a class="contact_e default_t_color" href="mailto:#">info@companyname.com</a>
                    </div>
                </li>
                <li>
                    <div class="clearfix">
                        <i class="fa fa-clock-o f_left color_dark"></i>

                        <p class="contact_e">Monday - Friday: 08.00-20.00 <br>Saturday: 09.00-15.00<br> Sunday: closed
                        </p>
                    </div>
                </li>
            </ul>
        </div>
    </li>
</ul>-->
<!--custom popup-->
<div class="popup_wrap d_none" id="quick_view_product">
    <section class="popup r_corners shadow">
        <button class="bg_tr color_dark tr_all_hover text_cs_hover close f_size_large"><i class="fa fa-times"></i>
        </button>
        <div class="clearfix">
            <div class="custom_scrollbar">
                <!--left popup column-->
                <div class="f_left half_column">
                    <div class="relative d_inline_b m_bottom_10 qv_preview">
                        <span class="hot_stripe"><img src="{{asset('/template_resource/images/sale_product.png')}}" alt=""></span>
                        <img src="{{asset('/template_resource/images/quick_view_img_1.jpg')}}" class="tr_all_hover" alt="">
                    </div>
                    <!--carousel-->
                    <div class="relative qv_carousel_wrap m_bottom_20">
                        <button class="button_type_11 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_prev">
                            <i class="fa fa-angle-left "></i>
                        </button>
                        <ul class="qv_carousel d_inline_middle">
                            <li data-src="{{asset('/template_resource/images/quick_view_img_1.jpg')}}" ><img src="{{asset('/template_resource/images/quick_view_img_4.jpg')}}" alt="">
                            </li>
                            <li data-src="{{asset('/template_resource/images/quick_view_img_2.jpg')}}"><img src="{{asset('/template_resource/images/quick_view_img_5.jpg')}}" alt="">
                            </li>
                            <li data-src="{{asset('/template_resource/images/quick_view_img_3.jpg')}}"><img src="{{asset('/template_resource/images/quick_view_img_6.jpg')}}" alt="">
                            </li>
                            <li data-src="{{asset('/template_resource/images/quick_view_img_1.jpg')}}" ><img src="{{asset('/template_resource/images/quick_view_img_4.jpg')}}" alt="">
                            </li>
                            <li data-src="{{asset('/template_resource/images/quick_view_img_2.jpg')}}" ><img src="{{asset('/template_resource/images/quick_view_img_5.jpg')}}" alt="">
                            </li>
                            <li data-src="{{asset('/template_resource/images/quick_view_img_3.jpg')}}" ><img src="{{asset('/template_resource/images/quick_view_img_6.jpg')}}" alt="">
                            </li>
                        </ul>
                        <button class="button_type_11 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_next">
                            <i class="fa fa-angle-right "></i>
                        </button>
                    </div>
                    <div class="d_inline_middle">Share this:</div>
                    <div class="d_inline_middle m_left_5">
                        <!-- AddThis Button BEGIN -->
                        <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
                            <a class="addthis_button_preferred_1"></a>
                            <a class="addthis_button_preferred_2"></a>
                            <a class="addthis_button_preferred_3"></a>
                            <a class="addthis_button_preferred_4"></a>
                            <a class="addthis_button_compact"></a>
                            <a class="addthis_counter addthis_bubble_style"></a>
                        </div>
                        <!-- AddThis Button END -->
                    </div>
                </div>
                <!--right popup column-->
                <div class="f_right half_column">
                    <!--description-->
                    <h2 class="m_bottom_10"><a href="#" class="color_dark fw_medium">Eget elementum vel</a></h2>

                    <div class="m_bottom_10">
                        <!--rating-->
                        <ul class="horizontal_list d_inline_middle type_2 clearfix rating_list tr_all_hover">
                            <li class="active">
                                <i class="fa fa-star-o empty tr_all_hover"></i>
                                <i class="fa fa-star active tr_all_hover"></i>
                            </li>
                            <li class="active">
                                <i class="fa fa-star-o empty tr_all_hover"></i>
                                <i class="fa fa-star active tr_all_hover"></i>
                            </li>
                            <li class="active">
                                <i class="fa fa-star-o empty tr_all_hover"></i>
                                <i class="fa fa-star active tr_all_hover"></i>
                            </li>
                            <li class="active">
                                <i class="fa fa-star-o empty tr_all_hover"></i>
                                <i class="fa fa-star active tr_all_hover"></i>
                            </li>
                            <li>
                                <i class="fa fa-star-o empty tr_all_hover"></i>
                                <i class="fa fa-star active tr_all_hover"></i>
                            </li>
                        </ul>
                        <a href="#" class="d_inline_middle default_t_color f_size_small m_left_5">1 Review(s) </a>
                    </div>
                    <hr class="m_bottom_10 divider_type_3">
                    <table class="description_table m_bottom_10">
                        <tr>
                            <td>Manufacturer:</td>
                            <td><a href="#" class="color_dark">Chanel</a></td>
                        </tr>
                        <tr>
                            <td>Availability:</td>
                            <td><span class="color_green">in stock</span> 20 item(s)</td>
                        </tr>
                        <tr>
                            <td>Product Code:</td>
                            <td>PS06</td>
                        </tr>
                    </table>
                    <h5 class="fw_medium m_bottom_10">Product Dimensions and Weight</h5>
                    <table class="description_table m_bottom_5">
                        <tr>
                            <td>Product Length:</td>
                            <td><span class="color_dark">10.0000M</span></td>
                        </tr>
                        <tr>
                            <td>Product Weight:</td>
                            <td>10.0000KG</td>
                        </tr>
                    </table>
                    <hr class="divider_type_3 m_bottom_10">
                    <p class="m_bottom_10">Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean
                        auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Donec sit amet eros. Lorem ipsum
                        dolor sit amet, consecvtetuer adipiscing elit. </p>
                    <hr class="divider_type_3 m_bottom_15">
                    <div class="m_bottom_15">
                        <s class="v_align_b f_size_ex_large">$152.00</s><span
                            class="v_align_b f_size_big m_left_5 scheme_color fw_medium">$102.00</span>
                    </div>
                    <table class="description_table type_2 m_bottom_15">
                        <tr>
                            <td class="v_align_m">Size:</td>
                            <td class="v_align_m">
                                <div class="custom_select f_size_medium relative d_inline_middle">
                                    <div class="select_title r_corners relative color_dark">s</div>
                                    <ul class="select_list d_none"></ul>
                                    <select name="product_name">
                                        <option value="s">s</option>
                                        <option value="m">m</option>
                                        <option value="l">l</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="v_align_m">Quantity:</td>
                            <td class="v_align_m">
                                <div class="clearfix quantity r_corners d_inline_middle f_size_medium color_dark">
                                    <button class="bg_tr d_block f_left" data-direction="down">-</button>
                                    <input type="text" name="" readonly value="1" class="f_left">
                                    <button class="bg_tr d_block f_left" data-direction="up">+</button>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div class="clearfix m_bottom_15">
                        <button class="button_type_12 r_corners bg_scheme_color color_light tr_delay_hover f_left f_size_large">
                            Add to Basket
                        </button>
                        <button class="button_type_12 bg_light_color_2 tr_delay_hover f_left r_corners color_dark m_left_5 p_hr_0">
                            <i class="fa fa-heart-o f_size_big"></i><span
                                class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span></button>
                        <button class="button_type_12 bg_light_color_2 tr_delay_hover f_left r_corners color_dark m_left_5 p_hr_0">
                            <i class="fa fa-files-o f_size_big"></i><span
                                class="tooltip tr_all_hover r_corners color_dark f_size_small">Compare</span></button>
<!--                        <button class="button_type_12 bg_light_color_2 tr_delay_hover f_left r_corners color_dark m_left_5 p_hr_0 relative">
                            <i class="fa fa-question-circle f_size_big"></i><span
                                class="tooltip tr_all_hover r_corners color_dark f_size_small">Ask a Question</span>
                        </button>-->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!--login popup-->

<button class="t_align_c r_corners tr_all_hover type_2 animate_ftl" id="go_to_top"><i class="fa fa-angle-up"></i>
</button>

<!--scripts include-->
@include('web/partial/script/core')
<script src="{{asset('/developer/js/script/product_cat_scripts.js')}}"></script>
<!-- Hidden Values -->
<input id="minMaxPrice" type="hidden" value="{{json_encode($minMaxPrice)}}" />

<input type="hidden" id="productsAttributesJsonObject" value='{{json_encode($productsAttributes)}}' />
<?php //<input type="hidden" id="productsCategoryIdListsonObject" value='{{json_encode($categoryIdList)}}' /> ?>
<input type="hidden" id="selectedProductAttribute" value='{{json_encode($selectedProductAttribute)}}' />
<input type="hidden" id="selectedManufacturersIdListJsonObj" value='{{json_encode($selectedManufacturersIdList)}}' />
<input type="hidden" id="selectPriceObject" value='{{json_encode($selectPrice)}}' />
<input type="hidden" id="parentCategory" value='{{$parentCategoryId}}' />


</body>
</html>
<script>
function changeActionFilter(){
        var orderBy = $('#selectActionFilter').val();
        var myselect = document.getElementById("selectViewSize");
       // alert(myselect.options[myselect.selectedIndex].value);
        var searchParams = "";
        var urlParams = url('?');
        try{
            if(typeof urlParams != "undefined" && typeof urlParams.q != "undefined"){
                searchParams = "q="+encodeURIComponent(url('?').q)+"&";
            }
        }catch(ex){
            console.log(ex);
        }

        var value = myselect.options[myselect.selectedIndex].value;
        var locationUrl = window.location.origin + window.location.pathname;
        window.location = locationUrl+"?"+searchParams+"orderBy="+orderBy+"&limit="+value+"&page=0";

}
function changeAction(){
        var orderBy = $('#selectActionFilter').val();
        var myselect = document.getElementById("selectViewSize");
       // alert(myselect.options[myselect.selectedIndex].value);
        var searchParams = "";
        var urlParams = url('?');
        try{
            if(typeof urlParams != "undefined" && typeof urlParams.q != "undefined"){
                searchParams = "q="+encodeURIComponent(url('?').q)+"&";
            }
        }catch(ex){
            console.log(ex);
        }

        var value = myselect.options[myselect.selectedIndex].value;
        var locationUrl = window.location.origin + window.location.pathname;
        window.location = locationUrl+"?"+searchParams+"orderBy="+orderBy+"&limit="+value+"&page=0";

}
function changeAction2(){
        var orderBy = $('#selectActionFilter').val();
        var myselect = document.getElementById("selectViewSize2");
       // alert(myselect.options[myselect.selectedIndex].value);
        var searchParams = "";
        var urlParams = url('?');
        try{
            if(typeof urlParams != "undefined" && typeof urlParams.q != "undefined"){
                searchParams = "q="+encodeURIComponent(url('?').q)+"&";
            }
        }catch(ex){
            console.log(ex);
        }

        var value = myselect.options[myselect.selectedIndex].value;
        var locationUrl = window.location.origin + window.location.pathname;
        window.location = locationUrl+"?"+searchParams+"orderBy="+orderBy+"&limit="+value+"&page=0";

}
function specificPage(page){
    var orderBy = $('#selectActionFilter').val();
    var myselect = document.getElementById("selectViewSize");
    var searchParams = "";
        var urlParams = url('?');
        try{
            if(typeof urlParams != "undefined" && typeof urlParams.q != "undefined"){
                searchParams = "q="+encodeURIComponent(url('?').q)+"&";
            }
        }catch(ex){
            console.log(ex);
        }

        var value = myselect.options[myselect.selectedIndex].value;
        var locationUrl = window.location.origin + window.location.pathname;
        window.location = locationUrl+"?"+searchParams+"orderBy="+orderBy+"&limit="+value+"&page="+page;
}
function nextPage(page){
    var orderBy = $('#selectActionFilter').val();
    if(!isNaN(page)){
        page = parseInt(page);
        page++;
    }else{
        return;
    }
    var lastPage = $('#paginationNumberUl').find('li').length-1;
    if(page>lastPage){
        return;
    }

    var myselect = document.getElementById("selectViewSize");
    var searchParams = "";
        var urlParams = url('?');
        try{
            if(typeof urlParams != "undefined" && typeof urlParams.q != "undefined"){
                searchParams = "q="+encodeURIComponent(url('?').q)+"&";
            }
        }catch(ex){
            console.log(ex);
        }

        var value = myselect.options[myselect.selectedIndex].value;
        var locationUrl = window.location.origin + window.location.pathname;
        window.location = locationUrl+"?"+searchParams+"orderBy="+orderBy+"&limit="+value+"&page="+page;
}
function prevPage(page){
    var orderBy = $('#selectActionFilter').val();
    if(!isNaN(page)){
        page = parseInt(page);
        page--;
    }else{
        return;
    }
    var lastPage = $('#paginationNumberUl').find('li').length-1;
    if(page>lastPage){
        return;
    }

    var myselect = document.getElementById("selectViewSize");
    var searchParams = "";
        var urlParams = url('?');
        try{
            if(typeof urlParams != "undefined" && typeof urlParams.q != "undefined"){
                searchParams = "q="+encodeURIComponent(url('?').q)+"&";
            }
        }catch(ex){
            console.log(ex);
        }

        var value = myselect.options[myselect.selectedIndex].value;
        var locationUrl = window.location.origin + window.location.pathname;
        window.location = locationUrl+"?"+searchParams+"orderBy="+orderBy+"&limit="+value+"&page="+page;
}
function getSearchAttributeSelection(){
    var obj = getProductsAttributes();
    var searchAttr = [];
    for(var k of obj){
        var  attrName = k.name.toLowerCase();
        console.log('input[name='+attrName+'_attr]:checked');

        if(attrName == "color"){
            $(".select_color").each(function(){
               if($(this).hasClass("active")) {
                   try {
                       searchAttr.push(parseInt($(this).attr("productAttrId")));
                   } catch (err){

                       console.log(err);
                   }
               }

            });
        }else if($('input[name='+attrName+'_attr]').is(":checked")){
            try {
                var attrval = $('input[name='+attrName+'_attr]:checked').val();
                searchAttr.push(parseInt(attrval));

            } catch (err){

                console.log(err);
            }
        }
    }

    return searchAttr;

}
function reset_filter(){
   // alert('test');
    var current_url=window.location.href;
    var split=current_url.split("?q=");
    window.location.assign(split[0]);

}
function searchProduct(){
    var orderBy = (typeof $('#selectActionFilter').val() !="undefined")? $('#selectActionFilter').val():0;
    var searchParamObj = {"pa":[]};
    var selectPriceObject = new SelectPriceObject();
    var manufacturersIdList = getManufacturersIdList();

    if(selectPriceObject.isPriceSelectionChanged()){
        searchParamObj['price'] = selectPriceObject.getSelectPriceObject();
    }
    if( manufacturersIdList.length > 0){
        searchParamObj['manufacturers'] =manufacturersIdList;
    }

    searchParamObj.pa = getSearchAttributeSelection();

    var searchParams =(
                        searchParamObj.pa.length > 0
                        || typeof searchParamObj['price'] != "undefined"
                        || manufacturersIdList.length > 0
                      )?"?q="+encodeURIComponent(JSON.stringify(searchParamObj)):"";
    var viewSize = (typeof $("#selectViewSize").val() !="undefined")? $("#selectViewSize").val():40;
    searchParams+=(searchParams!="")?"&orderBy="+orderBy+"&limit="+viewSize+"&page=0":"";
    window.location = location.protocol + '//' + location.host + location.pathname+searchParams;
}
function getProductsAttributes(){
    return JSON.parse(document.getElementById('productsAttributesJsonObject').value);
}
function getProductsCategoryIdList(){
    return JSON.parse(document.getElementById('productsCategoryIdListsonObject').value);
}
function getManufacturersIdList(){
    var manufacturerIdList = [];
    $(".ManufacturersCls:checked").each(function(){
        try {
            manufacturerIdList.push(parseInt($(this).val()));
        }catch(ex){
            console.log(ex);
        }
    });
    return manufacturerIdList;
}
function getSelectedManufacturersIdList(){
    return JSON.parse($("#selectedManufacturersIdListJsonObj").val());
}


function changePicture(picName,count)
{
    srcOfZoom = $("#zoom_image"+count).attr('src');
    parts = srcOfZoom.split("/");

    newSource = (parts[0]+"/"+parts[1]+"/"+parts[2]+"/"+parts[3]+"/"+parts[4]+"/"+parts[5]+"/"+parts[6]+"/"+parts[7]+"/"+picName);

    console.log(newSource);
    $("#zoom_image"+count).attr("src",newSource);

}
    $(document).ready(function(){
       var parentCategory = $('#parentCategory').val();
       console.log( $("#parent_id_"+parentCategory).addClass("current"));
    });
</script>
