<!doctype html>


<!--[if IE 9 ]><html class="ie9" lang="en" ng-app="mallBd"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" ng-app="mallBd"  ><!--<![endif]-->
<head>
    <title>Mall BD</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!--meta info-->
    <meta name="author" content="">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="icon" type="image/ico" href="{{asset('/template_resource/images/fav.ico')}}">
    <!--stylesheet include-->
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('/template_resource/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('/template_resource/css/settings.css')}}">
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('/template_resource/css/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('/template_resource/css/owl.transitions.css')}}">
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('/template_resource/css/jquery.custom-scrollbar.css')}}">
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('/template_resource/css/style.css')}}">
    <!--font include-->
    <link href="{{asset('/template_resource/css/font-awesome.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('/template_resource/css/fa-snapchat.css')}}">

    <script type="text/javascript" src="{{asset('/developer/angular/corelib/angular.min.js')}}"></script>
    <!--<script type="text/javascript" src="{{asset('/developer/angular/corelib/lazy-scroll.min.js')}}"></script>-->
    <script type="text/javascript" src="{{asset('developer/angular/helper/HtmlManipulationModel.js')}}"></script>
    <script type="text/javascript" src="{{asset('developer/angular/datamodel/datamodel.js')}}"></script>
    <script type="text/javascript" src="{{asset('developer/angular/controllers/cart/main.js')}}"></script>
    
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('developer/autocomplete/css/easy-autocomplete.css')}}">
<link rel="stylesheet" type="text/css" media="all" href="{{asset('developer/autocomplete/css/easy-autocomplete.themes.css')}}">
</head>

<?php $cls="";?>

<body ng-controller="CartController" >
<!--boxed layout-->
<div class="wide_layout relative w_xs_auto clearfix">
   
    <!--markup header-->
    <header role="banner" class="type_5 fixed-top">
        <!--header top part-->
        <?php
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
?>

<section id="topSection" class="h_bot_part header-most-top container header-other-page hidden-xs">
    <div class="clearfix container">
        <div class="row clearfix">
            <div class="col-lg-6 col-md-6 col-sm-4 t_xs_align_c">
                <a href="{{url('/')}}" class="logo m_xs_bottom_15 d_xs_inline_b">
                    <img width="200" src="{{asset('/template_resource/images/logo.png')}}" alt="">
                </a>
            </div>

            <?php if(Auth::check()){
                $loginDisplayType = "hidden";
            }else{
                $loginDisplayType = "";
            }
            ?>

            <?php if(!Auth::check()){
                $logoutDisplayType = "hidden";
            }else{
                $logoutDisplayType = "";
            }
            ?>

            <div class="col-lg-6 col-md-6 col-sm-8 t_align_r t_xs_align_c top-right-cart-box">
                <ul class="d_inline_b horizontal_list clearfix f_size_small users_nav user_nav_white">

                    <li>
                        <p class="default_t_color" id="loginText" {{ $loginDisplayType }}><a id="loginFormViewer" href="#" data-popup="#login_popup"> Log In</a> </p>
                            <p class="default_t_color" id="loginName" {{$logoutDisplayType}}>{{ $appCredential->user->firstName }}</p>
                    </li>

                    <li><a href="{{ url('/myaccount/info') }}" class="default_t_color">My Account</a></li>

                    <li><a href="{{ url('/checkout') }}" class="default_t_color">Checkout</a></li>




                    <li id="logoutText" {{ $logoutDisplayType }}><a class="default_t_color" href="javascript:void(0)" onclick="doLogout()">Logout</a></li>



                </ul>
                <ul class="d_inline_b horizontal_list clearfix t_align_l site_settings top-wish-fix">
                    <!--like-->
                        <li>
                            <a role="button" href="{{url('customer/wishlist/get')}}"
                               class="button_type_1 color_dark d_block bg_light_color_1 r_corners tr_delay_hover box_s_none only-single"><i
                                        class="fa fa-heart-o f_size_ex_large top-cart-btn"></i>
                                @if($wishListCount>=0)
                                    <span class="count circle t_align_c" id="wishListCount">{{$wishListCount}}</span>
                                @endif
                            </a>
                        </li>

<!--                    <li class="m_left_5">
                        <a role="button" href="#"
                           class="button_type_1 color_dark d_block bg_light_color_1 r_corners tr_delay_hover box_s_none">
                            <i class="fa fa-files-o f_size_ex_large"></i>
                            <span class="count circle t_align_c">3</span></a>
                    </li>-->
                    
                    <!-- Developer : Hidden Value -->
                    <input type="hidden" id="imageUrl" value="{{$imageUrl}}" />
                    <input type="hidden" id="baseUrl" value="{{url('/')}}/" />
                    <input type="hidden" id="isLogin" value="{{json_encode($isLogin)}}" />
                    <input type="hidden" id="userName" value=""/>
                    <!--language settings-->
                    <!--shopping cart-->
                    @include('web/partial/cart/main')
                    @include('web/partial/cart/popup')

                    <?php if(isset($redirectMainPage)){ ?>
                        @include('web/partial/login/loginPopupRedirectMain')
                    <?php }else{ ?>
                        @include('web/partial/login/loginPopup')
                    <?php } ?>
                    
                    @include('web/partial/chat/main')
                </ul>
            </div>
        </div>
    </div>
</section>



                <!--header bottom part-->
      
<section class="h_bot_part hidden-xs">
    <div class="menu_wrap">
        <div class="container custom-head-wide">
            <div class="clearfix row">
                <!--<div class="col-lg-2 t_md_align_c m_md_bottom_15">-->
                    <!--<a href="" class="logo d_md_inline_b">-->
                        <!--<img src="images/logo.png" alt="">-->
                    <!--</a>-->
                <!--</div>-->
                <div class="col-lg-12 clearfix t_sm_align_c">
                    <div class="clearfix t_sm_align_l f_left f_sm_none relative s_form_wrap m_sm_bottom_15 p_xs_hr_0 m_xs_bottom_5" style="padding-right: 0px;">
                        <!--button for responsive menu-->
                        <button id="menu_button"
                                class="r_corners centered_db d_none tr_all_hover d_xs_block m_xs_bottom_5">
                            <span class="centered_db r_corners"></span>
                            <span class="centered_db r_corners"></span>
                            <span class="centered_db r_corners"></span>
                        </button>
                        <!--main menu-->
                        <nav role="navigation" class="f_left f_xs_none d_xs_none m_right_35 m_md_right_30 m_sm_right_0">
                            <ul class="horizontal_list main_menu type_2 clearfix">
                                @foreach($categoryList as $rowData)
                                    <li class="relative f_xs_none m_xs_bottom_5 m_left_10 m_xs_left_0" id="parent_id_{{$rowData->id}}">
                                        <a href="{{url("category/".urlencode($rowData->title)."/$rowData->id")}}" class="tr_delay_hover color_dark tt_uppercase r_corners"><b>{{$rowData->title}}</b></a>

                                    @if(!empty(@$rowData->childrens))
                                    <!--sub menu-->
                                        <div class="sub_menu_wrap top_arrow d_xs_none tr_all_hover clearfix r_corners w_xs_auto">
                                        @foreach(@$rowData->childrens as $child)
                                        <div class="f_left f_xs_none">
                                            @if(sizeof($child->childrens)>1)
                                            <b class="color_dark m_left_20 m_bottom_5 m_top_5 d_inline_b">
                                                <a class="color_dark tr_delay_hover" href="{{url("category/".urlencode($child->title)."/$rowData->id/$child->id")}}">{{$child->title}}</a>
                                            </b>
                                            @else
                                                <b class="color_dark m_left_20 m_bottom_5 m_top_5 d_inline_b">
                                                    <a class="color_dark tr_delay_hover" href="{{url("category/".urlencode($child->title)."/$rowData->id/$child->id")}}">{{$child->title}}</a>
                                                </b>
                                                <ul class="sub_menu first">
                                                    <li></li>
                                                </ul>
                                                @endif
                                             @foreach(@$child->childrens as $child2)
                                            <ul class="sub_menu first">
                                                <li><a class="color_dark tr_delay_hover" href="{{url("category/".urlencode($child2->title)."/$rowData->id/$child->id/$child2->id")}}">{{@$child2->title}}</a>
                                                </li>
                                            </ul>
                                            @endforeach
                                        </div>
                                        @endforeach

                                    </div>
                               @endif
                               </li>
                               @endforeach

                                <!--<li class="relative f_xs_none m_xs_bottom_5 m_left_10 m_xs_left_0"><a href="#" class="tr_delay_hover color_dark tt_uppercase r_corners"><b>Contact</b></a></li>-->
                                <!--<li class="relative f_xs_none m_xs_bottom_5 m_left_10 m_xs_left_0"><a href="#" class="tr_delay_hover color_dark tt_uppercase r_corners"><b>Contact</b></a></li>-->
                            </ul>
                        </nav>
                        <button class="f_right search_button tr_all_hover f_xs_none d_xs_none">
                            <i class="fa fa-search"></i>&nbsp;&nbsp;SEARCH
                        </button>
                        <!--search form-->
                        <div class="searchform_wrap type_2 bg_tr tf_xs_none tr_all_hover w_inherit">
                            <div class="container vc_child h_inherit relative w_inherit">
                                <form role="search" class="d_inline_middle full_width">
                                    <input id="searchProduct" type="text" name="search" placeholder="Type text and hit enter" class="f_size_large" onkeypress="doSearch(event)">
                                </form>
                                <button class="close_search_form tr_all_hover d_xs_none color_dark">
                                    <i class="fa fa-times"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
                
<!--Mobile menu section-->
<div class="top-bar-mobile visible-xs">
    <ul class="mobile-top-menu">
        <li><a href="#"><i class="fa fa-key"></i></a></li>
        <li><a href="#"><i class="fa fa-user"></i></a></li>
        <li><a href="#"><i class="fa fa-sign-out"></i></a></li>
        <li>
            <a href="#"><i class="fa fa-heart-o"></i></a>
            <span class="number-counter">12</span>
        </li>
        <li>
            <a href="#"><i class="fa fa-shopping-cart"></i></a>
            <span class="number-counter">12</span>
        </li>
    </ul>
</div>
<nav class="navbar navbar-default nav-mobile hidden-lg hidden-md" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigationbar" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
        <a href="#" class="search-icon-mobile" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search"></i></a>
        
        <a href="{{url('/')}}" class="navbar-brand mobile-logo">
                      <img width="190" src="{{asset('/template_resource/images/logo.png')}}" alt="">
        </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="navigationbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
        <li><a href="#">Link</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<!--Mobile menu section-->
                
                
</header>
    
    <div id="carousel-example-generic" class="carousel slide main-carousel" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
              <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
              <li data-target="#carousel-example-generic" data-slide-to="5"></li>
              <li data-target="#carousel-example-generic" data-slide-to="7"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active" style="background: url('http://placehold.it/1920x600');">
                <img src="http://placehold.it/1920x600">
              </div>
              <div class="item" style="background: url('http://placehold.it/1920x600');">
                <img src="http://placehold.it/1920x600">
              </div>
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
              <i class="fa fa-chevron-left" aria-hidden="true"></i>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
              <i class="fa fa-chevron-right" aria-hidden="true"></i>
              <span class="sr-only">Next</span>
            </a>
    </div>
    
    
    <section class="free-shipping">
        <p><i class="fa fa-truck"></i>FREE SHIPPING on 1000tk Purchase inside Dhaka And 2000tk Purchase Outside Dhaka</p>
    </section>
    <div class="position-relative heading-styler clearfix">
        <h3 class="main-title">Product Brands</h3>
        <h2 class="main-small-title">Brands</h2>
    </div>
    
<!--    thumb carousel-->

<div class="container">
<!--product brands-->
<div class="m_bottom_25 m_sm_bottom_20 relative">
    <div class="f_right clearfix nav_buttons_wrap f_mxs_none">
        <button  class="button_type_7 left-arr pb_prev pb_prev_new">
            <i class="fa fa-angle-left"></i></button>
        <button  class="button_type_7 right-arr pb_next pb_next_new">
            <i class="fa fa-angle-right"></i></button>
    </div>
</div>
<!--product brands carousel-->
<div class="product_brands m_bottom_45 m_sm_bottom_35">
    <a href="#" class="d_block t_align_c"><img src="http://placehold.it/160x120" alt=""></a>
    <a href="#" class="d_block t_align_c"><img src="http://placehold.it/160x120" alt=""></a>
    <a href="#" class="d_block t_align_c"><img src="http://placehold.it/160x120" alt=""></a>
    <a href="#" class="d_block t_align_c"><img src="http://placehold.it/160x120" alt=""></a>
    <a href="#" class="d_block t_align_c"><img src="http://placehold.it/160x120" alt=""></a>
    <a href="#" class="d_block t_align_c"><img src="http://placehold.it/160x120" alt=""></a>
    <a href="#" class="d_block t_align_c"><img src="http://placehold.it/160x120" alt=""></a>
    <a href="#" class="d_block t_align_c"><img src="http://placehold.it/160x120" alt=""></a>
    <a href="#" class="d_block t_align_c"><img src="http://placehold.it/160x120" alt=""></a>
    <a href="#" class="d_block t_align_c"><img src="http://placehold.it/160x120" alt=""></a>
    <a href="#" class="d_block t_align_c"><img src="http://placehold.it/160x120" alt=""></a>
    <a href="#" class="d_block t_align_c"><img src="http://placehold.it/160x120" alt=""></a>
</div>
</div>
<!--    thumb carousel-->


<!--grid view and filter-->
<div class="container">
     <div class="position-relative heading-styler clearfix" style="margin-top: 20px;">
        <h3 class="main-title">Showcase</h3>
        <h2 class="main-small-title">All products</h2>
    </div>
<!--                <h2 class="tt_uppercase m_bottom_20 color_dark heading1 animate_ftr title-font"><span>All Products</span></h2>-->
</div>
<div class="container-fluid clearfix">
    <div class="row clearfix filter-new">
        <div class="container">
            <ul class="horizontal_list clearfix tt_uppercase isotope_menu f_size_ex_large filter-tabs">
                        <li class="active m_right_5 m_bottom_10 m_xs_bottom_5 animate_ftr animate_horizontal_finished">
                            <button class="button_type_2 bg_light_color_1 r_corners tr_delay_hover tt_uppercase box_s_none" data-filter="*">
                                All
                            </button>
                        </li>
                        <li class="m_right_5 m_bottom_10 m_xs_bottom_5 animate_ftr animate_horizontal_finished">
                            <button class="button_type_2 bg_light_color_1 r_corners tr_delay_hover tt_uppercase box_s_none" data-filter=".featured">Featured
                            </button>
                        </li>
                        <li class="m_right_5 m_bottom_10 m_xs_bottom_5 animate_ftr animate_horizontal_finished">
                            <button class="button_type_2 bg_light_color_1 r_corners tr_delay_hover tt_uppercase box_s_none" data-filter=".new">New
                            </button>
                        </li>
                        <li class="m_right_5 m_bottom_10 m_xs_bottom_5 animate_ftr animate_horizontal_finished">
                            <button class="button_type_2 bg_light_color_1 r_corners tr_delay_hover tt_uppercase box_s_none" data-filter=".specials">Specials
                            </button>
                        </li>
                        <li class="m_right_5 m_bottom_10 m_xs_bottom_5 animate_ftr animate_horizontal_finished">
                            <button class="button_type_2 bg_light_color_1 r_corners tr_delay_hover tt_uppercase box_s_none" data-filter=".package">Packages
                            </button>
                        </li>
        </ul>
        </div>
    </div>
</div>
<div class="container clearfix" style="min-height: 400px;">
    <div class="row clearfix">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="panel panel-default product-item">
                <div class="panel-heading">
                    <a href="#" class="relative prod-anchor">
                        <img src="http://placehold.it/242x242?text=No Image Found">
                        <span role="button" class="button_type_5 box_s_none color_light r_corners tr_all_hover d_xs_none new-qv">Quick View</span>
                    </a>
                </div>
                <div class="panel-body text-center">
                    <h5 class="prod_name_fix prod_name_new"><a href="#">Order test package</a></h5>
                    <p class="price-usual">$115.00</p>
                    <div class="clearfix position-relative prod-btn prod-btn-renew">
<!--                        <b style="color: red;">Out of Stock</b>-->
                        <button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light f_left mw_0 add-fix item-ad-btn" >Add to Basket</button>
                        <button class="active-wishlist button_type_4 bg_light_color_2 tr_all_hover f_right r_corners color_dark mw_0 m_left_5 p_hr_0 item-com-btn" ><i class="fa fa-files-o"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Compare</span></button>

                        <button class="active-wishlist button_type_4 bg_light_color_2 tr_all_hover f_right r_corners color_dark mw_0 p_hr_0 item-wish-btn"><i class="fa fa-heart-o"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span></button>
                        <div class="notify-small" hidden><span>Product Added</span></div>
                        <div class="notify-small"   hidden><span>Already Added</span></div>
                     </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="panel panel-default product-item">
                <div class="panel-heading">
                    <a href="#" class="relative prod-anchor">
                        <img src="http://placehold.it/242x242?text=No Image Found">
                        <span role="button" class="button_type_5 box_s_none color_light r_corners tr_all_hover d_xs_none new-qv">Quick View</span>
                    </a>
                </div>
                <div class="panel-body text-center">
                    <h5 class="prod_name_fix prod_name_new"><a href="#">Order test package</a></h5>
                    <p class="price-usual">$115.00</p>
                    <div class="clearfix position-relative prod-btn prod-btn-renew">
<!--                        <b style="color: red;">Out of Stock</b>-->
                        <button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light f_left mw_0 add-fix item-ad-btn" >Add to Basket</button>
                        <button class="active-wishlist button_type_4 bg_light_color_2 tr_all_hover f_right r_corners color_dark mw_0 m_left_5 p_hr_0 item-com-btn" ><i class="fa fa-files-o"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Compare</span></button>

                        <button class="active-wishlist button_type_4 bg_light_color_2 tr_all_hover f_right r_corners color_dark mw_0 p_hr_0 item-wish-btn"><i class="fa fa-heart-o"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span></button>
                        <div class="notify-small" hidden><span>Product Added</span></div>
                        <div class="notify-small"   hidden><span>Already Added</span></div>
                     </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="panel panel-default product-item">
                <div class="panel-heading">
                    <a href="#" class="relative prod-anchor">
                        <img src="http://placehold.it/242x242?text=No Image Found">
                        <span role="button" class="button_type_5 box_s_none color_light r_corners tr_all_hover d_xs_none new-qv">Quick View</span>
                    </a>
                </div>
                <div class="panel-body text-center">
                    <h5 class="prod_name_fix prod_name_new"><a href="#">Order test package</a></h5>
                    <p class="price-usual">$115.00</p>
                    <div class="clearfix position-relative prod-btn prod-btn-renew">
<!--                        <b style="color: red;">Out of Stock</b>-->
                        <button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light f_left mw_0 add-fix item-ad-btn" >Add to Basket</button>
                        <button class="active-wishlist button_type_4 bg_light_color_2 tr_all_hover f_right r_corners color_dark mw_0 m_left_5 p_hr_0 item-com-btn" ><i class="fa fa-files-o"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Compare</span></button>

                        <button class="active-wishlist button_type_4 bg_light_color_2 tr_all_hover f_right r_corners color_dark mw_0 p_hr_0 item-wish-btn"><i class="fa fa-heart-o"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span></button>
                        <div class="notify-small" hidden><span>Product Added</span></div>
                        <div class="notify-small"   hidden><span>Already Added</span></div>
                     </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="panel panel-default product-item">
                <div class="panel-heading">
                    <a href="#" class="relative prod-anchor">
                        <img src="http://placehold.it/242x242?text=No Image Found">
                        <span role="button" class="button_type_5 box_s_none color_light r_corners tr_all_hover d_xs_none new-qv">Quick View</span>
                    </a>
                </div>
                <div class="panel-body text-center">
                    <h5 class="prod_name_fix prod_name_new"><a href="#">Order test package</a></h5>
                    <p class="price-usual">$115.00</p>
                    <div class="clearfix position-relative prod-btn prod-btn-renew">
<!--                        <b style="color: red;">Out of Stock</b>-->
                        <button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light f_left mw_0 add-fix item-ad-btn" >Add to Basket</button>
                        <button class="active-wishlist button_type_4 bg_light_color_2 tr_all_hover f_right r_corners color_dark mw_0 m_left_5 p_hr_0 item-com-btn" ><i class="fa fa-files-o"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Compare</span></button>

                        <button class="active-wishlist button_type_4 bg_light_color_2 tr_all_hover f_right r_corners color_dark mw_0 p_hr_0 item-wish-btn"><i class="fa fa-heart-o"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span></button>
                        <div class="notify-small" hidden><span>Product Added</span></div>
                        <div class="notify-small"   hidden><span>Already Added</span></div>
                     </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="panel panel-default product-item">
                <div class="panel-heading">
                    <a href="#" class="relative prod-anchor">
                        <img src="http://placehold.it/242x242?text=No Image Found">
                        <span role="button" class="button_type_5 box_s_none color_light r_corners tr_all_hover d_xs_none new-qv">Quick View</span>
                    </a>
                </div>
                <div class="panel-body text-center">
                    <h5 class="prod_name_fix prod_name_new"><a href="#">Order test package</a></h5>
                    <p class="price-usual">$115.00</p>
                    <div class="clearfix position-relative prod-btn prod-btn-renew">
<!--                        <b style="color: red;">Out of Stock</b>-->
                        <button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light f_left mw_0 add-fix item-ad-btn" >Add to Basket</button>
                        <button class="active-wishlist button_type_4 bg_light_color_2 tr_all_hover f_right r_corners color_dark mw_0 m_left_5 p_hr_0 item-com-btn" ><i class="fa fa-files-o"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Compare</span></button>

                        <button class="active-wishlist button_type_4 bg_light_color_2 tr_all_hover f_right r_corners color_dark mw_0 p_hr_0 item-wish-btn"><i class="fa fa-heart-o"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span></button>
                        <div class="notify-small" hidden><span>Product Added</span></div>
                        <div class="notify-small"   hidden><span>Already Added</span></div>
                     </div>
                </div>
            </div>
        </div>
        
    </div>
</div>


<!--Common sections-->




<!--Common sections-->

<!--grid view and filter-->

</div>


    
<footer id="footer">
    <div class="footer_top_part">
        <div class="container">
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-3 m_xs_bottom_30">
                    <h3 class="color_light_2 m_bottom_20">Office Address</h3>

                    <p class="m_bottom_25">
                        The Mall Dhaka Office, House 6, Road 17/A, Block-E, Banani Dhaka Bangladesh<br>
                        +88 01977300901<br> 01977300902<br>
                        info@themallbd.com
                    </p>
                    <!--social icons-->
                    <ul class="clearfix horizontal_list social_icons">
                        <li class="facebook m_bottom_5 relative">
                            <span class="tooltip tr_all_hover r_corners color_dark f_size_small">Facebook</span>
                            <a target="_blank" href="https://www.facebook.com/themallbd" class="r_corners t_align_c tr_delay_hover f_size_ex_large">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li class="twitter m_left_5 m_bottom_5 relative">
                            <span class="tooltip tr_all_hover r_corners color_dark f_size_small">Twitter</span>
                            <a target="_blank" href="https://twitter.com/mallbd" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li class="google_plus m_left_5 m_bottom_5 relative">
                            <span class="tooltip tr_all_hover r_corners color_dark f_size_small">Google Plus</span>
                            <a target="_blank" href="https://plus.google.com/115620681186065872742 " class="r_corners f_size_ex_large t_align_c tr_delay_hover">
                                <i class="fa fa-google-plus"></i>
                            </a>
                        </li>
                        <li class="pinterest m_left_5 m_bottom_5 relative">
                            <span class="tooltip tr_all_hover r_corners color_dark f_size_small">Pinterest</span>
                            <a target="_blank" href="https://www.pinterest.com/themallbd/ " class="r_corners f_size_ex_large t_align_c tr_delay_hover">
                                <i class="fa fa-pinterest"></i>
                            </a>
                        </li>
                        <li class="instagram m_left_5 m_bottom_5 relative">
                            <span class="tooltip tr_all_hover r_corners color_dark f_size_small">Instagram</span>
                            <a target="_blank"  href="https://www.instagram.com/themallbd " class="r_corners f_size_ex_large t_align_c tr_delay_hover">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                        <li class="linkedin m_bottom_5 m_sm_left_5 relative">
                            <span class="tooltip tr_all_hover r_corners color_dark f_size_small">LinkedIn</span>
                            <a target="_blank" href="https://www.linkedin.com/company/10858584 " class="r_corners f_size_ex_large t_align_c tr_delay_hover">
                                <i class="fa fa-linkedin"></i>
                            </a>
                        </li>
                        <li class="youtube m_left_5 m_bottom_5 relative">
                            <span class="tooltip tr_all_hover r_corners color_dark f_size_small">Tumblr</span>
                            <a target="_blank" href="https://www.tumblr.com/blog/themallbd" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
                                <i class="fa fa-tumblr"></i>
                            </a>
                        </li>
                        <li class="youtube m_left_5 m_bottom_5 relative">
                            <span class="tooltip tr_all_hover r_corners color_dark f_size_small">Youtube</span>
                            <a target="_blank" href="https://www.youtube.com/channel/UCasyoiG4ZXRJ0cHfVVb5uWg" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
                                <i class="fa fa-youtube-play"></i>
                            </a>
                        </li>
                        <li class="youtube m_left_5 m_bottom_5 relative">
                            <span class="tooltip tr_all_hover r_corners color_dark f_size_small">Spapchat</span>
                            <a target="_blank" href="https://snapchat.com/add/themallbd" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
                              
                                <i class="fa fa-snapchat"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 m_xs_bottom_30">
                    <h3 class="color_light_2 m_bottom_20">The Service</h3>
                    <ul class="vertical_list">
                        <li><a class="color_light tr_delay_hover" href="{{ url('manufacturer') }}">Brands<i class="fa fa-angle-right"></i></a></li>
                        <li><a class="color_light tr_delay_hover" href="{{ url('bestsellerproducts') }}">Best Sellers<i class="fa fa-angle-right"></i></a></li>
                        <li><a class="color_light tr_delay_hover" href="{{ url('newproducts') }}">New Products<i class="fa fa-angle-right"></i></a></li>
                        <li><a class="color_light tr_delay_hover" href="{{ url('featuredproducts') }}">Featured Products<i class="fa fa-angle-right"></i></a></li>
                        <li><a class="color_light tr_delay_hover" href="{{ url('specialproducts') }}">Special Discount Products<i class="fa fa-angle-right"></i></a></li>
                        <li><a class="color_light tr_delay_hover" href="{{ url('packagelist') }}">Current Packages<i class="fa fa-angle-right"></i></a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 m_xs_bottom_30">
                    <h3 class="color_light_2 m_bottom_20">Information</h3>
                    <ul class="vertical_list">
                        @foreach($staticPages as $page)
                        <li><a class="color_light tr_delay_hover" href="{{url('page/')}}/{{$page['page_url']}}">{{$page['page_name']}}<i class="fa fa-angle-right"></i></a></li>
                        @endforeach
<!--                        <li><a class="color_light tr_delay_hover" href="{{url('page/aboutus')}}">About us</a></li>
                        <li><a class="color_light tr_delay_hover" href="{{ url('/page/privacypolicy') }}">Privacy policy<i class="fa fa-angle-right"></i></a></li>
                        <li><a class="color_light tr_delay_hover" href="{{ url('/page/terms') }}">Terms &amp; condition<i class="fa fa-angle-right"></i></a></li>
                        <li><a class="color_light tr_delay_hover" href="{{ url('/page/privacypolicy') }}">Delivery Info<i class="fa fa-angle-right"></i></a></li>
                        <li><a class="color_light tr_delay_hover" href="{{ url('/page/paymentinfo') }}">Payment Info<i class="fa fa-angle-right"></i></a></li>
                        <li><a class="color_light tr_delay_hover" href="{{ url('/page/voucher') }}">Voucher Card Rules and regulations <i class="fa fa-angle-right"></i></a></li>-->
                    </ul>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <h3 class="color_light_2 m_bottom_20">Newsletter</h3>

                    <p class="f_size_medium m_bottom_15">Sign up to our newsletter and get exclusive deals you wont find
                        anywhere else straight to your inbox!</p>
                    <span align="center" style="color: yellow;" id="newsalert" hidden></span>
                    <form id="newsletter" >
                        <input type="email" placeholder="Your email address"
                               class="m_bottom_20 r_corners f_size_medium full_width" name="newsletter-email" id="newsletteremail">
                        <button type="submit" class="button_type_8 r_corners bg_scheme_color color_light tr_all_hover" onclick="subscribe()">
                            Subscribe
                        </button>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
    <!--copyright part-->
    <div class="footer_bottom_part">
        <div class="container clearfix t_mxs_align_c">
            <p class="f_left f_mxs_none m_mxs_bottom_10">&copy; 2016 <span ><a href="http://themallbd.com/" class="color_light">The Mall BD</a></span>. All
                Rights Reserved.</p>
            <ul class="f_right horizontal_list clearfix f_mxs_none d_mxs_inline_b">
                <li><img src="{{asset('/template_resource/images/payment_img_1.png')}}" alt=""></li>
                <li class="m_left_5"><img src="{{asset('/template_resource/images/payment_img_2.png')}}" alt=""></li>
                <li class="m_left_5"><img src="{{asset('/template_resource/images/payment_img_3.png')}}" alt=""></li>
                <li class="m_left_5"><img src="{{asset('/template_resource/images/payment_img_4.png')}}" alt=""></li>
                <li class="m_left_5"><img src="{{asset('/template_resource/images/payment_img_5.png')}}" alt=""></li>
            </ul>
        </div>
    </div>
</footer>


        
<!--        search modal-->
        <div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Search Our Products</h4>
              </div>
              <div class="modal-body">
                  <form role="search" class="d_inline_middle full_width">
                      <input id="searchProduct" type="text" name="search" placeholder="Type text and hit enter" class="f_size_large" onkeypress="doSearch(event)">
                  </form>
              </div>
            </div>
          </div>
        </div>

<!--        search modal-->
    
<script>

    function subscribe()
    {
        $.ajax({
            url: $("#baseUrl").val()+"api/newsletter/subscribe",
            method: "POST",
            data: {
                "email": $('#newsletteremail').val()
            },
            success: function (data) {
                console.log(data);
                if(data.responseStat.status) {
                    $('#newsalert').html(data.responseStat.msg);
                    $('#newsalert').show();
                    $('#newsalert').fadeOut(2000);
                }
                else{
                    $('#newsalert').html(data.responseStat.msg);
                    $('#newsalert').fadeIn("slow");
                    $('#newsalert').fadeOut(2000);

                }
            }
        });
    }
</script>
</div>




<!-- Developer : Hidden Value -->
<input type="hidden" id="baseUrl" value="{{url('/')}}/" />
<input type="hidden" id="imageUrl" value="{{$imageUrl}}" />
<input type="hidden" id="isLogin" value="{{json_encode($isLogin)}}" />
<!--scripts include-->
<script src="{{asset('/template_resource/js/jquery-2.1.0.min.js')}}"></script>
<script src="{{asset('/template_resource/js/bootstrap.min.js')}}"></script>
<script src="{{asset('/template_resource/js/jquery-ui.min.js')}}"></script>
<!--<script src="{{asset('/template_resource/js/retina.js')}}"></script>
<script src="{{asset('/template_resource/js/waypoints.min.js')}}"></script>-->
<!--<script src="{{asset('/template_resource/js/jquery.isotope.min.js')}}"></script>
<script src="{{asset('/template_resource/js/jquery.tweet.min.js')}}"></script>-->
<script src="{{asset('/template_resource/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('/template_resource/js/jquery.custom-scrollbar.js')}}"></script>
<!--<script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=xa-5306f8f674bfda4c"></script>-->

<!--developer scripts-->
<script src="{{asset('/template_resource/js/url.min.js')}}"></script>
<script src="{{asset('/developer/js/wishlist/main.js')}}"></script>
<script src="{{asset('/developer/js/login/main.js')}}"></script>
<script src="{{asset('/developer/js/review/reviewCount.js')}}"></script>

<script src="{{asset('/developer/autocomplete/js/jquery.easy-autocomplete.js')}}"></script>
<script src="{{asset('/developer/js/search/main.js')}}"></script>
<script src="{{asset('/developer/js/helper/currencyConvertor.js')}}"></script>


<script src="{{asset('/template_resource/js/scripts.js')}}"></script>
<!--developer scripts-->


<script>
$('#thumbCarousel').carousel({
  interval: 4000
});

$('.#thumbCarousel .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));
  
  for (var i=0;i<2;i++) {
    next=next.next();
    if (!next.length) {
    	next = $(this).siblings(':first');
  	}
    
    next.children(':first-child').clone().appendTo($(this));
  }
});
</script>
<script>

    function productByManufacturer(i) {
        var searchParamObj = {"pa": []};
        var manufacturersIdList = [];
        manufacturersIdList.push(i);


        if (manufacturersIdList.length > 0) {
            searchParamObj['manufacturers'] = manufacturersIdList;
        }


        var searchParams = (
                searchParamObj.pa.length > 0
                || manufacturersIdList.length > 0
                ) ? "?q=" + encodeURIComponent(JSON.stringify(searchParamObj)) : "";
        var viewSize = 10;
        searchParams += (searchParams != "") ? "&limit=" + viewSize + "&page=0" : "";
//    window.location = location.protocol + '//' + location.host + location.pathname+searchParams;
        console.log(location);
        window.location = $("#baseUrl").val() + 'manufacturer' + searchParams;
    }
    function compare_product(id) {
        window.location.assign($('#baseUrl').val() + 'compareProducts/' + id);
    }
    function compare_package(id) {
        window.location.assign($('#baseUrl').val() + 'comparePackages/' + id);
    }

    function changePictureQuickView(id, picName, count)
    {
        srcOfZoom = $("#small-image" + id).attr('src');
        if (srcOfZoom == "http://placehold.it/90x90?text=No Image Found")
        {
            newSource = ("http://placehold.it/360x360?text=No Image Found");
        }
        else {
            parts = srcOfZoom.split("thumbnail");
            newSource = (parts[0] + "pagelarge/" + picName);
        }

        console.log(newSource);
        $("#zoom_image_package" + count).attr("src", newSource);

    }
    function changeproductview(view) {
        $.ajax({
            url: $("#baseUrl").val() + "changeproductview",
            method: "POST",
            data: {
                "view": view
            },
            success: function(data) {

                if (data.responseStat.status) {
                    if (view == "grid") {
                        $('#list_view').css('display', 'none');
                        $('#grid_view').css('display', 'block');
                        $('#grid_button').removeClass('color_dark');
                        $('#grid_button').removeClass('bg_light_color_1');
                        $('#grid_button').addClass('bg_scheme_color');
                        $('#grid_button').addClass('color_light');

                        $('#list_button').removeClass('color_light');
                        $('#list_button').removeClass('bg_scheme_color');
                        $('#list_button').addClass('bg_light_color_1');
                        $('#list_button').addClass('color_dark');
                    }
                    else {
                        $('#list_view').css('display', 'block');
                        $('#grid_view').css('display', 'none');
                        $('#list_button').removeClass('color_dark');
                        $('#list_button').removeClass('bg_light_color_1');
                        $('#list_button').addClass('bg_scheme_color');
                        $('#list_button').addClass('color_light');

                        $('#grid_button').removeClass('color_light');
                        $('#grid_button').removeClass('bg_scheme_color');
                        $('#grid_button').addClass('bg_light_color_1');
                        $('#grid_button').addClass('color_dark');
                    }


                }
                else {
                    alert('Some problem with server please try again');
                }

            }
        });
    }
    $(document).ready(function() {
        $('.sub_menu_wrap').each(function() {
            if ($(this).find('.sub_menu').length == 0) {
                $(this).addClass('type_2');
            }
        });
    });
</script>


<script>
    function changePicture(picName,count)
    {
        srcOfZoom = $("#zoom_image"+count).attr('src');
        console.log(srcOfZoom);
        parts = srcOfZoom.split("/");

        newSource = (parts[0]+"/"+parts[1]+"/"+parts[2]+"/"+parts[3]+"/"+parts[4]+"/"+parts[5]+"/"+parts[6]+"/"+parts[7]+"/"+picName);

        console.log(newSource);
        $("#zoom_image"+count).attr("src",newSource);

    }
    
    
</script>
<script>
    if(location.pathname.indexOf('/home')>0){

      //  document.getElementById("topSection").className = "h_bot_part header-most-top container";
    }

    function logIn()
    {
        if($('#userName').val()=="" ||$('#userName').val()=="undefined")
        {
            showLoginForm(" ");
        }
    }


    function addWishListClass()
    {
        $.ajax({

            url: $("#baseUrl").val() + "api/wishlist/product/array",
            method: "POST",
            data: {

            },
            success: function (data) {

                var productArray = data.responseData;

                if(productArray.length>0){
                    for(var i=0; i<productArray.length;i++){

                        $('#inner'+productArray[i]).addClass('active');
                        $('#outer'+productArray[i]).addClass('active');
                        console.log("product id's : "+productArray[i]);
                    }
                }
            }

        });
    }



</script>

</body>



</html>