<!doctype html>


<!--[if IE 9 ]><html class="ie9" lang="en" ng-app="mallBd"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" ng-app="mallBd"  ><!--<![endif]-->
    <head>
        <title>Mall BD</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <!--meta info-->
        <meta name="author" content="">
        <meta name="keywords" content="">
        <meta name="description" content="">
        <link rel="icon" type="image/ico" href="{{asset('/template_resource/images/fav.ico')}}">
        <!--stylesheet include-->
        <link rel="stylesheet" type="text/css" media="all" href="{{asset('/template_resource/css/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" media="all" href="{{asset('/template_resource/css/settings.css')}}">
        <link rel="stylesheet" type="text/css" media="all" href="{{asset('/template_resource/css/owl.carousel.css')}}">
        <link rel="stylesheet" type="text/css" media="all" href="{{asset('/template_resource/css/owl.transitions.css')}}">
        <link rel="stylesheet" type="text/css" media="all" href="{{asset('/template_resource/css/jquery.custom-scrollbar.css')}}">
        <link rel="stylesheet" type="text/css" media="all" href="{{asset('/template_resource/css/style.css')}}">
        <!--font include-->
        <link href="{{asset('/template_resource/css/font-awesome.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('/template_resource/css/fa-snapchat.css')}}">

        <script type="text/javascript" src="{{asset('/developer/angular/corelib/angular.min.js')}}"></script>
        <!--<script type="text/javascript" src="{{asset('/developer/angular/corelib/lazy-scroll.min.js')}}"></script>-->
        <script type="text/javascript" src="{{asset('developer/angular/helper/HtmlManipulationModel.js')}}"></script>
        <script type="text/javascript" src="{{asset('developer/angular/datamodel/datamodel.js')}}"></script>
        <script type="text/javascript" src="{{asset('developer/angular/controllers/cart/main.js')}}"></script>

        <link rel="stylesheet" type="text/css" media="all" href="{{asset('developer/autocomplete/css/easy-autocomplete.css')}}">
        <link rel="stylesheet" type="text/css" media="all" href="{{asset('developer/autocomplete/css/easy-autocomplete.themes.css')}}">
    </head>

    <?php $cls = ""; ?>

    <body ng-controller="CartController" >
        @include('web/partial/loader')
        <!--boxed layout-->
        <div class="wide_layout relative w_xs_auto clearfix">
            <!--[if (lt IE 9) | IE 9]>
            <div style="background:#fff;padding:8px 0 10px;">
                <div class="container" style="width:1170px;">
                    <div class="row wrapper">
                        <div class="clearfix" style="padding:9px 0 0;float:left;width:83%;"><i
                                class="fa fa-exclamation-triangle scheme_color f_left m_right_10"
                                style="font-size:25px;color:#e74c3c;"></i><b style="color:#e74c3c;">Attention! This page may not
                            display correctly.</b> <b>You are using an outdated version of Internet Explorer. For a faster,
                            safer browsing experience.</b></div>
                        <div class="t_align_r" style="float:left;width:16%;"><a
                                href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"
                                class="button_type_4 r_corners bg_scheme_color color_light d_inline_b t_align_c" target="_blank"
                                style="margin-bottom:2px;">Update Now!</a></div>
                    </div>
                </div>
            </div>
            <![endif]-->
            <!--markup header-->
            <header role="banner" class="type_5 fixed-top">
                <!--header top part-->
                @include('web/partial/header/top')
                <!--header bottom part-->
                @include('web/partial/new_menu')


            </header>
            <div id="carousel-example-generic" class="carousel slide main-carousel" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <?php $bannercount = 0; ?>
                    @foreach($banners as $banner)
                    <?php $bannercount++;
                    $banner_image = $imageUrl . 'banner/' . $banner->image ?>
<?php if ($bannercount < 2) { ?>
                        <div class="item active" style="background: url('{{$banner_image}}'); background-size: cover;">
                        </div>
<?php } else { ?>
                        <div class="item" style="background: url('{{$banner_image}}'); background-size: cover;">
                        </div>
<?php } ?>
                    @endforeach
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <i class="fa fa-chevron-left" aria-hidden="true"></i>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <i class="fa fa-chevron-right" aria-hidden="true"></i>
                    <span class="sr-only">Next</span>
                </a>
            </div>

            @include('web/partial/sideblock/shippingtext')

            <div class="position-relative heading-styler clearfix">
                <h3 class="main-title">Product Brands</h3>
                <h2 class="main-small-title">Brands</h2>
            </div>

            <!--<div class="container-fluid brands-container">-->

            <!--</div>-->

            <!--content-->
            <div class="container">
                <!--product brands-->
                <div class="m_bottom_25 m_sm_bottom_20 relative">
                    <div class="f_right clearfix nav_buttons_wrap f_mxs_none">
                        <button  class="button_type_7 left-arr pb_prev pb_prev_new">
                            <i class="fa fa-angle-left"></i></button>
                        <button  class="button_type_7 right-arr pb_next pb_next_new">
                            <i class="fa fa-angle-right"></i></button>
                    </div>
                </div>
                <!--product brands carousel-->
                <div class="product_brands m_bottom_45 m_sm_bottom_35">
                    @foreach($manufacturer as $m)
                    <a class="d_block t_align_c">
                        <img style="cursor: pointer;" onclick="productByManufacturer(<?php echo $m->id; ?>)" src="{{$imageUrl.'manufacturer/'.@$m->icon}}"   onerror="this.src='http://placehold.it/160x120?text=Icon not found'">
                    </a>
                    @endforeach                    
                </div>
            </div>
            <div class="container">
                <div class="position-relative heading-styler clearfix" style="margin-top: 20px;">
                    <h3 class="main-title">Showcase</h3>
                    <h2 class="main-small-title">All products</h2>
                </div>
            <!--                <h2 class="tt_uppercase m_bottom_20 color_dark heading1 animate_ftr title-font"><span>All Products</span></h2>-->
            </div>
            
            <div class="container-fluid clearfix">
                <div class="row clearfix filter-new">
                    <div class="container">
                        <ul id="home_allproducts_ul" class="horizontal_list clearfix tt_uppercase isotope_menu f_size_ex_large filter-tabs">
                            <li id="home_allproducts" class="active m_right_5 m_bottom_10 m_xs_bottom_5 animate_ftr animate_horizontal_finished">
                                <button class="button_type_2 bg_light_color_1 r_corners tr_delay_hover tt_uppercase box_s_none" data-filter="*" onclick="showproducts('all')">
                                    All
                                </button>
                            </li>
                            <li id="home_featuredproducts" class="m_right_5 m_bottom_10 m_xs_bottom_5 animate_ftr animate_horizontal_finished">
                                <button class="button_type_2 bg_light_color_1 r_corners tr_delay_hover tt_uppercase box_s_none" data-filter=".featured" onclick="showproducts('featured')" >Featured
                                </button>
                            </li>
                            <li id="home_newproducts" class="m_right_5 m_bottom_10 m_xs_bottom_5 animate_ftr animate_horizontal_finished">
                                <button class="button_type_2 bg_light_color_1 r_corners tr_delay_hover tt_uppercase box_s_none" data-filter=".new" onclick="showproducts('new')">New
                                </button>
                            </li>
                            <li id="home_specialsproducts" class="m_right_5 m_bottom_10 m_xs_bottom_5 animate_ftr animate_horizontal_finished">
                                <button class="button_type_2 bg_light_color_1 r_corners tr_delay_hover tt_uppercase box_s_none" data-filter=".specials" onclick="showproducts('specials')">Specials
                                </button>
                            </li>
                            <li id="home_packages" class="m_right_5 m_bottom_10 m_xs_bottom_5 animate_ftr animate_horizontal_finished">
                                <button class="button_type_2 bg_light_color_1 r_corners tr_delay_hover tt_uppercase box_s_none" data-filter=".package" onclick="showproducts('package')">Packages
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container clearfix" style="min-height: 400px;">
                <?php
                $countPic =0;
                $avgRating=0;
                $left=0;
                $countPackagePic=0;
				$cover_image_name="";	
                ?>
                <div class="row clearfix">
                
                    <!--product item-->
                    @foreach($mallBdItemList as $mallBdItem)
                        @if($mallBdItem->type == App\Model\DataModel\MallBdItem::_PRODUCT)
                            <?php $product = $mallBdItem->item; ?>
                            <div class="popup_wrap d_none" id="quick_view_product_{{$product->id}}" >
                                <section class="popup r_corners shadow">
                                    <button class="bg_tr color_dark tr_all_hover text_cs_hover close f_size_large"><i class="fa fa-times"></i></button>
                                    <div class="clearfix">
                                        <div class="custom_scrollbar">
                                            <!--left popup column-->
                                            <div class="f_left half_column">
                                                
                                                <div class="relative d_inline_b m_bottom_10 qv_preview">
                                                    <?php
                                                        if (($product->isFeatured)) {
                                                            ?>
                                                            <div class="f-tag">
                                                                <span>hot</span>
                                                            </div>
                                                            <?php
                                                        } else if ($product->discountActiveFlag) {
                                                            ?>
                                                            <div class="s-tag">
                                                                <span>Sale</span>
                                                            </div>
                                                             <?php
                                                            } else if ($product->createdOn >$filterDate) {
                                                                ?>
                                                                <div class="h-tag">
                                                                    <span>New</span>
                                                                </div>
                                                            <?php
                                                            } else if ($product->isBestSeller) {
                                                                ?>
                                                                <div class="b-tag">
                                                                    <span>Best Seller</span>
                                                                </div>
                                                        <?php } else {  ?>
                                                          
                                                        <?php  } ?>
                                                    {{--<span class="hot_stripe"><img src="{{asset('/template_resource/images/sale_product.png')}}" alt=""></span>--}}
                                                    @if(@$product->pictures == NULL)
                                                            <img id="zoom_image"src="http://placehold.it/360x360?text=No Image Found">
                                                     @endif
                                                    @foreach(@$product->pictures as $picture)
                                                        @if($picture->cover==1)
                                                            <?php $countPic++; $cover_image_name=$picture->name; ?>
                                                            <img id="zoom_image{{$countPic}}" src="{{$imageUrl.'product/pagelarge/'.$picture->name}}" class="tr_all_hover" alt="" onerror="this.src='http://placehold.it/360x360?text=No Image Found'">
                                                         @endif
                                                         
                                                    @endforeach
                                                </div>
                                                <!--carousel-->

                                                <div class="relative qv_carousel_wrap m_bottom_20">
                                                    <button class="button_type_11 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_prev">
                                                        <i class="fa fa-angle-left "></i>
                                                    </button>
                                                    <ul class="qv_carousel d_inline_middle">
                                                        @if(@$product->pictures == NULL)
                                                            <img id="small-image" src="http://placehold.it/90x90?text=No Image Found">
                                                        @endif
                                                        @foreach(@$product->pictures as $picture)
                                                            <li><img id="small-image{{$picture->name}}" src="{{$imageUrl.'product/thumbnail/'.$picture->name}}" onerror="this.src='http://placehold.it/90x90?text=No Image Found'" alt="" onclick="changePicture('{{$picture->name}}', '{{ $countPic}}')"></li>
                                                        @endforeach
                                                    </ul>
                                                    <button class="button_type_11 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_next">
                                                        <i class="fa fa-angle-right "></i>
                                                    </button>
                                                </div>

                                                <div class="d_inline_middle" style="display: none;">Share this:</div>
                                                <div class="d_inline_middle m_left_5" style="display: none;">
                                                    <!-- AddThis Button BEGIN -->
                                                    <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
                                                        <a class="addthis_button_preferred_1"></a>
                                                        <a class="addthis_button_preferred_2"></a>
                                                        <a class="addthis_button_preferred_3"></a>
                                                        <a class="addthis_button_preferred_4"></a>
                                                        <a class="addthis_button_compact"></a>
                                                        <a class="addthis_counter addthis_bubble_style"></a>
                                                    </div>
                                                    <!-- AddThis Button END -->
                                                </div>
                                            </div>
                                            <!--right popup column-->
                                            <div class="p_top_10 t_xs_align_l pop-desc" style="float: right; width: 360px;">
                                                <p class="manf-name"><a style="cursor: pointer;" onclick="productByManufacturer('{{$product->manufacturer->id}}');">{{$product->manufacturer->name}}</a></p>
                                                <p class="prod-name-detail"><a href="{{url('/product/' . $product->url . '/' . $product->code)}}">{{$product->title}}</a></p>
                                                 @if($product->discountActiveFlag)
                                                    <p style="color:red;">Your Save<span><?php echo $currency->HTMLCode; ?> {{ number_format($product->discountAmount,2)}} </span></p>
                                                 @endif
                                                <p class="detail-price">
                                                    @if($product->discountActiveFlag)
                                                    <span class="older"><?php echo $currency->HTMLCode; ?> {{ number_format($product->prices[0]->retailPrice,2)}}</span><span class="newer"><?php echo $currency->HTMLCode; ?> {{ number_format(($product->prices[0]->retailPrice-$product->discountAmount),2)}}</span>
                                                    @else
                                                    <span class="newer"><?php echo $currency->HTMLCode; ?> {{ number_format($product->prices[0]->retailPrice,2)}}</span>
                                                    @endif

                                                </p>
                                                <span class="detail-desc"><?php echo $product->description; ?></span>
                                                <input type="hidden" id="productJsonObj_{{$product->id}}" value="{{json_encode($product)}}"  />
                                                <p class="rate-it">
                                                    <ul class="horizontal_list d_inline_middle type_2 clearfix rating_list tr_all_hover">
                                                        <?php
                                                        $avgRating = ceil($product->avgRating);
                                                        if($avgRating>5)
                                                        {
                                                            $avgRating=5;
                                                        }

                                                        $left = 5-$avgRating;
                                                        ?>
                                                        @for($i=0;$i<$avgRating;$i++)
                                                            <li class="active">
                                                                <i class="fa fa-star-o empty tr_all_hover"></i>
                                                                <i class="fa fa-star active tr_all_hover"></i>
                                                            </li>
                                                        @endfor
                                                        @if($left>0)
                                                            @for($i=0;$i<$left;$i++)
                                                                <li>
                                                                    <i class="fa fa-star-o empty tr_all_hover"></i>
                                                                    <i class="fa fa-star active tr_all_hover"></i>
                                                                </li>
                                                            @endfor
                                                        @endif
                                                        <li>
                                                            <span class="rate_number">{{$avgRating}}</span>
                                                        </li>

                                                    </ul>                                                

                                                 </p>

                                                <p class="desc-other-block">

                                                        @if($product->isWished)
                                                            <a class="favourite-link active" href="" onclick="submitWishList({{$product->id}})"><i class="fa fa-heart"></i>Add to favorites</a>              
                                                         @else
                                                            <a class="favourite-link relative notify-flyout" href="" onclick="submitWishList('{{$product->id}}','quick_view_product_{{$product->id}}')"><i class="fa fa-heart-o favourite_{{$product->id}}"></i>Added to favorites
                                                              <span class="notify-small" id="outeraddnotify{{ $product->id }}" hidden>Product Added</span>
                                                              <span class="notify-small" id="outeraddnotify2{{ $product->id }}" hidden>Already Added</span>                                       
                                                            </a>    
                                                        @endif
                                                        <?php if (!$mobile) { ?>
                                                            <a class="wishlist-link" href="#" onclick="compare_product('{{$product->id}}');"><i class="fa fa-copy"></i>Compare Product</a>

                                                         <?php } ?>
                                                        
                                                    </p>
                                                <div class="notify-small" id="outeraddnotify{{ $product->id }}"  hidden><span>Product Added</span></div>
                                                 <div class="notify-small" id="outeraddnotify2{{ $product->id }}"  hidden><span>Already Added</span></div>
                                                            @foreach(@$product->attributes as $attributes)
                                                       <div class="add-qn" style="margin-bottom: 25px;">
                                                            <div class="col-md-3">
                                                                {{$attributes->name}}:
                                                            </div>
                                                            <div class="col-md-9">
                                                               <select class="qnty" style="width: 100%;" name="product_name" id="select_attribute_{{$product->id}}_{{$attributes->id}}">
                                                                        @foreach(@$attributes->attributesValue as $attributesValue)
                                                                        <option value="{{@$attributesValue->id}}">{{@$attributesValue->value}}</option>
                                                                        @endforeach
                                                               </select>
                                                            </div>
                                                    
                                                        </div>
                                                            @endforeach
                                                 <div class="add-qn">
                                                    <?php if($product->quantity <= 0){ ?>
                                                    <!--<button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light f_left mw_0" >Out of Stock</button>-->

                                                            <div class="col-md-12">
                                                                <b style="color: red;font-size: 25px;">Out of Stock</b>
                                                            </div>
                                                        <?php }else{ ?>
                                                        <div class="col-md-3">
                                                                <select class="qnty" id="quantity_modifier_{{$product->id}}">                                        
                                                                   @for($i=$product->minimumOrderQuantity;$i<=$product->quantity;$i++)
                                                                    <option value="{{$i}}">Qty- {{$i}}</option>
                                                                    @endfor

                                                                </select>
                                                            </div>
                                                            <div class="col-md-9">
                                                                <button class="btn-new-ad" ng-click="addToCart('productJsonObj_{{$product->id}}',-1,true)">Add to basket</button>
                                                            </div>
                                                    <?php } ?>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>

                            <?php
                            $cls = "";
                            if($product->isFeatured){
                                $cls = " featured";
                            }

                            if($product->discountActiveFlag)
                            {
                                $cls .=" specials";
                            }

//                            if($productCount<4 || (!$product->isFeatured && !$product->discountActiveFlag)){}
                            if((!$product->isFeatured && !$product->discountActiveFlag)){
                                $cls .=" new";

                            }
//                            $productCount++;
                            ?>
                            <div class="col-md-3 col-sm-6 col-xs-12 {{ $cls }}">
                                <div class="panel panel-default product-item">
                                    <div class="panel-heading">
                                        <?php
                                                        if (($product->isFeatured)) {
                                                            ?>
                                                            <div class="f-tag">
                                                                <span>hot</span>
                                                            </div>
                                                            <?php
                                                        } else if ($product->discountActiveFlag) {
                                                            ?>
                                                            <div class="s-tag">
                                                                <span>Sale</span>
                                                            </div>
                                                             <?php
                                                            } else if ($product->createdOn >$filterDate) {
                                                                ?>
                                                                <div class="h-tag">
                                                                    <span>New</span>
                                                                </div>
                                                            <?php
                                                            } else if ($product->isBestSeller) {
                                                                ?>
                                                                <div class="b-tag">
                                                                    <span>Best Seller</span>
                                                                </div>
                                                        <?php } else {  ?>
                                                          
                                                        <?php  } ?>
                                        <a href="{{url('/product/' . $product->url . '/' . $product->code)}}" class="relative prod-anchor">
                                            <img src="{{$imageUrl.'product/general/'.$cover_image_name}}" onerror="this.src='http://placehold.it/242x242?text=No Image Found'">
                                            <span id="quick_view_product_{{$product->id}}_trigger_btn" data-popup="#quick_view_product_{{$product->id}}" role="button" class="button_type_5 box_s_none color_light r_corners tr_all_hover d_xs_none new-qv" onclick="numberOfReview('{{$product->id}}')">Quick View</span>
                                        </a>
                                    </div>
                                    <?php
                                    $length = strlen($product->title);
                                    if($length>=19)
                                    {
                                        $editedTitle = substr($product->title, 0, 18)."...";
                                    }
                                    else{
                                        $editedTitle = $product->title;
                                    }

                                    ?>
                                    <div class="panel-body text-center">
                                        <h5 class="prod_name_fix prod_name_new"><a href="{{url('/product/' . $product->url . '/' . $product->code)}}">{{ @$editedTitle }}</a></h5>
                                        @if($product->discountActiveFlag)
                                         <p class="price-old"><span><?php echo $currency->HTMLCode; ?></span>{{ number_format(($product->prices[0]->retailPrice-$product->discountAmount),2)}}</p>
                                         <p class="price-usual"><span><?php echo $currency->HTMLCode; ?></span>{{number_format($product->prices[0]->retailPrice,2)}}</p>
                                        @else
                                        <p class="price-usual"><span><?php echo $currency->HTMLCode; ?></span>{{number_format($product->prices[0]->retailPrice,2)}}</p>
                                        @endif
                                        <ul class="horizontal_list f_right clearfix rating_list tr_all_hover rating-item">
                                                @for($i=0;$i<$avgRating;$i++)
                                                    <li class="active">
                                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                                        <i class="fa fa-star active tr_all_hover"></i>
                                                    </li>
                                                @endfor

                                                @if($left>0)
                                                    @for($i=0;$i<$left;$i++)
                                                        <li>
                                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                                            <i class="fa fa-star active tr_all_hover"></i>
                                                        </li>
                                                    @endfor
                                                @endif


                                            </ul>
                                         <input type="hidden" id="productJsonObj_{{$product->id}}" value="{{json_encode($product)}}"  />
                                        <div class="clearfix position-relative prod-btn prod-btn-renew">
                                            <?php if($product->quantity <= 0){ ?><b style="color: red;">Out of Stock</b>
                                            <?php }else{ ?>
                                                <button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light f_left mw_0 add-fix item-ad-btn" ng-click="addToCart('productJsonObj_{{$product->id}}',1,false)">Add to Basket</button>
                                            <?php } ?>    
                                             <?php if (!$mobile) { ?>
                                                    <button class="active-wishlist button_type_4 bg_light_color_2 tr_all_hover f_right r_corners color_dark mw_0 m_left_5 p_hr_0 item-com-btn" onclick="compare_product('{{$product->id}}');" ><i class="fa fa-files-o"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Compare</span></button>
                                                <?php } ?>
                                            <div class="abs-loader" id="load-img{{ $product->id }}" hidden></div>
                                            @if($product->isWished)

                                                <button id="outer{{ $product->id }}" class="active button_type_4 bg_light_color_2 tr_all_hover f_right r_corners color_dark mw_0 p_hr_0 item-wish-btn" onclick="submitWishList({{$product->id}})"><i class="fa fa-heart-o"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span></button>
                                            @else
                                                <button id="outer{{ $product->id }}" class="button_type_4 bg_light_color_2 tr_all_hover f_right r_corners color_dark mw_0 p_hr_0 item-wish-btn" onclick="submitWishList({{$product->id}})"><i class="fa fa-heart-o"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span></button>
                                            @endif
                                            <div class="notify-small" id="addnotify{{ $product->id }}"  hidden><span>Product Added</span></div>
                                            <div class="notify-small" id="addnotify2{{ $product->id }}"  hidden><span>Already Added</span></div>
                                         </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        @if($mallBdItem->type == App\Model\DataModel\MallBdItem::_PACKAGE)
                            <?php $package = $mallBdItem->item; ?>


                            <?php
                            $length = strlen($package->packageTitle);
                            if($length>=19)
                            {
                                $editedTitle = substr($package->packageTitle, 0, 18)."...";
                            }
                            else{
                                $editedTitle = $package->packageTitle;
                            }

                            ?>

                            <div class="col-md-3 col-sm-6 col-xs-12 package">
                                <div class="panel panel-default product-item">
                                    <div class="panel-heading">
                                        <div class="so-tag">
                                                        <span>Special Offer</span>
                                                    </div>
                                        <a href="{{url('/packages/'.urlencode($package->packageTitle).'/'.$package->id)}}" class="relative prod-anchor">
                                            <img onerror="this.src='http://placehold.it/242x242?text=No Image Found'" src="{{ $imageUrl }}package/general/{{@$package->image}}">
                                            <span data-popup="#quick_view_package_{{@$package->id}}" role="button" class="button_type_5 box_s_none color_light r_corners tr_all_hover d_xs_none new-qv" >Quick View</span>
                                        </a>
                                    </div>
                                    <div class="panel-body text-center">
                                        <h5 class="prod_name_fix prod_name_new"><a href="{{url('/packages/'.urlencode($package->packageTitle).'/'.$package->id)}}">{{ @$editedTitle }}</a></h5>
                                        <p class="price-usual"><span><?php echo $currency->HTMLCode; ?></span>{{ number_format($package->packagePriceTotal,2)}}</p>
                                        <div class="clearfix position-relative prod-btn prod-btn-renew">
                    <!--                        <b style="color: red;">Out of Stock</b>-->
                                            <button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light f_left mw_0 add-fix item-ad-btn" ng-click="addPackageToCart('packageJsonObj_{{$package->id}}',1,true)" >Add to Basket</button>
                                            <?php if (!$mobile) { ?>
                                                    <button class="active-wishlist button_type_4 bg_light_color_2 tr_all_hover f_right r_corners color_dark mw_0 m_left_5 p_hr_0 item-com-btn" onclick="compare_package('{{$package->id}}');"><i class="fa fa-files-o"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Compare</span></button>
                                             <?php } ?>
                                            <!--<button class="active-wishlist button_type_4 bg_light_color_2 tr_all_hover f_right r_corners color_dark mw_0 p_hr_0 item-wish-btn"><i class="fa fa-heart-o"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span></button>-->
                                            <div class="notify-small" hidden><span>Product Added</span></div>
                                            <div class="notify-small"   hidden><span>Already Added</span></div>
                                         </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="popup_wrap d_none" id="quick_view_package_{{@$package->id}}">
                                <section class="popup r_corners shadow">
                                    <button class="bg_tr color_dark tr_all_hover text_cs_hover close f_size_large"><i class="fa fa-times"></i>
                                    </button>
                                    <div class="clearfix">
                                        <div class="custom_scrollbar">
                                            <!--left popup column-->
                                            <div class="f_left half_column">
                                                <div class="relative d_inline_b m_bottom_10 qv_preview">
                                                     <div class="so-tag">
                                                        <span>Special Offer</span>
                                                    </div>                                                  
                                                    <?php $countPackagePic++;?>
                                                       <img id="zoom_image_package{{$countPackagePic}}" src="{{$imageUrl.'package/pagelarge/'.$package->image}}" class="tr_all_hover" alt="" onerror="this.src='http://placehold.it/360x360?text=No Image Found'">                                             
                                                    
                                                    @if(@$countPackagePic == 0)
                                                            <img id="zoom_image_package" src="http://placehold.it/360x360?text=No Image Found">
                                                    @endif         
                                                  
                                                    
                                                </div>
                                                <!--carousel-->

                                                <div class="relative qv_carousel_wrap m_bottom_20">
                                                    <button class="button_type_11 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_prev">
                                                        <i class="fa fa-angle-left "></i>
                                                    </button>
                                                    <ul class="qv_carousel d_inline_middle">
                                              
                                                        <li><img id="small-image{{@$package->id}}"  src="{{ $imageUrl }}package/thumbnail/{{@$package->image}}" alt="" onerror="this.src='http://placehold.it/90x90?text=No Image Found'" onclick="changePictureQuickView('{{@$package->id}}','{{@$package->image}}','{{ $countPackagePic}}')"></li>
                                                        @foreach($package->packageProduct as $products)
                                                             @if(isset($products->product->pictures[0]))
                                                                <li><img id="small-image{{@$package->id}}{{@$products->product->pictures[0]->id}}"  src="{{ $imageUrl }}product/thumbnail/{{@$products->product->pictures[0]->name}}" alt="" onerror="this.src='http://placehold.it/90x90?text=Product Banner not found'" onclick="changePictureQuickView('{{@$package->id}}{{@$products->product->pictures[0]->id}}','{{@$products->product->pictures[0]->name}}','{{ $countPackagePic}}')"></li>
                                                            @endif 
                                                       @endforeach
                                                    </ul>
                                                    <button class="button_type_11 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_next">
                                                        <i class="fa fa-angle-right "></i>
                                                    </button>
                                                </div>
                                                <div class="d_inline_middle" style="display: none;">Share this:</div>
                                                <div class="d_inline_middle m_left_5"  style="display: none;">
                                                    <!-- AddThis Button BEGIN -->
                                                    <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
                                                        <a class="addthis_button_preferred_1"></a>
                                                        <a class="addthis_button_preferred_2"></a>
                                                        <a class="addthis_button_preferred_3"></a>
                                                        <a class="addthis_button_preferred_4"></a>
                                                        <a class="addthis_button_compact"></a>
                                                        <a class="addthis_counter addthis_bubble_style"></a>
                                                    </div>
                                                    <!-- AddThis Button END -->
                                                </div>
                                            </div>
                                            <!--right popup column-->
                                            <div class="p_top_10 t_xs_align_l pop-desc" style="float: right; width: 360px;">
                                            <p class="manf-name"><a href="{{url('/packages/'.urlencode($package->packageTitle).'/'.$package->id)}}">{{$package->packageTitle}}</a></p>
                                            @foreach($package->packageProduct as $products)
                                                <p class="item-number" style="margin-bottom: 0px;"><a href="{{url('/product/'. $products->product->url . '/' . $products->product->code)}}" target="_blank" class="color_dark">{{@$products->product->title}}</a></p>
                                            @endforeach 
                                            <p style="color:red;">Your Save<span><?php echo $currency->HTMLCode; ?> {{ number_format($package->originalPriceTotal-$package->packagePriceTotal,2)}} </span></p>
                                            <p class="detail-price">
                                                <span class="older"><?php echo $currency->HTMLCode; ?> {{ number_format($package->originalPriceTotal,2)}}</span>
                                                <span class="newer"><?php echo $currency->HTMLCode; ?> {{ number_format($package->packagePriceTotal,2)}}</span>

                                            </p>
                                            <p class="detail-desc"><?php echo $package->description;?></p>

                                            <?php if (!$mobile) { ?>
                                                  <p class="desc-other-block">
                                                <a class="wishlist-link" href="#" onclick="compare_package('{{$package->id}}');"><i class="fa fa-copy"></i>Compare Package</a>
                                            </p>
                                                <?php } ?>
                                            
                                            <div class="add-qn">
                                                <div class="col-md-3">
                                                    <select class="qnty">
                                                        <option>Qty- 1</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-9">
                                                    <button class="btn-new-ad" ng-click="addPackageToCart('packageJsonObj_{{$package->id}}',1,true)">Add to basket</button>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                            <input type="hidden" id="packageJsonObj_{{$package->id}}" value="{{json_encode($package)}}"  />

                        @endif

                    @endforeach
               
                    

            </div>
        </div>
            <!--banners-->
            <div class="container">
                 <?php if($homeafterproductimages !=NULL){ ?>
                <section class="row clearfix m_bottom_45 m_sm_bottom_35">
                             <?php foreach ($homeafterproductimages as $homeafterproductimage){ ?>
                                <div class="col-lg-6 col-md-6 col-sm-6 animate_half_tc">
                                    <a href="<?php echo $homeafterproductimage['url']; ?>" class="d_block banner wrapper r_corners relative m_xs_bottom_30">
                                        <img src="{{ $imageUrl }}homeimages/afterproduct/<?php echo $homeafterproductimage['image']; ?>" alt="">
                                    </a>
                                </div>
                               <?php } ?>
                    
                </section>
                 <?php } ?>
                
                <br>
                <br>
                <!--blog-->
                <div class="row clearfix m_bottom_45 m_sm_bottom_35">
                     <?php if($homeyoutubevideos !=NULL){ ?>
                    <div class="col-lg-6 col-md-6 col-sm-12 m_sm_bottom_35 blog_animate animate_ftr">
                        <h2 class="tt_uppercase color_dark f_left title-lower-font"><span>YouTube Channel Videos</span></h2>
                        <div class="clearfix m_sm_bottom_20">
                            <div class="f_right clearfix nav_buttons_wrap">
                                <button class="button_type_7 bg_cs_hover box_s_none f_size_ex_large bg_light_color_1 f_left tr_delay_hover r_corners blog_prev pb_prev_new">
                                    <i class="fa fa-angle-left"></i></button>
                                <button class="button_type_7 bg_cs_hover box_s_none f_size_ex_large bg_light_color_1 f_left m_left_5 tr_delay_hover r_corners blog_next pb_next_new">
                                    <i class="fa fa-angle-right"></i></button>
                            </div>
                        </div>
                        <!--blog carousel-->
                       
                        <div class="blog_carousel">
                            
                            <?php
                               foreach ($homeyoutubevideos as $homeyoutubevideo){
                               $video_link=$homeyoutubevideo['video_link'];
                               $video_devide=explode("?",$video_link);
                               $emabed_video_link=explode("=",$video_devide[1]);
                            ?>
                                <div class="clearfix">
                                    <iframe width="560" height="220" src="https://www.youtube.com/embed/<?php echo $emabed_video_link[1]; ?>" frameborder="0" allowfullscreen></iframe>
                                </div>
                            <?php } ?>
                            
                        </div>
                       
                    </div>
                     <?php } ?>
                      <?php if($homefacebookreviews !=NULL){ ?>
                    <!--testimonials-->
                    <div class="col-lg-6 col-md-6 col-sm-12 ti_animate animate_ftr">
                        <h2 class="tt_uppercase color_dark f_left f_mxs_none m_mxs_bottom_15 title-lower-font"><span>What Our Customers Say On Facebook</span></h2>
                        <div class="clearfix m_sm_bottom_20">
                            <div class="f_right clearfix nav_buttons_wrap f_mxs_none">
                                <button class="button_type_7 bg_cs_hover box_s_none f_size_ex_large bg_light_color_1 f_left tr_delay_hover r_corners ti_prev pb_prev_new">
                                    <i class="fa fa-angle-left"></i></button>
                                <button class="button_type_7 bg_cs_hover box_s_none f_size_ex_large bg_light_color_1 f_left m_left_5 tr_delay_hover r_corners ti_next pb_next_new">
                                    <i class="fa fa-angle-right"></i></button>
                            </div>
                        </div>
                        <!--testimonials carousel-->
                       
                            <div class="testiomials_carousel">
                                <?php foreach ($homefacebookreviews as $homefacebookreview){ ?>
                                <div>
                                    <img src="{{ $imageUrl }}homeimages/facebookreviews/<?php echo $homefacebookreview['image']; ?>">
                                </div>
                                <?php } ?>
                            
                        </div>
                         
                        
                    </div>
                    <?php } ?>
                </div>
                <!--banners-->
                 <?php if($homedownimages !=NULL){ ?>
                    <div class="row clearfix">
                        <?php foreach ($homedownimages as $homedownimage){ ?>
                                 <div class="col-lg-4 col-md-4 col-sm-4 animate_half_tc">
                                    <a href="<?php echo $homedownimage['url']; ?>" class="d_block banner wrapper r_corners relative m_xs_bottom_30">
                                        <img src="{{ $imageUrl }}homeimages/downimages/<?php echo $homedownimage['image']; ?>" alt="">
                                    </a>
                                </div>
                         <?php } ?>
<!--                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <a href="#"
                               class="d_block animate_ftb h_md_auto m_xs_bottom_15 banner_type_2 r_corners red t_align_c tt_uppercase vc_child n_sm_vc_child">
                                                                    <span class="d_inline_middle">
                                                                            <img class="d_inline_middle m_md_bottom_5" src="{{asset('/template_resource/images/banner_img_3.png')}}" alt="">
                                                                            <span class="d_inline_middle m_left_10 t_align_l d_md_block t_md_align_c">
                                                                                    <b>100% Satisfaction</b><br><span class="color_dark">Guaranteed</span>
                                                                            </span>
                                                                    </span>
                            </a>
                        </div>-->
                    </div>
                 <?php } ?>                
            </div>
            
            </div>

        </div>
        <!--markup footer-->
        @include('web/partial/footer/newbottom')
        

        <!--        search modal-->



        <!--login popup-->

        <button class="t_align_c r_corners type_2 tr_all_hover animate_ftl" id="go_to_top"><i class="fa fa-angle-up"></i>
        </button>


        
        <!--scripts include-->
        @include('web/partial/script/core')        
        <script src="{{asset('/template_resource/js/scripts.js')}}"></script>
        
        <script>
                function showproducts(data){
                    $('#home_allproducts_ul li').removeClass('active');
                    if(data == "featured"){
                        $('.featured').css("display","block");                        
                        $('.new').css("display","none");
                        $('.specials').css("display","none");
                        $('.package').css("display","none");
                        $('#home_featuredproducts').addClass('active');
                    }
                    else if(data == "new"){
                        $('.featured').css("display","none");
                        $('.new').css("display","block");
                        $('.specials').css("display","none");
                        $('.package').css("display","none");
                        $('#home_newproducts').addClass('active');
                    }
                    else if(data == "specials"){
                        $('.featured').css("display","none");
                        $('.new').css("display","none");
                        $('.specials').css("display","block");
                        $('.package').css("display","none");
                        $('#home_specialsproducts').addClass('active');
                    }
                    else if(data == "package"){
                        $('.featured').css("display","none");
                        $('.new').css("display","none");
                        $('.specials').css("display","none");
                        $('.package').css("display","block");
                        $('#home_packages').addClass('active');
                    }
                    else{
                        $('.featured').css("display","block");
                        $('.new').css("display","block");
                        $('.specials').css("display","block");
                        $('.package').css("display","block");
                        $('#home_allproducts').addClass('active');
                    }
                }
            </script>
    </body>
    


</html>