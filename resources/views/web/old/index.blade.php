<!doctype html>
<!--[if IE 9 ]><html class="ie9" lang="en" ng-app="mallBd"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" ng-app="mallBd"><!--<![endif]-->
<head>
    <title>MallBD</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!--meta info-->
    <meta name="author" content="">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="icon" type="image/ico" href="images/fav.ico">
    <!--stylesheet include-->
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('/template_resource/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('/template_resource/css/camera.css')}}">
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('/template_resource/css/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('/template_resource/css/owl.transitions.css')}}">
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('/template_resource/css/jquery.custom-scrollbar.css')}}">
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('/template_resource/css/style.css')}}">
    <!--font include-->
    <link href="{{asset('/template_resource/css/font-awesome.min.css')}}" rel="stylesheet">

    <script type="text/javascript" src="{{asset('/developer/angular/corelib/angular.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('developer/angular/datamodel/datamodel.js')}}"></script>
    <script type="text/javascript" src="{{asset('developer/angular/controllers/cart.js')}}"></script>
</head>

<body>
    <!--boxed layout-->
    <div class="boxed_layout relative w_xs_auto">
        <!--[if (lt IE 9) | IE 9]>
        <div style="background:#fff;padding:8px 0 10px;">
            <div class="container" style="width:1170px;"><div class="row wrapper"><div class="clearfix" style="padding:9px 0 0;float:left;width:83%;"><i class="fa fa-exclamation-triangle scheme_color f_left m_right_10" style="font-size:25px;color:#e74c3c;"></i><b style="color:#e74c3c;">Attention! This page may not display correctly.</b> <b>You are using an outdated version of Internet Explorer. For a faster, safer browsing experience.</b></div><div class="t_align_r" style="float:left;width:16%;"><a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode" class="button_type_4 r_corners bg_scheme_color color_light d_inline_b t_align_c" target="_blank" style="margin-bottom:2px;">Update Now!</a></div></div></div></div>
        <![endif]-->
    <input type="hidden" id="fileUrl" value="{{asset('/')}}" />
        <!--markup header-->
        <header role="banner">
            <!--header top part-->
            @include('web/partial/header/top')
            <!--header bottom part-->
            <section class="h_bot_part container">
                <div class="clearfix row">
                    <div class="col-lg-6 col-md-6 col-sm-4 t_xs_align_c">
                        <a href="index.html" class="logo m_xs_bottom_15 d_xs_inline_b">
                            <img src="{{asset('/template_resource/images/logo.png')}}" alt="">
                        </a>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-8 t_align_r t_xs_align_c">
                        <ul class="d_inline_b horizontal_list clearfix t_align_l site_settings">
                            <!--like-->
                            @include('web/partial/like')
                                    <!--language settings-->
                            @include('web/partial/language')
                                    <!--currency settings-->
                            @include('web/partial/currency')
                                    <!--shopping cart-->
                            @include('web/partial/cart/main')
                        </ul>
                    </div>
                </div>
            </section>
            <!--main menu container-->
            @include('web/partial/menu')

        </header>
        <!--slider-->
        <div class="camera_wrap m_bottom_0">
            <div data-src="{{asset('/template_resource/images/slide_02.jpg')}}" data-custom-thumb="{{asset('/template_resource/images/slide_02.jpg')}}">
                <div class="camera_caption_1 fadeFromTop t_align_c d_xs_none">
                    <div class="f_size_large color_light tt_uppercase slider_title_3 m_bottom_5">Meet New Theme</div>
                    <hr class="slider_divider d_inline_b m_bottom_5">
                    <div class="color_light slider_title tt_uppercase t_align_c m_bottom_45 m_sm_bottom_20"><b>Attractive &amp; Elegant<br>HTML Theme</b></div>
                    <div class="color_light slider_title_2 m_bottom_45">$<b>15.00</b></div>
                    <a href="#" role="button" class="tr_all_hover button_type_4 bg_scheme_color color_light r_corners tt_uppercase">Buy Now</a>
                </div>
            </div>
            <div data-src="{{asset('/template_resource/images/slide_01.jpg')}}" data-custom-thumb="{{asset('/template_resource/images/slide_01.jpg')}}">
                <div class="camera_caption_2 fadeIn t_align_c d_xs_none">
                    <div class="f_size_large tt_uppercase slider_title_3 scheme_color">New arrivals</div>
                    <hr class="slider_divider type_2 m_bottom_5 d_inline_b">
                    <div class="color_light slider_title tt_uppercase t_align_c m_bottom_65 m_sm_bottom_20"><b><span class="scheme_color">Spring/Summer 2014</span><br><span class="color_dark">Ready-To-Wear</span></b></div>
                    <a href="#" role="button" class="d_sm_inline_b button_type_4 bg_scheme_color color_light r_corners tt_uppercase tr_all_hover">View Collection</a>
                </div>
            </div>
            <div data-src="{{asset('/template_resource/images/slide_03.jpg')}}" data-custom-thumb="{{asset('/template_resource/images/slide_03.jpg')}}">
                <div class="camera_caption_3 fadeFromTop t_align_c d_xs_none">
                    <img src="{{asset('/template_resource/images/slider_layer_img.png')}}" alt="" class="m_bottom_5">
                    <div class="color_light slider_title tt_uppercase t_align_c m_bottom_60 m_sm_bottom_20"><b class="color_dark">up to 70% off</b></div>
                    <a href="#" role="button" class="d_sm_inline_b button_type_4 bg_scheme_color color_light r_corners tt_uppercase tr_all_hover">Shop Now</a>
                </div>
            </div>
        </div>
        <!--content-->
        <div class="page_content_offset">
            <div class="container">
                <h2 class="tt_uppercase m_bottom_20 color_dark heading1 animate_ftr">All Products</h2>
                <!--filter navigation of products-->
                <ul class="horizontal_list clearfix tt_uppercase isotope_menu f_size_ex_large">
                    <li class="active m_right_5 m_bottom_10 m_xs_bottom_5 animate_ftr"><button class="button_type_2 bg_light_color_1 r_corners tr_delay_hover tt_uppercase box_s_none" data-filter="*">All</button></li>
                    <li class="m_right_5 m_bottom_10 m_xs_bottom_5 animate_ftr"><button class="button_type_2 bg_light_color_1 r_corners tr_delay_hover tt_uppercase box_s_none" data-filter=".featured">Featured</button></li>
                    <li class="m_right_5 m_bottom_10 m_xs_bottom_5 animate_ftr"><button class="button_type_2 bg_light_color_1 r_corners tr_delay_hover tt_uppercase box_s_none" data-filter=".new">New</button></li>
                    <li class="m_right_5 m_bottom_10 m_xs_bottom_5 animate_ftr"><button class="button_type_2 bg_light_color_1 r_corners tr_delay_hover tt_uppercase box_s_none" data-filter=".specials">Specials</button></li>
                    <li class="m_right_5 m_bottom_10 m_xs_bottom_5 animate_ftr"><button class="button_type_2 bg_light_color_1 r_corners tr_delay_hover tt_uppercase box_s_none" data-filter=".hit">Hit</button></li>
                    <li class="m_right_5 m_bottom_10 m_xs_bottom_5 animate_ftr"><button class="button_type_2 bg_light_color_1 r_corners tr_delay_hover tt_uppercase box_s_none" data-filter=".random">Random</button></li>
                    <li class="m_right_5 m_bottom_10 m_xs_bottom_5 animate_ftr"><button class="button_type_2 bg_light_color_1 r_corners tr_delay_hover tt_uppercase box_s_none" data-filter=".rated">Rated</button></li>
                </ul>
                <!--products-->
                <section class="products_container clearfix m_bottom_25 m_sm_bottom_15">


                     @foreach($productList as $product)
                         <div class="popup_wrap d_none" id="quick_view_product_{{$product->id}}" >
                                <section class="popup r_corners shadow">
                                    <button class="bg_tr color_dark tr_all_hover text_cs_hover close f_size_large"><i class="fa fa-times"></i></button>
                                    <div class="clearfix">
                                        <div class="custom_scrollbar">
                                            <!--left popup column-->
                                            <div class="f_left half_column">
                                                <div class="relative d_inline_b m_bottom_10 qv_preview">
                                                    {{--<span class="hot_stripe"><img src="{{asset('/template_resource/images/sale_product.png')}}" alt=""></span>--}}
                                                   @foreach(@$product->pictures as $picture)
                                                    @if($picture->cover==1)
                                                        <img src="{{asset('product_images/'.$picture->name)}}" class="tr_all_hover" alt="">

                                                    @endif
                                                    @endforeach
                                                </div>
                                                <!--carousel-->

                                                <div class="relative qv_carousel_wrap m_bottom_20">
                                                    <button class="button_type_11 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_prev">
                                                        <i class="fa fa-angle-left "></i>
                                                    </button>
                                                    <ul class="qv_carousel d_inline_middle">
                                                    @foreach($product->pictures as $picture)
                                                        <li data-src="{{asset('product_images/'.$picture->name)}}"><img src="{{asset('product_images/'.$picture->name)}}" alt=""></li>
                                                    @endforeach
                                                    </ul>
                                                    <button class="button_type_11 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_next">
                                                        <i class="fa fa-angle-right "></i>
                                                    </button>
                                                </div>

                                                <div class="d_inline_middle">Share this:</div>
                                                <div class="d_inline_middle m_left_5">
                                                    <!-- AddThis Button BEGIN -->
                                                    <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
                                                        <a class="addthis_button_preferred_1"></a>
                                                        <a class="addthis_button_preferred_2"></a>
                                                        <a class="addthis_button_preferred_3"></a>
                                                        <a class="addthis_button_preferred_4"></a>
                                                        <a class="addthis_button_compact"></a>
                                                        <a class="addthis_counter addthis_bubble_style"></a>
                                                    </div>
                                                    <!-- AddThis Button END -->
                                                </div>
                                            </div>
                                            <!--right popup column-->
                                            <div class="f_right half_column">
                                                <!--description-->
                                                <h2 class="m_bottom_10"><a href="#" class="color_dark fw_medium">{{$product->title}}</a></h2>
                                                <div class="m_bottom_10">
                                                    <!--rating-->
                                                    <ul class="horizontal_list d_inline_middle type_2 clearfix rating_list tr_all_hover">
                                                        <li class="active">
                                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                                            <i class="fa fa-star active tr_all_hover"></i>
                                                        </li>
                                                        <li class="active">
                                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                                            <i class="fa fa-star active tr_all_hover"></i>
                                                        </li>
                                                        <li class="active">
                                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                                            <i class="fa fa-star active tr_all_hover"></i>
                                                        </li>
                                                        <li class="active">
                                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                                            <i class="fa fa-star active tr_all_hover"></i>
                                                        </li>
                                                        <li>
                                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                                            <i class="fa fa-star active tr_all_hover"></i>
                                                        </li>
                                                    </ul>
                                                    <a href="#" class="d_inline_middle default_t_color f_size_small m_left_5">1 Review(s) </a>
                                                </div>
                                                <hr class="m_bottom_10 divider_type_3">
                                                <table class="description_table m_bottom_10">
                                                    <tr>
                                                        <td>Manufacturer:</td>
                                                        <td><a href="#" class="color_dark">{{@$product->manufacturer->name}}</a></td>
                                                    </tr>
                                                    <tr>@if(@$product->quantity>0)
                                                            <td>Availability:</td>
                                                            <td><span class="color_green">in stock </span>{{@$product->quantity}} item(s)</td>
                                                         @else
                                                            <td>Availability:</td>
                                                            <td><span class="colorpicker_rgb_r">out of stock</span>
                                                         @endif
                                                    </tr>
                                                    <tr>
                                                        <td>Product Code:</td>
                                                        <td>{{@$product->code}}</td>
                                                    </tr>
                                                </table>
                                                <h5 class="fw_medium m_bottom_10">Product Dimensions and Weight</h5>
                                                <table class="description_table m_bottom_5">
                                                    <tr>
                                                        <td>Product Length(WxHxD):</td>
                                                        <td><span class="color_dark">{{@$product->width}}x{{@$product->height}}x{{@$product->depth}}</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Product Weight:</td>
                                                        <td>{{@$product->weight}} KG</td>
                                                    </tr>
                                                </table>
                                                <hr class="divider_type_3 m_bottom_10">
                                                <p class="m_bottom_10">{{@$product->description}}</p>
                                                <hr class="divider_type_3 m_bottom_15">
                                                <div class="m_bottom_15">
                                                    {{--<s class="v_align_b f_size_ex_large">$152.00</s><span class="v_align_b f_size_big m_left_5 scheme_color fw_medium">$102.00</span>--}}
                                                <span class="v_align_b f_size_big m_left_5 scheme_color fw_medium">&#2547 {{@$product->prices[0]->retailPrice}}</span>
                                                </div>
                                                <table class="description_table type_2 m_bottom_15">
                                                    @foreach(@$product->attributes as $attributes)
                                                    <tr>
                                                        <td class="v_align_m">{{$attributes->name}}:</td>
                                                        <td class="v_align_m">
                                                            {{--<div class="custom_select f_size_medium relative d_inline_middle">--}}
                                                                {{--<div class="select_title r_corners relative color_dark">Pick</div>--}}
                                                                {{--<ul class="select_list d_none"></ul>--}}


                                                            {{--</div>--}}
                                                            <select name="product_name" id="select_attribute_{{$product->id}}_{{$attributes->id}}">
                                                                @foreach(@$attributes->attributesValue as $attributesValue)
                                                                    <option value="{{@$attributesValue->id}}">{{@$attributesValue->value}}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                    </tr>
                                                     @endforeach
                                                    <tr>
                                                        <td class="v_align_m">Quantity:</td>
                                                        <td class="v_align_m">
                                                            <div class="clearfix quantity r_corners d_inline_middle f_size_medium color_dark">
                                                                <button class="bg_tr d_block f_left" data-direction="down">-</button>
                                                                <input id="quantity_modifier_{{$product->id}}"type="text" name="" readonly value="1" class="f_left">
                                                                <button class="bg_tr d_block f_left" data-direction="up">+</button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="clearfix m_bottom_15">
                                                    <button class="button_type_12 r_corners bg_scheme_color color_light tr_delay_hover f_left f_size_large" ng-click="addToCart('productJsonObj_{{$product->id}}',-1,true)" >Add to Cart</button>
                                                    <button class="button_type_12 bg_light_color_2 tr_delay_hover f_left r_corners color_dark m_left_5 p_hr_0"><i class="fa fa-heart-o f_size_big"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span></button>
                                                    <button class="button_type_12 bg_light_color_2 tr_delay_hover f_left r_corners color_dark m_left_5 p_hr_0"><i class="fa fa-files-o f_size_big"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Compare</span></button>
<!--                                                    <button class="button_type_12 bg_light_color_2 tr_delay_hover f_left r_corners color_dark m_left_5 p_hr_0 relative"><i class="fa fa-question-circle f_size_big"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Ask a Question</span></button>-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        <div class="product_item featured">
                            <figure class="r_corners photoframe shadow relative animate_ftb long">
                                <!--product preview-->
                                <a href="#" class="d_block relative wrapper pp_wrap">
                                    <img src="{{asset('product_images/'.$product->pictures[0]->name)}}" class="tr_all_hover" alt="">
                                    <span id="quick_view_product_{{$product->id}}_trigger_btn" data-popup="#quick_view_product_{{$product->id}}" class="box_s_none button_type_5 color_light r_corners tr_all_hover d_xs_none">Quick View</span>
                                </a>
                                <!--description and price of product-->
                                <figcaption>
                                    <h5 class="m_bottom_10"><a href="#" class="color_dark">{{$product->title}}</a></h5>
                                    <div class="clearfix m_bottom_15">
                                        <p class="scheme_color f_size_large f_left">{{@$product->prices[0]->retailPrice}}</p>
                                        <!--rating-->
                                        <ul class="horizontal_list f_right clearfix rating_list tr_all_hover">
                                            <li class="active">
                                                <i class="fa fa-star-o empty tr_all_hover"></i>
                                                <i class="fa fa-star active tr_all_hover"></i>
                                            </li>
                                            <li class="active">
                                                <i class="fa fa-star-o empty tr_all_hover"></i>
                                                <i class="fa fa-star active tr_all_hover"></i>
                                            </li>
                                            <li class="active">
                                                <i class="fa fa-star-o empty tr_all_hover"></i>
                                                <i class="fa fa-star active tr_all_hover"></i>
                                            </li>
                                            <li class="active">
                                                <i class="fa fa-star-o empty tr_all_hover"></i>
                                                <i class="fa fa-star active tr_all_hover"></i>
                                            </li>
                                            <li>
                                                <i class="fa fa-star-o empty tr_all_hover"></i>
                                                <i class="fa fa-star active tr_all_hover"></i>
                                            </li>
                                        </ul>
                                    </div>
                                    <input type="hidden" id="productJsonObj_{{$product->id}}" value="{{json_encode($product)}}"  />
                                    <div class="clearfix">
                                        <button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light f_left mw_0" ng-click="addToCart('productJsonObj_{{$product->id}}',1,false)" >Add to Cart</button>
                                        <button class="button_type_4 bg_light_color_2 tr_all_hover f_right r_corners color_dark mw_0 m_left_5 p_hr_0"><i class="fa fa-files-o"></i></button>
                                        <button class="button_type_4 bg_light_color_2 tr_all_hover f_right r_corners color_dark mw_0 p_hr_0"><i class="fa fa-heart-o"></i></button>
                                    </div>
                                </figcaption>
                            </figure>
                        </div>

                    @endforeach
                    <!--product item-->
                    <div class="product_item new">
                        <figure class="r_corners photoframe shadow relative animate_ftb long">
                            <!--product preview-->
                            <a href="#" class="d_block relative wrapper pp_wrap">
                                <img src="{{asset('/template_resource/images/product_img_3.jpg')}}" class="tr_all_hover" alt="">
                                <span data-popup="#quick_view_product" class="box_s_none button_type_5 color_light r_corners tr_all_hover d_xs_none">Quick View</span>
                            </a>
                            <!--description and price of product-->
                            <figcaption>
                                <h5 class="m_bottom_10"><a href="#" class="color_dark">Cursus eleifend elit aenean aucto.</a></h5>
                                <div class="clearfix">
                                    <p class="scheme_color f_left f_size_large m_bottom_15">$99.00</p>
                                    <!--rating-->
                                    <ul class="horizontal_list f_right clearfix rating_list tr_all_hover">
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                    </ul>
                                </div>
                                <button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light mw_0">Add to Cart</button>
                            </figcaption>
                        </figure>
                    </div>
                    <!--product item-->
                    <div class="product_item specials">
                        <figure class="r_corners photoframe shadow relative animate_ftb long">
                            <!--product preview-->
                            <a href="#" class="d_block relative pp_wrap">
                                <!--sale product-->
                                <span class="hot_stripe"><img src="{{asset('/template_resource/images/sale_product.png')}}" alt=""></span>
                                <img src="{{asset('/template_resource/images/product_img_4.jpg')}}" class="tr_all_hover" alt="">
                                <span data-popup="#quick_view_product" class="box_s_none button_type_5 color_light r_corners tr_all_hover d_xs_none">Quick View</span>
                            </a>
                            <!--description and price of product-->
                            <figcaption>
                                <h5 class="m_bottom_10"><a href="#" class="color_dark">Aliquam erat volutpat</a></h5>
                                <div class="clearfix">
                                    <p class="scheme_color f_left f_size_large m_bottom_15"><s>$79.00</s> $36.00</p>
                                    <!--rating-->
                                    <ul class="horizontal_list f_right clearfix rating_list tr_all_hover">
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                    </ul>
                                </div>
                                <button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light mw_0">Add to Cart</button>
                            </figcaption>
                        </figure>
                    </div>
                    <!--product item-->
                    <div class="product_item hit">
                        <figure class="r_corners photoframe shadow relative animate_ftb long">
                            <!--product preview-->
                            <a href="#" class="d_block relative pp_wrap">
                                <!--hot product-->
                                <span class="hot_stripe type_2"><img src="{{asset('/template_resource/images/hot_product_type_2.png')}}" alt=""></span>
                                <img src="{{asset('/template_resource/images/product_img_5.jpg')}}" class="tr_all_hover" alt="">
                                <span data-popup="#quick_view_product" class="box_s_none button_type_5 color_light r_corners tr_all_hover d_xs_none">Quick View</span>
                            </a>
                            <!--description and price of product-->
                            <figcaption>
                                <h5 class="m_bottom_10"><a href="#" class="color_dark">Eget elementum vel</a></h5>
                                <div class="clearfix">
                                    <p class="scheme_color f_left f_size_large m_bottom_15">$102.00</p>
                                    <!--rating-->
                                    <ul class="horizontal_list f_right clearfix rating_list tr_all_hover">
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                    </ul>
                                </div>
                                <button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light mw_0">Add to Cart</button>
                            </figcaption>
                        </figure>
                    </div>
                    <!--product item-->
                    <div class="product_item featured">
                        <figure class="r_corners photoframe shadow relative animate_ftb long">
                            <!--product preview-->
                            <a href="#" class="d_block relative wrapper pp_wrap">
                                <img src="{{asset('/template_resource/images/product_img_6.jpg')}}" class="tr_all_hover" alt="">
                                <span data-popup="#quick_view_product" class="box_s_none button_type_5 color_light r_corners tr_all_hover d_xs_none">Quick View</span>
                                        <span class="clearfix p_buttons d_block tr_all_hover">
                                            <span class="box_s_none button_type_5 tr_delay_hover f_left r_corners color_light p_hr_0"><i class="fa fa-heart-o"></i></span>
                                            <span class="box_s_none button_type_5 tr_delay_hover f_left r_corners color_light m_left_5 p_hr_0"><i class="fa fa-files-o"></i></span>
                                        </span>
                            </a>
                            <!--description and price of product-->
                            <figcaption>
                                <h5 class="m_bottom_10"><a href="#" class="color_dark">Ut tellus dolor dapibus</a></h5>
                                <div class="clearfix m_bottom_15">
                                    <p class="scheme_color f_size_large f_left">$57.00</p>
                                    <!--rating-->
                                    <ul class="horizontal_list f_right clearfix rating_list tr_all_hover">
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                    </ul>
                                </div>
                                <button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light mw_0">Add to Cart</button>
                            </figcaption>
                        </figure>
                    </div>
                    <!--product item-->
                    <div class="product_item specials">
                        <figure class="r_corners photoframe shadow relative animate_ftb long">
                            <!--product preview-->
                            <a href="#" class="d_block relative wrapper pp_wrap">
                                <img src="{{asset('/template_resource/images/product_img_7.jpg')}}" class="tr_all_hover" alt="">
                                <span data-popup="#quick_view_product" class="box_s_none button_type_5 color_light r_corners tr_all_hover d_xs_none">Quick View</span>
                            </a>
                            <!--description and price of product-->
                            <figcaption>
                                <h5 class="m_bottom_10"><a href="#" class="color_dark">Cursus eleifend elit aenean aucto.</a></h5>
                                <div class="clearfix">
                                    <p class="scheme_color f_left f_size_large m_bottom_15">$99.00</p>
                                    <!--rating-->
                                    <ul class="horizontal_list f_right clearfix rating_list tr_all_hover">
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                    </ul>
                                </div>
                                <button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light mw_0">Add to Cart</button>
                            </figcaption>
                        </figure>
                    </div>
                    <!--product item-->
                    <div class="product_item rated">
                        <figure class="r_corners photoframe shadow relative animate_ftb long">
                            <!--product preview-->
                            <a href="#" class="d_block relative pp_wrap">
                                <!--sale product-->
                                <span class="hot_stripe type_2"><img src="{{asset('/template_resource/images/sale_product_type_2.png')}}" alt=""></span>
                                <img src="{{asset('/template_resource/images/product_img_8.jpg')}}" class="tr_all_hover" alt="">
                                <span data-popup="#quick_view_product" class="box_s_none button_type_5 color_light r_corners tr_all_hover d_xs_none">Quick View</span>
                            </a>
                            <!--description and price of product-->
                            <figcaption>
                                <h5 class="m_bottom_10"><a href="#" class="color_dark">Aliquam erat volutpat</a></h5>
                                <div class="clearfix">
                                    <p class="scheme_color f_left f_size_large m_bottom_15"><s>$79.00</s> $36.00</p>
                                    <!--rating-->
                                    <ul class="horizontal_list f_right clearfix rating_list tr_all_hover">
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                    </ul>
                                </div>
                                <button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light mw_0">Add to Cart</button>
                            </figcaption>
                        </figure>
                    </div>
                </section>
                <!--banners-->
                <section class="row clearfix m_bottom_45 m_sm_bottom_35">
                    <div class="col-lg-6 col-md-6 col-sm-6 animate_half_tc">
                        <a href="#" class="d_block banner wrapper r_corners relative m_xs_bottom_30">
                            <img src="{{asset('/template_resource/images/banner_img_1.png')}}" alt="">
                                    <span class="banner_caption d_block vc_child t_align_c w_sm_auto">
                                        <span class="d_inline_middle">
                                            <span class="d_block color_dark tt_uppercase m_bottom_5 let_s">New Collection!</span>
                                            <span class="d_block divider_type_2 centered_db m_bottom_5"></span>
                                            <span class="d_block color_light tt_uppercase m_bottom_25 m_xs_bottom_10 banner_title"><b>Colored Fashion</b></span>
                                            <span class="button_type_6 bg_scheme_color tt_uppercase r_corners color_light d_inline_b tr_all_hover box_s_none f_size_ex_large">Shop Now!</span>
                                        </span>
                                    </span>
                        </a>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 animate_half_tc">
                        <a href="#" class="d_block banner wrapper r_corners type_2 relative">
                            <img src="{{asset('/template_resource/images/banner_img_2.png')}}" alt="">
                                    <span class="banner_caption d_block vc_child t_align_c w_sm_auto">
                                        <span class="d_inline_middle">
                                            <span class="d_block scheme_color banner_title type_2 m_bottom_5 m_mxs_bottom_5"><b>-50%</b></span>
                                            <span class="d_block divider_type_2 centered_db m_bottom_5 d_sm_none"></span>
                                            <span class="d_block color_light tt_uppercase m_bottom_15 banner_title_3 m_md_bottom_5 d_mxs_none">On All<br><b>Sunglasses</b></span>
                                            <span class="button_type_6 bg_dark_color tt_uppercase r_corners color_light d_inline_b tr_all_hover box_s_none f_size_ex_large">Shop Now!</span>
                                        </span>
                                    </span>
                        </a>
                    </div>
                </section>
                <!--product brands-->
                <div class="clearfix m_bottom_25 m_sm_bottom_20">
                    <h2 class="tt_uppercase color_dark f_left heading2 animate_fade f_mxs_none m_mxs_bottom_15">Product Brands</h2>
                    <div class="f_right clearfix nav_buttons_wrap animate_fade f_mxs_none">
                        <button class="button_type_7 bg_cs_hover box_s_none f_size_ex_large t_align_c bg_light_color_1 f_left tr_delay_hover r_corners pb_prev"><i class="fa fa-angle-left"></i></button>
                        <button class="button_type_7 bg_cs_hover box_s_none f_size_ex_large t_align_c bg_light_color_1 f_left m_left_5 tr_delay_hover r_corners pb_next"><i class="fa fa-angle-right"></i></button>
                    </div>
                </div>
                <!--product brands carousel-->
                <div class="product_brands m_bottom_45 m_sm_bottom_35">
                    <a href="#" class="d_block t_align_c animate_fade"><img src="{{asset('/template_resource/images/brand_logo.jpg')}}" alt=""></a>
                    <a href="#" class="d_block t_align_c animate_fade"><img src="{{asset('/template_resource/images/brand_logo.jpg')}}" alt=""></a>
                    <a href="#" class="d_block t_align_c animate_fade"><img src="{{asset('/template_resource/images/brand_logo.jpg')}}" alt=""></a>
                    <a href="#" class="d_block t_align_c animate_fade"><img src="{{asset('/template_resource/images/brand_logo.jpg')}}" alt=""></a>
                    <a href="#" class="d_block t_align_c animate_fade"><img src="{{asset('/template_resource/images/brand_logo.jpg')}}" alt=""></a>
                    <a href="#" class="d_block t_align_c animate_fade"><img src="{{asset('/template_resource/images/brand_logo.jpg')}}" alt=""></a>
                    <a href="#" class="d_block t_align_c animate_fade"><img src="{{asset('/template_resource/images/brand_logo.jpg')}}" alt=""></a>
                    <a href="#" class="d_block t_align_c animate_fade"><img src="{{asset('/template_resource/images/brand_logo.jpg')}}" alt=""></a>
                    <a href="#" class="d_block t_align_c animate_fade"><img src="{{asset('/template_resource/images/brand_logo.jpg')}}" alt=""></a>
                    <a href="#" class="d_block t_align_c animate_fade"><img src="{{asset('/template_resource/images/brand_logo.jpg')}}" alt=""></a>
                    <a href="#" class="d_block t_align_c animate_fade"><img src="{{asset('/template_resource/images/brand_logo.jpg')}}" alt=""></a>
                    <a href="#" class="d_block t_align_c animate_fade"><img src="{{asset('/template_resource/images/brand_logo.jpg')}}" alt=""></a>
                </div>
                <!--blog-->
                <div class="row clearfix m_bottom_45 m_sm_bottom_35">
                    <div class="col-lg-6 col-md-6 col-sm-12 m_sm_bottom_35 blog_animate animate_ftr">
                        <div class="clearfix m_bottom_25 m_sm_bottom_20">
                            <h2 class="tt_uppercase color_dark f_left">From The Blog</h2>
                            <div class="f_right clearfix nav_buttons_wrap">
                                <button class="button_type_7 bg_cs_hover box_s_none f_size_ex_large bg_light_color_1 f_left tr_delay_hover r_corners blog_prev"><i class="fa fa-angle-left"></i></button>
                                <button class="button_type_7 bg_cs_hover box_s_none f_size_ex_large bg_light_color_1 f_left m_left_5 tr_delay_hover r_corners blog_next"><i class="fa fa-angle-right"></i></button>
                            </div>
                        </div>
                        <!--blog carousel-->
                        <div class="blog_carousel">
                            <div class="clearfix">
                                <!--image-->
                                <a href="#" class="d_block photoframe relative shadow wrapper r_corners f_left m_right_20 animate_ftt f_mxs_none m_mxs_bottom_10">
                                    <img class="tr_all_long_hover" src="{{asset('/template_resource/images/blog_img_1.jpg')}}" alt="">
                                </a>
                                <!--post content-->
                                <div class="mini_post_content">
                                    <h4 class="m_bottom_5 animate_ftr"><a href="#" class="color_dark"><b>Ut tellus dolor, dapibus eget, elementum vel</b></a></h4>
                                    <p class="f_size_medium m_bottom_10 animate_ftr">25 January, 2013, 5 comments</p>
                                    <p class="m_bottom_10 animate_ftr">Aliquam erat volutpat. Duis ac turpis. Donec sit amet eros. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Mauris fermentum dictum magna. </p>
                                    <a class="tt_uppercase f_size_large animate_ftr" href="#">Read More</a>
                                </div>
                            </div>
                            <div class="clearfix">
                                <!--image-->
                                <a href="#" class="d_block photoframe relative shadow wrapper r_corners f_left m_right_20 animate_ftt f_mxs_none m_mxs_bottom_10">
                                    <img class="tr_all_long_hover" src="{{asset('/template_resource/images/blog_img_2.jpg')}}" alt="">
                                </a>
                                <!--post content-->
                                <div class="mini_post_content">
                                    <h4 class="m_bottom_5 animate_ftr"><a href="#" class="color_dark"><b>Cursus eleifend, elit aenean set amet lorem</b></a></h4>
                                    <p class="f_size_medium m_bottom_10 animate_ftr">30 January, 2013, 6 comments</p>
                                    <p class="m_bottom_10 animate_ftr">Aliquam erat volutpat. Duis ac turpis. Donec sit amet eros. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Mauris fermentum dictum magna. </p>
                                    <a class="tt_uppercase f_size_large animate_ftr" href="#">Read More</a>
                                </div>
                            </div>
                            <div class="clearfix">
                                <!--image-->
                                <a href="#" class="d_block photoframe relative shadow wrapper r_corners f_left m_right_20 animate_ftt f_mxs_none m_mxs_bottom_10">
                                    <img class="tr_all_long_hover" src="{{asset('/template_resource/images/blog_img_3.jpg')}}" alt="">
                                </a>
                                <!--post content-->
                                <div class="mini_post_content">
                                    <h4 class="m_bottom_5 animate_ftr"><a href="#" class="color_dark"><b>In pede mi, aliquet sit ut tellus dolor</b></a></h4>
                                    <p class="f_size_medium m_bottom_10 animate_ftr">1 February, 2013, 12 comments</p>
                                    <p class="m_bottom_10 animate_ftr">Aliquam erat volutpat. Duis ac turpis. Donec sit amet eros. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Mauris fermentum dictum magna. </p>
                                    <a class="tt_uppercase f_size_large animate_ftr" href="#">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--testimonials-->
                    <div class="col-lg-6 col-md-6 col-sm-12 ti_animate animate_ftr">
                        <div class="clearfix m_bottom_25 m_sm_bottom_20">
                            <h2 class="tt_uppercase color_dark f_left f_mxs_none m_mxs_bottom_15">What Our Customers Say</h2>
                            <div class="f_right clearfix nav_buttons_wrap f_mxs_none">
                                <button class="button_type_7 bg_cs_hover box_s_none f_size_ex_large bg_light_color_1 f_left tr_delay_hover r_corners ti_prev"><i class="fa fa-angle-left"></i></button>
                                <button class="button_type_7 bg_cs_hover box_s_none f_size_ex_large bg_light_color_1 f_left m_left_5 tr_delay_hover r_corners ti_next"><i class="fa fa-angle-right"></i></button>
                            </div>
                        </div>
                        <!--testimonials carousel-->
                        <div class="testiomials_carousel">
                            <div>
                                <blockquote class="r_corners shadow f_size_large relative m_bottom_15 animate_ftr">Mauris fermentum dictum magna. Sed laoreet aliquam leo. Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis.</blockquote>
                                <img class="circle m_left_20 d_inline_middle animate_ftr" src="{{asset('/template_resource/images/testimonial_img_1.jpg')}}" alt="">
                                <div class="d_inline_middle m_left_15 animate_ftr">
                                    <h5 class="color_dark"><b>Marta Healy</b></h5>
                                    <p>Los Angeles</p>
                                </div>
                            </div>
                            <div>
                                <blockquote class="r_corners shadow f_size_large relative m_bottom_15">Integer rutrum ante eu lacus.Vestibulum libero nisl, porta vel, scelerisque eget, malesuada at, neque.</blockquote>
                                <img class="circle m_left_20 d_inline_middle" src="{{asset('/template_resource/images/testimonial_img_2.jpg')}}" alt="">
                                <div class="d_inline_middle m_left_15">
                                    <h5 class="color_dark"><b>Alan Smith</b></h5>
                                    <p>New York</p>
                                </div>
                            </div>
                            <div>
                                <blockquote class="r_corners shadow f_size_large relative m_bottom_15">Aenean nec eros. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse sollicitudin velit sed leo. Ut pharetra augue nec augue. Nam elit agna,endrerit sit amet, tincidunt.</blockquote>
                                <img class="circle m_left_20 d_inline_middle" src="{{asset('/template_resource/images/testimonial_img_3.jpg')}}" alt="">
                                <div class="d_inline_middle m_left_15">
                                    <h5 class="color_dark"><b>Anna Johnson</b></h5>
                                    <p>Detroid</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--banners-->
                <div class="row clearfix">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <a href="#" class="d_block animate_ftb h_md_auto m_xs_bottom_15 banner_type_2 r_corners red t_align_c tt_uppercase vc_child n_sm_vc_child">
                                    <span class="d_inline_middle">
                                        <img class="d_inline_middle m_md_bottom_5" src="{{asset('/template_resource/images/banner_img_3.png')}}" alt="">
                                        <span class="d_inline_middle m_left_10 t_align_l d_md_block t_md_align_c">
                                            <b>100% Satisfaction</b><br><span class="color_dark">Guaranteed</span>
                                        </span>
                                    </span>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <a href="#" class="d_block animate_ftb h_md_auto m_xs_bottom_15 banner_type_2 r_corners green t_align_c tt_uppercase vc_child n_sm_vc_child">
                                    <span class="d_inline_middle">
                                        <img class="d_inline_middle m_md_bottom_5" src="{{asset('/template_resource/images/banner_img_4.png')}}" alt="">
                                        <span class="d_inline_middle m_left_10 t_align_l d_md_block t_md_align_c">
                                            <b>Free Shipping</b><br><span class="color_dark">On All Items</span>
                                        </span>
                                    </span>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <a href="#" class="d_block animate_ftb h_md_auto banner_type_2 r_corners orange t_align_c tt_uppercase vc_child n_sm_vc_child">
                                    <span class="d_inline_middle">
                                        <img class="d_inline_middle m_md_bottom_5" src="{{asset('/template_resource/images/banner_img_5.png')}}" alt="">
                                        <span class="d_inline_middle m_left_10 t_align_l d_md_block t_md_align_c">
                                            <b>Great Daily Deals</b><br><span class="color_dark">Shop Now!</span>
                                        </span>
                                    </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!--markup footer-->
        <footer id="footer">
            <div class="footer_top_part">
                <div class="container">
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-3 col-sm-3 m_xs_bottom_30">
                            <h3 class="color_light_2 m_bottom_20">About</h3>
                            <p class="m_bottom_25">Ut pharetra augue nec augue. Nam elit agna, endrerit sit amet, tincidunt ac, viverra sed, nulla. Donec porta diam eu massa. Quisque diam lorem, interdum vitae, dapibus ac, scelerisque.</p>
                            <!--social icons-->
                            <ul class="clearfix horizontal_list social_icons">
                                <li class="facebook m_bottom_5 relative">
                                    <span class="tooltip tr_all_hover r_corners color_dark f_size_small">Facebook</span>
                                    <a href="#" class="r_corners t_align_c tr_delay_hover f_size_ex_large">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li class="twitter m_left_5 m_bottom_5 relative">
                                    <span class="tooltip tr_all_hover r_corners color_dark f_size_small">Twitter</span>
                                    <a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li class="google_plus m_left_5 m_bottom_5 relative">
                                    <span class="tooltip tr_all_hover r_corners color_dark f_size_small">Google Plus</span>
                                    <a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
                                        <i class="fa fa-google-plus"></i>
                                    </a>
                                </li>
                                <li class="rss m_left_5 m_bottom_5 relative">
                                    <span class="tooltip tr_all_hover r_corners color_dark f_size_small">Rss</span>
                                    <a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
                                        <i class="fa fa-rss"></i>
                                    </a>
                                </li>
                                <li class="pinterest m_left_5 m_bottom_5 relative">
                                    <span class="tooltip tr_all_hover r_corners color_dark f_size_small">Pinterest</span>
                                    <a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
                                        <i class="fa fa-pinterest"></i>
                                    </a>
                                </li>
                                <li class="instagram m_left_5 m_bottom_5 relative">
                                    <span class="tooltip tr_all_hover r_corners color_dark f_size_small">Instagram</span>
                                    <a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                                <li class="linkedin m_bottom_5 m_sm_left_5 relative">
                                    <span class="tooltip tr_all_hover r_corners color_dark f_size_small">LinkedIn</span>
                                    <a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                </li>
                                <li class="vimeo m_left_5 m_bottom_5 relative">
                                    <span class="tooltip tr_all_hover r_corners color_dark f_size_small">Vimeo</span>
                                    <a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
                                        <i class="fa fa-vimeo-square"></i>
                                    </a>
                                </li>
                                <li class="youtube m_left_5 m_bottom_5 relative">
                                    <span class="tooltip tr_all_hover r_corners color_dark f_size_small">Youtube</span>
                                    <a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
                                        <i class="fa fa-youtube-play"></i>
                                    </a>
                                </li>
                                <li class="flickr m_left_5 m_bottom_5 relative">
                                    <span class="tooltip tr_all_hover r_corners color_dark f_size_small">Flickr</span>
                                    <a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
                                        <i class="fa fa-flickr"></i>
                                    </a>
                                </li>
                                <li class="envelope m_left_5 m_bottom_5 relative">
                                    <span class="tooltip tr_all_hover r_corners color_dark f_size_small">Contact Us</span>
                                    <a href="#" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
                                        <i class="fa fa-envelope-o"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 m_xs_bottom_30">
                            <h3 class="color_light_2 m_bottom_20">The Service</h3>
                            <ul class="vertical_list">
                                <li><a class="color_light tr_delay_hover" href="#">My account<i class="fa fa-angle-right"></i></a></li>
                                <li><a class="color_light tr_delay_hover" href="#">Order history<i class="fa fa-angle-right"></i></a></li>
                                <li><a class="color_light tr_delay_hover" href="#">Wishlist<i class="fa fa-angle-right"></i></a></li>
                                <li><a class="color_light tr_delay_hover" href="#">Vendor contact<i class="fa fa-angle-right"></i></a></li>
                                <li><a class="color_light tr_delay_hover" href="#">Front page<i class="fa fa-angle-right"></i></a></li>
                                <li><a class="color_light tr_delay_hover" href="#">Virtuemart categories<i class="fa fa-angle-right"></i></a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 m_xs_bottom_30">
                            <h3 class="color_light_2 m_bottom_20">Information</h3>
                            <ul class="vertical_list">
                                <li><a class="color_light tr_delay_hover" href="#">About us<i class="fa fa-angle-right"></i></a></li>
                                <li><a class="color_light tr_delay_hover" href="#">New collection<i class="fa fa-angle-right"></i></a></li>
                                <li><a class="color_light tr_delay_hover" href="#">Best sellers<i class="fa fa-angle-right"></i></a></li>
                                <li><a class="color_light tr_delay_hover" href="#">Manufacturers<i class="fa fa-angle-right"></i></a></li>
                                <li><a class="color_light tr_delay_hover" href="#">Privacy policy<i class="fa fa-angle-right"></i></a></li>
                                <li><a class="color_light tr_delay_hover" href="#">Terms &amp; condition<i class="fa fa-angle-right"></i></a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <h3 class="color_light_2 m_bottom_20">Newsletter</h3>
                            <p class="f_size_medium m_bottom_15">Sign up to our newsletter and get exclusive deals you wont find anywhere else straight to your inbox!</p>
                            <form id="newsletter">
                                <input type="email" placeholder="Your email address" class="m_bottom_20 r_corners f_size_medium full_width" name="newsletter-email">
                                <button type="submit" class="button_type_8 r_corners bg_scheme_color color_light tr_all_hover">Subscribe</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--copyright part-->
            <div class="footer_bottom_part">
                <div class="container clearfix t_mxs_align_c">
                    <p class="f_left f_mxs_none m_mxs_bottom_10">&copy; 2014 <span class="color_light">Flatastic</span>. All Rights Reserved.</p>
                    <ul class="f_right horizontal_list clearfix f_mxs_none d_mxs_inline_b">
                        <li><img src="{{asset('/template_resource/images/payment_img_1.png')}}" alt=""></li>
                        <li class="m_left_5"><img src="{{asset('/template_resource/images/payment_img_2.png')}}" alt=""></li>
                        <li class="m_left_5"><img src="{{asset('/template_resource/images/payment_img_3.png')}}" alt=""></li>
                        <li class="m_left_5"><img src="{{asset('/template_resource/images/payment_img_4.png')}}" alt=""></li>
                        <li class="m_left_5"><img src="{{asset('/template_resource/images/payment_img_5.png')}}" alt=""></li>
                    </ul>
                </div>
            </div>
        </footer>
    </div>
    <!--social widgets-->
    @include('web/partial/social_widgets/main')
<!--    <ul class="social_widgets d_xs_none">
        facebook
        <li class="relative">
            <button class="sw_button t_align_c facebook"><i class="fa fa-facebook"></i></button>
            <div class="sw_content">
                <h3 class="color_dark m_bottom_20">Join Us on Facebook</h3>
                <iframe src="http://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fenvato&amp;width=235&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=438889712801266" style="border:none; overflow:hidden; width:235px; height:258px;"></iframe>
            </div>
        </li>
        twitter feed
        <li class="relative">
            <button class="sw_button t_align_c twitter"><i class="fa fa-twitter"></i></button>
            <div class="sw_content">
                <h3 class="color_dark m_bottom_20">Latest Tweets</h3>
                <div class="twitterfeed m_bottom_25"></div>
                <a role="button" class="button_type_4 d_inline_b r_corners tr_all_hover color_light tw_color" href="https://twitter.com/fanfbmltemplate">Follow on Twitter</a>
            </div>
        </li>
        contact form
        <li class="relative">
            <button class="sw_button t_align_c contact"><i class="fa fa-envelope-o"></i></button>
            <div class="sw_content">
                <h3 class="color_dark m_bottom_20">Contact Us</h3>
                <p class="f_size_medium m_bottom_15">Lorem ipsum dolor sit amet, consectetuer adipis mauris</p>
                <form id="contactform" class="mini">
                    <input class="f_size_medium m_bottom_10 r_corners full_width" type="text" name="cf_name" placeholder="Your name">
                    <input class="f_size_medium m_bottom_10 r_corners full_width" type="email" name="cf_email" placeholder="Email">
                    <textarea class="f_size_medium r_corners full_width m_bottom_20" placeholder="Message" name="cf_message"></textarea>
                    <button type="submit" class="button_type_4 r_corners mw_0 tr_all_hover color_dark bg_light_color_2">Send</button>
                </form>
            </div>
        </li>
        contact info
        <li class="relative">
            <button class="sw_button t_align_c googlemap"><i class="fa fa-map-marker"></i></button>
            <div class="sw_content">
                <h3 class="color_dark m_bottom_20">Store Location</h3>
                <ul class="c_info_list">
                    <li class="m_bottom_10">
                        <div class="clearfix m_bottom_15">
                            <i class="fa fa-map-marker f_left color_dark"></i>
                            <p class="contact_e">8901 Marmora Road,<br> Glasgow, D04 89GR.</p>
                        </div>
                        <iframe class="r_corners full_width" id="gmap_mini" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=ru&amp;geocode=&amp;q=Manhattan,+New+York,+NY,+United+States&amp;aq=0&amp;oq=monheten&amp;sll=37.0625,-95.677068&amp;sspn=65.430355,129.814453&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=%D0%9C%D0%B0%D0%BD%D1%85%D1%8D%D1%82%D1%82%D0%B5%D0%BD,+%D0%9D%D1%8C%D1%8E-%D0%99%D0%BE%D1%80%D0%BA,+%D0%9D%D1%8C%D1%8E+%D0%99%D0%BE%D1%80%D0%BA,+%D0%9D%D1%8C%D1%8E-%D0%99%D0%BE%D1%80%D0%BA&amp;ll=40.790278,-73.959722&amp;spn=0.015612,0.031693&amp;z=13&amp;output=embed"></iframe>
                    </li>
                    <li class="m_bottom_10">
                        <div class="clearfix m_bottom_10">
                            <i class="fa fa-phone f_left color_dark"></i>
                            <p class="contact_e">800-559-65-80</p>
                        </div>
                    </li>
                    <li class="m_bottom_10">
                        <div class="clearfix m_bottom_10">
                            <i class="fa fa-envelope f_left color_dark"></i>
                            <a class="contact_e default_t_color" href="mailto:#">info@companyname.com</a>
                        </div>
                    </li>
                    <li>
                        <div class="clearfix">
                            <i class="fa fa-clock-o f_left color_dark"></i>
                            <p class="contact_e">Monday - Friday: 08.00-20.00 <br>Saturday: 09.00-15.00<br> Sunday: closed</p>
                        </div>
                    </li>
                </ul>
            </div>
        </li>
    </ul>-->

    <!--Add to cart popup-->

    @include('web/partial/cart/popup')


    <!--Add to cart popup-->
    <!--login popup-->
    <div class="popup_wrap d_none" id="login_popup">
        <section class="popup r_corners shadow">
            <button class="bg_tr color_dark tr_all_hover text_cs_hover close f_size_large"><i class="fa fa-times"></i></button>
            <h3 class="m_bottom_20 color_dark">Log In</h3>
            <form onsubmit="return doLogin();">
                <ul>
                    <li class="m_bottom_15">
                        <label for="username" class="m_bottom_5 d_inline_b">Email</label><br>
                        <input type="email" name="" id="username" class="r_corners full_width">
                    </li>
                    <li class="m_bottom_25">
                        <label for="password" class="m_bottom_5 d_inline_b">Password</label><br>
                        <input type="password" name="" id="password" class="r_corners full_width">
                    </li>
                    <li class="m_bottom_15">
                        <input type="checkbox" class="d_none" id="checkbox_10"><label for="checkbox_10">Remember me</label>
                    </li>
                    <li class="clearfix m_bottom_30">
                        <button class="button_type_4 tr_all_hover r_corners f_left bg_scheme_color color_light f_mxs_none m_mxs_bottom_15">Log In</button>
                        <div class="f_right f_size_medium f_mxs_none">
                            <a href="{{ url('/password/reset') }}" class="color_dark">Forgot your password?</a><br>
                        </div>
                    </li>
                </ul>
            </form>
            <div id="msg" class="color_dark"></div>
            <footer class="bg_light_color_1 t_mxs_align_c">
                <h3 class="color_dark d_inline_middle d_mxs_block m_mxs_bottom_15">New Customer?</h3>
                <a href="{{ url('/api/registration/create') }}" role="button" class="tr_all_hover r_corners button_type_4 bg_dark_color bg_cs_hover color_light d_inline_middle m_mxs_left_0">Create an Account</a>
            </footer>
        </section>
    </div>
    <!--custom popup-->
    <button class="t_align_c r_corners tr_all_hover animate_ftl" id="go_to_top"><i class="fa fa-angle-up"></i></button>
    <!--scripts include-->
    <script src="{{asset('/template_resource/js/jquery-2.1.0.min.js')}}"></script>
    <script src="{{asset('/template_resource/js/jquery-migrate-1.2.1.min.js')}}"></script>
    <script src="{{asset('/template_resource/js/retina.js')}}"></script>
    <script src="{{asset('/template_resource/js/camera.min.js')}}"></script>
    <script src="{{asset('/template_resource/js/jquery.easing.1.3.js')}}"></script>
    <script src="{{asset('/template_resource/js/waypoints.min.js')}}"></script>
    <script src="{{asset('/template_resource/js/jquery.isotope.min.js')}}"></script>
    <script src="{{asset('/template_resource/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('/template_resource/js/jquery.tweet.min.js')}}"></script>
    <script src="{{asset('/template_resource/js/jquery.custom-scrollbar.js')}}"></script>
    <script src="{{asset('/template_resource/js/scripts.js')}}"></script>
    <script type="text/javascript" src="{{asset('/template_resource/js/addthis_widget.js')}}"></script>
    <script type="text/javascript" src="{{asset('/developer/json_zipper/jsonZipper.js')}}"></script>

{{--addthis_widget.js downloaded from this link : http://s7.addthis.com/js/300/addthis_widget.js#pubid=xa-5306f8f674bfda4c"--}}
</body>
</html>
<script>
function doLogin(){

    var email = $('#username').val();
    var password = $('#password').val();
    $('#msg').show();
    $('#msg').html("");
    if(email=="" || password == "")
    {
        $('#msg').html("Email or Password field is empty");
        return false;
    }
    $.ajax({
        url: $("#baseUrl").val() + "index.php/login",
        method: "POST",
        data: {
            "email": email,
            "password": password
        },
        success: function (data) {
            console.log(data);
            console.log(data.responseStat.status);
            console.log(data.responseStat.isLogin);
            $('#msg').html(data.responseStat.msg);
            if (data.responseStat.status && data.responseStat.isLogin) {
                $('#msg').delay(1000).fadeOut(500, function () {
                    $('#login_popup').hide();
                });
            }
        }

    });
    return false;
}
window.addEventListener('beforeunload', function() {
   alert("ok");
}, false)
</script>

