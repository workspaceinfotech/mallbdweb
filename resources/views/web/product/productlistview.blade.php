
<!--sort-->
 @if(!empty($productList))
<div class="row clearfix m_bottom_10">
    {{--<div class="col-lg-7 col-md-8 col-sm-12 m_sm_bottom_10">--}}
        {{--<p class="d_inline_middle f_size_medium">Sort by:</p>--}}

        {{--<div class="clearfix d_inline_middle m_left_10">--}}
            {{--<!--product name select-->--}}
            {{--<div class="custom_select f_size_medium relative f_left">--}}
                {{--<div class="select_title r_corners relative color_dark">Product name</div>--}}
                {{--<ul class="select_list d_none"></ul>--}}
                {{--<select name="product_name">--}}
                    {{--<option value="Product SKU">Product SKU</option>--}}
                    {{--<option value="Product Price">Product Price</option>--}}
                    {{--<option value="Product ID">Product ID</option>--}}
                {{--</select>--}}
            {{--</div>--}}
            {{--<button class="button_type_7 bg_light_color_1 color_dark tr_all_hover r_corners mw_0 box_s_none bg_cs_hover f_left m_left_5">--}}
                {{--<i class="fa fa-sort-amount-asc m_left_0 m_right_0"></i></button>--}}
        {{--</div>--}}
        {{--<!--manufacturer select-->--}}
        {{--<div class="custom_select f_size_medium relative d_inline_middle m_left_15 m_xs_left_5 m_mxs_left_0 m_mxs_top_10">--}}
            {{--<div class="select_title r_corners relative color_dark">Select manufacturer</div>--}}
            {{--<ul class="select_list d_none"></ul>--}}
            {{--<select name="manufacturer">--}}
                {{--<option value="Manufacture 1">Manufacture 1</option>--}}
                {{--<option value="Manufacture 2">Manufacture 2</option>--}}
                {{--<option value="Manufacture 3">Manufacture 3</option>--}}
            {{--</select>--}}
        {{--</div>--}}
    {{--</div>--}}
    <?php if (!$mobile) { ?>
    <div class="col-lg-5 col-md-4 col-sm-12 t_align_r t_sm_align_l pull-right">
        <p class="d_inline_middle f_size_medium m_right_5 text-uppercase">View as:</p>

        <div class="clearfix d_inline_middle">
            <?php if(Session::get('productview') == "grid"){ ?>
            <button  style="line-height: 32px;" class="button_type_7 bg_scheme_color color_light tr_delay_hover r_corners mw_0 box_s_none bg_cs_hover f_left" id="grid_button" onclick="changeproductview('grid');">
                <i class="fa fa-th m_left_0 m_right_0"></i></button>
            <button style="line-height: 32px;" class="button_type_7 bg_light_color_1 color_dark tr_delay_hover r_corners mw_0 box_s_none bg_cs_hover f_left m_left_5" id="list_button" onclick="changeproductview('list');">
                <i class="fa fa-th-list m_left_0 m_right_0"></i></button>
            <?php }else{ ?>
            <button style="line-height: 32px;" class="button_type_7 bg_light_color_1 color_dark tr_delay_hover r_corners mw_0 box_s_none bg_cs_hover f_left" id="grid_button" onclick="changeproductview('grid');">
                <i class="fa fa-th m_left_0 m_right_0"></i></button>
            <button style="line-height: 32px;" class="button_type_7 bg_scheme_color color_light tr_delay_hover r_corners mw_0 box_s_none bg_cs_hover f_left m_left_5" id="list_button"  onclick="changeproductview('list');">
                <i class="fa fa-th-list m_left_0 m_right_0" ></i></button>
            <?php } ?>
            
        </div>
    </div>
    <?php } ?>
    
</div>

<!--<hr class="m_bottom_10 divider_type_3">-->
<div class="row clearfix m_bottom_15 filter-bar">
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 m_xs_bottom_10 mobile-paginate">
        
        <p class="d_inline_middle f_size_medium m_left_20 m_xs_left_0">Sort By:</p>
        <select name="selectActionFilter" id="selectActionFilter" onchange="changeActionFilter()">
           <option value="0" <?php if(isset($selected) && $selected && $selectedAction=='0') echo 'selected="selected"'?>>--</option>
           <option value="1" <?php if(isset($selected) && $selected && $selectedAction=='1') echo 'selected="selected"'?>>Price: Lowest First</option>
           <option value="2" <?php if(isset($selected) && $selected && $selectedAction=='2') echo 'selected="selected"'?>>Price: Highest First</option>
           <option value="3" <?php if(isset($selected) && $selected && $selectedAction=='3') echo 'selected="selected"'?>>Product Name: A to Z</option>
           <option value="4" <?php if(isset($selected) && $selected && $selectedAction=='4') echo 'selected="selected"'?>>Product Name: Z to A</option>  
           <option value="5" <?php if(isset($selected) && $selected && $selectedAction=='5') echo 'selected="selected"'?>>In Stock</option>  
           <option value="6" <?php if(isset($selected) && $selected && $selectedAction=='6') echo 'selected="selected"'?>>Rating: Lowest First</option>  
           <option value="7" <?php if(isset($selected) && $selected && $selectedAction=='7') echo 'selected="selected"'?>>Rating: Highest First</option>  
           <option value="8" <?php if(isset($selected) && $selected && $selectedAction=='8') echo 'selected="selected"'?>>Best Seller</option>  
        </select>
        <p class="d_inline_middle f_size_medium m_left_20 m_xs_left_0">Show:</p>
        <!--show items per page select-->
        {{--<div class="custom_select f_size_medium relative d_inline_middle m_left_5">
            <div class="select_title r_corners relative color_dark">9</div>
            <ul class="select_list d_none"></ul>
            <select name="show">
                <option value="Manufacture 1">6</option>
                <option value="Manufacture 2">3</option>
                <option value="Manufacture 3">1</option>
            </select>
        </div>--}}
        <select name="show" id="selectViewSize" onchange="changeAction()">
           <option value="40" <?php if(isset($selected) && $selected && $limit=='40') echo 'selected="selected"'?>>40</option>
           <option value="80" <?php if(isset($selected) && $selected && $limit=='80') echo 'selected="selected"'?>>80</option>
          <option value="120" <?php if(isset($selected) && $selected && $limit=='120') echo 'selected="selected"'?>>120</option>
           <option value="-1" <?php if(isset($selected) && $selected && $limit=='-1') echo 'selected="selected"'?>>Show All</option>
           <!--<option value="50" <?php // if(isset($selected) && $selected && $limit=='50') echo 'selected="selected"'?>>50</option>-->  
        </select>

        <p class="d_inline_middle f_size_medium m_left_5">items per page</p>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 t_align_r t_xs_align_l mobile-pageinate-pos">
        <!--pagination-->
        @if($currentPage>0)
        <a role="button" onclick="prevPage({{{$currentPage}}})" href="javascript:void(0)"
           class="button_type_10 color_dark d_inline_middle bg_cs_hover bg_light_color_1 t_align_c tr_delay_hover r_corners box_s_none"><i
                    class="fa fa-angle-left"></i></a>
        @endif
        <ul id="paginationNumberUl" class="horizontal_list clearfix d_inline_middle f_size_medium m_left_10 paginate-top">
            <?php
            $numberOfPages = ceil($totalProduct / $limit);
            for($i=0;$i<$numberOfPages;$i++){
            ?>
            <li class="m_right_10"><a class="color_dark" href="javascript:void(0)" onclick="specificPage(<?php echo $i;?>)"><?php echo $i+1;?></a></li>
            <?php }?>
        </ul>
        @if($currentPage<$numberOfPages-1)
            <a onclick="nextPage({{{$currentPage}}})" role="button" href="javascript:void(0)"
               class="button_type_10 color_dark d_inline_middle bg_cs_hover bg_light_color_1 t_align_c tr_delay_hover r_corners box_s_none" ><i
                        class="fa fa-angle-right"></i></a>
        @endif
    </div>
</div>
<p class="d_inline_middle f_size_medium d_xs_block m_xs_bottom_5 text-center result-info">Results <?php echo $startProductNumber;  ?> -  <?php echo $endProductNumber;  ?>
         of  {{$totalProduct}}</p>
 @endif
<!--products-->
            <?php
            $countPic =0;
            ?>
<!--product quick view-->
<!--product item-->
<?php
//echo '<pre>';
//print_r($productList);
//die;
?>
 @if(!empty($productList))

    @foreach($productList as $product)
            <div class="popup_wrap d_none" id="quick_view_product_{{$product->id}}" >
                <section class="popup r_corners shadow">
                    <button class="bg_tr color_dark tr_all_hover text_cs_hover close f_size_large"><i class="fa fa-times"></i></button>
                    <div class="clearfix">
                        <div class="custom_scrollbar">
                            <!--left popup column-->
                            <div class="f_left half_column">
                                <div class="relative d_inline_b m_bottom_10 qv_preview">
                                                        <?php if($product->quantity <= 0){ ?>
                                                        <div class="o-tag">
                                                                    <span>hot</span>
                                                         </div>
                                                        <?php                                                        
                                                        }else{
                                                           if(isset($pagename)){
                                                            if (($pagename == 'featured')) {
                                                                ?>
                                                                <div class="f-tag">
                                                                    <span>hot</span>
                                                                </div>
                                                                <?php
                                                            } else if ($pagename == 'special') {
                                                                ?>
                                                                <div class="s-tag">
                                                                    <span>Sale</span>
                                                                </div>
                                                            <?php
                                                            } else if ($pagename == 'bestseller') {
                                                                ?>
                                                                <div class="b-tag">
                                                                    <span>best seller</span>
                                                                </div>
                                                            <?php } else { if($product->createdOn >$filterDate){ ?>
                                                                <div class="h-tag">
                                                                    <span>New</span>
                                                                </div>
                                                            <?php
                                                            }
                                                            }
                                                        }else{
                                                            if (($product->isFeatured)) {
                                                                ?>
                                                                <div class="f-tag">
                                                                    <span>hot</span>
                                                                </div>
                                                                <?php
                                                            } else if ($product->discountActiveFlag) {
                                                                ?>
                                                                <div class="s-tag">
                                                                    <span>Sale</span>
                                                                </div>
                                                                <?php
                                                            } else if ($product->isBestSeller) {
                                                                ?>
                                                                <div class="b-tag">
                                                                    <span>Best Seller</span>
                                                                </div>
                                                            <?php } else { if($product->createdOn >$filterDate){ ?>
                                                                <div class="h-tag">
                                                                    <span>New</span>
                                                                </div>
                                                            <?php
                                                            }
                                                        } 
                                                        }
                                                        } 
                                                        ?>
                                    {{--<span class="hot_stripe"><img src="{{asset('/template_resource/images/sale_product.png')}}" alt=""></span>--}}
                                    @if(@$product->pictures == NULL)
                                            <img id="zoom_image"src="http://placehold.it/360x360?text=No Image Found">
                                    @endif
                                    @foreach(@$product->pictures as $picture)
                                        @if($picture->cover==1)
                                            <?php $countPic++; ?>
                                            <img id="zoom_image{{$countPic}}" src="{{$imageUrl.'product/large/'.$picture->name}}" class="tr_all_hover" alt="" onerror="this.src='http://placehold.it/360x360?text=No Image Found'">
                                        @endif
                                    @endforeach
                                </div>
                                <!--carousel-->

                                <div class="relative qv_carousel_wrap m_bottom_20">
                                    <button class="button_type_11 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_prev">
                                        <i class="fa fa-angle-left "></i>
                                    </button>
                                    <ul class="qv_carousel d_inline_middle">
                                        @foreach($product->pictures as $picture)
                                            <li><img id="small-image{{$picture->name}}" src="{{$imageUrl.'product/thumbnail/'.$picture->name}}" alt="" onclick="changePicture('{{$picture->name}}', '{{ $countPic}}')" onerror="this.src='http://placehold.it/90x90?text=No Image Found'"></li>
                                        @endforeach
                                    </ul>
                                    <button class="button_type_11 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_next">
                                        <i class="fa fa-angle-right "></i>
                                    </button>
                                </div>

                                <div class="d_inline_middle" style="display: none;">Share this:</div>
                                <div class="d_inline_middle m_left_5" style="display: none;">
                                    <!-- AddThis Button BEGIN -->
                                    <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
                                        <a class="addthis_button_preferred_1"></a>
                                        <a class="addthis_button_preferred_2"></a>
                                        <a class="addthis_button_preferred_3"></a>
                                        <a class="addthis_button_preferred_4"></a>
                                        <a class="addthis_button_compact"></a>
                                        <a class="addthis_counter addthis_bubble_style"></a>
                                    </div>
                                    <!-- AddThis Button END -->
                                </div>
                            </div>
                            <!--right popup column-->
                            <div class="p_top_10 t_xs_align_l pop-desc" style="float: right; width: 360px;">
                                <p class="manf-name"><a style="cursor: pointer;" onclick="productByManufacturer('{{$product->manufacturer->id}}');">{{$product->manufacturer->name}}</a></p>
                                <p class="prod-name-detail"><a href="{{url('/product/' . $product->url . '/' . $product->code)}}">{{$product->title}}</a></p>
                               
                               @if($product->discountActiveFlag)
                                <p style="color:red;">Your Save<span><?php echo $currency->HTMLCode; ?> {{ number_format($product->discountAmount,2)}} </span></p>
                             @endif
                                <p class="detail-price">
                                    @if($product->discountActiveFlag)
                                    <span class="older"><?php echo $currency->HTMLCode; ?> {{ number_format($product->prices[0]->retailPrice,2)}}</span><span class="newer"><?php echo $currency->HTMLCode; ?> {{ number_format(($product->prices[0]->retailPrice-$product->discountAmount),2)}}</span>
                                    @else
                                    <span class="newer"><?php echo $currency->HTMLCode; ?> {{ number_format($product->prices[0]->retailPrice,2)}}</span>
                                    @endif
                                </p>
                                <span class="detail-desc"><?php echo $product->description; ?></span>
                                <input type="hidden" id="productJsonObj_{{$product->id}}" value="{{json_encode($product)}}"  />
                                <p class="rate-it">
                                    <ul class="horizontal_list d_inline_middle type_2 clearfix rating_list tr_all_hover">
                                        <?php
                                        $avgRating = ceil($product->avgRating);
                                        if($avgRating>5)
                                        {
                                            $avgRating=5;
                                        }

                                        $left = 5-$avgRating;
                                        ?>
                                        @for($i=0;$i<$avgRating;$i++)
                                            <li class="active">
                                                <i class="fa fa-star-o empty tr_all_hover"></i>
                                                <i class="fa fa-star active tr_all_hover"></i>
                                            </li>
                                        @endfor
                                        @if($left>0)
                                            @for($i=0;$i<$left;$i++)
                                                <li>
                                                    <i class="fa fa-star-o empty tr_all_hover"></i>
                                                    <i class="fa fa-star active tr_all_hover"></i>
                                                </li>
                                            @endfor
                                        @endif
                                         <li>
                                             <span class="rate_number">{{$avgRating}}</span>
                                         </li>   
                                    </ul>
                                <p class="write-review"><a href="#tab-3">Write a review</a></p>

                                 </p>

                                <p class="desc-other-block">

                                    @if($product->isWished)
                                        <a class="favourite-link active" href="" onclick="submitWishList({{$product->id}})"><i class="fa fa-heart"></i>Add to favorites</a>              
                                     @else
                                        <a class="favourite-link relative notify-flyout" href="" onclick="submitWishList('{{$product->id}}','quick_view_product_{{$product->id}}')"><i class="fa fa-heart-o favourite_{{$product->id}}"></i>Add to favorites
                                          <span class="notify-small" id="outeraddnotify{{ $product->id }}" hidden>Product Added</span>
                                          <span class="notify-small" id="outeraddnotify2{{ $product->id }}" hidden>Already Added</span>                                       
                                        </a>    
                                    @endif
                                    <?php if (!$mobile) { ?>
                                         <a class="wishlist-link" href="#" onclick="compare_product('{{$product->id}}');"><i class="fa fa-copy"></i>Compare Product</a>

                                    <?php } ?>

                                </p>
                                
                                <div class="notify-small" id="outeraddnotify{{ $product->id }}"  hidden><span>Product Added</span></div>
                                                 <div class="notify-small" id="outeraddnotify2{{ $product->id }}"  hidden><span>Already Added</span></div>
                                                            @foreach(@$product->attributes as $attributes)
                                                       <div class="add-qn" style="margin-bottom: 25px;">
                                                            <div class="col-md-3">
                                                                {{$attributes->name}}:
                                                            </div>
                                                            <div class="col-md-9">
                                                               <select class="qnty" style="width: 100%;" name="product_name" id="select_attribute_{{$product->id}}_{{$attributes->id}}">
                                                                        @foreach(@$attributes->attributesValue as $attributesValue)
                                                                        <option value="{{@$attributesValue->id}}">{{@$attributesValue->value}}</option>
                                                                        @endforeach
                                                               </select>
                                                            </div>
                                                    
                                                        </div>
                                                            @endforeach
                                                 <div class="add-qn">
                                                    <?php if($product->quantity <= 0){ ?>
                                                    <!--<button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light f_left mw_0" >Out of Stock</button>-->

                                                            <div class="col-md-12">
                                                                <b style="color: red;font-size: 25px;">Out of Stock</b>
                                                            </div>
                                                        <?php }else{ ?>
                                                        <div class="col-md-3">
                                                                <select class="qnty" id="quantity_modifier_{{$product->id}}">                                        
                                                                   @for($i=$product->minimumOrderQuantity;$i<=$product->quantity;$i++)
                                                                    <option value="{{$i}}">Qty- {{$i}}</option>
                                                                    @endfor

                                                                </select>
                                                            </div>
                                                            <div class="col-md-9">
                                                                <button class="btn-new-ad" ng-click="addToCart('productJsonObj_{{$product->id}}',-1,true)">Add to basket</button>
                                                            </div>
                                                    <?php } ?>
                                                    
                                                </div>
                        </div>
                        </div>
                    </div>
                </section>
            </div>
    @endforeach
  @endif
<!--end product quick view-->
<?php if(Session::get('productview') == "grid"){ ?>
<section class="category_grid clearfix m_bottom_15" id="grid_view" >
<?php }else{ ?>
    <section class="category_grid clearfix m_bottom_15" id="grid_view" style="display:none;" >
<?php } ?>

<!--product item-->
 @if(!empty($productList))

    @foreach($productList as $product)
      <div class="product_item w_xs_full col-md-4 margin-zero" style="padding-bottom: 40px;width: 33.3333%;">
                   <figure class="r_corners photoframe type_2 t_align_c tr_all_hover shadow relative">
                                                    <?php if($product->quantity <= 0){ ?>
                                                        <div class="o-tag">
                                                                    <span>hot</span>
                                                         </div>
                                                        <?php                                                        
                                                        }else{
                                                           if(isset($pagename)){
                                                            if (($pagename == 'featured')) {
                                                                ?>
                                                                <div class="f-tag">
                                                                    <span>hot</span>
                                                                </div>
                                                                <?php
                                                            } else if ($pagename == 'special') {
                                                                ?>
                                                                <div class="s-tag">
                                                                    <span>Sale</span>
                                                                </div>
                                                            <?php
                                                            } else if ($pagename == 'bestseller') {
                                                                ?>
                                                                <div class="b-tag">
                                                                    <span>best seller</span>
                                                                </div>
                                                            <?php } else { if($product->createdOn >$filterDate){ ?>
                                                                <div class="h-tag">
                                                                    <span>New</span>
                                                                </div>
                                                            <?php
                                                            }
                                                            }
                                                        }else{
                                                            if (($product->isFeatured)) {
                                                                ?>
                                                                <div class="f-tag">
                                                                    <span>hot</span>
                                                                </div>
                                                                <?php
                                                            } else if ($product->discountActiveFlag) {
                                                                ?>
                                                                <div class="s-tag">
                                                                    <span>Sale</span>
                                                                </div>
                                                                <?php
                                                            } else if ($product->isBestSeller) {
                                                                ?>
                                                                <div class="b-tag">
                                                                    <span>Best Seller</span>
                                                                </div>
                                                            <?php } else { if($product->createdOn >$filterDate){ ?>
                                                                <div class="h-tag">
                                                                    <span>New</span>
                                                                </div>
                                                            <?php
                                                            }
                                                        } 
                                                        }
                                                        } 
                                                        ?>
                       <!--product preview-->
                       <a href="{{url('/product/' . $product->url . '/' . $product->code)}}" class="d_block relative wrapper pp_wrap m_bottom_15">
                           @foreach(@$product->pictures as $picture)
                                 @if($picture->cover==1)
                                       <img src="{{$imageUrl.'product/general/'.@$picture->name}}" onerror="this.src='http://placehold.it/242x242?text=Image not found'" class="tr_all_hover" alt="">     
                                   @endif
                           @endforeach
                           
                           <span id="quick_view_product_{{$product->id}}_trigger_btn"  role="button" data-popup="#quick_view_product_{{$product->id}}"
                                 class="button_type_5 box_s_none color_light r_corners tr_all_hover d_xs_none" onclick="numberOfReview('{{$product->id}}')">Quick View</span>
                       </a>

                       <?php
                       $length = strlen($product->title);
                       if($length>=19)
                       {
                           $editedTitle = substr($product->title, 0, 18)."...";
                       }
                       else{
                           $editedTitle = $product->title;
                       }

                       ?>

                       <!--description and price of product-->
                       <figcaption>
                           <h5 class="m_bottom_10 prod_name_fix text-center"><a href="{{url('/product/' . $product->url . '/' . $product->code)}}" class="color_dark">{{$editedTitle}}</a></h5>
                           <!--rating-->
                           <p class="scheme_color f_size_large f_left item-redesign-price">
                           @if($product->discountActiveFlag)
                               <s class="v_align_b f_size_ex_large price-old"><span><?php echo $currency->HTMLCode; ?></span>{{ number_format($product->prices[0]->retailPrice,2)}}</s><span
                                       class="v_align_b f_size_big m_left_5 scheme_color fw_medium price-usual"><span><?php echo $currency->HTMLCode; ?></span>{{ number_format(($product->prices[0]->retailPrice-$product->discountAmount),2)}} &nbsp;</span>
                           @else
                               <span
                                   class="v_align_b f_size_big m_left_5 scheme_color fw_medium price-usual"><span><?php echo $currency->HTMLCode; ?></span>{{ number_format($product->prices[0]->retailPrice,2)}} &nbsp;</span></p>

                           @endif
                           <?php
                                        $avgRating = ceil($product->avgRating);
                                        if($avgRating>5)
                                        {
                                            $avgRating=5;
                                        }

                                        $left = 5-$avgRating;
                                        ?>
                           <ul class="horizontal_list d_inline_b m_bottom_10 clearfix rating_list type_2 tr_all_hover rating-item">
                               @for($i=0;$i<$avgRating;$i++)
                                   <li class="active">
                                       <i class="fa fa-star-o empty tr_all_hover"></i>
                                       <i class="fa fa-star active tr_all_hover"></i>
                                   </li>
                               @endfor
                               @if($left>0)
                                   @for($i=0;$i<$left;$i++)
                                       <li>
                                           <i class="fa fa-star-o empty tr_all_hover"></i>
                                           <i class="fa fa-star active tr_all_hover"></i>
                                       </li>
                                   @endfor
                               @endif
                           </ul>

                           {{--@if(Auth::check())--}}
                           <div class="clearfix position-relative prod-btn prod-btn-mobile" style="bottom: 0px;">
                                   <?php if($product->quantity <= 0){ ?>
                                    <span class="out-stock-tag">Out of stock</span>
                                    <?php }else{ ?>
                                        <button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light f_left mw_0 item-ad-btn" ng-click="addToCart('productJsonObj_{{$product->id}}',1,false)" >Add to Basket</button>
                                    <?php } ?>
                                   <?php if (!$mobile) { ?>
                                          <button class="button_type_4 bg_light_color_2 tr_all_hover f_right r_corners color_dark mw_0 m_left_5 p_hr_0 item-com-btn" onclick="compare_product('{{$product->id}}');" ><i class="fa fa-files-o"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Compare</span></button>

                                    <?php } ?>
                                   <div class="abs-loader" id="load-img{{ $product->id }}" hidden></div>

                                   @if($product->isWished)
                                       <button id="outer{{ $product->id }}" class="active button_type_4 bg_light_color_2 tr_all_hover f_right r_corners color_dark mw_0 p_hr_0 item-wish-btn" onclick="submitWishList({{$product->id}})"><i class="fa fa-heart-o"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span></button>
                                   @else
                                       <button id="outer{{ $product->id }}" class="button_type_4 bg_light_color_2 tr_all_hover f_right r_corners color_dark mw_0 p_hr_0 item-wish-btn" onclick="submitWishList({{$product->id}})"><i class="fa fa-heart-o"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span></button>
                                   @endif

                                   <div class="notify-small" id="addnotify{{ $product->id }}"  hidden><span>Product Added</span></div>
                                   <div class="notify-small" id="addnotify2{{ $product->id }}"  hidden><span>Already Added</span></div>
                               </div>
                           {{--@else
                               <div class="clearfix position-relative">
                                   <button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light f_left mw_0 item-ad-btn" ng-click="addToCart('productJsonObj_{{$product->id}}',1,false)" >Add to Basket</button>
                                   <button class="button_type_4 bg_light_color_2 tr_all_hover f_right r_corners color_dark mw_0 m_left_5 p_hr_0 item-com-btn" onclick="compare_product('{{$product->id}}');"><i class="fa fa-files-o" data-popup="#login_popup"></i></button>
                                   <button id="outer{{ $product->id }}" class="button_type_4 bg_light_color_2 tr_all_hover f_right r_corners color_dark mw_0 p_hr_0 item-wish-btn" onclick="submitWishList({{$product->id}})"><i class="fa fa-heart-o"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span></button>
                               </div>
                           @endif--}}

                           {{--<button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light mw_0 m_bottom_15 " ng-click="addToCart('productJsonObj_{{$product->id}}',1,false)" >Add to--}}
                               {{--Cart--}}
                           {{--</button>--}}
                           {{--<div class="clearfix m_bottom_5">--}}
                               {{--<ul class="horizontal_list d_inline_b l_width_divider">--}}
                                   {{--<li  id="outer{{ $product->id }}"--}}
                                               {{--class="m_right_15 f_md_none m_md_right_0"><a href="javascript:void(0)" class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light mw_0 m_bottom_15" onclick="submitWishList({{$product->id}})">Add to Wishlist</a>--}}
                                   {{--</li>--}}
                                   {{--<li class="f_md_none"><a href="#" class="color_dark">Add to Compare</a></li>--}}
                               {{--</ul>--}}
                           {{--</div>--}}
                           {{--<div align="center" id="addnotify{{ $product->id }}"  hidden>Product Added</div>--}}
                           {{--<div align="center" id="addnotify2{{ $product->id }}"  hidden>Already Added</div>--}}
                       </figcaption>
                   </figure>
               </div>
    @endforeach
  @endif
</section>
<?php if(Session::get('productview') == "grid"){ ?>
<section class="products_container list_type clearfix m_bottom_5 m_left_0 m_right_0" id="list_view" style="display: none;" >
<?php }else{ ?>
    <section class="products_container list_type clearfix m_bottom_5 m_left_0 m_right_0" id="list_view">
<?php } ?>

    <!--product item-->
 @if(!empty($productList))

    @foreach($productList as $product)
      <div class="product_list full_width list_type hit m_left_0 m_right_0 list-item">
            <figure class="r_corners photoframe tr_all_hover type_2 shadow relative clearfix">
                <!--product preview-->
                
                       <a href="{{url('/product/' . $product->url . '/' . $product->code)}}" class="d_block f_left relative pp_wrap m_right_30 m_xs_right_25 relative product_list_img">
                        <!--hot product-->
                        @foreach(@$product->pictures as $picture)
                                 @if($picture->cover==1)
                                       <img src="{{$imageUrl.'product/general/'.@$picture->name}}" onerror="this.src='http://placehold.it/242x242?text=Image not found'" class="tr_all_hover" alt="">     
                                   @endif
                           @endforeach
                         <span id="quick_view_product_{{$product->id}}_trigger_btn"  role="button" data-popup="#quick_view_product_{{$product->id}}"   class="button_type_5 box_s_none color_light r_corners tr_all_hover d_xs_none" onclick="numberOfReview('{{$product->id}}')">Quick View</span>
                                                   
                                                    <?php if($product->quantity <= 0){ ?>
                                                        <div class="o-tag">
                                                                    <span>hot</span>
                                                         </div>
                                                        <?php                                                        
                                                        }else{
                                                           if(isset($pagename)){
                                                            if (($pagename == 'featured')) {
                                                                ?>
                                                                <div class="f-tag">
                                                                    <span>hot</span>
                                                                </div>
                                                                <?php
                                                            } else if ($pagename == 'special') {
                                                                ?>
                                                                <div class="s-tag">
                                                                    <span>Sale</span>
                                                                </div>
                                                            <?php
                                                            } else if ($pagename == 'bestseller') {
                                                                ?>
                                                                <div class="b-tag">
                                                                    <span>best seller</span>
                                                                </div>
                                                            <?php } else { if($product->createdOn >$filterDate){ ?>
                                                                <div class="h-tag">
                                                                    <span>New</span>
                                                                </div>
                                                            <?php
                                                            }
                                                            }
                                                        }else{
                                                            if (($product->isFeatured)) {
                                                                ?>
                                                                <div class="f-tag">
                                                                    <span>hot</span>
                                                                </div>
                                                                <?php
                                                            } else if ($product->discountActiveFlag) {
                                                                ?>
                                                                <div class="s-tag">
                                                                    <span>Sale</span>
                                                                </div>
                                                                <?php
                                                            } else if ($product->isBestSeller) {
                                                                ?>
                                                                <div class="b-tag">
                                                                    <span>Best Seller</span>
                                                                </div>
                                                            <?php } else { if($product->createdOn >$filterDate){ ?>
                                                                <div class="h-tag">
                                                                    <span>New</span>
                                                                </div>
                                                            <?php
                                                            }
                                                        } 
                                                        }
                                                        } 
                                                        ?>
                    </a>
                

                       <?php
                       $length = strlen($product->title);
                       if($length>=19)
                       {
                           $editedTitle = substr($product->title, 0, 18)."...";
                       }
                       else{
                           $editedTitle = $product->title;
                       }

                       ?>
                <!--description and price of product-->
                <figcaption>
                    <div class="clearfix">
                        <div class="f_left p_list_description f_sm_none w_sm_full m_xs_bottom_10" style="color: #333 !important">
                            <h4 class="fw_medium list-head"><a href="{{url('/product/' . $product->url . '/' . $product->code)}}" class="color_dark">{{$editedTitle}}</a></h4>
                            <p class="manufacturer-title"><label>Manufacturer name:</label>Mayblin</p>
                            

                            <div class="m_bottom_10">
                                <!--rating-->
                                <ul class="horizontal_list d_inline_middle clearfix rating_list type_2 tr_all_hover">
                                    @for($i=0;$i<$avgRating;$i++)
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                    @endfor
                                    @if($left>0)
                                        @for($i=0;$i<$left;$i++)
                                            <li>
                                                <i class="fa fa-star-o empty tr_all_hover"></i>
                                                <i class="fa fa-star active tr_all_hover"></i>
                                            </li>
                                        @endfor
                                    @endif
                                </ul>
                                <a href="#" class="d_inline_middle default_t_color f_size_medium m_left_10">{{$avgRating}} Review(s) </a>
                            </div>
                            <hr class="m_bottom_10">
                            <p class="d_sm_none d_xs_block prod-desc-list" style="margin: 0px !important;"><?php echo $product->description; ?></p>

                            
                        </div>
                        <div class="f_right f_sm_none t_align_c t_sm_align_l" style="text-align: center !important;width: 190px;padding: 7px;">
                            @if($product->discountActiveFlag)                               
                            <p class="scheme_color f_size_large m_bottom_15 price-list"><s class="f_size_ex_large price-old"><?php echo $currency->HTMLCode; ?><span>{{ number_format($product->prices[0]->retailPrice,2)}}</span></s><?php echo $currency->HTMLCode; ?><span class="fw_medium price-usual">{{ number_format(($product->prices[0]->retailPrice-$product->discountAmount),2)}}</span></p>
                           @else
                               <p class="scheme_color f_size_large m_bottom_15 price-list"><span><?php echo $currency->HTMLCode; ?></span><span class="fw_medium price-usual">{{ number_format($product->prices[0]->retailPrice,2)}}</span></p>
                           @endif 
                           <?php if($product->quantity <= 0){ ?>
                           <p class="m_bottom_10"><span class="out-stock-tag">Out of stock</span></p>
                               <?php }else{ ?>
                           <button style="width: 100%;" class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light f_left mw_0 item-ad-btn" ng-click="addToCart('productJsonObj_{{$product->id}}',1,false)" >Add to Basket</button>
                            <?php } ?>
<!--                            <br class="d_sm_none">-->
                            
                            <p class="desc-other-block">
                                <a href="{{url('/product/' . $product->url . '/' . $product->code)}}" class="show-more"><i class="fa fa-plus"></i>Show More</a>
                                 <!--<a class="favourite-link" href="" onclick=""><i class="fa fa-heart-o"></i>Add to favorites</a>-->
                                  @if($product->isWished)
                                        <a class="favourite-link" href="" onclick="submitWishList({{$product->id}})"><i class="fa fa-heart"></i>Add to favorites</a>              
                                     @else
                                        <a class="favourite-link relative notify-flyout" href="" onclick="submitWishList('{{$product->id}}','quick_view_product_{{$product->id}}')"><i class="fa fa-heart-o favourite_{{$product->id}}"></i>Add to favorites
                                          <span class="notify-small" id="outeraddnotify{{ $product->id }}" hidden>Product Added</span>
                                          <span class="notify-small" id="outeraddnotify2{{ $product->id }}" hidden>Already Added</span>                                       
                                        </a>    
                                    @endif
                                     <?php if (!$mobile) { ?>
                                        <a class="wishlist-link" href="#" onclick="compare_product('{{$product->id}}');"><i class="fa fa-copy"></i>Compare Product</a>
                                
                                    <?php } ?>
                                 
                            </p>
                            
                            <button style="display: none;" class="button_type_4 bg_light_color_2 tr_all_hover f_right r_corners color_dark mw_0 m_left_5 p_hr_0 item-com-btn" onclick="compare_product('{{$product->id}}');" ><i class="fa fa-files-o"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Compare</span></button>
                                   <div class="abs-loader" id="load-img{{ $product->id }}" hidden></div>

                                   @if($product->isWished)
                                   <button style="display: none;" id="outer{{ $product->id }}" class="active-wishlist button_type_4 bg_light_color_2 tr_all_hover f_right r_corners color_dark mw_0 p_hr_0 item-wish-btn" onclick="submitWishList({{$product->id}})"><i class="fa fa-heart-o"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span></button>
                                   @else
                                   <button style="display: none;" id="outer{{ $product->id }}" class="button_type_4 bg_light_color_2 tr_all_hover f_right r_corners color_dark mw_0 p_hr_0 item-wish-btn" onclick="submitWishList({{$product->id}})"><i class="fa fa-heart-o"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span></button>
                                   @endif

                                   <div class="notify-small" id="addnotify{{ $product->id }}"  hidden><span>Product Added</span></div>
                                   <div class="notify-small" id="addnotify2{{ $product->id }}"  hidden><span>Already Added</span></div>
                        </div>
                    </div>
                </figcaption>
            </figure>
        </div> 
      
    @endforeach
  @endif    
</section>
@if(!empty($productList))
<hr class="m_bottom_10 divider_type_3">
<div class="row clearfix m_bottom_15 m_xs_bottom_30">
    <div class="col-lg-7 col-md-7 col-sm-12 m_xs_bottom_10 col-xs-12 mobile-paginate">
        <p class="d_inline_middle f_size_medium d_xs_block m_xs_bottom_5">Results <?php echo $startProductNumber;  ?> -  <?php echo $endProductNumber;  ?>
         of  {{$totalProduct}}</p>

        <p class="d_inline_middle f_size_medium m_left_20 m_xs_left_0">Show:</p>
        <!--show items per page select-->
       {{-- <div class="custom_select f_size_medium relative d_inline_middle m_left_5">
            <div class="select_title r_corners relative color_dark">9</div>
            <ul class="select_list d_none"></ul>
            <select name="show_second">
                <option value="Manufacture 1">6</option>
                <option value="Manufacture 2">3</option>
                <option value="Manufacture 3">1</option>
            </select>
        </div>--}}
        <select name="show2" id="selectViewSize2" onchange="changeAction2()" style="width: 90px;">
           <option value="40" <?php if(isset($selected) && $selected && $limit=='40') echo 'selected="selected"'?>>40</option>
           <option value="80" <?php if(isset($selected) && $selected && $limit=='80') echo 'selected="selected"'?>>80</option>
           <option value="120" <?php if(isset($selected) && $selected && $limit=='120') echo 'selected="selected"'?>>120</option>
           <option value="-1" <?php if(isset($selected) && $selected && $limit=='-1') echo 'selected="selected"'?>>Show All</option>
           <!--<option value="50" <?php // if(isset($selected) && $selected && $limit=='50') echo 'selected="selected"'?>>50</option>-->  
        </select>
        <p class="d_inline_middle f_size_medium m_left_5">items per page</p>
    </div>
    <div class="col-lg-5 col-md-5 col-sm-4 t_align_r t_xs_align_l mobile-paginate-pos">
        <!--pagination-->
        @if($currentPage>0)
            <a role="button" onclick="prevPage({{{$currentPage}}})" href="javascript:void(0)"
               class="button_type_10 color_dark d_inline_middle bg_cs_hover bg_light_color_1 t_align_c tr_delay_hover r_corners box_s_none"><i
                        class="fa fa-angle-left"></i></a>
        @endif
        <ul id="paginationNumberUl" class="horizontal_list clearfix d_inline_middle f_size_medium m_left_10">
            <?php
            $numberOfPages = ceil($totalProduct / $limit);
            for($i=0;$i<$numberOfPages;$i++){
            ?>
            <li class="m_right_10"><a class="color_dark" href="javascript:void(0)" onclick="specificPage(<?php echo $i;?>)"><?php echo $i+1;?></a></li>
            <?php }?>
        </ul>
        @if($currentPage<$numberOfPages-1)
            <a onclick="nextPage({{{$currentPage}}})" role="button" href="javascript:void(0)"
               class="button_type_10 color_dark d_inline_middle bg_cs_hover bg_light_color_1 t_align_c tr_delay_hover r_corners box_s_none" ><i
                        class="fa fa-angle-right"></i></a>
        @endif
    </div>
</div>
@endif
       @if(empty($productList))
            <div class="row clearfix text-center">
                <img src="{{asset('/template_resource/images/box.png')}}" />
                 <p class="error-not-found">Hmm,Something went wrong. No Product found</p>
            </div>
       @endif
