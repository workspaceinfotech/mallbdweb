<figure class="widget shadow r_corners wrapper m_bottom_30 wid-cstm hidden-xs">
    <figcaption>
        <h3 class="color_light">Categories</h3>
    </figcaption>
    <div class="widget_content">
        <!--Categories list-->
         @foreach($subcategoryList as $category)
        <ul class="categories_list cat-fix-side">
            <li class="active">

                <label><a href="{{url("category/".urlencode($category->title)."/$category->id")}}">{{$category->title}}</a></label>
                <a href="{{url("category/$category->id")}}" class="f_size_large scheme_color d_block relative">

<!--                   <b>{{$category->title}}</b>-->
                    <span class="bg_light_color_1 r_corners f_right color_dark talign_c"></span>
                </a>
                <!--second level-->
                @if(!empty(@$category->childrens))
                <ul>
                @foreach(@$category->childrens as $firstChild)
                    <li class="active">

                        <label><a href="{{url("category/".urlencode($firstChild->title)."/$category->id/$firstChild->id")}}">{{$firstChild->title}}</a></label>
                        <a href="{{url("category/$category->id/$firstChild->id")}}" class="d_block f_size_large color_dark relative">

                             <span class="bg_light_color_1 r_corners f_right color_dark talign_c"></span>
                        </a>
                        <!--third level-->
                         @if(!empty($firstChild->childrens))
                        <ul>
                          @foreach(@$firstChild->childrens as $secondChild)
                            <li>
                                <a href="{{url("category/".urlencode($secondChild->title)."/$category->id/$firstChild->id/$secondChild->id")}}" class="color_dark d_block">
                                {{$secondChild->title}}</a>
                            </li>
                          @endforeach
                        </ul>
                        @endif
                    </li>
                     @endforeach
                </ul>
                @endif
            </li>
        </ul>
        @endforeach
    </div>
</figure>