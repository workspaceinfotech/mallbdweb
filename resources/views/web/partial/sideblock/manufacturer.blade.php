<figure class="widget shadow r_corners wrapper m_bottom_30">
    <figcaption>
        <h3 class="color_light">Filter</h3>
    </figcaption>
    <div class="widget_content mobile-filter">
        <!--filter form-->
        <form>
            <!--checkboxes-->
            <fieldset class="m_bottom_15">
                <legend class="default_t_color f_size_large m_bottom_15 clearfix full_width relative">
                    <b class="f_left">Manufacturers</b>
                    <button type="button" class="f_size_medium f_right color_dark bg_tr tr_all_hover close_fieldset"><i
                            class="fa fa-times lh_inherit"></i></button>
                </legend>
                @foreach($manufacturers as $manufacturer)
                    <input type="checkbox"  value="{{$manufacturer->id}}" id="manufacturer_{{$manufacturer->id}}" class="ManufacturersCls d_none"  onclick="searchProduct()"><label for="manufacturer_{{$manufacturer->id}}">{{$manufacturer->name}}</label><br>
                @endforeach
            </fieldset>

            <!--price-->
            @if($minMaxPrice['max']>0)
                <fieldset class="m_bottom_20">
                    <legend class="default_t_color f_size_large m_bottom_15 clearfix full_width relative">
                        <b class="f_left">Price</b>
                        <button type="button" class="f_size_medium f_right color_dark bg_tr tr_all_hover close_fieldset"><i
                                class="fa fa-times lh_inherit"></i></button>
                    </legend>
                    <div id="price" class="m_bottom_10"></div>
                    <div class="clearfix range_values">
                        <input class="f_left first_limit" readonly name="" type="text" value="">
                        <input class="f_right last_limit t_align_r" readonly name="" type="text" value="">
                    </div>
                </fieldset>
                <style>
                    .ui-slider::after {
                        background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
                        content: "";
                        display: block;
                        height: 8px;
                        position: absolute;
                        right: 0;
                        top: 0;
                        width: 100%;
                    }
                    .ui-widget-header{
                        background: #b50709 none repeat scroll 0 0 !important;
                        height: 100% !important;
                        position: absolute;
                    }
                </style>
            @endif

                <!--<button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light mw_0 m_bottom_15 pull-right" onclick="searchProduct()">Filter</button>-->
        </form>
    </div>
</figure>