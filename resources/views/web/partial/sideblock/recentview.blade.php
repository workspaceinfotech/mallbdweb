<figure class="widget shadow r_corners wrapper m_bottom_30 wid-cstm">
    <figcaption>
        <a href="{{url('/recentviewproducts')}}"><h3 class="color_light">Recently Viewed</h3></a>
    </figcaption>
    <div class="widget_content text-center">
        <?php if(count($recentProductList) > 0){ ?>
        @foreach($recentProductList as $bestSellerProduct)
            <?php if($bestSellerProduct !=NULL){ ?>
            <div class="clearfix m_bottom_5">

                {{--<a href="{{url('/product/'.$product->id)}}" class="d_block relative wrapper pp_wrap">
                    <img style="max-width: 100% !important;" src="{{$imageUrl.'product/general/'.@$product->pictures[0]->name}}" onerror="this.src='http://placehold.it/242x242'" class="tr_all_hover" alt="">
                    <span id="quick_view_product_{{$product->id}}_trigger_btn" data-popup="#quick_view_product_{{$product->id}}" class="box_s_none button_type_5 color_light r_corners tr_all_hover d_xs_none" onclick="numberOfReview('{{$product->id}}')">Quick View</span>
                </a>--}}


                <div class="col-xs-12">
                    <a href="{{url('/product/' . $bestSellerProduct->url . '/' . $bestSellerProduct->code)}}" class="color_dark d_block bt_link a_fix_bs" style="font-weight: bold;">
                        <img style="max-width: 100% !important;" src="{{$imageUrl.'product/general/'.@$bestSellerProduct->pictures[0]->name}}" onerror="this.src='http://placehold.it/242x242'"  alt="" class="f_left m_right_15 m_sm_bottom_10 f_sm_none f_xs_left m_xs_bottom_0">
                    </a>
                </div>

                <?php                  
                $length = strlen($bestSellerProduct->title);                
                if($length >= 19)
                {
                    $editedTitle = substr($bestSellerProduct->title, 0, 18)."...";
                }
                else{
                    $editedTitle = $bestSellerProduct->title;
                }
                ?>

                <a href="{{url('/product/' . $bestSellerProduct->url . '/' . $bestSellerProduct->code)}}" class="color_dark d_block bt_link" style="font-weight: bold;">{{ $editedTitle }}</a>
                <!--rating-->
                <ul class="horizontal_list clearfix d_inline_b rating_list type_2 tr_all_hover m_bottom_10">
                    <?php
                    $avgRating = ceil($bestSellerProduct->avgRating);
                    if($avgRating>5)
                    {
                        $avgRating=5;
                    }

                    $left = 5-($bestSellerProduct->avgRating);
                    ?>
                    @for($i=0;$i<$avgRating;$i++)
                        <li class="active">
                            <i class="fa fa-star-o empty tr_all_hover"></i>
                            <i class="fa fa-star active tr_all_hover"></i>
                        </li>
                    @endfor
                    @if($left>0)
                        @for($i=0;$i<$left;$i++)
                            <li>
                                <i class="fa fa-star-o empty tr_all_hover"></i>
                                <i class="fa fa-star active tr_all_hover"></i>
                            </li>
                        @endfor
                    @endif
                </ul>
                
                <p class="scheme_color">
                    @if($bestSellerProduct->discountActiveFlag)
                        <s class="v_align_b f_size_ex_large price-old"><span><?php echo $currency->HTMLCode; ?></span>{{number_format($bestSellerProduct->prices[0]->retailPrice,2)}}</s><span
                                class="v_align_b f_size_big m_left_5 scheme_color fw_medium price-usual"><span><?php echo $currency->HTMLCode; ?></span>{{ number_format(($bestSellerProduct->prices[0]->retailPrice-$bestSellerProduct->discountAmount),2)}}</span>
                    @else
                        <span
                                class="v_align_b f_size_big m_left_5 scheme_color fw_medium price-usual"><span><?php echo $currency->HTMLCode; ?></span>{{number_format($bestSellerProduct->prices[0]->retailPrice,2)}}</span>

                    @endif
                </p>                
            </div>
            <?php } ?>
        @endforeach
        <?php }else{ ?>
        No recent product viewed.
        <?php } ?>
    </div>
</figure>