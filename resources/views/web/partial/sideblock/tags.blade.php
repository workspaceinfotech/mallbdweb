<figure class="widget shadow r_corners wrapper m_bottom_30 wid-cstm hidden-xs">
    <figcaption>
        <h3 class="color_light">Tags</h3>
    </figcaption>
    <div class="widget_content">
        <div class="tags_list">
            <a href="{{ url('tags/product?product_tag=skincare') }}" class="color_dark d_inline_b v_align_b">Skin Care</a>
            <a href="{{ url('tags/product?product_tag=woman') }}" class="color_dark d_inline_b f_size_ex_large v_align_b">Woman</a>
            <a href="{{ url('tags/product?product_tag=men') }}" class="color_dark d_inline_b v_align_b">Men</a>
            <a href="{{ url('tags/product?product_tag=bodyshop') }}" class="color_dark d_inline_b f_size_big v_align_b">Body shop</a>
            <a href="{{ url('tags/product?product_tag=cosmetics') }}" class="color_dark d_inline_b v_align_b">Cosmetics</a>
            <a href="{{ url('tags/product?product_tag=Makeup') }}" class="color_dark d_inline_b f_size_large v_align_b">Makeup</a>
            <a href="{{ url('tags/product?product_tag=lipstick') }}" class="color_dark d_inline_b v_align_b">Lipstick</a>
            <a href="{{ url('tags/product?product_tag=makeup') }}" class="color_dark d_inline_b v_align_b">makeup</a>
            <a href="{{ url('tags/product?product_tag=face') }}" class="color_dark d_inline_b v_align_b">face</a>
            <a href="{{ url('tags/product?product_tag=facewash') }}" class="color_dark d_inline_b f_size_ex_large v_align_b">face wash</a>
            <a href="{{ url('tags/product?product_tag=thebodyshop') }}" class="color_dark d_inline_b v_align_b">the body shop</a>
            <a href="{{ url('tags/product?product_tag=bodyshop') }}" class="color_dark d_inline_b f_size_big v_align_b">body shop</a>
        </div>
    </div>
</figure>