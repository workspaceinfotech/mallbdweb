<nav class="navbar navbar-default nav-mobile hidden-lg hidden-md" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigationbar" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="#" class="search-icon-mobile" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search"></i></a>

            <a href="{{url('/')}}" class="navbar-brand mobile-logo">
                <img width="190" src="{{asset('/template_resource/images/logo.png')}}" alt="">
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navigationbar">
            <ul class="nav navbar-nav">
                @foreach($categoryList as $rowData)

                <li class="dropdown" id="parent_id_{{$rowData->id}}">
                    <a href="{{url("category/".urlencode($rowData->title)."/$rowData->id")}}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{$rowData->title}} <span class="caret"></span></a>

                    @if(!empty(@$rowData->childrens))
                    @foreach(@$rowData->childrens as $child)
                    @if(sizeof($child->childrens)>1)
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{url("category/".urlencode($child->title)."/$rowData->id/$child->id")}}" ><b>{{$child->title}}</b></a>
                        </li>
                        <li role="separator" class="divider"></li>
                        @foreach(@$child->childrens as $child2)
                        <li><a href="{{url("category/".urlencode($child2->title)."/$rowData->id/$child->id/$child2->id")}}">{{@$child2->title}}</a></li>
                        @endforeach
                    </ul>

                    @else
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{url("category/".urlencode($child->title)."/$rowData->id/$child->id")}}" >{{$child->title}}</a>
                        </li>
                        <li role="separator" class="divider"></li>
                    </ul>

                    @endif

                    @endforeach
                    @endif
                </li>
                @endforeach
                <li><a href="{{url('/manufacturer')}}" >Brands</a></li> 
                    <li><a href="{{url('/packagelist')}}" >Packages</a></li>
            </ul>
        </div><!-- .navbar-collapse -->
    </div><!-- .container-fluid -->
</nav>