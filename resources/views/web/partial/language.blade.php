<li class="m_left_5 relative container3d">
    <a role="button" href="#" class="button_type_2 color_dark d_block bg_light_color_1 r_corners tr_delay_hover box_s_none" id="lang_button"><img class="d_inline_middle m_right_10 m_mxs_right_0" src="{{asset('/template_resource/images/flag_en.jpg')}}" alt=""><span class="d_mxs_none">English</span></a>
    <ul class="dropdown_list top_arrow color_light">
        <li><a href="#" class="tr_delay_hover color_light"><img class="d_inline_middle" src="{{asset('/template_resource/images/flag_en.jpg')}}" alt="">English</a></li>
        <li><a href="#" class="tr_delay_hover color_light"><img class="d_inline_middle" src="{{asset('/template_resource/images/flag_fr.jpg')}}" alt="">French</a></li>
        <li><a href="#" class="tr_delay_hover color_light"><img class="d_inline_middle" src="{{asset('/template_resource/images/flag_g.jpg')}}" alt="">German</a></li>
        <li><a href="#" class="tr_delay_hover color_light"><img class="d_inline_middle" src="{{asset('/template_resource/images/flag_i.jpg')}}" alt="">Italian</a></li>
        <li><a href="#" class="tr_delay_hover color_light"><img class="d_inline_middle" src="{{asset('/template_resource/images/flag_s.jpg')}}" alt="">Spanish</a></li>
    </ul>
</li>