<script src="{{asset('/template_resource/js/jquery-2.1.0.min.js')}}"></script>
<script src="{{asset('/template_resource/js/jquery-ui.min.js')}}"></script>
<script src="{{asset('/template_resource/js/retina.js')}}"></script>
<script src="{{asset('/template_resource/js/waypoints.min.js')}}"></script>
<script src="{{asset('/template_resource/js/jquery.isotope.min.js')}}"></script>
<script src="{{asset('/template_resource/js/jquery.tweet.min.js')}}"></script>
<script src="{{asset('/template_resource/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('/template_resource/js/jquery.custom-scrollbar.js')}}"></script>
<!--<script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=xa-5306f8f674bfda4c"></script>-->


<script src="{{asset('/template_resource/js/url.min.js')}}"></script>
<script src="{{asset('/developer/js/wishlist/main.js')}}"></script>
<script src="{{asset('/developer/js/login/main.js')}}"></script>
<script src="{{asset('/developer/js/review/reviewCount.js')}}"></script>

<script src="{{asset('/developer/autocomplete/js/jquery.easy-autocomplete.js')}}"></script>
<script src="{{asset('/developer/js/search/main.js')}}"></script>
<script src="{{asset('/developer/js/helper/currencyConvertor.js')}}"></script>
<script>

    function productByManufacturer(i) {
        var searchParamObj = {"pa": []};
        var manufacturersIdList = [];
        manufacturersIdList.push(i);


        if (manufacturersIdList.length > 0) {
            searchParamObj['manufacturers'] = manufacturersIdList;
        }


        var searchParams = (
                searchParamObj.pa.length > 0
                || manufacturersIdList.length > 0
                ) ? "?q=" + encodeURIComponent(JSON.stringify(searchParamObj)) : "";
        var viewSize = 10;
        searchParams += (searchParams != "") ? "&limit=" + viewSize + "&page=0" : "";
//    window.location = location.protocol + '//' + location.host + location.pathname+searchParams;
        console.log(location);
        window.location = $("#baseUrl").val() + 'manufacturer' + searchParams;
    }
    function compare_product(id) {
        window.location.assign($('#baseUrl').val() + 'compareProducts/' + id);
    }
    function compare_package(id) {
        window.location.assign($('#baseUrl').val() + 'comparePackages/' + id);
    }

    function changePictureQuickView(id, picName, count)
    {
        srcOfZoom = $("#small-image" + id).attr('src');
        if (srcOfZoom == "http://placehold.it/90x90?text=No Image Found")
        {
            newSource = ("http://placehold.it/360x360?text=No Image Found");
        }
        else {
            parts = srcOfZoom.split("thumbnail");
            newSource = (parts[0] + "pagelarge/" + picName);
        }

        console.log(newSource);
        $("#zoom_image_package" + count).attr("src", newSource);

    }
    function changeproductview(view) {
        $.ajax({
            url: $("#baseUrl").val() + "changeproductview",
            method: "POST",
            data: {
                "view": view
            },
            success: function(data) {

                if (data.responseStat.status) {
                    if (view == "grid") {
                        $('#list_view').css('display', 'none');
                        $('#grid_view').css('display', 'block');
                        $('#grid_button').removeClass('color_dark');
                        $('#grid_button').removeClass('bg_light_color_1');
                        $('#grid_button').addClass('bg_scheme_color');
                        $('#grid_button').addClass('color_light');

                        $('#list_button').removeClass('color_light');
                        $('#list_button').removeClass('bg_scheme_color');
                        $('#list_button').addClass('bg_light_color_1');
                        $('#list_button').addClass('color_dark');
                    }
                    else {
                        $('#list_view').css('display', 'block');
                        $('#grid_view').css('display', 'none');
                        $('#list_button').removeClass('color_dark');
                        $('#list_button').removeClass('bg_light_color_1');
                        $('#list_button').addClass('bg_scheme_color');
                        $('#list_button').addClass('color_light');

                        $('#grid_button').removeClass('color_light');
                        $('#grid_button').removeClass('bg_scheme_color');
                        $('#grid_button').addClass('bg_light_color_1');
                        $('#grid_button').addClass('color_dark');
                    }


                }
                else {
                    alert('Some problem with server please try again');
                }

            }
        });
    }
    $(document).ready(function() {
        $('.sub_menu_wrap').each(function() {
            if ($(this).find('.sub_menu').length == 0) {
                $(this).addClass('type_2');
            }
        });
    });
</script>
