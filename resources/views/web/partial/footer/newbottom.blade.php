<?php
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
?>
<!--        search modal-->
        <div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Search Our Products</h4>
                    </div>
                    <div class="modal-body">
                        <form role="search" class="d_inline_middle full_width">
                            <input id="searchProduct" type="text" name="search" placeholder="Type text and hit enter" class="f_size_large" onkeypress="doSearch(event)">
                        </form>
                    </div>
                </div>
            </div>
        </div>
<footer id="footer">
    <div class="footer_top_part">
        <div class="container">
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-3 m_xs_bottom_30">
                    <h3 class="color_light_2 m_bottom_20">Office Address</h3>

                    <p class="m_bottom_25">
                        The Mall Dhaka Office, House 6, Road 17/A, Block-E, Banani Dhaka Bangladesh<br>
                        +88 01977300901<br> 01977300902<br>
                        info@themallbd.com
                    </p>
                    <!--social icons-->
                    <ul class="clearfix horizontal_list social_icons">
                        <li class="facebook m_bottom_5 relative">
                            <span class="tooltip tr_all_hover r_corners color_dark f_size_small">Facebook</span>
                            <a target="_blank" href="https://www.facebook.com/themallbd" class="r_corners t_align_c tr_delay_hover f_size_ex_large">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li class="twitter m_left_5 m_bottom_5 relative">
                            <span class="tooltip tr_all_hover r_corners color_dark f_size_small">Twitter</span>
                            <a target="_blank" href="https://twitter.com/mallbd" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li class="google_plus m_left_5 m_bottom_5 relative">
                            <span class="tooltip tr_all_hover r_corners color_dark f_size_small">Google Plus</span>
                            <a target="_blank" href="https://plus.google.com/115620681186065872742 " class="r_corners f_size_ex_large t_align_c tr_delay_hover">
                                <i class="fa fa-google-plus"></i>
                            </a>
                        </li>
                        <li class="pinterest m_left_5 m_bottom_5 relative">
                            <span class="tooltip tr_all_hover r_corners color_dark f_size_small">Pinterest</span>
                            <a target="_blank" href="https://www.pinterest.com/themallbd/ " class="r_corners f_size_ex_large t_align_c tr_delay_hover">
                                <i class="fa fa-pinterest"></i>
                            </a>
                        </li>
                        <li class="instagram m_left_5 m_bottom_5 relative">
                            <span class="tooltip tr_all_hover r_corners color_dark f_size_small">Instagram</span>
                            <a target="_blank"  href="https://www.instagram.com/themallbd " class="r_corners f_size_ex_large t_align_c tr_delay_hover">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                        <li class="linkedin m_bottom_5 m_sm_left_5 relative">
                            <span class="tooltip tr_all_hover r_corners color_dark f_size_small">LinkedIn</span>
                            <a target="_blank" href="https://www.linkedin.com/company/10858584 " class="r_corners f_size_ex_large t_align_c tr_delay_hover">
                                <i class="fa fa-linkedin"></i>
                            </a>
                        </li>
                        <li class="youtube m_left_5 m_bottom_5 relative">
                            <span class="tooltip tr_all_hover r_corners color_dark f_size_small">Tumblr</span>
                            <a target="_blank" href="https://www.tumblr.com/blog/themallbd" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
                                <i class="fa fa-tumblr"></i>
                            </a>
                        </li>
                        <li class="youtube m_left_5 m_bottom_5 relative">
                            <span class="tooltip tr_all_hover r_corners color_dark f_size_small">Youtube</span>
                            <a target="_blank" href="https://www.youtube.com/channel/UCasyoiG4ZXRJ0cHfVVb5uWg" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
                                <i class="fa fa-youtube-play"></i>
                            </a>
                        </li>
                        <li class="youtube m_left_5 m_bottom_5 relative">
                            <span class="tooltip tr_all_hover r_corners color_dark f_size_small">Spapchat</span>
                            <a target="_blank" href="https://snapchat.com/add/themallbd" class="r_corners f_size_ex_large t_align_c tr_delay_hover">
                              
                                <i class="fa fa-snapchat"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 m_xs_bottom_30">
                    <h3 class="color_light_2 m_bottom_20">The Service</h3>
                    <ul class="vertical_list">
                        <li><a class="color_light tr_delay_hover" href="{{ url('manufacturer') }}">Brands<i class="fa fa-angle-right"></i></a></li>
                        <li><a class="color_light tr_delay_hover" href="{{ url('bestsellerproducts') }}">Best Sellers<i class="fa fa-angle-right"></i></a></li>
                        <li><a class="color_light tr_delay_hover" href="{{ url('newproducts') }}">New Products<i class="fa fa-angle-right"></i></a></li>
                        <li><a class="color_light tr_delay_hover" href="{{ url('featuredproducts') }}">Featured Products<i class="fa fa-angle-right"></i></a></li>
                        <li><a class="color_light tr_delay_hover" href="{{ url('specialproducts') }}">Special Discount Products<i class="fa fa-angle-right"></i></a></li>
                        <li><a class="color_light tr_delay_hover" href="{{ url('packagelist') }}">Current Packages<i class="fa fa-angle-right"></i></a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 m_xs_bottom_30">
                    <h3 class="color_light_2 m_bottom_20">Information</h3>
                    <ul class="vertical_list">
                        @foreach($staticPages as $page)
                        <li><a class="color_light tr_delay_hover" href="{{url('page/')}}/{{$page['page_url']}}">{{$page['page_name']}}<i class="fa fa-angle-right"></i></a></li>
                        @endforeach
<!--                        <li><a class="color_light tr_delay_hover" href="{{url('page/aboutus')}}">About us</a></li>
                        <li><a class="color_light tr_delay_hover" href="{{ url('/page/privacypolicy') }}">Privacy policy<i class="fa fa-angle-right"></i></a></li>
                        <li><a class="color_light tr_delay_hover" href="{{ url('/page/terms') }}">Terms &amp; condition<i class="fa fa-angle-right"></i></a></li>
                        <li><a class="color_light tr_delay_hover" href="{{ url('/page/privacypolicy') }}">Delivery Info<i class="fa fa-angle-right"></i></a></li>
                        <li><a class="color_light tr_delay_hover" href="{{ url('/page/paymentinfo') }}">Payment Info<i class="fa fa-angle-right"></i></a></li>
                        <li><a class="color_light tr_delay_hover" href="{{ url('/page/voucher') }}">Voucher Card Rules and regulations <i class="fa fa-angle-right"></i></a></li>-->
                    </ul>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <h3 class="color_light_2 m_bottom_20">Newsletter</h3>

                    <p class="f_size_medium m_bottom_15">Sign up to our newsletter and get exclusive deals you wont find
                        anywhere else straight to your inbox!</p>
                    <span align="center" style="color: yellow;" id="newsalert" hidden></span>
                    <form id="newsletter" >
                        <input type="email" placeholder="Your email address"
                               class="m_bottom_20 r_corners f_size_medium full_width" name="newsletter-email" id="newsletteremail">
                        <button type="submit" class="button_type_8 r_corners bg_scheme_color color_light tr_all_hover" onclick="subscribe()">
                            Subscribe
                        </button>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
    <!--copyright part-->
    <div class="footer_bottom_part">
        <div class="container clearfix t_mxs_align_c">
            <p class="f_left f_mxs_none m_mxs_bottom_10">&copy; 2016 <span ><a href="http://themallbd.com/" class="color_light">The Mall BD</a></span>. All
                Rights Reserved.</p>
            <ul class="f_right horizontal_list clearfix f_mxs_none d_mxs_inline_b">
                <li><img src="{{asset('/template_resource/images/payment_img_1.png')}}" alt=""></li>
                <li class="m_left_5"><img src="{{asset('/template_resource/images/payment_img_2.png')}}" alt=""></li>
                <li class="m_left_5"><img src="{{asset('/template_resource/images/payment_img_3.png')}}" alt=""></li>
                <li class="m_left_5"><img src="{{asset('/template_resource/images/payment_img_4.png')}}" alt=""></li>
                <li class="m_left_5"><img src="{{asset('/template_resource/images/payment_img_5.png')}}" alt=""></li>
            </ul>
        </div>
    </div>
</footer>

<script>

    function subscribe()
    {
        $.ajax({
            url: $("#baseUrl").val()+"api/newsletter/subscribe",
            method: "POST",
            data: {
                "email": $('#newsletteremail').val()
            },
            success: function (data) {
                console.log(data);
                if(data.responseStat.status) {
                    $('#newsalert').html(data.responseStat.msg);
                    $('#newsalert').show();
                    $('#newsalert').fadeOut(2000);
                }
                else{
                    $('#newsalert').html(data.responseStat.msg);
                    $('#newsalert').fadeIn("slow");
                    $('#newsalert').fadeOut(2000);

                }
            }
        });
    }
</script>