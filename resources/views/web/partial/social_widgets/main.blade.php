<!--<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.7&appId=1648526795437995";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>-->
<ul class="social_widgets d_xs_none">
    <!--facebook-->
<!--    <li class="relative">
        <button class="sw_button t_align_c facebook"><i class="fa fa-facebook"></i></button>
        <div class="sw_content">
            <h3 class="color_dark m_bottom_20">Join Us on Facebook</h3>
            <div class="fb-like" data-href="https://www.facebook.com/Mall-1098931143533376/" data-layout="standard" data-action="like" data-size="large" data-show-faces="true" data-share="true"></div>
        </div>
    </li>-->
    <!--twitter feed-->
<!--    <li class="relative">
        <button class="sw_button t_align_c twitter"><i class="fa fa-twitter"></i></button>
        <div class="sw_content">
            <h3 class="color_dark m_bottom_20">Latest Tweets</h3>

            <div class="twitterfeed m_bottom_25"></div>
            <a href="https://twitter.com/riyadcsesust" class="twitter-follow-button" data-show-count="false">Follow @riyadcsesust</a><script async src="http://platform.twitter.com/widgets.js" charset="utf-8"></script>
        </div>
    </li>-->
    <!--contact form-->
    <li class="relative">
        <button class="sw_button t_align_c contact"><i class="fa fa-envelope-o"></i></button>
        <div class="sw_content">
            <h3 class="color_dark m_bottom_20">Contact Us</h3>

            <p class="f_size_medium m_bottom_15" id="success_message"></p>

            <form id="contactform" class="mini">
                <input class="f_size_medium m_bottom_10 r_corners full_width" type="text" id="cf_name" name="name"
                       placeholder="Your name">
                <input class="f_size_medium m_bottom_10 r_corners full_width" type="email" id="cf_email" name="email"
                       placeholder="Email">
                <textarea class="f_size_medium r_corners full_width m_bottom_20" placeholder="Message"
                          id="cf_message" name="message"></textarea>
                <button type="button" onclick="sendContactMessage()" class="button_type_4 r_corners mw_0 tr_all_hover color_dark bg_light_color_2">
                    Send
                </button>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </form>
        </div>
    </li>
    <!--contact info-->
    <li class="relative">
        <button class="sw_button t_align_c googlemap"><i class="fa fa-map-marker"></i></button>
        <div class="sw_content">
            <h3 class="color_dark m_bottom_20">Store Location</h3>
            <ul class="c_info_list">
                <li class="m_bottom_10">
                    <div class="clearfix m_bottom_15">
                        <i class="fa fa-map-marker f_left color_dark"></i>

                        <p class="contact_e">The Mall Dhaka Office, <br>House 6, Road 17/A, Block-E, <br>Banani Dhaka Bangladesh </p>
                    </div>
                    <iframe class="r_corners full_width" id="gmap_mini" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3650.716826035678!2d90.40186231498214!3d23.793096084568436!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c126364cc215%3A0x1dc5b21ba50f04e1!2sRd+17%2C+Dhaka%2C+Bangladesh!5e0!3m2!1sen!2s!4v1470829150183"></iframe>
                    <iframe class="r_corners full_width" id="gmap_mini"
                            src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=ru&amp;geocode=&amp;q=Manhattan,+New+York,+NY,+United+States&amp;aq=0&amp;oq=monheten&amp;sll=37.0625,-95.677068&amp;sspn=65.430355,129.814453&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=%D0%9C%D0%B0%D0%BD%D1%85%D1%8D%D1%82%D1%82%D0%B5%D0%BD,+%D0%9D%D1%8C%D1%8E-%D0%99%D0%BE%D1%80%D0%BA,+%D0%9D%D1%8C%D1%8E+%D0%99%D0%BE%D1%80%D0%BA,+%D0%9D%D1%8C%D1%8E-%D0%99%D0%BE%D1%80%D0%BA&amp;ll=40.790278,-73.959722&amp;spn=0.015612,0.031693&amp;z=13&amp;output=embed"></iframe>
                </li>
                <li class="m_bottom_10">
                    <div class="clearfix m_bottom_10">
                        <i class="fa fa-phone f_left color_dark"></i>

                        <p class="contact_e">800-559-65-80</p>
                    </div>
                </li>
                <li class="m_bottom_10">
                    <div class="clearfix m_bottom_10">
                        <i class="fa fa-envelope f_left color_dark"></i>
                        <a class="contact_e default_t_color" href="mailto:#">info@themallbd.com</a>
                    </div>
                </li>
                <li>
                    <div class="clearfix">
                        <i class="fa fa-clock-o f_left color_dark"></i>

                        <p class="contact_e">Saturday - Thursday: 08.00-20.00  Friday: closed
                        </p>
                    </div>
                </li>
            </ul>
        </div>
    </li>
</ul>
<input type="hidden" id="send_contactus_mail_url" value="{{URL::to('/send-contactus-mail')}}">
<input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">

<script>
    function sendContactMessage() {
//        alert('Under construction');
        $('#contactform :input').css("border-color", "#c3c3c3");

        $.ajax({
            method: "POST",
            url: $('#send_contactus_mail_url').val(),
            data: $('#contactform').serialize(),
            success: function (data, textStatus, jqXHR) {
                if ($("#error_message").length != 0) {
                    document.getElementById("error_message").remove();
                }

                $('#success_message').prepend("<div style='color:green;' id='error_message'>Your message has been sent successfully.</div>")

                $('#cf_name').val('');
                $('#cf_email').val('');
                $('#cf_message').val('');
//                console.log(data);
//                window.location.reload(true);

            },
            error: function (data) {
                var errors = data.responseJSON;
//                console.log(errors);
                var i = 0;

                $.each(errors['message'], function (index, value) {
                    $('#cf_' + index).css("border-color", "red");
                    if (i == 0) {
                        if ($("#error_message").length != 0) {
                            document.getElementById("error_message").remove();
                        }

                        $('#success_message').prepend("<div style='color:red;' id='error_message'>" + value + "</div>")
                        i++;
                    }

                });
            }
        });
    }
</script>