         <!--header bottom part-->
      
<section class="h_bot_part hidden-xs">
    <div class="menu_wrap">
        <div class="container custom-head-wide">
            <div class="clearfix row">
                <!--<div class="col-lg-2 t_md_align_c m_md_bottom_15">-->
                    <!--<a href="" class="logo d_md_inline_b">-->
                        <!--<img src="images/logo.png" alt="">-->
                    <!--</a>-->
                <!--</div>-->
                <div class="col-lg-12 clearfix t_sm_align_c">
                    <div class="clearfix t_sm_align_l f_left f_sm_none relative s_form_wrap m_sm_bottom_15 p_xs_hr_0 m_xs_bottom_5" style="padding-right: 0px;">
                        <!--button for responsive menu-->
                        <button id="menu_button"
                                class="r_corners centered_db d_none tr_all_hover d_xs_block m_xs_bottom_5">
                            <span class="centered_db r_corners"></span>
                            <span class="centered_db r_corners"></span>
                            <span class="centered_db r_corners"></span>
                        </button>
                        <!--main menu-->
                        <nav role="navigation" class="f_left f_xs_none d_xs_none m_right_35 m_md_right_30 m_sm_right_0">
                            <ul class="horizontal_list main_menu type_2 clearfix">
                                @foreach($categoryList as $rowData)
                                    <li class="relative f_xs_none m_xs_bottom_5 m_left_10 m_xs_left_0" id="parent_id_{{$rowData->id}}">
                                        <a href="{{url("category/".urlencode($rowData->title)."/$rowData->id")}}" class="tr_delay_hover color_dark tt_uppercase r_corners"><b>{{$rowData->title}}</b></a>

                                    @if(!empty(@$rowData->childrens))
                                    <!--sub menu-->
                                        <div class="sub_menu_wrap top_arrow d_xs_none tr_all_hover clearfix r_corners w_xs_auto">
                                        @foreach(@$rowData->childrens as $child)
                                        <div class="f_left f_xs_none">
                                            @if(sizeof($child->childrens)>1)
                                            <b class="color_dark m_left_20 m_bottom_5 m_top_5 d_inline_b">
                                                <a class="color_dark tr_delay_hover" href="{{url("category/".urlencode($child->title)."/$rowData->id/$child->id")}}">{{$child->title}}</a>
                                            </b>
                                            @else
                                                <b class="color_dark m_left_20 m_bottom_5 m_top_5 d_inline_b">
                                                    <a class="color_dark tr_delay_hover" href="{{url("category/".urlencode($child->title)."/$rowData->id/$child->id")}}">{{$child->title}}</a>
                                                </b>
                                                <ul class="sub_menu first">
                                                    <li></li>
                                                </ul>
                                                @endif
                                             @foreach(@$child->childrens as $child2)
                                            <ul class="sub_menu first">
                                                <li><a class="color_dark tr_delay_hover" href="{{url("category/".urlencode($child2->title)."/$rowData->id/$child->id/$child2->id")}}">{{@$child2->title}}</a>
                                                </li>
                                            </ul>
                                            @endforeach
                                        </div>
                                        @endforeach
                                        

                                    </div>
                               @endif
                               </li>
                               @endforeach
                               

                                <li class="relative f_xs_none m_xs_bottom_5 m_left_10 m_xs_left_0"><a href="{{ url('packagelist') }}" class="tr_delay_hover color_dark tt_uppercase r_corners"><b>Packages</b></a></li>
                                <li class="relative f_xs_none m_xs_bottom_5 m_left_10 m_xs_left_0"><a href="{{ url('manufacturer') }}" class="tr_delay_hover color_dark tt_uppercase r_corners"><b>Brands</b></a></li>
                            </ul>
                        </nav>
                        <button class="f_right search_button tr_all_hover f_xs_none d_xs_none">
                            <i class="fa fa-search"></i>&nbsp;&nbsp;SEARCH
                        </button>
                        <!--search form-->
                        <div class="searchform_wrap type_2 bg_tr tf_xs_none tr_all_hover w_inherit">
                            <div class="container vc_child h_inherit relative w_inherit cstm-search-block">
                                <form role="search" class="d_inline_middle full_width">
                                    <input id="searchProduct" type="text" name="search" placeholder="Please type and hit enter to search more MALLBD products" class="f_size_large" onkeypress="doSearch(event)">
                                </form>
                                <button class="close_search_form tr_all_hover d_xs_none color_dark">
                                    <i class="fa fa-times"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>