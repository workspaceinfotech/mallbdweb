<ul class="d_inline_b horizontal_list clearfix t_align_l site_settings">
    <!--like-->
    @include('web/partial/like')
    <!--language settings-->
    @include('web/partial/language')
    <!--currency settings-->
    @include('web/partial/currency')
    <!--shopping cart-->
    @include('web/partial/cart/main')
</ul>