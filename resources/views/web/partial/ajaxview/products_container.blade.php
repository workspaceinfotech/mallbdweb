
    <?php
    $countPic =0;
    $productCount = 0;
    $cls="";
    ?>
    <section class="products_container clearfix m_bottom_25 m_sm_bottom_15">
        <!--product item-->
        @foreach($mallBdItemList as $mallBdItem)
            @if($mallBdItem->type == App\Model\DataModel\MallBdItem::_PRODUCT)
                <?php $product = $mallBdItem->item; ?>
                <div class="popup_wrap d_none" id="quick_view_product_{{$product->id}}" >
                    <section class="popup r_corners shadow">
                        <button class="bg_tr color_dark tr_all_hover text_cs_hover close f_size_large"><i class="fa fa-times"></i></button>
                        <div class="clearfix">
                            <div class="custom_scrollbar">
                                <!--left popup column-->
                                <div class="f_left half_column">
                                    <div class="relative d_inline_b m_bottom_10 qv_preview">
                                        {{--<span class="hot_stripe"><img src="{{asset('/template_resource/images/sale_product.png')}}" alt=""></span>--}}
                                        @foreach(@$product->pictures as $picture)
                                            @if($picture->cover==1)
                                                <?php $countPic++;?>
                                                <img id="zoom_image{{$countPic}}" src="{{$imageUrl.'product/pagelarge/'.$picture->name}}" class="tr_all_hover" alt="" onerror="this.src='http://placehold.it/90x90'">
                                            @endif
                                        @endforeach
                                    </div>
                                    <!--carousel-->

                                    <div class="relative qv_carousel_wrap m_bottom_20">
                                        <button class="button_type_11 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_prev">
                                            <i class="fa fa-angle-left "></i>
                                        </button>
                                        <ul class="qv_carousel d_inline_middle">
                                            @foreach(@$product->pictures as $picture)
                                                <li><img id="small-image{{$picture->name}}" src="{{$imageUrl.'product/thumbnail/'.$picture->name}}" onerror="this.src='http://placehold.it/90x90'" alt="" onclick="changePicture('{{$picture->name}}', '{{ $countPic}}')"></li>
                                            @endforeach
                                        </ul>
                                        <button class="button_type_11 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_next">
                                            <i class="fa fa-angle-right "></i>
                                        </button>
                                    </div>

                                    <div class="d_inline_middle">Share this:</div>
                                    <div class="d_inline_middle m_left_5">
                                        <!-- AddThis Button BEGIN -->
                                        <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
                                            <a class="addthis_button_preferred_1"></a>
                                            <a class="addthis_button_preferred_2"></a>
                                            <a class="addthis_button_preferred_3"></a>
                                            <a class="addthis_button_preferred_4"></a>
                                            <a class="addthis_button_compact"></a>
                                            <a class="addthis_counter addthis_bubble_style"></a>
                                        </div>
                                        <!-- AddThis Button END -->
                                    </div>
                                </div>
                                <!--right popup column-->
                                <div class="f_right half_column">
                                    <!--description-->
                                    <h2 class="m_bottom_10"><a href="{{url('/product/'.$product->id)}}" class="color_dark fw_medium">{{$product->title}}</a></h2>
                                    <div class="m_bottom_10">
                                        <!--rating-->
                                        <ul class="horizontal_list d_inline_middle type_2 clearfix rating_list tr_all_hover">

                                            <?php
                                            $avgRating = ceil($product->avgRating);
                                            if($avgRating>5)
                                            {
                                                $avgRating=5;
                                            }

                                            $left = 5-($product->avgRating);
                                            ?>
                                            @for($i=0;$i<$avgRating;$i++)
                                                <li class="active">
                                                    <i class="fa fa-star-o empty tr_all_hover"></i>
                                                    <i class="fa fa-star active tr_all_hover"></i>
                                                </li>
                                            @endfor
                                            @if($left>0)
                                                @for($i=0;$i<$left;$i++)
                                                    <li>
                                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                                        <i class="fa fa-star active tr_all_hover"></i>
                                                    </li>
                                                @endfor
                                            @endif
                                        </ul>
                                        <a href="#" class="d_inline_middle default_t_color f_size_small m_left_5"><span id="reviewCount{{$product->id}}"></span> Review(s) </a>
                                    </div>
                                    <hr class="m_bottom_10 divider_type_3">
                                    <table class="description_table m_bottom_10">
                                        <tr>
                                            <td>Manufacturer:</td>
                                            <td><a href="#" class="color_dark">{{@$product->manufacturer->name}}</a></td>
                                        </tr>
                                        <tr>@if(@$product->quantity>0)
                                                <td>Availability:</td>
                                                <td><span class="color_green">in stock </span>{{@$product->quantity}} item(s)</td>
                                            @else
                                                <td>Availability:</td>
                                                <td><span class="colorpicker_rgb_r">out of stock</span>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td>Product Code:</td>
                                            <td>{{@$product->code}}</td>
                                        </tr>
                                    </table>
                                    <h5 class="fw_medium m_bottom_10">Product Dimensions and Weight</h5>
                                    <table class="description_table m_bottom_5">
                                        <tr>
                                            <td>Product Length(WxHxD):</td>
                                            <td><span class="color_dark">{{@$product->width}}x{{@$product->height}}x{{@$product->depth}}</span></td>
                                        </tr>
                                        <tr>
                                            <td>Product Weight:</td>
                                            <td>{{@$product->weight}} KG</td>
                                        </tr>
                                    </table>
                                    <hr class="divider_type_3 m_bottom_10">
                                    <p class="m_bottom_10"><?php print $product->description; ?></p>
                                    <hr class="divider_type_3 m_bottom_15">
                                    <div class="m_bottom_15">
                                        {{--@if($product->previousPrice>0)
                                            <s class="v_align_b f_size_ex_large">&#2547 {{@$product->previousPrice}}</s>
                                        @endif
                                            <span>&#2547</span> {{@$product->prices[0]->retailPrice}}--}}

                                        @if($product->discountActiveFlag)
                                            <s class="v_align_b f_size_ex_large"><span><?php echo $currency->HTMLCode; ?></span>{{ number_format(($product->prices[0]->retailPrice),2)}}</s><span
                                                    class="v_align_b f_size_big m_left_5 scheme_color fw_medium"><span><?php echo $currency->HTMLCode; ?></span>{{ number_format(($product->prices[0]->retailPrice-$product->discountAmount),2)}}</span>
                                        @else
                                            <span
                                                    class="v_align_b f_size_big m_left_5 scheme_color fw_medium"><span><?php echo $currency->HTMLCode; ?></span>{{ number_format($product->prices[0]->retailPrice,2)}}</span>

                                        @endif
                                    </div>

                                    {{--<s class="v_align_b f_size_ex_large"><span>&#2547</span>{{ $package->originalPriceTotal }}</s><span
                                            class="v_align_b f_size_big m_left_5 scheme_color fw_medium"><span>&#2547</span>{{$package->packagePriceTotal}}</span>--}}

                                    <table class="description_table type_2 m_bottom_15">
                                        @foreach(@$product->attributes as $attributes)
                                            <tr>
                                                <td class="v_align_m">{{$attributes->name}}:</td>
                                                <td class="v_align_m">
                                                    {{--<div class="custom_select f_size_medium relative d_inline_middle">--}}
                                                    {{--<div class="select_title r_corners relative color_dark">Pick</div>--}}
                                                    {{--<ul class="select_list d_none"></ul>--}}


                                                    {{--</div>--}}
                                                    <select name="product_name" id="select_attribute_{{$product->id}}_{{$attributes->id}}">
                                                        @foreach(@$attributes->attributesValue as $attributesValue)
                                                            <option value="{{@$attributesValue->id}}">{{@$attributesValue->value}}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td class="v_align_m">Quantity:</td>
                                            <td class="v_align_m">
                                                <div class="clearfix quantity r_corners d_inline_middle f_size_medium color_dark">
                                                    <button class="bg_tr d_block f_left" data-direction="down">-</button>
                                                    <input id="quantity_modifier_{{$product->id}}"type="text" name="" readonly value="1" class="f_left">
                                                    <button class="bg_tr d_block f_left" data-direction="up">+</button>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <input type="hidden" id="productJsonObj_{{$product->id}}" value="{{json_encode($product)}}"  />
                                    {{-- @if(Auth::check())--}}
                                    <div class="clearfix m_bottom_15 position-relative">
                                        <button class="button_type_12 r_corners bg_scheme_color color_light tr_delay_hover f_left f_size_large" ng-click="addToCart('productJsonObj_{{$product->id}}',-1,true)" >Add to Cart</button>
                                        <div class="abs-loader" id="load-img2{{ $product->id }}" hidden></div>
                                        @if($product->isWished)
                                            <button id="inner{{ $product->id }}" class="active-wishlist button_type_12 bg_light_color_2 tr_delay_hover f_left r_corners color_dark m_left_5 p_hr_0 "  onclick="submitWishList({{$product->id}})"><i class="fa fa-heart-o f_size_big "></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span></button>
                                        @else
                                            <button id="inner{{ $product->id }}" class="button_type_12 bg_light_color_2 tr_delay_hover f_left r_corners color_dark m_left_5 p_hr_0 "  onclick="submitWishList('{{$product->id}}','quick_view_product_{{$product->id}}')"><i class="fa fa-heart-o f_size_big "></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span></button>

                                        @endif
                                        <button class="button_type_12 bg_light_color_2 tr_delay_hover f_left r_corners color_dark m_left_5 p_hr_0"><i class="fa fa-files-o f_size_big"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Compare</span></button>
<!--                                        <button class="button_type_12 bg_light_color_2 tr_delay_hover f_left r_corners color_dark m_left_5 p_hr_0 relative"><i class="fa fa-question-circle f_size_big"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Ask a Question</span></button>-->
                                        <div class="notify-small" id="outeraddnotify{{ $product->id }}"  hidden><span>Product Added</span></div>
                                        <div class="notify-small" id="outeraddnotify2{{ $product->id }}"  hidden><span>Already Added</span></div>
                                    </div>
                                    {{--@else
                                    <div class="clearfix m_bottom_15 position-relative">
                                        <button class="button_type_12 r_corners bg_scheme_color color_light tr_delay_hover f_left f_size_large" ng-click="addToCart('productJsonObj_{{$product->id}}',-1,true)" >Add to Cart</button>
                                        <div class="abs-loader" id="load-img2{{ $product->id }}" hidden></div>
                                        <button id="inner{{ $product->id }}" class="button_type_12 bg_light_color_2 tr_delay_hover f_left r_corners color_dark m_left_5 p_hr_0 "  onclick="submitWishList('{{$product->id}}','quick_view_product_{{$product->id}}')"><i class="fa fa-heart-o f_size_big "></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span></button>
                                        <button class="button_type_12 bg_light_color_2 tr_delay_hover f_left r_corners color_dark m_left_5 p_hr_0"><i class="fa fa-files-o f_size_big"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Compare</span></button>
<!--                                        <button class="button_type_12 bg_light_color_2 tr_delay_hover f_left r_corners color_dark m_left_5 p_hr_0 relative"><i class="fa fa-question-circle f_size_big"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Ask a Question</span></button>-->
                                        <div class="notify-small" id="loginnotify{{ $product->id }}"  hidden><span>Please Login First</span></div>
                                    </div>
                                    @endif--}}
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <?php

                if($product->isFeatured){
                    $cls = " featured";
                }

                if($product->discountActiveFlag)
                {
                    $cls .=" specials";
                }

                if($productCount<4 || (!$product->isFeatured && !$product->discountActiveFlag)){
                    $cls .=" new";

                }
                $productCount++;
                ?>
                <div class="product_item {{ $cls }}"> {{--{{change}}--}}
                    <figure class="r_corners photoframe shadow relative animate_ftb long">
                        <!--product preview-->
                        <a href="{{url('/product/'.$product->id)}}" class="d_block relative wrapper pp_wrap">
                            <img src="{{$imageUrl.'product/general/'.@$product->pictures[0]->name}}" onerror="this.src='http://placehold.it/242x242'" class="tr_all_hover" alt="">
                            <span id="quick_view_product_{{$product->id}}_trigger_btn" data-popup="#quick_view_product_{{$product->id}}" class="box_s_none button_type_5 color_light r_corners tr_all_hover d_xs_none" onclick="numberOfReview('{{$product->id}}')">Quick View</span>
                        </a>
                        <!--description and price of product-->

                        <?php
                        $length = strlen($product->title);
                        if($length>=19)
                        {
                            $editedTitle = substr($product->title, 0, 18)."...";
                        }
                        else{
                            $editedTitle = $product->title;
                        }

                        ?>

                        <figcaption>
                            <h5 class="m_bottom_10 prod_name_fix"><a href="{{url('/product/'.$product->id)}}" class="color_dark">
                                    {{ @$editedTitle }}
                                </a></h5>

                            <div class="clearfix m_bottom_15">
                                <p class="scheme_color f_size_large f_left">
                                    {{--@if($product->previousPrice>0)
                                            <s class="v_align_b f_size_ex_large">&#2547 {{@$product->previousPrice}}</s>
                                    @endif
                                    <span>&#2547 {{@$product->prices[0]->retailPrice}}</span></p>--}}
                                    @if($product->discountActiveFlag)
                                        <s class="v_align_b f_size_ex_large price-old"><span><?php echo $currency->HTMLCode; ?></span>{{number_format($product->prices[0]->retailPrice,2)}}</s><span
                                                class="v_align_b f_size_big m_left_5 scheme_color fw_medium price-usual"><span><?php echo $currency->HTMLCode; ?></span>{{ number_format(($product->prices[0]->retailPrice-$product->discountAmount),2)}}</span>
                                    @else
                                        <span
                                                class="v_align_b f_size_big m_left_5 scheme_color fw_medium price-usual"><span><?php echo $currency->HTMLCode; ?></span>{{number_format($product->prices[0]->retailPrice,2)}}</span>

                                        @endif
                                                <!--rating-->
                                <ul class="horizontal_list f_right clearfix rating_list tr_all_hover">
                                    @for($i=0;$i<$avgRating;$i++)
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                    @endfor

                                    @if($left>0)
                                        @for($i=0;$i<$left;$i++)
                                            <li>
                                                <i class="fa fa-star-o empty tr_all_hover"></i>
                                                <i class="fa fa-star active tr_all_hover"></i>
                                            </li>
                                        @endfor
                                    @endif


                                </ul>
                            </div>
                            <input type="hidden" id="productJsonObj_{{$product->id}}" value="{{json_encode($product)}}"  />
                            {{--@if(Auth::check())--}}
                            <div class="clearfix position-relative">
                                <button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light f_left mw_0" ng-click="addToCart('productJsonObj_{{$product->id}}',1,false)" >Add to Cart</button>
                                <button class="active-wishlist button_type_4 bg_light_color_2 tr_all_hover f_right r_corners color_dark mw_0 m_left_5 p_hr_0" ><i class="fa fa-files-o"></i></button>
                                <div class="abs-loader" id="load-img{{ $product->id }}" hidden></div>
                                @if($product->isWished)

                                    <button id="outer{{ $product->id }}" class="active-wishlist button_type_4 bg_light_color_2 tr_all_hover f_right r_corners color_dark mw_0 p_hr_0" onclick="submitWishList({{$product->id}})"><i class="fa fa-heart-o"></i></button>
                                @else
                                    <button id="outer{{ $product->id }}" class="button_type_4 bg_light_color_2 tr_all_hover f_right r_corners color_dark mw_0 p_hr_0" onclick="submitWishList({{$product->id}})"><i class="fa fa-heart-o"></i></button>
                                @endif
                                <div class="notify-small" id="addnotify{{ $product->id }}"  hidden><span>Product Added</span></div>
                                <div class="notify-small" id="addnotify2{{ $product->id }}"  hidden><span>Already Added</span></div>
                            </div>
                            {{--@else
                            <div class="clearfix position-relative">
                                <button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light f_left mw_0" ng-click="addToCart('productJsonObj_{{$product->id}}',1,false)" >Add to Cart</button>
                                <button class="button_type_4 bg_light_color_2 tr_all_hover f_right r_corners color_dark mw_0 m_left_5 p_hr_0" ><i class="fa fa-files-o" data-popup="#login_popup"></i></button>
                                <button id="outer{{ $product->id }}" class="button_type_4 bg_light_color_2 tr_all_hover f_right r_corners color_dark mw_0 p_hr_0" onclick="submitWishList('{{$product->id}}')"><i class="fa fa-heart-o"></i></button>
                            </div>
                            @endif--}}
                        </figcaption>
                    </figure>
                </div>
            @endif

            @if($mallBdItem->type == App\Model\DataModel\MallBdItem::_PACKAGE)
                <?php $package = $mallBdItem->item; ?>


                <?php
                $length = strlen($package->packageTitle);
                if($length>=19)
                {
                    $editedTitle = substr($package->packageTitle, 0, 18)."...";
                }
                else{
                    $editedTitle = $package->packageTitle;
                }

                ?>


                <div class="product_item hit w_xs_full package">
                    <figure class="r_corners photoframe shadow relative animate_ftb long">
                        <!--product preview-->
                        <a href="{{url('/packages/'.$package->id)}}" class="d_block relative wrapper pp_wrap">
                            <img src="{{ $imageUrl }}package/general/{{@$package->image}}" class="tr_all_hover" alt="">
                                 <span role="button" data-popup="#quick_view_package_{{@$package->id}}"
                                       class="button_type_5 box_s_none color_light r_corners tr_all_hover d_xs_none">Quick View</span>
                        </a>
                        <!--description and price of product-->
                        <figcaption>
                            <h5 class="m_bottom_10" style="height: 50px;"><a href="{{url('/packages/'.$package->id)}}" class="color_dark" target="_blank">{{$editedTitle}}</a></h5>
                            <!--rating-->
                            {{--<ul class="horizontal_list d_inline_b m_bottom_10 clearfix rating_list type_2 tr_all_hover">
                                <li class="active">
                                    <i class="fa fa-star-o empty tr_all_hover"></i>
                                    <i class="fa fa-star active tr_all_hover"></i>
                                </li>
                                <li class="active">
                                    <i class="fa fa-star-o empty tr_all_hover"></i>
                                    <i class="fa fa-star active tr_all_hover"></i>
                                </li>
                                <li class="active">
                                    <i class="fa fa-star-o empty tr_all_hover"></i>
                                    <i class="fa fa-star active tr_all_hover"></i>
                                </li>
                                <li class="active">
                                    <i class="fa fa-star-o empty tr_all_hover"></i>
                                    <i class="fa fa-star active tr_all_hover"></i>
                                </li>
                                <li>
                                    <i class="fa fa-star-o empty tr_all_hover"></i>
                                    <i class="fa fa-star active tr_all_hover"></i>
                                </li>
                            </ul>--}}
                            <div class="clearfix m_bottom_20">
                                <s class="v_align_b f_size_ex_large price-old"><span><?php echo $currency->HTMLCode; ?></span>{{ number_format($package->packagePriceTotal,2)}} </s><span
                                        class="v_align_b f_size_big m_left_5 scheme_color fw_medium price-usual"><span>&nbsp; <?php echo $currency->HTMLCode; ?></span>{{ number_format($package->originalPriceTotal,2)}} &nbsp;</span>
                            </div>
                            <div class="position-relative">
                                <button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light mw_0" ng-click="addPackageToCart('packageJsonObj_{{$package->id}}',1,false)" >Add to
                                    Cart
                                </button>
                                <button class="button_type_4 bg_light_color_2 tr_all_hover f_right r_corners color_dark mw_0 m_left_5 p_hr_0" ><i class="fa fa-files-o"></i></button>
                                <button class="button_type_4 bg_light_color_2 tr_all_hover f_right r_corners color_dark mw_0 p_hr_0"><i class="fa fa-heart-o"></i></button>
                            </div>
                            <div class="clearfix m_bottom_5" style="display: none;">
                                <ul class="horizontal_list d_inline_b l_width_divider">
                                    {{--<li class="m_right_15 f_md_none m_md_right_0"><a href="#" class="color_dark">Add to Wishlist</a>
                                    </li>--}}
                                    {{--<li class="f_md_none"><a href="#" class="color_dark">Add to Compare</a></li>--}}
                                </ul>
                            </div>
                        </figcaption>
                    </figure>
                </div>
                <div class="popup_wrap d_none" id="quick_view_package_{{@$package->id}}">
                    <section class="popup r_corners shadow">
                        <button class="bg_tr color_dark tr_all_hover text_cs_hover close f_size_large"><i class="fa fa-times"></i>
                        </button>
                        <div class="clearfix">
                            <div class="custom_scrollbar">
                                <!--left popup column-->
                                <div class="f_left half_column">
                                    <div class="relative d_inline_b m_bottom_10 qv_preview">
                                        <span class="hot_stripe"><img src="{{asset('/template_resource/images/sale_product.png')}}" alt=""></span>
                                        <img src="{{ $imageUrl }}package/pagelarge/{{@$package->image}}" class="tr_all_hover" alt="" id="zoom_image">
                                    </div>
                                    <!--carousel-->
                                    <div class="relative qv_carousel_wrap m_bottom_20">
                                        <button class="button_type_11 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_prev">
                                            <i class="fa fa-angle-left "></i>
                                        </button>
                                        <ul class="qv_carousel d_inline_middle">
                                            <a href="javascript:void(0)" data-image="images/quick_view_img_7.jpg')}}" data-zoom-image="images/preview_zoom_1.jpg')}}"><img id="small-image{{@$package->image}}"
                                                                                                                                                                           src="{{ $imageUrl }}package/thumbnail/{{@$package->image}}" alt="" onerror="this.src='http://placehold.it/90x90?text=Product Banner not found'" onclick="changePicture('{{@$package->image}}')"></a>

                                            @foreach($package->packageProduct as $products)
                                                <a href="javascript:void(0)" data-image="images/quick_view_img_7.jpg')}}" data-zoom-image="images/preview_zoom_1.jpg')}}"><img id="small-image{{@$products->product->pictures[0]->name}}"
                                                                                                                                                                               src="{{ $imageUrl }}product/thumbnail/{{@$products->product->pictures[0]->name}}" alt="" onerror="this.src='http://placehold.it/90x90?text=Product Banner not found'" onclick="changePicture('{{@$products->product->pictures[0]->name}}')"></a>
                                            @endforeach
                                        </ul>
                                        <button class="button_type_11 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_next">
                                            <i class="fa fa-angle-right "></i>
                                        </button>
                                    </div>
                                    <div class="d_inline_middle">Share this:</div>
                                    <div class="d_inline_middle m_left_5">
                                        <!-- AddThis Button BEGIN -->
                                        <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
                                            <a class="addthis_button_preferred_1"></a>
                                            <a class="addthis_button_preferred_2"></a>
                                            <a class="addthis_button_preferred_3"></a>
                                            <a class="addthis_button_preferred_4"></a>
                                            <a class="addthis_button_compact"></a>
                                            <a class="addthis_counter addthis_bubble_style"></a>
                                        </div>
                                        <!-- AddThis Button END -->
                                    </div>
                                </div>
                                <!--right popup column-->
                                <div class="f_right half_column">
                                    <!--description-->
                                    <h2 class="m_bottom_10"><a href="{{url('/packages/'.$package->id)}}" class="color_dark fw_medium" target="_blank">{{$package->packageTitle}}</a></h2>

                                    {{--<div class="m_bottom_10">
                                        <!--rating-->
                                        <ul class="horizontal_list d_inline_middle type_2 clearfix rating_list tr_all_hover">
                                            <li class="active">
                                                <i class="fa fa-star-o empty tr_all_hover"></i>
                                                <i class="fa fa-star active tr_all_hover"></i>
                                            </li>
                                            <li class="active">
                                                <i class="fa fa-star-o empty tr_all_hover"></i>
                                                <i class="fa fa-star active tr_all_hover"></i>
                                            </li>
                                            <li class="active">
                                                <i class="fa fa-star-o empty tr_all_hover"></i>
                                                <i class="fa fa-star active tr_all_hover"></i>
                                            </li>
                                            <li class="active">
                                                <i class="fa fa-star-o empty tr_all_hover"></i>
                                                <i class="fa fa-star active tr_all_hover"></i>
                                            </li>
                                            <li>
                                                <i class="fa fa-star-o empty tr_all_hover"></i>
                                                <i class="fa fa-star active tr_all_hover"></i>
                                            </li>
                                        </ul>
                                        <a href="#" class="d_inline_middle default_t_color f_size_small m_left_5">1 Review(s) </a>
                                    </div>--}}

                                    <hr class="divider_type_3 m_bottom_10">
                                    <p class="m_bottom_10"><?php echo $package->description;?></p>
                                    <hr class="divider_type_3 m_bottom_15">
                                    <div class="m_bottom_15">
                                        <s class="v_align_b f_size_ex_large"><span><?php echo $currency->HTMLCode; ?></span>{{ number_format($package->originalPriceTotal,2) }}</s><span
                                                class="v_align_b f_size_big m_left_5 scheme_color fw_medium"><span>&nbsp;&nbsp;<?php echo $currency->HTMLCode; ?></span>{{ number_format($package->packagePriceTotal,2) }}</span>
                                    </div>

                                    <div class="clearfix m_bottom_15">
                                        <button class="button_type_12 r_corners bg_scheme_color color_light tr_delay_hover f_left f_size_large">
                                            Add to Cart
                                        </button>
                                        {{--<button class="button_type_12 bg_light_color_2 tr_delay_hover f_left r_corners color_dark m_left_5 p_hr_0">
                                            <i class="fa fa-heart-o f_size_big"></i><span
                                                class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span></button>--}}
                                        <button class="button_type_12 bg_light_color_2 tr_delay_hover f_left r_corners color_dark m_left_5 p_hr_0">
                                            <i class="fa fa-files-o f_size_big"></i><span
                                                    class="tooltip tr_all_hover r_corners color_dark f_size_small">Compare</span></button>
<!--                                        <button class="button_type_12 bg_light_color_2 tr_delay_hover f_left r_corners color_dark m_left_5 p_hr_0 relative">
                                            <i class="fa fa-question-circle f_size_big"></i><span
                                                    class="tooltip tr_all_hover r_corners color_dark f_size_small">Ask a Question</span>
                                        </button>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <input type="hidden" id="packageJsonObj_{{$package->id}}" value="{{json_encode($package)}}"  />

            @endif

        @endforeach
    </section>

    <script type="application/javascript">
      $(document).ready(function(){
          var globalDfd = $.Deferred();
          globalDfd.done(function () {
              $('.home_products_container:not(.a_type_2) .photoframe.animate_ftb').waypointSynchronise({
                  container: '.home_products_container',
                  delay: 0,
                  offset: 700,
                  classN: "animate_vertical_finished"
              });
              $('.home_products_container.a_type_2 .photoframe.animate_ftb').waypointSynchronise({
                  container: '.home_products_container',
                  delay: 0,
                  offset: 700,
                  classN: "animate_vertical_finished"
              });

          });

          if ($('.home_products_container').length) {

              var container = $('.products_container');

              container.isotope({
                  itemSelector: '.product_item',
                  layoutMode: 'fitRows'
              });

              // filter items when filter link is clicked

              $('.isotope_menu').on('click', 'button', function () {
                  var self = $(this),
                          selector = self.attr('data-filter');
                  randomSort.call(self, self.data('filter'), container.find('.product_item'));
                  self.closest('li').addClass('active').siblings().removeClass('active');
                  container.isotope({ filter: selector });
              });
          }

      });
    </script>
