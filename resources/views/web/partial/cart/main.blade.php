<?php if ($mobile) { ?>
    <li class="m_left_5 relative container3d" id="shopping_button" style="display: none;"  ng-mouseover="checkCartIsUpdated()"  >
<?php } else { ?>
   <li class="m_left_5 relative container3d" id="shopping_button"  ng-mouseover="checkCartIsUpdated()"  >
<?php } ?>
       
    <a role="button" href="#" class="button_type_3 color_light bg_scheme_color d_block r_corners tr_delay_hover box_s_none top-cart-btn only-single">
        <span class="d_inline_middle shop_icon m_mxs_right_0">
            <i class="fa fa-shopping-cart"></i>
            <span class="count tr_delay_hover type_2 circle t_align_c" ng-show="shoppingCart.productCell.length > 0 || shoppingCart.mallBdPackageCell.length > 0" ng-bind="getTotalQuantity()" ></span>
        </span>
        <b class="d_mxs_none"  ><span ng-bind-html="currencySymbol"  ></span>&nbsp;<span ng-bind="getTotalAmount()"  ></span></b>
    </a>
    <div class="shopping_cart top_arrow tr_all_hover r_corners cart_top_right">
        <div class="pop-mall-f" style="display: none;" id="checkout_loader_cart">
	
        </div>
        <div ng-show="shoppingCart.productCell.length > 0 || shoppingCart.mallBdPackageCell.length > 0" class="f_size_medium sc_header fly-cart-bg text-center"><span class="add_to_cart_success cart-success text-center">Recently added item(s)</span></div>
        <div ng-show="shoppingCart.productCell.length == 0 && shoppingCart.mallBdPackageCell.length == 0" class="f_size_medium sc_header fly-cart-bg text-center"><span class="add_to_cart_success cart-success text-center">Your Shopping Basket Empty</span></div>
        <p class="alert-txt product_qutantity_msg" style="font-size: 14px;    margin-bottom: 4px;"></p>
        <!--        <ul  ng-if="lastItemAddedInCart==_PRODUCT" class="products_list">
                    <li ng-repeat="productCell in shoppingCart.productCell track by $index">
        
                        <div class="clearfix">
                            {{--onerror="this.src='http://placehold.it/62x62?text=Image not found'"--}}
                            product image
                            <img class="f_left m_right_10"style="width: 62px;height: 62px" src="@{{getPictureUrl(productCell.product.pictures[0].name)}}" alt="">
                            product description
                            <div class="f_left product_description" >
                                <a ng-href="{{url('/product/')}}/@{{ productCell.product.id }}" class="color_dark m_bottom_5 d_block" ng-bind="productCell.product.title"></a>
                                <div  ng-repeat="selectedAttribute in productCell.selectedAttributes" >
                                    <span class="f_size_medium" ng-bind="selectedAttribute.name"></span>
                                    <span class="f_size_medium" >:</span>
                                    <span class="f_size_medium" ng-bind="selectedAttribute.value"></span>
                                </div>
                            </div>
                            product price
                            <div class="f_left f_size_medium">
                                <div class="clearfix">
                                    <span ng-bind="productCell.quantity"></span> x <b class="color_dark" ng-bind="productCell.product.prices[0].retailPrice"></b>
                                </div>
                                {{--close_product--}}
                                <button class="color_dark tr_hover" style="background: transparent"><i class="fa fa-times" ng-click="removeProductCell($index)"></i></button>
                                <button style="background: transparent" ng-show="productCell.quantity>0" ><i ng-click="decreaseProductInProductCell($index)" class="fa fa-minus" ></i></button>
                            </div>
                        </div>
                    </li>
                    <li ng-repeat="mallBdPackageCell in shoppingCart.mallBdPackageCell track by $index">
                        <div class="clearfix">
                            {{--onerror="this.src='http://placehold.it/62x62?text=Image not found'"--}}
                            product image
                            <img class="f_left m_right_10"style="width: 62px;height: 62px" src="@{{getPackagesPictureUrl(mallBdPackageCell.mallBdPackage.image)}}"  alt="">
                            product description
                            <div class="f_left product_description" >
                                <a ng-href="{{url('/packages/')}}/@{{ mallBdPackageCell.mallBdPackage.id }}" class="color_dark m_bottom_5 d_block" ng-bind="mallBdPackageCell.mallBdPackage.packageTitle"></a>
                            </div>
                            product price
                            <div class="f_left f_size_medium">
                                <div class="clearfix">
                                    <span ng-bind="mallBdPackageCell.quantity"></span> x <b class="color_dark" ng-bind="mallBdPackageCell.mallBdPackage.packagePriceTotal"></b>
                                </div>
                                {{--close_product--}}
                                <button class="color_dark tr_hover" style="background: transparent"><i class="fa fa-times" ng-click="removePackageCell($index)"></i></button>
                                <button style="background: transparent" ng-show="mallBdPackageCell.quantity>0" ><i ng-click="decreasePackageInPackageCell($index)" class="fa fa-minus" ></i></button>
                            </div>
                        </div>
                    </li>
                </ul>-->
        <!--        <ul  ng-if="lastItemAddedInCart==_PACKAGE" class="products_list">
                    <li ng-repeat="mallBdPackageCell in shoppingCart.mallBdPackageCell track by $index">
                        <div class="clearfix">
                            {{--onerror="this.src='http://placehold.it/62x62?text=Image not found'"--}}
                            product image
                            <img class="f_left m_right_10"style="width: 62px;height: 62px" src="@{{getPackagesPictureUrl(mallBdPackageCell.mallBdPackage.image)}}"  alt="">
                            product description
                            <div class="f_left product_description" >
                                <a ng-href="{{url('/packages/')}}/@{{ mallBdPackageCell.mallBdPackage.id }}" class="color_dark m_bottom_5 d_block" ng-bind="mallBdPackageCell.mallBdPackage.packageTitle"></a>
                            </div>
                            product price
                            <div class="f_left f_size_medium">
                                <div class="clearfix">
                                    <span ng-bind="mallBdPackageCell.quantity"></span> x <b class="color_dark" ng-bind="mallBdPackageCell.mallBdPackage.packagePriceTotal"></b>
                                </div>
                                {{--close_product--}}
                                <button class="color_dark tr_hover" style="background: transparent"><i class="fa fa-times" ng-click="removePackageCell($index)"></i></button>
                                <button style="background: transparent" ng-show="mallBdPackageCell.quantity>0" ><i ng-click="decreasePackageInPackageCell($index)" class="fa fa-minus" ></i></button>
                            </div>
                        </div>
                    </li>
                    <li ng-repeat="productCell in shoppingCart.productCell track by $index">
        
                        <div class="clearfix">
                            {{--onerror="this.src='http://placehold.it/62x62?text=Image not found'"--}}
                            product image
                            <img class="f_left m_right_10"style="width: 62px;height: 62px" src="@{{getPictureUrl(productCell.product.pictures[0].name)}}" alt="">
                            product description
                            <div class="f_left product_description" >
                                <a ng-href="{{url('/product/')}}/@{{ productCell.product.id }}" class="color_dark m_bottom_5 d_block" ng-bind="productCell.product.title"></a>
                                <div  ng-repeat="selectedAttribute in productCell.selectedAttributes" >
                                    <span class="f_size_medium" ng-bind="selectedAttribute.name"></span>
                                    <span class="f_size_medium" >:</span>
                                    <span class="f_size_medium" ng-bind="selectedAttribute.value"></span>
                                </div>
                            </div>
                            product price
                            <div class="f_left f_size_medium">
                                <div class="clearfix">
                                    <span ng-bind="productCell.quantity"></span> x <b class="color_dark" ng-bind="productCell.product.prices[0].retailPrice"></b>
                                </div>
                                {{--close_product--}}
                                <button class="color_dark tr_hover" style="background: transparent"><i class="fa fa-times" ng-click="removeProductCell($index)"></i></button>
                                <button style="background: transparent" ng-show="productCell.quantity>0" ><i ng-click="decreaseProductInProductCell($index)" class="fa fa-minus" ></i></button>
                            </div>
                        </div>
                    </li>
        
                </ul>-->
        <ul ng-if="lastItemAddedInCart==_PRODUCT" class="products_list add_cart_product_list add_cart_product_list_cstm">

            <li ng-repeat="productCell in shoppingCart.productCell track by $index" >
                <div class="clearfix">
                    <!--product image-->
                    <div ng-show="isEmpty(getPictureUrl(productCell.product.pictures[0].name))">
                        <img class="f_left m_right_10" style="width: 62px;height: 62px" src="http://placehold.it/90x90?text=Image not found"  alt="">
                    </div>
                    <div ng-show="!isEmpty(getPictureUrl(productCell.product.pictures[0].name))">
                        <img class="f_left m_right_10" style="width: 62px;height: 62px" src="@{{getPictureUrl(productCell.product.pictures[0].name)}}"  alt="">
                    </div>
                    <!--product description-->
                    <div class="f_left product_description">
                        <a ng-href="{{url('/product/')}}/@{{ productCell.product.url }}/@{{ productCell.product.code }}"  class="color_dark m_bottom_5 d_block prod_name pop-cart-desc" ng-bind="productCell.product.title" ></a>

                        <p class="color_dark pop-cart-price">Price: <span ng-if="productCell.product.discountActiveFlag" class="line-through--for-cart-price" ng-bind="getPriceIndividual($index)" ></span><span ng-if="productCell.product.discountActiveFlag"  ng-bind="getPriceAfterDiscount($index)" > </span><span ng-if="!productCell.product.discountActiveFlag" ng-bind="getPriceIndividual($index)" > </span></p>
                        <p class="fw_medium scheme_color pop-cart-price">Subtotal: <span ng-bind="getPrice($index)"></span></p>
                        <div class="pop-cart-price"  ng-repeat="selectedAttribute in productCell.selectedAttributes" >
                            <span class="f_size_medium" ng-bind="selectedAttribute.name"></span>
                            <span class="f_size_medium" >:</span>
                            <span class="f_size_medium" ng-bind="selectedAttribute.value"></span>
                        </div>
                    </div>
                    <!--product price-->
                    <div class="f_right f_size_medium">
                        <!--                                    <div class="clearfix">
                                                                <span ng-bind="productCell.quantity"></span> x <b class="color_dark" ng-bind="productCell.product.prices[0].retailPrice"></b>
                                                            </div>-->
                        <div class="clearfix quantity r_corners d_inline_middle f_size_medium color_dark m_bottom_10 pull-left">
                            <button class="bg_tr d_block f_left" data-direction="down"  ng-click="decreaseProductInProductCell($index)" >-</button>
                            <!--<input type="text" name="" readonly ng-value="getProductQuantity($index)" class="f_left">-->
                            <span style="margin-top: 6px;" ng-bind="getProductQuantity($index)" class="f_left"></span>
                            <button class="bg_tr d_block f_left" data-direction="up" ng-click="increaseProductInProductCellCartMain($index)" >+</button>
                        </div>
<!--                                    <button class="close_product color_dark tr_hover"><i class="fa fa-times" ng-click="removeProductCell($index)"></i></button>-->
                        <a href="javascript:void(0)" ng-click="removeProductCell($index)" class="color_dark pull-left" style="margin: 8px 0px 0px 5px;"><i class="fa fa-times f_size_medium m_right_5"></i></a>
                        <!--<button style="background: transparent" ng-show="productCell.quantity>0" ><i ng-click="decreaseProductInProductCell($index)" class="fa fa-minus" ></i></button>-->
                    </div>
                </div>
            </li>
            <li ng-repeat="mallBdPackageCell in shoppingCart.mallBdPackageCell track by $index">

                <div class="clearfix">
                    <!--product image-->
                    <div ng-show="isEmpty(getPackagesPictureUrl(mallBdPackageCell.mallBdPackage.image))">
                        <img class="f_left m_right_10" style="width: 62px;height: 62px" src="http://placehold.it/90x90?text=Image not found"  alt="">
                    </div>
                    <div ng-show="!isEmpty(getPackagesPictureUrl(mallBdPackageCell.mallBdPackage.image))">
                        <img class="f_left m_right_10" style="width: 62px;height: 62px" src="@{{getPackagesPictureUrl(mallBdPackageCell.mallBdPackage.image)}}"  alt="">
                    </div>
                    <!--product description-->
                    <div class="f_left product_description" >
                        <a ng-href="{{url('/packages')}}/@{{ mallBdPackageCell.mallBdPackage.packageTitle}}/@{{ mallBdPackageCell.mallBdPackage.id}}"  class="color_dark m_bottom_5 d_block prod_name pop-cart-desc" ng-bind="mallBdPackageCell.mallBdPackage.packageTitle"></a>
                        <p class=" color_dark pop-cart-price">Price: <span ng-bind="getPackageItemPriceIndividual($index)" > </span></p>
                        <p class=" fw_medium scheme_color pop-cart-price">Subtotal: <span ng-bind="getPackageItemPrice($index)"></span></p>
                    </div>
                    <div class="f_left product_description" >
                        <p  class="color_dark m_bottom_5 d_block pop-cart-price" ng-bind="mallBdPackageCell.mallBdPackage.quantity"></p>
                    </div>
                    <!--product price-->
                    <div class="f_right f_size_medium">
                        <!--                                    <div class="clearfix">
                                                                <span ng-bind="mallBdPackageCell.quantity"></span> x <b class="color_dark" ng-bind="mallBdPackageCell.mallBdPackage.packagePriceTotal"></b>
                                                            </div>-->
                        <div class="clearfix quantity r_corners d_inline_middle f_size_medium color_dark m_bottom_10 pull-left">
                            <button class="bg_tr d_block f_left" data-direction="down"  ng-click="decreasePackageInPackageCell($index)" >-</button>
                            <!--<input type="text" name="" readonly ng-value="getPackageQuantity($index)" class="f_left">-->
                            <span style="margin-top: 6px;" ng-bind="getPackageQuantity($index)" class="f_left"></span>
                            <button class="bg_tr d_block f_left" data-direction="up" ng-click="increasePackageInPackageCellCartMain($index)" >+</button>
                        </div>
                        {{--close_product--}}
                        <!--<button class="color_dark tr_hover" style="background: transparent"><i class="fa fa-times" ng-click="removePackageCell($index)"></i></button>-->
                        <a href="javascript:void(0)" ng-click="removePackageCell($index)" class="color_dark pull-left" style="margin: 8px 0px 0px 5px;"><i class="fa fa-times f_size_medium m_right_5"></i></a>
<!--                                    <button style="background: transparent" ng-show="mallBdPackageCell.quantity>0" ><i ng-click="decreasePackageInPackageCell($index)" class="fa fa-minus" ></i></button>-->
                    </div>
                </div>
            </li>

        </ul>
        <ul ng-if="lastItemAddedInCart==_PACKAGE" class="products_list add_cart_product_list">
            <li ng-repeat="mallBdPackageCell in shoppingCart.mallBdPackageCell track by $index">

                <div class="clearfix">
                    <!--product image-->
                    <div ng-show="isEmpty(getPackagesPictureUrl(mallBdPackageCell.mallBdPackage.image))">
                        <img class="f_left m_right_10" style="width: 62px;height: 62px" src="http://placehold.it/90x90?text=Image not found"  alt="">
                    </div>
                    <div ng-show="!isEmpty(getPackagesPictureUrl(mallBdPackageCell.mallBdPackage.image))">
                        <img class="f_left m_right_10" style="width: 62px;height: 62px" src="@{{getPackagesPictureUrl(mallBdPackageCell.mallBdPackage.image)}}"  alt="">
                    </div>
                    <!--product description-->
                    <div class="f_left product_description" >
                        <a ng-href="{{url('/packages')}}/@{{ mallBdPackageCell.mallBdPackage.packageTitle}}/@{{ mallBdPackageCell.mallBdPackage.id}}"  class="color_dark m_bottom_5 d_block prod_name pop-cart-desc" ng-bind="mallBdPackageCell.mallBdPackage.packageTitle"></a>
                        <p class=" color_dark pop-cart-price">Price: <span ng-bind="getPackageItemPriceIndividual($index)" > </span></p>
                        <p class=" fw_medium scheme_color pop-cart-price">Subtotal: <span ng-bind="getPackageItemPrice($index)"></span></p>
                    </div>
                    <div class="f_left product_description" >
                        <p  class="color_dark m_bottom_5 d_block pop-cart-price" ng-bind="mallBdPackageCell.mallBdPackage.quantity"></p>
                    </div>
                    <!--product price-->
                    <div class="f_right f_size_medium">
                        <!--                                    <div class="clearfix">
                                                                <span ng-bind="mallBdPackageCell.quantity"></span> x <b class="color_dark" ng-bind="mallBdPackageCell.mallBdPackage.packagePriceTotal"></b>
                                                            </div>-->
                        <div class="clearfix quantity r_corners d_inline_middle f_size_medium color_dark m_bottom_10 pull-left">
                            <button class="bg_tr d_block f_left" data-direction="down"  ng-click="decreasePackageInPackageCell($index)" >-</button>
                            <!--<input type="text" name="" readonly ng-value="getPackageQuantity($index)" class="f_left">-->
                            <span style="margin-top: 6px;" ng-bind="getPackageQuantity($index)" class="f_left"></span>
                            <button class="bg_tr d_block f_left" data-direction="up" ng-click="increasePackageInPackageCellCartMain($index)" >+</button>
                        </div>
                        {{--close_product--}}
                        <!--<button class="color_dark tr_hover" style="background: transparent"><i class="fa fa-times" ng-click="removePackageCell($index)"></i></button>-->
                        <a href="javascript:void(0)" ng-click="removePackageCell($index)" class="color_dark pull-left" style="margin: 8px 0px 0px 5px;"><i class="fa fa-times f_size_medium m_right_5"></i></a>
<!--                                    <button style="background: transparent" ng-show="mallBdPackageCell.quantity>0" ><i ng-click="decreasePackageInPackageCell($index)" class="fa fa-minus" ></i></button>-->
                    </div>
                </div>
            </li>
            <li ng-repeat="productCell in shoppingCart.productCell track by $index" >
                <div class="clearfix">
                    <!--product image-->
                    <div ng-show="isEmpty(getPictureUrl(productCell.product.pictures[0].name))">
                        <img class="f_left m_right_10" style="width: 62px;height: 62px" src="http://placehold.it/90x90?text=Image not found"  alt="">
                    </div>
                    <div ng-show="!isEmpty(getPictureUrl(productCell.product.pictures[0].name))">
                        <img class="f_left m_right_10" style="width: 62px;height: 62px" src="@{{getPictureUrl(productCell.product.pictures[0].name)}}"  alt="">
                    </div>
                    <!--product description-->
                    <div class="f_left product_description">
                        <a ng-href="{{url('/product/')}}/@{{ productCell.product.url }}/@{{ productCell.product.code }}"  class="color_dark m_bottom_5 d_block prod_name pop-cart-desc" ng-bind="productCell.product.title" ></a>

                        <p class="color_dark pop-cart-price">Price: <span ng-if="productCell.product.discountActiveFlag" class="line-through--for-cart-price" ng-bind="getPriceIndividual($index)" ></span><span ng-if="productCell.product.discountActiveFlag"  ng-bind="getPriceAfterDiscount($index)" > </span><span ng-if="!productCell.product.discountActiveFlag" ng-bind="getPriceIndividual($index)" > </span></p>
                        <p class="fw_medium scheme_color ">Subtotal: <span ng-bind="getPrice($index)"></span></p>
                        <div class="pop-cart-price"  ng-repeat="selectedAttribute in productCell.selectedAttributes" >
                            <span class="f_size_medium" ng-bind="selectedAttribute.name"></span>
                            <span class="f_size_medium" >:</span>
                            <span class="f_size_medium" ng-bind="selectedAttribute.value"></span>
                        </div>
                    </div>
                    <!--product price-->
                    <div class="f_right f_size_medium">
                        <!--                                    <div class="clearfix">
                                                                <span ng-bind="productCell.quantity"></span> x <b class="color_dark" ng-bind="productCell.product.prices[0].retailPrice"></b>
                                                            </div>-->
                        <div class="clearfix quantity r_corners d_inline_middle f_size_medium color_dark m_bottom_10 pull-left">
                            <button class="bg_tr d_block f_left" data-direction="down"  ng-click="decreaseProductInProductCell($index)" >-</button>
                            <!--<input type="text" name="" readonly ng-value="getProductQuantity($index)" class="f_left">-->
                            <span style="margin-top: 6px;" ng-bind="getProductQuantity($index)" class="f_left"></span>
                            <button class="bg_tr d_block f_left" data-direction="up" ng-click="increaseProductInProductCellCartMain($index)" >+</button>
                        </div>
<!--                                    <button class="close_product color_dark tr_hover"><i class="fa fa-times" ng-click="removeProductCell($index)"></i></button>-->
                        <a href="javascript:void(0)" ng-click="removeProductCell($index)" class="color_dark pull-left" style="margin: 8px 0px 0px 5px;"><i class="fa fa-times f_size_medium m_right_5"></i></a>
                        <!--<button style="background: transparent" ng-show="productCell.quantity>0" ><i ng-click="decreaseProductInProductCell($index)" class="fa fa-minus" ></i></button>-->
                    </div>
                </div>
            </li>
        </ul>
        <div ng-show="shoppingCart.productCell.length > 0 || shoppingCart.mallBdPackageCell.length > 0" class="f_size_medium sc_header fly-cart-bg">
            <!--total price-->

            <table class="table table-cart-pop text-right" style="text-align: right;">
                <tr>
                    <td>Subtotal: </td>
                    <td class="tax"><span ng-bind-html="currencySymbol"></span>&nbsp;<span ng-bind="getSubTotalAmount()"></span></td>
                </tr>
                <tr>
                    <td>Products Discount: </td>
                    <td class="discount-label"><span ng-bind-html="currencySymbol"></span>&nbsp;<span ng-bind="getTotalDiscountAmount()"></span></td>
                </tr>
                <tr>
                    <td>Grand Total: </td>
                    <td class="price"><span ng-bind-html="currencySymbol"  ></span>&nbsp;<span ng-bind="getTotalAmount()"  ></span></td>
                </tr>
            </table>
            <div class="sc_footer t_align_c">
                <a href="javascript:void(0)" onclick="viewCart()" role="button" class="button_type_4 d_inline_middle bg_light_color_2 r_corners color_dark t_align_c tr_all_hover m_mxs_bottom_5">View Basket</a>
                <a href="{{url('/checkout/')}}" role="button" class="button_type_4 bg_scheme_color d_inline_middle r_corners tr_all_hover color_light">Checkout</a>
            </div>
        </div>

    </div>
</li>