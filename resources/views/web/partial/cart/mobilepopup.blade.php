<!-- Dummy Button Used by JS developer[ Start] -->
<button id="add_to_cart_mobilepopup_triggerBtn" data-popup="#add_to_cart_mobilepopup" style="display:none"></button>
<!-- [Ends] -->
<!--product item-->
<div class="popup_wrap d_none" id="add_to_cart_mobilepopup" >
    
    <section class="popup r_corners shadow add_to_cart_pop pop-fix">
        <button class="bg_tr color_dark tr_all_hover text_cs_hover close f_size_large"><i class="fa fa-times"></i>
        </button>
         
        <div class="clearfix row">
            <!--right popup column-->
            <div class="f_right col-md-12">
                <p class="items_added_info" ng-if="getTotalQuantity() < 2">
                    There is <span ng-bind="getTotalQuantity()"></span> item on your basket
                </p>
                <p class="items_added_info" ng-if="getTotalQuantity() > 1">
                    There are <span ng-bind="getTotalQuantity()"></span> items on your basket
                </p>
                <!--total price-->
                <table class="table table-cart-pop">
                    <tr>
                        <td>Subtotal: </td>
                        <td class="tax"><span ng-bind-html="currencySymbol"></span>&nbsp;<span ng-bind="getSubTotalAmount()"></span></td>
                    </tr>
                    <tr>
                        <td>Products Total Discount: </td>
                        <td class="discount-label"><span ng-bind-html="currencySymbol"></span>&nbsp;<span ng-bind="getTotalDiscountAmount()"></span></td>
                    </tr>
                    <tr>
                        <td>Grand Total: </td>
                        <td class="price"><span ng-bind-html="currencySymbol"  ></span>&nbsp;<span ng-bind="getTotalAmount()"  ></span></td>
                    </tr>
                </table>
                <div class="sc_footer t_align_c add-cart-foot" style="padding:0px;">
                    <a href="javascript:void(0)" role="button" style="padding:5px;" 
                       class="button_type_4 d_inline_middle bg_light_color_2 r_corners color_dark t_align_c tr_all_hover m_mxs_bottom_5" onclick="hideCartPopup()">Continue Shopping</a>
                    <a href="{{url('/checkout')}}" role="button" style="padding: 5px;margin-top: -5px;margin-left: 6px;" 
                       class="button_type_4 bg_scheme_color d_inline_middle r_corners tr_all_hover color_light">Proceed to checkout</a>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
                           function hideCartPopup() {
                               $('#add_to_cart_mobilepopup').fadeOut(1000);
                           }
</script>