<link rel="stylesheet" type="text/css" media="all" href="{{asset('/template_resource/css/chat.css')}}">

<?php
//$chatUserId = $appCredential->user->id;
$chatUserId = Session::get('chatUserId');
if($chatUserId == NULL){
    $chatUserId = 0;
}
//var_dump(Session::get('chatUserId'));
//die;
?>

<div class="customer-service" id="chat_btn_id">
    <a id="open_chat_anchor" onclick="openChat({{ $chatUserId }})">
        <img width="60" src="{{asset('/template_resource/images/service.png')}}">
    </a>
</div>
<input type="hidden" id="create_chat_guest_url" value="{{URL::to('/create-chat-guest')}}">

<input type="hidden" id="chatUserId" value="<?php echo $chatUserId; ?>">

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="{{asset('/developer/chat_socket/v1/node_modules/delivery/lib/client/delivery.js')}}"></script>
<script src="http://cdn.socket.io/socket.io-1.2.0.js"></script>
<script type="text/javascript" src="{{asset('/developer/chat_socket/v1/chatJs.js')}}"></script>