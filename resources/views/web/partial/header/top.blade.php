<?php if ($mobile) { ?>
    <input type="hidden" id="device_track" value="mobile" />
    <!--header top part-->
    @include('web/partial/header/mobiletop')
<?php } else { ?>
    <input type="hidden" id="device_track" value="computer" />
    <!--header top part-->
    @include('web/partial/header/webtop')
<?php } ?>