<head>
    <title>The Mall BD</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!--meta info-->
    <meta name="author" content="">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta property="fb:app_id" content="1648526795437995"/>
    
    <link rel="icon" type="images/ico" href="{{asset('/template_resource/images/fav.ico')}}">
    <!--stylesheet include-->
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('/template_resource/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('/template_resource/css/jquery.custom-scrollbar.css')}}">
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('/template_resource/css/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('/template_resource/css/style.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('/template_resource/css/fa-snapchat.css')}}">
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('developer/autocomplete/css/easy-autocomplete.css')}}">
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('developer/autocomplete/css/easy-autocomplete.themes.css')}}">

    <!--font include-->
    <link href="{{asset('/template_resource/css/font-awesome.min.css')}}" rel="stylesheet">


    


</head>