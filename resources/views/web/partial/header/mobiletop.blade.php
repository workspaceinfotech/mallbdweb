<?php
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
?>
  <?php if(Auth::check()){
                $loginDisplayType = "hidden";
            }else{
                $loginDisplayType = "";
            }
            ?>

            <?php if(!Auth::check()){
                $logoutDisplayType = "hidden";
            }else{
                $logoutDisplayType = "";
            }
            ?>
<div class="top-bar-mobile visible-xs">
    <ul class="mobile-top-menu">
        <?php if(!Auth::check()){
              ?>
        <li ><a id="loginFormViewer" href="#" data-popup="#login_popup"><i class="fa fa-key"></i></a></li>
        <li ><a href="{{url('registration/create')}}"><i class="fa fa-lock"></i></a></li>
    <?php
            }
            ?>
        <?php if(Auth::check()){
              ?>
        <li ><a href="{{ url('/myaccount/info') }}"><i class="fa fa-user"></i></a></li>
        <li ><a href="javascript:void(0)" onclick="doLogout()"><i class="fa fa-sign-out"></i></a></li>
        <li >
            <a href="{{url('customer/wishlist/get')}}"><i class="fa fa-heart-o"></i></a>
             @if($wishListCount>0)
             <span class="number-counter">{{$wishListCount}}</span>
             @endif
        </li>
    <?php
            }
            ?>
        
        
        <li {{ $loginDisplayType }}>
            <a href="{{ url('/checkout') }}"><i class="fa fa-shopping-cart"></i></a>
            <span class="number-counter" ng-show="shoppingCart.productCell.length > 0 || shoppingCart.mallBdPackageCell.length > 0" ng-bind="getTotalQuantity()" ></span>
        </li>
    </ul>
</div>
 <!-- Developer : Hidden Value -->
                    <input type="hidden" id="imageUrl" value="{{$imageUrl}}" />
                    <input type="hidden" id="baseUrl" value="{{url('/')}}/" />
                    <input type="hidden" id="isLogin" value="{{json_encode($isLogin)}}" />
                    <input type="hidden" id="userName" value=""/>
                    <!--language settings-->
                    <!--shopping cart-->
                    @include('web/partial/cart/main')
                    @include('web/partial/cart/mobilepopup')

                    <?php if(isset($redirectMainPage)){ ?>
                        @include('web/partial/login/loginPopupRedirectMain')
                    <?php }else{ ?>
                        @include('web/partial/login/loginPopup')
                    <?php } ?>
                    
<script>
    if(location.pathname.indexOf('/home')>0){

      //  document.getElementById("topSection").className = "h_bot_part header-most-top container";
    }

    function logIn()
    {
        if($('#userName').val()=="" ||$('#userName').val()=="undefined")
        {
            showLoginForm(" ");
        }
    }


    function addWishListClass()
    {
        $.ajax({

            url: $("#baseUrl").val() + "api/wishlist/product/array",
            method: "POST",
            data: {

            },
            success: function (data) {

                var productArray = data.responseData;

                if(productArray.length>0){
                    for(var i=0; i<productArray.length;i++){

                        $('#inner'+productArray[i]).addClass('active');
                        $('#outer'+productArray[i]).addClass('active');
                        console.log("product id's : "+productArray[i]);
                    }
                }
            }

        });
    }



</script>





