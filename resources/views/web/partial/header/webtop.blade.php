 <!--header top part-->
<?php
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
?>

<section id="topSection" class="h_bot_part header-most-top container header-other-page hidden-xs">
    <div class="clearfix container">
        <div class="row clearfix">
            <div class="col-lg-6 col-md-6 col-sm-4 t_xs_align_c">
                <a href="{{url('/')}}" class="logo m_xs_bottom_15 d_xs_inline_b">
                    <img width="200" src="{{asset('/template_resource/images/logo.png')}}" alt="">
                </a>
            </div>

            <?php if(Auth::check()){
                $loginDisplayType = "hidden";
            }else{
                $loginDisplayType = "";
            }
            ?>

            <?php if(!Auth::check()){
                $logoutDisplayType = "hidden";
            }else{
                $logoutDisplayType = "";
            }
            ?>

            <div class="col-lg-6 col-md-6 col-sm-8 t_align_r t_xs_align_c top-right-cart-box">
                <ul class="d_inline_b horizontal_list clearfix f_size_small users_nav user_nav_white">

                    <li>
                        <p class="default_t_color" id="loginText" {{ $loginDisplayType }}><a id="loginFormViewer" href="#" data-popup="#login_popup"> Log In</a> </p>
                            <p class="default_t_color" id="loginName" {{$logoutDisplayType}}>{{ $appCredential->user->firstName }}</p>
                    </li>

                    <li {{ $logoutDisplayType }} id="myaccountText" ><a href="{{ url('/myaccount/info') }}" class="default_t_color">My Account</a></li>
                    <li  {{ $loginDisplayType }} id="registrationText"><a href="{{ url('/registration/create') }}" class="default_t_color">Registration</a></li>

                    <li><a href="{{ url('/checkout') }}" class="default_t_color">Checkout</a></li>




                    <li id="logoutText" {{ $logoutDisplayType }}><a class="default_t_color" href="javascript:void(0)" onclick="doLogout()">Logout</a></li>



                </ul>
                <ul class="d_inline_b horizontal_list clearfix t_align_l site_settings top-wish-fix">
                    <!--like-->
                        <li {{ $logoutDisplayType }} id="wishlist_webtop">
                            <a role="button" href="{{url('customer/wishlist/get')}}"
                               class="button_type_1 color_dark d_block bg_light_color_1 r_corners tr_delay_hover box_s_none only-single"><i
                                        class="fa fa-heart-o f_size_ex_large top-cart-btn"></i>
                                @if($wishListCount>=0)
                                    <span class="count circle t_align_c" id="wishListCount">{{$wishListCount}}</span>
                                @endif
                            </a>
                        </li>

<!--                    <li class="m_left_5">
                        <a role="button" href="#"
                           class="button_type_1 color_dark d_block bg_light_color_1 r_corners tr_delay_hover box_s_none">
                            <i class="fa fa-files-o f_size_ex_large"></i>
                            <span class="count circle t_align_c">3</span></a>
                    </li>-->
                    
                    <!-- Developer : Hidden Value -->
                    <input type="hidden" id="imageUrl" value="{{$imageUrl}}" />
                    <input type="hidden" id="baseUrl" value="{{url('/')}}/" />
                    <input type="hidden" id="isLogin" value="{{json_encode($isLogin)}}" />
                    <input type="hidden" id="userName" value=""/>
                    <!--language settings-->
                    <!--shopping cart-->
                    @include('web/partial/cart/main')
                    @include('web/partial/cart/popup')

                    <?php if(isset($redirectMainPage)){ ?>
                        @include('web/partial/login/loginPopupRedirectMain')
                    <?php }else{ ?>
                        @include('web/partial/login/loginPopup')
                    <?php } ?>
                    
                    @include('web/partial/chat/main')
                </ul>
            </div>
        </div>
    </div>
</section>
<script>
    if(location.pathname.indexOf('/home')>0){

      //  document.getElementById("topSection").className = "h_bot_part header-most-top container";
    }

    function logIn()
    {
        if($('#userName').val()=="" ||$('#userName').val()=="undefined")
        {
            showLoginForm(" ");
        }
    }


    function addWishListClass()
    {
        $.ajax({

            url: $("#baseUrl").val() + "api/wishlist/product/array",
            method: "POST",
            data: {

            },
            success: function (data) {

                var productArray = data.responseData;

                if(productArray.length>0){
                    for(var i=0; i<productArray.length;i++){

                        $('#inner'+productArray[i]).addClass('active');
                        $('#outer'+productArray[i]).addClass('active');
                        console.log("product id's : "+productArray[i]);
                    }
                }
            }

        });
    }



</script>





