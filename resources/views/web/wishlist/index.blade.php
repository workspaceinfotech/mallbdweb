<!doctype html>
<!--[if IE 9 ]><html class="ie9" lang="en" ng-app="mallBd" ><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" ng-app="mallBd"  ><!--<![endif]-->

    @include('web/partial/header/mainStart')
    @include('web/partial/header/cartScript')
    @include('web/partial/header/mainEnds')

    <body ng-controller="CartController">
        @include('web/partial/loader')
        <!--wide layout-->
        <div class="wide_layout relative">
            <!--[if (lt IE 9) | IE 9]>
            <div style="background:#fff;padding:8px 0 10px;">
                <div class="container" style="width:1170px;">
                    <div class="row wrapper">
                        <div class="clearfix" style="padding:9px 0 0;float:left;width:83%;"><i
                                class="fa fa-exclamation-triangle scheme_color f_left m_right_10"
                                style="font-size:25px;color:#e74c3c;"></i><b style="color:#e74c3c;">Attention! This page may not
                            display correctly.</b> <b>You are using an outdated version of Internet Explorer. For a faster, safer
                            browsing experience.</b></div>
                        <div class="t_align_r" style="float:left;width:16%;"><a
                                href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"
                                class="button_type_4 r_corners bg_scheme_color color_light d_inline_b t_align_c" target="_blank"
                                style="margin-bottom:2px;">Update Now!</a></div>
                    </div>
                </div>
            </div>
            <![endif]-->
            <!--markup header-->
            <header role="banner" class="type_5 fixed-top">


                @include('web/partial/header/top')
                <!--header bottom part-->
                @include('web/partial/new_menu')

            </header>
            <div class="other-free-gap">

            </div>
            <!--breadcrumbs-->
            <section class="breadcrumbs">
                <div class="container">
                    <ul class="horizontal_list clearfix bc_list f_size_medium">
                        <li class="m_right_10 current"><a href="{{url('')}}" class="default_t_color">Home<i
                                    class="fa fa-angle-right d_inline_middle m_left_10"></i></a></li>
                        <li><a href="#" class="default_t_color">Wishlist</a></li>
                    </ul>
                </div>
            </section>
            <!--content-->
            <div class="page_content_offset">
                <div class="container">
                    <div class="row clearfix">
                        <!--left content column-->
                        <section class="col-lg-9 col-md-9 col-sm-9 m_xs_bottom_30">
                            <h2 class="tt_uppercase color_dark m_bottom_20">My Wishlist</h2>

                            <!--    <div class="row clearfix m_bottom_15">
                                    <div class="col-lg-7 col-md-7 col-sm-7 f_sx_none m_xs_bottom_10">
                                        <p class="d_inline_middle f_size_medium">Results 1 - 5 of 45</p>
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-5 f_sx_none t_xs_align_l t_align_r">
                                        pagination
                                        <a role="button" href="#"
                                           class="f_size_large button_type_10 color_dark d_inline_middle bg_cs_hover bg_light_color_1 t_align_c tr_delay_hover r_corners box_s_none"><i
                                                class="fa fa-angle-left"></i></a>
                                        <ul class="horizontal_list clearfix d_inline_middle f_size_medium m_left_10">
                                            <li class="m_right_10"><a class="color_dark" href="#">1</a></li>
                                            <li class="m_right_10"><a class="scheme_color" href="#">2</a></li>
                                            <li class="m_right_10"><a class="color_dark" href="#">3</a></li>
                                        </ul>
                                        <a role="button" href="#"
                                           class="f_size_large button_type_10 color_dark d_inline_middle bg_cs_hover bg_light_color_1 t_align_c tr_delay_hover r_corners box_s_none"><i
                                                class="fa fa-angle-right"></i></a>
                                    </div>
                                </div>-->
                            <hr class="m_bottom_30 divider_type_3">
                            <div class="row clearfix m_bottom_15">
                                <div class="col-lg-7 col-md-7 col-sm-8 col-xs-12 m_xs_bottom_10">
                                    <p class="d_inline_middle f_size_medium d_xs_block m_xs_bottom_5">Results <?php echo $startProductNumber; ?> -  <?php echo $endProductNumber; ?>
                                        of  {{$totalProduct}}</p>

                                    <p class="d_inline_middle f_size_medium m_left_20 m_xs_left_0">Show:</p>
                                    <!--show items per page select-->
                                    <select name="show" id="selectViewSize" onchange="changeAction()">
                                        <option value="10" <?php if (isset($selected) && $selected && $limit == '10') echo 'selected="selected"' ?>>10</option>
                                        <option value="20" <?php if (isset($selected) && $selected && $limit == '20') echo 'selected="selected"' ?>>20</option>
                                        <option value="30" <?php if (isset($selected) && $selected && $limit == '30') echo 'selected="selected"' ?>>30</option>
                                        <option value="40" <?php if (isset($selected) && $selected && $limit == '40') echo 'selected="selected"' ?>>40</option>
                                        <option value="50" <?php if (isset($selected) && $selected && $limit == '50') echo 'selected="selected"' ?>>50</option>

                                    </select>

                                    <p class="d_inline_middle f_size_medium m_left_5">items per page</p>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-4 t_align_r t_xs_align_l">
                                    <!--pagination-->
                                    @if($currentPage>0)
                                    <a role="button" onclick="prevPage({{{$currentPage}}})" href="javascript:void(0)"
                                       class="button_type_10 color_dark d_inline_middle bg_cs_hover bg_light_color_1 t_align_c tr_delay_hover r_corners box_s_none"><i
                                            class="fa fa-angle-left"></i></a>
                                    @endif
                                    <ul id="paginationNumberUl" class="horizontal_list clearfix d_inline_middle f_size_medium m_left_10">
                                        <?php
                                        $numberOfPages = ceil($totalProduct / $limit);
                                        for ($i = 0; $i < $numberOfPages; $i++) {
                                            ?>
                                            <li class="m_right_10"><a class="color_dark" href="javascript:void(0)" onclick="specificPage(<?php echo $i; ?>)"><?php echo $i + 1; ?></a></li>
                                        <?php } ?>
                                    </ul>
                                    @if($currentPage<$numberOfPages-1)
                                    <a onclick="nextPage({{{$currentPage}}})" role="button" href="javascript:void(0)"
                                       class="button_type_10 color_dark d_inline_middle bg_cs_hover bg_light_color_1 t_align_c tr_delay_hover r_corners box_s_none" ><i
                                            class="fa fa-angle-right"></i></a>
                                    @endif
                                </div>
                            </div>
                            <hr class="m_bottom_30 divider_type_3">
                            <!--wishlist table-->
                            <table class="table_type_1 responsive_table full_width t_align_l r_corners wraper shadow bg_light_color_1 m_bottom_30">
                                <thead>
                                    <tr class="f_size_large">
                                        <!--titles for td-->
                                        <th>Product Image</th>
                                        <th>Product Name &amp; Category</th>
                                        <th>Price</th>
                                        <th>Quanity</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                     <?php
                                    $countPic =0;
                                    $avgRating=0;
                                    $left=0;
                                    ?>
                                    @foreach($products as $product)

                                    <tr id="tr{{$product->id}}">
                                        <!--product image-->
                                        <td data-title="Product Image">
                                            <a href="{{url('/product/' . $product->url . '/' . $product->code)}}" class="fw_medium d_inline_b f_size_ex_large color_dark m_bottom_5">
                                                <img src="{{$imageUrl.'product/thumbnail/'.@$product->pictures[0]->name}}" onerror="this.src='http://placehold.it/90x90?text=Image not found'" alt="">
                                            </a>
                                        </td>
                                        <!--product name and category-->
                                        <td data-title="Product Name">
                                            <a href="{{url('/product/' . $product->url . '/' . $product->code)}}" class="fw_medium d_inline_b f_size_ex_large color_dark m_bottom_5">{{$product->title}}</a><br>

                                            <a href="#" class="default_t_color">Dresses</a>
                                        </td>
                                        <!--product price-->
                                        <td data-title="Price">
                                            {{--<span class="scheme_color fw_medium f_size_large"><span>&#2547</span>{{@$product->prices[0]->retailPrice}}</span>--}}
                                            @if($product->discountActiveFlag)
                                <s class="v_align_b f_size_ex_large price-old"><span><?php echo $currency->HTMLCode; ?></span>{{ number_format($product->prices[0]->retailPrice,2)}}</s>
                                <br/><span
                                    class="v_align_b f_size_big m_left_5 scheme_color fw_medium price-usual"><span><?php echo $currency->HTMLCode; ?></span>{{ number_format(($product->prices[0]->retailPrice-$product->discountAmount),2)}} &nbsp;</span>
                                @else
                                <span
                                    class="v_align_b f_size_big m_left_5 scheme_color fw_medium price-usual"><span><?php echo $currency->HTMLCode; ?></span>{{ number_format($product->prices[0]->retailPrice,2)}} &nbsp;</span>

                                @endif
                                </td>
                                <!--quanity-->
                                <td data-title="Quantity">
                                    <div class="clearfix quantity r_corners d_inline_middle f_size_medium color_dark">
                                        <button class="bg_tr d_block f_left" data-direction="down">-</button>
                                        <input id="outter_quantity_modifier_{{$product->id}}"  type="text" name="" readonly value="1" class="f_left">
                                        <button class="bg_tr d_block f_left" data-direction="up">+</button>
                                    </div>
                                </td>
                                <!--add or remove buttons-->

                                <td data-title="Action" >
                                    <?php if($product->quantity <= 0){ ?>
                                    <button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light f_left mw_0 item-ad-btn" style="background: red; color: white;">Out Of Stock</button>
                                    <?php }else{ ?>
                                        <input type="hidden" id="productJsonObj_{{$product->id}}" value="{{json_encode($product)}}"  />
                                        <button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light f_left mw_0 item-ad-btn" ng-click="addToCart('productJsonObj_{{$product->id}}',-1,false)">Add to Basket</button>
                                    <?php } ?>
                                    <br>
                                    <a href="javascript:void(0)"  class="color_dark" onclick="doRemove('{{$product->id}}', 'tr{{$product->id}}')"><i class="fa fa-times m_right_5"></i> Remove</a>


                                    <span id="quick_view_product_{{$product->id}}_trigger_btn" data-popup="#quick_view_product_{{$product->id}}" style="display:none;"></span>
                                    <div class="popup_wrap d_none" id="quick_view_product_{{$product->id}}" >
                                        <section class="popup r_corners shadow">
                                    <button class="bg_tr color_dark tr_all_hover text_cs_hover close f_size_large"><i class="fa fa-times"></i></button>
                                    <div class="clearfix">
                                        <div class="custom_scrollbar">
                                            <!--left popup column-->
                                            <div class="f_left half_column">
                                                
                                                <div class="relative d_inline_b m_bottom_10 qv_preview">
                                                    <?php
                                                        if (($product->isFeatured)) {
                                                            ?>
                                                            <div class="f-tag">
                                                                <span>hot</span>
                                                            </div>
                                                            <?php
                                                        } else if ($product->discountActiveFlag) {
                                                            ?>
                                                            <div class="s-tag">
                                                                <span>Sale</span>
                                                            </div>
                                                        <?php } else { ?>
                                                            <div class="h-tag">
                                                                <span>New</span>
                                                            </div>
                                                        <?php } ?>
                                                    {{--<span class="hot_stripe"><img src="{{asset('/template_resource/images/sale_product.png')}}" alt=""></span>--}}
                                                    @if(@$product->pictures == NULL)
                                                            <img id="zoom_image"src="http://placehold.it/360x360?text=No Image Found">
                                                     @endif
                                                    @foreach(@$product->pictures as $picture)
                                                        @if($picture->cover==1)
                                                            <?php $countPic++;?>
                                                            <img id="zoom_image{{$countPic}}" src="{{$imageUrl.'product/pagelarge/'.$picture->name}}" class="tr_all_hover" alt="" onerror="this.src='http://placehold.it/360x360?text=No Image Found'">
                                                         @endif
                                                         
                                                    @endforeach
                                                </div>
                                                <!--carousel-->

                                                <div class="relative qv_carousel_wrap m_bottom_20">
                                                    <button class="button_type_11 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_prev">
                                                        <i class="fa fa-angle-left "></i>
                                                    </button>
                                                    <ul class="qv_carousel d_inline_middle">
                                                        @foreach(@$product->pictures as $picture)
                                                            <li><img id="small-image{{$picture->name}}" src="{{$imageUrl.'product/thumbnail/'.$picture->name}}" onerror="this.src='http://placehold.it/90x90?text=No Image Found'" alt="" onclick="changePicture('{{$picture->name}}', '{{ $countPic}}')"></li>
                                                        @endforeach
                                                    </ul>
                                                    <button class="button_type_11 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_next">
                                                        <i class="fa fa-angle-right "></i>
                                                    </button>
                                                </div>

                                                <div class="d_inline_middle" style="display: none;">Share this:</div>
                                                <div class="d_inline_middle m_left_5" style="display: none;">
                                                    <!-- AddThis Button BEGIN -->
                                                    <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
                                                        <a class="addthis_button_preferred_1"></a>
                                                        <a class="addthis_button_preferred_2"></a>
                                                        <a class="addthis_button_preferred_3"></a>
                                                        <a class="addthis_button_preferred_4"></a>
                                                        <a class="addthis_button_compact"></a>
                                                        <a class="addthis_counter addthis_bubble_style"></a>
                                                    </div>
                                                    <!-- AddThis Button END -->
                                                </div>
                                            </div>
                                            <!--right popup column-->
                                            <div class="p_top_10 t_xs_align_l pop-desc" style="float: right; width: 360px;">
                                                <p class="manf-name"><a href="">{{$product->manufacturer->name}}</a></p>
                                                <p class="prod-name-detail">{{$product->title}}</p>
                                                <p class="item-number">item:#{{$product->code}}</p>
                                                <p class="detail-price">
                                                    @if($product->discountActiveFlag)
                                                    <span class="older"><?php echo $currency->HTMLCode; ?> {{ number_format($product->prices[0]->retailPrice,2)}}</span><span class="newer"><?php echo $currency->HTMLCode; ?> {{ number_format(($product->prices[0]->retailPrice-$product->discountAmount),2)}}</span>
                                                    @else
                                                    <span class="newer"><?php echo $currency->HTMLCode; ?> {{ number_format($product->prices[0]->retailPrice,2)}}</span>
                                                    @endif

                                                </p>
                                                <span class="detail-desc"><?php echo $product->description; ?></span>
                                                <input type="hidden" id="productJsonObj_{{$product->id}}" value="{{json_encode($product)}}"  />
                                                <p class="rate-it">
                                                    <ul class="horizontal_list d_inline_middle type_2 clearfix rating_list tr_all_hover">
                                                        <?php
                                                        $avgRating = ceil($product->avgRating);
                                                        if($avgRating>5)
                                                        {
                                                            $avgRating=5;
                                                        }

                                                        $left = 5-$avgRating;
                                                        ?>
                                                        @for($i=0;$i<$avgRating;$i++)
                                                            <li class="active">
                                                                <i class="fa fa-star-o empty tr_all_hover"></i>
                                                                <i class="fa fa-star active tr_all_hover"></i>
                                                            </li>
                                                        @endfor
                                                        @if($left>0)
                                                            @for($i=0;$i<$left;$i++)
                                                                <li>
                                                                    <i class="fa fa-star-o empty tr_all_hover"></i>
                                                                    <i class="fa fa-star active tr_all_hover"></i>
                                                                </li>
                                                            @endfor
                                                        @endif
                                                        <li>
                                                            <span class="rate_number">{{$avgRating}}</span>
                                                        </li>

                                                    </ul>                                                

                                                 </p>

                                                <p class="desc-other-block">
                                                        <a href="#tab-1">Add a Comment</a>

                                                        @if($product->isWished)
                                                            <a class="favourite-link" href="" onclick="submitWishList({{$product->id}})"><i class="fa fa-heart"></i>Add to favorites</a>              
                                                         @else
                                                            <a class="favourite-link relative notify-flyout" href="" onclick="submitWishList('{{$product->id}}','quick_view_product_{{$product->id}}')"><i class="fa fa-heart-o"></i>Added to favorites
                                                              <span class="notify-small" id="outeraddnotify{{ $product->id }}" hidden>Product Added</span>
                                                              <span class="notify-small" id="outeraddnotify2{{ $product->id }}" hidden>Already Added</span>                                       
                                                            </a>    
                                                        @endif
                                                        <a class="wishlist-link" href="#" onclick="compare_product('{{$product->id}}');"><i class="fa fa-copy"></i>Compare Product</a>

                                                    </p>
                                                <div class="notify-small" id="outeraddnotify{{ $product->id }}"  hidden><span>Product Added</span></div>
                                                 <div class="notify-small" id="outeraddnotify2{{ $product->id }}"  hidden><span>Already Added</span></div>
                                                            @foreach(@$product->attributes as $attributes)
                                                       <div class="add-qn" style="margin-bottom: 25px;">
                                                            <div class="col-md-3">
                                                                {{$attributes->name}}:
                                                            </div>
                                                            <div class="col-md-9">
                                                               <select class="qnty" style="width: 100%;" name="product_name" id="select_attribute_{{$product->id}}_{{$attributes->id}}">
                                                                        @foreach(@$attributes->attributesValue as $attributesValue)
                                                                        <option value="{{@$attributesValue->id}}">{{@$attributesValue->value}}</option>
                                                                        @endforeach
                                                               </select>
                                                            </div>
                                                    
                                                        </div>
                                                            @endforeach
                                                 <div class="add-qn">
                                                    <?php if($product->quantity <= 0){ ?>
                                                    <!--<button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light f_left mw_0" >Out of Stock</button>-->

                                                            <div class="col-md-12">
                                                                <b style="color: red;font-size: 25px;">Out of Stock</b>
                                                            </div>
                                                        <?php }else{ ?>
                                                        <div class="col-md-3">
                                                                <select class="qnty" id="quantity_modifier_{{$product->id}}">                                        
                                                                   @for($i=1;$i<=$product->quantity;$i++)
                                                                    <option value="{{$i}}">Qty- {{$i}}</option>
                                                                    @endfor

                                                                </select>
                                                            </div>
                                                            <div class="col-md-9">
                                                                <button class="btn-new-ad" ng-click="addToCart('productJsonObj_{{$product->id}}',-1,true)">Add to basket</button>
                                                            </div>
                                                    <?php } ?>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                    </div>
                                </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <hr class="m_bottom_10 divider_type_3">
                            <!--    <div class="row clearfix m_bottom_40">
                                    <div class="col-lg-7 col-md-7 col-sm-7 f_sx_none m_xs_bottom_10">
                                        <p class="d_inline_middle f_size_medium">Results 1 - 5 of 45</p>
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-5 t_align_r f_sx_none t_xs_align_l">
                                        pagination
                                        <a role="button" href="#"
                                           class="f_size_large button_type_10 color_dark d_inline_middle bg_cs_hover bg_light_color_1 t_align_c tr_delay_hover r_corners box_s_none"><i
                                                class="fa fa-angle-left"></i></a>
                                        <ul class="horizontal_list clearfix d_inline_middle f_size_medium m_left_10">
                                            <li class="m_right_10"><a class="color_dark" href="#">1</a></li>
                                            <li class="m_right_10"><a class="scheme_color" href="#">2</a></li>
                                            <li class="m_right_10"><a class="color_dark" href="#">3</a></li>
                                        </ul>
                                        <a role="button" href="#"
                                           class="f_size_large button_type_10 color_dark d_inline_middle bg_cs_hover bg_light_color_1 t_align_c tr_delay_hover r_corners box_s_none"><i
                                                class="fa fa-angle-right"></i></a>
                                    </div>
                                </div>-->
                            <!--alert boxex-->
                            <div class="row clearfix m_bottom_15">
                                <div class="col-lg-7 col-md-7 col-sm-8 col-xs-12 m_xs_bottom_10">
                                    <p class="d_inline_middle f_size_medium d_xs_block m_xs_bottom_5">Results <?php echo $startProductNumber; ?> -  <?php echo $endProductNumber; ?>
                                        of  {{$totalProduct}}</p>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-4 t_align_r t_xs_align_l">
                                    <!--pagination-->
                                    @if($currentPage>0)
                                    <a role="button" onclick="prevPage({{{$currentPage}}})" href="javascript:void(0)"
                                       class="button_type_10 color_dark d_inline_middle bg_cs_hover bg_light_color_1 t_align_c tr_delay_hover r_corners box_s_none"><i
                                            class="fa fa-angle-left"></i></a>
                                    @endif
                                    <ul id="paginationNumberUl" class="horizontal_list clearfix d_inline_middle f_size_medium m_left_10">
                                        <?php
                                        $numberOfPages = ceil($totalProduct / $limit);
                                        for ($i = 0; $i < $numberOfPages; $i++) {
                                            ?>
                                            <li class="m_right_10"><a class="color_dark" href="javascript:void(0)" onclick="specificPage(<?php echo $i; ?>)"><?php echo $i + 1; ?></a></li>
                                        <?php } ?>
                                    </ul>
                                    @if($currentPage<$numberOfPages-1)
                                    <a onclick="nextPage({{{$currentPage}}})" role="button" href="javascript:void(0)"
                                       class="button_type_10 color_dark d_inline_middle bg_cs_hover bg_light_color_1 t_align_c tr_delay_hover r_corners box_s_none" ><i
                                            class="fa fa-angle-right"></i></a>
                                    @endif
                                </div>
                            </div>
                        </section>
                        <!--right column-->
                        <aside class="col-lg-3 col-md-3 col-sm-3">
                            <!--widgets-->

                            <!--banner-->
                            <a href="#" class="d_block r_corners m_bottom_30">
                                <img src="{{asset('/template_resource/images/banner_img_6.jpg')}}" alt="">
                            </a>
                            <!--Bestsellers-->
                            @include('web/partial/sideblock/bestsellers')
                            <a href="#" class="d_block r_corners m_bottom_30">
                                <img src="{{asset('/template_resource/images/banner_img_6.jpg')}}" alt="">
                            </a>
                            <!--tags-->
                            @include('web/partial/sideblock/tags')
                            <a href="#" class="d_block r_corners m_bottom_30">
                                <img src="{{asset('/template_resource/images/banner_img_6.jpg')}}" alt="">
                            </a>
                            <a href="#" class="d_block r_corners m_bottom_30">
                                <img src="{{asset('/template_resource/images/banner_img_6.jpg')}}" alt="">
                            </a>
                            
                        </aside>
                    </div>
                </div>
            </div>
            <!--markup footer-->
            @include('web/partial/footer/newbottom')

        </div>
        <!--social widgets-->
        @include('web/partial/social_widgets/main')
        <!--login popup-->

        <button class="t_align_c r_corners tr_all_hover type_2 animate_ftl" id="go_to_top"><i class="fa fa-angle-up"></i>
        </button>

        <!--scripts include-->
        @include('web/partial/script/core')
        <script src="{{asset('/template_resource/js/scripts.js')}}"></script>

        <script>

                            function changeAction(){
                            var myselect = document.getElementById("selectViewSize");
                                    // alert(myselect.options[myselect.selectedIndex].value);
                                    var searchParams = "";
                                    var urlParams = url('?');
                                    try{
                            if (typeof urlParams != "undefined" && typeof urlParams.q != "undefined"){
                            searchParams = "q=" + encodeURIComponent(url('?').q) + "&";
                            }
                            } catch (ex){
                            console.log(ex);
                            }

                            var value = myselect.options[myselect.selectedIndex].value;
                                    var locationUrl = window.location.origin + window.location.pathname;
                                    window.location = locationUrl + "?" + searchParams + "limit=" + value + "&page=0";
                                    }
                    function specificPage(page){

                    var myselect = document.getElementById("selectViewSize");
                            var value = myselect.options[myselect.selectedIndex].value;
                            var url = window.location.origin + window.location.pathname;
                            window.location = url + "?limit=" + value + "&page=" + page;
                            }
                    function nextPage(page){

                    if (!isNaN(page)){
                    page = parseInt(page);
                            page++;
                    } else{
                    return;
                    }
                    var lastPage = $('#paginationNumberUl').find('li').length - 1;
                            if (page > lastPage){
                    return;
                    }

                    var myselect = document.getElementById("selectViewSize");
                            var value = myselect.options[myselect.selectedIndex].value;
                            var url = window.location.origin + window.location.pathname;
                            window.location = url + "?limit=" + value + "&page=" + page;
                            }
                    function prevPage(page){

                    if (!isNaN(page)){
                    page = parseInt(page);
                            page--;
                    } else{
                    return;
                    }
                    var lastPage = $('#paginationNumberUl').find('li').length - 1;
                            if (page > lastPage){
                    return;
                    }

                    var myselect = document.getElementById("selectViewSize");
                            var value = myselect.options[myselect.selectedIndex].value;
                            var url = window.location.origin + window.location.pathname;
                            window.location = url + "?limit=" + value + "&page=" + page;
                            }
        </script>


    </body>
</html>