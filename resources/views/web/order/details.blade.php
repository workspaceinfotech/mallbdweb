<!doctype html>
<!--[if IE 9 ]><html class="ie9" lang="en" ng-app="mallBd"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" ng-app="mallBd"><!--<![endif]-->

@include('web/partial/header/mainStart')
@include('web/partial/header/cartScript')
@include('web/partial/header/mainEnds')

<body id="MainWrap"  ng-controller="CartController" >
    @include('web/partial/loader')
<!--wide layout-->
<div class="wide_layout relative">
    <!--[if (lt IE 9) | IE 9]>
    <div style="background:#fff;padding:8px 0 10px;">
        <div class="container" style="width:1170px;">
            <div class="row wrapper">
                <div class="clearfix" style="padding:9px 0 0;float:left;width:83%;"><i
                        class="fa fa-exclamation-triangle scheme_color f_left m_right_10"
                        style="font-size:25px;color:#e74c3c;"></i><b style="color:#e74c3c;">Attention! This page may not
                    display correctly.</b> <b>You are using an outdated version of Internet Explorer. For a faster, safer
                    browsing experience.</b></div>
                <div class="t_align_r" style="float:left;width:16%;"><a
                        href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"
                        class="button_type_4 r_corners bg_scheme_color color_light d_inline_b t_align_c" target="_blank"
                        style="margin-bottom:2px;">Update Now!</a></div>
            </div>
        </div>
    </div>
    <![endif]-->
    <!--markup header-->
    <header role="banner" class="type_5 fixed-top">

        <!--header top part-->
        @include('web/partial/header/top')

                <!--main menu container-->
        @include('web/partial/new_menu')
    </header>

    <div class="other-free-gap">

    </div>
    <!--breadcrumbs-->
    <section class="breadcrumbs">
        <div class="container">
            <ul class="horizontal_list clearfix bc_list f_size_medium">
                <li class="m_right_10 current"><a href="{{url('/home')}}" class="default_t_color">Home<i
                                class="fa fa-angle-right d_inline_middle m_left_10"></i></a></li>
                <li><a href="javascript:void(0)" class="default_t_color">Order Details</a></li>
            </ul>
        </div>
    </section>
    <!--content-->
    <div class="page_content_offset">
        <div class="container">
            <div class="row clearfix">
                <!--left content column-->
                <section class="col-lg-12 col-md-12 col-sm-12 m_xs_bottom_30">
                    <h2 class="tt_uppercase color_dark m_bottom_25">Order Details</h2>
                    <!--cart table-->
                    <table class="table_type_4 responsive_table full_width r_corners wraper shadow t_align_l t_xs_align_c m_bottom_30">
                        <thead>
                        <tr class="f_size_large">
                            <!--titles for td-->

                            <th>Product Image &amp; Name</th>
                            <th>Type</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Subtotal</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                       
                            $total = 0;
                        ?>
                        @foreach($orderDetails->orderProducts as $orderProduct)
                            @if($orderProduct->itemType==\App\Model\DataModel\OrderProducts::$_PRODUCT && $orderProduct->packageId==0)
                                <tr>
                                    <!--Product name and image-->
                                    <td data-title="Product Image &amp; name"  class="t_md_align_c">

                                        <img src="{{$imageUrl.'product/thumbnail/'.@$orderProduct->item->pictures[0]->name}}" alt="" class="m_md_bottom_5 d_xs_block d_xs_centered" onerror="this.src='http://placehold.it/90x90?text=No Image Found'">
                                        <a href="{{url('/product').'/'.$orderProduct->item->id}}" class="d_inline_b m_left_5 color_dark"  >{{$orderProduct->item->title}} </a>
                                    </td>
                                    <!--product key-->
                                    <td data-title="SKU" width="30%">
                                        @if(count($orderProduct->selectedProductAttribute)>0)
                                            <p class="fw_medium f_size_large  t_xs_align_c" >Product types</p><br>
                                        @endif
                                        @foreach($orderProduct->selectedProductAttribute as $selectedProductAttribute)
                                            <p class="f_size_large color_dark" >
                                                @foreach($selectedProductAttribute->selectedAttribute as $selectedAttribute)
                                                    <span class="f_size_medium ng-binding" >{{$selectedAttribute->name}}</span>
                                                    @foreach($selectedAttribute->attributesValue as $attributesValue)
                                                        <span class="f_size_medium">:</span>
                                                        <span class="f_size_medium ng-binding" >{{$attributesValue->value}}</span>
                                                    @endforeach
                                                    x <b class="color_dark ng-binding" >{{$selectedProductAttribute->quantity}}</b>
                                                @endforeach
                                            </p>
                                        @endforeach

                                    </td>
                                    <!--product price-->
                                    <td data-title="Price">
                                        <p class="f_size_large color_dark" ><?php echo $currency->HTMLCode; ?>; {{$orderProduct->price}}</p>
                                    </td>
                                    <!--quanity-->
                                    <td data-title="Quantity">
                                        <p class="f_size_large color_dark">{{$orderProduct->productQuantity}}</p>
                                    </td>
                                    <!--subtotal-->
                                    <td data-title="Subtotal">
                                        <p class="f_size_large fw_medium scheme_color" ><?php echo $currency->HTMLCode; ?>; {{$orderProduct->total}}</p>
                                        <?php $total += $orderProduct->total; ?>
                                    </td>
                                </tr>
                           @elseif($orderProduct->itemType==\App\Model\DataModel\OrderProducts::$_PACKAGE && $orderProduct->packageId>0)
                                <tr>

                                    <!--Product name and image-->
                                    <td data-title="Product Image &amp; name" class="t_md_align_c">

                                        <img src="{{$imageUrl.'package/thumbnail/'.@$orderProduct->item->image}}" alt="" class="m_md_bottom_5 d_xs_block d_xs_centered" onerror="this.src='http://placehold.it/90x90?text=No Image Found'">
                                        <a href="{{url('/packages').'/'.$orderProduct->item->id}}" class="d_inline_b m_left_5 color_dark"  >{{$orderProduct->item->packageTitle}} </a>
                                    </td>
                                    <!--product key-->
                                    <td data-title="SKU" width="30%">
                                        @if(count($orderProduct->item->packageProduct)>0)
                                            <p class="fw_medium f_size_large  t_xs_align_c" >Product in package</p><br>
                                        @endif
                                        <ul>
                                            <?php $count=1; ?>

                                            @foreach($orderProduct->item->packageProduct as $packageProduct)
                                                <li>
                                                    <a href="{{url('/product').'/'.$packageProduct->product->id}}" class="d_inline_b m_left_5 color_dark"  ><span><b>{{$count++}}.</b></span>{{strlen($packageProduct->product->title) > 27 ? substr($packageProduct->product->title,0,27)."..." : $packageProduct->product->title}} </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </td>
                                    <!--product price-->
                                    <td data-title="Price">
                                        <p class="f_size_large color_dark" ><?php echo $currency->HTMLCode; ?> {{$orderProduct->price}}</p>
                                    </td>
                                    <!--quanity-->
                                    <td data-title="Quantity">
                                        <p class="f_size_large color_dark">{{$orderProduct->productQuantity}}</p>
                                    </td>
                                    <!--subtotal-->
                                    <td data-title="Subtotal">
                                        <p class="f_size_large fw_medium scheme_color" ><?php echo $currency->HTMLCode; ?> {{$orderProduct->total}}</p>
                                        <?php $total += $orderProduct->total; ?>
                                    </td>
                                </tr>
                           @endif
                        @endforeach
                         <?php $total =  $orderDetails->orderTotal + $orderDetails->shipping_cost -$orderDetails->discount_total - $orderDetails->voucher_discount  ?>
                        <tr>
                            <td colspan="4">
                                <p class="fw_medium f_size_large t_align_r t_xs_align_c">Sub Total:</p>
                            </td>
                            <td colspan="1">
                                <p class="fw_medium f_size_large color_dark" ><span><?php echo $currency->HTMLCode; ?></span>&nbsp;<span >{{$orderDetails->orderTotal}}</span></p>
                            </td>
                        </tr>
                        <!--prices-->                        
                        <tr>
                            <td colspan="4">
                                <p class="fw_medium f_size_large t_align_r t_xs_align_c">Shipment Fee:</p>
                            </td>
                            <td colspan="1">
                                <p class="fw_medium f_size_large color_dark"><span><?php echo $currency->HTMLCode; ?></span>&nbsp;<span>{{$orderDetails->shipping_cost}}</span></p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <p class="fw_medium f_size_large t_align_r t_xs_align_c">Coupon Discount:</p>
                            </td>
                            <td colspan="1">
                                <p class="fw_medium f_size_large color_dark"><span><?php echo $currency->HTMLCode; ?></span>&nbsp;<span>{{$orderDetails->voucher_discount}}</span></p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <p class="fw_medium f_size_large t_align_r t_xs_align_c">Product Discount:</p>
                            </td>
                            <td colspan="1">
                                <p class="fw_medium f_size_large color_dark"><span><?php echo $currency->HTMLCode; ?></span>&nbsp;<span>{{$orderDetails->discount_total}}</span></p>
                            </td>
                        </tr>
                        <!--total-->
                        <tr>
                            <td colspan="4" class="v_align_m d_ib_offset_large t_xs_align_l">
                                <!--coupon-->
                                {{--<form class="d_ib_offset_0 d_inline_middle half_column d_xs_block w_xs_full m_xs_bottom_5">--}}
                                {{--<input type="text" placeholder="Enter your coupon code" name="" class="r_corners f_size_medium">--}}
                                {{--<button class="button_type_4 r_corners bg_light_color_2 m_left_5 mw_0 tr_all_hover color_dark">Save--}}
                                {{--</button>--}}
                                {{--</form>--}}
                                <p class="fw_medium f_size_large t_align_r t_xs_align_c scheme_color">
                                    Total:</p>
                            </td>
                            <td colspan="1" class="v_align_m">
                                <p class="fw_medium f_size_large scheme_color m_xs_bottom_10" ><span ><?php echo $currency->HTMLCode; ?></span>&nbsp;<span>{{$total}}</span></p>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                </section>
                <!--right column-->
                {{--<aside class="col-lg-3 col-md-3 col-sm-3">--}}
                    {{--<!--widgets-->--}}
                    {{--<figure class="widget shadow r_corners wrapper m_bottom_30">--}}
                        {{--<figcaption>--}}
                            {{--<h3 class="color_light">Categories</h3>--}}
                        {{--</figcaption>--}}
                        {{--<div class="widget_content">--}}
                            {{--<!--Categories list-->--}}
                            {{--@foreach($categoryList as $category)--}}
                                {{--<ul class="categories_list">--}}
                                    {{--<li>--}}
                                        {{--<a href="{{url("category/$category->id")}}" class="f_size_large scheme_color d_block relative">--}}
                                            {{--<b>{{$category->title}}</b>--}}
                                            {{--<span class="bg_light_color_1 r_corners f_right color_dark talign_c"></span>--}}
                                        {{--</a>--}}
                                        {{--<!--second level-->--}}
                                        {{--@if(!empty(@$category->childrens))--}}
                                            {{--<ul style="display: none">--}}
                                                {{--@foreach(@$category->childrens as $firstChild)--}}
                                                    {{--<li> --}}{{--class="active"--}}
                                                        {{--<a href="{{url("category/$category->id/$firstChild->id")}}" class="d_block f_size_large color_dark relative">--}}
                                                            {{--{{$firstChild->title}}<span class="bg_light_color_1 r_corners f_right color_dark talign_c"></span>--}}
                                                        {{--</a>--}}
                                                        {{--<!--third level-->--}}
                                                        {{--@if(!empty($firstChild->childrens))--}}
                                                            {{--<ul style="display: none">--}}
                                                                {{--@foreach(@$firstChild->childrens as $secondChild)--}}
                                                                    {{--<li>--}}
                                                                        {{--<a href="{{url("category/$category->id/$firstChild->id/$secondChild->id")}}" class="color_dark d_block">--}}
                                                                            {{--{{$secondChild->title}}</a>--}}
                                                                    {{--</li>--}}
                                                                {{--@endforeach--}}
                                                            {{--</ul>--}}
                                                        {{--@endif--}}
                                                    {{--</li>--}}
                                                {{--@endforeach--}}
                                            {{--</ul>--}}
                                        {{--@endif--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--@endforeach--}}
                        {{--</div>--}}
                    {{--</figure>--}}
                    {{--<!--compare products-->--}}

                    {{--<!--wishlist-->--}}

                    {{--<!--banner-->--}}
                    {{--<a href="#" class="d_block r_corners m_bottom_30">--}}
                        {{--<img src="{{asset('/template_resource/images/banner_img_6.jpg')}}" alt="">--}}
                    {{--</a>--}}

                {{--</aside>--}}
            </div>
        </div>
    </div>
    <!--markup footer-->
    @include('web/partial/footer/newbottom')
</div>
<!--social widgets-->
@include('web/partial/social_widgets/main')
<!--<ul class="social_widgets d_xs_none">
    facebook
    <li class="relative">
        <button class="sw_button t_align_c facebook"><i class="fa fa-facebook"></i></button>
        <div class="sw_content">
            <h3 class="color_dark m_bottom_20">Join Us on Facebook</h3>
            <iframe src="http://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fenvato&amp;width=235&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=438889712801266"
                    style="border:none; overflow:hidden; width:235px; height:258px;"></iframe>
        </div>
    </li>
    twitter feed
    <li class="relative">
        <button class="sw_button t_align_c twitter"><i class="fa fa-twitter"></i></button>
        <div class="sw_content">
            <h3 class="color_dark m_bottom_20">Latest Tweets</h3>

            <div class="twitterfeed m_bottom_25"></div>
            <a role="button" class="button_type_4 d_inline_b r_corners tr_all_hover color_light tw_color"
               href="https://twitter.com/fanfbmltemplate">Follow on Twitter</a>
        </div>
    </li>
    contact form
    <li class="relative">
        <button class="sw_button t_align_c contact"><i class="fa fa-envelope-o"></i></button>
        <div class="sw_content">
            <h3 class="color_dark m_bottom_20">Contact Us</h3>

            <p class="f_size_medium m_bottom_15">Lorem ipsum dolor sit amet, consectetuer adipis mauris</p>

            <form id="contactform" class="mini">
                <input class="f_size_medium m_bottom_10 r_corners full_width" type="text" name="cf_name"
                       placeholder="Your name">
                <input class="f_size_medium m_bottom_10 r_corners full_width" type="email" name="cf_email"
                       placeholder="Email">
                <textarea class="f_size_medium r_corners full_width m_bottom_20" placeholder="Message"
                          name="cf_message"></textarea>
                <button type="submit" class="button_type_4 r_corners mw_0 tr_all_hover color_dark bg_light_color_2">
                    Send
                </button>
            </form>
        </div>
    </li>
    contact info
    <li class="relative">
        <button class="sw_button t_align_c googlemap"><i class="fa fa-map-marker"></i></button>
        <div class="sw_content">
            <h3 class="color_dark m_bottom_20">Store Location</h3>
            <ul class="c_info_list">
                <li class="m_bottom_10">
                    <div class="clearfix m_bottom_15">
                        <i class="fa fa-map-marker f_left color_dark"></i>

                        <p class="contact_e">8901 Marmora Road,<br> Glasgow, D04 89GR.</p>
                    </div>
                    <iframe class="r_corners full_width" id="gmap_mini"
                            src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=ru&amp;geocode=&amp;q=Manhattan,+New+York,+NY,+United+States&amp;aq=0&amp;oq=monheten&amp;sll=37.0625,-95.677068&amp;sspn=65.430355,129.814453&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=%D0%9C%D0%B0%D0%BD%D1%85%D1%8D%D1%82%D1%82%D0%B5%D0%BD,+%D0%9D%D1%8C%D1%8E-%D0%99%D0%BE%D1%80%D0%BA,+%D0%9D%D1%8C%D1%8E+%D0%99%D0%BE%D1%80%D0%BA,+%D0%9D%D1%8C%D1%8E-%D0%99%D0%BE%D1%80%D0%BA&amp;ll=40.790278,-73.959722&amp;spn=0.015612,0.031693&amp;z=13&amp;output=embed"></iframe>
                </li>
                <li class="m_bottom_10">
                    <div class="clearfix m_bottom_10">
                        <i class="fa fa-phone f_left color_dark"></i>

                        <p class="contact_e">800-559-65-80</p>
                    </div>
                </li>
                <li class="m_bottom_10">
                    <div class="clearfix m_bottom_10">
                        <i class="fa fa-envelope f_left color_dark"></i>
                        <a class="contact_e default_t_color" href="mailto:#">info@companyname.com</a>
                    </div>
                </li>
                <li>
                    <div class="clearfix">
                        <i class="fa fa-clock-o f_left color_dark"></i>

                        <p class="contact_e">Monday - Friday: 08.00-20.00 <br>Saturday: 09.00-15.00<br> Sunday: closed
                        </p>
                    </div>
                </li>
            </ul>
        </div>
    </li>
</ul>-->
<!--login popup-->
<div class="popup_wrap d_none" id="login_popup">
    <section class="popup r_corners shadow">
        <button class="bg_tr color_dark tr_all_hover text_cs_hover close f_size_large"><i class="fa fa-times"></i>
        </button>
        <h3 class="m_bottom_20 color_dark">Log In</h3>

        <form>
            <ul>
                <li class="m_bottom_15">
                    <label for="username" class="m_bottom_5 d_inline_b">Username</label><br>
                    <input type="text" name="" id="username" class="r_corners full_width">
                </li>
                <li class="m_bottom_25">
                    <label for="password" class="m_bottom_5 d_inline_b">Password</label><br>
                    <input type="password" name="" id="password" class="r_corners full_width">
                </li>
                <li class="m_bottom_15">
                    <input type="checkbox" class="d_none" id="checkbox_10"><label for="checkbox_10">Remember me</label>
                </li>
                <li class="clearfix m_bottom_30">
                    <button class="button_type_4 tr_all_hover r_corners f_left bg_scheme_color color_light f_mxs_none m_mxs_bottom_15">
                        Log In
                    </button>
                    <div class="f_right f_size_medium f_mxs_none">
                        <a href="#" class="color_dark">Forgot your password?</a><br>
                        <a href="#" class="color_dark">Forgot your username?</a>
                    </div>
                </li>
            </ul>
        </form>
        <footer class="bg_light_color_1 t_mxs_align_c">
            <h3 class="color_dark d_inline_middle d_mxs_block m_mxs_bottom_15">New Customer?</h3>
            <a href="#" role="button"
               class="tr_all_hover r_corners button_type_4 bg_dark_color bg_cs_hover color_light d_inline_middle m_mxs_left_0">Create
                an Account</a>
        </footer>
    </section>
</div>
<button class="t_align_c r_corners tr_all_hover type_2 animate_ftl" id="go_to_top" ><i class="fa fa-angle-up"></i>
</button>
<!-- Developer : hidden value-->
<input type="hidden" id="isCheckoutPage" value = "true"/>
<!--scripts include-->
<!--scripts include-->
@include('web/partial/script/core')
<script src="{{asset('/developer/js/script/chackout_script.js')}}"></script>

</body>
</html>