@foreach($orderList as $order)
    <?php $count = 0; ?>
    <tr>

        <td data-title="SKU"><a href="{{url('/order/details/').'/'.$order->id}}" >{{$order->invoiceNo}}</a></td>
        <td data-title="Product Name">

            @foreach($order->orderProducts as $orderProduct)
                @if($orderProduct->itemType==\App\Model\DataModel\OrderProducts::$_PRODUCT && $orderProduct->packageId==0)
                    <?php $count++; ?>
                    <a href="{{url('/product/'.$orderProduct->item->url.'/'.$orderProduct->item->code)}}" class="color_dark d_inline_b m_bottom_5"><span><b>{{$count}}. </b></span>{{ $orderProduct->item->title }}</a><br>
                @elseif($orderProduct->itemType==\App\Model\DataModel\OrderProducts::$_PACKAGE && $orderProduct->packageId>0)
                    <?php $count++; ?>
                    <a href="{{url('/packages/'.urlencode($orderProduct->item->packageTitle).'/'.$orderProduct->item->id)}}" class="color_dark d_inline_b m_bottom_5"><span><b>{{$count}}. </b></span>{{ $orderProduct->item->packageTitle }}</a><br>

                @endif
            @endforeach
        </td>
        {{--<td data-title="Product Status"><div>{{ $order->orderStatus->orderStatusList->statusName }}</div></td>

        <td data-title="Total Qty">{{ $order->totalQuantity }}</td>
        <td data-title="Tax"><span>&#2547</span>0</td>
        <td data-title="Discount"><span>&#2547</span>0</td>--}}
        <td data-title="Total"><p class="color_dark f_size_large"><span>&#2547</span>{{ number_format(($order->orderTotal - $order->voucher_discount - $order->discount_total + $order->shipping_cost - $order->employee_discount),2) }}</p></td>
        <td data-title="Product Status"><p class="color_dark f_size_large">{{ $order->orderStatus->orderStatusList->statusName }}</p></td>
    </tr>
@endforeach