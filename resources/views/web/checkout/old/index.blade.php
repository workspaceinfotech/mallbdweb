<!doctype html>
<!--[if IE 9 ]><html class="ie9" lang="en" ng-app="mallBd"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" ng-app="mallBd"><!--<![endif]-->

@include('web.partial.header.mainStart')
@include('web.partial.header.cartScript')
@include('web.partial.header.mainEnds')

<body id="MainWrap"  ng-controller="CartController" >
<!--wide layout-->
<div class="wide_layout relative">
    <!--[if (lt IE 9) | IE 9]>
    <div style="background:#fff;padding:8px 0 10px;">
        <div class="container" style="width:1170px;">
            <div class="row wrapper">
                <div class="clearfix" style="padding:9px 0 0;float:left;width:83%;"><i
                        class="fa fa-exclamation-triangle scheme_color f_left m_right_10"
                        style="font-size:25px;color:#e74c3c;"></i><b style="color:#e74c3c;">Attention! This page may not
                    display correctly.</b> <b>You are using an outdated version of Internet Explorer. For a faster, safer
                    browsing experience.</b></div>
                <div class="t_align_r" style="float:left;width:16%;"><a
                        href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"
                        class="button_type_4 r_corners bg_scheme_color color_light d_inline_b t_align_c" target="_blank"
                        style="margin-bottom:2px;">Update Now!</a></div>
            </div>
        </div>
    </div>
    <![endif]-->
    <!--markup header-->
    <header role="banner" class="type_5 fixed-top">

        <!--header top part-->
        @include('web.partial.header.top')

                <!--main menu container-->
        @include('web.partial.new_menu')
    </header>

    <div class="other-free-gap">

    </div>
    <!--breadcrumbs-->
    <section class="breadcrumbs">
        <div class="container">
            <ul class="horizontal_list clearfix bc_list f_size_medium">
                <li class="m_right_10 current"><a href="#" class="default_t_color">Home<i
                                class="fa fa-angle-right d_inline_middle m_left_10"></i></a></li>
                <li class="m_right_10 current"><a href="#" class="default_t_color">Checkout<i
                                class="fa fa-angle-right d_inline_middle m_left_10"></i></a></li>
                <li><a href="#" class="default_t_color">Shopping Cart</a></li>
            </ul>
        </div>
    </section>
    <!--content-->
    <div class="page_content_offset">
        <div class="container">
            <div class="row clearfix">
                <!--left content column-->
                <section class="col-lg-9 col-md-9 col-sm-9 m_xs_bottom_30">
                    <h2 class="tt_uppercase color_dark m_bottom_25">Cart</h2>
                    <!--cart table-->
                    <table class="table_type_4 responsive_table full_width r_corners wraper shadow t_align_l t_xs_align_c m_bottom_30">
                        <thead>
                        <tr class="f_size_large">
                            <!--titles for td-->

                            <th>Product Image &amp; Name</th>
                            <th>Type</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Subtotal</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="productCell in shoppingCart.productCell track by $index" >

                            <!--Product name and image-->
                            <td data-title="Product Image &amp; name" class="t_md_align_c">
                                <img src="@{{getPictureUrl(productCell.product.pictures[0].name)}}" alt="" class="m_md_bottom_5 d_xs_block d_xs_centered">
                                <a href="{{url('/product')}} / @{{ productCell.product.id }}" class="d_inline_b m_left_5 color_dark" ng-bind="productCell.product.title" > </a>
                            </td>
                            <!--product key-->
                            <td data-title="SKU"> Product</td>
                            <!--product price-->
                            <td data-title="Price">
                                <s ng-if="productCell.product.previousPrice>0" class="v_align_b f_size_ex_large" ng-bind="'&#2547'+@{{productCell.product.previousPrice}}"></s>

                                <p class="f_size_large color_dark" ng-bind="'&#2547'+@{{productCell.product.prices[0].retailPrice}}" > </p>
                            </td>
                            <!--quanity-->
                            <td data-title="Quantity">
                                <div class="clearfix quantity r_corners d_inline_middle f_size_medium color_dark m_bottom_10">
                                    <button class="bg_tr d_block f_left" data-direction="down"  ng-click="decreaseProductInProductCell($index)" >-</button>
                                    <input type="text" name="" readonly value="@{{productCell.quantity}}" class="f_left">
                                    <button class="bg_tr d_block f_left" data-direction="up" ng-click="increaseProductInProductCell($index)" >+</button>
                                </div>
                                <div>
                                    {{--<a href="#" class="color_dark"><i class="fa fa-check f_size_medium m_right_5"></i>Update</a><br>--}}
                                    <a href="javascript:void(0)" ng-click="removeProductCell($index)" class="color_dark"><i class="fa fa-times f_size_medium m_right_5"></i>Remove</a><br>
                                </div>
                            </td>
                            <!--subtotal-->
                            <td data-title="Subtotal">
                                <p class="f_size_large fw_medium scheme_color" ng-bind="getPrice($index)"></p>
                            </td>
                        </tr>

                        <tr ng-repeat="mallBdPackageCell in shoppingCart.mallBdPackageCell track by $index" >

                            <!--Product name and image-->
                            <td data-title="Product Image &amp; name" class="t_md_align_c">
                                <img src="@{{getPackagesPictureUrl(mallBdPackageCell.mallBdPackage.image)}}" alt="" class="m_md_bottom_5 d_xs_block d_xs_centered">
                                <a href="{{url('/package')}} / @{{ mallBdPackageCell.mallBdPackage.id }}" class="d_inline_b m_left_5 color_dark" ng-bind="mallBdPackageCell.mallBdPackage.packageTitle" ></a>
                            </td>
                            <td data-title="SKU">Package</td>
                            <!--product price-->
                            <td data-title="Price">
                                {{--<s ng-if="productCell.product.previousPrice>0" class="v_align_b f_size_ex_large" ng-bind="'&#2547'+@{{productCell.product.previousPrice}}"></s>--}}

                                <p class="f_size_large color_dark" ng-bind="'&#2547'+@{{mallBdPackageCell.mallBdPackage.packagePriceTotal}}" > </p>
                            </td>
                            <!--quanity-->
                            <td data-title="Quantity">
                                <div class="clearfix quantity r_corners d_inline_middle f_size_medium color_dark m_bottom_10">
                                    <button class="bg_tr d_block f_left" data-direction="down"  ng-click="decreasePackageInPackageCell($index)" >-</button>
                                    <input type="text" name="" readonly value="@{{mallBdPackageCell.quantity}}" class="f_left">
                                    <button class="bg_tr d_block f_left" data-direction="up" ng-click="increasePackageInPackageCell($index)" >+</button>
                                </div>
                                <div>
                                    {{--<a href="#" class="color_dark"><i class="fa fa-check f_size_medium m_right_5"></i>Update</a><br>--}}
                                    <a href="javascript:void(0)" ng-click="removePackageCell($index)" class="color_dark"><i class="fa fa-times f_size_medium m_right_5"></i>Remove</a><br>
                                </div>
                            </td>
                            <!--subtotal-->
                            <td data-title="Subtotal">
                                <p class="f_size_large fw_medium scheme_color" ng-bind="getPackageItemPrice($index)"></p>
                            </td>
                        </tr>
                        <!--prices-->
                        <tr>
                            <td colspan="4">
                                <p class="fw_medium f_size_large t_align_r t_xs_align_c">Coupon Discount:</p>
                            </td>
                            <td colspan="1">
                                <p class="fw_medium f_size_large color_dark"><span ng-bind-html="currencySymbol"></span>&nbsp;<span>0</span></p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <p class="fw_medium f_size_large t_align_r t_xs_align_c">Subtotal:</p>
                            </td>
                            <td colspan="1">
                                <p class="fw_medium f_size_large color_dark" ><span ng-bind-html="currencySymbol"></span>&nbsp;<span ng-bind="getSubTotalAmount()"></span></p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <p class="fw_medium f_size_large t_align_r t_xs_align_c">Payment Fee:</p>
                            </td>
                            <td colspan="1">
                                <p class="fw_medium f_size_large color_dark"><span ng-bind-html="currencySymbol"></span>&nbsp;<span>0</span></p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <p class="fw_medium f_size_large t_align_r t_xs_align_c">Shipment Fee:</p>
                            </td>
                            <td colspan="1">
                                <p class="fw_medium f_size_large color_dark"><span ng-bind-html="currencySymbol"></span>&nbsp;<span ng-bind="deliveryMethod.deliveryPrice"></span></p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <p class="fw_medium f_size_large t_align_r t_xs_align_c">Tax Total:</p>
                            </td>
                            <td colspan="1">
                                <p class="fw_medium f_size_large color_dark"><span ng-bind-html="currencySymbol"></span>&nbsp;<span>0</span></p>
                            </td>
                        </tr>
                        <!--total-->
                        <tr>
                            <td colspan="4" class="v_align_m d_ib_offset_large t_xs_align_l">
                                <!--coupon-->
                                {{--<form class="d_ib_offset_0 d_inline_middle half_column d_xs_block w_xs_full m_xs_bottom_5">--}}
                                    {{--<input type="text" placeholder="Enter your coupon code" name="" class="r_corners f_size_medium">--}}
                                    {{--<button class="button_type_4 r_corners bg_light_color_2 m_left_5 mw_0 tr_all_hover color_dark">Save--}}
                                    {{--</button>--}}
                                {{--</form>--}}
                                <p class="fw_medium f_size_large t_align_r t_xs_align_c scheme_color">
                                    Total:</p>
                            </td>
                            <td colspan="1" class="v_align_m">
                                <p class="fw_medium f_size_large scheme_color m_xs_bottom_10" ><span ng-bind-html="currencySymbol"  ></span>&nbsp;<span ng-bind="getTotalAmount()"  ></span></p>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <h2 ng-show="shippingAddressList.length > 0" class="color_dark tt_uppercase m_bottom_25">Select a shipping address</h2>
                    <div ng-repeat="shippingAddress in shippingAddressList">
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-6 m_xs_bottom_30 bs_inner_offsets bg_light_color_3 shadow r_corners m_bottom_45">
                                    <h3 class="checkout-name" ng-bind="shippingAddress.shippingCity"></h3>
                                    <address class="checkout-address" ng-bind="shippingAddress.shippingAddress">
                                    </address>
                                    <br>
                                    <button class="button_type_4 r_corners bg_scheme_color color_light tr_all_hover" ng-click="setShippingAddressAndFocusToElement($index,'element_shipping_address')">Ship to this address</button>
                                    {{--<button class="button_type_4 r_corners bg_scheme_color color_light tr_all_hover edit-address">Edit</button>--}}
                                    <button ng-click="removeShippingAddress($index)" class="button_type_4 r_corners bg_scheme_color color_light tr_all_hover edit-address">Delete</button>
                                </div>
                            </div>

                    </div>

                    <h2 class="color_dark tt_uppercase m_bottom_25">Personal &amp; Shipping Information</h2>

                    <div class="bs_inner_offsets bg_light_color_3 shadow r_corners m_bottom_45">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-6 m_xs_bottom_30 angularFocus" scrolltopposition="950">
                                <h5 class="fw_medium m_bottom_15">Bill to</h5>

                                <form>
                                    <ul>
                                        <li class="m_bottom_15">
                                            <label for="element_email" class="d_inline_b m_bottom_5 required">Email</label>
                                            <input type="text" id="element_email" name="element_email" class="r_corners full_width" ng-model="userInfo.email" onchange="getUserInformation('element_email')" >
                                            <p class="alert-txt"  ng-hide="htmlManipulateObj.email.isHidden" ng-bind="htmlManipulateObj.email.htmlValus" ></p>
                                        </li>
                                        <li class="m_bottom_15">
                                            <label for="element_firstname" class="d_inline_b m_bottom_5">First name</label>
                                            <input type="text" id="element_firstname" name="first_name" class="r_corners full_width" ng-model="userInfo.firstName" >
                                            <p class="help-block" ng-hide="htmlManipulateObj.firstname.isHidden" ng-bind="htmlManipulateObj.firstname.htmlValus"></p>
                                        </li>

                                        <li class="m_bottom_15">
                                            <label for="element_lastname" class="d_inline_b m_bottom_5 required">Last Name</label>
                                            <input type="text" id="element_lastname" name="last_name" class="r_corners full_width" ng-model="userInfo.lastName" >
                                            <p class="alert-txt"  ng-hide="htmlManipulateObj.lastname.isHidden" ng-bind="htmlManipulateObj.lastname.htmlValus"></p>
                                        </li>
                                        <li class="m_bottom_15">
                                            <label for="element_phone" class="d_inline_b m_bottom_5">Phone</label>
                                            <input type="text" id="element_phone" name="phone" class="r_corners full_width" ng-model="userInfo.phone"  >
                                            <p class="alert-txt" ng-hide="htmlManipulateObj.phone.isHidden" ng-bind="htmlManipulateObj.phone.htmlValus"></p>
                                        </li>
                                        <li class="m_bottom_15">
                                            <label for="element_address" class="d_inline_b m_bottom_5 required">Address</label>
                                            <input type="text" id="element_address" name="address" class="r_corners full_width" ng-model="userInfo.userDetails.address.address" >
                                            <p class="alert-txt"  ng-hide="htmlManipulateObj.address.isHidden" ng-bind="htmlManipulateObj.address.htmlValus"></p>
                                        </li>
                                        <li class="m_bottom_15">
                                            <label for="element_city" class="d_inline_b m_bottom_5 required">City</label>
                                            <input type="text" id="element_city" name="city" class="r_corners full_width" ng-model="userInfo.userDetails.address.city" >
                                            <p class="alert-txt"  ng-hide="htmlManipulateObj.city.isHidden" ng-bind="htmlManipulateObj.city.htmlValus"></p>
                                        </li>
                                        <li class="m_bottom_15">
                                            <label for="element_zipcode" class="d_inline_b m_bottom_5 required">Zipcode</label>
                                            <input type="text" id="element_zipcode" name="zipcode" class="r_corners full_width" ng-model="userInfo.userDetails.address.zipCode" >
                                            <p class="alert-txt" ng-hide="htmlManipulateObj.zipcode.isHidden" ng-bind="htmlManipulateObj.zipcode.htmlValus"></p>
                                        </li>
                                        <li class="m_bottom_15">
                                            <label for="element_country" class="d_inline_b m_bottom_5 required">Country</label>
                                            <input type="text" id="element_country" name="country" class="r_corners full_width" ng-model="userInfo.userDetails.address.country" >
                                            <p class="alert-txt" class="help-block" ng-hide="htmlManipulateObj.country.isHidden" ng-bind="htmlManipulateObj.country.htmlValus"></p>
                                        </li>


                                    </ul>
                                </form>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 angularFocus" scrolltopposition="950">
                                <h5 class="fw_medium m_bottom_15">Ship To</h5>

                                <form>
                                    <ul>
                                        <li class="m_bottom_15">
                                            <label for="element_shipping_address" class="d_inline_b m_bottom_5">Shipping address</label>
                                            <input type="text" id="element_shipping_address" name="shipping_address" class="r_corners full_width" ng-model="shippingAddress.shippingAddress" >
                                            <p class="alert-txt"  ng-hide="htmlManipulateObj.shipping_address.isHidden" ng-bind="htmlManipulateObj.shipping_address.htmlValus"></p>
                                        </li>
                                        <li class="m_bottom_15">
                                            <label for="element_shipping_country" class="d_inline_b m_bottom_5">Shipping country</label>
                                            <input type="text" id="element_shipping_country" name="shipping_country" class="r_corners full_width" ng-model="shippingAddress.shippingCountry" >
                                            <p class="alert-txt" ng-hide="htmlManipulateObj.shipping_country.isHidden" ng-bind="htmlManipulateObj.shipping_country.htmlValus"></p>
                                        </li>
                                        <li class="m_bottom_15">
                                            <label for="element_shipping_zipcode" class="d_inline_b m_bottom_5">Shipping zipcode</label>
                                            <input  id="element_shipping_zipcode" type="text"class="r_corners full_width" ng-model="shippingAddress.shippingZipCode" >
                                            <p class="alert-txt"  ng-hide="htmlManipulateObj.shipping_zipcode.isHidden" ng-bind="htmlManipulateObj.shipping_zipcode.htmlValus"></p>
                                        </li>
                                        <li class="m_bottom_15">
                                            <label for="element_shipping_city" class="d_inline_b m_bottom_5">Shipping city</label>
                                            <input type="text"  id="element_shipping_city" name="shipping_city"  name="shipping_zipcode" class="r_corners full_width" ng-model="shippingAddress.shippingCity" >
                                            <p class="alert-txt"  ng-hide="htmlManipulateObj.shipping_city.isHidden" ng-bind="htmlManipulateObj.shipping_city.htmlValus"></p>
                                        </li>
                                        <li class="m_bottom_15">


                                        </li>
                                    </ul>
                                </form>
                            </div>
                        </div>
                    </div>
                    <h2 class="tt_uppercase color_dark m_bottom_30">Delivery methods</h2>
                    <p class="alert-txt"  ng-hide="htmlManipulateObj.delivery_method_id.isHidden" ng-bind="htmlManipulateObj.delivery_method_id.htmlValus"></p>
                    <div id="element_delivery_method_id" class="bs_inner_offsets bg_light_color_3 shadow r_corners m_bottom_45 angularFocus"  scrolltopposition="1790">

                        <div ng-repeat="tempDeliveryMethod in deliveryMethods">
                            <figure class="block_select clearfix relative m_bottom_15 deliveryMethodRadioForAngular" ng-click="selectDeliveryMethods($index)" >
                                <input type="radio" name="delivery_method" class="d_none">
                                <img src="{{$imageUrl.'/delivery/'}}@{{ tempDeliveryMethod.icon }}" alt="" class="f_left m_right_20 f_mxs_none m_mxs_bottom_10">
                                <figcaption>
                                    <div class="d_table_cell d_sm_block p_sm_right_0 p_right_45 m_mxs_bottom_5">
                                    <h5 class="color_dark fw_medium m_bottom_15 m_sm_bottom_5" ng-bind="tempDeliveryMethod.title"></h5>

                                    <p>Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor wisi et urna. Aliquam
                                        erat volutpat. Duis ac turpis. Donec sit amet eros. Lorem ipsum dolor sit amet, consecvtetuer. </p>
                                    </div>
                                    <div class="d_table_cell d_sm_block discount">
                                        <h5 class="color_dark fw_medium m_bottom_15 m_sm_bottom_0">Charges/Fee</h5>

                                        <p class="color_dark"><span ng-bind-html="currencySymbol"></span> @{{tempDeliveryMethod.deliveryPrice}}</p>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="d_table_cell d_sm_block p_sm_right_0 p_right_45 m_mxs_bottom_5" ng-show="deliveryMethod.id == 1"  ng-if="$index==0" >
                                <h5 class="color_dark fw_medium m_bottom_15 m_sm_bottom_0">Area Information</h5>
                                <p class="alert-txt"  ng-hide="htmlManipulateObj.zone_id.isHidden" ng-bind="htmlManipulateObj.zone_id.htmlValus"></p>
                                <select id="element_zone_id" ng-model="zone.id"  >
                                    <option value="0">Select area</option>
                                    <option ng-selected="zone.id == option.id" ng-repeat="option in zones"  value="@{{option.id}}">@{{option.area}}</option>
                                </select>
                             </div>
                            <hr class="m_bottom_20" ng-if="$index<deliveryMethods.length-1">
                        </div>

                    </div>
                    <h2 class="tt_uppercase color_dark m_bottom_30">Payment</h2>
                    <p class="alert-txt"  ng-hide="htmlManipulateObj.payment_method_id.isHidden" ng-bind="htmlManipulateObj.payment_method_id.htmlValus"></p>


                    <div id="element_payment_method_id" class="bs_inner_offsets bg_light_color_3 shadow r_corners m_bottom_45 angularFocus" scrolltopposition="2250">

                        <div ng-repeat="temPaymentMethod in paymentMethods">
                            <figure class="block_select clearfix relative m_bottom_15 paymentMethodRadioForAngular" ng-click="selectPaymentMethods($index)" >
                                <input type="radio" name="radio_2" class="d_none">
                                {{--onerror="this.src='{{asset('/template_resource/images/payment_logo.jpg')}}'"--}}
                                <img src="@{{imageUrl+'payment/'+temPaymentMethod.icon}}"  alt="" class="f_left m_right_20 f_mxs_none m_mxs_bottom_10">
                                <figcaption class="d_table d_sm_block">
                                    <div class="d_table_cell d_sm_block p_sm_right_0 p_right_45 m_mxs_bottom_5">
                                        <h5 class="color_dark fw_medium m_bottom_15 m_sm_bottom_5" ng-bind="temPaymentMethod.methodTitle"></h5>

                                        <p>Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor wisi et urna.
                                            Aliquam erat volutpat. Duis ac turp. Donec sit amet eros. </p>
                                    </div>

                                </figcaption>
                            </figure>
                            <hr class="m_bottom_20" ng-if="$index<paymentMethods.length-1">
                        </div>

                    </div>
                    <h2 class="tt_uppercase color_dark m_bottom_30">Terms of service</h2>

                    <div class="bs_inner_offsets bg_light_color_3 shadow r_corners m_bottom_45">
                        <p class="m_bottom_10">Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor wisi et
                            urna. Aliquam erat volutpat. Duis ac turpis. Donec sit amet eros. Lorem ipsum dolor sit amet, consecvtetuer
                            adipiscing elit. Mauris fermentum dictum magna. Sed laoreet aliquam leo. Ut tellus dolor, dapibus eget,
                            elementum vel, cursus eleifend, elit. Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Integer
                            rutrum ante eu lacus.Vestibulum libero nisl, porta vel, scelerisque eget, malesuada at, neque. </p>

                        <p>Vivamus eget nibh. Etiam cursus leo vel metus. Nulla facilisi. Aenean nec eros. Vestibulum ante ipsum primis in
                            faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse sollicitudin velit sed leo. Ut pharetra
                            augue nec augue. Nam elit agna,endrerit sit amet, tincidunt ac, viverra sed, nulla. Donec porta diam eu massa.
                            Quisque diam lorem, interdum vitae,dapibus ac, scelerisque vitae, pede. Donec eget tellus non erat lacinia
                            fermentum. Donec in velit vel ipsum auctor pulvinar. </p>
                    </div>
                    <h2 class="tt_uppercase color_dark m_bottom_30">Notes and special requests</h2>
                    <!--requests table-->
                    <table class="table_type_5 full_width r_corners wraper shadow t_align_l">
                        <tr>
                            <td colspan="2">
                                <label for="notes" class="d_inline_b m_bottom_5">Notes and special requests:</label>
                                <textarea id="notes" class="r_corners notes full_width"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td class="t_align_r">
                                <p class="f_size_large fw_medium">Coupon Discount:</p>
                            </td>
                            <td>
                                <p class="f_size_large fw_medium color_dark"><span ng-bind-html="currencySymbol"  ></span><span>0</span></p>
                            </td>
                        </tr>
                        <tr>
                            <td class="t_align_r">
                                <p class="f_size_large fw_medium">Subtotal:</p>
                            </td>
                            <td>
                                <p class="f_size_large fw_medium color_dark" ><span ng-bind-html="currencySymbol"  ></span>&nbsp;<span ng-bind="getSubTotalAmount()"  ></span></p>
                            </td>
                        </tr>
                        <tr>
                            <td class="t_align_r">
                                <p class="f_size_large fw_medium">Payment Fee:</p>
                            </td>
                            <td>
                                <p class="f_size_large fw_medium color_dark"><span ng-bind-html="currencySymbol"  ></span><span>0</span></p>
                            </td>
                        </tr>
                        <tr>
                            <td class="t_align_r">
                                <p class="f_size_large fw_medium">Shipment Fee:</p>
                            </td>
                            <td>
                                <p class="f_size_large fw_medium color_dark" ><span ng-bind-html="currencySymbol"></span>&nbsp;<span ng-bind="deliveryMethod.deliveryPrice"></span></p>
                            </td>
                        </tr>
                        <tr>
                            <td class="t_align_r">
                                <p class="f_size_large fw_medium">Tax Total:</p>
                            </td>
                            <td>
                                <p class="f_size_large fw_medium color_dark"><span ng-bind-html="currencySymbol"></span>&nbsp;<span>0</span></p>
                            </td>
                        </tr>
                        <tr>
                            <td class="t_align_r">
                                <p class="f_size_large fw_medium scheme_color">Total:</p>
                            </td>
                            <td>
                                <p class="f_size_large fw_medium scheme_color"><span ng-bind-html="currencySymbol"  ></span>&nbsp;<span ng-bind="getTotalAmount()"  ></span></p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="checkbox" name="checkbox_8" id="checkbox_8"  ng-model="agreeTerms" class="d_none"  ><label for="checkbox_8">I agree to
                                    the Terms of Service (Terms of service)</label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <button class="button_type_6 bg_scheme_color f_size_large r_corners tr_all_hover color_light m_bottom_20" ng-click="submitOrder()" >
                                    Confirm Purchase
                                </button>
                                <p class="alert-text" ng-show="success.status" ng-bind="success.msg"></p>
                                <p class="alert-text" ng-show="!processing.done" ng-bind="processing.msg"></p>
                                <p class="alert-text" ng-show="responseError.status" ng-bind="responseError.msg"></p>

                            </td>

                        </tr>
                    </table>
                </section>
                <!--right column-->
                <aside class="col-lg-3 col-md-3 col-sm-3">
                    <!--widgets-->
                    <figure class="widget shadow r_corners wrapper m_bottom_30">
                        <figcaption>
                            <h3 class="color_light">Categories</h3>
                        </figcaption>
                        <div class="widget_content">
                            <!--Categories list-->
                            @foreach($categoryList as $category)
                                <ul class="categories_list">
                                    <li>
                                        <a href="{{url("category/$category->id")}}" class="f_size_large scheme_color d_block relative">
                                            <b>{{$category->title}}</b>
                                            <span class="bg_light_color_1 r_corners f_right color_dark talign_c"></span>
                                        </a>
                                        <!--second level-->
                                        @if(!empty(@$category->childrens))
                                            <ul style="display: none">
                                                @foreach(@$category->childrens as $firstChild)
                                                    <li> {{--class="active"--}}
                                                        <a href="{{url("category/$category->id/$firstChild->id")}}" class="d_block f_size_large color_dark relative">
                                                            {{$firstChild->title}}<span class="bg_light_color_1 r_corners f_right color_dark talign_c"></span>
                                                        </a>
                                                        <!--third level-->
                                                        @if(!empty($firstChild->childrens))
                                                            <ul style="display: none">
                                                                @foreach(@$firstChild->childrens as $secondChild)
                                                                    <li>
                                                                        <a href="{{url("category/$category->id/$firstChild->id/$secondChild->id")}}" class="color_dark d_block">
                                                                            {{$secondChild->title}}</a>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        @endif
                                                    </li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </li>
                                </ul>
                            @endforeach
                        </div>
                    </figure>
                    <!--compare products-->
                    <figure class="widget shadow r_corners wrapper m_bottom_30">
                        <figcaption>
                            <h3 class="color_light">Compare Products</h3>
                        </figcaption>
                        <div class="widget_content">
                            <div class="clearfix m_bottom_15 relative cw_product">
                                <img src="{{asset('/template_resource/images/bestsellers_img_1.jpg')}}" alt=""
                                     class="f_left m_right_15 m_sm_bottom_10 f_sm_none f_xs_left m_xs_bottom_0">
                                <a href="#" class="color_dark d_block bt_link">Ut tellus dolor dapibus</a>
                                <button type="button" class="f_size_medium f_right color_dark bg_tr tr_all_hover close_fieldset"><i
                                            class="fa fa-times lh_inherit"></i></button>
                            </div>
                            <hr class="m_bottom_15">
                            <div class="clearfix m_bottom_25 relative cw_product">
                                <img src="{{asset('/template_resource/images/bestsellers_img_2.jpg')}}" alt=""
                                     class="f_left m_right_15 m_sm_bottom_10 f_sm_none f_xs_left m_xs_bottom_0">
                                <a href="#" class="color_dark d_block bt_link">Elemenum vel</a>
                                <button type="button" class="f_size_medium f_right color_dark bg_tr tr_all_hover close_fieldset"><i
                                            class="fa fa-times lh_inherit"></i></button>
                            </div>
                            <a href="#" class="color_dark"><i class="fa fa-files-o m_right_10"></i>Go to Compare</a>
                        </div>
                    </figure>
                    <!--wishlist-->
                    <figure class="widget shadow r_corners wrapper m_bottom_30">
                        <figcaption>
                            <h3 class="color_light">Wishlist</h3>
                        </figcaption>
                        <div class="widget_content">
                            <div class="clearfix m_bottom_15 relative cw_product">
                                <img src="{{asset('/template_resource/images/bestsellers_img_1.jpg')}}" alt=""
                                     class="f_left m_right_15 m_sm_bottom_10 f_sm_none f_xs_left m_xs_bottom_0">
                                <a href="#" class="color_dark d_block bt_link">Ut tellus dolor dapibus</a>
                                <button type="button" class="f_size_medium f_right color_dark bg_tr tr_all_hover close_fieldset"><i
                                            class="fa fa-times lh_inherit"></i></button>
                            </div>
                            <hr class="m_bottom_15">
                            <div class="clearfix m_bottom_25 relative cw_product">
                                <img src="{{asset('/template_resource/images/bestsellers_img_2.jpg')}}" alt=""
                                     class="f_left m_right_15 m_sm_bottom_10 f_sm_none f_xs_left m_xs_bottom_0">
                                <a href="#" class="color_dark d_block bt_link">Elemenum vel</a>
                                <button type="button" class="f_size_medium f_right color_dark bg_tr tr_all_hover close_fieldset"><i
                                            class="fa fa-times lh_inherit"></i></button>
                            </div>
                            <a href="#" class="color_dark"><i class="fa fa-heart-o m_right_10"></i>Go to Wishlist</a>
                        </div>
                    </figure>
                    <!--banner-->
                    <a href="#" class="d_block r_corners m_bottom_30">
                        <img src="{{asset('/template_resource/images/banner_img_6.jpg')}}" alt="">
                    </a>
                    <!--Bestsellers-->
                    <figure class="widget shadow r_corners wrapper m_bottom_30">
                        <figcaption>
                            <h3 class="color_light">Bestsellers</h3>
                        </figcaption>
                        <div class="widget_content">
                            <div class="clearfix m_bottom_15">
                                <img src="{{asset('/template_resource/images/bestsellers_img_1.jpg')}}" alt=""
                                     class="f_left m_right_15 m_sm_bottom_10 f_sm_none f_xs_left m_xs_bottom_0">
                                <a href="#" class="color_dark d_block bt_link">Ut dolor dapibus</a>
                                <!--rating-->
                                <ul class="horizontal_list clearfix d_inline_b rating_list type_2 tr_all_hover m_bottom_10">
                                    <li class="active">
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                    <li class="active">
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                    <li class="active">
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                    <li class="active">
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                </ul>
                                <p class="scheme_color">$61.00</p>
                            </div>
                            <hr class="m_bottom_15">
                            <div class="clearfix m_bottom_15">
                                <img src="{{asset('/template_resource/images/bestsellers_img_2.jpg')}}" alt=""
                                     class="f_left m_right_15 m_sm_bottom_10 f_sm_none f_xs_left m_xs_bottom_0">
                                <a href="#" class="color_dark d_block bt_link">Elementum vel</a>
                                <!--rating-->
                                <ul class="horizontal_list clearfix d_inline_b rating_list type_2 tr_all_hover m_bottom_10">
                                    <li class="active">
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                    <li class="active">
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                    <li class="active">
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                    <li class="active">
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                </ul>
                                <p class="scheme_color">$57.00</p>
                            </div>
                            <hr class="m_bottom_15">
                            <div class="clearfix m_bottom_5">
                                <img src="{{asset('/template_resource/images/bestsellers_img_3.jpg')}}" alt=""
                                     class="f_left m_right_15 m_sm_bottom_10 f_sm_none f_xs_left m_xs_bottom_0">
                                <a href="#" class="color_dark d_block bt_link">Crsus eleifend elit</a>
                                <!--rating-->
                                <ul class="horizontal_list clearfix d_inline_b rating_list type_2 tr_all_hover m_bottom_10">
                                    <li class="active">
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                    <li class="active">
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                    <li class="active">
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                    <li class="active">
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                </ul>
                                <p class="scheme_color">$24.00</p>
                            </div>
                        </div>
                    </figure>
                    <!--tags-->
                    <figure class="widget shadow r_corners wrapper m_bottom_30">
                        <figcaption>
                            <h3 class="color_light">Tags</h3>
                        </figcaption>
                        <div class="widget_content">
                            <div class="tags_list">
                                <a href="#" class="color_dark d_inline_b v_align_b">accessories,</a>
                                <a href="#" class="color_dark d_inline_b f_size_ex_large v_align_b">bestseller,</a>
                                <a href="#" class="color_dark d_inline_b v_align_b">clothes,</a>
                                <a href="#" class="color_dark d_inline_b f_size_big v_align_b">dresses,</a>
                                <a href="#" class="color_dark d_inline_b v_align_b">fashion,</a>
                                <a href="#" class="color_dark d_inline_b f_size_large v_align_b">men,</a>
                                <a href="#" class="color_dark d_inline_b v_align_b">pants,</a>
                                <a href="#" class="color_dark d_inline_b v_align_b">sale,</a>
                                <a href="#" class="color_dark d_inline_b v_align_b">short,</a>
                                <a href="#" class="color_dark d_inline_b f_size_ex_large v_align_b">skirt,</a>
                                <a href="#" class="color_dark d_inline_b v_align_b">top,</a>
                                <a href="#" class="color_dark d_inline_b f_size_big v_align_b">women</a>
                            </div>
                        </div>
                    </figure>
                    <!--New products-->
                    <figure class="widget shadow r_corners wrapper m_bottom_30">
                        <figcaption>
                            <h3 class="color_light">New Products</h3>
                        </figcaption>
                        <div class="widget_content">
                            <div class="clearfix m_bottom_15">
                                <img src="{{asset('/template_resource/images/new_products_img_1.jpg')}}" alt=""
                                     class="f_left m_right_15 m_sm_bottom_10 f_sm_none f_xs_left m_xs_bottom_0">
                                <a href="#" class="color_dark d_block m_bottom_5 bt_link">Ut tellus dolor dapibus</a>

                                <p class="scheme_color">$61.00</p>
                            </div>
                            <hr class="m_bottom_15">
                            <div class="clearfix m_bottom_15">
                                <img src="{{asset('/template_resource/images/new_products_img_2.jpg')}}" alt=""
                                     class="f_left m_right_15 m_sm_bottom_10 f_sm_none f_xs_left m_xs_bottom_0">
                                <a href="#" class="color_dark d_block m_bottom_5 bt_link">Elementum vel</a>

                                <p class="scheme_color">$57.00</p>
                            </div>
                            <hr class="m_bottom_15">
                            <div class="clearfix m_bottom_5">
                                <img src="{{asset('/template_resource/images/new_products_img_3.jpg')}}" alt=""
                                     class="f_left m_right_15 m_sm_bottom_10 f_sm_none f_xs_left m_xs_bottom_0">
                                <a href="#" class="color_dark d_block m_bottom_5 bt_link">Crsus eleifend elit</a>

                                <p class="scheme_color">$24.00</p>
                            </div>
                        </div>
                    </figure>
                    <!--Specials-->
                    <figure class="widget shadow r_corners wrapper m_bottom_30">
                        <figcaption class="clearfix relative">
                            <h3 class="color_light f_left f_sm_none m_sm_bottom_10 m_xs_bottom_0">Specials</h3>

                            <div class="f_right nav_buttons_wrap_type_2 tf_sm_none f_sm_none clearfix">
                                <button class="button_type_7 bg_cs_hover box_s_none f_size_ex_large color_light t_align_c bg_tr f_left tr_delay_hover r_corners sc_prev">
                                    <i class="fa fa-angle-left"></i></button>
                                <button class="button_type_7 bg_cs_hover box_s_none f_size_ex_large color_light t_align_c bg_tr f_left m_left_5 tr_delay_hover r_corners sc_next">
                                    <i class="fa fa-angle-right"></i></button>
                            </div>
                        </figcaption>
                        <div class="widget_content">
                            <div class="specials_carousel">
                                <!--carousel item-->
                                <div class="specials_item">
                                    <a href="#" class="d_block d_xs_inline_b wrapper m_bottom_20">
                                        <img class="tr_all_long_hover" src="{{asset('/template_resource/images/product_img_6.jpg')}}" alt="">
                                    </a>
                                    <h5 class="m_bottom_10"><a href="#" class="color_dark">Aliquam erat volutpat</a></h5>

                                    <p class="f_size_large m_bottom_15"><s>$79.00</s> <span class="scheme_color">$36.00</span></p>
                                    <button class="button_type_4 mw_sm_0 r_corners color_light bg_scheme_color tr_all_hover m_bottom_5">Add
                                        to Cart
                                    </button>
                                </div>
                                <!--carousel item-->
                                <div class="specials_item">
                                    <a href="#" class="d_block d_xs_inline_b wrapper m_bottom_20">
                                        <img class="tr_all_long_hover" src="{{asset('/template_resource/images/product_img_7.jpg')}}" alt="">
                                    </a>
                                    <h5 class="m_bottom_10"><a href="#" class="color_dark">Integer rutrum ante </a></h5>

                                    <p class="f_size_large m_bottom_15"><s>$79.00</s> <span class="scheme_color">$36.00</span></p>
                                    <button class="button_type_4 mw_sm_0 r_corners color_light bg_scheme_color tr_all_hover m_bottom_5">Add
                                        to Cart
                                    </button>
                                </div>
                                <!--carousel item-->
                                <div class="specials_item">
                                    <a href="#" class="d_block d_xs_inline_b wrapper m_bottom_20">
                                        <img class="tr_all_long_hover" src="{{asset('/template_resource/images/product_img_5.jpg')}}" alt="">
                                    </a>
                                    <h5 class="m_bottom_10"><a href="#" class="color_dark">Aliquam erat volutpat</a></h5>

                                    <p class="f_size_large m_bottom_15"><s>$79.00</s> <span class="scheme_color">$36.00</span></p>
                                    <button class="button_type_4 mw_sm_0 r_corners color_light bg_scheme_color tr_all_hover m_bottom_5">Add
                                        to Cart
                                    </button>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <!--Popular articles-->
                    <figure class="widget shadow r_corners wrapper m_bottom_30">
                        <figcaption>
                            <h3 class="color_light">Popular Articles</h3>
                        </figcaption>
                        <div class="widget_content">
                            <article class="clearfix m_bottom_15">
                                <img src="{{asset('/template_resource/images/article_img_1.jpg')}}" alt=""
                                     class="f_left m_right_15 m_sm_bottom_10 f_sm_none f_xs_left m_xs_bottom_0">
                                <a href="#" class="color_dark d_block bt_link p_vr_0">Aliquam erat volutpat.</a>

                                <p class="f_size_medium">50 comments</p>
                            </article>
                            <hr class="m_bottom_15">
                            <article class="clearfix m_bottom_15">
                                <img src="{{asset('/template_resource/images/article_img_2.jpg')}}" alt=""
                                     class="f_left m_right_15 m_sm_bottom_10 f_sm_none f_xs_left m_xs_bottom_0">
                                <a href="#" class="color_dark d_block p_vr_0 bt_link">Integer rutrum ante </a>

                                <p class="f_size_medium">34 comments</p>
                            </article>
                            <hr class="m_bottom_15">
                            <article class="clearfix m_bottom_5">
                                <img src="{{asset('/template_resource/images/article_img_3.jpg')}}" alt=""
                                     class="f_left m_right_15 m_sm_bottom_10 f_sm_none f_xs_left m_xs_bottom_0">
                                <a href="#" class="color_dark d_block p_vr_0 bt_link">Vestibulum libero nisl, porta vel</a>

                                <p class="f_size_medium">21 comments</p>
                            </article>
                        </div>
                    </figure>
                    <!--Latest articles-->
                    <figure class="widget shadow r_corners wrapper m_bottom_30">
                        <figcaption>
                            <h3 class="color_light">Latest Articles</h3>
                        </figcaption>
                        <div class="widget_content">
                            <article class="clearfix m_bottom_15">
                                <img src="{{asset('/template_resource/images/article_img_4.jpg')}}" alt=""
                                     class="f_left m_right_15 m_sm_bottom_10 f_sm_none f_xs_left m_xs_bottom_0">
                                <a href="#" class="color_dark d_block bt_link p_vr_0">Aliquam erat volutpat.</a>

                                <p class="f_size_medium">25 January, 2013</p>
                            </article>
                            <hr class="m_bottom_15">
                            <article class="clearfix m_bottom_15">
                                <img src="{{asset('/template_resource/images/article_img_5.jpg')}}" alt=""
                                     class="f_left m_right_15 m_sm_bottom_10 f_sm_none f_xs_left m_xs_bottom_0">
                                <a href="#" class="color_dark d_block p_vr_0 bt_link">Integer rutrum ante </a>

                                <p class="f_size_medium">21 January, 2013</p>
                            </article>
                            <hr class="m_bottom_15">
                            <article class="clearfix m_bottom_5">
                                <img src="{{asset('/template_resource/images/article_img_6.jpg')}}" alt=""
                                     class="f_left m_right_15 m_sm_bottom_10 f_sm_none f_xs_left m_xs_bottom_0">
                                <a href="#" class="color_dark d_block p_vr_0 bt_link">Vestibulum libero nisl, porta vel</a>

                                <p class="f_size_medium">18 January, 2013</p>
                            </article>
                        </div>
                    </figure>
                </aside>
            </div>
        </div>
    </div>
    <!--markup footer-->
    @include('web.partial.footer.bottom')
</div>
<!--social widgets-->
@include('web/partial/social_widgets/main')
<!--<ul class="social_widgets d_xs_none">
    facebook
    <li class="relative">
        <button class="sw_button t_align_c facebook"><i class="fa fa-facebook"></i></button>
        <div class="sw_content">
            <h3 class="color_dark m_bottom_20">Join Us on Facebook</h3>
            <iframe src="http://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fenvato&amp;width=235&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=438889712801266"
                    style="border:none; overflow:hidden; width:235px; height:258px;"></iframe>
        </div>
    </li>
    twitter feed
    <li class="relative">
        <button class="sw_button t_align_c twitter"><i class="fa fa-twitter"></i></button>
        <div class="sw_content">
            <h3 class="color_dark m_bottom_20">Latest Tweets</h3>

            <div class="twitterfeed m_bottom_25"></div>
            <a role="button" class="button_type_4 d_inline_b r_corners tr_all_hover color_light tw_color"
               href="https://twitter.com/fanfbmltemplate">Follow on Twitter</a>
        </div>
    </li>
    contact form
    <li class="relative">
        <button class="sw_button t_align_c contact"><i class="fa fa-envelope-o"></i></button>
        <div class="sw_content">
            <h3 class="color_dark m_bottom_20">Contact Us</h3>

            <p class="f_size_medium m_bottom_15">Lorem ipsum dolor sit amet, consectetuer adipis mauris</p>

            <form id="contactform" class="mini">
                <input class="f_size_medium m_bottom_10 r_corners full_width" type="text" name="cf_name"
                       placeholder="Your name">
                <input class="f_size_medium m_bottom_10 r_corners full_width" type="email" name="cf_email"
                       placeholder="Email">
                <textarea class="f_size_medium r_corners full_width m_bottom_20" placeholder="Message"
                          name="cf_message"></textarea>
                <button type="submit" class="button_type_4 r_corners mw_0 tr_all_hover color_dark bg_light_color_2">
                    Send
                </button>
            </form>
        </div>
    </li>
    contact info
    <li class="relative">
        <button class="sw_button t_align_c googlemap"><i class="fa fa-map-marker"></i></button>
        <div class="sw_content">
            <h3 class="color_dark m_bottom_20">Store Location</h3>
            <ul class="c_info_list">
                <li class="m_bottom_10">
                    <div class="clearfix m_bottom_15">
                        <i class="fa fa-map-marker f_left color_dark"></i>

                        <p class="contact_e">8901 Marmora Road,<br> Glasgow, D04 89GR.</p>
                    </div>
                    <iframe class="r_corners full_width" id="gmap_mini"
                            src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=ru&amp;geocode=&amp;q=Manhattan,+New+York,+NY,+United+States&amp;aq=0&amp;oq=monheten&amp;sll=37.0625,-95.677068&amp;sspn=65.430355,129.814453&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=%D0%9C%D0%B0%D0%BD%D1%85%D1%8D%D1%82%D1%82%D0%B5%D0%BD,+%D0%9D%D1%8C%D1%8E-%D0%99%D0%BE%D1%80%D0%BA,+%D0%9D%D1%8C%D1%8E+%D0%99%D0%BE%D1%80%D0%BA,+%D0%9D%D1%8C%D1%8E-%D0%99%D0%BE%D1%80%D0%BA&amp;ll=40.790278,-73.959722&amp;spn=0.015612,0.031693&amp;z=13&amp;output=embed"></iframe>
                </li>
                <li class="m_bottom_10">
                    <div class="clearfix m_bottom_10">
                        <i class="fa fa-phone f_left color_dark"></i>

                        <p class="contact_e">800-559-65-80</p>
                    </div>
                </li>
                <li class="m_bottom_10">
                    <div class="clearfix m_bottom_10">
                        <i class="fa fa-envelope f_left color_dark"></i>
                        <a class="contact_e default_t_color" href="mailto:#">info@companyname.com</a>
                    </div>
                </li>
                <li>
                    <div class="clearfix">
                        <i class="fa fa-clock-o f_left color_dark"></i>

                        <p class="contact_e">Monday - Friday: 08.00-20.00 <br>Saturday: 09.00-15.00<br> Sunday: closed
                        </p>
                    </div>
                </li>
            </ul>
        </div>
    </li>
</ul>-->
<!--login popup-->
<div class="popup_wrap d_none" id="login_popup">
    <section class="popup r_corners shadow">
        <button class="bg_tr color_dark tr_all_hover text_cs_hover close f_size_large"><i class="fa fa-times"></i>
        </button>
        <h3 class="m_bottom_20 color_dark">Log In</h3>

        <form>
            <ul>
                <li class="m_bottom_15">
                    <label for="username" class="m_bottom_5 d_inline_b">Username</label><br>
                    <input type="text" name="" id="username" class="r_corners full_width">
                </li>
                <li class="m_bottom_25">
                    <label for="password" class="m_bottom_5 d_inline_b">Password</label><br>
                    <input type="password" name="" id="password" class="r_corners full_width">
                </li>
                <li class="m_bottom_15">
                    <input type="checkbox" class="d_none" id="checkbox_10"><label for="checkbox_10">Remember me</label>
                </li>
                <li class="clearfix m_bottom_30">
                    <button class="button_type_4 tr_all_hover r_corners f_left bg_scheme_color color_light f_mxs_none m_mxs_bottom_15">
                        Log In
                    </button>
                    <div class="f_right f_size_medium f_mxs_none">
                        <a href="#" class="color_dark">Forgot your password?</a><br>
                        <a href="#" class="color_dark">Forgot your username?</a>
                    </div>
                </li>
            </ul>
        </form>
        <footer class="bg_light_color_1 t_mxs_align_c">
            <h3 class="color_dark d_inline_middle d_mxs_block m_mxs_bottom_15">New Customer?</h3>
            <a href="#" role="button"
               class="tr_all_hover r_corners button_type_4 bg_dark_color bg_cs_hover color_light d_inline_middle m_mxs_left_0">Create
                an Account</a>
        </footer>
    </section>
</div>
<button class="t_align_c r_corners tr_all_hover type_2 animate_ftl" id="go_to_top" ><i class="fa fa-angle-up"></i>
</button>
    <!-- Developer : hidden value-->
    <input type="hidden" id="isCheckoutPage" value = "true"/>
    <!--scripts include-->
    <!--scripts include-->
    @include('web.partial.script.core')
    <script src="{{asset('/developer/js/script/chackout_script.js')}}"></script>

</body>
</html>