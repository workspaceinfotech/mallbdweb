<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="Walletmix Payment Gateway" />
        <meta name="author" content="" />

        <title>Payment Error</title>
        
    </head>
    <body>
        <div id="wrap">
            <div class="container">
               <div class="container" style="padding: 0px;min-height: 200px;">
                    <figure class="widget r_corners  m_bottom_10 cstm_new_block pad-check">
                        <figcaption class="pad-check">
                            <h3 class="color_ligh">Order Success</h3>
                        </figcaption>
                    </figure>
                    <p style="padding: 15px; "><h2 style="color: red;"><?php echo $success; ?></h2></p>
                </div> 
            </div>
        </div>

    </body>
</html>