<!doctype html>
<!--[if IE 9 ]><html class="ie9" lang="en" ng-app="mallBd"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" ng-app="mallBd"><!--<![endif]-->

    @include('web/partial/header/mainStart')
    @include('web/partial/header/cartScript')
    @include('web/partial/header/mainEnds')

    <body id="MainWrap"  ng-controller="CartController" >
        @include('web/partial/loader')
        <!--wide layout-->
        <div class="wide_layout relative">
            <!--[if (lt IE 9) | IE 9]>
            <div style="background:#fff;padding:8px 0 10px;">
                <div class="container" style="width:1170px;">
                    <div class="row wrapper">
                        <div class="clearfix" style="padding:9px 0 0;float:left;width:83%;"><i
                                class="fa fa-exclamation-triangle scheme_color f_left m_right_10"
                                style="font-size:25px;color:#e74c3c;"></i><b style="color:#e74c3c;">Attention! This page may not
                            display correctly.</b> <b>You are using an outdated version of Internet Explorer. For a faster, safer
                            browsing experience.</b></div>
                        <div class="t_align_r" style="float:left;width:16%;"><a
                                href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"
                                class="button_type_4 r_corners bg_scheme_color color_light d_inline_b t_align_c" target="_blank"
                                style="margin-bottom:2px;">Update Now!</a></div>
                    </div>
                </div>
            </div>
            <![endif]-->
            <!--markup header-->
            <header role="banner" class="type_5 fixed-top">

                <!--header top part-->
                @include('web/partial/header/top')

                <!--main menu container-->
                @include('web/partial/new_menu')
            </header>

            <div class="other-free-gap">

            </div>
             @include('web/partial/sideblock/shippingtext')
            <!--breadcrumbs-->
            <section class="breadcrumbs">
                <div class="container">
                    <ul class="horizontal_list clearfix bc_list f_size_medium">
                        <li class="m_right_10 current"><a href="{{url('')}}" class="default_t_color">Home<i
                                    class="fa fa-angle-right d_inline_middle m_left_10"></i></a></li>
                        <li class="m_right_10 current"><a href="javascript:void(0);" class="default_t_color">Checkout</a></li>
                    </ul>
                </div>
            </section>
            
            <div ng-show="!isEmpty(shoppingCart.productCell) || !isEmpty(shoppingCart.mallBdPackageCell)">
                <!--content-->
                <div class="page_content_offset">
                    <div class="container">
                        <h2 class="tt_uppercase color_dark m_bottom_25">Checkout
                            <?php if (!$mobile) { ?>
                <span class="checkout-page-big-error" style="display: none;" id="checkout_page_big_error"></span>
            <?php } ?>
                        </h2>
                        <div class="row clearfix">

                            <section class="col-md-4">
                                @if($appCredential->id ==0)
                                <div class="tabs m_bottom_10" id="checkoutLoginForm" style="margin-bottom: 10px;">
                                    <nav>
                                        <ul class="tabs_nav horizontal_list clearfix">
                                            <li style="width: 100%;"><a href="#tab-1" class="bg_light_color_1 color_dark tr_delay_hover r_corners d_block tog pad-check">Already registered?
                                                    <?php if ($mobile) { ?>
                                                        <span style="text-decoration:underline" onclick="show_login_checkoutmobile();">Login here</span></a>
                                                    <?php } else { ?>
                                                        <span style="text-decoration:underline">Login here</span></a>
                                                    <?php } ?>
                                            </li>
                                        </ul>
                                    </nav>
                                        <section class="tabs_content shadow r_corners formal" style="display: none;" id="login_checkoutmobile">
                                    
                                        <div id="tab-1">
                                            <!--login form-->
                                            <!--<h5 class="fw_medium m_bottom_15">I am Already Registered</h5>-->

                                            <form>
                                                <ul>
                                                    <li class="clearfix m_bottom_15">
                                                        <div class="half_column type_2 f_left">
                                                            <label for="username" class="m_bottom_5 d_inline_b">E-mail</label>
                                                            <input type="text" id="checkoutLogInEmail" name="" class="r_corners full_width m_bottom_5">
                                                            <a href="{{ url('/password/reset') }}" class="color_dark f_size_medium">Forgot your password?</a>
                                                        </div>
                                                        <div class="half_column type_2 f_left">
                                                            <label for="pass" class="m_bottom_5 d_inline_b">Password</label>
                                                            <input type="password" id="checkoutLogInPassword" name="" class="r_corners full_width m_bottom_5">
                                                        </div>
                                                    </li>
                                                    <li class="m_bottom_15">
                                                        <p class="alert-txt help-block" id="checkoutLoginMsg"></p>
                                                    </li>
                                                    <li class="m_bottom_15">
                                                        <input type="checkbox" class="d_none" name="checkbox_4" id="checkbox_4"><label for="checkbox_4">Remember
                                                            me</label>
                                                    </li>
                                                    <li>
                                                        <button class="button_type_4 r_corners bg_scheme_color color_light tr_all_hover" ng-click="doLoginFromCheckout()">Log In</button>
                                                    </li>
                                                </ul>
                                            </form>
                                        </div>
                                    </section>
                                </div>
                                @endif
                                <figure class="widget shadow r_corners wrapper m_bottom_10">
                                    <figcaption class="pad-check">
                                        <h3 class="color_light">Delivery & Personal Information</h3>
                                    </figcaption>
                                    <div class="widget_content shadow-check">
                                        <form>
                                            <ul>
                                                {{-- < li class = "m_bottom_15" > --}}
                                                {{-- < input type = "checkbox" class = "d_none" name = "checkbox_4" id = "checkbox_5" > < label for = "checkbox_5" > Create an account and enjoy benefits of registered customers. < /label>--}}
                                                {{-- < /li>--}}
                                                <li class="clearfix m_bottom_15">
                                                    <label for="element_email" class="d_inline_b m_bottom_5 ">Email</label>
                                                    <input type="text" id="element_email" name="element_email" class="r_corners full_width" ng-model="userInfo.email" ng-disabled="isLogin()">
                                                    <p class="alert-txt"  ng-hide="htmlManipulateObj.email.isHidden" ng-bind="htmlManipulateObj.email.htmlValus" ></p>
                                                </li>
                                                <li class="m_bottom_15">
                                                    <input type="checkbox" class="d_none" name="checkbox_4" id="checkbox_6"><label for="checkbox_6">Sign up for our newsletter</label>
                                                </li>
                                            </ul>
                                            <hr class="m_bottom_15">
                                            <h4 class="m_bottom_10">Delivery Address</h4>

                                            <ul>

                                                <li class="m_bottom_15">
                                                    <label for="element_firstname" class="d_inline_b m_bottom_5">First name</label>
                                                    <input type="text" id="element_firstname" name="first_name" class="r_corners full_width" ng-model="userInfo.firstName" >
                                                    <p class="alert-txt help-block" ng-hide="htmlManipulateObj.firstname.isHidden" ng-bind="htmlManipulateObj.firstname.htmlValus"></p>
                                                </li>

                                                <li class="m_bottom_15">
                                                    <label for="element_lastname" class="d_inline_b m_bottom_5 ">Last Name</label>
                                                    <input type="text" id="element_lastname" name="last_name" class="r_corners full_width" ng-model="userInfo.lastName" >
                                                    <p class="alert-txt"  ng-hide="htmlManipulateObj.lastname.isHidden" ng-bind="htmlManipulateObj.lastname.htmlValus"></p>
                                                </li>
                                                <li class="m_bottom_15">
                                                    <label for="element_phone" class="d_inline_b m_bottom_5">Phone</label>
                                                    <input type="text" id="element_phone" name="phone" class="r_corners full_width" ng-model="userInfo.phone"  >
                                                    <p class="alert-txt" ng-hide="htmlManipulateObj.phone.isHidden" ng-bind="htmlManipulateObj.phone.htmlValus"></p>
                                                </li>
                                                <li class="m_bottom_15">
                                                    <label for="element_address" class="d_inline_b m_bottom_5 ">Address</label>
                                                    <input type="text" id="element_address" name="address" class="r_corners full_width" ng-model="userInfo.userDetails.address.address" >
                                                    <p class="alert-txt"  ng-hide="htmlManipulateObj.address.isHidden" ng-bind="htmlManipulateObj.address.htmlValus"></p>
                                                </li>
                                                <li class="m_bottom_15 checkout_city">
                                                    <label for="element_city" class="d_inline_b m_bottom_5 ">City</label>
                                                    <select id="element_city" name="city" class="r_corners full_width" ng-model="userInfo.userDetails.address.city">
                                                            <?php foreach($districts as $district){  ?>
                                                             <option value="<?php echo $district['name']; ?>"><?php echo $district['name']; ?></option>
                                                            <?php } ?>
                                                    </select>
                                                    <!--<input type="text" id="element_city" name="city" class="r_corners full_width" ng-model="userInfo.userDetails.address.city" >-->
                                                    <p class="alert-txt"  ng-hide="htmlManipulateObj.city.isHidden" ng-bind="htmlManipulateObj.city.htmlValus"></p>
                                                </li>
                                            </ul>
                                            <p class="checkbox is_customer_param" id="invoice_address_checkbox">
                                                    <div class="checker" id="uniform-invoice_address">
                                                        <span class="checked">
                                                            <input onclick="invoice_shipping(this)" name="invoice_address" class="d_none" id="invoice_address" type="checkbox">
                                                            <label for="invoice_address"><b>Please use another address for gift or invoice</b></label>
                                                        </span>                                             
                                                    </div>
                                             </p>
                                             <ul id="invoice_address_div" style="display:block;">
                                                <li class="m_bottom_15">
                                                    <label for="element_firstname" class="d_inline_b m_bottom_5">First name</label>
                                                    <input type="text" id="element_shipping_firstname" name="shipping_firstname" class="r_corners full_width" ng-model="userInfo.userDetails.address.shipping_firstname" >
                                                    <p class="alert-txt help-block" ng-hide="htmlManipulateObj.shipping_firstname.isHidden" ng-bind="htmlManipulateObj.shipping_firstname.htmlValus"></p>
                                                </li>

                                                <li class="m_bottom_15">
                                                    <label for="element_lastname" class="d_inline_b m_bottom_5 ">Last Name</label>
                                                    <input type="text" id="element_shipping_lastname" name="shipping_last_name" class="r_corners full_width" ng-model="userInfo.userDetails.address.shipping_lastname" >
                                                    <p class="alert-txt"  ng-hide="htmlManipulateObj.shipping_lastname.isHidden" ng-bind="htmlManipulateObj.shipping_lastname.htmlValus"></p>
                                                </li>
                                                <li class="m_bottom_15">
                                                    <label for="element_phone" class="d_inline_b m_bottom_5">Phone</label>
                                                    <input type="text" id="element_shipping_phone" name="shipping_phone" class="r_corners full_width" ng-model="userInfo.userDetails.address.shipping_phone"  >
                                                    <p class="alert-txt" ng-hide="htmlManipulateObj.shipping_phone.isHidden" ng-bind="htmlManipulateObj.shipping_phone.htmlValus"></p>
                                                </li>
                                                <li class="m_bottom_15">
                                                    <label for="element_address" class="d_inline_b m_bottom_5 ">Address</label>
                                                    <input type="text" id="element_shipping_address" name="shipping_address" class="r_corners full_width" ng-model="userInfo.userDetails.address.shipping_address" >
                                                    <p class="alert-txt"  ng-hide="htmlManipulateObj.shipping_address.isHidden" ng-bind="htmlManipulateObj.shipping_address.htmlValus"></p>
                                                </li>
                                                <li class="m_bottom_15">
                                                    <label for="element_city" class="d_inline_b m_bottom_5 ">City</label>
                                                    <select id="element_shipping_city" name="shipping_city" class="r_corners full_width" ng-model="userInfo.userDetails.address.city">
                                                            <?php foreach($districts as $district){  ?>
                                                             <option value="<?php echo $district['name']; ?>"><?php echo $district['name']; ?></option>
                                                            <?php } ?>
                                                    </select>
                                                    <!--<input type="text" id="element_city" name="city" class="r_corners full_width" ng-model="userInfo.userDetails.address.city" >-->
                                                    <p class="alert-txt"  ng-hide="htmlManipulateObj.shipping_city.isHidden" ng-bind="htmlManipulateObj.shipping_city.htmlValus"></p>
                                                </li>
                                            </ul>
                                        </form>
                                        
                                    </div>
                                </figure>
                            </section>
                            <section class="col-md-8">
                                <figure class="widget r_corners m_bottom_10 cstm_new_block pad-check" style="padding-top:0px !important;">
                                    <figcaption class="pad-check">
                                        <h3 class="color_light text-capitalize">Choose Your Delivery Method</h3>
                                        <p class="alert-txt-for-methods"  ng-hide="htmlManipulateObj.delivery_method_id.isHidden" ng-bind="htmlManipulateObj.delivery_method_id.htmlValus"></p>
                                    </figcaption>
                                    <div class="widget_content shadow-check">
                                        <div id="element_delivery_method_id" class="bg_light_color_3 r_corners  angularFocus"  scrolltopposition="870" >

                                            <div ng-repeat="tempDeliveryMethod in deliveryMethods" class="clearfix" style="margin-bottom: 20px;">
                                                <figure class="block_select clearfix relative deliveryMethodRadioForAngular" ng-click="selectDeliveryMethods($index)">

                                                    <!--<div ng-if="tempDeliveryMethod.id < 2">-->
                                                    <input type="radio" name="delivery_method" class="d_none" value="(( $index ))"  ng-checked="true">
                                                    <!--</div>-->
                                                    <img src="{{$imageUrl.'/delivery/'}}@{{ tempDeliveryMethod.icon}}" alt="" class="f_left m_right_20 f_mxs_none m_mxs_bottom_10" />
                                                    <figcaption>
                                                        <div class="d_table_cell d_sm_block p_sm_right_0 p_right_45 m_mxs_bottom_5 d_method" >
                                                            <h5 class="color_dark fw_medium  m_sm_bottom_5" ng-bind="tempDeliveryMethod.title"></h5>                   
                                                        </div>
                                                        <div class="d_table_cell d_sm_block discount text-right">
                                                            <h5 class="color_dark fw_medium  m_sm_bottom_0"><span ng-bind-html="currencySymbol"></span> @{{tempDeliveryMethod.deliveryPrice}}</h5>
                                                        </div>
                                                    </figcaption>
                                                </figure>
                                                <!--                                            <div class="pull-right" style="width: 300px;padding: 10px;" ng-show="deliveryMethod.id == 1"  ng-if="$index == 0" >
                                                                                                <h5 class="color_dark fw_medium  m_sm_bottom_0">Area Information</h5>
                                                                                                <p class="alert-txt"  ng-hide="htmlManipulateObj.zone_id.isHidden" ng-bind="htmlManipulateObj.zone_id.htmlValus"></p>
                                                                                                <select id="element_zone_id" ng-model="zone.id" class="form-control" style="width: 100%;border:1px solid #cecece;" >
                                                                                                    <option value="0">Select area</option>
                                                                                                    <option ng-selected="zone.id == option.id" ng-repeat="option in zones"  value="@{{option.id}}">@{{option.area}}</option>
                                                                                                </select>
                                                                                            </div>-->
                                            </div>

                                        </div>
                                    </div>
                                </figure>
                                <figure class="widget r_corners  m_bottom_10 cstm_new_block pad-check">
                                    <figcaption class="pad-check">
                                        <h3 class="color_ligh text-capitalize">Choose Your Payment Method</h3>
                                        <p class="alert-txt-for-methods"  ng-hide="htmlManipulateObj.payment_method_id.isHidden" ng-bind="htmlManipulateObj.payment_method_id.htmlValus"></p>
                                    </figcaption>
                                    <div class="widget_content shadow-check" style="padding: 10px 20px 5px;">

                                        <div id="element_payment_method_id" class=" bg_light_color_3 r_corners m_bottom_0 angularFocus" scrolltopposition="1450">

                                            <div ng-repeat="temPaymentMethod in paymentMethods">
                                                <figure class="block_select clearfix relative m_bottom_5 paymentMethodRadioForAngular" ng-click="selectPaymentMethods($index)" >

                                                    <input type="radio" name="radio_2" class="d_none" >

                                                    {{--onerror = "this.src='{{asset('/template_resource/images/payment_logo.jpg')}}'"--}}
                                                    <div class="pay-img"><img src="@{{imageUrl + 'payment/' + temPaymentMethod.icon}}"  alt="" class="f_left m_right_20 f_mxs_none m_mxs_bottom_10"></div>
                                                    <figcaption class="d_table d_sm_block">
                                                        <div class="d_table_cell d_sm_block p_sm_right_0 p_right_45 m_mxs_bottom_5">
                                                            <h5 class="color_dark fw_medium m_bottom_15 m_sm_bottom_5" ng-bind="temPaymentMethod.methodTitle"></h5>
                                                        </div>

                                                    </figcaption>
                                                </figure>
                                                <hr class="m_bottom_10" ng-if="$index < paymentMethods.length - 1">
                                            </div>

                                        </div>
                                    </div>




                                </figure>

                                <div style="padding: 10px;" class="clearfix">
                                    <p class="alert-txt product_qutantity_msg" style="font-size: 14px;    margin-bottom: 4px;"></p>
                                    <table class="table_type_4 responsive_table full_width r_corners wraper shadow t_align_l t_xs_align_c table-bordered m_bottom_30 table-striped table-cart table_form_mb">
                                        <!--<thead style="background:#323a45;color: #fff;">-->
                                        <thead style="background:#898989;color: #fff;">
                                            <tr class="f_size_large">
                                                <!--titles for td-->

                                                <th style="padding: 8px 20px;">Product Image &amp; Name</th>
                                                <th style="padding: 8px 20px;">Type</th>
                                                <th style="padding: 8px 20px;">Price</th>
                                                <th style="padding: 8px 20px;">Quantity</th>
                                                <th style="padding: 8px 20px;">Subtotal</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="productCell in shoppingCart.productCell track by $index" class="t_tr_mb">

                                                <!--Product name and image-->
                                                <td data-title="Product Image &amp; name" class="t_md_align_c t_n_mb">
                                                    <div ng-show="isEmpty(getPictureUrl(productCell.product.pictures[0].name))">
                                                        <img class="m_md_bottom_5 d_xs_block d_xs_centered pull-left" style="width: 62px;height: 62px" src="http://placehold.it/90x90?text=Image not found"  alt="">
                                                    </div>
                                                    <div ng-show="!isEmpty(getPictureUrl(productCell.product.pictures[0].name))">
                                                        <img width="50" src="@{{getPictureUrl(productCell.product.pictures[0].name)}}" alt="" class="m_md_bottom_5 d_xs_block d_xs_centered pull-left">
                                                    </div>

                                                    <a href="{{url('/product/')}}/@{{ productCell.product.url }}/@{{ productCell.product.code }}" class="d_inline_b m_left_5 color_dark pull-left"  ng-bind="productCell.product.title" > </a>
                                                </td>
                                                <!--product key-->
                                                <td data-title="SKU" class="t_ty_mb"> Product</td>
                                                <!--product price-->
                                                <td data-title="Price">
                                        <s ng-if="productCell.product.discountActiveFlag" class="v_align_b f_size_ex_large" ng-bind="getPriceIndividual($index)"></s>

                                        <p ng-if="productCell.product.discountActiveFlag" class="f_size_large color_dark" ng-bind="getPriceAfterDiscount($index)"> </p>
                                        <p ng-if="!productCell.product.discountActiveFlag" class="f_size_large color_dark" ng-bind="getPriceIndividual($index)"> </p>
                                        </td>
                                        <!--quanity-->
                                        <td data-title="Quantity" class="t_q_mb">
                                            <div class="clearfix quantity r_corners d_inline_middle f_size_medium color_dark m_bottom_10 pull-left">
                                                <button class="bg_tr d_block f_left" data-direction="down"  ng-click="decreaseProductInProductCell($index)" >-</button>
                                                <input type="text" name="" readonly ng-value="getProductQuantity($index)" class="f_left">
                                                <button class="bg_tr d_block f_left" data-direction="up" ng-click="increaseProductInProductCell($index)" >+</button>
                                            </div>
                                            <div>
                                                {{-- < a href = "#" class = "color_dark" > < i class = "fa fa-check f_size_medium m_right_5" > < /i>Update</a > < br > --}}
                                                <a href="javascript:void(0)" ng-click="removeProductCell($index)" class="color_dark pull-left" style="margin: 8px 0px 0px 5px;"><i class="fa fa-times f_size_medium m_right_5"></i></a><br>
                                            </div>
                                        </td>
                                        <!--subtotal-->
                                        <td data-title="Subtotal" class="t_st_mb">
                                            <p class="f_size_large fw_medium scheme_color" ng-bind="getPrice($index)"></p>
                                        </td>
                                        </tr>

                                        <tr ng-repeat="mallBdPackageCell in shoppingCart.mallBdPackageCell track by $index" class="t_tr_mb">

                                            <!--Product name and image-->
                                            <td data-title="Product Image &amp; name" class="t_md_align_c t_n_mb">
                                                <div ng-show="isEmpty(getPackagesPictureUrl(mallBdPackageCell.mallBdPackage.image))">
                                                    <img class="m_md_bottom_5 d_xs_block d_xs_centered pull-left" style="width: 62px;height: 62px" src="http://placehold.it/90x90?text=Image not found"  alt="">
                                                </div>
                                                <div ng-show="!isEmpty(getPackagesPictureUrl(mallBdPackageCell.mallBdPackage.image))">
                                                    <img width="50" src="@{{getPackagesPictureUrl(mallBdPackageCell.mallBdPackage.image)}}" alt="" class="m_md_bottom_5 d_xs_block d_xs_centered pull-left">
                                                </div>
                                                <a href="{{url('/packages')}}/@{{ mallBdPackageCell.mallBdPackage.packageTitle}}/@{{ mallBdPackageCell.mallBdPackage.id}}" class="d_inline_b m_left_5 color_dark pull-left" ng-bind="mallBdPackageCell.mallBdPackage.packageTitle" ></a>
                                            </td>
                                            <td data-title="SKU" class="t_ty_mb">Package</td>
                                            <!--product price-->
                                            <td data-title="Price" class="t_p_mb">
                                                {{-- < s ng - if = "productCell.product.previousPrice>0" class = "v_align_b f_size_ex_large" ng - bind = "'&#2547'+@{{productCell.product.previousPrice}}"></s>--}}

                                                <p class="f_size_large color_dark" ng-bind="getPackageItemPriceIndividual($index)" > </p>
                                            </td>
                                            <!--quanity-->
                                            <td data-title="Quantity" class="t_q_mb">
                                                <div class="clearfix quantity r_corners d_inline_middle f_size_medium color_dark m_bottom_10 pull-left">
                                                    <button class="bg_tr d_block f_left" data-direction="down"  ng-click="decreasePackageInPackageCell($index)" >-</button>
                                                    <input type="text" name="" readonly ng-value="getPackageQuantity($index)" class="f_left">
                                                    <button class="bg_tr d_block f_left" data-direction="up" ng-click="increasePackageInPackageCell($index)" >+</button>
                                                </div>
                                                <div>
                                                    {{-- < a href = "#" class = "color_dark" > < i class = "fa fa-check f_size_medium m_right_5" > < /i>Update</a > < br > --}}
                                                    <a href="javascript:void(0)" ng-click="removePackageCell($index)" class="color_dark pull-left" style="margin: 8px 0px 0px 5px;"><i class="fa fa-times f_size_medium m_right_5"></i></a><br>
                                                </div>
                                            </td>
                                            <!--subtotal-->
                                            <td data-title="Subtotal" class="t_st_mb">
                                                <p class="f_size_large fw_medium scheme_color" ng-bind="getPackageItemPrice($index)"></p>
                                            </td>
                                        </tr>
                                        <!--prices-->
                                        <tr ng-repeat="voucherarr in voucherarray track by $index" class="t_other_txt_mb">
                                            <td colspan="3" style="color: #b41f23;font-weight: bold; font-size: 15px;" >
                                                <p class="fw_medium f_size_large">@{{voucherarr.voucher_code}}</p>
                                            </td>
                                            <td colspan="1">
                                                <p class="fw_medium f_size_large t_align_r t_xs_align_c">&nbsp;@{{(getSubTotalAmount()*voucherarr.discount)/100}}
                                            </td>
                                            <td colspan="1">
                                                <p class="fw_medium f_size_large color_dark fix-cross-vcode"><a style="margin: 8px 0px 0px 5px;"  ng-click="removeVoucherarray($index)" class="color_dark pull-left"><i class="fa fa-times f_size_large m_right_5" ></i></a>
                                            </td>
                                        </tr>
                                        <tr class="t_other_txt_mb">
                                            <td colspan="4">
                                                <p class="fw_medium f_size_large t_align_r t_xs_align_c">Subtotal:</p>
                                            </td>
                                            <td colspan="1">
                                                <p class="fw_medium f_size_large color_dark" style="text-align: right;"><span ng-bind-html="currencySymbol"></span>&nbsp;<span ng-bind="getSubTotalAmount()"></span></p>
                                            </td>
                                        </tr>
                                        <tr class="t_other_txt_mb">
                                            <td colspan="4">
                                                <p class="fw_medium f_size_large t_align_r t_xs_align_c">Products Discount:</p>
                                            </td>
                                            <td colspan="1">
                                                <p class="fw_medium f_size_large color_dark" style="text-align: right;"><span ng-bind-html="currencySymbol"></span>&nbsp;<span ng-bind="getTotalDiscountAmount()"></span></p>
                                            </td>
                                        </tr>

                                        <tr id="voucher_tab" class="t_other_txt_mb">
                                            <td colspan="3">
                                                <p class="fw_medium f_size_large">
                                                    Voucher Code:&nbsp; <input type="text" name="voucher_code" style="height:30px;padding:3px 10px;" ng-model="userInfo.voucher_code" id="voucher_code">
                                                    <input type="button" value="ADD" style="background: #b50709; height: 30px;padding: 3px 10px; color: #fff; border: 0px;" ng-click="submitVoucher()">
                                                </p>
                                                <p class="alert-txt" id="voucher_error"></p>
                                            </td>
                                            <td colspan="1">
                                                <p class="fw_medium f_size_large t_align_r t_xs_align_c">Voucher Discount:</p>
                                            </td>
                                            <td colspan="1">
                                                <p class="fw_medium f_size_large color_dark" style="text-align: right;"><span ng-bind-html="currencySymbol"></span>&nbsp;<span id="voucher_discount_amount" ng-bind="getvoucherDiscountAmount()"></span></p>
                                            </td>
                                        </tr>


                                        <tr class="t_other_txt_mb">
                                            <td colspan="4">
                                                <p class="fw_medium f_size_large t_align_r t_xs_align_c">Customer Discount:</p>
                                            </td>
                                            <td colspan="1">
                                                <p class="fw_medium f_size_large color_dark" style="text-align: right;"><span ng-bind-html="currencySymbol"></span>&nbsp;<span ng-bind="getCustomerPurchaseDiscountCalculation()"></span></p>
                                            </td>
                                        </tr>
                                        <tr class="t_other_txt_mb">
                                            <td colspan="4">
                                                <p class="fw_medium f_size_large t_align_r t_xs_align_c">Shipment Fee:</p>
                                            </td>
                                            <td colspan="1">
                                                <p class="fw_medium f_size_large color_dark" style="text-align: right;"><span ng-bind-html="currencySymbol"></span>&nbsp;<span ng-bind="getDeliveryCharge()"></span></p>
                                            </td>
                                        </tr>
                                        {{-- < tr > --}}
                                        {{-- < td colspan = "4" > --}}
                                        {{--<p class="fw_medium f_size_large t_align_r t_xs_align_c">Tax Total:</p>--}}
                                        {{-- < /td>--}}
                                        {{-- < td colspan = "1" > --}}
                                        {{-- < p class = "fw_medium f_size_large color_dark" > < span ng - bind - html = "currencySymbol" > < /span>&nbsp;<span>0</span > < /p>--}}
                                        {{-- < /td>--}}
                                        {{-- < /tr>--}}
                                        <!--total-->
                                        <tr class="t_other_txt_mb">
                                            <td colspan="4" class="v_align_m d_ib_offset_large t_xs_align_l">
                                                <!--coupon-->
                                                {{-- < form class = "d_ib_offset_0 d_inline_middle half_column d_xs_block w_xs_full m_xs_bottom_5" > --}}
                                                {{-- < input type = "text" placeholder = "Enter your coupon code" name = "" class = "r_corners f_size_medium" > --}}
                                                {{-- < button class = "button_type_4 r_corners bg_light_color_2 m_left_5 mw_0 tr_all_hover color_dark" > Save--}}
                                                {{-- < /button>--}}
                                                {{-- < /form>--}}
                                                <p class="fw_medium f_size_large t_align_r t_xs_align_c scheme_color">
                                                    Total:</p>
                                            </td>
                                            <td colspan="1" class="v_align_m">
                                                <p class="fw_medium f_size_large scheme_color m_xs_bottom_10"><span ng-bind-html="currencySymbol"  ></span>&nbsp;<span ng-bind="getTotalAmount()"  ></span></p>
                                            </td>
                                        </tr>
        <!--                                <tr>
                                            <td colspan="3">
                                                <input type="checkbox" name="checkbox_8" id="checkbox_8"  ng-model="agreeTerms" class="d_none"  ><label for="checkbox_8">I agree to
                                                    the Terms of Service (Terms of service)</label>
                                            </td>
                                        </tr>-->
<!--                                        <tr>
                                            <td colspan="3">
                                                <button class="button_type_6 bg_scheme_color f_size_large r_corners tr_all_hover color_light m_bottom_20" ng-click="submitOrder()" >
                                                    I confirm my order
                                                </button>
                                                <p class="alert-text" ng-show="success.status" ng-bind="success.msg"></p>
                                                <p class="alert-text" ng-show="!processing.done" ng-bind="processing.msg"></p>
                                                <p class="alert-text" ng-show="responseError.status" ng-bind="responseError.msg"></p>

                                            </td>

                                        </tr>-->
                                        </tbody>
                                    </table>
                                    <div class="container text-center" style="margin-bottom: -10px;">
                                        <p class="alert-txt" style="font-size: 14px;" ng-show="success.status" ng-bind="success.msg"></p>
                                        <p class="alert-txt" style="font-size: 14px;" ng-show="!processing.done" ng-bind="processing.msg"></p>
                                        <p class="alert-txt" style="font-size: 14px;" ng-show="responseError.status" ng-bind="responseError.msg"></p>
                                        <button  class="button_type_6 bg_scheme_color f_size_large r_corners tr_all_hover color_light m_bottom_20 checkout_btn_mb" style="width:300px; margin-top: 2px;" ng-click="submitOrder()" >
                                            I confirm my order
                                        </button>

                                    </div>
                                </div>
                        </div>



                        </section>

                    </div>
                </div>
            </div>
            <div ng-show="isEmpty(shoppingCart.productCell) && isEmpty(shoppingCart.mallBdPackageCell)">
                <div class="container" style="padding: 0px;min-height: 200px;">
                    <figure class="widget r_corners  m_bottom_10 cstm_new_block pad-check">
                        <figcaption class="pad-check">
                            <h3 class="color_ligh">Your shopping cart</h3>
                        </figcaption>
                    </figure>
                    <p style="padding: 15px;">Your shopping cart is empty.</p>
                </div>
            </div>
            <!--markup footer-->
            @include('web/partial/footer/newbottom')
        </div>
        <!--social widgets-->
        @include('web/partial/social_widgets/main')

        <!--login popup-->
        <div class="popup_wrap d_none" id="login_popup">
            <section class="popup r_corners shadow">
                <button class="bg_tr color_dark tr_all_hover text_cs_hover close f_size_large"><i class="fa fa-times"></i>
                </button>
                <h3 class="m_bottom_20 color_dark">Log In</h3>

                <form>
                    <ul>
                        <li class="m_bottom_15">
                            <label for="username" class="m_bottom_5 d_inline_b">Username</label><br>
                            <input type="text" name="" id="username" class="r_corners full_width">
                        </li>
                        <li class="m_bottom_25">
                            <label for="password" class="m_bottom_5 d_inline_b">Password</label><br>
                            <input type="password" name="" id="password" class="r_corners full_width">
                        </li>
                        <li class="m_bottom_15">
                            <input type="checkbox" class="d_none" id="checkbox_10"><label for="checkbox_10">Remember me</label>
                        </li>
                        <li class="clearfix m_bottom_30">
                            <button class="button_type_4 tr_all_hover r_corners f_left bg_scheme_color color_light f_mxs_none m_mxs_bottom_15">
                                Log In
                            </button>
                            <div class="f_right f_size_medium f_mxs_none">
                                <a href="#" class="color_dark">Forgot your password?</a><br>
                                <a href="#" class="color_dark">Forgot your username?</a>
                            </div>
                        </li>
                    </ul>
                </form>
                <footer class="bg_light_color_1 t_mxs_align_c">
                    <h3 class="color_dark d_inline_middle d_mxs_block m_mxs_bottom_15">New Customer?</h3>
                    <a href="#" role="button"
                       class="tr_all_hover r_corners button_type_4 bg_dark_color bg_cs_hover color_light d_inline_middle m_mxs_left_0">Create
                        an Account</a>
                </footer>
            </section>
        </div>
        
        <style>
            .table_type_4 td{
                padding: 7px;
            }
            .table_type_4 td[colspan], .table_type_4 td[colspan] + td {
                padding-top: 5px;
                padding-bottom: 5px;
            }
        </style>
        <script>
            function invoice_shipping(t){
                if(t.checked){
                    $('#invoice_address_div').css('display','block');
                }
                else{
                    $('#invoice_address_div').css('display','none');
                }
            }
            $(document).ready(function() {
                $(".tog").click(function() {
                    $(".formal").slideToggle();
                });
            });
        </script>
        <button class="t_align_c r_corners tr_all_hover type_2 animate_ftl" id="go_to_top" ><i class="fa fa-angle-up"></i>
        </button>
        <!-- Developer : hidden value-->
        <input type="hidden" id="isCheckoutPage" value = "true"/>
        <!--scripts include-->
        <!--scripts include-->
        @include('web/partial/script/core')
        <script src="{{asset('/developer/js/script/chackout_script.js')}}"></script>
        <link href="{{asset('/developer/select2/select2.min.css')}}"  rel="stylesheet" type="text/css" media="all" >
        <script src="{{asset('/developer/select2/select2.min.js')}}"></script>
        <!--<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />-->
<!--        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>-->
        <script type="text/javascript">
            function show_login_checkoutmobile(){
                if($('#login_checkoutmobile').css("display") == "block"){
                    $('#login_checkoutmobile').css("display","none");
                }
                else{
                     $('#login_checkoutmobile').css("display","block");
                }
            }
        $(document).ready(function() {
          $("#element_city").select2();
          $("#element_shipping_city").select2();
          $('#invoice_address_div').css("display","none");
        });
        </script>
    </body>
</html>