<!doctype html>
<!--[if IE 9 ]><html class="ie9" lang="en" ng-app="mallBd"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" ng-app="mallBd"><!--<![endif]-->

    @include('web/partial/header/mainStart')
    @include('web/partial/header/cartScript')
    @include('web/partial/header/mainEnds')

    <body id="MainWrap"  ng-controller="CartController" >
        <!--wide layout-->
        <div class="wide_layout relative">
            <!--[if (lt IE 9) | IE 9]>
            <div style="background:#fff;padding:8px 0 10px;">
                <div class="container" style="width:1170px;">
                    <div class="row wrapper">
                        <div class="clearfix" style="padding:9px 0 0;float:left;width:83%;"><i
                                class="fa fa-exclamation-triangle scheme_color f_left m_right_10"
                                style="font-size:25px;color:#e74c3c;"></i><b style="color:#e74c3c;">Attention! This page may not
                            display correctly.</b> <b>You are using an outdated version of Internet Explorer. For a faster, safer
                            browsing experience.</b></div>
                        <div class="t_align_r" style="float:left;width:16%;"><a
                                href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"
                                class="button_type_4 r_corners bg_scheme_color color_light d_inline_b t_align_c" target="_blank"
                                style="margin-bottom:2px;">Update Now!</a></div>
                    </div>
                </div>
            </div>
            <![endif]-->
            <!--markup header-->
            <header role="banner" class="type_5 fixed-top">

                <!--header top part-->
                @include('web/partial/header/top')

                <!--main menu container-->
                @include('web/partial/new_menu')
            </header>

            <div class="other-free-gap">

            </div>
            <!--breadcrumbs-->
            <section class="breadcrumbs">
                <div class="container">
                    <ul class="horizontal_list clearfix bc_list f_size_medium">
                        <li class="m_right_10 current"><a href="{{url('')}}" class="default_t_color">Home<i
                                    class="fa fa-angle-right d_inline_middle m_left_10"></i></a></li>
                        <li class="m_right_10 current"><a href="javascript:void(0);" class="default_t_color">Checkout<i
                                    class="fa fa-angle-right d_inline_middle m_left_10"></i></a></li>
                    </ul>
                </div>
            </section>
            <div ng-show="isEmpty(shoppingCart.productCell) && isEmpty(shoppingCart.mallBdPackageCell)">
                <div class="container" style="padding: 0px;min-height: 200px;">
                    <figure class="widget r_corners  m_bottom_10 cstm_new_block pad-check">
                        <figcaption class="pad-check">
                            <h3 class="color_ligh">Order Placed</h3>
                        </figcaption>
                    </figure>
                    <p style="padding: 15px;"><h2>
                        Thank you for your order. Invoice number for your order is {{$orderData->invoice_no}}.<br>
                        The total cost of your order is <?php echo $currency->HTMLCode." ";?><?php echo number_format((float)$orderData->order_total - $orderData->voucher_discount - $orderData->discount_total - $orderData->employee_discount - $orderData->special_discount - $orderData->onpurchase_discount + $orderData->shipping_cost, 2, '.', ''); ?>. 
                        We will contact you soon.
                    </h2></p>
                </div>
            </div>
            
                        </section>

                    </div>
                </div>
            </div>
            <!--markup footer-->
            @include('web/partial/footer/newbottom')
        </div>
        <!--social widgets-->
        @include('web/partial/social_widgets/main')
<!--        <ul class="social_widgets d_xs_none">
            facebook
            <li class="relative">
                <button class="sw_button t_align_c facebook"><i class="fa fa-facebook"></i></button>
                <div class="sw_content">
                    <h3 class="color_dark m_bottom_20">Join Us on Facebook</h3>
                    <iframe src="http://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fenvato&amp;width=235&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=438889712801266"
                            style="border:none; overflow:hidden; width:235px; height:258px;"></iframe>
                </div>
            </li>
            twitter feed
            <li class="relative">
                <button class="sw_button t_align_c twitter"><i class="fa fa-twitter"></i></button>
                <div class="sw_content">
                    <h3 class="color_dark m_bottom_20">Latest Tweets</h3>

                    <div class="twitterfeed m_bottom_25"></div>
                    <a role="button" class="button_type_4 d_inline_b r_corners tr_all_hover color_light tw_color"
                       href="https://twitter.com/fanfbmltemplate">Follow on Twitter</a>
                </div>
            </li>
            contact form
            <li class="relative">
                <button class="sw_button t_align_c contact"><i class="fa fa-envelope-o"></i></button>
                <div class="sw_content">
                    <h3 class="color_dark m_bottom_20">Contact Us</h3>

                    <p class="f_size_medium m_bottom_15">Lorem ipsum dolor sit amet, consectetuer adipis mauris</p>

                    <form id="contactform" class="mini">
                        <input class="f_size_medium m_bottom_10 r_corners full_width" type="text" name="cf_name"
                               placeholder="Your name">
                        <input class="f_size_medium m_bottom_10 r_corners full_width" type="email" name="cf_email"
                               placeholder="Email">
                        <textarea class="f_size_medium r_corners full_width m_bottom_20" placeholder="Message"
                                  name="cf_message"></textarea>
                        <button type="submit" class="button_type_4 r_corners mw_0 tr_all_hover color_dark bg_light_color_2">
                            Send
                        </button>
                    </form>
                </div>
            </li>
            contact info
            <li class="relative">
                <button class="sw_button t_align_c googlemap"><i class="fa fa-map-marker"></i></button>
                <div class="sw_content">
                    <h3 class="color_dark m_bottom_20">Store Location</h3>
                    <ul class="c_info_list">
                        <li class="m_bottom_10">
                            <div class="clearfix m_bottom_15">
                                <i class="fa fa-map-marker f_left color_dark"></i>

                                <p class="contact_e">8901 Marmora Road,<br> Glasgow, D04 89GR.</p>
                            </div>
                            <iframe class="r_corners full_width" id="gmap_mini"
                                    src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=ru&amp;geocode=&amp;q=Manhattan,+New+York,+NY,+United+States&amp;aq=0&amp;oq=monheten&amp;sll=37.0625,-95.677068&amp;sspn=65.430355,129.814453&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=%D0%9C%D0%B0%D0%BD%D1%85%D1%8D%D1%82%D1%82%D0%B5%D0%BD,+%D0%9D%D1%8C%D1%8E-%D0%99%D0%BE%D1%80%D0%BA,+%D0%9D%D1%8C%D1%8E+%D0%99%D0%BE%D1%80%D0%BA,+%D0%9D%D1%8C%D1%8E-%D0%99%D0%BE%D1%80%D0%BA&amp;ll=40.790278,-73.959722&amp;spn=0.015612,0.031693&amp;z=13&amp;output=embed"></iframe>
                        </li>
                        <li class="m_bottom_10">
                            <div class="clearfix m_bottom_10">
                                <i class="fa fa-phone f_left color_dark"></i>

                                <p class="contact_e">800-559-65-80</p>
                            </div>
                        </li>
                        <li class="m_bottom_10">
                            <div class="clearfix m_bottom_10">
                                <i class="fa fa-envelope f_left color_dark"></i>
                                <a class="contact_e default_t_color" href="mailto:#">info@companyname.com</a>
                            </div>
                        </li>
                        <li>
                            <div class="clearfix">
                                <i class="fa fa-clock-o f_left color_dark"></i>

                                <p class="contact_e">Monday - Friday: 08.00-20.00 <br>Saturday: 09.00-15.00<br> Sunday: closed
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>-->
        <!--login popup-->
        <div class="popup_wrap d_none" id="login_popup">
            <section class="popup r_corners shadow">
                <button class="bg_tr color_dark tr_all_hover text_cs_hover close f_size_large"><i class="fa fa-times"></i>
                </button>
                <h3 class="m_bottom_20 color_dark">Log In</h3>

                <form>
                    <ul>
                        <li class="m_bottom_15">
                            <label for="username" class="m_bottom_5 d_inline_b">Username</label><br>
                            <input type="text" name="" id="username" class="r_corners full_width">
                        </li>
                        <li class="m_bottom_25">
                            <label for="password" class="m_bottom_5 d_inline_b">Password</label><br>
                            <input type="password" name="" id="password" class="r_corners full_width">
                        </li>
                        <li class="m_bottom_15">
                            <input type="checkbox" class="d_none" id="checkbox_10"><label for="checkbox_10">Remember me</label>
                        </li>
                        <li class="clearfix m_bottom_30">
                            <button class="button_type_4 tr_all_hover r_corners f_left bg_scheme_color color_light f_mxs_none m_mxs_bottom_15">
                                Log In
                            </button>
                            <div class="f_right f_size_medium f_mxs_none">
                                <a href="#" class="color_dark">Forgot your password?</a><br>
                                <a href="#" class="color_dark">Forgot your username?</a>
                            </div>
                        </li>
                    </ul>
                </form>
                <footer class="bg_light_color_1 t_mxs_align_c">
                    <h3 class="color_dark d_inline_middle d_mxs_block m_mxs_bottom_15">New Customer?</h3>
                    <a href="#" role="button"
                       class="tr_all_hover r_corners button_type_4 bg_dark_color bg_cs_hover color_light d_inline_middle m_mxs_left_0">Create
                        an Account</a>
                </footer>
            </section>
        </div>
        <style>
            .table_type_4 td{
                padding: 7px;
            }
            .table_type_4 td[colspan], .table_type_4 td[colspan] + td {
                padding-top: 5px;
                padding-bottom: 5px;
            }
        </style>
        <script>
            $(document).ready(function() {
                $(".tog").click(function() {
                    $(".formal").slideToggle();
                });
            });
        </script>
        <button class="t_align_c r_corners tr_all_hover type_2 animate_ftl" id="go_to_top" ><i class="fa fa-angle-up"></i>
        </button>
        <!-- Developer : hidden value-->
        <input type="hidden" id="isCheckoutPage" value = "true"/>
        <!--scripts include-->
        <!--scripts include-->
        @include('web/partial/script/core')
        <script src="{{asset('/developer/js/script/chackout_script.js')}}"></script>

    </body>
</html>