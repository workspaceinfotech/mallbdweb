<!doctype html>
<!--[if IE 9 ]><html class="ie9" lang="en" ng-app="mallBd"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" ng-app="mallBd"><!--<![endif]-->

@include('web/partial/header/mainStart')
@include('web/partial/header/cartScript')
@include('web/partial/header/mainEnds')

<body ng-controller="CartController">
    @include('web/partial/loader')
<!--wide layout-->
<div class="wide_layout relative">
    <!--[if (lt IE 9) | IE 9]>
    <div style="background:#fff;padding:8px 0 10px;">
        <div class="container" style="width:1170px;">
            <div class="row wrapper">
                <div class="clearfix" style="padding:9px 0 0;float:left;width:83%;"><i
                        class="fa fa-exclamation-triangle scheme_color f_left m_right_10"
                        style="font-size:25px;color:#e74c3c;"></i><b style="color:#e74c3c;">Attention! This page may not
                    display correctly.</b> <b>You are using an outdated version of Internet Explorer. For a faster, safer
                    browsing experience.</b></div>
                <div class="t_align_r" style="float:left;width:16%;"><a
                        href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"
                        class="button_type_4 r_corners bg_scheme_color color_light d_inline_b t_align_c" target="_blank"
                        style="margin-bottom:2px;">Update Now!</a></div>
            </div>
        </div>
    </div>
    <![endif]-->
    <!--markup header-->
    <header role="banner" class="type_5 fixed-top">


        @include('web/partial/header/top')
                <!--header bottom part-->
        @include('web/partial/new_menu')

    </header>
    <div class="other-free-gap">

    </div>
    <!--breadcrumbs-->
    <section class="breadcrumbs">
        <div class="container">
            <ul class="horizontal_list clearfix bc_list f_size_medium">
                <li class="m_right_10 current"><a href="{{url('')}}" class="default_t_color">Home<i
                                class="fa fa-angle-right d_inline_middle m_left_10"></i></a></li>
                <li><a href="#" class="default_t_color">Registration</a></li>
            </ul>
        </div>
    </section>
    <!--content-->
    <div class="page_content_offset">
        <div class="container">
            <div class="row clearfix">
                <!--left content column-->
                <section class="col-lg-9 col-md-9 col-sm-9 m_xs_bottom_30">
                    <h2 class="tt_uppercase color_dark m_bottom_25" align="center">Registration</h2>
                    <!--cart table-->

                    <!--tabs-->
                    <div class="tabs m_bottom_45">
                        <!--tabs navigation-->
                        <section class="tabs_content shadow r_corners">
                            <div id="tab-2">
                                <form class="reg-input">
                                    <ul>
                                        <li class="m_bottom_25">
                                            <label for="d_name" class="d_inline_b m_bottom_5 required">First Name</label>
                                            <input type="text" id="first_name" name="" class="r_corners full_width">
                                        </li>

                                        <li class="m_bottom_15">
                                            <label for="u_name" class="d_inline_b m_bottom_5 required">Last Name</label>
                                            <input type="text" id="last_name" name="" class="r_corners full_width">
                                        </li>
                                        <li class="m_bottom_15">
                                            <label for="u_email" class="d_inline_b m_bottom_5 required">Email</label>
                                            <input type="email" id="email" name="" class="r_corners full_width" value="">
                                        </li>
                                        <li class="m_bottom_15">
                                            <label for="d_name" class="d_inline_b m_bottom_5 required">Phone</label>
                                            <input type="text" id="phone" name="" class="r_corners full_width" value="">
                                        </li>
                                        <li>
                                            <label for="u_repeat_pass" class="d_inline_b m_bottom_5 required">Password</label>
                                            <input type="password" id="p_password" name="" class="r_corners full_width" value="">
                                        </li> 
                                        <li>
                                            <label for="u_repeat_pass" class="d_inline_b m_bottom_5 required">Confirm Password</label>
                                            <input type="password" id="confirm_password" name="" class="r_corners full_width" value="">
                                        </li>
                                                                               
                                        <li>
                                            <br>
                                            <div id="errmessage" style="color: red;" > </div>
                                        </li>
                                        <li>
                                            <br>
                                            <input type="button" id="register" name="register" value="submit" onclick="doRegister()" class="button_type_4 bg_scheme_color btn-red">
                                        </li>
                                        
                                    </ul>
                                </form>
                            </div>
                        </section>
                    </div>

                </section>
                <!--right column-->
                <aside class="col-lg-3 col-md-3 col-sm-3">
     
                    <!--banner-->
                    <a href="#" class="d_block r_corners m_bottom_30">
                        <img src="{{asset('/template_resource/images/banner_img_6.jpg')}}" alt="">
                    </a>
                    <a href="#" class="d_block r_corners m_bottom_30">
                        <img src="{{asset('/template_resource/images/banner_img_6.jpg')}}" alt="">
                    </a>
                </aside>
            </div>
        </div>
    </div>
    <!--markup footer-->
    @include('web/partial/footer/newbottom')

</div>
<!--social widgets-->
@include('web/partial/social_widgets/main')
<!--<ul class="social_widgets d_xs_none">
    facebook
    <li class="relative">
        <button class="sw_button t_align_c facebook"><i class="fa fa-facebook"></i></button>
        <div class="sw_content">
            <h3 class="color_dark m_bottom_20">Join Us on Facebook</h3>
            <iframe src="http://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fenvato&amp;width=235&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=438889712801266"
                    style="border:none; overflow:hidden; width:235px; height:258px;"></iframe>
        </div>
    </li>
    twitter feed
    <li class="relative">
        <button class="sw_button t_align_c twitter"><i class="fa fa-twitter"></i></button>
        <div class="sw_content">
            <h3 class="color_dark m_bottom_20">Latest Tweets</h3>

            <div class="twitterfeed m_bottom_25"></div>
            <a role="button" class="button_type_4 d_inline_b r_corners tr_all_hover color_light tw_color"
               href="https://twitter.com/fanfbmltemplate">Follow on Twitter</a>
        </div>
    </li>
    contact form
    <li class="relative">
        <button class="sw_button t_align_c contact"><i class="fa fa-envelope-o"></i></button>
        <div class="sw_content">
            <h3 class="color_dark m_bottom_20">Contact Us</h3>

            <p class="f_size_medium m_bottom_15">Lorem ipsum dolor sit amet, consectetuer adipis mauris</p>

            <form id="contactform" class="mini">
                <input class="f_size_medium m_bottom_10 r_corners full_width" type="text" name="cf_name"
                       placeholder="Your name">
                <input class="f_size_medium m_bottom_10 r_corners full_width" type="email" name="cf_email"
                       placeholder="Email">
                <textarea class="f_size_medium r_corners full_width m_bottom_20" placeholder="Message"
                          name="cf_message"></textarea>
                <button type="submit" class="button_type_4 r_corners mw_0 tr_all_hover color_dark bg_light_color_2">
                    Send
                </button>
            </form>
        </div>
    </li>
    contact info
    <li class="relative">
        <button class="sw_button t_align_c googlemap"><i class="fa fa-map-marker"></i></button>
        <div class="sw_content">
            <h3 class="color_dark m_bottom_20">Store Location</h3>
            <ul class="c_info_list">
                <li class="m_bottom_10">
                    <div class="clearfix m_bottom_15">
                        <i class="fa fa-map-marker f_left color_dark"></i>

                        <p class="contact_e">8901 Marmora Road,<br> Glasgow, D04 89GR.</p>
                    </div>
                    <iframe class="r_corners full_width" id="gmap_mini"
                            src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=ru&amp;geocode=&amp;q=Manhattan,+New+York,+NY,+United+States&amp;aq=0&amp;oq=monheten&amp;sll=37.0625,-95.677068&amp;sspn=65.430355,129.814453&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=%D0%9C%D0%B0%D0%BD%D1%85%D1%8D%D1%82%D1%82%D0%B5%D0%BD,+%D0%9D%D1%8C%D1%8E-%D0%99%D0%BE%D1%80%D0%BA,+%D0%9D%D1%8C%D1%8E+%D0%99%D0%BE%D1%80%D0%BA,+%D0%9D%D1%8C%D1%8E-%D0%99%D0%BE%D1%80%D0%BA&amp;ll=40.790278,-73.959722&amp;spn=0.015612,0.031693&amp;z=13&amp;output=embed"></iframe>
                </li>
                <li class="m_bottom_10">
                    <div class="clearfix m_bottom_10">
                        <i class="fa fa-phone f_left color_dark"></i>

                        <p class="contact_e">800-559-65-80</p>
                    </div>
                </li>
                <li class="m_bottom_10">
                    <div class="clearfix m_bottom_10">
                        <i class="fa fa-envelope f_left color_dark"></i>
                        <a class="contact_e default_t_color" href="mailto:#">info@companyname.com</a>
                    </div>
                </li>
                <li>
                    <div class="clearfix">
                        <i class="fa fa-clock-o f_left color_dark"></i>

                        <p class="contact_e">Monday - Friday: 08.00-20.00 <br>Saturday: 09.00-15.00<br> Sunday: closed
                        </p>
                    </div>
                </li>
            </ul>
        </div>
    </li>
</ul>-->
<!--login popup-->

<button class="t_align_c r_corners tr_all_hover type_2 animate_ftl" id="go_to_top"><i class="fa fa-angle-up"></i>
</button>

<!--scripts include-->
@include('web/partial/script/core')
<script src="{{asset('/template_resource/js/scripts.js')}}"></script>
<script>

    function redirectToHome()
    {
        window.location.href = $('#baseUrl').val()+"/home";
    }

    function doRegister()
    {

        var validationFlag = true;

        if($("#first_name").val()=="")
        {
            validationFlag = false;
            $("#errmessage").html("First Name is required.");
            $("#first_name").focus();
        }
        else if($("#last_name").val()=="")
        {
            validationFlag = false;
            $("#errmessage").html("Last Name is required.");
            $("#last_name").focus();
        }

        else if($("#email").val()=="")
        {
            validationFlag = false;
            $("#errmessage").html("Email is required.");
            $("#email").focus();
        }
        else if( !isValidEmailAddress( $("#email").val() ) ) { 
            $('#errmessage').html("Please enter valid email");
            $("#email").focus();
            validationFlag = false;
        }

        else if($("#phone").val()=="")
        {
            validationFlag = false;
            $("#errmessage").html("Phone is required.");
            $("#phone").focus();
        }
        else if($("#p_password").val()=="")
        {
            validationFlag = false;
            $("#errmessage").html("Password is required.");
            $("#p_password").focus();
        }
        else{
            validationFlag = true;
        }

        
        if(validationFlag){
            $("#checkout_loader").css("display", "block");
            $.ajax({

                url: $("#baseUrl").val()+"api/registration/register",
                method: "POST",
                data: {
                    "first_name": $("#first_name").val(),
                    "last_name": $("#last_name").val(),
                    "email": $("#email").val(),
                    "phone": $("#phone").val(),
                    "password": $("#p_password").val(),
                    "confirm_password": $("#confirm_password").val()
                },
                success: function (data) {
                    if (data.responseStat.status) {
//                        $('#msg').html(data.responseStat.msg);                        
                        $("#errmessage").html(data.responseStat.msg);
                        $.ajax({
                        url: $("#baseUrl").val() + "login",
                        method: "POST",
                        data: {
                            "email": $("#email").val(),
                            "password": $("#p_password").val()
                        },
                        success: function (data) {
                            $("#checkout_loader").css("display", "none");
//                             $("#errmessage").html(data.responseStat.msg);
                            var MytimeOut = setTimeout(redirectToHome,2000);
//                            $('#msg').html(data.responseStat.msg);
                            if (data.responseStat.status && data.responseStat.isLogin) {
                                $('#msg').delay(1000).fadeOut(500, function () {
                                    $('#login_popup').hide();
                                    $('#userName').val(data.responseData.user.firstName);
                                    $('#loginText').hide();
                                    $('#loginName').html(data.responseData.user.firstName).show();
                                    $('#logoutText').show();
                                    afterLoginFunctionCall();
                                    $('#open_chat_anchor').attr('onclick', 'openChat(' + data['responseData']['user']['id'] + ')');
                                    //            $('#open_chat_anchor').prop("onclick", false);
                                    //            document.getElementById('open_chat_anchor').onclick = function () { openChat(data['responseData']['id']) };
                                    //            console.log($('#open_chat_anchor').html());
                                   socket.emit('init', data['responseData']['user']['id']);

                                    //location.reload();
                                });
                            }
                            else {
                                $('#username').prop('disabled', false);
                                $('#password').prop('disabled', false);
                            }
                        }
                    });
                       
                    }
                    else {                       
                        $("#checkout_loader").css("display", "none");
                        $("#errmessage").html(data.responseStat.msg);
                        console.log(data);
                    }


                }

            });
        }else{
            console.log("Error in user input");
        }

    }
</script>
</body>
</html>