<!doctype html>
<!--[if IE 9 ]><html class="ie9" lang="en" ng-app="mallBd"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" ng-app="mallBd"><!--<![endif]-->

@include('web/partial/header/mainStart')
@include('web/partial/header/cartScript')
@include('web/partial/header/mainEnds')

<body ng-controller="CartController">
    @include('web/partial/loader')
<!--wide layout-->
<div class="wide_layout relative">
    <!--[if (lt IE 9) | IE 9]>
    <div style="background:#fff;padding:8px 0 10px;">
        <div class="container" style="width:1170px;">
            <div class="row wrapper">
                <div class="clearfix" style="padding:9px 0 0;float:left;width:83%;"><i
                        class="fa fa-exclamation-triangle scheme_color f_left m_right_10"
                        style="font-size:25px;color:#e74c3c;"></i><b style="color:#e74c3c;">Attention! This page may not
                    display correctly.</b> <b>You are using an outdated version of Internet Explorer. For a faster, safer
                    browsing experience.</b></div>
                <div class="t_align_r" style="float:left;width:16%;"><a
                        href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"
                        class="button_type_4 r_corners bg_scheme_color color_light d_inline_b t_align_c" target="_blank"
                        style="margin-bottom:2px;">Update Now!</a></div>
            </div>
        </div>
    </div>
    <![endif]-->
    <!--markup header-->
    <header role="banner" class="type_5 fixed-top">


        @include('web/partial/header/top')
                <!--header bottom part-->
        @include('web/partial/new_menu')

    </header>
    <div class="other-free-gap">

    </div>
    <!--breadcrumbs-->
    <section class="breadcrumbs">
        <div class="container">
            <ul class="horizontal_list clearfix bc_list f_size_medium">
                <li class="m_right_10 current"><a href="{{url('')}}" class="default_t_color">Home<i
                                class="fa fa-angle-right d_inline_middle m_left_10"></i></a></li>
                <li><a href="#" class="default_t_color">Contacts</a></li>
            </ul>
        </div>
    </section>
    <!--content-->
    <div class="page_content_offset">
        <div class="container">
            <div class="row clearfix">
                <!--left content column-->
                <section class="col-lg-9 col-md-9 col-sm-9">
                    <h2 class="tt_uppercase color_dark m_bottom_25">Contacts</h2>

                    <div class="r_corners photoframe map_container shadow m_bottom_45">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d12098.04228269596!2d-74.00499255597757!3d40.70677554722762!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2z0J3RjNGOLdCZ0L7RgNC6!5e0!3m2!1sru!2s!4v1393474990482"></iframe>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 m_xs_bottom_30">
                            <h2 class="tt_uppercase color_dark m_bottom_25">Contact Info</h2>
                            <ul class="c_info_list">
                                <li class="m_bottom_10">
                                    <div class="clearfix m_bottom_15">
                                        <i class="fa fa-map-marker f_left color_dark"></i>

                                        <p class="contact_e">8901 Marmora Road,<br> Glasgow, D04 89GR.</p>
                                    </div>
                                </li>
                                <li class="m_bottom_10">
                                    <div class="clearfix m_bottom_10">
                                        <i class="fa fa-phone f_left color_dark"></i>

                                        <p class="contact_e">800-559-65-80</p>
                                    </div>
                                </li>
                                <li class="m_bottom_10">
                                    <div class="clearfix m_bottom_10">
                                        <i class="fa fa-envelope f_left color_dark"></i>
                                        <a class="contact_e default_t_color" href="mailto:#">info@companyname.com</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="clearfix">
                                        <i class="fa fa-clock-o f_left color_dark"></i>

                                        <p class="contact_e">Monday - Friday: 08.00-20.00 <br>Saturday: 09.00-15.00<br>
                                            Sunday: closed</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 m_xs_bottom_30">
                            <h2 class="tt_uppercase color_dark m_bottom_25">Contact Form</h2>

                            <p class="m_bottom_10">Send an email. All fields with an <span class="scheme_color">*</span> are
                                required.</p>

                            <form id="contactform">
                                <ul>
                                    <li class="clearfix m_bottom_15">
                                        <div class="f_left half_column">
                                            <label for="cf_name" class="required d_inline_b m_bottom_5">Your Name</label>
                                            <input type="text" id="cf_name" name="cf_name" class="full_width r_corners">
                                        </div>
                                        <div class="f_left half_column">
                                            <label for="cf_email" class="required d_inline_b m_bottom_5">Email</label>
                                            <input type="email" id="cf_email" name="cf_email" class="full_width r_corners">
                                        </div>
                                    </li>
                                    <li class="m_bottom_15">
                                        <label for="cf_subject" class="d_inline_b m_bottom_5">Subject</label>
                                        <input type="text" id="cf_subject" name="cf_subject" class="full_width r_corners">
                                    </li>
                                    <li class="m_bottom_15">
                                        <label for="cf_message" class="d_inline_b m_bottom_5 required">Message</label>
                                        <textarea id="cf_message" name="cf_message" class="full_width r_corners"></textarea>
                                    </li>
                                    <li>
                                        <input type="button" class="button_type_4 bg_light_color_2 r_corners mw_0 tr_all_hover color_dark" onclick="submitContactUs()" value="Submit">

                                    </li>
                                </ul>
                            </form>
                            <div id="notification" align="center"></div>
                        </div>
                    </div>
                </section>
                <!--right column-->
                <aside class="col-lg-3 col-md-3 col-sm-3">
                    <!--widgets-->
                    <figure class="widget shadow r_corners wrapper m_bottom_30">
                        <figcaption>
                            <h3 class="color_light">Categories</h3>
                        </figcaption>
                        <div class="widget_content">
                            <!--Categories list-->
                            <ul class="categories_list">
                                <li class="active">
                                    <a href="#" class="f_size_large scheme_color d_block relative">
                                        <b>Women</b>
                                        <span class="bg_light_color_1 r_corners f_right color_dark talign_c"></span>
                                    </a>
                                    <!--second level-->
                                    <ul>
                                        <li class="active">
                                            <a href="#" class="d_block f_size_large color_dark relative">
                                                Dresses<span
                                                        class="bg_light_color_1 r_corners f_right color_dark talign_c"></span>
                                            </a>
                                            <!--third level-->
                                            <ul>
                                                <li><a href="#" class="color_dark d_block">Evening Dresses</a></li>
                                                <li><a href="#" class="color_dark d_block">Casual Dresses</a></li>
                                                <li><a href="#" class="color_dark d_block">Party Dresses</a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#" class="d_block f_size_large color_dark relative">
                                                Accessories<span
                                                        class="bg_light_color_1 r_corners f_right color_dark talign_c"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="d_block f_size_large color_dark relative">
                                                Tops<span
                                                        class="bg_light_color_1 r_corners f_right color_dark talign_c"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#" class="f_size_large color_dark d_block relative">
                                        <b>Men</b>
                                        <span class="bg_light_color_1 r_corners f_right color_dark talign_c"></span>
                                    </a>
                                    <!--second level-->
                                    <ul class="d_none">
                                        <li>
                                            <a href="#" class="d_block f_size_large color_dark relative">
                                                Shorts<span
                                                        class="bg_light_color_1 r_corners f_right color_dark talign_c"></span>
                                            </a>
                                            <!--third level-->
                                            <ul class="d_none">
                                                <li><a href="#" class="color_dark d_block">Evening</a></li>
                                                <li><a href="#" class="color_dark d_block">Casual</a></li>
                                                <li><a href="#" class="color_dark d_block">Party</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#" class="f_size_large color_dark d_block relative">
                                        <b>Kids</b>
                                        <span class="bg_light_color_1 r_corners f_right color_dark talign_c"></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </figure>
                    <!--compare products-->
                    <figure class="widget shadow r_corners wrapper m_bottom_30">
                        <figcaption>
                            <h3 class="color_light">Compare Products</h3>
                        </figcaption>
                        <div class="widget_content">
                            You have no product to compare.
                        </div>
                    </figure>
                    <!--banner-->
                    <a href="#" class="d_block r_corners m_bottom_30">
                        <img src="images/banner_img_6.jpg" alt="">
                    </a>
                </aside>
            </div>
        </div>
    </div>
    <!--markup footer-->
    @include('web/partial/footer/newbottom')
</div>
<!--social widgets-->
@include('web/partial/social_widgets/main')
<!--<ul class="social_widgets d_xs_none">
    facebook
    <li class="relative">
        <button class="sw_button t_align_c facebook"><i class="fa fa-facebook"></i></button>
        <div class="sw_content">
            <h3 class="color_dark m_bottom_20">Join Us on Facebook</h3>
            <iframe src="http://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fenvato&amp;width=235&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=438889712801266"
                    style="border:none; overflow:hidden; width:235px; height:258px;"></iframe>
        </div>
    </li>
    twitter feed
    <li class="relative">
        <button class="sw_button t_align_c twitter"><i class="fa fa-twitter"></i></button>
        <div class="sw_content">
            <h3 class="color_dark m_bottom_20">Latest Tweets</h3>

            <div class="twitterfeed m_bottom_25"></div>
            <a role="button" class="button_type_4 d_inline_b r_corners tr_all_hover color_light tw_color"
               href="https://twitter.com/fanfbmltemplate">Follow on Twitter</a>
        </div>
    </li>
    contact form
    <li class="relative">
        <button class="sw_button t_align_c contact"><i class="fa fa-envelope-o"></i></button>
        <div class="sw_content">
            <h3 class="color_dark m_bottom_20">Contact Us</h3>

            <p class="f_size_medium m_bottom_15">Lorem ipsum dolor sit amet, consectetuer adipis mauris</p>

            <form id="contactform" class="mini">
                <input class="f_size_medium m_bottom_10 r_corners full_width" type="text" name="cf_name"
                       placeholder="Your name">
                <input class="f_size_medium m_bottom_10 r_corners full_width" type="email" name="cf_email"
                       placeholder="Email">
                <textarea class="f_size_medium r_corners full_width m_bottom_20" placeholder="Message"
                          name="cf_message"></textarea>
                <button type="submit" class="button_type_4 r_corners mw_0 tr_all_hover color_dark bg_light_color_2">
                    Send
                </button>
            </form>
        </div>
    </li>
    contact info
    <li class="relative">
        <button class="sw_button t_align_c googlemap"><i class="fa fa-map-marker"></i></button>
        <div class="sw_content">
            <h3 class="color_dark m_bottom_20">Store Location</h3>
            <ul class="c_info_list">
                <li class="m_bottom_10">
                    <div class="clearfix m_bottom_15">
                        <i class="fa fa-map-marker f_left color_dark"></i>

                        <p class="contact_e">8901 Marmora Road,<br> Glasgow, D04 89GR.</p>
                    </div>
                    <iframe class="r_corners full_width" id="gmap_mini"
                            src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=ru&amp;geocode=&amp;q=Manhattan,+New+York,+NY,+United+States&amp;aq=0&amp;oq=monheten&amp;sll=37.0625,-95.677068&amp;sspn=65.430355,129.814453&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=%D0%9C%D0%B0%D0%BD%D1%85%D1%8D%D1%82%D1%82%D0%B5%D0%BD,+%D0%9D%D1%8C%D1%8E-%D0%99%D0%BE%D1%80%D0%BA,+%D0%9D%D1%8C%D1%8E+%D0%99%D0%BE%D1%80%D0%BA,+%D0%9D%D1%8C%D1%8E-%D0%99%D0%BE%D1%80%D0%BA&amp;ll=40.790278,-73.959722&amp;spn=0.015612,0.031693&amp;z=13&amp;output=embed"></iframe>
                </li>
                <li class="m_bottom_10">
                    <div class="clearfix m_bottom_10">
                        <i class="fa fa-phone f_left color_dark"></i>

                        <p class="contact_e">800-559-65-80</p>
                    </div>
                </li>
                <li class="m_bottom_10">
                    <div class="clearfix m_bottom_10">
                        <i class="fa fa-envelope f_left color_dark"></i>
                        <a class="contact_e default_t_color" href="mailto:#">info@companyname.com</a>
                    </div>
                </li>
                <li>
                    <div class="clearfix">
                        <i class="fa fa-clock-o f_left color_dark"></i>

                        <p class="contact_e">Monday - Friday: 08.00-20.00 <br>Saturday: 09.00-15.00<br> Sunday: closed
                        </p>
                    </div>
                </li>
            </ul>
        </div>
    </li>
</ul>-->
<!--custom popup-->

<!--login popup-->

<button class="t_align_c r_corners tr_all_hover type_2 animate_ftl" id="go_to_top"><i class="fa fa-angle-up"></i>
</button>

<!--scripts include-->
@include('web/partial/script/core')

<script src="{{asset('/template_resource/js/jquery.fancybox-1.3.4.js')}}"></script>
<script src="{{asset('/template_resource/js/scripts.js')}}"></script>
<script src="{{asset('/template_resource/js/elevatezoom.min.js')}}"></script>

<script>

    function getReviewPoint(){
       return $('#reviewPoint').find("li.active").length;
    }
    function submitContactUs(){

            $("#notification").html("processing...");

            $.ajax({

                url: $("#baseUrl").val() + "api/contactus/add",
                method: "POST",
                data: {
                    "name": $('#cf_name').val(),
                    "email":$('#cf_email').val(),
                    "subject": $('#cf_subject').val(),
                    "message": $('#cf_message').val()
                },
                success: function (data) {
                    if (data.responseStat.status) {
                        $("#notification").html(data.responseStat.msg);
                        $("#notification").show();
                        $("#notification").fadeOut(2000);

                        $('#cf_name').val("");
                        $('#cf_email').val("");
                        $('#cf_subject').val("");
                        $('#cf_message').val("");

                        console.log(data);

                    }
                    else {
                        //$("#errmsg").html(data.responseStat.msg);
                        $("#notification").html(data.responseStat.msg);
                        $("#notification").show();
                        $("#notification").fadeOut(2000);
                        console.log(data);
                    }

                }

            });
    }

</script>
</body>
</html>