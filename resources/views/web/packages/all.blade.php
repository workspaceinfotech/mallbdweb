<!doctype html>
<!--[if IE 9 ]><html class="ie9" lang="en" ng-app="mallBd"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" ng-app="mallBd"><!--<![endif]-->

@include('web/partial/header/mainStart')
@include('web/partial/header/cartScript')
@include('web/partial/header/mainEnds')

<body ng-controller="CartController" >
    @include('web/partial/loader')
<!--wide layout-->
<div class="wide_layout relative">
<!--[if (lt IE 9) | IE 9]>
<div style="background:#fff;padding:8px 0 10px;">
    <div class="container" style="width:1170px;">
        <div class="row wrapper">
            <div class="clearfix" style="padding:9px 0 0;float:left;width:83%;"><i
                    class="fa fa-exclamation-triangle scheme_color f_left m_right_10"
                    style="font-size:25px;color:#e74c3c;"></i><b style="color:#e74c3c;">Attention! This page may not
                display correctly.</b> <b>You are using an outdated version of Internet Explorer. For a faster, safer
                browsing experience.</b></div>
            <div class="t_align_r" style="float:left;width:16%;"><a
                    href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"
                    class="button_type_4 r_corners bg_scheme_color color_light d_inline_b t_align_c" target="_blank"
                    style="margin-bottom:2px;">Update Now!</a></div>
        </div>
    </div>
</div>
<![endif]-->
<!--markup header-->
<header role="banner" class="type_5 fixed-top">

    <!--header top part-->
@include('web/partial/header/top')

    <!--main menu container-->
@include('web/partial/new_menu')
</header>

<div class="other-free-gap">
</div>

    <?php
            $linkStr ="";
            $count=1;
        ?>
@include('web/partial/sideblock/shippingtext')
    <section class="breadcrumbs">
        <div class="container">
            <ul class="horizontal_list clearfix bc_list f_size_medium">
                <li class="m_right_10 current"><a href="{{url('/home')}}" class="default_t_color">Home<i
                                class="fa fa-angle-right d_inline_middle m_left_10"></i></a></li>
            </ul>
        </div>
    </section>
    <!--content-->
    <div class="page_content_offset">
        <div class="container">
            <div class="row clearfix">
                <aside class="col-lg-3 col-md-3 col-sm-3">
                    <!--widgets-->
                    {{--<figure class="widget shadow r_corners wrapper m_bottom_30">--}}
                        {{--<figcaption>--}}
                            {{--<h3 class="color_light">Filter</h3>--}}
                        {{--</figcaption>--}}
                        {{--<div class="widget_content">--}}
                            {{--<!--filter form-->--}}
                            {{--<form>--}}

                                {{--<!--price-->--}}
                                {{--<fieldset class="m_bottom_20">--}}
                                    {{--<legend class="default_t_color f_size_large m_bottom_15 clearfix full_width relative">--}}
                                        {{--<b class="f_left">Price</b>--}}
                                        {{--<button type="button" class="f_size_medium f_right color_dark bg_tr tr_all_hover close_fieldset"><i--}}
                                                    {{--class="fa fa-times lh_inherit"></i></button>--}}
                                    {{--</legend>--}}
                                    {{--<div id="price" class="m_bottom_10"></div>--}}
                                    {{--<div class="clearfix range_values">--}}
                                        {{--<input class="f_left first_limit" readonly name="" type="text" value="$0">--}}
                                        {{--<input class="f_right last_limit t_align_r" readonly name="" type="text" value="$250">--}}
                                    {{--</div>--}}
                                {{--</fieldset>--}}

                                {{--<button type="reset" class="color_dark bg_tr text_cs_hover tr_all_hover"><i--}}
                                            {{--class="fa fa-refresh lh_inherit m_right_10"></i>Reset--}}
                                {{--</button>--}}
                            {{--</form>--}}
                        {{--</div>--}}
                    {{--</figure>--}}
                    <figure class="widget shadow r_corners wrapper m_bottom_30">
                        <figcaption>
                            <h3 class="color_light">Categories</h3>
                        </figcaption>
                        <div class="widget_content">
                            <!--Categories list-->
                            @foreach($categoryList as $category)
                                <ul class="categories_list">
                                    <li>
                                        <a href="{{url("category/$category->id")}}" class="f_size_large scheme_color d_block relative">
                                            <b>{{$category->title}}</b>
                                            <span class="bg_light_color_1 r_corners f_right color_dark talign_c"></span>
                                        </a>
                                        <!--second level-->
                                        @if(!empty(@$category->childrens))
                                            <ul style="display: none">
                                                @foreach(@$category->childrens as $firstChild)
                                                    <li> {{--class="active"--}}
                                                        <a href="{{url("category/$category->id/$firstChild->id")}}" class="d_block f_size_large color_dark relative">
                                                            {{$firstChild->title}}<span class="bg_light_color_1 r_corners f_right color_dark talign_c"></span>
                                                        </a>
                                                        <!--third level-->
                                                        @if(!empty($firstChild->childrens))
                                                            <ul style="display: none">
                                                                @foreach(@$firstChild->childrens as $secondChild)
                                                                    <li>
                                                                        <a href="{{url("category/$category->id/$firstChild->id/$secondChild->id")}}" class="color_dark d_block">
                                                                            {{$secondChild->title}}</a>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        @endif
                                                    </li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </li>
                                </ul>
                            @endforeach
                        </div>
                    </figure>
                    <!--compare products-->
                    <figure class="widget shadow r_corners wrapper m_bottom_30">
                        <figcaption>
                            <h3 class="color_light">Compare Products</h3>
                        </figcaption>
                        <div class="widget_content">
                            You have no product to compare.
                        </div>
                    </figure>
                    <!--banner-->
                    <a href="#" class="d_block r_corners m_bottom_30">
                        <img src="images/banner_img_6.jpg" alt="">
                    </a>
                    <!--Bestsellers-->
                    <figure class="widget shadow r_corners wrapper m_bottom_30">
                        <figcaption>
                            <h3 class="color_light">Bestsellers</h3>
                        </figcaption>
                        <div class="widget_content">
                            <div class="clearfix m_bottom_15">
                                <img src="images/bestsellers_img_1.jpg" alt=""
                                     class="f_left m_right_15 m_sm_bottom_10 f_sm_none f_xs_left m_xs_bottom_0">
                                <a href="#" class="color_dark d_block bt_link">Ut tellus dolor dapibus</a>
                                <!--rating-->
                                <ul class="horizontal_list clearfix d_inline_b rating_list type_2 tr_all_hover m_bottom_10">
                                    <li class="active">
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                    <li class="active">
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                    <li class="active">
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                    <li class="active">
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                </ul>
                                <p class="scheme_color">$61.00</p>
                            </div>
                            <hr class="m_bottom_15">
                            <div class="clearfix m_bottom_15">
                                <img src="images/bestsellers_img_2.jpg" alt=""
                                     class="f_left m_right_15 m_sm_bottom_10 f_sm_none f_xs_left m_xs_bottom_0">
                                <a href="#" class="color_dark d_block bt_link">Elementum vel</a>
                                <!--rating-->
                                <ul class="horizontal_list clearfix d_inline_b rating_list type_2 tr_all_hover m_bottom_10">
                                    <li class="active">
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                    <li class="active">
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                    <li class="active">
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                    <li class="active">
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                </ul>
                                <p class="scheme_color">$57.00</p>
                            </div>
                            <hr class="m_bottom_15">
                            <div class="clearfix m_bottom_5">
                                <img src="images/bestsellers_img_3.jpg" alt=""
                                     class="f_left m_right_15 m_sm_bottom_10 f_sm_none f_xs_left m_xs_bottom_0">
                                <a href="#" class="color_dark d_block bt_link">Crsus eleifend elit</a>
                                <!--rating-->
                                <ul class="horizontal_list clearfix d_inline_b rating_list type_2 tr_all_hover m_bottom_10">
                                    <li class="active">
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                    <li class="active">
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                    <li class="active">
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                    <li class="active">
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                </ul>
                                <p class="scheme_color">$24.00</p>
                            </div>
                        </div>
                    </figure>
                    <!--tags-->
                    <figure class="widget shadow r_corners wrapper m_bottom_30">
                        <figcaption>
                            <h3 class="color_light">Tags</h3>
                        </figcaption>
                        <div class="widget_content">
                            <div class="tags_list">
                                <a href="#" class="color_dark d_inline_b v_align_b">accessories,</a>
                                <a href="#" class="color_dark d_inline_b f_size_ex_large v_align_b">bestseller,</a>
                                <a href="#" class="color_dark d_inline_b v_align_b">clothes,</a>
                                <a href="#" class="color_dark d_inline_b f_size_big v_align_b">dresses,</a>
                                <a href="#" class="color_dark d_inline_b v_align_b">fashion,</a>
                                <a href="#" class="color_dark d_inline_b f_size_large v_align_b">men,</a>
                                <a href="#" class="color_dark d_inline_b v_align_b">pants,</a>
                                <a href="#" class="color_dark d_inline_b v_align_b">sale,</a>
                                <a href="#" class="color_dark d_inline_b v_align_b">short,</a>
                                <a href="#" class="color_dark d_inline_b f_size_ex_large v_align_b">skirt,</a>
                                <a href="#" class="color_dark d_inline_b v_align_b">top,</a>
                                <a href="#" class="color_dark d_inline_b f_size_big v_align_b">women</a>
                            </div>
                        </div>
                    </figure>
                </aside>
                <!--left content column-->
                <section class="col-lg-9 col-md-9 col-sm-9">
                    <h2 class="tt_uppercase color_dark m_bottom_25">Packages</h2>
                    <!--sort-->
                    {{--<div class="row clearfix m_bottom_10">--}}
                        {{--<div class="col-lg-7 col-md-8 col-sm-12 m_sm_bottom_10">--}}
                            {{--<p class="d_inline_middle f_size_medium">Sort by:</p>--}}

                            {{--<div class="clearfix d_inline_middle m_left_10">--}}
                                {{--<!--product name select-->--}}
                                {{--<div class="custom_select f_size_medium relative f_left">--}}
                                    {{--<div class="select_title r_corners relative color_dark">Product name</div>--}}
                                    {{--<ul class="select_list d_none"></ul>--}}
                                    {{--<select name="product_name">--}}
                                        {{--<option value="Product SKU">Product SKU</option>--}}
                                        {{--<option value="Product Price">Product Price</option>--}}
                                        {{--<option value="Product ID">Product ID</option>--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                                {{--<button class="button_type_7 bg_light_color_1 color_dark tr_all_hover r_corners mw_0 box_s_none bg_cs_hover f_left m_left_5">--}}
                                    {{--<i class="fa fa-sort-amount-asc m_left_0 m_right_0"></i></button>--}}
                            {{--</div>--}}
                            {{--<!--manufacturer select-->--}}
                            {{--<div class="custom_select f_size_medium relative d_inline_middle m_left_15 m_xs_left_5 m_mxs_left_0 m_mxs_top_10">--}}
                                {{--<div class="select_title r_corners relative color_dark">Select manufacturer</div>--}}
                                {{--<ul class="select_list d_none"></ul>--}}
                                {{--<select name="manufacturer">--}}
                                    {{--<option value="Manufacture 1">Manufacture 1</option>--}}
                                    {{--<option value="Manufacture 2">Manufacture 2</option>--}}
                                    {{--<option value="Manufacture 3">Manufacture 3</option>--}}
                                {{--</select>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                      {{----}}
                    {{--</div>--}}
                    <hr class="m_bottom_10 divider_type_3">
                    <div class="row clearfix m_bottom_15">
                        <div class="col-lg-7 col-md-7 col-sm-8 col-xs-12 m_xs_bottom_10">
                            <p class="d_inline_middle f_size_medium d_xs_block m_xs_bottom_5">Results 1 - <?php echo $limit;  ?>
                                of  {{$totalPackage}}</p>

                            <p class="d_inline_middle f_size_medium m_left_20 m_xs_left_0">Show:</p>
                            <!--show items per page select-->
                            <select name="show" id="selectViewSize" onchange="changeAction()">
                                <option value="5" <?php if($limit=='5') echo 'selected="selected"'?>>5</option>
                                <option value="10" <?php if($limit=='10') echo 'selected="selected"'?>>10</option>
                                <option value="15" <?php if($limit=='15') echo 'selected="selected"'?>>15</option>
                            </select>


                            <p class="d_inline_middle f_size_medium m_left_5">items per page</p>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-4 col-xs-12 t_align_r t_xs_align_l">
                            <!--pagination-->
                            <a role="button" {{--onclick="prevPage({{{$currentPage}}})"--}} href="javascript:void(0)"
                               class="button_type_10 color_dark d_inline_middle bg_cs_hover bg_light_color_1 t_align_c tr_delay_hover r_corners box_s_none"><i
                                        class="fa fa-angle-left"></i></a>

                            <ul class="horizontal_list clearfix d_inline_middle f_size_medium m_left_10">
                                <?php
                                $numberOfPages = ceil($totalPackage / $limit);
                                for($i=0;$i<$numberOfPages;$i++){
                                ?>
                                <li class="m_right_10"><a class="color_dark" href="javascript:void(0)" onclick="specificPage(<?php echo $i;?>)"><?php echo $i+1;?></a></li>
                                <?php }?>
                            </ul>
                            <a {{--onclick="nextPage({{{$currentPage}}})"--}} role="button" href="javascript:void(0)"
                               class="button_type_10 color_dark d_inline_middle bg_cs_hover bg_light_color_1 t_align_c tr_delay_hover r_corners box_s_none" ><i
                                        class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                    <!--products-->

                    <section class="products_container category_grid clearfix m_bottom_15">
                        <!--product item-->

                        @foreach($packages as $package)

                            <?php
                            $length = strlen($package->packageTitle);
                            if($length>=19)
                            {
                                $editedTitle = substr($package->packageTitle, 0, 18)."...";
                            }
                            else{
                                $editedTitle = $package->packageTitle;
                            }

                            ?>


                        <div class="product_item hit w_xs_full">
                            <figure class="r_corners photoframe type_2 t_align_c tr_all_hover shadow relative">
                                <div class="o-tag">
                                       <span>hot</span>
                                </div>
                                <!--product preview-->
                                <a href="{{url('/packages/'.$package->id)}}" class="d_block relative wrapper pp_wrap m_bottom_15">
                                    <img src="{{ $imageUrl }}package/general/{{@$package->image}}" class="tr_all_hover" alt="">
            <span role="button" data-popup="#quick_view_package_{{@$package->id}}"
                  class="button_type_5 box_s_none color_light r_corners tr_all_hover d_xs_none">Quick View</span>
                                </a>
                                <!--description and price of product-->
                                <figcaption>
                                    <h5 class="m_bottom_10"><a href="{{url('/packages/'.$package->id)}}" class="color_dark" target="_blank">{{$editedTitle}}</a></h5>
                                    <!--rating-->
                                    {{--<ul class="horizontal_list d_inline_b m_bottom_10 clearfix rating_list type_2 tr_all_hover">
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                    </ul>--}}
                                    <s class="v_align_b f_size_ex_large price-old"><span><?php echo $currency->HTMLCode; ?></span>{{ number_format($package->originalPriceTotal,2)}} </s><span
                                            class="v_align_b f_size_big m_left_5 scheme_color fw_medium price-usual"><span>&nbsp; <?php echo $currency->HTMLCode; ?></span>{{ number_format($package->packagePriceTotal,2)}} &nbsp;</span>
                                    <button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light mw_0 m_bottom_15" ng-click="addPackageToCart('packageJsonObj_{{$package->id}}',1,false)" >Add to
                                        Cart
                                    </button>
                                    <div class="clearfix m_bottom_5">
                                        <ul class="horizontal_list d_inline_b l_width_divider">
                                            {{--<li class="m_right_15 f_md_none m_md_right_0"><a href="#" class="color_dark">Add to Wishlist</a>
                                            </li>--}}
                                            {{--<li class="f_md_none"><a href="#" class="color_dark">Add to Compare</a></li>--}}
                                        </ul>
                                    </div>
                                </figcaption>
                            </figure>
                        </div>
                                <input type="hidden" id="packageJsonObj_{{$package->id}}" value="{{json_encode($package)}}"  />
                            @endforeach

                    </section>
                    <hr class="m_bottom_10 divider_type_3">
                    <div class="row clearfix m_bottom_15">
                        <div class="col-lg-7 col-md-7 col-sm-8 col-xs-12 m_xs_bottom_10">
                            <p class="d_inline_middle f_size_medium d_xs_block m_xs_bottom_5">Results 1 - <?php echo $limit;  ?>
                                of  {{$totalPackage}}</p>

                            <p class="d_inline_middle f_size_medium m_left_20 m_xs_left_0">Show:</p>
                            <!--show items per page select-->
                            <select name="show" id="selectViewSize2" onchange="changeActionBottom()">
                                <option value="5" <?php if($limit=='5') echo 'selected="selected"'?>>5</option>
                                <option value="10" <?php if($limit=='10') echo 'selected="selected"'?>>10</option>
                                <option value="15" <?php if($limit=='15') echo 'selected="selected"'?>>15</option>
                            </select>


                            <p class="d_inline_middle f_size_medium m_left_5">items per page</p>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-4 col-xs-12 t_align_r t_xs_align_l">
                            <!--pagination-->
                            <a role="button" {{--onclick="prevPage({{{$currentPage}}})"--}} href="javascript:void(0)"
                               class="button_type_10 color_dark d_inline_middle bg_cs_hover bg_light_color_1 t_align_c tr_delay_hover r_corners box_s_none"><i
                                        class="fa fa-angle-left"></i></a>

                            <ul class="horizontal_list clearfix d_inline_middle f_size_medium m_left_10">
                                <?php
                                $numberOfPages = ceil($totalPackage / $limit);
                                for($i=0;$i<$numberOfPages;$i++){
                                ?>
                                <li class="m_right_10"><a class="color_dark" href="javascript:void(0)" onclick="specificPage(<?php echo $i;?>)"><?php echo $i+1;?></a></li>
                                <?php }?>
                            </ul>
                            <a {{--onclick="nextPage({{{$currentPage}}})"--}} role="button" href="javascript:void(0)"
                                                                              class="button_type_10 color_dark d_inline_middle bg_cs_hover bg_light_color_1 t_align_c tr_delay_hover r_corners box_s_none" ><i
                                        class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </section>
                <!--right column-->
            </div>
        </div>
    </div>
<!--markup footer-->
    @include('web/partial/footer/newbottom')
</div>
<!--social widgets-->
@include('web/partial/social_widgets/main')
<!--<ul class="social_widgets d_xs_none">
    facebook
    <li class="relative">
        <button class="sw_button t_align_c facebook"><i class="fa fa-facebook"></i></button>
        <div class="sw_content">
            <h3 class="color_dark m_bottom_20">Join Us on Facebook</h3>
            <iframe src="http://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fenvato&amp;width=235&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=438889712801266"
                    style="border:none; overflow:hidden; width:235px; height:258px;"></iframe>
        </div>
    </li>
    twitter feed
    <li class="relative">
        <button class="sw_button t_align_c twitter"><i class="fa fa-twitter"></i></button>
        <div class="sw_content">
            <h3 class="color_dark m_bottom_20">Latest Tweets</h3>

            <div class="twitterfeed m_bottom_25"></div>
            <a role="button" class="button_type_4 d_inline_b r_corners tr_all_hover color_light tw_color"
               href="https://twitter.com/fanfbmltemplate">Follow on Twitter</a>
        </div>
    </li>
    contact form
    <li class="relative">
        <button class="sw_button t_align_c contact"><i class="fa fa-envelope-o"></i></button>
        <div class="sw_content">
            <h3 class="color_dark m_bottom_20">Contact Us</h3>

            <p class="f_size_medium m_bottom_15">Lorem ipsum dolor sit amet, consectetuer adipis mauris</p>

            <form id="contactform" class="mini">
                <input class="f_size_medium m_bottom_10 r_corners full_width" type="text" name="cf_name"
                       placeholder="Your name">
                <input class="f_size_medium m_bottom_10 r_corners full_width" type="email" name="cf_email"
                       placeholder="Email">
                <textarea class="f_size_medium r_corners full_width m_bottom_20" placeholder="Message"
                          name="cf_message"></textarea>
                <button type="submit" class="button_type_4 r_corners mw_0 tr_all_hover color_dark bg_light_color_2">
                    Send
                </button>
            </form>
        </div>
    </li>
    contact info
    <li class="relative">
        <button class="sw_button t_align_c googlemap"><i class="fa fa-map-marker"></i></button>
        <div class="sw_content">
            <h3 class="color_dark m_bottom_20">Store Location</h3>
            <ul class="c_info_list">
                <li class="m_bottom_10">
                    <div class="clearfix m_bottom_15">
                        <i class="fa fa-map-marker f_left color_dark"></i>

                        <p class="contact_e">8901 Marmora Road,<br> Glasgow, D04 89GR.</p>
                    </div>
                    <iframe class="r_corners full_width" id="gmap_mini"
                            src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=ru&amp;geocode=&amp;q=Manhattan,+New+York,+NY,+United+States&amp;aq=0&amp;oq=monheten&amp;sll=37.0625,-95.677068&amp;sspn=65.430355,129.814453&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=%D0%9C%D0%B0%D0%BD%D1%85%D1%8D%D1%82%D1%82%D0%B5%D0%BD,+%D0%9D%D1%8C%D1%8E-%D0%99%D0%BE%D1%80%D0%BA,+%D0%9D%D1%8C%D1%8E+%D0%99%D0%BE%D1%80%D0%BA,+%D0%9D%D1%8C%D1%8E-%D0%99%D0%BE%D1%80%D0%BA&amp;ll=40.790278,-73.959722&amp;spn=0.015612,0.031693&amp;z=13&amp;output=embed"></iframe>
                </li>
                <li class="m_bottom_10">
                    <div class="clearfix m_bottom_10">
                        <i class="fa fa-phone f_left color_dark"></i>

                        <p class="contact_e">800-559-65-80</p>
                    </div>
                </li>
                <li class="m_bottom_10">
                    <div class="clearfix m_bottom_10">
                        <i class="fa fa-envelope f_left color_dark"></i>
                        <a class="contact_e default_t_color" href="mailto:#">info@companyname.com</a>
                    </div>
                </li>
                <li>
                    <div class="clearfix">
                        <i class="fa fa-clock-o f_left color_dark"></i>

                        <p class="contact_e">Monday - Friday: 08.00-20.00 <br>Saturday: 09.00-15.00<br> Sunday: closed
                        </p>
                    </div>
                </li>
            </ul>
        </div>
    </li>
</ul>-->
<!--custom popup-->
@foreach($packages as $package)
<div class="popup_wrap d_none" id="quick_view_package_{{@$package->id}}">
    <section class="popup r_corners shadow">
        <button class="bg_tr color_dark tr_all_hover text_cs_hover close f_size_large"><i class="fa fa-times"></i>
        </button>
        <div class="clearfix">
            <div class="custom_scrollbar">
                <!--left popup column-->
                <div class="f_left half_column">
                    <div class="relative d_inline_b m_bottom_10 qv_preview">
                        <span class="hot_stripe"><img src="{{asset('/template_resource/images/sale_product.png')}}" alt=""></span>
                        <img src="{{ $imageUrl }}package/large/{{@$package->image}}" class="tr_all_hover" alt="" id="zoom_image">
                    </div>
                    <!--carousel-->
                    <div class="relative qv_carousel_wrap m_bottom_20">
                        <button class="button_type_11 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_prev">
                            <i class="fa fa-angle-left "></i>
                        </button>
                        <ul class="qv_carousel d_inline_middle">
                            <a href="javascript:void(0)" data-image="images/quick_view_img_7.jpg')}}" data-zoom-image="images/preview_zoom_1.jpg')}}"><img id="small-image{{@$package->image}}"
                                                                                                                                          src="{{ $imageUrl }}package/thumbnail/{{@$package->image}}" alt="" onerror="this.src='http://placehold.it/90x90?text=Product Banner not found'" onclick="changePicture('{{@$package->image}}')"></a>

                            @foreach($package->packageProduct as $products)
                                <a href="javascript:void(0)" data-image="images/quick_view_img_7.jpg')}}" data-zoom-image="images/preview_zoom_1.jpg')}}"><img id="small-image{{@$products->product->pictures[0]->name}}"
                                                                                                                                                               src="{{ $imageUrl }}product/thumbnail/{{@$products->product->pictures[0]->name}}" alt="" onerror="this.src='http://placehold.it/90x90?text=Product Banner not found'" onclick="changePicture('{{@$products->product->pictures[0]->name}}')"></a>
                            @endforeach
                        </ul>
                        <button class="button_type_11 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_next">
                            <i class="fa fa-angle-right "></i>
                        </button>
                    </div>
                    <div class="d_inline_middle">Share this:</div>
                    <div class="d_inline_middle m_left_5">
                        <!-- AddThis Button BEGIN -->
                        <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
                            <a class="addthis_button_preferred_1"></a>
                            <a class="addthis_button_preferred_2"></a>
                            <a class="addthis_button_preferred_3"></a>
                            <a class="addthis_button_preferred_4"></a>
                            <a class="addthis_button_compact"></a>
                            <a class="addthis_counter addthis_bubble_style"></a>
                        </div>
                        <!-- AddThis Button END -->
                    </div>
                </div>
                <!--right popup column-->
                <div class="f_right half_column">
                    <!--description-->
                    <h2 class="m_bottom_10"><a href="{{url('/packages/'.$package->id)}}" class="color_dark fw_medium" target="_blank">{{$package->packageTitle}}</a></h2>

                    {{--<div class="m_bottom_10">
                        <!--rating-->
                        <ul class="horizontal_list d_inline_middle type_2 clearfix rating_list tr_all_hover">
                            <li class="active">
                                <i class="fa fa-star-o empty tr_all_hover"></i>
                                <i class="fa fa-star active tr_all_hover"></i>
                            </li>
                            <li class="active">
                                <i class="fa fa-star-o empty tr_all_hover"></i>
                                <i class="fa fa-star active tr_all_hover"></i>
                            </li>
                            <li class="active">
                                <i class="fa fa-star-o empty tr_all_hover"></i>
                                <i class="fa fa-star active tr_all_hover"></i>
                            </li>
                            <li class="active">
                                <i class="fa fa-star-o empty tr_all_hover"></i>
                                <i class="fa fa-star active tr_all_hover"></i>
                            </li>
                            <li>
                                <i class="fa fa-star-o empty tr_all_hover"></i>
                                <i class="fa fa-star active tr_all_hover"></i>
                            </li>
                        </ul>
                        <a href="#" class="d_inline_middle default_t_color f_size_small m_left_5">1 Review(s) </a>
                    </div>--}}

                    <hr class="divider_type_3 m_bottom_10">
                    <p class="m_bottom_10"><?php echo $package->description;?></p>
                    <hr class="divider_type_3 m_bottom_15">
                    <div class="m_bottom_15">
                        <s class="v_align_b f_size_ex_large"><span><?php echo $currency->HTMLCode; ?></span>{{ number_format($package->originalPriceTotal,2) }}</s><span
                            class="v_align_b f_size_big m_left_5 scheme_color fw_medium"><span>&nbsp;&nbsp; <?php echo $currency->HTMLCode; ?></span>{{ number_format($package->packagePriceTotal,2) }}</span>
                    </div>

                    <div class="clearfix m_bottom_15">
                        <button class="button_type_12 r_corners bg_scheme_color color_light tr_delay_hover f_left f_size_large">
                            Add to Basket
                        </button>
                        {{--<button class="button_type_12 bg_light_color_2 tr_delay_hover f_left r_corners color_dark m_left_5 p_hr_0">
                            <i class="fa fa-heart-o f_size_big"></i><span
                                class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span></button>--}}
                        <button class="button_type_12 bg_light_color_2 tr_delay_hover f_left r_corners color_dark m_left_5 p_hr_0">
                            <i class="fa fa-files-o f_size_big"></i><span
                                class="tooltip tr_all_hover r_corners color_dark f_size_small">Compare</span></button>
<!--                        <button class="button_type_12 bg_light_color_2 tr_delay_hover f_left r_corners color_dark m_left_5 p_hr_0 relative">
                            <i class="fa fa-question-circle f_size_big"></i><span
                                class="tooltip tr_all_hover r_corners color_dark f_size_small">Ask a Question</span>
                        </button>-->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endforeach
<!--login popup-->

<button class="t_align_c r_corners tr_all_hover type_2 animate_ftl" id="go_to_top"><i class="fa fa-angle-up"></i>
</button>

<!--scripts include-->
@include('web/partial/script/core')
<script src="{{asset('/template_resource/js/scripts.js')}}"></script>
<!-- Hidden Values -->

    <script>

        function changePicture(picName)
        {
            srcOfZoom = $('#zoom_image').attr('src');
            parts = srcOfZoom.split("/");

            if(picName == "<?php echo $package->image; ?>")
            {
                newSource = (parts[0]+"/"+parts[1]+"/"+parts[2]+"/"+parts[3]+"/"+parts[4]+"/"+parts[5]+"/"+"package"+"/"+parts[7]+"/"+picName);

            }else{
                newSource = (parts[0]+"/"+parts[1]+"/"+parts[2]+"/"+parts[3]+"/"+parts[4]+"/"+parts[5]+"/"+"product"+"/"+parts[7]+"/"+picName);
            }

            $("#zoom_image").attr("src",newSource);


        }

        function specificPage($offset)
        {
            var url =window.location.origin + window.location.pathname;
            var myselect = document.getElementById("selectViewSize");
            var limitValue = myselect.options[myselect.selectedIndex].value;
            var specificPageUrl = url+"?limit="+limitValue+"&offset="+$offset;
            window.location = specificPageUrl;
        }

        function changeAction(){
            var myselect = document.getElementById("selectViewSize");
            var value = myselect.options[myselect.selectedIndex].value;
            var locationUrl = window.location.origin + window.location.pathname;
            window.location = locationUrl+"?"+"limit="+value+"&offset=0";

        }

        function changeActionBottom(){
            var myselect = document.getElementById("selectViewSize2");
            var value = myselect.options[myselect.selectedIndex].value;
            var locationUrl = window.location.origin + window.location.pathname;
            window.location = locationUrl+"?"+"limit="+value+"&offset=0";

        }

    </script>

</body>
</html>
<script>


</script>
