<!doctype html>
<!--[if IE 9 ]><html class="ie9" lang="en" ng-app="mallBd"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" ng-app="mallBd"><!--<![endif]-->

@include('web/partial/header/mainStartFacebook')
@include('web/partial/header/cartScript')
@include('web/partial/header/mainEnds')
<link href="{{asset('/template_resource/elevatezoom/jquery.fancybox.css')}}" rel="stylesheet">
<body ng-controller="CartController">
    @include('web/partial/loader')
    <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.7&appId=1648526795437995";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!--wide layout-->
<div class="wide_layout relative">
    <!--[if (lt IE 9) | IE 9]>
    <div style="background:#fff;padding:8px 0 10px;">
        <div class="container" style="width:1170px;">
            <div class="row wrapper">
                <div class="clearfix" style="padding:9px 0 0;float:left;width:83%;"><i
                        class="fa fa-exclamation-triangle scheme_color f_left m_right_10"
                        style="font-size:25px;color:#e74c3c;"></i><b style="color:#e74c3c;">Attention! This page may not
                    display correctly.</b> <b>You are using an outdated version of Internet Explorer. For a faster, safer
                    browsing experience.</b></div>
                <div class="t_align_r" style="float:left;width:16%;"><a
                        href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"
                        class="button_type_4 r_corners bg_scheme_color color_light d_inline_b t_align_c" target="_blank"
                        style="margin-bottom:2px;">Update Now!</a></div>
            </div>
        </div>
    </div>
    <![endif]-->
    <!--markup header-->
    <header role="banner" class="type_5 fixed-top">


        @include('web/partial/header/top')
                <!--header bottom part-->
        @include('web/partial/new_menu')

    </header>
    <div class="other-free-gap">

    </div>
    @include('web/partial/sideblock/shippingtext')
    <!--breadcrumbs-->
    <!--breadcrumbs-->
    <section class="breadcrumbs">
        <div class="container">
            <ul class="horizontal_list clearfix bc_list f_size_medium">
                <li class="m_right_10 current"><a href="{{url('home')}}" class="default_t_color">Home<i
                                class="fa fa-angle-right d_inline_middle m_left_10"></i></a></li>
                <li><a href="javascript:void(0)" class="default_t_color">{{ $package->packageTitle }}</a></li>
            </ul>
        </div>
    </section>
    <input type="hidden" id="packageJsonObj_{{$package->id}}" value="{{json_encode($package)}}"  />
    <!--content-->
    <div class="page_content_offset">
        <div class="container">
            <div class="row clearfix">
                <!--left content column-->
                <section class="col-lg-9 col-md-9 col-sm-9 m_xs_bottom_30">
                    <?php
                    if($package->id == 0){ ?>
                    <div class="row clearfix text-center">
                        <img src="{{asset('/template_resource/images/box.png')}}" />
                        <p class="error-not-found">Hmm,Something went wrong. No package found</p>
                      </div>
                    <?php }else{
                    ?>
                    <div class="clearfix m_bottom_30 t_xs_align_c">
                        <div class="photoframe type_2 shadow r_corners f_left f_sm_none d_xs_inline_b product_single_preview relative m_right_30 m_bottom_5 m_sm_bottom_20 m_xs_right_0 w_mxs_full" style="float: left;margin-right: 0px;">
                            <!--<span class="hot_stripe"><img src="{{asset('/template_resource/images/sale_product.png')}}" alt=""></span>-->
                            
                            
                            <div class="relative d_inline_b m_bottom_10 qv_preview d_xs_block " style="border:1px solid #eee">
                                <div class="so-tag">
                                                                    <span>hot</span>
                                </div>
                                <img id="package" src="{{ $imageUrl }}package/pagelarge/{{@$package->image}}" data-zoom-image="{{ $imageUrl }}package/large/{{@$package->image}}" onerror="this.src='http://placehold.it/360x360?text=Product Banner not found'"/>
                            </div>
                            <!--carousel-->
                            <div class="relative qv_carousel_wrap">
                                <button class="button_type_11 bg_light_color_1 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_single_prev">
                                    <i class="fa fa-angle-left "></i>
                                </button>

                                <div class="qv_carousel_single d_inline_middle" id="packagegallery">
                                    <a href="#" data-image="{{ $imageUrl }}package/pagelarge/{{@$package->image}}" onerror="this.src='http://placehold.it/360x360?text=Product Banner not found'" data-zoom-image="{{ $imageUrl }}package/large/{{@$package->image}}">
                                      <img id="package" src="{{ $imageUrl }}package/thumbnail/{{@$package->image}}" onerror="this.src='http://placehold.it/90x90?text=Product Banner not found'" />
                                    </a>
                                    @foreach($package->packageProduct as $products)
                                    <a href="#" data-image="{{ $imageUrl }}product/pagelarge/{{@$products->product->pictures[0]->name}}" onerror="this.src='http://placehold.it/360x360?text=Product Banner not found'" data-zoom-image="{{ $imageUrl }}product/large/{{@$products->product->pictures[0]->name}}">
                                      <img id="package" src="{{ $imageUrl }}product/thumbnail/{{@$products->product->pictures[0]->name}}" onerror="this.src='http://placehold.it/90x90?text=Product Banner not found'" />
                                    </a>
                                    @endforeach

                                  </div>
                                    
                                
                                <button class="button_type_11 bg_light_color_1 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_single_next">
                                    <i class="fa fa-angle-right "></i>
                                </button>
                            </div>
                        </div>
<!--                        <div class="p_top_10 t_xs_align_l  pop-desc" style="float: right;width: 360px;">
                            description
                            <h2 class="color_dark fw_medium m_bottom_10 pop-title">{{ $package->packageTitle }}</h2>

                            {{--<div class="m_bottom_10">
                                rating
                                <ul class="horizontal_list d_inline_middle type_2 clearfix rating_list tr_all_hover">
                                    <li class="active">
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                    <li class="active">
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                    <li class="active">
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                    <li class="active">
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                </ul>
                                <a href="#" class="d_inline_middle default_t_color f_size_small m_left_5">1 Review(s) </a>
                            </div>--}}
                            {{--<hr class="m_bottom_10 divider_type_3">
                            <table class="description_table m_bottom_10">
                                <tr>
                                    <td>Manufacturer:</td>
                                    <td><a href="#" class="color_dark">Chanel</a></td>
                                </tr>
                                <tr>
                                    <td>Availability:</td>
                                    <td><span class="color_green">in stock</span> 20 item(s)</td>
                                </tr>
                                <tr>
                                    <td>Product Code:</td>
                                    <td>PS06</td>
                                </tr>
                            </table>
                            <h5 class="fw_medium m_bottom_10">Product Dimensions and Weight</h5>
                            <table class="description_table m_bottom_5">
                                <tr>
                                    <td>Product Length:</td>
                                    <td><span class="color_dark">10.0000M</span></td>
                                </tr>
                                <tr>
                                    <td>Product Weight:</td>
                                    <td>10.0000KG</td>
                                </tr>
                            </table>--}}
                            <hr class="divider_type_3 m_bottom_10">
                            <p class="m_bottom_10"> {{@$package->description}}</p>
                            <hr class="divider_type_3 m_bottom_15">
                            <div class="m_bottom_15">
                                <s class="v_align_b f pop-old"><span><?php //echo $currency->HTMLCode; ?></span>{{ $package->originalPriceTotal }}</s><span
                                        class="v_align_b f_size_big m_left_5 scheme_color fw_medium pop-price"><span><?php //echo $currency->HTMLCode; ?></span>{{$package->packagePriceTotal}}</span>
                            </div>
                            {{--<table class="description_table type_2 m_bottom_15 pop-table">
                                <tr>
                                    <td class="v_align_m">Size:</td>
                                    <td class="v_align_m td-last">
                                        <div class="custom_select f_size_medium relative d_inline_middle">
                                            <div class="select_title r_corners relative color_dark">s</div>
                                            <ul class="select_list d_none"></ul>
                                            <select name="product_name">
                                                <option value="s">s</option>
                                                <option value="m">m</option>
                                                <option value="l">l</option>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="v_align_m">Quantity:</td>
                                    <td class="v_align_m td-last">
                                        <div class="clearfix quantity r_corners d_inline_middle f_size_medium color_dark">
                                            <button class="bg_tr d_block f_left" data-direction="down">-</button>
                                            <input type="text" name="" readonly value="1" class="f_left">
                                            <button class="bg_tr d_block f_left" data-direction="up">+</button>
                                        </div>
                                    </td>
                                </tr>
                            </table>--}}
                            <div class="pull-left">
                                <div class="d_ib_offset_0 m_bottom_20 position-relative">
                                <button class="button_type_12 r_corners bg_scheme_color color_light tr_delay_hover d_inline_b f_size_large add-busket" ng-click="addPackageToCart('packageJsonObj_{{$package->id}}',1,true)" >
                                    Add to Basket
                                </button>
                                <button class="button_type_12 bg_light_color_2 tr_delay_hover d_inline_b r_corners color_dark m_left_5 p_hr_0 relative compare-btn f_left" onclick="compare_package('{{$package->id}}');"><i class="fa fa-files-o" style="font-size: 22px;"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Compare</span></button>
                                {{--<button class="button_type_12 bg_light_color_2 tr_delay_hover d_inline_b r_corners color_dark m_left_5 p_hr_0">
                                    <span class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span><i
                                            class="fa fa-heart-o f_size_big"></i></button>--}}
                                {{--<button class="button_type_12 bg_light_color_2 tr_delay_hover d_inline_b r_corners color_dark m_left_5 p_hr_0">
                                    <span class="tooltip tr_all_hover r_corners color_dark f_size_small">Compare</span><i
                                            class="fa fa-files-o f_size_big"></i></button>--}}
                                <button class="button_type_12 bg_light_color_2 tr_delay_hover d_inline_b r_corners color_dark m_left_5 p_hr_0 relative ques-btn f_left">
                                    <i class="fa fa-question-circle f_size_big"></i><span
                                            class="tooltip tr_all_hover r_corners color_dark f_size_small">Ask a Question</span></button>
                            </div>
                            <p class="d_inline_middle" style="display: none;">Share this:</p>

                            <div class="d_inline_middle m_left_5 addthis_widget_container" style="display: none;">
                                 AddThis Button BEGIN 
                                <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
                                    <a class="addthis_button_preferred_1"></a>
                                    <a class="addthis_button_preferred_2"></a>
                                    <a class="addthis_button_preferred_3"></a>
                                    <a class="addthis_button_preferred_4"></a>
                                    <a class="addthis_button_compact"></a>
                                    <a class="addthis_counter addthis_bubble_style"></a>
                                </div>
                                 AddThis Button END 
                            </div>
                            </div>
                        </div>-->
                        <div class="p_top_10 t_xs_align_l pop-desc" style="float: right; width: 360px;">
                            <p class="manf-name"><a href="">{{$package->packageTitle}}</a></p>
                            @foreach($package->packageProduct as $products)
                                <p class="item-number" style="margin-bottom: 0px;"><a href="{{url('/product/'. $products->product->url . '/' . $products->product->code)}}" target="_blank" class="color_dark">{{@$products->product->title}}</a></p>
                            
                            @endforeach 
                            
                            <p class="item-number">item:#package-00{{$package->id}}</p>
                            <p style="color:red;">Your Save<span><?php echo $currency->HTMLCode; ?> {{ number_format($package->originalPriceTotal-$package->packagePriceTotal,2)}} </span></p>
                            <p class="detail-price">
                                <span class="older"><?php echo $currency->HTMLCode; ?> {{ number_format($package->originalPriceTotal,2)}}</span>
                                <span class="newer"><?php echo $currency->HTMLCode; ?> {{ number_format($package->packagePriceTotal,2)}}</span>
                                
                            </p>
                            <p class="detail-desc"><?php echo $package->description;?></p>
                            
                            
                            
                            <p class="desc-other-block">
                                <a class="wishlist-link" href="#" onclick="compare_package('{{$package->id}}');"><i class="fa fa-copy"></i>Compare Package</a>
                            </p>
                            <div class="add-qn">
                                <div class="col-md-4">
                                    <select class="qnty q_selector">
                                        <option>Qty- 1</option>
                                    </select>
                                </div>
                                <div class="col-md-8">
                                    <button class="btn-new-ad" ng-click="addPackageToCart('packageJsonObj_{{$package->id}}',1,true)">Add to basket</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--tabs-->
                    <div class="tabs m_bottom_45">
                        <!--tabs navigation-->
                        <nav>
                            <ul class="tabs_nav horizontal_list clearfix">
                                <li class="f_xs_none"><a href="#tab-1" class="bg_light_color_1 color_dark tr_delay_hover r_corners d_block">Comments</a>
                                </li>
                                <li class="f_xs_none"><a href="#tab-2" class="bg_light_color_1 color_dark tr_delay_hover r_corners d_block">Package Products</a>
                                </li>
                            </ul>
                        </nav>
                        <section class="tabs_content shadow r_corners">
                            <div id="tab-1">
                                <div class="fb-comments" data-href="{{url('/packages/'.urlencode($package->packageTitle).'/'.$package->id)}}" data-numposts="5"></div>
                                
                            </div>
                            <div id="tab-2 other-package-mb">
                                 @foreach($package->packageProduct as $products)
                                 <div class="package-items row clearfix">
                                    <div class="product_item full_width list_type hit m_left_0 m_right_0">
                                        <figure class="r_corners photoframe tr_all_hover type_2 shadow relative clearfix mb-frame" style="min-height:110px !important;">
                                            <!--product preview-->
                                            
                                                <a href="" class="d_block f_left relative pp_wrap m_right_30 m_sm_right_20 m_xs_right_25 op-img">
                                                <img src="{{ $imageUrl }}product/thumbnail/{{@$products->product->pictures[0]->name}}" alt="" onerror="this.src='http://placehold.it/90x90?text=Product Banner not found'" class="tr_all_hover" alt="">
                                                    <span role="button" data-popup="#quick_view_product_{{$products->product->id}}" class="button_type_5 box_s_none color_light r_corners tr_all_hover d_xs_none">Quick View</span>
                                                </a>
                                            
                                            <!--description and price of product-->
                                            <figcaption class="fix-pa-detail">
                                                <div class="clearfix">
                                                    <div class="f_left p_list_description f_sm_none w_sm_full m_xs_bottom_10 detail-block" >
                                                        <p class="manf-name"><a href="" onclick="productByManufacturer('{{$products->product->manufacturer->id}}');">{{$products->product->manufacturer->name}}</a></p><br>
                                                        <h4 class="fw_medium"><a href="{{url('/product/'. $products->product->url . '/' . $products->product->code)}}" target="_blank" class="color_dark">{{@$products->product->title}}</a></h4>
                                                        <p class="item-number">item:#{{$products->product->code}}</p>    
                                                    </div>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </div>
                                </div>
                                 @endforeach                             
                            </div>

                        </section>
                    </div>
                    <p style="color: #b50709;    font-size: 30px;    margin: 0 0 20px 15px;">Package Products Details</p>
                    <?php
                $countPic =0;
                ?>
                    @foreach($package->packageProduct as $products)
                                    
                                <div class="package-items row clearfix other-package-mb">
                                    <div class="product_item full_width list_type hit m_left_0 m_right_0">
                                        <figure class="r_corners photoframe tr_all_hover type_2 shadow relative clearfix mb-frame" style="min-height:180px !important;">
                                            <!--product preview-->
                                            

                                                <a href="" class="d_block f_left relative pp_wrap m_right_30 m_sm_right_20 m_xs_right_25 op-img">
                                                    @foreach(@$products->product->pictures as $picture)
                                                            @if($picture->cover==1)
                                                                     <img src="{{ $imageUrl }}product/thumbnail/{{@$picture->name}}" alt="" onerror="this.src='http://placehold.it/90x90?text=Product Banner not found'" class="tr_all_hover" alt="">
                                                              @endif
                                                      @endforeach
                                                   
                                                        <span role="button" data-popup="#quick_view_product_{{$products->product->id}}" class="button_type_5 box_s_none color_light r_corners tr_all_hover d_xs_none">Quick View</span>
                                                </a>
                                            
                                            <!--description and price of product-->
                                            <figcaption class="fix-pa-detail">
                                                <div class="clearfix">
                                                    <div class="f_left p_list_description f_sm_none w_sm_full m_xs_bottom_10 detail-block" >
                                                        <h4 class="fw_medium"><a href="{{url('/product/'. $products->product->url . '/' . $products->product->code)}}" target="_blank" class="color_dark">{{@$products->product->title}}</a></h4>

                                                        <div class="m_bottom_10">
                                                            <!--rating-->
                                                        </div>
                                                        <hr class="m_bottom_10">
                                                        <p class="d_sm_none d_xs_block"><?php print $products->product->description; ?></p>
                                                    </div>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </div>
                                </div>
                                <div class="popup_wrap d_none" id="quick_view_product_{{$products->product->id}}" >
                                            <section class="popup r_corners shadow">
                                                <button class="bg_tr color_dark tr_all_hover text_cs_hover close f_size_large"><i class="fa fa-times"></i></button>
                                                <div class="clearfix">
                                                    <div class="custom_scrollbar">
                                                        <!--left popup column-->
                                                        <div class="f_left half_column">

                                                            <div class="relative d_inline_b m_bottom_10 qv_preview">
                                                                <?php
                                                                    if($products->product->quantity <= 0){ ?>
                                                                    <div class="o-tag">
                                                                                <span>hot</span>
                                                                     </div>
                                                                    <?php                                                        
                                                                    }else{                                                           
                                                                        if (($products->product->isFeatured)) {
                                                                            ?>
                                                                            <div class="f-tag">
                                                                                <span>hot</span>
                                                                            </div>
                                                                            <?php
                                                                        } else if ($products->product->discountActiveFlag) {
                                                                            ?>
                                                                            <div class="s-tag">
                                                                                <span>Sale</span>
                                                                            </div>
                                                                            <?php
                                                                        } else if ($products->product->isBestSeller) {
                                                                            ?>
                                                                            <div class="b-tag">
                                                                                <span>Best Seller</span>
                                                                            </div>
                                                                        <?php } else { if($products->product->createdOn >$filterDate){ ?>
                                                                            <div class="h-tag">
                                                                                <span>New</span>
                                                                            </div>
                                                                        <?php
                                                                        }
                                                                    } 

                                                                    } 
                                                                    ?>
                                                                {{--<span class="hot_stripe"><img src="{{asset('/template_resource/images/sale_product.png')}}" alt=""></span>--}}
                                                                @if(@$products->product->pictures == NULL)
                                                                        <img id="zoom_image"src="http://placehold.it/360x360?text=No Image Found">
                                                                 @endif
                                                                @foreach(@$products->product->pictures as $picture)
                                                                    @if($picture->cover==1)
                                                                        <?php $countPic++;?>
                                                                        <img id="zoom_image{{$countPic}}" src="{{$imageUrl.'product/pagelarge/'.$picture->name}}" class="tr_all_hover" alt="" onerror="this.src='http://placehold.it/360x360?text=No Image Found'">
                                                                     @endif

                                                                @endforeach
                                                            </div>
                                                            <!--carousel-->

                                                            <div class="relative qv_carousel_wrap m_bottom_20">
                                                                <button class="button_type_11 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_prev">
                                                                    <i class="fa fa-angle-left "></i>
                                                                </button>
                                                                <ul class="qv_carousel d_inline_middle">
                                                                    @foreach(@$products->product->pictures as $picture)
                                                                        <li><img id="small-image{{$picture->name}}" src="{{$imageUrl.'product/thumbnail/'.$picture->name}}" onerror="this.src='http://placehold.it/90x90?text=No Image Found'" alt="" onclick="changePicture('{{$picture->name}}', '{{ $countPic}}')"></li>
                                                                    @endforeach
                                                                </ul>
                                                                <button class="button_type_11 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_next">
                                                                    <i class="fa fa-angle-right "></i>
                                                                </button>
                                                            </div>

                                                            <div class="d_inline_middle" style="display: none;">Share this:</div>
                                                            <div class="d_inline_middle m_left_5" style="display: none;">
                                                                <!-- AddThis Button BEGIN -->
                                                                <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
                                                                    <a class="addthis_button_preferred_1"></a>
                                                                    <a class="addthis_button_preferred_2"></a>
                                                                    <a class="addthis_button_preferred_3"></a>
                                                                    <a class="addthis_button_preferred_4"></a>
                                                                    <a class="addthis_button_compact"></a>
                                                                    <a class="addthis_counter addthis_bubble_style"></a>
                                                                </div>
                                                                <!-- AddThis Button END -->
                                                            </div>
                                                        </div>
                                                        <!--right popup column-->
                                                        
                                                        <!--right popup column-->
                                                        <div class="p_top_10 t_xs_align_l pop-desc" style="float: right; width: 360px;">
                                                            <p class="manf-name"><a href="">{{$products->product->manufacturer->name}}</a></p>
                                                            <p class="prod-name-detail">{{$products->product->title}}</p>
                                                            <p class="item-number">item:#{{$products->product->code}}</p>
                                                            <p class="detail-price">
                                                                @if($products->product->discountActiveFlag)
                                                                <span class="older"><?php echo $currency->HTMLCode; ?> {{ number_format($products->product->prices[0]->retailPrice,2)}}</span><span class="newer"><?php echo $currency->HTMLCode; ?> {{ number_format(($products->product->prices[0]->retailPrice-$products->product->discountAmount),2)}}</span>
                                                                @else
                                                                <span class="newer"><?php echo $currency->HTMLCode; ?> {{ number_format($products->product->prices[0]->retailPrice,2)}}</span>
                                                                @endif

                                                            </p>
                                                            <span class="detail-desc"><?php echo $products->product->description; ?></span>
                                                            <input type="hidden" id="productJsonObj_{{$products->product->id}}" value="{{json_encode($products->product)}}"  />
                                                            <p class="rate-it">
                                                                <ul class="horizontal_list d_inline_middle type_2 clearfix rating_list tr_all_hover">
                                                                    <?php
                                                                    $avgRating = ceil($products->product->avgRating);
                                                                    if($avgRating>5)
                                                                    {
                                                                        $avgRating=5;
                                                                    }

                                                                    $left = 5-$avgRating;
                                                                    ?>
                                                                    @for($i=0;$i<$avgRating;$i++)
                                                                        <li class="active">
                                                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                                                            <i class="fa fa-star active tr_all_hover"></i>
                                                                        </li>
                                                                    @endfor
                                                                    @if($left>0)
                                                                        @for($i=0;$i<$left;$i++)
                                                                            <li>
                                                                                <i class="fa fa-star-o empty tr_all_hover"></i>
                                                                                <i class="fa fa-star active tr_all_hover"></i>
                                                                            </li>
                                                                        @endfor
                                                                    @endif
                                                                    <li>
                                                                        <span class="rate_number">{{$avgRating}}</span>
                                                                    </li>

                                                                </ul>                                                

                                                             </p>

                                                            <p class="desc-other-block">


                                                                @if($products->product->isWished)
                                                                          <a class="favourite-link" href="" onclick="submitWishListforproductdetails({{$products->product->id}})"><i class="fa fa-heart"></i>Add to favorites</a>              
                                                                 @else
                                                                    <a class="favourite-link" href="" onclick="submitWishListforproductdetails('{{$products->product->id}}','quick_view_product_{{$products->product->id}}')"><i class="fa fa-heart-o"></i>Add to favorites</a>    
                                                                @endif
                                                                <a class="wishlist-link" href="#" onclick="compare_product('{{$products->product->id}}');"><i class="fa fa-copy"></i>Compare Product</a>

                                                            </p>
                                                            <div class="notify-small" id="outeraddnotify{{ $products->product->id }}"  hidden><span>Product Added</span></div>
                                                 <div class="notify-small" id="outeraddnotify2{{ $products->product->id }}"  hidden><span>Already Added</span></div>
                                                            @foreach(@$products->product->attributes as $attributes)
                                                       <div class="add-qn" style="margin-bottom: 25px;">
                                                            <div class="col-md-4">
                                                                {{$attributes->name}}:
                                                            </div>
                                                            <div class="col-md-8">
                                                               <select class="qnty" style="width: 100%;" name="product_name" id="select_attribute_{{$products->product->id}}_{{$attributes->id}}">
                                                                        @foreach(@$attributes->attributesValue as $attributesValue)
                                                                        <option value="{{@$attributesValue->id}}">{{@$attributesValue->value}}</option>
                                                                        @endforeach
                                                               </select>
                                                            </div>
                                                    
                                                        </div>
                                                            @endforeach
                                                 <div class="add-qn">
                                                    <?php if($products->product->quantity <= 0){ ?>
                                                    <!--<button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light f_left mw_0" >Out of Stock</button>-->

                                                            <div class="col-md-12">
                                                                <b style="color: red;font-size: 25px;">Out of Stock</b>
                                                            </div>
                                                        <?php }else{ ?>
                                                        <div class="col-md-4">
                                                                <select class="qnty" id="quantity_modifier_{{$products->product->id}}">                                        
                                                                   @for($i=1;$i<=$products->product->quantity;$i++)
                                                                    <option value="{{$i}}">Qty- {{$i}}</option>
                                                                    @endfor

                                                                </select>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <button class="btn-new-ad" ng-click="addToCart('productJsonObj_{{$products->product->id}}',-1,true)">Add to basket</button>
                                                            </div>
                                                    <?php } ?>
                                                    
                                                </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                    
                                @endforeach
                    {{--<div class="clearfix">
                        <h2 class="color_dark tt_uppercase f_left m_bottom_15 f_mxs_none">Related Products</h2>

                        <div class="f_right clearfix nav_buttons_wrap f_mxs_none m_mxs_bottom_5">
                            <button class="button_type_7 bg_cs_hover box_s_none f_size_ex_large t_align_c bg_light_color_1 f_left tr_delay_hover r_corners rp_prev">
                                <i class="fa fa-angle-left"></i></button>
                            <button class="button_type_7 bg_cs_hover box_s_none f_size_ex_large t_align_c bg_light_color_1 f_left m_left_5 tr_delay_hover r_corners rp_next">
                                <i class="fa fa-angle-right"></i></button>
                        </div>
                    </div>--}}
                    {{--<div class="related_projects m_bottom_15 m_sm_bottom_0 m_xs_bottom_15">
                        <figure class="r_corners photoframe shadow relative d_xs_inline_b tr_all_hover">
                            <!--product preview-->
                            <a href="#" class="d_block relative pp_wrap">
                                <!--hot product-->
                                <span class="hot_stripe type_2"><img src="{{asset('/template_resource/images/hot_product_type_2.png')}}" alt=""></span>
                                <img src="{{asset('/template_resource/images/product_img_5.jpg')}}" class="tr_all_hover" alt="">
        <span data-popup="#quick_view_product"
              class="t_md_align_c button_type_5 box_s_none color_light r_corners tr_all_hover d_xs_none">Quick View</span>
                            </a>
                            <!--description and price of product-->
                            <figcaption class="t_xs_align_l">
                                <h5 class="m_bottom_10"><a href="#" class="color_dark ellipsis">Eget elementum vel</a></h5>

                                <div class="clearfix">
                                    <p class="scheme_color f_left f_size_large m_bottom_15">$102.00</p>
                                    <!--rating-->
                                    <ul class="horizontal_list f_right clearfix rating_list tr_all_hover">
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                    </ul>
                                </div>
                                <button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light mw_0" >Add to Basket</button>
                            </figcaption>
                        </figure>
                        <figure class="r_corners photoframe shadow relative d_xs_inline_b tr_all_hover">
                            <!--product preview-->
                            <a href="#" class="d_block relative pp_wrap">
                                <img src="{{asset('/template_resource/images/product_img_7.jpg')}}" class="tr_all_hover" alt="">
        <span data-popup="#quick_view_product"
              class="t_md_align_c button_type_5 box_s_none color_light r_corners tr_all_hover d_xs_none">Quick View</span>
                            </a>
                            <!--description and price of product-->
                            <figcaption class="t_xs_align_l">
                                <h5 class="m_bottom_10"><a href="#" class="color_dark ellipsis">Cursus eleifend elit aenean elit aenean</a></h5>

                                <div class="clearfix">
                                    <p class="scheme_color f_left f_size_large m_bottom_15">$99.00</p>
                                    <!--rating-->
                                    <ul class="horizontal_list f_right clearfix rating_list tr_all_hover">
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                    </ul>
                                </div>
                                <button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light mw_0">Add to Basket</button>
                            </figcaption>
                        </figure>
                        <figure class="r_corners photoframe shadow relative d_xs_inline_b tr_all_hover">
                            <!--product preview-->
                            <a href="#" class="d_block relative pp_wrap">
                                <!--sale product-->
                                <span class="hot_stripe type_2"><img src="{{asset('/template_resource/images/sale_product_type_2.png')}}" alt=""></span>
                                <img src="{{asset('/template_resource/images/product_img_8.jpg')}}" class="tr_all_hover" alt="">
        <span data-popup="#quick_view_product"
              class="t_md_align_c button_type_5 box_s_none color_light r_corners tr_all_hover d_xs_none">Quick View</span>
                            </a>
                            <!--description and price of product-->
                            <figcaption class="t_xs_align_l">
                                <h5 class="m_bottom_10"><a href="#" class="color_dark ellipsis">Aliquam erat volutpat</a></h5>

                                <div class="clearfix">
                                    <p class="scheme_color f_left f_size_large m_bottom_15">$36.00</p>
                                    <!--rating-->
                                    <ul class="horizontal_list f_right clearfix rating_list tr_all_hover">
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                    </ul>
                                </div>
                                <button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light mw_0">Add to Basket</button>
                            </figcaption>
                        </figure>
                        <figure class="r_corners photoframe shadow relative d_xs_inline_b tr_all_hover">
                            <!--product preview-->
                            <a href="#" class="d_block relative pp_wrap">
                                <!--hot product-->
                                <span class="hot_stripe type_2"><img src="{{asset('/template_resource/images/hot_product_type_2.png')}}" alt=""></span>
                                <img src="{{asset('/template_resource/images/product_img_3.jpg')}}" class="tr_all_hover" alt="">
        <span data-popup="#quick_view_product"
              class="t_md_align_c button_type_5 box_s_none color_light r_corners tr_all_hover d_xs_none">Quick View</span>
                            </a>
                            <!--description and price of product-->
                            <figcaption class="t_xs_align_l">
                                <h5 class="m_bottom_10"><a href="#" class="color_dark ellipsis">Eget elementum vel</a></h5>

                                <div class="clearfix">
                                    <p class="scheme_color f_left f_size_large m_bottom_15">$102.00</p>
                                    <!--rating-->
                                    <ul class="horizontal_list f_right clearfix rating_list tr_all_hover">
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                    </ul>
                                </div>
                                <button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light mw_0">Add to Basket</button>
                            </figcaption>
                        </figure>
                        <figure class="r_corners photoframe shadow relative d_xs_inline_b tr_all_hover">
                            <!--product preview-->
                            <a href="#" class="d_block relative pp_wrap">
                                <img src="{{asset('/template_resource/images/product_img_1.jpg')}}" class="tr_all_hover" alt="">
        <span data-popup="#quick_view_product"
              class="t_md_align_c button_type_5 box_s_none color_light r_corners tr_all_hover d_xs_none">Quick View</span>
                            </a>
                            <!--description and price of product-->
                            <figcaption class="t_xs_align_l">
                                <h5 class="m_bottom_10"><a href="#" class="color_dark ellipsis">Cursus eleifend elit aenean...</a></h5>

                                <div class="clearfix">
                                    <p class="scheme_color f_left f_size_large m_bottom_15">$99.00</p>
                                    <!--rating-->
                                    <ul class="horizontal_list f_right clearfix rating_list tr_all_hover">
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                    </ul>
                                </div>
                                <button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light mw_0">Add to Basket</button>
                            </figcaption>
                        </figure>
                        <figure class="r_corners photoframe shadow relative d_xs_inline_b tr_all_hover">
                            <!--product preview-->
                            <a href="#" class="d_block relative pp_wrap">
                                <!--sale product-->
                                <span class="hot_stripe type_2"><img src="{{asset('/template_resource/images/sale_product_type_2.png')}}" alt=""></span>
                                <img src="{{asset('/template_resource/images/product_img_9.jpg')}}" class="tr_all_hover" alt="">
        <span data-popup="#quick_view_product"
              class="t_md_align_c button_type_5 box_s_none color_light r_corners tr_all_hover d_xs_none">Quick View</span>
                            </a>
                            <!--description and price of product-->
                            <figcaption class="t_xs_align_l">
                                <h5 class="m_bottom_10"><a href="#" class="color_dark ellipsis">Aliquam erat volutpat</a></h5>

                                <div class="clearfix">
                                    <p class="scheme_color f_left f_size_large m_bottom_15">$36.00</p>
                                    <!--rating-->
                                    <ul class="horizontal_list f_right clearfix rating_list tr_all_hover">
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li class="active">
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star-o empty tr_all_hover"></i>
                                            <i class="fa fa-star active tr_all_hover"></i>
                                        </li>
                                    </ul>
                                </div>
                                <button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light mw_0">Add to Basket</button>
                            </figcaption>
                        </figure>
                    </div>--}}

                    {{--<hr class="divider_type_3 m_bottom_15">
                    <a href="category_grid.html" role="button"
                       class="d_inline_b bg_light_color_2 color_dark tr_all_hover button_type_4 r_corners"><i
                                class="fa fa-reply m_left_5 m_right_10 f_size_large"></i>Back to: Woman</a>--}}
                                
                    <?php } ?>
                </section>
                <!--right column-->
                <aside class="col-lg-3 col-md-3 col-sm-3">
                   
                    <!--compare products-->
                    <a href="#" class="d_block r_corners m_bottom_30 hidden-xs ">
                        <img src="{{asset('/template_resource/images/banner_img_6.jpg')}}" alt="">
                    </a>
                    <!--banner-->
<!--                    <a href="#" class="d_block r_corners m_bottom_30">
                        <img src="images/banner_img_6.jpg" alt="">
                    </a>-->
                    <!--Bestsellers-->
                    
                    @include('web/partial/sideblock/bestsellers')
                    <!--tags-->
                    <a href="#" class="d_block r_corners m_bottom_30 hidden-xs ">
                        <img src="{{asset('/template_resource/images/banner_img_6.jpg')}}" alt="">
                    </a>
                    @include('web/partial/sideblock/tags')
                </aside>
            </div>
        </div>
    </div>
    <!--markup footer-->
    @include('web/partial/footer/newbottom')
</div>
<!--social widgets-->
@include('web/partial/social_widgets/main')

<!--login popup-->

<button class="t_align_c r_corners tr_all_hover type_2 animate_ftl" id="go_to_top"><i class="fa fa-angle-up"></i>
</button>

<!--scripts include-->
@include('web/partial/script/core')


<!--<script src="{{asset('/template_resource/js/jquery.fancybox-1.3.4.js')}}"></script>-->
<script src="{{asset('/template_resource/js/scripts.js')}}"></script>
<!--<script src="{{asset('/template_resource/js/elevatezoom.min.js')}}"></script>-->
<script src="{{asset('/template_resource/elevatezoom/jquery.elevatezoom.js')}}"></script>
<script src="{{asset('/template_resource/elevatezoom/jquery.mousewheel-3.0.6.pack.js')}}"></script>
<script src="{{asset('/template_resource/elevatezoom/jquery.fancybox.js')}}"></script>

<script>
 //initiate the plugin and pass the id of the div containing gallery images
$("#package").elevateZoom({gallery:'packagegallery', cursor: 'pointer', galleryActiveClass: 'active', imageCrossfade: true, loadingIcon: 'http://www.elevateweb.co.uk/spinner.gif'}); 

//pass the images to Fancybox
$("#package").bind("click", function(e) {  
  var ez =   $('#package').data('elevateZoom');	
	$.fancybox(ez.getGalleryList());
  return false;
});
</script>


<script>

    function getReviewPoint(){
        return $('#reviewPoint').find("li.active").length;
    }

    function submitReview(productId)
    {
        $("#notification").html("processing...");

        $.ajax({

            url: $("#baseUrl").val() + "api/customer/review/add",
            method: "POST",
            data: {
                "product_id": productId,
                "note":$('#note').val(),
                "rating": getReviewPoint()
            },
            success: function (data) {
                if (data.responseStat.status) {
                    $("#notification").html(data.responseStat.msg);
                    $("#notification").show();
                    $("#notification").fadeOut(3000);
                    $('#note').val("");
                    $('#reviewLoader').show();
                    $.ajax({

                        url: $("#baseUrl").val() + "review/get"+
                        "?product_id="+$('#productId').html(),
                        method: "POST",
                        data: {
                        },
                        success: function (data) {

                            $('#reviewLoader').hide();
                            $('#reviews').html(data);
                        }

                    });
                    console.log(data);

                }
                else {
                    //$("#errmsg").html(data.responseStat.msg);
                    $("#notification").html(data.responseStat.msg);
                    $("#notification").show();
                    $("#notification").fadeOut(3000,function(){
                        showLoginForm("");
                    });
                    console.log(data);
                }

            }

        });
    }

    $(document).ready(function() {

        $('#reviewLoader').show();
        $.ajax({

            url: $("#baseUrl").val() + "review/get"+
            "?product_id="+$('#productId').html(),
            method: "POST",
            data: {
            },
            success: function (data) {

                $('#reviewLoader').hide();
                $('#reviews').html(data);
            }

        });
    });

    function changePicture(picName)
    {
        srcOfZoom = $('#zoom_image').attr('src');
        parts = srcOfZoom.split("/");

        if(picName == "<?php echo $package->image; ?>")
        {
            newSource = (parts[0]+"/"+parts[1]+"/"+parts[2]+"/"+parts[3]+"/"+parts[4]+"/"+parts[5]+"/"+"package"+"/"+parts[7]+"/"+picName);

        }else{
            newSource = (parts[0]+"/"+parts[1]+"/"+parts[2]+"/"+parts[3]+"/"+parts[4]+"/"+parts[5]+"/"+"product"+"/"+parts[7]+"/"+picName);
        }

        $("#zoom_image").attr("src",newSource);


    }








</script>
</body>
</html>