<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="Walletmix Payment Gateway" />
        <meta name="author" content="" />

        <title>Bkash  - Payment</title>
        <link rel="icon" type="image/ico" href="{{asset('/template_resource/images/fav.ico')}}">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">

        <!-- card-selection -->
        <link rel="stylesheet" type="text/css" media="all" href="{{asset('/template_resource/bkash/bootstrap.css')}}">

        <link rel="stylesheet" type="text/css" media="all" href="{{asset('/template_resource/bkash/card_selection_page.css')}}">
        <link rel="stylesheet" type="text/css" media="all" href="{{asset('/template_resource/bkash/style.css')}}">
        <link rel="stylesheet" type="text/css" media="all" href="{{asset('/template_resource/bkash/card_selection_responsive.css')}}">
        <!-- /-card-selection -->

        <!-- Jquery -->
        <script src="{{asset('/template_resource/bkash/jquery-2.1.4.min.js')}}"></script>
        
    <script type="text/javascript" src="{{asset('/developer/angular/corelib/angular.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/developer/angular/corelib/lazy-scroll.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('developer/angular/helper/HtmlManipulationModel.js')}}"></script>
    <script type="text/javascript" src="{{asset('developer/angular/datamodel/datamodel.js')}}"></script>
    <script type="text/javascript" src="{{asset('developer/angular/controllers/cart/main.js')}}"></script>
       
    </head>
    <body>
        <div id="wrap">
            <div class="container">
                <?php if(isset($orderDetails) && sizeof($orderDetails) == 0){ ?>
                <div class="row">
                    <h1> Wrong URL or Some information missing & Please Try again </h1>
                </div>
                <?php }else{ ?>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="row">
                            <div class="col-md-12 card_selection_container">
                                <div class="row">
                                    <div class="aligncenter purchase-button col-md-12">
                                        <a class="dt-sc-button medium with-icon" href="#"> <i class="fa fa-shopping-cart"></i> Bkash Payment Gateway </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="customer_info " style="width: 100%;margin:10px auto;">
                                            <section class="module">
                                                <div class="module-head">
                                                    <b>Customer Details </b>
                                                </div>
                                                <div class="module-body" style="margin:10px auto; height:auto;">
                                                    <div class="row">
                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                                            <p>Customer Name </p>
                                                        </div>
                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                                            <p><?php echo $orderDetails[0]['orderCustomer']['firstname']." ".$orderDetails[0]['orderCustomer']['lastname']; ?></p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                                            <p>Shipping Address</p>
                                                        </div>
                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                                            <p><?php echo $orderDetails[0]['orderCustomer']['userDetails']['address']; ?></p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                                            <p>Merchant </p>
                                                        </div>
                                                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                                            <p align="center">http://themallbd.com/</p>
                                                        </div>
                                                    </div>
<!--                                                    <div class="row">
                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                                            <p>Amount </p>
                                                        </div>
                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                                            <p><?php //echo $grand_total; ?> BDT </p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                                            <p>Charge </p>
                                                        </div>
                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                                            <p><?php //echo $bkash_charge; ?> BDT </p>
                                                        </div>
                                                    </div>-->
                                                    <hr>
                                                    <div class="row">
                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                                            <p>Total Pay Amount </p>
                                                        </div>
                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                                            <p><?php echo $grand_total; ?> BDT </p>
                                                        </div>
                                                    </div>

                                                </div>
                                            </section>
                                        </div>				</div>
                                    <div class="col-md-8">
                                        <script src="{{asset('/template_resource/bkash/bkash.js')}}"></script>


                                        <style type="text/css">

                                            .form-group p {
                                                margin: 0;
                                            }

                                            #main-container{
                                                padding: 20px 50px 0 50px;
                                            }

                                            @media (max-width: 767px) {
                                                #main-container{
                                                    padding: 20px 20px 0 20px;
                                                }
                                            }


                                            .pagination {
                                                margin-top: 10px;
                                                margin-bottom: 0;
                                            }



                                            .ajax-loader {
                                                display: none;
                                            }



                                            /* Forms part */
                                            .form-horizontal .form-group {
                                                margin-left: 0;
                                                margin-right: 0;
                                            }

                                            .form-control,
                                            .uneditable-input {
                                                font-size: 13px;
                                                margin-bottom: 0;
                                                border-radius: 2px;
                                                height: 38px;
                                            }

                                            .input-group-addon {
                                                padding: 5px 12px;
                                            }

                                            .add-input,
                                            .remove-input {
                                                cursor: pointer;
                                            }

                                            /** LABEL **/
                                            .label {
                                                float: left;
                                                margin-top: 5px;
                                                font-size: 90%;
                                                border-radius: 2px;
                                            }

                                            .label-important {
                                                display: table;
                                            }

                                            /** Module part **/
                                            .module {
                                                border: 1px solid #d2d4d8;
                                                border-radius: 3px;
                                                margin-bottom: 15px;
                                            }

                                            .module .module-head {
                                                background: #f7f8f9;
                                                border-bottom: 1px solid #d9d9dd;
                                                border-radius: 2px 2px 0 0;
                                                height: 40px;
                                            }

                                            .module .module-head > a,
                                            .module .module-head > b {
                                                color: #727476;
                                                display: inline-block;
                                                font-weight: 500;
                                                padding: 10px 15px;
                                            }

                                            .module .module-head a {
                                                color: inherit;
                                                text-decoration: none;
                                            }

                                            .module .module-head i {
                                                font-size: 14px;
                                                color: #666;
                                                display: inline-block;
                                                line-height: 20px;
                                            }

                                            .module .module-body {
                                                background: #fff;
                                                padding: 15px;
                                                border-radius: 0 0 2px 2px;
                                            }

                                            .module .module-body .upper-menu {
                                                margin: 0 0 10px 0;
                                            }

                                            .module .module-body [class*="span"] {
                                                margin-left: 0px;
                                            }

                                        </style>
                                        <div class="customer_info" style="width: 100%;margin:10px auto;">
                                            <section class="module">
                                                <div class="module-head">
                                                    <b>bKash Payment </b>
                                                </div>
                                                <div style="margin:10px auto; height:auto;" class="module-body">
                                                    <section class="module">
                                                        <div class="module-head">
                                                            <b>bKash Instruction </b> 
                                                        </div>
                                                        <div style="margin:10px auto; height:auto;" class="module-body">
                                                            <div class="row">
                                                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                    <div class="row">
                                                                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                            <p>Step : 1</p>
                                                                        </div>
                                                                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                                            <p>Dial *247#</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-4">
                                                                            <p>Step : 2</p>
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                            <p>Choose Option: "Payment"</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-4">
                                                                            <p>Step : 3</p>
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                            <p>Enter Merchant bKash Account No: 01710300648</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-4">
                                                                            <p>Step : 4</p>
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                            <p>Enter Amount: <?php echo $total_pay; ?>	</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                    <div class="row">
                                                                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                            <p>Step : 5</p>
                                                                        </div>
                                                                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                                            <p>Enter Reference: 1</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-4">
                                                                            <p>Step : 6</p>
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                            <p>Enter Counter No: 1</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-4">
                                                                            <p>Step : 7</p>
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                            <p>Enter Your Pin to Confirm the Transaction: XXXX</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-4">
                                                                            <p>Step : 8</p>
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                            <p>Enter Your Mobile Number and TrxID to complete your Transaction.</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                    <input type="hidden" id="baseUrl" value="{{url('/')}}/" />
                                                    <span style="color: red; font-weight: bold" id="error_show"></span>
                                                    <form method="POST" action="http://sandbox.walletmix.com/bank-data-process" accept-charset="UTF-8" id="gateway_bkash_card" class="form-horizontal form-groups-bordered">
                                                        
                                                        <div class="row">
                                                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <label for="mobileNumber" class="control-label">Sender Mobile Number</label>
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">+88</span>
                                                                        <input type="text" autocomplete="off" maxlength="11" required="" placeholder="01xxxxxxxxx" class="form-control" name="mobileNumber" id="mobileNumber" >	
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <label for="transactionId" class="control-label">TrxID</label><br>
                                                                    <input type="text" autocomplete="off" maxlength="10" required="" placeholder="TrxID" class="form-control" name="transactionId" id="transactionId">	
                                                                </div>
                                                            </div>
                                                        </div>  
                                                        <input name="unique_code" type="hidden" value="<?php echo $orderDetails[0]['unique_code']; ?>">

                                                        <div class="row">
                                                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <div class="input-group">
                                                                    <input class="btn btn-success" id="bKashBtn" type="submit" value="Pay Now">
                                                                    <input class="btn btn-danger" onclick="returntostore(<?php echo $orderDetails[0]['id']; ?>);" type="button" value="Back to Store">
                                                                    <div style="display: none;float:right;margin-top:-15px;" class="ajax-loader">
                                                                        <img src="http://sandbox.walletmix.com/images/bKash_loader.gif" alt="ajax-loader">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </section>
                                        </div>								</div>
                                </div>
                            </div>
                            <div class="aligncenter purchase-button col-md-12">
                                <a style="font-weight:normal;float:left;font-size:10px;padding:0px;border:none;padding:10px;" class="dt-sc-button medium with-icon" href="#"> <i class="fa fa-shopping-cart"></i> Copyright &copy; 2016 The MallBD Ltd.</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                
            </div>
        </div>

    </body>
</html>