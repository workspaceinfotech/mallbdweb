
@foreach($allReviews as $review)
<article>
    <div class="clearfix m_bottom_10">
        <p class="f_size_medium f_left f_mxs_none m_mxs_bottom_5">By <b>{{$review->customer->lastName}}</b> - {{ $review->createdOn }}</p>
        <!--rating-->
        <ul class="horizontal_list f_right f_mxs_none clearfix rating_list type_2">
            @for($i=0;$i<$review->rating;$i++)
            <li class="active">
                <i class="fa fa-star-o empty tr_all_hover"></i>
                <i class="fa fa-star active tr_all_hover"></i>
            </li>
           @endfor
            <?php
                $left = 5-($review->rating);?>
                @if($left>0)
                        @for($i=0;$i<$left;$i++)
                            <li>
                                <i class="fa fa-star-o empty tr_all_hover"></i>
                                <i class="fa fa-star active tr_all_hover"></i>
                            </li>
                        @endfor
                    @endif

        </ul>
    </div>
    <p class="m_bottom_15">{{$review->note}}</p>
</article>
    @endforeach
