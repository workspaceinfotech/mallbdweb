<!doctype html>
<!--[if IE 9 ]><html class="ie9" lang="en" ng-app="mallBd"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" ng-app="mallBd"><!--<![endif]-->

    @include('web/partial/header/mainStart')
    @include('web/partial/header/cartScript')
    @include('web/partial/header/mainEnds')
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
    <body ng-controller="CartController" >
        @include('web/partial/loader')
        <!--wide layout-->
        <div class="wide_layout relative">
         
            <!--markup header-->
            <header role="banner" class="type_5 fixed-top">

                <!--header top part-->
                @include('web/partial/header/top')

                <!--main menu container-->
                @include('web/partial/new_menu')
            </header>

            <div class="other-free-gap">
            </div>
            @include('web/partial/sideblock/shippingtext')
            <!--breadcrumbs-->
            <section class="breadcrumbs">
                <div class="container">
                    <ul class="horizontal_list clearfix bc_list f_size_medium">
                        <li class="m_right_10"><a href="{{url('')}}" class="default_t_color">
                                Home
                                <i class="fa fa-angle-right d_inline_middle m_left_10">
                                </i>
                            </a>
                        </li>

                        <li class="m_right_10 current">
                            <a href="javascript:void(0)" class="default_t_color">
                                Compare Products
                            </a>
                        </li> 
                    </ul>
                </div>
            </section>

            <!--content-->
            <div class="page_content_offset">
                <div class="container">
                    <div class="row clearfix">

                        <!--left content column-->

                        <section class="col-lg-12 col-md-12 col-sm-12">
                            <h3 style="margin-bottom:20px;">Compare Products</h3>

                            <div style="padding: 10px;" class="clearfix">

                                <table class="table_type_4 responsive_table full_width r_corners wraper shadow t_align_l t_xs_align_c table-bordered m_bottom_30 table-striped table-cart">
                    <!--<table class="table_type_6 compare_table responsive_table full_width t_align_l r_corners shadow bg_light_color_3 wrapper w_break">-->
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td width="30%" style="color:#000;line-height: 36px;">Product to Compare</td>
                                            <td width="30%" class="custom_btn_size">
                                                <select id="selectedProduct1" class="selectpicker col-md-12 compare_select" data-live-search="true">
                                                    <!--<option value="" style="background-position: left center; background-repeat: no-repeat; background-size: 30px 30px;height: 30px; line-height: 50px; margin-bottom: 5px;padding: 5px;  padding-left: 40px; ">Please select a product</option>-->
                                                    <option value="" style="background-position: left center; background-repeat: no-repeat; background-size: 30px 30px;height: 30px; line-height: 50px; margin-bottom: 5px;padding: 5px;  padding-left: 40px; ">Please select a product</option>
                                                     <?php
                                                    foreach ($allProducts as $prod):
                                                        $img = $imageUrl;
                                                        $image_name = '';
                                                        if (count($prod['productImages']) == 0) {
                                                            $img = $img . 'no_image90.png';
                                                        } else {
                                                            foreach ($prod['productImages'] as $prodimg):
                                                                if ($prodimg['cover'] == 1) {
                                                                    $img = $img . 'product/thumbnail/' . $prodimg['image_name'];
                                                                    $image_name = $prodimg['image_name'];
                                                                }
                                                            endforeach;
                                                        }
                                                        ?>
                                                        <!--<option onclick="compareFirst(<?php //echo $prod['id']; ?>, '<?php //echo addslashes($prod['product_title']); ?>', '<?php //echo $prod['product_description_mobile']; ?>', '<?php //echo $prod['productManufacturer']['name']; ?>', <?php //echo $prod['productPrices'][0]['retail_price']; ?>, <?php //echo $prod['discount']; ?>,<?php //echo $prod['avg_rating']; ?>, '<?php// echo $image_name; ?>', '<?php //echo $imageUrl; ?>')" style="background-image:url(<?php// echo $img; ?>);background-position: left center; background-repeat: no-repeat; background-size: 30px 30px;height: 30px; line-height: 50px; margin-bottom: 5px;padding: 5px;  padding-left: 40px; "><?php //echo $prod['product_title'] ?></option>-->
                                                    <option data-tokens="<?php echo addslashes($prod['product_title']); ?>" value="<?php echo $prod['id']; ?>###<?php echo addslashes($prod['product_title']); ?>###<?php echo $prod['product_description_mobile']; ?>###<?php echo $prod['productManufacturer']['name']; ?>###<?php echo $prod['productPrices'][0]['retail_price']; ?>###<?php echo $prod['discount']; ?>###<?php echo $prod['avg_rating']; ?>###<?php echo $image_name; ?>###<?php echo $imageUrl; ?>" style="background-image:url(<?php echo $img; ?>);background-position: left center; background-repeat: no-repeat; background-size: 30px 30px;height: 30px; line-height: 50px; margin-bottom: 5px;padding: 5px;  padding-left: 40px; "><?php echo $prod['product_title'] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </td>
                                            <td width="30%" class="custom_btn_size">
                                                <select id="selectedProduct2" class=" selectpicker col-md-12 compare_select" data-live-search="true">
                                                    <!--<option value="" style="background-position: left center; background-repeat: no-repeat; background-size: 30px 30px;height: 30px; line-height: 50px; margin-bottom: 5px;padding: 5px;  padding-left: 40px; ">Please select a product</option>-->
                                                    <option value="" style="background-position: left center; background-repeat: no-repeat; background-size: 30px 30px;height: 30px; line-height: 50px; margin-bottom: 5px;padding: 5px;  padding-left: 40px; ">Please select a product</option>
                                                      <?php
                                                    foreach ($allProducts as $prod):
                                                        $img = $imageUrl;
                                                        $image_name = '';
                                                        if (count($prod['productImages']) == 0) {
                                                            $img = $img . 'no_image90.png';
                                                        } else {
                                                            foreach ($prod['productImages'] as $prodimg):
                                                                if ($prodimg['cover'] == 1) {
                                                                    $img = $img . 'product/thumbnail/' . $prodimg['image_name'];
                                                                    $image_name = $prodimg['image_name'];
                                                                }
                                                            endforeach;
                                                        }
                                                        ?>
                                                        <option value="<?php echo $prod['id']; ?>###<?php echo addslashes($prod['product_title']); ?>###<?php echo $prod['product_description_mobile']; ?>###<?php echo $prod['productManufacturer']['name']; ?>###<?php echo $prod['productPrices'][0]['retail_price']; ?>###<?php echo $prod['discount']; ?>###<?php echo $prod['avg_rating']; ?>###<?php echo $image_name; ?>###<?php echo $imageUrl; ?>" style="background-image:url(<?php echo $img; ?>);background-position: left center; background-repeat: no-repeat; background-size: 30px 30px;height: 30px; line-height: 50px; margin-bottom: 5px;padding: 5px;  padding-left: 40px; "><?php echo $prod['product_title'] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td width="30%">
                                                <?php if (count($selectedProductDetails[0]['productImages']) == 0) { ?>
                                                    <img src="{{$imageUrl.'no_image.png'}}"  class="tr_all_hover img-responsive" alt="" onerror="this.src='http://placehold.it/360x360?text=Image not found'">
                                                    <?php
                                                } else {
                                                    foreach ($selectedProductDetails[0]['productImages'] as $img):
                                                        if ($img['cover'] == 1) {
                                                            ?>
                                                            <img src="{{$imageUrl.'product/general/'.$img['image_name']}}" class="tr_all_hover img-responsive" alt="" onerror="this.src='http://placehold.it/360x360?text=Image not found'">
                                                            <?php
                                                        }
                                                    endforeach;
                                                }
                                                ?>
                                            </td>
                                            <td id="image_one" width="30%">
                                                <?php
                                                if (isset($similarProducts[0])) {
                                                    if (count($similarProducts[0]['productImages']) == 0) {
                                                        ?>
                                                        <img src="{{$imageUrl.'no_image.png'}}" class="tr_all_hover img-responsive" alt="" onerror="this.src='http://placehold.it/360x360?text=Image not found'">
                                                        <?php
                                                    } else {
                                                        foreach ($similarProducts[0]['productImages'] as $img):
                                                            if ($img['cover'] == 1) {
                                                                ?>
                                                                <img src="{{$imageUrl.'product/general/'.$img['image_name']}}"  class="tr_all_hover img-responsive" alt="" onerror="this.src='http://placehold.it/360x360?text=Image not found'">
                                                                <?php
                                                            }
                                                        endforeach;
                                                    }
                                                }
                                                ?>
                                            </td>
                                            <td id="image_two" width="30%">
                                                <?php
                                                if (isset($similarProducts[1])) {
                                                    if (count($similarProducts[1]['productImages']) == 0) {
                                                        ?>
                                                        <img src="{{$imageUrl.'no_image.png'}}"  class="tr_all_hover img-responsive" alt="" onerror="this.src='http://placehold.it/360x360?text=Image not found'">
                                                        <?php
                                                    } else {
                                                        foreach ($similarProducts[1]['productImages'] as $img):
                                                            if ($img['cover'] == 1) {
                                                                ?>
                                                                <img src="{{$imageUrl.'product/general/'.$img['image_name']}}"  class="tr_all_hover img-responsive" alt="" onerror="this.src='http://placehold.it/360x360?text=Image not found'">
                                                                <?php
                                                            }
                                                        endforeach;
                                                    }
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="color:#000;">Name</td>
                                            <td style="color:#b50709;">{{$selectedProductDetails[0]['product_title']}}</td>
                                            <td style="color:#b50709;" id="product_title_one"><?php if (isset($similarProducts[0])) echo $similarProducts[0]['product_title']; ?></td>
                                            <td style="color:#b50709;" id="product_title_two"><?php if (isset($similarProducts[1])) echo $similarProducts[1]['product_title']; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="color:#000;">Description</td>
                                            <td>{{$selectedProductDetails[0]['product_description_mobile']}}</td>
                                            <td id="description_one">
                                                <?php
                                                if (isset($similarProducts[0])) {
                                                    if (trim($similarProducts[0]['product_description_mobile']) == '') {
                                                        echo 'No description';
                                                    } else {
                                                        echo $similarProducts[0]['product_description_mobile'];
                                                    }
                                                }
                                                ?>
                                            </td>
                                            <td id="description_two">
                                                <?php
                                                if (isset($similarProducts[1])) {
                                                    if (trim($similarProducts[1]['product_description_mobile']) == '') {
                                                        echo 'No description';
                                                    } else {
                                                        echo $similarProducts[1]['product_description_mobile'];
                                                    }
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="color:#000;">Brand</td>
                                            <td>{{$selectedProductDetails[0]['productManufacturer']['name']}}</td>
                                            <td id="manufacturer_one"><?php if (isset($similarProducts[0])) echo $similarProducts[0]['productManufacturer']['name']; ?></td>
                                            <td id="manufacturer_two"><?php if (isset($similarProducts[1])) echo $similarProducts[1]['productManufacturer']['name']; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="color:#000;">Product Price</td>
                                            <td><?php echo $currency->HTMLCode." ".$selectedProductDetails[0]['productPrices'][0]['retail_price']; ?></td>
                                            <td id="retail_price_one"><?php if (isset($similarProducts[0])) echo $currency->HTMLCode." ".$similarProducts[0]['productPrices'][0]['retail_price']; ?></td>
                                            <td id="retail_price_two"><?php if (isset($similarProducts[1])) echo $currency->HTMLCode." ".$similarProducts[1]['productPrices'][0]['retail_price']; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="color:#000;">Discount</td>
                                            <td><?php echo $currency->HTMLCode." ".$selectedProductDetails[0]['discount']; ?></td>
                                            <td id="discount_one"><?php if (isset($similarProducts[0])) echo $currency->HTMLCode." ".$similarProducts[0]['discount']; ?></td>
                                            <td id="discount_two"><?php if (isset($similarProducts[1])) echo $currency->HTMLCode." ".$similarProducts[1]['discount']; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="color:#000;">Average Rating</td>
                                            <td>{{$selectedProductDetails[0]['avg_rating']}}</td>
                                            <td id="avg_rating_one"><?php if (isset($similarProducts[0])) echo $similarProducts[0]['avg_rating']; ?></td>
                                            <td id="avg_rating_two"><?php if (isset($similarProducts[1])) echo $similarProducts[1]['avg_rating']; ?></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td><button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light f_left mw_0 item-ad-btn" ng-click="addToCart('productJsonObj_{{$selectedProductDetails[0]['id']}}',-1,true)" >Add to Basket</button></td>
                                            <?php if (isset($similarProducts[0])) { ?>
                                                <td id="similarProductsId1">
                                                    <button class="addtobasketProducts1_ button_type_4 bg_scheme_color r_corners tr_all_hover color_light f_left mw_0 item-ad-btn" id="similarProductsIdText1" ng-click="addToCart('productJsonObj_{{$similarProducts[0]['id']}}',-1,true)" >Add to Basket</button>
                                                    <?php
                                                    foreach ($productList as $product):
                                                        ?>    
                                                        <span class="addtobasketProducts1_" id="addtobasketProducts1_{{$product->id}}" style="display:none;" ><button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light f_left mw_0 item-ad-btn" id="similarProductsIdText1" ng-click="addToCart('productJsonObj_{{$product->id}}',-1,true)" >Add to Basket</button></span>

                                                    <?php endforeach; ?> 
                                                </td>
                                            <?php } else { ?>
                                                <td id="similarProductsId1">
                                                    <?php
                                                    foreach ($productList as $product):
                                                        ?>    
                                                        <span class="addtobasketProducts1_" id="addtobasketProducts1_{{$product->id}}" style="display:none;" ><button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light f_left mw_0 item-ad-btn" id="similarProductsIdText1" ng-click="addToCart('productJsonObj_{{$product->id}}',-1,true)" >Add to Basket</button></span>

                                                    <?php endforeach; ?> 
                                                </td>
                                            <?php } if (isset($similarProducts[1])) { ?>
                                                <td id="similarProductsId2" >
                                                    <button class="addtobasketProducts2_ button_type_4 bg_scheme_color r_corners tr_all_hover color_light f_left mw_0 item-ad-btn" id="similarProductsIdText2" ng-click="addToCart('productJsonObj_{{$similarProducts[1]['id']}}',-1,true)" >Add to Basket</button>
                                                <?php
                                            foreach ($productList as $product):
                                                ?>    
                                                <span class="addtobasketProducts1_" id="addtobasketProducts2_{{$product->id}}" style="display:none;" ><button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light f_left mw_0 item-ad-btn" id="similarProductsIdText1" ng-click="addToCart('productJsonObj_{{$product->id}}',-1,true)" >Add to Basket</button></span>

                                            <?php endforeach; ?> 
                                                </td>
                                            <?php } else { ?>
                                                <td id="similarProductsId2">
                                                    <?php
                                            foreach ($productList as $product):
                                                ?>    
                                                <span class="addtobasketProducts2_"  id="addtobasketProducts2_{{$product->id}}" style="display:none;" ><button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light f_left mw_0 item-ad-btn" id="similarProductsIdText1" ng-click="addToCart('productJsonObj_{{$product->id}}',-1,true)" >Add to Basket</button></span>

                                            <?php endforeach; ?> 
                                                </td>
                                            <?php } ?>
                                               

                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </section>
                        <?php
                        foreach ($productList as $product):
                            ?>    
                            <input id="quantity_modifier_{{$product->id}}"type="hidden"   value="1">
                            <div  id="quick_view_product_{{$product->id}}"style="display:none;" >

                            </div>
                            <?php if($product->attributes !=NULL){ ?>
                            @foreach(@$product->attributes as $attributes)
                            @foreach(@$attributes->attributesValue as $attributesValue)
                            <input type="hidden" id="select_attribute_{{$product->id}}_{{$attributes->id}}" value="{{@$attributesValue->id}}" />
                            @endforeach
                            @endforeach
                            <?php } ?>
                            <input type="hidden" id="productJsonObj_{{$product->id}}" value="{{json_encode($product)}}"  />

                        <?php endforeach; ?>
                        <!--right column-->
                    </div>
                </div>
            </div>
            <!--markup footer-->
            @include('web/partial/footer/newbottom')
        </div>
        <!--social widgets-->
        @include('web/partial/social_widgets/main')
<!--        <ul class="social_widgets d_xs_none">
            facebook
            <li class="relative">
                <button class="sw_button t_align_c facebook"><i class="fa fa-facebook"></i></button>
                <div class="sw_content">
                    <h3 class="color_dark m_bottom_20">Join Us on Facebook</h3>
                    <iframe src="http://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fenvato&amp;width=235&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=438889712801266"
                            style="border:none; overflow:hidden; width:235px; height:258px;"></iframe>
                </div>
            </li>
            twitter feed
            <li class="relative">
                <button class="sw_button t_align_c twitter"><i class="fa fa-twitter"></i></button>
                <div class="sw_content">
                    <h3 class="color_dark m_bottom_20">Latest Tweets</h3>

                    <div class="twitterfeed m_bottom_25"></div>
                    <a role="button" class="button_type_4 d_inline_b r_corners tr_all_hover color_light tw_color"
                       href="https://twitter.com/fanfbmltemplate">Follow on Twitter</a>
                </div>
            </li>
            contact form
            <li class="relative">
                <button class="sw_button t_align_c contact"><i class="fa fa-envelope-o"></i></button>
                <div class="sw_content">
                    <h3 class="color_dark m_bottom_20">Contact Us</h3>

                    <p class="f_size_medium m_bottom_15">Lorem ipsum dolor sit amet, consectetuer adipis mauris</p>

                    <form id="contactform" class="mini">
                        <input class="f_size_medium m_bottom_10 r_corners full_width" type="text" name="cf_name"
                               placeholder="Your name">
                        <input class="f_size_medium m_bottom_10 r_corners full_width" type="email" name="cf_email"
                               placeholder="Email">
                        <textarea class="f_size_medium r_corners full_width m_bottom_20" placeholder="Message"
                                  name="cf_message"></textarea>
                        <button type="submit" class="button_type_4 r_corners mw_0 tr_all_hover color_dark bg_light_color_2">
                            Send
                        </button>
                    </form>
                </div>
            </li>
            contact info
            <li class="relative">
                <button class="sw_button t_align_c googlemap"><i class="fa fa-map-marker"></i></button>
                <div class="sw_content">
                    <h3 class="color_dark m_bottom_20">Store Location</h3>
                    <ul class="c_info_list">
                        <li class="m_bottom_10">
                            <div class="clearfix m_bottom_15">
                                <i class="fa fa-map-marker f_left color_dark"></i>

                                <p class="contact_e">8901 Marmora Road,<br> Glasgow, D04 89GR.</p>
                            </div>
                            <iframe class="r_corners full_width" id="gmap_mini"
                                    src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=ru&amp;geocode=&amp;q=Manhattan,+New+York,+NY,+United+States&amp;aq=0&amp;oq=monheten&amp;sll=37.0625,-95.677068&amp;sspn=65.430355,129.814453&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=%D0%9C%D0%B0%D0%BD%D1%85%D1%8D%D1%82%D1%82%D0%B5%D0%BD,+%D0%9D%D1%8C%D1%8E-%D0%99%D0%BE%D1%80%D0%BA,+%D0%9D%D1%8C%D1%8E+%D0%99%D0%BE%D1%80%D0%BA,+%D0%9D%D1%8C%D1%8E-%D0%99%D0%BE%D1%80%D0%BA&amp;ll=40.790278,-73.959722&amp;spn=0.015612,0.031693&amp;z=13&amp;output=embed"></iframe>
                        </li>
                        <li class="m_bottom_10">
                            <div class="clearfix m_bottom_10">
                                <i class="fa fa-phone f_left color_dark"></i>

                                <p class="contact_e">800-559-65-80</p>
                            </div>
                        </li>
                        <li class="m_bottom_10">
                            <div class="clearfix m_bottom_10">
                                <i class="fa fa-envelope f_left color_dark"></i>
                                <a class="contact_e default_t_color" href="mailto:#">info@companyname.com</a>
                            </div>
                        </li>
                        <li>
                            <div class="clearfix">
                                <i class="fa fa-clock-o f_left color_dark"></i>

                                <p class="contact_e">Monday - Friday: 08.00-20.00 <br>Saturday: 09.00-15.00<br> Sunday: closed
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>-->
        <!--custom popup-->
        <div class="popup_wrap d_none" id="quick_view_product">
            <section class="popup r_corners shadow">
                <button class="bg_tr color_dark tr_all_hover text_cs_hover close f_size_large"><i class="fa fa-times"></i>
                </button>
                <div class="clearfix">
                    <div class="custom_scrollbar">
                        <!--left popup column-->
                        <div class="f_left half_column">
                            <div class="relative d_inline_b m_bottom_10 qv_preview">
                                <span class="hot_stripe"><img src="{{asset('/template_resource/images/sale_product.png')}}" alt=""></span>
                                <img src="{{asset('/template_resource/images/quick_view_img_1.jpg')}}" class="tr_all_hover" alt="">
                            </div>
                            <!--carousel-->
                            <div class="relative qv_carousel_wrap m_bottom_20">
                                <button class="button_type_11 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_prev">
                                    <i class="fa fa-angle-left "></i>
                                </button>
                                <ul class="qv_carousel d_inline_middle">
                                    <li data-src="{{asset('/template_resource/images/quick_view_img_1.jpg')}}" ><img src="{{asset('/template_resource/images/quick_view_img_4.jpg')}}" alt="">
                                    </li>
                                    <li data-src="{{asset('/template_resource/images/quick_view_img_2.jpg')}}"><img src="{{asset('/template_resource/images/quick_view_img_5.jpg')}}" alt="">
                                    </li>
                                    <li data-src="{{asset('/template_resource/images/quick_view_img_3.jpg')}}"><img src="{{asset('/template_resource/images/quick_view_img_6.jpg')}}" alt="">
                                    </li>
                                    <li data-src="{{asset('/template_resource/images/quick_view_img_1.jpg')}}" ><img src="{{asset('/template_resource/images/quick_view_img_4.jpg')}}" alt="">
                                    </li>
                                    <li data-src="{{asset('/template_resource/images/quick_view_img_2.jpg')}}" ><img src="{{asset('/template_resource/images/quick_view_img_5.jpg')}}" alt="">
                                    </li>
                                    <li data-src="{{asset('/template_resource/images/quick_view_img_3.jpg')}}" ><img src="{{asset('/template_resource/images/quick_view_img_6.jpg')}}" alt="">
                                    </li>
                                </ul>
                                <button class="button_type_11 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_next">
                                    <i class="fa fa-angle-right "></i>
                                </button>
                            </div>
                            <div class="d_inline_middle">Share this:</div>
                            <div class="d_inline_middle m_left_5">
                                <!-- AddThis Button BEGIN -->
                                <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
                                    <a class="addthis_button_preferred_1"></a>
                                    <a class="addthis_button_preferred_2"></a>
                                    <a class="addthis_button_preferred_3"></a>
                                    <a class="addthis_button_preferred_4"></a>
                                    <a class="addthis_button_compact"></a>
                                    <a class="addthis_counter addthis_bubble_style"></a>
                                </div>
                                <!-- AddThis Button END -->
                            </div>
                        </div>
                        <!--right popup column-->
                        <div class="f_right half_column">
                            <!--description-->
                            <h2 class="m_bottom_10"><a href="#" class="color_dark fw_medium">Eget elementum vel</a></h2>

                            <div class="m_bottom_10">
                                <!--rating-->
                                <ul class="horizontal_list d_inline_middle type_2 clearfix rating_list tr_all_hover">
                                    <li class="active">
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                    <li class="active">
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                    <li class="active">
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                    <li class="active">
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star-o empty tr_all_hover"></i>
                                        <i class="fa fa-star active tr_all_hover"></i>
                                    </li>
                                </ul>
                                <a href="#" class="d_inline_middle default_t_color f_size_small m_left_5">1 Review(s) </a>
                            </div>
                            <hr class="m_bottom_10 divider_type_3">
                            <table class="description_table m_bottom_10">
                                <tr>
                                    <td>Manufacturer:</td>
                                    <td><a href="#" class="color_dark">Chanel</a></td>
                                </tr>
                                <tr>
                                    <td>Availability:</td>
                                    <td><span class="color_green">in stock</span> 20 item(s)</td>
                                </tr>
                                <tr>
                                    <td>Product Code:</td>
                                    <td>PS06</td>
                                </tr>
                            </table>
                            <h5 class="fw_medium m_bottom_10">Product Dimensions and Weight</h5>
                            <table class="description_table m_bottom_5">
                                <tr>
                                    <td>Product Length:</td>
                                    <td><span class="color_dark">10.0000M</span></td>
                                </tr>
                                <tr>
                                    <td>Product Weight:</td>
                                    <td>10.0000KG</td>
                                </tr>
                            </table>
                            <hr class="divider_type_3 m_bottom_10">
                            <p class="m_bottom_10">Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean
                                auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Donec sit amet eros. Lorem ipsum
                                dolor sit amet, consecvtetuer adipiscing elit. </p>
                            <hr class="divider_type_3 m_bottom_15">
                            <div class="m_bottom_15">
                                <s class="v_align_b f_size_ex_large">$152.00</s><span
                                    class="v_align_b f_size_big m_left_5 scheme_color fw_medium">$102.00</span>
                            </div>
                            <table class="description_table type_2 m_bottom_15">
                                <tr>
                                    <td class="v_align_m">Size:</td>
                                    <td class="v_align_m">
                                        <div class="custom_select f_size_medium relative d_inline_middle">
                                            <div class="select_title r_corners relative color_dark">s</div>
                                            <ul class="select_list d_none"></ul>
                                            <select name="product_name">
                                                <option value="s">s</option>
                                                <option value="m">m</option>
                                                <option value="l">l</option>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="v_align_m">Quantity:</td>
                                    <td class="v_align_m">
                                        <div class="clearfix quantity r_corners d_inline_middle f_size_medium color_dark">
                                            <button class="bg_tr d_block f_left" data-direction="down">-</button>
                                            <input type="text" name="" readonly value="1" class="f_left">
                                            <button class="bg_tr d_block f_left" data-direction="up">+</button>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <div class="clearfix m_bottom_15">
                                <button class="button_type_12 r_corners bg_scheme_color color_light tr_delay_hover f_left f_size_large">
                                    Add to Basket
                                </button>
                                <button class="button_type_12 bg_light_color_2 tr_delay_hover f_left r_corners color_dark m_left_5 p_hr_0">
                                    <i class="fa fa-heart-o f_size_big"></i><span
                                        class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span></button>
                                <button class="button_type_12 bg_light_color_2 tr_delay_hover f_left r_corners color_dark m_left_5 p_hr_0">
                                    <i class="fa fa-files-o f_size_big"></i><span
                                        class="tooltip tr_all_hover r_corners color_dark f_size_small">Compare</span></button>
<!--                                <button class="button_type_12 bg_light_color_2 tr_delay_hover f_left r_corners color_dark m_left_5 p_hr_0 relative">
                                    <i class="fa fa-question-circle f_size_big"></i><span
                                        class="tooltip tr_all_hover r_corners color_dark f_size_small">Ask a Question</span>
                                </button>-->
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!--login popup-->
        <style>
            .table_type_4 td{
                padding: 7px;
            }
            .table_type_4 td[colspan], .table_type_4 td[colspan] + td {
                padding-top: 5px;
                padding-bottom: 5px;
            }
        </style>
        <script>
                                                        $(document).ready(function() {
                                                            $(".tog").click(function() {
                                                                $(".formal").slideToggle();
                                                            });
                                                        });
        </script>
        <button class="t_align_c r_corners tr_all_hover type_2 animate_ftl" id="go_to_top" ><i class="fa fa-angle-up"></i>
        </button>
        <!-- Developer : hidden value-->
        <input type="hidden" id="isCheckoutPage" value = "true"/>
        <!--scripts include-->
        <!--scripts include-->
        <script src="{{asset('/template_resource/js/jquery-2.1.0.min.js')}}"></script>
        <script src="{{asset('/template_resource/js/jquery-ui.min.js')}}"></script>
        <script src="{{asset('/template_resource/js/retina.js')}}"></script>
        <script src="{{asset('/template_resource/js/waypoints.min.js')}}"></script>
        <script src="{{asset('/template_resource/js/jquery.isotope.min.js')}}"></script>
        <script src="{{asset('/template_resource/js/jquery.tweet.min.js')}}"></script>
        <script src="{{asset('/template_resource/js/owl.carousel.min.js')}}"></script>
        <script src="{{asset('/template_resource/js/jquery.custom-scrollbar.js')}}"></script>
       
        <script src="{{asset('/template_resource/js/url.min.js')}}"></script>
        <script src="{{asset('/developer/js/wishlist/main.js')}}"></script>
        <script src="{{asset('/developer/js/login/main.js')}}"></script>
        <script src="{{asset('/developer/js/review/reviewCount.js')}}"></script>

        <script src="{{asset('/developer/autocomplete/js/jquery.easy-autocomplete.js')}}"></script>
        <script src="{{asset('/developer/js/search/main.js')}}"></script>
        <script src="{{asset('/developer/js/helper/currencyConvertor.js')}}"></script>
        
        
        <script src="{{asset('/developer/js/script/chackout_script.js')}}"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>




    </body>
</html>

<script>
    function compareFirst(id, product_title, description, manufacturer, retail_price, discount, avg_rating, img, imageUrl) {
        console.log(product_title);
        if (img == '') {
            $('#image_one').html('<img src="' + imageUrl + 'no_image.png" width="242px" height="242px" class="tr_all_hover" alt="">');
        } else {
            $('#image_one').html('<img src="' + imageUrl + 'product/general/' + img + '" width="242px" height="242px" class="tr_all_hover" alt="">');
        }
        var add_to_cart = "addToCart('productJsonObj_" + id + "',-1,true)";
        $('#product_title_one').html(product_title);
        $('#description_one').html(description);
        $('#manufacturer_one').html(manufacturer);
        $('#retail_price_one').html('<?php echo $currency->HTMLCode." ";?>'+retail_price);
        $('#discount_one').html('<?php echo $currency->HTMLCode." ";?>'+discount);
        $('#avg_rating_one').html(avg_rating);
        $('.addtobasketProducts1_').css('display','none');
        $('#addtobasketProducts1_'+id).css('display','block');
//        $('#similarProductsIdText1').attr("ng-click", add_to_cart);
        

    }
    $( "#selectedProduct1" ).change(function() {
        var datastring=$(this).val();
        var data = datastring.split("###");
        compareFirst(data[0],data[1],data[2],data[3],data[4],data[5],data[6],data[7],data[8]);
     });

    function compareSecond(id, product_title, description, manufacturer, retail_price, discount, avg_rating, img, imageUrl) {
        if (img == '') {
            $('#image_two').html('<img src="' + imageUrl + 'no_image.png" width="242px" height="242px" class="tr_all_hover" alt="">');
        } else {
            $('#image_two').html('<img src="' + imageUrl + 'product/general/' + img + '" width="242px" height="242px" class="tr_all_hover" alt="">');
        }
        $('#product_title_two').html(product_title);
        $('#description_two').html(description);
        $('#manufacturer_two').html(manufacturer);
        $('#retail_price_two').html('<?php echo $currency->HTMLCode." ";?>'+retail_price);
        $('#discount_two').html('<?php echo $currency->HTMLCode." ";?>'+discount);
        $('#avg_rating_two').html(avg_rating);
        $('.addtobasketProducts2_').css('display','none');
        $('#addtobasketProducts2_'+id).css('display','block');
//        $('#similarProductsId2').html('<button class="button_type_12 r_corners bg_scheme_color color_light tr_delay_hover f_left f_size_large" ng-click="addToCart("productJsonObj_' + id + '",-1,true)" >Add to Basket</button>');

    }
    $( "#selectedProduct2" ).change(function() {
        var datastring=$(this).val();
        var data = datastring.split("###");
        compareSecond(data[0],data[1],data[2],data[3],data[4],data[5],data[6],data[7],data[8]);
     });
</script>