<!doctype html>
<!--[if IE 9 ]><html class="ie9" lang="en" ng-app="mallBd"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" ng-app="mallBd"><!--<![endif]-->
<?php
//echo '<pre>';
//print_r($orderList);
//die;
?>
@include('web/partial/header/mainStart')
@include('web/partial/header/cartScript')
@include('web/partial/header/mainEnds')

<body ng-controller="CartController" >
    @include('web/partial/loader')
<!--wide layout-->
<div class="wide_layout relative">
    <!--[if (lt IE 9) | IE 9]>
    <div style="background:#fff;padding:8px 0 10px;">
        <div class="container" style="width:1170px;">
            <div class="row wrapper">
                <div class="clearfix" style="padding:9px 0 0;float:left;width:83%;"><i
                        class="fa fa-exclamation-triangle scheme_color f_left m_right_10"
                        style="font-size:25px;color:#e74c3c;"></i><b style="color:#e74c3c;">Attention! This page may not
                    display correctly.</b> <b>You are using an outdated version of Internet Explorer. For a faster, safer
                    browsing experience.</b></div>
                <div class="t_align_r" style="float:left;width:16%;"><a
                        href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"
                        class="button_type_4 r_corners bg_scheme_color color_light d_inline_b t_align_c" target="_blank"
                        style="margin-bottom:2px;">Update Now!</a></div>
            </div>
        </div>
    </div>
    <![endif]-->
    <!--markup header-->
    <header role="banner" class="type_5 fixed-top">

        <!--header top part-->
        @include('web/partial/header/top')

                <!--main menu container-->
        @include('web/partial/new_menu')
    </header>

    <div class="other-free-gap">
    </div>
    <!--breadcrumbs-->
    <section class="breadcrumbs">
    <div class="container">
        <ul class="horizontal_list clearfix bc_list f_size_medium">
            <li class="m_right_10"><a href="{{URL::to('home')}}" class="default_t_color">Home<i
                    class="fa fa-angle-right d_inline_middle m_left_10"></i></a></li>

           <li class="m_right_10 current">
                    <a href="#" class="default_t_color">My Account                      
                    </a>
                </li>
        </ul>
    </div>
</section>
    <!--content-->
    <div class="page_content_offset">
        <div class="container">
            <div class="row clearfix">
                <!--left content column-->
                <section class="col-lg-9 col-md-9 col-sm-9 acc-info">
                    <h2 class="tt_uppercase color_dark m_bottom_25"><i class="fa fa-info-circle"></i> &nbsp;Account Information</h2>
                    <div class="row clearfix m_bottom_30">
                        <div class="col-lg-12 col-md-12 col-sm-12 m_xs_bottom_30">                                               
                            <table class="table_type_6 responsive_table full_width t_align_l r_corners bg_light_color_3 wrapper w_break cstm_class_table account_info">
                                <tr>
                                    <td class="f_size_large d_xs_none"><i class="fa fa-envelope"></i> &nbsp;E-Mail</td>
                                    <td data-title="E-Mail" class=""><a href="mailto:#"
                                                                                   class="color_dark_cstm">{{ @$userInfo->email }}</a></td>
                                </tr>
<!--                                <tr>
                                    <td class="f_size_large d_xs_none"><i class="fa fa-ellipsis-h"></i> &nbsp;Title</td>
                                    <td data-title="Title" class="">
                                        <span class="show-text">Mr</span>
                                        <select class="form-control hide-form" style="display: none;">
                                            <option>Mr</option>
                                            <option>Mrs</option>
                                        </select>
                                    </td>
                                </tr>-->
                                <tr>
                                    <td class="f_size_large d_xs_none"><i class="fa fa-sort-numeric-asc"></i> &nbsp;First Name</td>
                                    <td data-title="First Name" class="">
                                        <span class="show-text">{{ @$userInfo->firstname }}</span>
                                        <input type="text" id="u_firstname" class="form-control hide-form" style="display: none;" value="{{ @$userInfo->firstname }}"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="f_size_large d_xs_none"><i class="fa fa-sort-numeric-desc"></i> &nbsp;Last Name</td>
                                    <td data-title="Last Name" class="">
                                        <span class="show-text">{{ @$userInfo->lastname }}</span>
                                        <input type="text" id="u_lastname" class="form-control hide-form" style="display: none;" value="{{ @$userInfo->lastname }}"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="f_size_large d_xs_none"><i class="fa fa-map-marker"></i> &nbsp;Address 1</td>
                                    <td data-title="Address 1" class="">
                                        <span class="show-text">{{ @$userDetails->address }}</span>
                                        <input type="text" id="u_address" class="form-control hide-form" style="display: none;" value="{{ @$userDetails->address }}"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="f_size_large d_xs_none"><i class="fa fa-thumb-tack"></i> &nbsp;Zip / Postal Code</td>
                                    <td data-title="Zip / Postal Code" class="">
                                        <span class="show-text">{{ @$userDetails->zipcode }}</span>
                                        <input type="text" id="u_zipcode" class="form-control hide-form" style="display: none;" value="{{ @$userDetails->zipcode }}"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="f_size_large d_xs_none"><i class="fa fa-building-o"></i> &nbsp;City</td>
                                    <td data-title="City" class="">
                                        <span class="show-text">{{ @$userDetails->city }}</span>
                                        <div id="city_div">
                                            <select id="u_city" name="u_city" class="form-control hide-form" >
                                                            <?php foreach($districts as $district){  ?>
                                                                    <?php if($userDetails->city == $district['name']){ ?>
                                                                    <option value="<?php echo $district['name']; ?>" selected="selected" ><?php echo $district['name']; ?></option>
                                                                    <?php }else{ ?>
                                                                    <option value="<?php echo $district['name']; ?>"><?php echo $district['name']; ?></option>
                                                                    <?php } ?>
                                                            <?php } ?>
                                        </select>
                                        </div>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td class="f_size_large d_xs_none"><i class="fa fa-globe"></i> &nbsp;Country</td>
                                    <td data-title="Country" class="">
                                        <span class="show-text">{{ @$userDetails->country }}</span>
                                        <input type="text" id="u_country" class="form-control hide-form" style="display: none;" value="{{ @$userDetails->country }}"/>
                                    </td>
                                </tr>
<!--                                <tr>
                                    <td class="f_size_large d_xs_none"><i class="fa fa-compass"></i> &nbsp;State/Province/Region</td>
                                    <td data-title="State/Region" class="">
                                        <span class="show-text">-</span>
                                        <input type="text" id="u_region" class="form-control hide-form" style="display: none;"/>

                                    </td>
                                </tr>-->
                                <tr>
                                    <td class="f_size_large d_xs_none"><i class="fa fa-phone"></i> &nbsp;Phone</td>
                                    <td data-title="Phone" class="">
                                        <span class="show-text">{{ @$userInfo->phone }}</span>
                                        <input type="text" id="u_phone" class="form-control hide-form" style="display: none;" value="{{ @$userInfo->phone }}"/>
                                    </td>
                                </tr>
                            </table>
                            <br>

                            <a href="{{ url('/pass/reset/req') }}"  class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light mw_0 m_bottom_10" style="line-height: 37px;padding: 10px 15px;">Reset Your Password</a>
                            <div class="btn_holder_acnt">
                                <button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light mw_0 m_bottom_10" id="show">Edit Account Information
                                </button>
                                <button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light mw_0 m_bottom_10" style="display: none;" id="hide" onclick="updateProfile()">Save
                                </button>
                            </div>
                        </div>
                    </div>
                    <!--<h2  ng-show="shippingAddressList.length > 0"  class="tt_uppercase color_dark m_bottom_25"><i class="fa fa-info"></i> &nbsp;Shipping Information</h2>-->

                        <!--<button class="button_type_4 r_corners bg_scheme_color color_light tr_all_hover edit-address" ng-click="showShippingAddressAddFrom('element_shipping_address')" >Add new</button>-->

                    <!--<div  class="row clearfix"></div>-->
                    <br>
<!--                    <div ng-repeat="shippingAddress in shippingAddressList">

                        <div ng-show="shippingAddressList.length > 0" >
                            {{--<div class="col-lg-6 col-md-6 col-sm-6 m_xs_bottom_30 bs_inner_offsets bg_light_color_3 shadow r_corners m_bottom_45">--}}
                                {{--<h3 class="checkout-name" ng-bind="shippingAddress.shippingCity"></h3>--}}
                                {{--<address class="checkout-address" ng-bind="shippingAddress.shippingAddress">--}}
                                {{--</address>--}}
                                {{--<br>--}}
                                {{--<button class="button_type_4 r_corners bg_scheme_color color_light tr_all_hover edit-address" ng-click="setShippingAddressAndFocusToElement($index,'element_shipping_address')" >Edit</button>--}}
                                {{--<button ng-click="removeShippingAddress($index)" class="button_type_4 r_corners bg_scheme_color color_light tr_all_hover edit-address">Delete</button>--}}
                            {{--</div>--}}
                            <table class="table_type_6 responsive_table full_width t_align_l r_corners shadow bg_light_color_3 wrapper w_break">
                                {{--<thead>--}}
                                    {{--<tr>--}}
                                        {{--<th>City</th>--}}
                                        {{--<th width="30%">Address</th>--}}
                                        {{--<th>Zipcode</th>--}}
                                        {{--<th>Country</th>--}}
                                        {{--<th>Action</th>--}}
                                    {{--</tr>--}}
                                {{--</thead>--}}
                                <tbody>
                                    <tr>
                                        <td><p ng-bind="shippingAddress.shippingCity"></p></td>
                                        <td width="30%;"><p ng-bind="shippingAddress.shippingAddress"></p></td>
                                        <td><p ng-bind="shippingAddress.shippingZipCode"></p></td>
                                        <td><p ng-bind="shippingAddress.shippingCountry"></p></td>
                                        <td class="normal-cstm-btn-position">
                                            <button ng-click="removeShippingAddress($index)" class="button_type_4 r_corners bg_scheme_color color_light tr_all_hover edit-address edit-ship-address"><i class="fa fa-trash-o"></i></button>
                                            <button class="button_type_4 r_corners bg_scheme_color color_light tr_all_hover edit-address edit-ship-address" ng-click="setShippingAddressAndFocusToElement($index,'element_shipping_address')" ><i class="fa fa-pencil"></i></button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <br>

                    </div>-->
<!--                    <div  class="row clearfix"></div>
                    <div class="tabs m_bottom_45" ng-show="shippingAddressAction.edit||shippingAddressAction.add">
                        <h2 class="tt_uppercase color_dark m_bottom_25"><span>@{{shippingAddressAction.edit?"Edit":"Add"}}</span> shipping address</h2>
                        <form>
                            <ul>
                                <li class="m_bottom_15">
                                    <label for="element_shipping_address" class="d_inline_b m_bottom_5">Shipping address</label>
                                    <input type="text" id="element_shipping_address" name="shipping_address" class="r_corners full_width" ng-model="shippingAddress.shippingAddress" >
                                    <p class="alert-txt"  ng-hide="htmlManipulateObj.shipping_address.isHidden" ng-bind="htmlManipulateObj.shipping_address.htmlValus"></p>
                                </li>
                                <li class="m_bottom_15">
                                    <label for="element_shipping_country" class="d_inline_b m_bottom_5">Shipping country</label>
                                    <input type="text" id="element_shipping_country" name="shipping_country" class="r_corners full_width" ng-model="shippingAddress.shippingCountry" >
                                    <p class="alert-txt" ng-hide="htmlManipulateObj.shipping_country.isHidden" ng-bind="htmlManipulateObj.shipping_country.htmlValus"></p>
                                </li>
                                <li class="m_bottom_15">
                                    <label for="element_shipping_zipcode" class="d_inline_b m_bottom_5">Shipping zipcode</label>
                                    <input  id="element_shipping_zipcode" type="text"class="r_corners full_width" ng-model="shippingAddress.shippingZipCode" >
                                    <p class="alert-txt"  ng-hide="htmlManipulateObj.shipping_zipcode.isHidden" ng-bind="htmlManipulateObj.shipping_zipcode.htmlValus"></p>
                                </li>
                                <li class="m_bottom_15">
                                    <label for="element_shipping_city" class="d_inline_b m_bottom_5">Shipping city</label>
                                    <input type="text"  id="element_shipping_city" name="shipping_city"  name="shipping_zipcode" class="r_corners full_width" ng-model="shippingAddress.shippingCity" >
                                    <p class="alert-txt"  ng-hide="htmlManipulateObj.shipping_city.isHidden" ng-bind="htmlManipulateObj.shipping_city.htmlValus"></p>
                                </li>
                                <li class="m_bottom_15">
                                    <p class="alert-txt"  ng-show="processing.status" ng-bind="processing.msg"></p>
                                    <p class="alert-txt"  ng-show="responseError.status" ng-bind="responseError.msg"></p>
                                    <p class="alert-txt"  ng-show="success.status" ng-bind="success.msg"></p>
                                </li>


                                <li class="m_bottom_15">
                                    <button ng-if="shippingAddressAction.edit" ng-click="editShippingAddress($index)" class="button_type_4 r_corners bg_scheme_color color_light tr_all_hover edit-address">Update</button>
                                    <button ng-if="shippingAddressAction.add" ng-click="addShippingAddress($index)" class="button_type_4 r_corners bg_scheme_color color_light tr_all_hover edit-address">Add</button>
                                </li>
                            </ul>
                        </form>
                    </div>-->

                    <div class="tabs m_bottom_45">
                        <!--tabs navigation-->
                        <nav>
                            <ul class="tabs_nav horizontal_list clearfix">
                               {{-- <li class="f_xs_none"><a href="#tab-1" class="bg_light_color_1 color_dark tr_delay_hover r_corners d_block">Current order
                                        Items</a></li>--}}
                                        <h2  href="#tab-2" class="tt_uppercase color_dark m_bottom_25"><i class="fa fa-arrows"></i> &nbsp;Order History</h2>
                                <!--<li><a href="#tab-2" class="tt_uppercase color_dark m_bottom_25"><i class="fa fa-info-circle"></i>Order History</a></li>-->
                            </ul>
                        </nav>
                        <section class="tabs_content shadow r_corners p_hr_0 p_vr_0 wrapper">
                            {{--<div id="tab-1">
                                <table class="table_type_7 responsive_table full_width t_align_l">
                                    <thead>
                                    <tr class="f_size_large">
                                        <th>Code</th>
                                        <th>Product Name</th>
                                        <th>Product Status</th>
                                        <th>Price</th>
                                        <th>Qty</th>
                                        <th>Tax</th>
                                        <th>Discount</th>
                                        <th>Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td data-title="SKU">PS01</td>
                                        <td data-title="Product Name">
                                            <a href="#" class="color_dark d_inline_b m_bottom_5">Eget elementum vel</a><br>
                                            <ul>
                                                <li>Color: red</li>
                                                <li>Size: M</li>
                                            </ul>
                                        </td>
                                        <td data-title="Product Status">Confirmed by<br> shopper</td>
                                        <td data-title="Price">
                                            <s>$102.00</s>

                                            <p class="f_size_large color_dark">$102.00</p>
                                        </td>
                                        <td data-title="Qty">2</td>
                                        <td data-title="Tax">$13.99</td>
                                        <td data-title="Discount">-$29.00</td>
                                        <td data-title="Total"><p class="color_dark f_size_large">$102.00</p></td>
                                    </tr>
                                    <tr>
                                        <td data-title="SKU">PS02</td>
                                        <td data-title="Product Name">
                                            <a href="#" class="color_dark d_inline_b m_bottom_5">Aenean nec eros</a><br>
                                            <ul>
                                                <li>Color: green</li>
                                                <li>Size: L</li>
                                            </ul>
                                        </td>
                                        <td data-title="Product Status">&nbsp;</td>
                                        <td data-title="Price"><p class="f_size_large color_dark">$52.00</p></td>
                                        <td data-title="Qty">1</td>
                                        <td data-title="Tax">$1.99</td>
                                        <td data-title="Discount">-</td>
                                        <td data-title="Total"><p class="color_dark f_size_large">$52.00</p></td>
                                    </tr>
                                    <tr>
                                        <td data-title="SKU">PS03</td>
                                        <td data-title="Product Name">
                                            <a href="#" class="color_dark d_inline_b m_bottom_5">Ut tellus dolor dapibus</a><br>
                                            <ul>
                                                <li>Color: blue</li>
                                                <li>Size: S</li>
                                            </ul>
                                        </td>
                                        <td data-title="Product Status">&nbsp;</td>
                                        <td data-title="Price"><p class="f_size_large color_dark">$360.00</p></td>
                                        <td data-title="Qty">1</td>
                                        <td data-title="Tax">$4.99</td>
                                        <td data-title="Discount">-$10.00</td>
                                        <td data-title="Total"><p class="color_dark f_size_large">$360.00</p></td>
                                    </tr>
                                    <tr>
                                        <td colspan="5">
                                            <p class="fw_medium f_size_large t_align_r t_xs_align_c">Product prices result:</p>
                                        </td>
                                        <td colspan="1" class="color_dark">$-74.96</td>
                                        <td colspan="1" class="color_dark">$-74.96</td>
                                        <td colspan="1" class="color_dark"><p class="fw_medium f_size_large">$-74.96</p></td>
                                    </tr>
                                    <tr>
                                        <td colspan="5">
                                            <p class="fw_medium f_size_large t_xs_align_c t_align_r">Shipment Fee:</p>
                                        </td>
                                        <td colspan="1" class="color_dark">$6.05</td>
                                        <td colspan="1" class="color_dark">&nbsp;</td>
                                        <td colspan="1" class="color_dark"><p class="fw_medium f_size_large">$6.05</p></td>
                                    </tr>
                                    <tr>
                                        <td colspan="5">
                                            <p class="fw_medium f_size_large t_align_r t_xs_align_c">Payment Fee:</p>
                                        </td>
                                        <td colspan="1" class="color_dark">$17.54</td>
                                        <td colspan="1" class="color_dark">&nbsp;</td>
                                        <td colspan="1" class="color_dark"><p class="fw_medium f_size_large">$17.54</p></td>
                                    </tr>
                                    <tr>
                                        <td colspan="5">
                                            <p class="fw_medium f_size_large t_align_r t_xs_align_c scheme_color">Total:</p>
                                        </td>
                                        <td colspan="1"><p class="color_dark fw_medium f_size_large">$11.05</p></td>
                                        <td colspan="1"><p class="color_dark fw_medium f_size_large">$-74.96</p></td>
                                        <td colspan="1" class="color_dark"><p class="fw_medium f_size_large scheme_color">$101.05</p></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>--}}
                            <div id="tab-2">
                                <table class="table_type_7 responsive_table full_width t_align_l table-bordered" id="mytable">
                                    <thead>
                                    <tr class="f_size_large">
<!--                                        <th>SKU</th>-->
                                        <th>Invoice Number</th>
                                        <th>Product Name</th>
                                        {{--<th>Product Status</th>
                                        <th>Total Qty</th>
                                        <th>Tax</th>
                                        <th>Discount</th>--}}
                                        <th>Total</th>
                                        <th>Product Status</th>
                                    </tr>
                                    </thead>
                                    <div id="offset" hidden>1</div>
                                    <tbody>


                                    @foreach($orderList as $order)
                                        <?php $count = 0; ?>
                                    <tr>

                                        <td data-title="SKU"><a style="padding: 10px;" href="{{url('/order/details/').'/'.$order->id}}" >{{$order->invoiceNo}}</a></td>
                                        <td data-title="Product Name">

                                            @foreach($order->orderProducts as $orderProduct)
                                                @if($orderProduct->itemType==\App\Model\DataModel\OrderProducts::$_PRODUCT && $orderProduct->packageId==0)
                                                    <?php $count++; ?>
                                                    <a href="{{url('/product/'.$orderProduct->item->url.'/'.$orderProduct->item->code)}}" class="color_dark d_inline_b m_bottom_5"><span><b>{{$count}}. </b></span>{{ $orderProduct->item->title }}</a><br>
                                                @elseif($orderProduct->itemType==\App\Model\DataModel\OrderProducts::$_PACKAGE && $orderProduct->packageId>0)
                                                    <?php $count++; ?>
                                                    <a style="padding: 10px;" href="{{url('/packages/'.urlencode($orderProduct->item->packageTitle).'/'.$orderProduct->item->id)}}" class="color_dark d_inline_b m_bottom_5"><span><b>{{$count}}. </b></span>{{ $orderProduct->item->packageTitle }}</a><br>

                                                @endif
                                            @endforeach
                                        </td>
                                        {{--<td data-title="Product Status"><div>{{ $order->orderStatus->orderStatusList->statusName }}</div></td>

                                        <td data-title="Total Qty">{{ $order->totalQuantity }}</td>
                                        <td data-title="Tax"><span>&#2547</span>0</td>
                                        <td data-title="Discount"><span>&#2547</span>0</td>--}}
                                        <td data-title="Total"><p class="color_dark f_size_large"><span><?php echo $currency->HTMLCode; ?></span>{{ number_format(($order->orderTotal - $order->voucher_discount - $order->discount_total + $order->shipping_cost - $order->employee_discount),2) }}</p></td>
                                        <td data-title="Product Status"><p class="color_dark f_size_large">{{ $order->orderStatus->orderStatusList->statusName }}</p></td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <center>
                            <!--<input type="button" class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light mw_0 m_bottom_10" id="loaderbtn" value="Load More" onclick="loadMoreOrders()">-->
                                <button id="loaderbtn"  class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light mw_0 m_bottom_10" style="margin: 20px 0px;" onclick="loadMoreOrders()">Load More</button>
                            </center>
                        </section>
                    </div>
                </section>


                <!--right column-->
                <aside class="col-lg-3 col-md-3 col-sm-3">
                    <!--widgets-->
                    
                    <!--banner-->
                    <a href="#" class="d_block r_corners m_bottom_30 hidden-xs">
                        <img src="{{asset('/template_resource/images/banner_img_6.jpg')}}" alt="">
                    </a>
                    <!--Bestsellers-->
                    
                @include('web/partial/sideblock/bestsellers')
                    
                    <a href="#" class="d_block r_corners m_bottom_30 hidden-xs">
                        <img src="{{asset('/template_resource/images/banner_img_6.jpg')}}" alt="">
                    </a>
                    
                    <!--tags-->                    
                    @include('web/partial/sideblock/tags')
                    
                    <a href="#" class="d_block r_corners m_bottom_30 hidden-xs">
                        <img src="{{asset('/template_resource/images/banner_img_6.jpg')}}" alt="">
                    </a>
                </aside>
            </div>
        </div>
    </div>
    <!--markup footer-->

    @include('web/partial/footer/newbottom')

</div>
<!--social widgets-->
@include('web/partial/social_widgets/main')
<!--login popup-->
<div class="popup_wrap d_none" id="login_popup">
    <section class="popup r_corners shadow">
        <button class="bg_tr color_dark tr_all_hover text_cs_hover close f_size_large"><i class="fa fa-times"></i>
        </button>
        <h3 class="m_bottom_20 color_dark">Log In</h3>

        <form>
            <ul>
                <li class="m_bottom_15">
                    <label for="username" class="m_bottom_5 d_inline_b">Username</label><br>
                    <input type="text" name="" id="username" class="r_corners full_width">
                </li>
                <li class="m_bottom_25">
                    <label for="password" class="m_bottom_5 d_inline_b">Password</label><br>
                    <input type="password" name="" id="password" class="r_corners full_width">
                </li>
                <li class="m_bottom_15">
                    <input type="checkbox" class="d_none" id="checkbox_10"><label for="checkbox_10">Remember me</label>
                </li>
                <li class="clearfix m_bottom_30">
                    <button class="button_type_4 tr_all_hover r_corners f_left bg_scheme_color color_light f_mxs_none m_mxs_bottom_15">
                        Log In
                    </button>
                    <div class="f_right f_size_medium f_mxs_none">
                        <a href="#" class="color_dark">Forgot your password?</a><br>
                        <a href="#" class="color_dark">Forgot your username?</a>
                    </div>
                </li>
            </ul>
        </form>
        <footer class="bg_light_color_1 t_mxs_align_c">
            <h3 class="color_dark d_inline_middle d_mxs_block m_mxs_bottom_15">New Customer?</h3>
            <a href="#" role="button"
               class="tr_all_hover r_corners button_type_4 bg_dark_color bg_cs_hover color_light d_inline_middle m_mxs_left_0">Create
                an Account</a>
        </footer>
    </section>
</div>
<button class="t_align_c r_corners tr_all_hover type_2 animate_ftl" id="go_to_top"><i class="fa fa-angle-up"></i>
</button>
<!--scripts include-->

<input type="hidden" id="isMyAccountPage" value="true" />

@include('web/partial/script/core')

<script src="{{asset('/template_resource/js/scripts.js')}}"></script>
<link href="{{asset('/developer/select2/select2.min.css')}}"  rel="stylesheet" type="text/css" media="all" >
        <script src="{{asset('/developer/select2/select2.min.js')}}"></script>
<script>
    $(document).ready(function() {
          $("#u_city").select2();
          $('#city_div').css("display","none");
        });
        $("#show").click(function () {
            $('#city_div').css("display","block");
            $(".show-text").hide();
            $(".hide-form").show();
            $("#hide").show();
        });
        $("#hide").click(function () {
            $('#city_div').css("display","none");
            $(".hide-form").hide();
            $(".show-text").show();
        });

</script>

<script>
    function updateProfile()
    {
        $.ajax({

            url: $("#baseUrl").val() + "api/myaccount/update",
            method: "POST",
            data: {
                "firstname": $("#u_firstname").val(),
                "lastname": $("#u_lastname").val(),
                "phone": $("#u_phone").val(),
                "address": $("#u_address").val(),
                "zipcode": $("#u_zipcode").val(),
                "city": $("#u_city").val(),
                "country": $("#u_country").val()

            },
            success: function (data) {
                if (data.responseStat.status) {
                    $("#errmsg").html(data.responseStat.msg);
                    console.log(data);
                    location.reload();
                }
                else {
                    $("#errmsg").html(data.responseStat.msg);
                    console.log(data);
                }


            }

        });
    }

    function loadMoreOrders(){

        $.ajax({
            url: $("#baseUrl").val() + "order/partial/get",
            method: "POST",
            data: {
                "offset" : $('#offset').html()
            },
            success: function (data) {

                if(data != ""){
                    $('#offset').html(parseInt($('#offset').html())+1);
                    $('#mytable tbody').append(data);
                }else{
                    $('#loaderbtn').fadeOut(2000);
                }


            }

        });
    }
</script>

</body>
</html>