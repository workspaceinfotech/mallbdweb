<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title>The MallBD - 404 Page Not Found</title>

	<meta name="description" content="">
	<meta name="author" content="">

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
        <link rel="icon" type="image/ico" href="{{asset('/template_resource/images/fav.ico')}}">
	<!-- BEGIN CORE CSS -->
        <link rel="stylesheet" type="text/css" media="all" href="{{asset('/error_resource/css/admin1.css')}}">
        <link rel="stylesheet" type="text/css" media="all" href="{{asset('/error_resource/css/elements.css')}}">
	<!-- END CORE CSS -->
        

	<!-- BEGIN PLUGINS CSS -->
</head>
<body class="http-error-outside">

	<div class="error-bg error-bg1"></div><!--.error-bg1-->

	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
                            <img src="{{asset('/template_resource/images/logo.png')}}" />
				<div class="error-number">404</div>
                                <p style="color: #fff;font-size: 28px;">Page Not Found</p>
				<div class="error-text">
					<p style="color: #fff;font-size: 16px;">You tried to access /not-found-page, which have been deleted or never have existed. </p>
                                        <p style="color: rgb(255, 163, 163);font-size: 20px;">You can go back to <a style="color: #fff;font-weight: 600;" href="{{ url('/home') }}">homepage</a>.</p>
				</div>

<!--				<div class="input-group">
					<div class="inputer">
						<div class="input-wrapper">
							<input type="text" class="form-control">
						</div>
					</div>
					<div class="input-group-btn">
						<button type="button" class="btn btn-default">Search</button>
					</div>.input-group-btn
				</div>.input-group-->

			</div>
		</div><!--.row-->
	</div><!--.container-->

</body>
</html>